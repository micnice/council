package zw.co.hitrac.council.web.pages.registration;

//~--- non-JDK imports --------------------------------------------------------
import java.util.List;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.Panel;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.pages.examinations.RegistrantExaminationsPage;
import zw.co.hitrac.council.web.utility.CustomDateLabel;
import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;

/**
 *
 * @author tdhlakama
 * @author Michael Matiashe
 */
public class RegistrationsPanelList extends Panel {

    /**
     *
     * * @param registrant
     */
    public RegistrationsPanelList(String id, final List<Registration> registrations) {
        super(id);
        add(new PropertyListView<Registration>("registrationsList", registrations) {
            @Override
            protected void populateItem(ListItem<Registration> item) {
                item.add(new CustomDateLabel("registrationDate"));
                item.add(new CustomDateLabel("deRegistrationDate"));
                item.add(new Label("institution"));
                item.add(new Label("register"));
                item.add(new Label("course"));

                Link<Registration> viewLink = new Link<Registration>("registrantExaminationsPage", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new RegistrantExaminationsPage(getModelObject().getId()));
                    }

                    @Override
                    protected void onConfigure() {
                        if (getModelObject().getExamRegistrationsList().isEmpty()) {
                            setVisible(Boolean.FALSE);
                        }
                    }
                };

                //appear only for administrator
                Link<Registration> viewLink2 = new Link<Registration>("registrationClosePage", item.getModel()) {
                    // any application
                    @Override
                    public void onClick() {
                        setResponsePage(new RegistrationClosePage(getModelObject().getId()));
                    }

                    @Override
                    protected void onConfigure() {
                        setVisibilityAllowed(CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.IT_OFFICER))));
                    }
                };

                //Administrator only no payment for these changes
                Link<Registration> viewLink3 = new Link<Registration>("registrationPage", item.getModel()) {
                    // any application
                    @Override
                    public void onClick() {
                        setResponsePage(new RegistrationPage(getModelObject().getId(),null,null ,null,null));
                    }

                    @Override
                    protected void onConfigure() {
                        setVisibilityAllowed(CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR,Role.IT_OFFICER))));
                    }
                };

                //~ Formatted by Jindent --- http://www.jindent.com
                item.add(viewLink);
                item.add(viewLink2);
                item.add(viewLink3);
            }
        });
    }
}