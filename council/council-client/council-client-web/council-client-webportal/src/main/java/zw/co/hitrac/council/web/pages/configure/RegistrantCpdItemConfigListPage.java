
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.RegistrantCpdItemConfig;
import zw.co.hitrac.council.business.service.RegistrantCpdItemConfigService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;

/**
 *
 * @author hitrac
 */
public class RegistrantCpdItemConfigListPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private RegistrantCpdItemConfigService addressTypeService;

    public RegistrantCpdItemConfigListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantCpdItemConfigEditPage(null));
            }
        });

        IModel<List<RegistrantCpdItemConfig>> model = new LoadableDetachableModel<List<RegistrantCpdItemConfig>>() {
            @Override
            protected List<RegistrantCpdItemConfig> load() {
                return addressTypeService.findAll();
            }
        };
        PropertyListView<RegistrantCpdItemConfig> eachItem = new PropertyListView<RegistrantCpdItemConfig>("eachItem",
                                                                 model) {
            @Override
            protected void populateItem(ListItem<RegistrantCpdItemConfig> item) {
                Link<RegistrantCpdItemConfig> viewLink = new Link<RegistrantCpdItemConfig>("viewLink",
                                                             item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new RegistrantCpdItemConfigViewPage(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };

        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
