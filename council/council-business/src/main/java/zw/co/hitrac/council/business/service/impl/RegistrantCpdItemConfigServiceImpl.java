/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.RegistrantCpdItemConfigDAO;
import zw.co.hitrac.council.business.domain.RegistrantCpdItemConfig;
import zw.co.hitrac.council.business.service.RegistrantCpdItemConfigService;

import java.util.List;

/**
 *
 * @author hitrac
 */
@Service
@Transactional
public class RegistrantCpdItemConfigServiceImpl implements RegistrantCpdItemConfigService {

    @Autowired
    private RegistrantCpdItemConfigDAO registrantCpdItemConfigDAO;

    @Transactional
    public RegistrantCpdItemConfig save(RegistrantCpdItemConfig registrantCpdItemConfig) {
        return registrantCpdItemConfigDAO.save(registrantCpdItemConfig);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantCpdItemConfig> findAll() {
        return registrantCpdItemConfigDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantCpdItemConfig get(Long id) {
        return registrantCpdItemConfigDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantCpdItemConfig> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantCpdItemConfig get() {
        List<RegistrantCpdItemConfig> parameters = registrantCpdItemConfigDAO.findAll();
        if (parameters.isEmpty()) {
            return new RegistrantCpdItemConfig();
        } else {
            return parameters.get(0);
        }
    }

    public void setRegistrantCpdItemConfigDAO(RegistrantCpdItemConfigDAO registrantCpdItemConfigDAO) {
        this.registrantCpdItemConfigDAO = registrantCpdItemConfigDAO;
    }
}
