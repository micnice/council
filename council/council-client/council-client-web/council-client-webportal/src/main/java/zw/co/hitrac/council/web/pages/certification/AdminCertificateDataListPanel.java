/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.certification;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PageableListView;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigator;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import zw.co.hitrac.council.business.domain.ProductIssuance;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.utility.CustomDateLabel;
import zw.co.hitrac.council.web.utility.CustomDateTimeLabel;

import java.util.List;

/**
 *
 * @author tdhlakama
 */
public class AdminCertificateDataListPanel extends Panel {

    public AdminCertificateDataListPanel(String id, IModel<List<ProductIssuance>> model) {
        super(id);
        PageableListView<ProductIssuance> eachItem = new PageableListView<ProductIssuance>("eachItem", model, 20) {
            @Override
            protected void populateItem(ListItem<ProductIssuance> item) {
                item.setModel(new CompoundPropertyModel<ProductIssuance>(item.getModel()));
                item.add(new Link<ProductIssuance>("edit", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new CertificationEditPage(getModelObject().getId(),null));
                    }
                });

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                item.add(new Label("registrant.fullname"));
                item.add(new Label("productNumber"));
                item.add(new Label("productIssuanceType"));
                item.add(new Label("issueDate"));
                item.add(new Label("createdBy"));
                item.add(new Label("modifiedBy"));
                item.add(new CustomDateLabel("dateCreated"));
                item.add(new CustomDateLabel("dateModified"));
            }
        };

        add(new PagingNavigator("navigator", eachItem));
        add(eachItem);
    }
}
