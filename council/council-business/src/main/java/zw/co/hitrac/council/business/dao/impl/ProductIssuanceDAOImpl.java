package zw.co.hitrac.council.business.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.ProductIssuanceDAO;
import zw.co.hitrac.council.business.dao.repo.ProductIssuanceRepository;
import zw.co.hitrac.council.business.domain.ProductIssuance;
import zw.co.hitrac.council.business.domain.ProductIssuanceType;
import zw.co.hitrac.council.business.domain.Registrant;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;

/**
 * @author Edward Zengeni
 * @author Matiashe Michael
 */
@Repository
public class ProductIssuanceDAOImpl implements ProductIssuanceDAO {

    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private ProductIssuanceRepository productIssuanceRepository;

    public ProductIssuance save(ProductIssuance productIssuance) {
        return productIssuanceRepository.save(productIssuance);
    }

    public List<ProductIssuance> findAll() {
        return productIssuanceRepository.findAll();
    }

    public ProductIssuance get(Long id) {
        return productIssuanceRepository.findOne(id);
    }

    public List<ProductIssuance> getProductIssuances(Registrant registrant) {
        return entityManager.createQuery("select pi from ProductIssuance pi where pi.registrant=:registrant and pi.transactionDebtComponent.remainingBalance<=0").setParameter("registrant", registrant).getResultList();
    }

    public List<ProductIssuance> getProductIssuances(ProductIssuanceType productIssuanceType, Registrant registrant) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ProductIssuance.class);
        criteria.add(Restrictions.eq("productIssuanceType", productIssuanceType));
        criteria.add(Restrictions.isNull("issueDate"));
        criteria.add(Restrictions.eq("registrant", registrant));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).addOrder(Order.desc("createDate")).list();
    }

    public List<ProductIssuance> getProductAllIssuances(Registrant registrant) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ProductIssuance.class);
        criteria.add(Restrictions.eq("registrant", registrant));
        criteria.add(Restrictions.isNull("issueDate"));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).addOrder(Order.desc("createDate")).list();
    }

    /**
     * A setter method that will make mocking repo object easier
     *
     * @param productIssuanceRepository
     */
    public void setProductIssuanceRepository(ProductIssuanceRepository productIssuanceRepository) {
        this.productIssuanceRepository = productIssuanceRepository;
    }

    public List<ProductIssuance> getProductIssuanceList(String query, String productNumber) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ProductIssuance.class);
        if (query != null) {
            Criteria r = criteria.createCriteria("registrant");
            r.add(Restrictions.or(Restrictions.eq("idNumber", query), Restrictions.or(Restrictions.eq("registrationNumber", query))));
        }
        if (productNumber != null) {
            criteria.add(Restrictions.eq("productNumber", productNumber));
            criteria.add(Restrictions.or(Restrictions.eq("productNumber", productNumber), Restrictions.or(Restrictions.eq("practiceCertificateNumber", productNumber),
                    Restrictions.or(Restrictions.eq("diplomaCertificateNumber", productNumber), Restrictions.eq("cgsCertificateNumber", productNumber)), Restrictions.or(Restrictions.eq("registrationCertificateNumber", productNumber), Restrictions.eq("barCode", productNumber)))));
        }

        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).addOrder(Order.desc("createDate")).list();
    }

    public List<ProductIssuance> getProductIssuances(ProductIssuanceType productIssuanceType) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ProductIssuance.class);
        criteria.add(Restrictions.eq("productIssuanceType", productIssuanceType));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).addOrder(Order.desc("createDate")).list();
    }

    public List<ProductIssuance> getProductUnClosedIssuances(ProductIssuanceType productIssuanceType) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ProductIssuance.class);
        criteria.add(Restrictions.eq("productIssuanceType", productIssuanceType));
        criteria.add(Restrictions.isNull("issueDate"));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).addOrder(Order.desc("createDate")).list();
    }

    public List<ProductIssuance> getProductIssuances(ProductIssuanceType certificateType, Date startDate, Date endDate) {
        return entityManager.createQuery("select p from ProductIssuance p where p.productIssuanceType = :certificateType " +
                " and (p.createDate between :startDate and :endDate)").setParameter("certificateType", certificateType).setParameter("startDate", startDate).setParameter("endDate", endDate).getResultList();

    }

    public List<ProductIssuance> getUnIssuedPractisingCertificates(Registrant registrant) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ProductIssuance.class);
        criteria.add(Restrictions.eq("productIssuanceType", ProductIssuanceType.PRACTICING_CERTIFICATE));
        criteria.add(Restrictions.isNull("issueDate"));
        criteria.add(Restrictions.eq("registrant", registrant));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).addOrder(Order.desc("createDate")).list();
    }

    public List<ProductIssuance> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
