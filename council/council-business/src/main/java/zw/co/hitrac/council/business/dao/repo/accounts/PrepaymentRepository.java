
package zw.co.hitrac.council.business.dao.repo.accounts;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.accounts.Prepayment;

/**
 *
 * @author Michael Matiashe
 */
public interface PrepaymentRepository extends CrudRepository<Prepayment, Long> {

    public List<Prepayment> findAll();

    @Query("from Prepayment where canceled=:canceled")
    public List<Prepayment> findByCanceled(@Param("canceled") Boolean canceled);
}
