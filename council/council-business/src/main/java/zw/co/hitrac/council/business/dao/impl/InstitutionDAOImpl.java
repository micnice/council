package zw.co.hitrac.council.business.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.InstitutionDAO;
import zw.co.hitrac.council.business.dao.repo.InstitutionRepository;
import zw.co.hitrac.council.business.domain.City;
import zw.co.hitrac.council.business.domain.District;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.InstitutionCategory;
import zw.co.hitrac.council.business.domain.InstitutionType;
import zw.co.hitrac.council.business.domain.Province;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.AccountType;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.service.GeneralParametersService;

/**
 *
 * @author tidza
 */
@Repository
public class InstitutionDAOImpl implements InstitutionDAO {

    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private InstitutionRepository institutionRepository;
    @Autowired
    GeneralParametersService generalParametersService;

    public Institution save(Institution institution) {
        if (institution.getCustomerAccount() == null || institution.getCustomerAccount().getId() == null) {
            institution.setCustomerAccount(new Customer());
            institution.getCustomerAccount().setCode(UUID.randomUUID().toString());
            institution.getCustomerAccount().setAccount(new Account());
            institution.getCustomerAccount().getAccount().setBalance(BigDecimal.ZERO);
            institution.getCustomerAccount().getAccount().setAccountType(AccountType.ACCOUNTS_RECEIVABLE);
            institution.getCustomerAccount().getAccount().setCode(UUID.randomUUID().toString());
            institution.getCustomerAccount().setCustomerName(institution.getName());
            institution.getCustomerAccount().getAccount().setName(institution.getName());
            institution.getCustomerAccount().getAccount().setPersonalAccount(Boolean.TRUE);
        }
        return institutionRepository.save(institution);
    }

    public List<Institution> findAll() {
        return institutionRepository.findAll();
    }

    public Institution get(Long id) {
        return institutionRepository.findOne(id);
    }

    public void setInstitutionRepository(InstitutionRepository institutionRepository) {
        this.institutionRepository = institutionRepository;
    }

    public Institution getInstitutionByAccount(Customer customer) {
        List<Institution> institutions
                = entityManager.createQuery("select i from Institution i where i.customerAccount=:customer").setParameter("customer", customer).getResultList();

        if (institutions.isEmpty()) {
            return null;
        } else {
            return institutions.get(0);
        }

    }

    public Boolean checkInstitutionSupervisor(Registrant registrant) {
        List<Institution> institutions
                = entityManager.createQuery("select i from Institution i where i.supervisor=:registrant").setParameter("registrant", registrant).getResultList();
        if (institutions.isEmpty()) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }
    }

    public Institution getInstitutionBySupervisor(Registrant registrant) {
        List<Institution> institutions
                = entityManager.createQuery("select i from Institution i where i.supervisor=:registrant").setParameter("registrant", registrant).getResultList();
        if (institutions.isEmpty()) {
            return null;
        } else {
            return institutions.get(0);
        }
    }

    public List<Institution> getInstitutionByPractitionerInCharge(Registrant registrant) {
        return entityManager.createQuery("select i from Institution i where i.registrant=:registrant").setParameter("registrant", registrant).getResultList();

    }

    public Institution getInstitution(Customer customer) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Institution.class);
        criteria.add(Restrictions.eq("customerAccount", customer));
        return (Institution) criteria.uniqueResult();
    }

    public List<Institution> getInstitutionInDistrict(District district) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Institution.class);
        criteria.add(Restrictions.eq("district", district));
        criteria.addOrder(Order.asc("name"));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<Institution> getAll(Boolean academic, Boolean school, Boolean council) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Institution.class);
        if (academic != null) {
            criteria.add(Restrictions.eq("academic", academic));
        }
        if (school != null) {
            criteria.add(Restrictions.eq("trainingInstitution", school));
        }
        if (council != null) {
            criteria.add(Restrictions.eq("council", council));
        }
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        criteria.addOrder(Order.asc("name"));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<Institution> getInstitutions(InstitutionType institutionType) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Institution.class);
        criteria.add(Restrictions.eq("institutionType", institutionType));
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        criteria.addOrder(Order.asc("name"));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<Institution> getInstitutions(String code, InstitutionType institutionType, InstitutionCategory institutionCategory, Boolean retired) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Institution.class);
        if (institutionType != null) {
            criteria.add(Restrictions.eq("institutionType", institutionType));
        }
        if (institutionCategory != null) {
            criteria.add(Restrictions.eq("institutionCategory", institutionCategory));
        }
        if (retired != null) {
            criteria.add(Restrictions.eq("retired", retired));
        } else {
            criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        }
        if (code != null) {
            criteria.add(Restrictions.or(Restrictions.like("name", code, MatchMode.START), Restrictions.or(Restrictions.eq("code", code))));
            criteria.setMaxResults(60);
        }
        criteria.addOrder(Order.asc("name"));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public Long getTotalCount(Province province, District district, City city, InstitutionType institutionType, InstitutionCategory institutionCategory) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Institution.class);
        ProjectionList p = Projections.projectionList().add(Projections.rowCount());
        Conjunction exist = (Conjunction) Restrictions.conjunction();
        if (institutionType != null) {
            exist.add(Restrictions.eq("institutionType", institutionType));
        }
        if (institutionCategory != null) {
            exist.add(Restrictions.eq("institutionCategory", institutionCategory));
        }
        if (province != null) {
            criteria.createAlias("district", "d");
            exist.add(Restrictions.eq("d.province", province));
        }
        if (district != null) {
            exist.add(Restrictions.eq("district", district));
        }
        if (city != null) {
            exist.add(Restrictions.eq("city", city));
        }
        exist.add(Restrictions.eq("retired", Boolean.FALSE));
        criteria.setProjection(p).add(exist);
        return ((Long) criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).uniqueResult());
    }

    @Override
    public void createInstitutionAccounts() {
        List<Institution> institutions =new ArrayList<Institution>();
        Session session =entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Institution.class);
        criteria.add(Restrictions.isNull("customerAccount"));
        institutions = (List<Institution>) criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        for (Institution institution : institutions){
            save(institution);
        }
    }

    public void removeDuplicates(Institution institution, List<Institution> institutions) {
        String list = "";
        for (Institution d : institutions) {
            list += d.getId().toString() + ",";
        }
        list = list.substring(0, list.length() - 1);
        String queryString = "update registrantqualification ra set ra.institution_id = " + institution.getId() + ", ra.awardingInstitution_id = " + institution.getId() + " WHERE ra.institution_id IN (" + list + ")";
        entityManager.createNativeQuery(queryString).executeUpdate();
    }

    public void retire(List<Institution> institutions) {
        String list = "";
        for (Institution d : institutions) {
            list += d.getId().toString() + ",";
        }
        list = list.substring(0, list.length() - 1);
        String queryString = "update institution i set retired = true WHERE id IN (" + list + ")";
        entityManager.createNativeQuery(queryString).executeUpdate();
    }

    public List<Institution> findAll(Boolean retired) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Institution.class);
        criteria.add(Restrictions.eq("retired", retired));
        criteria.addOrder(Order.asc("name"));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

}
