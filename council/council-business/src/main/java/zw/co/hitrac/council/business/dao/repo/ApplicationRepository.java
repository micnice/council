package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import zw.co.hitrac.council.business.domain.Application;

/**
 *
 * @author Michael Matiashe
 */
public interface ApplicationRepository extends CrudRepository<Application, Long> {

    public List<Application> findAll();

    public List<Application> findByApplicationStatus(@Param("applicationStatus") String applicationStatus);
}
