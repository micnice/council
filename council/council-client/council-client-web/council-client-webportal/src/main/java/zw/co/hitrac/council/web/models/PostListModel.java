package zw.co.hitrac.council.web.models;

import java.util.List;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.Post;
import zw.co.hitrac.council.business.service.PostService;

/**
 *
 * @author Michael Matiashe
 */
public class PostListModel extends LoadableDetachableModel<List<Post>> {
    
    private final PostService postService;

    public PostListModel(PostService postService) {
        this.postService = postService;
    }
    

    @Override
    protected List<Post> load() {
      return this.postService.findAll();
    }
    
}
