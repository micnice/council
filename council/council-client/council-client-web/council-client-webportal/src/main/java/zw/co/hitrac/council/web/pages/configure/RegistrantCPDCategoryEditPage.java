package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.RegistrantCPDCategory;
import zw.co.hitrac.council.business.service.RegistrantCPDCategoryService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Constance Mabaso
 */
public class RegistrantCPDCategoryEditPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private RegistrantCPDCategoryService registrantCPDCategoryService;

    public RegistrantCPDCategoryEditPage(Long id) {
        setDefaultModel(
            new CompoundPropertyModel<RegistrantCPDCategory>(new LoadableDetachableRegistrantCPDCategoryModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));

        Form<RegistrantCPDCategory> form = new Form<RegistrantCPDCategory>("form",
                                               (IModel<RegistrantCPDCategory>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                registrantCPDCategoryService.save(getModelObject());
                setResponsePage(new RegistrantCPDCategoryViewPage(getModelObject().getId()));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("maximumPoints").add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink", RegistrantCPDCategoryListPage.class));
        add(form);
    }

    private final class LoadableDetachableRegistrantCPDCategoryModel
            extends LoadableDetachableModel<RegistrantCPDCategory> {
        private Long id;

        public LoadableDetachableRegistrantCPDCategoryModel(Long id) {
            this.id = id;
        }

        @Override
        protected RegistrantCPDCategory load() {
            if (id == null) {
                return new RegistrantCPDCategory();
            }

            return registrantCPDCategoryService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
