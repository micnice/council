package zw.co.hitrac.council.web.pages.institution;

//~--- non-JDK imports --------------------------------------------------------

import com.googlecode.wicket.jquery.ui.form.button.AjaxButton;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButton;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButtons;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogIcon;
import com.googlecode.wicket.jquery.ui.widget.dialog.MessageDialog;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.InstitutionManager;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.domain.accounts.PaymentDetails;
import zw.co.hitrac.council.business.domain.accounts.Prepayment;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.InstitutionManagerService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.service.accounts.PaymentDetailsService;
import zw.co.hitrac.council.business.service.accounts.PrepaymentService;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.accounts.documents.InvoicePage;
import zw.co.hitrac.council.web.pages.accounts.documents.PrepaymentEditPage;
import zw.co.hitrac.council.web.pages.accounts.documents.PrepaymentViewPage;
import zw.co.hitrac.council.web.pages.accounts.payments.TransactionDetailHistory;
import zw.co.hitrac.council.web.pages.application.SupervisionApplicationPage;
import zw.co.hitrac.council.web.pages.registration.RegistrantAddressPanelList;
import zw.co.hitrac.council.web.pages.registration.RegistrantContactPanelList;
import zw.co.hitrac.council.web.utility.CustomDateLabel;

import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;

/**
 * @author charlesc
 */
public class InstitutionViewPage extends IInstitutionView {

    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private PrepaymentService prepaymentService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private RegistrationService registrationService;
    @SpringBean
    private PaymentDetailsService paymentDetailsService;
    @SpringBean
    private InstitutionManagerService institutionManagerService;

    public InstitutionViewPage(Long id) {
        final CompoundPropertyModel<Institution> model = new CompoundPropertyModel<Institution>(new LoadableDetachableInstitutionModel(id));

        setDefaultModel(model);
        add(new Label("name"));
        add(new Label("schoolName"));
        add(new Label("institutionType"));
        add(new Label("institutionCategory"));
        add(new Label("services"));
        add(new Label("institution", model.bind("name")));
        add(new Label("customerAccount.account.balance"));
        add(new Label("examiniationCenter"));
        add(new Label("council"));


        IModel<List<InstitutionManager>> institutionManagerModelList = new LoadableDetachableModel<List<InstitutionManager>>() {

            @Override
            protected List<InstitutionManager> load() {

                return institutionManagerService.findInstitutionManagers(null, model.getObject());
            }
        };


        add(new Label("supervisors", new LoadableDetachableModel<String>() {

            @Override
            protected String load() {
                String names = "";
                for (InstitutionManager institutionManager : institutionManagerModelList.getObject()) {
                    if (institutionManager.isInstitutionSupervisor() && institutionManager.getInstitutionSupervisorEndDate() == null) {
                        names = names + institutionManager.getRegistrant().getFullname() + " - ";
                    }
                }
                if (names.isEmpty()) {
                    return "N/A";
                }
                names = names.substring(0, names.length() - 2);
                return names;
            }
        }));

        add(new Label("practitionersInCharge", new LoadableDetachableModel<String>() {

            @Override
            protected String load() {
                String names = "";
                for (InstitutionManager institutionManager : institutionManagerModelList.getObject()) {
                    if (institutionManager.isPractitionerInCharge() && institutionManager.getPractitionerInChargeEndDate() == null) {
                        names = names + institutionManager.getRegistrant().getFullname() + " - ";
                    }
                }
                if (names.isEmpty()) {
                    return "N/A";
                }
                names = names.substring(0, names.length() - 2);
                return names;
            }

        }));

        Registration registration = registrationService.getCurrentRegistration(model.getObject());
        if (registration != null) {
            add(new Label("registrationDate", registration.getRegistrationDate()));
        } else {
            add(new Label("registrationDate", ""));
        }

        registration = registrationService.getCurrentRegistration(model.getObject());

        if (registration != null) {
            add(new Label("deRegistrationDate", registration.getDeRegistrationDate()));
        } else {
            add(new Label("deRegistrationDate", ""));
        }

        add(new FeedbackPanel("feedback"));
        add(new Link<Institution>("editLink") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionEditPage(model.getObject().getId()));
            }
        });
        add(new Link<Institution>("registerInstitution") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionRegistrationPage(model.getObject().getId(), Boolean.TRUE));
            }
        });
        add(new Link<Institution>("deRegisterInstitution") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionRegistrationPage(model.getObject().getId(), Boolean.FALSE));
            }
        });

        add(new Link<Institution>("addInstitutionOwnerPage") {
            @Override
            public void onClick() {

                setResponsePage(new SearchInstitutionManagerPage(model.getObject().getId()));

            }
        });
        add(new Link<Institution>("addSupervisorInstitutionPage") {
            @Override
            public void onClick() {
                setResponsePage(new SearchInstitutionManagerPage(model.getObject().getId()));
            }
        });

        add(new Link("addPrepayment") {

            @Override
            public void onClick() {
                setResponsePage(new PrepaymentEditPage(model.getObject().getId()));
            }

            @Override
            public boolean isVisible() {
                return generalParametersService.get().isBillingModule();
            }
        });

        add(new Link<Registrant>("invoiceAmountLink") {
            @Override
            public void onClick() {
                setResponsePage(new InvoicePage(model.getObject().getCustomerAccount(), Boolean.TRUE));
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                return enabled;
            }

            @Override
            public boolean isVisible() {
                return generalParametersService.get().isBillingModule();
            }

        });

        add(new BookmarkablePageLink<Void>("returnLink", InstitutionListPage.class));

        add(new RegistrantContactPanelList("registrantContactPanelList", model.getObject()));

        add(new RegistrantAddressPanelList("registrantAddressPanelList", model.getObject()));

        IModel<List<Prepayment>> prepaymentIModel = new LoadableDetachableModel<List<Prepayment>>() {

            @Override
            protected List<Prepayment> load() {
                return prepaymentService.findByInstitution(model.getObject());
            }
        };

        PropertyListView<Prepayment> eachItem = new PropertyListView<Prepayment>("eachItem", prepaymentIModel) {
            @Override
            protected void populateItem(ListItem<Prepayment> item) {
                Link<Prepayment> viewLink = new Link<Prepayment>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new PrepaymentViewPage(getModelObject().getId()));
                    }
                };
                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                item.add(new CustomDateLabel("dateOfPayment"));
                item.add(new CustomDateLabel("dateOfDeposit"));
                item.add(new Label("institution"));
                item.add(new Label("balance"));
                item.add(new Label("amount"));
            }

            @Override
            public boolean isVisible() {
                return generalParametersService.get().isBillingModule();
            }
        };

        IModel<List<PaymentDetails>> paymentsModel = new LoadableDetachableModel<List<PaymentDetails>>() {
            @Override
            protected List<PaymentDetails> load() {
                return paymentDetailsService.transactionHistory(model.getObject().getCustomerAccount());
            }
        };

        ListView<PaymentDetails> eachPaymentItem = new ListView<PaymentDetails>("eachPaymentItem", paymentsModel) {
            @Override
            protected void populateItem(ListItem<PaymentDetails> item) {
                Link<PaymentDetails> viewLink = new Link<PaymentDetails>("transactionDetailHistory", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new TransactionDetailHistory(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.setModel(new CompoundPropertyModel<PaymentDetails>(item.getModel()));
                item.add(viewLink);
                item.setModel(new CompoundPropertyModel<PaymentDetails>(item.getModel()));
                item.add(new Label("paymentMethod"));
                item.add(new CustomDateLabel("dateOfPayment"));
                item.add(new CustomDateLabel("dateOfDeposit"));
                item.add(new Label("receiptHeader.totalAmount"));
                item.add(new Label("receiptHeader.id"));
                item.add(new Label("receiptHeader.carryForward"));
                item.add(new Label("receiptHeader.canceled"));
            }
        };

        add(eachPaymentItem);

        add(eachItem);

        add(new PropertyListView<InstitutionManager>("institutionManagerList", institutionManagerModelList.getObject()) {
            @Override
            protected void populateItem(ListItem<InstitutionManager> item) {
                Link<InstitutionManager> viewLink = new Link<InstitutionManager>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new InstitutionManagerEditPage(getModelObject().getId(), null, null));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(viewLink);
                item.add(new Label("registrant.fullname"));
                item.add(new CustomDateLabel("practitionerInChargeStartDate"));
                item.add(new CustomDateLabel("practitionerInChargeEndDate"));
                item.add(new CustomDateLabel("institutionSupervisorStartDate"));
                item.add(new CustomDateLabel("institutionSupervisorEndDate"));

            }

        });

    }

    private final class LoadableDetachableInstitutionModel extends LoadableDetachableModel<Institution> {

        private Long id;

        public LoadableDetachableInstitutionModel(Long id) {
            this.id = id;
        }

        @Override
        protected Institution load() {
            return institutionService.get(id);
        }
    }
}
