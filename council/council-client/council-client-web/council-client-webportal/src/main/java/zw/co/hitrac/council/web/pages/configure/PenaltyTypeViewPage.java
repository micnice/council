/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.PenaltyType;
import zw.co.hitrac.council.business.service.PenaltyTypeService;

/**
 *
 * @author kelvin
 */
public class PenaltyTypeViewPage extends IAdministerDatabaseBasePage{
 @SpringBean
    private PenaltyTypeService penaltyTypeService;

    public PenaltyTypeViewPage(Long id) {
        CompoundPropertyModel<PenaltyType> model=new CompoundPropertyModel<PenaltyType>(new PenaltyTypeViewPage.LoadableDetachablePenaltyTypeModel(id));
        setDefaultModel(model);
        add(new Link<PenaltyType>("editLink",model){

            @Override
            public void onClick() {
                setResponsePage(new PenaltyTypeEditPage(getModelObject().getId()));
            }
            
        });
        add(new BookmarkablePageLink<Void>("returnLink", PenaltyTypeListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("penaltyType",model.bind("name")));
    }


    private final class LoadableDetachablePenaltyTypeModel extends LoadableDetachableModel<PenaltyType> {

        private Long id;

        public LoadableDetachablePenaltyTypeModel(Long id) {
            this.id = id;
        }

        @Override
        protected PenaltyType load() {

            return penaltyTypeService.get(id);
        }
    }
}
