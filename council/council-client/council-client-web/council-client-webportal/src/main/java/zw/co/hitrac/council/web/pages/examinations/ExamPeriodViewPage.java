package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.examinations.ExamPeriod;
import zw.co.hitrac.council.business.service.examinations.ExamPeriodService;

/**
 *
 * @author Constance Mabaso
 */
public class ExamPeriodViewPage extends IExaminationsPage {
    @SpringBean
    private ExamPeriodService examPeriodService;

    public ExamPeriodViewPage(Long id) {
        CompoundPropertyModel<ExamPeriod> model =
            new CompoundPropertyModel<ExamPeriod>(new LoadableDetachableExamPeriodModel(id));

        setDefaultModel(model);
        add(new Link<ExamPeriod>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new ExamPeriodEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", ExamPeriodListPage.class));
        add(new Label("name"));
    }

    private final class LoadableDetachableExamPeriodModel extends LoadableDetachableModel<ExamPeriod> {
        private Long id;

        public LoadableDetachableExamPeriodModel(Long id) {
            this.id = id;
        }

        @Override
        protected ExamPeriod load() {
            return examPeriodService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
