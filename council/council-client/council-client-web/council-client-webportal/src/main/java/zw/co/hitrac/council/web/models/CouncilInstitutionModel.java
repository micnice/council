package zw.co.hitrac.council.web.models;

import java.util.List;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.utils.HrisComparator;

/**
 *
 * @author Charles Chigoriwa
 */
public class CouncilInstitutionModel extends LoadableDetachableModel<List<Institution>> {

    private final InstitutionService institutionService;
    private HrisComparator hrisComparator = new HrisComparator();

    public CouncilInstitutionModel(InstitutionService institutionService) {
        this.institutionService = institutionService;
    }

    @Override
    protected List<Institution> load() {
        return (List<Institution>) hrisComparator.sort(institutionService.getAll(null, null, Boolean.TRUE));
    }
}
