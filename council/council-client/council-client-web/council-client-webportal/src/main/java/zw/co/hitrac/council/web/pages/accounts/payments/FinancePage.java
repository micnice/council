/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.accounts.payments;

import com.googlecode.wicket.jquery.core.JQueryBehavior;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.TypeOfService;
import zw.co.hitrac.council.business.domain.accounts.*;
import zw.co.hitrac.council.business.domain.reports.ProductSale;
import zw.co.hitrac.council.business.domain.reports.RegistrantBalance;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.accounts.AccountService;
import zw.co.hitrac.council.business.service.accounts.CustomerService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.business.service.accounts.PrepaymentService;
import zw.co.hitrac.council.business.service.accounts.ReceiptHeaderService;
import zw.co.hitrac.council.business.service.accounts.ReceiptItemService;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.reports.ItemDueReport;
import zw.co.hitrac.council.reports.PrepaymentReport;
import zw.co.hitrac.council.reports.ProductSaleCommentReport;
import zw.co.hitrac.council.reports.RegistrantsReport;
import zw.co.hitrac.council.reports.RemovedDebtComponentsReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.accounts.documents.PrepaymentListPage;
import zw.co.hitrac.council.web.pages.accounts.documents.PrepaymentReportPage;
import zw.co.hitrac.council.web.pages.configure.PaymentConfigurationListPage;
import zw.co.hitrac.council.web.pages.reports.CPDPointsDetailedReportPage;
import zw.co.hitrac.council.web.pages.reports.CpdPointsReportPage;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;

/**
 * @author tdhlakama
 */
public class FinancePage extends TemplatePage {

    @SpringBean
    private RegistrantService registrantService;
    private TransactionType transactionType;
    private HrisComparator hrisComparator = new HrisComparator();
    @SpringBean
    private DebtComponentService debtComponentService;
    @SpringBean
    private CustomerService customerService;
    @SpringBean
    private ReceiptHeaderService receiptHeaderService;
    @SpringBean
    private PrepaymentService prepaymentService;
    @SpringBean
    private ReceiptItemService receiptItemService;
    @SpringBean
    private AccountService accountService;

    public FinancePage() {

        this.add(new JQueryBehavior("#tabs", "tabs"));
        add(new BookmarkablePageLink<Void>("prepaymentList", PrepaymentListPage.class));
        add(new BookmarkablePageLink<Void>("searchPaymentPage", SearchPaymentPage.class));
        add(new BookmarkablePageLink<Void>("searchCanceledPaymentPage", SearchCanceledPaymentPage.class));
        add(new BookmarkablePageLink<Void>("productSalePage", ProductSalePage.class));
        add(new BookmarkablePageLink<Void>("saleByBankPage", SaleByBankPage.class));
        add(new BookmarkablePageLink<Void>("saleByBankDepositPage", SaleByBankDepositPage.class));
        add(new BookmarkablePageLink<Void>("productSaleByBankDepositPage", ProductSaleByBankDepositPage.class));
        add(new BookmarkablePageLink<Void>("itemPaymentPage", ItemPaymentPage.class));
        add(new BookmarkablePageLink<Void>("searchItemPaymentPage", SearchItemPaymentPage.class));
        add(new BookmarkablePageLink<Void>("productSaleByBankPage", ProductSaleByBankPage.class));
        add(new BookmarkablePageLink<Void>("examinationSettingPaymentPage", ExaminationSettingPaymentPage.class));
        add(new BookmarkablePageLink<Void>("examinationPaymentPage", ExaminationPaymentPage.class));
        add(new BookmarkablePageLink<Void>("institutionExamPaperPaymentPage", InstitutionExamPaperPaymentPage.class));
        add(new BookmarkablePageLink<Void>("institutionExamPaymentPage", InstitutionExamPaymentPage.class));
        add(new BookmarkablePageLink<Void>("searchOldReceiptPaymentPage", SearchOldReceiptPaymentPage.class));
        add(new BookmarkablePageLink<Void>("searchOldProductPaymentPage", SearchOldProductPaymentPage.class));
        add(new BookmarkablePageLink<Void>("itemUserPaymentPage", ItemUserPaymentPage.class));
        add(new BookmarkablePageLink<Void>("bankExaminationPage", BankExaminationPage.class));
        add(new BookmarkablePageLink("changeOfBankReportPage", ChangeOfBankReportPage.class));
        add(new BookmarkablePageLink("changeOfPrepaymentInstitutionReportPage", ChangeOfPrepaymentInstitutionReportPage.class));
        add(new BookmarkablePageLink("changeOfBankSummaryReportPage", ChangeOfBankSummaryReportPage.class));
        add(new BookmarkablePageLink("bankReportPage", BankReportPage.class));
        add(new BookmarkablePageLink("systemCarryForwardPage", SystemCarryForwardPage.class));
        add(new BookmarkablePageLink("prepaymentReportPage", PrepaymentReportPage.class));
        add(new BookmarkablePageLink("cPDPointsDetailedReportPage", CPDPointsDetailedReportPage.class));
        add(new BookmarkablePageLink("cpdPointsReportPage", CpdPointsReportPage.class));
        add(new BookmarkablePageLink<Void>("paymentConfigurationLink", PaymentConfigurationListPage.class));

        add(new Link("removedDebtComponents") {
            // Report for all removed items with commets
            @Override
            public void onClick() {
                try {
                    Map parameters = new HashMap();
                    // Report for all removed items with commets
                    RemovedDebtComponentsReport removedDebtComponentsReport = new RemovedDebtComponentsReport();
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(removedDebtComponentsReport, contentType, debtComponentService.getRemovedDeptComponents(), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);
                    resource.respond(a);
                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }

            }
        });

        add(new Link("debtors") {
            @Override
            public void onClick() {
                Map parameters = new HashMap();
                ItemDueReport itemDueReport = new ItemDueReport();
                parameters.put("comment", "Debts ");
                List<ProductSale> productSales = new ArrayList<ProductSale>();
                for (DebtComponent dc : debtComponentService.getUnPaidDeptComponets()) {
                    Registrant registrant = customerService.getRegistrant(dc.getAccount());
                    ProductSale productSale = new ProductSale();
                    productSale.setDebtComponent(dc);
                    productSale.setRegistrant(registrant);
                    productSales.add(productSale);
                }

                try {
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(itemDueReport, contentType, hrisComparator.sortProductSaleByRegistrantFullName(productSales), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);
                    resource.respond(a);
                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

        add(new Link("penalty") {
            @Override
            public void onClick() {
                Map parameters = new HashMap();
                ItemDueReport itemDueReport = new ItemDueReport();
                parameters.put("comment", "Penalty");
                List<ProductSale> productSales = new ArrayList<ProductSale>();
                for (DebtComponent dc : debtComponentService.getPenaltyDeptComponets()) {
                    Registrant registrant = customerService.getRegistrant(dc.getAccount());
                    ProductSale productSale = new ProductSale();
                    productSale.setDebtComponent(dc);
                    productSale.setRegistrant(registrant);
                    productSales.add(productSale);
                }

                try {
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(itemDueReport, contentType, hrisComparator.sortProductSaleByRegistrantFullName(productSales), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);
                    resource.respond(a);
                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

        add(new Link("incompleteRenewals") {
            @Override
            public void onClick() {
                Map parameters = new HashMap();
                ItemDueReport itemDueReport = new ItemDueReport();
                parameters.put("comment", "System Account Errors - In complete Renewals");
                List<ProductSale> productSales = new ArrayList<ProductSale>();
                for (DebtComponent dc : debtComponentService.getUnPaidDeptComponets()) {
                    if (dc.getProduct().getTypeOfService().equals(TypeOfService.ANNUAL_FEES)) {
                        Registrant registrant = customerService.getRegistrant(dc.getAccount());
                        if (registrant != null) {
                            ProductSale productSale = new ProductSale();
                            productSale.setDebtComponent(dc);
                            productSale.setRegistrant(registrant);
                            productSales.add(productSale);
                        }
                    }
                }

                try {
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(itemDueReport, contentType, hrisComparator.sortProductSaleByRegistrantFullName(productSales), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);
                    resource.respond(a);
                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                return enabled;
            }

        });

        add(new Link("systemAccountErrors") {
            @Override
            public void onClick() {
                Map parameters = new HashMap();
                ItemDueReport itemDueReport = new ItemDueReport();
                parameters.put("comment", "System Account Errors ");
                List<ProductSale> productSales = new ArrayList<ProductSale>();
                for (DebtComponent dc : debtComponentService.getUnPaidDeptComponets()) {
                    if (!dc.getProduct().getTypeOfService().equals(TypeOfService.PENALTY_FEES)) {
                        Registrant registrant = customerService.getRegistrant(dc.getAccount());
                        if (registrant != null) {
                            ProductSale productSale = new ProductSale();
                            productSale.setDebtComponent(dc);
                            productSale.setRegistrant(registrant);
                            productSales.add(productSale);
                        }
                    }
                }

                try {
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(itemDueReport, contentType, hrisComparator.sortProductSaleByRegistrantFullName(productSales), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);
                    resource.respond(a);
                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }

        });

        add(new Link("commentReceipts") {
            @Override
            public void onClick() {
                ProductSaleCommentReport productSaleBankReport = new ProductSaleCommentReport();
                List<ProductSale> productSales = new ArrayList<ProductSale>();

                List<ReceiptHeader> receiptHeaders = receiptHeaderService.getReceiptListComments();
                for (ReceiptHeader r : receiptHeaders) {
                    ProductSale productSale = new ProductSale();
                    productSale.setPaymentDetail(r.getPaymentDetails());
                    productSale.setAmountPaid(r.getTotalAmountPaid());
                    productSale.setCarryForward(r.getCarryForward());
                    productSale.setComment(r.getComment());
                    //If properly configured depositDateRequired - reflects a bank transcation
                    final Registrant registrant = customerService.getRegistrant(r.getPaymentDetails().getCustomer().getAccount());
                    if (registrant != null) {
                        productSale.setRegistrant(registrant);
                    }
                    if (registrant == null) {
                        Institution institution = customerService.getInstitution(r.getPaymentDetails().getCustomer().getAccount());
                        productSale.setInstitution(institution);
                    }
                    productSales.add(productSale);
                }

                Map parameters = new HashMap();
                parameters.put("comment", "Receipts with Comments");
                try {
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(productSaleBankReport, contentType, hrisComparator.sortProductSaleByRegistrantFullName(productSales), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);
                    resource.respond(a);
                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

        add(new Link("prepaymentRemaining") {
            @Override
            public void onClick() {
                try {
                    List<RegistrantBalance> finalbalances = new ArrayList<RegistrantBalance>();
                    List<Prepayment> prepayments = prepaymentService.findByCanceled(Boolean.FALSE);
                    for (Prepayment pre : prepayments) {
                        if (pre.getBalance().compareTo(new BigDecimal("0")) == -1) {
                            RegistrantBalance rb = new RegistrantBalance();
                            rb.setInstitution(pre.getInstitution());
                            rb.setDateOfDeposit(pre.getDateOfDeposit());
                            rb.setDateOfPayment(pre.getDateOfPayment());
                            rb.setInitialBalance(pre.getAmount());
                            rb.setCustomerAccount(pre.getInstitution().getCustomerAccount());
                            rb.setCurrentBalance(pre.getBalance());
                            rb.setAmountUsed(pre.getAmountUsed());
                            finalbalances.add(rb);
                        }
                    }

                    PrepaymentReport prepaymentReport = new PrepaymentReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();
                    parameters.put("comment", "Prepayments Remaining as of " + DateUtil.convertDateToString(new Date()));

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(prepaymentReport,
                            contentType, (List<RegistrantBalance>) HrisComparator.sortInstitutionBalance(new ArrayList<RegistrantBalance>(finalbalances)), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);
                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        add(new Link("printAllPrepayments") {
            @Override
            public void onClick() {
                try {
                    List<RegistrantBalance> finalbalances = new ArrayList<RegistrantBalance>();
                    List<Prepayment> prepayments = prepaymentService.findByCanceled(Boolean.FALSE);
                    for (Prepayment pre : prepayments) {
                        if ((pre.getBalance().compareTo(new BigDecimal("0")) == 0) || (pre.getBalance().compareTo(new BigDecimal("0")) == -1)) {
                            RegistrantBalance rb = new RegistrantBalance();
                            rb.setInstitution(pre.getInstitution());
                            rb.setDateOfDeposit(pre.getDateOfDeposit());
                            rb.setDateOfPayment(pre.getDateOfPayment());
                            rb.setInitialBalance(pre.getAmount());
                            rb.setCustomerAccount(pre.getInstitution().getCustomerAccount());
                            rb.setCurrentBalance(pre.getBalance());
                            rb.setAmountUsed(pre.getAmountUsed());
                            finalbalances.add(rb);
                        }
                    }

                    PrepaymentReport prepaymentReport = new PrepaymentReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();
                    parameters.put("comment", "All Prepayment Listing as of " + DateUtil.convertDateToString(new Date()));

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(prepaymentReport,
                            contentType, (List<RegistrantBalance>) HrisComparator.sortInstitutionBalance(new ArrayList<RegistrantBalance>(finalbalances)), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);
                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        add(new Link("printAllCanceledPrepayments") {
            @Override
            public void onClick() {
                try {
                    List<RegistrantBalance> finalbalances = new ArrayList<RegistrantBalance>();
                    List<Prepayment> prepayments = prepaymentService.findByCanceled(Boolean.TRUE);
                    for (Prepayment pre : prepayments) {
                        RegistrantBalance rb = new RegistrantBalance();
                        rb.setInstitution(pre.getInstitution());
                        rb.setDateOfDeposit(pre.getDateOfDeposit());
                        rb.setDateOfPayment(pre.getDateOfPayment());
                        rb.setInitialBalance(pre.getAmount());
                        rb.setCustomerAccount(pre.getInstitution().getCustomerAccount());
                        rb.setCurrentBalance(pre.getBalance());
                        rb.setAmountUsed(pre.getAmountUsed());
                        finalbalances.add(rb);
                    }

                    PrepaymentReport prepaymentReport = new PrepaymentReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();
                    parameters.put("comment", "Prepayments Canceled as of " + DateUtil.convertDateToString(new Date()));

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(prepaymentReport,
                            contentType, (List<RegistrantBalance>) HrisComparator.sortInstitutionBalance(new ArrayList<RegistrantBalance>(finalbalances)), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);
                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });
        add(new Link("viewErrors") {
            @Override
            public void onClick() {
                List<Registrant> registrants = new ArrayList<Registrant>();
                for (Account account : receiptItemService.getReceiptItemsWithNoReceiptHeaders()) {
                    registrants.add(registrantService.getRegistrantByAccountData(account));
                }
                try {
                    Map parameters = new HashMap();
                    ContentType contentType = ContentType.PDF;
                    RegistrantsReport registrantDataReport = new RegistrantsReport();
                    parameters.put("comment", "Accounts with Problems Please Click on Accounts and verify Accounts");
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(registrantDataReport, contentType, registrants, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);
                    resource.respond(a);
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

        add(new Link("correctReceiptItems") {
            @Override
            public void onClick() {
                receiptItemService.correctReceiptItems();
            }
        });

        add(new Link("updateAccounts") {

            @Override
            public void onClick() {
                Set<RegistrantBalance> balances = new HashSet<RegistrantBalance>();
                Set<RegistrantBalance> finalbalances = new HashSet<RegistrantBalance>();
                List<ReceiptHeader> receiptHeaders = receiptHeaderService.getRegistrantCarryForwardCollected(new Date(), transactionType);

                for (ReceiptHeader r : receiptHeaders) {
                    RegistrantBalance rb = new RegistrantBalance();
                    Registrant registrant = customerService.getRegistrant(r.getPaymentDetails().getCustomer().getAccount());
                    if (registrant != null) {
                        rb.setCustomerAccount(registrant.getCustomerAccount());
                        rb.setRegistrant(registrant);
                        rb.setCurrentBalance(accountService.get(rb.getRegistrant().getCustomerAccount().getAccount().getId()).getBalance());
                        balances.add(rb);
                    }
                }

                for (RegistrantBalance rb : balances) {
                    rb.setAmountUsed(receiptHeaderService.getCarryForwardBalance(new Date(), Boolean.TRUE, rb.getRegistrant().getCustomerAccount(), transactionType));
                    rb.setCarryforward(receiptHeaderService.getCarryForwardBalance(new Date(), Boolean.FALSE, rb.getRegistrant().getCustomerAccount(), transactionType));
                    if (rb.getBalanceRemaining().doubleValue() != new Double("0.00")) {
                        finalbalances.add(rb);
                    }

                }

                for (RegistrantBalance balance : finalbalances) {
                    Account account = accountService.get(balance.getRegistrant().getCustomerAccount().getAccount().getId());
                    if (balance.getBalanceRemaining() != null) {
                        account.setBalance(new BigDecimal("-1").multiply(balance.getBalanceRemaining()));
                    }
                    accountService.save(account);
                }

                List<Account> accounts = accountService.getPersonalAccountsErrors();
                for (Account account : accounts) {
                    account.setBalance(new BigDecimal("0"));
                    accountService.save(account);
                }

            }
        });

        add(new Link("provisionBadDebts") {

            @Override
            public void onClick() {

                setResponsePage(new ProvisionBadDebtsViewPage(getPageReference()));
            }
        });
        add(new Link("incomeExpected") {

            @Override
            public void onClick() {

                setResponsePage(new IncomeExpectedPage(getPageReference()));
            }
        });

    }
}
