/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.accounts.payments;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.domain.accounts.ReceiptHeader;
import zw.co.hitrac.council.business.domain.accounts.TransactionType;
import zw.co.hitrac.council.business.domain.reports.ProductSale;
import zw.co.hitrac.council.business.service.accounts.CustomerService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.business.service.accounts.PaymentDetailsService;
import zw.co.hitrac.council.business.service.accounts.PaymentMethodService;
import zw.co.hitrac.council.business.service.accounts.ProductService;
import zw.co.hitrac.council.business.service.accounts.ReceiptHeaderService;
import zw.co.hitrac.council.business.service.accounts.TransactionTypeService;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.reports.ChangeOfBankReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.BankAccountsModel;
import zw.co.hitrac.council.web.pages.accounts.IAccountingPage;
import zw.co.hitrac.council.web.utility.DatePickerUtil;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 *
 * @author tdhlakama
 */
public class ChangeOfBankReportPage extends IAccountingPage {

    private Date startDate, endDate;
    private Product product;
    private TransactionType transactionType;
    @SpringBean
    ProductService productService;
    @SpringBean
    ReceiptHeaderService receiptHeaderService;
    @SpringBean
    DebtComponentService debtComponentService;
    @SpringBean
    private CustomerService customerService;
    @SpringBean
    PaymentDetailsService paymentDetailsService;
    @SpringBean
    PaymentMethodService paymentMethodService;
    @SpringBean
    TransactionTypeService transactionTypeService;
    private HrisComparator hrisComparator = new HrisComparator();

    public ChangeOfBankReportPage() {
        PropertyModel<Date> startDateModel = new PropertyModel<Date>(this, "startDate");
        PropertyModel<Date> endDateModel = new PropertyModel<Date>(this, "endDate");
        PropertyModel<TransactionType> transactionTypeModel = new PropertyModel<TransactionType>(this, "transactionType");

        Form<?> form = new Form("form");
        form.add(new TextField<Date>("startDate", startDateModel).add(DatePickerUtil.getDatePicker()));
        form.add(new TextField<Date>("endDate", endDateModel).add(DatePickerUtil.getDatePicker()));
        form.add(new DropDownChoice("transactionType", transactionTypeModel, new BankAccountsModel(transactionTypeService)).setRequired(true).add(new ErrorBehavior()));
        add(form);
        add(new FeedbackPanel("feedback"));
        form.add(new Button("print") {
            @Override
            public void onSubmit() {
                try {
                    ChangeOfBankReport changeOfBankReport = new ChangeOfBankReport();
                    List<ProductSale> productSales = new ArrayList<ProductSale>();
                    String selectedDates = "";
                    if (startDate != null && endDate == null) {
                        endDate = new Date();
                    }
                    if (startDate != null) {
                        selectedDates = "Dates From" + DateUtil.convertDateToString(startDate) + " to " + DateUtil.convertDateToString(endDate) + ". Printed On " + DateUtil.convertDateToString(new Date());
                    }
                    List<ReceiptHeader> receiptHeaders = receiptHeaderService.getChangedBanksList(transactionType, startDate, endDate);
                    for (ReceiptHeader r : receiptHeaders) {
                        ProductSale productSale = new ProductSale();
                        productSale.setPaymentDetail(r.getPaymentDetails());
                        productSale.setAmountPaid(r.getTotalAmountPaid());
                        productSale.setCarryForward(r.getCarryForward());
                        productSale.setComment(r.getComment());
                        //If properly configured depositDateRequired - reflects a bank transcation
                        final Registrant registrant = customerService.getRegistrant(r.getPaymentDetails().getCustomer().getAccount());
                        if (registrant != null) {
                            productSale.setRegistrant(registrant);
                        } else {
                            Institution institution = customerService.getInstitution(r.getPaymentDetails().getCustomer().getAccount());
                            productSale.setInstitution(institution);
                        }
                        productSales.add(productSale);
                    }

                    Map parameters = new HashMap();
                    parameters.put("comment", "Bank Changes Done on " + transactionType + " " + selectedDates);
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource;

                    resource = ReportResourceUtils.getReportResource(changeOfBankReport, contentType, hrisComparator.sortProductSaleByRegistrantFullName(productSales), parameters);

                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);
                    resource.respond(a);
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    Logger.getLogger(ChangeOfBankReportPage.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

}
