package zw.co.hitrac.council.business.dao;

import java.util.List;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.InstitutionPracticeControl;

/**
 *
 * @author Constance Mabaso
 */
public interface InstitutionPracticeControlDAO extends IGenericDAO<InstitutionPracticeControl>{
    public List<InstitutionPracticeControl> get(Application application);
}
