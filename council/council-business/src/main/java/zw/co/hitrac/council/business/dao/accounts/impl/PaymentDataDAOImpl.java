package zw.co.hitrac.council.business.dao.accounts.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.accounts.PaymentDataDAO;
import zw.co.hitrac.council.business.dao.repo.accounts.PaymentDataRepository;
import zw.co.hitrac.council.business.domain.accounts.PaymentData;

/**
 *s
 * @author Michael Matiashe
 */
@Repository
public class PaymentDataDAOImpl implements PaymentDataDAO {

    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private PaymentDataRepository paymentDataRepository;

    public PaymentData save(PaymentData t) {
        return paymentDataRepository.save(t);
    }

    public List<PaymentData> findAll() {
        return paymentDataRepository.findAll();
    }

    public PaymentData get(Long id) {
        return paymentDataRepository.findOne(id);
    }

    public PaymentDataRepository getPaymentDataRepository() {
        return paymentDataRepository;
    }

    public void setPaymentDataRepository(PaymentDataRepository paymentDataRepository) {
        this.paymentDataRepository = paymentDataRepository;
    }

    public List<PaymentData> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
