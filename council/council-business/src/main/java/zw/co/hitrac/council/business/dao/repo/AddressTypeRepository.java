package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.AddressType;

/**
 *
 * @author Charles Chigoriwa
 */
public interface AddressTypeRepository extends CrudRepository<AddressType, Long> {
    public List<AddressType> findAll();
	public AddressType findByName(String name);
}
