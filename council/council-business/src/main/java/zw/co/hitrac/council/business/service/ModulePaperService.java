package zw.co.hitrac.council.business.service;

import java.util.List;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.ModulePaper;

/**
 *
 * @author Charles Chigoriwa
 */
public interface ModulePaperService extends IGenericService<ModulePaper> {
        public List<ModulePaper> findPapersInCourse(Course course);
}
