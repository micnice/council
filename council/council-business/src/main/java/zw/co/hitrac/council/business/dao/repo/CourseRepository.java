package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.Course;

/**
 *
 * @author Charles Chigoriwa
 */
public interface CourseRepository extends CrudRepository<Course, Long> {

    public List<Course> findAll();

	public Course findByPrefixName(String prefixName);

}
