package zw.co.hitrac.council.business.domain;

/**
 *
 * @author charlesc
 * 
 */
public enum ProductIssuanceType {

    REGISTRATION_CERTIFICATE("Registration Certificate"),
    PROVISIONAL_REGISTRATION_CERTIFICATE("Provisional Registration Certificate"),
    PRACTICING_CERTIFICATE("Practising Certificate"),
    SPECIAL_PRACTICING_CERTIFICATE("Special Practising Certificate"),
    SPECIALIST_REGISTRATION_CERTIFICATE("Specialist Registration Certificate"),
    MEDICAL_LABS_REGISTRATION_CERTIFICATE("Medical Labs Nurse Registration Certificate"),
    QUALIFICATION_CERTIFICATE("Qualification Certificate"),
    CERTIFICATE_OF_GOOD_STANDING("Certification of Good Standing (CGS)"),
    GROUP_ONE_QUALIFICATION_CERTIFICATE("Group One Qualification Certificate"),
    GROUP_TWO_QUALIFICATION_CERTIFICATE("Group Two Qualification Certificate"),
    GROUP_THREE_QUALIFICATION_CERTIFICATE("Group Three Qualification Certificate"),
    GROUP_FOUR_QUALIFICATION_CERTIFICATE("Group Four Qualification Certificate"),
    GROUP_FIVE_QUALIFICATION_CERTIFICATE("Group Five Qualification Certificate"),
    GROUP_SIX_QUALIFICATION_CERTIFICATE("Group Six Qualification Certificate"),
    GROUP_SEVEN_QUALIFICATION_CERTIFICATE("Group Seven Qualification Certificate"),
    GROUP_EIGHT_QUALIFICATION_CERTIFICATE("Group Eight Qualification Certificate"),
    GROUP_NINE_QUALIFICATION_CERTIFICATE("Group Nine Qualification Certificate"),
    GROUP_TEN_QUALIFICATION_CERTIFICATE("Group Ten Qualification Certificate"),
    PCN_REGISTRATION_CERTIFICATE("PCN Registration Certificate"),
    UNRESTRICTED_PRACTICING_CERTIFICATE("unrestricted_practicing_certificate"),
    ADDITIONAL_QUALIFICATION_CERTIFICATE("Additional Qualification Certificate"),
   QUARTERLY_RENEWAL_CERTIFICATE("Quaterly renewal Certificate");

    private ProductIssuanceType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    final private String name;


}

