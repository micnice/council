/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.repo.accounts;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.accounts.Bank;

/**
 *
 * @author tidza
 */
public interface BankRepository extends CrudRepository<Bank, Long> {

    public List<Bank> findAll();
}
