package zw.co.hitrac.council.reports.utils;

//~--- non-JDK imports --------------------------------------------------------

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.Map;

//~--- JDK imports ------------------------------------------------------------

/**
 * @author Charles Chigoriwa
 */
public interface IReportManager extends Serializable {

    public JasperReport getJasperReport(String reportPath) throws JRException;

    public byte[] getPdfReportBytes(Map<String, Object> parameters, String reportPath, Collection<?> beanCollection)
            throws JRException;

    public byte[] getPdfReportBytes(Map<String, Object> parameters, String reportPath, Connection connection)
            throws JRException;

    public byte[] getReportHTML(Map<String, Object> parameters, String reportPath, Collection<?> beanCollection)
            throws JRException;

    public byte[] getReportHTML(Map<String, Object> parameters, String reportPath, Connection connection)
            throws JRException;

    public byte[] getReportXls(Map<String, Object> parameters, String reportPath, Collection<?> beanCollection)
            throws JRException;

    public byte[] getReportXls(Map<String, Object> parameters, String reportPath, Connection connection)
            throws JRException;

    public byte[] getReportDoc(Map<String, Object> parameters, String reportPath, Collection<?> beanCollection)
            throws JRException;

    public byte[] getReportDoc(Map<String, Object> parameters, String reportPath, Connection connection)
            throws JRException;

    public void printJasperPrint(String printerName, Map<String, Object> parameters, String reportPath, Collection<?> beanCollection)
            throws JRException;

    public void printJasperPrint(String printerName, Map<String, Object> parameters, String reportPath, Connection connection)
            throws JRException;

    public JasperPrint getJasperPrint(Map<String, Object> parameters, String reportPath, Collection<?> beanCollection)
            throws JRException;

    public JasperPrint getJasperPrint(Map<String, Object> parameters, String reportPath, Connection connection)
            throws JRException;

    public JasperPrint getJasperPrintAfterCompile(Map<String, Object> parameters, String reportPath,
                                                  Collection<?> beanCollection)
            throws JRException;

    public JasperPrint getJasperPrintAfterCompile(Map<String, Object> parameters, String reportPath,
                                                  Connection connection)
            throws JRException;
}

//~ Formatted by Jindent --- http://www.jindent.com
