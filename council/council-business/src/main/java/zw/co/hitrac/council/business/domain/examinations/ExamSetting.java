/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain.examinations;

import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.domain.BaseIdEntity;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.ModulePaper;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 *
 * @author tdhlakama
 */
@Entity
@Table(name="examsetting")
@Audited
public class ExamSetting extends BaseIdEntity implements Serializable {

    private Date startDate;
    private Date endDate;
    private Date markingStartDate;
    private Date markingEndDate;
    private ExamYear examYear;
    private ExamPeriod examPeriod;
    private Set<Exam> exams = new HashSet<Exam>();
    private Boolean active = Boolean.FALSE;
  
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getMarkingStartDate() {
        return markingStartDate;
    }

    public void setMarkingStartDate(Date markingStartDate) {
        this.markingStartDate = markingStartDate;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getMarkingEndDate() {
        return markingEndDate;
    }

    public void setMarkingEndDate(Date markingEndDate) {
        this.markingEndDate = markingEndDate;
    }

    @OneToMany(mappedBy = "examSetting")
    public Set<Exam> getExams() {
        return exams;
    }

    public void setExams(Set<Exam> exams) {
        this.exams = exams;
    }

    @ManyToOne
    public ExamYear getExamYear() {
        return examYear;
    }

    public void setExamYear(ExamYear examYear) {
        this.examYear = examYear;
    }

    @ManyToOne
    public ExamPeriod getExamPeriod() {
        return examPeriod;
    }

    public void setExamPeriod(ExamPeriod examPeriod) {
        this.examPeriod = examPeriod;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return examPeriod + " " + examYear;
    }

    @Transient
    public String getActivePeriod() {
        if (active == null) {
            return "";
        }
        return active ? "YES" : "NO";
    }

    @Transient
    public List<Exam> getExamList() {
        List<Exam> items = new ArrayList<Exam>();
        if (getExams() != null && !getExams().isEmpty()) {
            items.addAll(getExams());
        }
        return items;
    }

    @Transient
    public List<ModulePaper> getExamPapers() {
        Set<ModulePaper> items = new HashSet<ModulePaper>();
        for (Exam e : getExamList()) {
            items.add(e.getModulePaper());
        }
        return new ArrayList<ModulePaper>(items);
    }

    @Transient
    public List<Course> getlistOfCourseSet() {
        Set<Course> courses = new HashSet<Course>();
        for (Exam e : getExamList()) {
            courses.add(e.getModulePaper().getCourse());
        }
        return new ArrayList<Course>(courses);
    }
}
