package zw.co.hitrac.council.business.dao.repo;

import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.Citizenship;
import zw.co.hitrac.council.business.domain.Registrant;

import java.util.List;

/**
 *
 * @author Takunda Dhlakama
 * @author Michael Matiashe
 */
public interface RegistrantRepository extends CrudRepository<Registrant, Long> {

    public List<Registrant> findAll();

    public List<Registrant> findByCitizenship(Citizenship citizenship);

    public Registrant findByIdNumber(String idNumber);
}
