/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.examinations.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.examinations.ExamYearDao;
import zw.co.hitrac.council.business.domain.examinations.ExamYear;
import zw.co.hitrac.council.business.service.examinations.ExamYearService;

import java.util.List;

/**
 *
 * @author tidza
 */
@Service
@Transactional
public class ExamYearServiceImpl implements ExamYearService {

    @Autowired
    private ExamYearDao examYearDao;

    @Transactional
    public ExamYear save(ExamYear t) {
        return examYearDao.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ExamYear> findAll() {
        return examYearDao.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public ExamYear get(Long id) {
        return examYearDao.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ExamYear> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setExamYearDao(ExamYearDao examYearDao) {

        this.examYearDao = examYearDao;
    }
    
}
