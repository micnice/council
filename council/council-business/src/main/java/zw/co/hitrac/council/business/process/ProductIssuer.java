/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.process;

import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;

import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.domain.examinations.Exam;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.business.service.CourseCertificateNumberService;
import zw.co.hitrac.council.business.service.DurationService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.ModulePaperService;
import zw.co.hitrac.council.business.service.ProductIssuanceService;
import zw.co.hitrac.council.business.service.RegistrantActivityService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationService;
import zw.co.hitrac.council.business.service.examinations.ExamService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.business.utils.DateUtil;

/**
 * @author micnice
 */
@Service
public class ProductIssuer {

    @Autowired
    RegistrantService registrantService;
    @Autowired
    private RegistrationService registrationService;
    @Autowired
    private ProductIssuanceService productIssuanceService;
    @Autowired
    private DurationService durationService;
    @Autowired
    private RegistrationProcess registrationProcess;
    @Autowired
    private RegistrantActivityService registrantActivityService;
    @Autowired
    private GeneralParametersService generalParametersService;
    @Autowired
    private ExamRegistrationService examRegistrationService;
    @Autowired
    private ExamService examService;
    @Autowired
    private ModulePaperService modulePaperService;
    @Autowired
    private ApplicationProcess applicationProcess;
    @Autowired
    private CourseCertificateNumberService courseCertificateNumberService;

    @Transactional
    public void issueProducts(Registration registration, DebtComponent transactionDebtComponent, boolean issueQualificationCertificate, Course course) {

        //Check Course Product Issuance Type
        if (course.getProductIssuanceType() == null) {
            throw new CouncilException("Certificate Type not Set for the Course");
        }
        //Issue Registration Certificate,  //Issue PCN Registration Certificate
        if (course.getProductIssuanceType().equals(ProductIssuanceType.REGISTRATION_CERTIFICATE)
                || course.getProductIssuanceType().equals(ProductIssuanceType.PCN_REGISTRATION_CERTIFICATE)
                || course.getProductIssuanceType().equals(ProductIssuanceType.PROVISIONAL_REGISTRATION_CERTIFICATE)
                || course.getProductIssuanceType().equals(ProductIssuanceType.ADDITIONAL_QUALIFICATION_CERTIFICATE)) {
            issueRegistrationOrAdditionalQualficationCertificate(registration, transactionDebtComponent);
            System.out.println("REGISTRATION_CERTIFICATE");
        }

        //Issue Additional Qualification Certificate
        if (course.getProductIssuanceType().equals(ProductIssuanceType.ADDITIONAL_QUALIFICATION_CERTIFICATE)) {
            issueProductAdditionalQualificationCertificate(registration, transactionDebtComponent);
        }

        int random = 0;
        int random1 = 0;
        ProductIssuance productIssuance = new ProductIssuance();

        if (issueQualificationCertificate) {
            if (course.getCourseGroup() == null) {
                throw new CouncilException("Course Group not set");
            }

            ExamRegistration examRegistration = null;
            registrationloop:
            for (Registration r : registrationService.getRegistrationsAll(course, generalParametersService.get().getStudentRegister(), registration.getRegistrant())) {
                for (ExamRegistration e : examRegistrationService.getExamRegistrations(r)) {
                    if (course.equals(e.getRegistration().getCourse())) {
                        if (e.getClosed() && e.getCompletedStatus() && e.getPassStatus()) {
                            examRegistration = e;
                            break registrationloop;
                        }
                    }
                }
            }

            String additionalQualification = "";
            String statement = "";
            if (examRegistration != null) {
                Set<ModulePaper> modulePapers = new HashSet<>();

                //filter module papers for course and exam settings
                for (ModulePaper modulePaper : examService.findExamPapers(examRegistration.getExamSetting())) {
                    if (modulePaperService.findPapersInCourse(course).contains(modulePaper)) {
                        modulePapers.add(modulePaper);
                    }
                }

                //filter exams needed settings
                for (Exam exam : examService.findAll(examRegistration.getExamSetting())) {
                    if (modulePapers.contains(exam.getModulePaper())) {
                        statement = statement.concat(exam.getExamDate().getDate() + " / ");
                    }
                    additionalQualification = "" + exam.getExamDate().getYear();
                }
                if (statement == null || statement.length() == 0) {
                    statement = " / ";
                }
                statement = statement.substring(0, statement.length() - 3);
                statement = statement.concat(" " + examRegistration.getExamSetting().toString());
            }

            productIssuance = new ProductIssuance();
            random = new Random().nextInt();
            random = random < 0 ? random * -1 : random;
            random1 = new Random().nextInt();
            random1 = random1 < 0 ? random1 * -1 : random1;
            productIssuance.setProductNumber(String.valueOf(random1));
            productIssuance.setBarCode(random);
            productIssuance.setSerialNumber(random);
            productIssuance.setExpiryDate(null);
            productIssuance.setCreateDate(new Date());
            productIssuance.setRegistrant(registration.getRegistrant());
            productIssuance.setTransactionDebtComponent(transactionDebtComponent);

            switch (course.getCourseGroup()) {
                case GROUP:
                    productIssuance.setProductIssuanceType(ProductIssuanceType.QUALIFICATION_CERTIFICATE);
                    break;
                case GROUP_ONE:
                    productIssuance.setProductIssuanceType(ProductIssuanceType.GROUP_ONE_QUALIFICATION_CERTIFICATE);
                    break;
                case GROUP_TWO:
                    productIssuance.setProductIssuanceType(ProductIssuanceType.GROUP_TWO_QUALIFICATION_CERTIFICATE);
                    break;
                case GROUP_THREE:
                    productIssuance.setProductIssuanceType(ProductIssuanceType.GROUP_THREE_QUALIFICATION_CERTIFICATE);
                    break;
                case GROUP_FOUR:
                    productIssuance.setProductIssuanceType(ProductIssuanceType.GROUP_FOUR_QUALIFICATION_CERTIFICATE);
                    break;
                case GROUP_FIVE:
                    productIssuance.setProductIssuanceType(ProductIssuanceType.GROUP_FIVE_QUALIFICATION_CERTIFICATE);
                    break;
                case GROUP_SIX:
                    productIssuance.setProductIssuanceType(ProductIssuanceType.GROUP_SIX_QUALIFICATION_CERTIFICATE);
                    break;
                case GROUP_SEVEN:
                    productIssuance.setProductIssuanceType(ProductIssuanceType.GROUP_SEVEN_QUALIFICATION_CERTIFICATE);
                    break;
                case GROUP_EIGHT:
                    productIssuance.setProductIssuanceType(ProductIssuanceType.GROUP_EIGHT_QUALIFICATION_CERTIFICATE);
                    break;
                case GROUP_NINE:
                    productIssuance.setProductIssuanceType(ProductIssuanceType.GROUP_NINE_QUALIFICATION_CERTIFICATE);
                    break;
                case GROUP_TEN:
                    productIssuance.setProductIssuanceType(ProductIssuanceType.GROUP_TEN_QUALIFICATION_CERTIFICATE);
                    break;
                default:
                    break;
            }

            productIssuance.setCourse(course);
            productIssuance.setRegister(registration.getRegister());
            productIssuance.setPrefixName(course.getPrefixName());
            if (examRegistration != null) {
                productIssuance.setExamDate(statement);
            } else {
                productIssuance.setExamDate(DateUtil.getDate(new Date()));
            }
            productIssuance.setDiplomaCertificateNumber(generateLastCertificateNumber());
            productIssuanceService.save(productIssuance);
            saveLastCertificateNumber();
        }
    }

    @Transactional
    public void issue_Product_CERTIFICATE_OF_GOOD_STANDING(Application application, DebtComponent transactionDebtComponent) {

        ProductIssuance productIssuance = new ProductIssuance();
        int random = new Random().nextInt();
        random = random < 0 ? random * -1 : random;
        int random1 = new Random().nextInt();
        random1 = random1 < 0 ? random1 * -1 : random1;

        if (generalParametersService.get().getCaptureCPDsDirectlyAndInDirectly()) {
            productIssuance.setProductNumber(getFormattedCGSCertificateNumber());
        } else {
            productIssuance.setProductNumber(String.valueOf(random1));
        }

        Duration duration = null;
        if (registrationService.hasCurrentRegistration(application.getRegistrant())) {
            if (registrationProcess.studentNoneRegistration(application.getRegistrant())) {
                duration = durationService.getCurrentCourseDuration(registrationProcess.activeRegisterNotStudentRegister(application.getRegistrant()).getCourse());
            }
        }
        RegistrantActivity registrantActivity = null;
        if (duration != null) {
            registrantActivity = registrantActivityService.getRegistrantActivityByDuration(application.getRegistrant(), duration);
        }
        if (registrantActivity == null) {
            throw new CouncilException("This individual has no current registration Activity");
        }
        productIssuance.setExpiryDate(registrantActivity.getDuration().getEndDate());
        productIssuance.setCreateDate(new Date());
        productIssuance.setRegistrant(application.getRegistrant());
        productIssuance.setTransactionDebtComponent(transactionDebtComponent);
        productIssuance.setProductIssuanceType(ProductIssuanceType.CERTIFICATE_OF_GOOD_STANDING);
        productIssuance.setCourse(application.getCourse());
        productIssuance.setBarCode(random);
        productIssuance.setSerialNumber(random);
        productIssuance.setDuration(duration);
        productIssuance.setRegister(applicationProcess.getApplicationRegister(application));
        productIssuance.setCgsCertificateNumber(generateLastCGSCertificateNumber());
        productIssuanceService.save(productIssuance);
        saveLastCGSCertificateNumber();
    }

    @Transactional
    public void issue_Product_CERTIFICATE_OF_GOOD_STANDING(Application application, DebtComponent transactionDebtComponent, Duration duration) {

        ProductIssuance productIssuance = new ProductIssuance();
        int random = new Random().nextInt();
        random = random < 0 ? random * -1 : random;
        int random1 = new Random().nextInt();
        random1 = random1 < 0 ? random1 * -1 : random1;
        if (generalParametersService.get().getCaptureCPDsDirectlyAndInDirectly()) {
            productIssuance.setProductNumber(getFormattedCGSCertificateNumber());
        } else {
            productIssuance.setProductNumber(String.valueOf(random1));
        }

        if (duration == null) {
            throw new CouncilException("Duration For This Registration Not Set");
        }
        productIssuance.setExpiryDate(duration.getEndDate());
        productIssuance.setCreateDate(new Date());
        productIssuance.setRegistrant(application.getRegistrant());
        productIssuance.setTransactionDebtComponent(transactionDebtComponent);
        productIssuance.setProductIssuanceType(ProductIssuanceType.CERTIFICATE_OF_GOOD_STANDING);
        productIssuance.setCourse(application.getCourse());
        productIssuance.setBarCode(random);
        productIssuance.setSerialNumber(random);
        productIssuance.setDuration(duration);
        productIssuance.setRegister(applicationProcess.getApplicationRegister(application));
        productIssuance.setCgsCertificateNumber(generateLastCGSCertificateNumber());
        productIssuanceService.save(productIssuance);
        saveLastCGSCertificateNumber();
    }

    @Transactional
    public void issueProductPracticeCertificate(Registration registration, DebtComponent transactionDebtComponent) {
        //Issue Practising certificate after renewal

        ProductIssuance productIssuance = new ProductIssuance();
        int random = new Random().nextInt();
        random = random < 0 ? random * -1 : random;
        int random1 = new Random().nextInt();
        random1 = random1 < 0 ? random1 * -1 : random1;
        Duration duration = durationService.getCurrentCourseDuration(registration.getCourse());
        if (duration == null) {
            throw new CouncilException("Duration For This Registration Not Set");
        }
        productIssuance.setExpiryDate(duration.getEndDate());
        productIssuance.setCreateDate(new Date());
        productIssuance.setCourse(registration.getCourse());
        productIssuance.setRegistrant(registration.getRegistrant());
        productIssuance.setTransactionDebtComponent(transactionDebtComponent);
        productIssuance.setProductIssuanceType(ProductIssuanceType.PRACTICING_CERTIFICATE);
        productIssuance.setBarCode(random);
        productIssuance.setSerialNumber(random);
        productIssuance.setDuration(duration);
        productIssuance.setRegister(registration.getRegister());
        productIssuance.setPrefixName(registration.getCourse().getPrefixName());
        savePracticeCertificate(productIssuance);
    }

    @Transactional
    public void issueProductPracticeCertificate(Registration registration, DebtComponent transactionDebtComponent, Duration duration) {
        //Issue Practising certificate after renewal

        ProductIssuance productIssuance = new ProductIssuance();
        int random = new Random().nextInt();
        random = random < 0 ? random * -1 : random;
        int random1 = new Random().nextInt();
        random1 = random1 < 0 ? random1 * -1 : random1;
        if (duration == null) {
            throw new CouncilException("Duration For This Registration Not Set");
        }
        productIssuance.setExpiryDate(duration.getEndDate());
        productIssuance.setCreateDate(new Date());
        productIssuance.setCourse(registration.getCourse());
        productIssuance.setRegistrant(registration.getRegistrant());
        productIssuance.setTransactionDebtComponent(transactionDebtComponent);
        productIssuance.setProductIssuanceType(ProductIssuanceType.PRACTICING_CERTIFICATE);
        productIssuance.setBarCode(random);
        productIssuance.setSerialNumber(random);
        productIssuance.setDuration(duration);
        productIssuance.setRegister(registration.getRegister());
        productIssuance.setPrefixName(registration.getCourse().getPrefixName());
        savePracticeCertificate(productIssuance);

    }

    @Transactional
    public void savePracticeCertificate(ProductIssuance productIssuance) {
        if (!generalParametersService.get().getCourseCertificateNumber()) {
            productIssuance.setPracticeCertificateNumber(getLastPracticeCertificateNumber());
            productIssuance.setProductNumber(getLastPracticeCertificateNumber());
            productIssuanceService.save(productIssuance);
            updateLastPracticeCertificateNumber();

        } else {
            CourseCertificateNumber courseCertificateNumber = generateAndSetCoursePracticeCertificateNumber(productIssuance);
            productIssuance.setProductNumber(productIssuance.getPracticeCertificateNumber());
            productIssuanceService.save(productIssuance);
            courseCertificateNumberService.save(courseCertificateNumber);
        }

    }

    @Transactional
    public void issueProductAdditionalQualificationCertificate(Registration registration, DebtComponent transactionDebtComponent) {

        //Issue Additional Qaulification Certificate
        if (registration.getCourse().getProductIssuanceType() == null) {
            throw new CouncilException("Certificate Type not Set for the Course");
        }
        ProductIssuance productIssuance = new ProductIssuance();
        int random = new Random().nextInt();
        random = random < 0 ? random * -1 : random;
        int random1 = new Random().nextInt();
        productIssuance.setProductNumber(String.valueOf(random1));
        productIssuance.setSerialNumber(random);
        productIssuance.setBarCode(random);
        productIssuance.setCreateDate(new Date());
        productIssuance.setRegistrant(registration.getRegistrant());
        productIssuance.setTransactionDebtComponent(transactionDebtComponent);
        productIssuance.setProductIssuanceType(ProductIssuanceType.ADDITIONAL_QUALIFICATION_CERTIFICATE);
        productIssuance.setCourse(registration.getCourse());
        productIssuance.setPrefixName(registration.getCourse().getPrefixName());
        productIssuance.setRegister(registration.getRegister());
        productIssuanceService.save(productIssuance);
        saveLastCertificateNumber();
    }

    @Transactional
    public void issueUnrestrictedPractisingCertificate(Registration registration, DebtComponent transactionDebtComponent) {

        //Issue Registration and Practising certificate
        if (registration.getCourse().getProductIssuanceType() == null) {
            throw new CouncilException("Registration Certificate Type not Set for the Course");
        }
        ProductIssuance productIssuance = new ProductIssuance();
        int random = new Random().nextInt();
        random = random < 0 ? random * -1 : random;
        int random1 = new Random().nextInt();
        random1 = random1 < 0 ? random1 * -1 : random1;
        productIssuance.setProductNumber(String.valueOf(random1));
        productIssuance.setBarCode(random);
        productIssuance.setSerialNumber(random);
        productIssuance.setCreateDate(new Date());
        productIssuance.setRegistrant(registration.getRegistrant());
        productIssuance.setTransactionDebtComponent(transactionDebtComponent);
        productIssuance.setProductIssuanceType(ProductIssuanceType.UNRESTRICTED_PRACTICING_CERTIFICATE);
        productIssuance.setCourse(registration.getCourse());
        productIssuance.setPrefixName(registration.getCourse().getPrefixName());
        productIssuance.setRegister(registration.getRegister());
        productIssuance.setDiplomaCertificateNumber(generateLastCertificateNumber());
        productIssuanceService.save(productIssuance);
        updateLastPracticeCertificateNumber();
        System.out.println("issueUnrestrictedPractisingCertificate");
    }

    @Transactional
    public void issueProductSpecialPracticeCertificate(Registration registration, DebtComponent transactionDebtComponent) {
        //Issue Practising certificate after renewal

        ProductIssuance productIssuance = new ProductIssuance();
        int random = new Random().nextInt();
        random = random < 0 ? random * -1 : random;
        int random1 = new Random().nextInt();
        random1 = random1 < 0 ? random1 * -1 : random1;
        productIssuance.setProductNumber(String.valueOf(random1));
        Duration duration = durationService.getCurrentCourseDuration(registration.getCourse());
        if (duration == null) {
            throw new CouncilException("Duration For This Registration Not Set");
        }
        productIssuance.setSerialNumber(random);
        productIssuance.setExpiryDate(duration.getEndDate());
        productIssuance.setCreateDate(new Date());
        productIssuance.setCourse(registration.getCourse());
        productIssuance.setRegistrant(registration.getRegistrant());
        productIssuance.setTransactionDebtComponent(transactionDebtComponent);
        productIssuance.setProductIssuanceType(ProductIssuanceType.SPECIAL_PRACTICING_CERTIFICATE);
        productIssuance.setBarCode(random);
        productIssuance.setDuration(duration);
        productIssuance.setRegister(registration.getRegister());
        productIssuance.setPrefixName(registration.getCourse().getPrefixName());
        productIssuance.setPracticeCertificateNumber(getLastPracticeCertificateNumber());
        productIssuanceService.save(productIssuance);
        updateLastPracticeCertificateNumber();
    }

    @Transactional
    public void issueRegistrationOrAdditionalQualficationCertificate(Registration registration, DebtComponent transactionDebtComponent) {

        if (registration.getCourse().getProductIssuanceType() == null) {
            throw new CouncilException("Certificate Type not Set for the Course");
        }

        final ProductIssuanceType type = registration.getCourse().getProductIssuanceType();
        ProductIssuance productIssuance = new ProductIssuance();
        int random = new Random().nextInt();
        random = random < 0 ? random * -1 : random;
        int random1 = new Random().nextInt();
        productIssuance.setSerialNumber(random);
        random1 = random1 < 0 ? random1 * -1 : random1;
        productIssuance.setCreateDate(new Date());
        productIssuance.setRegistrant(registration.getRegistrant());
        productIssuance.setTransactionDebtComponent(transactionDebtComponent);
        productIssuance.setProductIssuanceType(type);
        productIssuance.setCourse(registration.getCourse());
        productIssuance.setPrefixName(registration.getCourse().getPrefixName());
        productIssuance.setRegister(registration.getRegister());

        if (generalParametersService.get().getRegistrationCertificateSerialIsIncrimental()) {
            productIssuance.setSerialNumber(generateLastRegistrationCertificateSerialNumber());
            productIssuance.setRegistrationCertificateNumber(productIssuance.getSerialNumber().toString());
            productIssuance.setProductNumber(String.valueOf(random));
            productIssuance.setBarCode(generateLastRegistrationCertificateSerialNumber());
            saveLastRegistrationCertificateSerialNumber();
            System.out.println("getRegistrationCertificateSerialIsIncrimental");
        } else {
            productIssuance.setSerialNumber(random);
            productIssuance.setBarCode(random);
            productIssuance.setProductNumber(String.valueOf(random1));
            productIssuance.setRegistrationCertificateNumber(productIssuance.getBarCode().toString());
        }
        if (type == ProductIssuanceType.ADDITIONAL_QUALIFICATION_CERTIFICATE) {
            productIssuance.setSerialNumber(random);
            productIssuance.setBarCode(random);
            productIssuance.setProductNumber(String.valueOf(random1));
            productIssuance.setRegistrationCertificateNumber(productIssuance.getBarCode().toString());
        }

        if (type == ProductIssuanceType.REGISTRATION_CERTIFICATE) {
            productIssuance.setProductIssuanceType(ProductIssuanceType.REGISTRATION_CERTIFICATE);
            Calendar calendar = Calendar.getInstance();
            if (registration.getRegister().equals(generalParametersService.get().getProvisionalRegister())) {
                calendar.add(Calendar.YEAR, 3);
                productIssuance.setExpiryDate(calendar.getTime());
            }
            if (generalParametersService.get().getInternRegister() != null) {
                if (registration.getRegister().equals(generalParametersService.get().getInternRegister())) {
                    calendar.add(Calendar.YEAR, 1);
                    productIssuance.setExpiryDate(calendar.getTime());
                }
            }
        }
        if (type == ProductIssuanceType.PROVISIONAL_REGISTRATION_CERTIFICATE) {

            productIssuance.setProductIssuanceType(ProductIssuanceType.PROVISIONAL_REGISTRATION_CERTIFICATE);
            Calendar calendar = Calendar.getInstance();
            if (registration.getRegister().equals(generalParametersService.get().getProvisionalRegister())) {
                calendar.add(Calendar.YEAR, 3);
                productIssuance.setExpiryDate(calendar.getTime());
            }
            if (generalParametersService.get().getInternRegister() != null) {
                if (registration.getRegister().equals(generalParametersService.get().getInternRegister())) {
                    calendar.add(Calendar.YEAR, 1);
                    productIssuance.setExpiryDate(calendar.getTime());
                }
            }
        }

        if (type == ProductIssuanceType.ADDITIONAL_QUALIFICATION_CERTIFICATE) {
            productIssuance.setProductIssuanceType(ProductIssuanceType.ADDITIONAL_QUALIFICATION_CERTIFICATE);
        }

        productIssuanceService.save(productIssuance);
    }

    public String getLastPracticeCertificateNumber() {

        int lastNumber = generalParametersService.get().getLastPracticeCertificateNumber();
        lastNumber = lastNumber + generalParametersService.get().getIncrementCertificateNumber();
        return String.valueOf(lastNumber);
    }

    public String getFormattedCGSCertificateNumber() {

        //generated value of cgs is left padded with zeros
        int lengthOfCgsCertificateNumber = 4;
        String formattedCGSCertificateNumber = org.apache.commons.lang.StringUtils.leftPad(generateLastCGSCertificateNumber(), lengthOfCgsCertificateNumber, "0");
        return formattedCGSCertificateNumber;
    }

    public String getLastQualificationCertificateNumber() {
        return null;
    }

    @Transactional
    public void updateLastPracticeCertificateNumber() {

        int lastNumber = Integer.valueOf(getLastPracticeCertificateNumber());
        GeneralParameters generalParameters = generalParametersService.get();
        generalParameters.setLastPracticeCertificateNumber(lastNumber);
        generalParametersService.save(generalParameters);
    }

    public String generateLastCertificateNumber() { // diploma
        return getLastPracticeCertificateNumber();
    }

    @Transactional
    public void saveLastCertificateNumber() { // diploma
        updateLastPracticeCertificateNumber();
    }

    public String generateLastCGSCertificateNumber() { // diploma
        int lastNumber = generalParametersService.get().getLastCGSCertificateNumber();
        lastNumber = lastNumber + generalParametersService.get().getIncrementCertificateNumber();
        return String.valueOf(lastNumber);
    }

    @Transactional
    public void saveLastCGSCertificateNumber() { // diploma
        int lastNumber = generalParametersService.get().getLastCGSCertificateNumber();
        GeneralParameters generalParameters = generalParametersService.findAll().get(0);
        lastNumber = lastNumber + generalParametersService.get().getIncrementCertificateNumber();
        generalParameters.setLastCGSCertificateNumber(lastNumber);
        generalParametersService.save(generalParameters);
    }

    public Integer generateLastRegistrationCertificateSerialNumber() { // diploma
        Integer lastNumber = generalParametersService.get().getRegistrationCertificateSerial();
        lastNumber = lastNumber + 1;
        return lastNumber;
    }

    @Transactional
    public void saveLastRegistrationCertificateSerialNumber() { // diploma
        int lastNumber = generalParametersService.get().getRegistrationCertificateSerial();
        GeneralParameters generalParameters = generalParametersService.findAll().get(0);
        lastNumber = lastNumber + 1;
        generalParameters.setRegistrationCertificateSerial(lastNumber);
        generalParametersService.save(generalParameters);
    }

    public CourseCertificateNumber generateAndSetCoursePracticeCertificateNumber(ProductIssuance productIssuance) {

        CourseCertificateNumber courseCertificateNumber = null;
        try {
            courseCertificateNumber = courseCertificateNumberService.get(productIssuance.getCourse());
        } catch (Exception e) {
            throw new CouncilException("Failed to generate course practice certificate number");
        }

        courseCertificateNumber.addOne();

        try {

            if (productIssuance.getDuration() == null) {
                throw new CouncilException("Failed to generate course practice certificate number");
            }

            if (productIssuance.getDuration().getStartDate() == null) {
                throw new CouncilException("Failed to generate course practice certificate number");
            }

            int durationYear = DateUtil.getYearFromDate(productIssuance.getDuration().getStartDate());
            int durationYearPart = durationYear % 100;

            String certificateNumber = String.format("%s.%s/%s", courseCertificateNumber.getCoursePrefix(),
                    courseCertificateNumber.getCertificateNumber(), durationYearPart);

            productIssuance.setPracticeCertificateNumber(certificateNumber);

            return courseCertificateNumber;

        } catch (Exception e) {

            throw new CouncilException(e.getMessage());
        }

    }

}
