package zw.co.hitrac.council.web.pages.reports;

//~--- non-JDK imports --------------------------------------------------------
import java.util.Arrays;
import net.sf.jasperreports.engine.JRException;

import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.RegisterService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.reports.RegistrationReport;
import zw.co.hitrac.council.reports.utils.ContentType;

//~--- JDK imports ------------------------------------------------------------

import java.util.HashMap;
import java.util.List;
import org.apache.wicket.model.PropertyModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.web.utility.JFreeChartImage;

/**
 *
 * @author Matiashe
 */
public class RegistrantReportPage extends IReportPage {

    @SpringBean
    private RegistrationService registrationService;
    @SpringBean
    private RegisterService registerService;
    @SpringBean
    private CourseService courseService;
    private ContentType contentType;

    public RegistrantReportPage() {
        PropertyModel<ContentType> contentTypeModel = new PropertyModel<ContentType>(this, "contentType");
        Form<Registration> form = new Form<Registration>("form2",
                new CompoundPropertyModel<Registration>(new Registration())) {
            @Override
            protected void onSubmit() {
                try {
                    Registration registration = this.getModelObject();
                    List<Registration> collection = registrationService.getRegistrations(registration.getCourse(),
                            registration.getRegister(), registration.getInstitution());
                    RegistrationReport report = new RegistrationReport();
                    

                    processReport(report, contentType, collection, new HashMap<String, Object>());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        };

        form.add(new DropDownChoice("register", registerService.findAll()));
        form.add(new DropDownChoice("course", courseService.findAll()));
        form.add(new DropDownChoice("contentType", contentTypeModel, Arrays.asList(ContentType.values())));
        add(form);

        DefaultPieDataset d = new DefaultPieDataset();

        List<Register> registers = this.registerService.findAll();
        for (Register register : registers) {
            List<Registration> registrations = this.registrationService.getRegistrations(null, register, null);
            d.setValue(register.getName(), registrations.size());
        }



        JFreeChart chart = ChartFactory.createPieChart3D("Registers registration ratio", d, true, true, true);
        add(new JFreeChartImage("image", chart, 500, 300));
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
