/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.models;

import java.util.List;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.District;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.utils.HrisComparator;

/**
 *
 * @author kelvin
 */
public class InstitutionDistrictListModel extends LoadableDetachableModel<List<Institution>> {

    private final InstitutionService institutionService;
    private final District district;
    private HrisComparator hrisComparator = new HrisComparator();

    public InstitutionDistrictListModel(InstitutionService institutionService, District district) {
        this.institutionService = institutionService;
        this.district = district;
    }

    @Override
    protected List<Institution> load() {
        if (district == null) {
            return (List<Institution>) hrisComparator.sort(institutionService.findAll());
        }
        return (List<Institution>) hrisComparator.sort(institutionService.getInstitutionInDistrict(district));
    }
}
