/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author tdhlakama
 */
@Entity
@Table(name = "registrantcondition")
@Audited
public class RegistrantCondition extends BaseIdEntity implements Serializable {

    private Conditions condition;
    private Registrant registrant;
    private Boolean status = Boolean.FALSE;
    private Boolean personal = Boolean.FALSE;
    private Date startDate;
    private Date endDate;
    private String description;

    @ManyToOne
    public Conditions getCondition() {
        return condition;
    }

    public void setCondition(Conditions condition) {
        this.condition = condition;
    }

    @ManyToOne
    public Registrant getRegistrant() {
        return registrant;
    }

    @Column(length = 1000)
    public String getDescription() {
        if (description == null) {
            return "";
        }
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setRegistrant(Registrant registrant) {
        this.registrant = registrant;
    }

    @Basic(optional = false)
    @Column(name = "Status", nullable = false)
    public Boolean getStatus() {
        if (status == null) {
            return Boolean.FALSE;
        }
        return status;
    }

    public Boolean getPersonal() {
        if (personal == null) {
            return Boolean.FALSE;
        }
        return personal;
    }

    public void setPersonal(Boolean personal) {
        this.personal = personal;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Transient
    public String getStatusText() {
        return getStatus() ? "Active" : "Inactive";
    }

    @Override
    public String toString() {
        if (getCondition() != null) {
            return getCondition().getComments();
        } else if (getPersonal()) {
            return getDescription();
        } else {
            return "";
        }

    }

    @Transient
    public String getConditionText() {
        if (condition != null) {
            return condition.getName();
        } else {
            return description;
        }
    }
}
