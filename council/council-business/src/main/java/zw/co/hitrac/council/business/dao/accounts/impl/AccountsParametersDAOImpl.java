package zw.co.hitrac.council.business.dao.accounts.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.accounts.AccountsParametersDAO;
import zw.co.hitrac.council.business.dao.repo.accounts.AccountsParametersRepository;
import zw.co.hitrac.council.business.domain.accounts.AccountsParameters;

/**
 *
 * @author kelvin goredema
 */
@Repository
public class AccountsParametersDAOImpl implements AccountsParametersDAO {

    @Autowired
    private AccountsParametersRepository accountsParametersRepository;

    public AccountsParameters save(AccountsParameters accountsParameters) {
        return accountsParametersRepository.save(accountsParameters);
    }

    public List<AccountsParameters> findAll() {
        return accountsParametersRepository.findAll();
    }

    public AccountsParameters get(Long id) {
        return accountsParametersRepository.findOne(id);
    }
    /*
     * A setter method that will make mocking repo object easier
     * @param  TransactionType
     */

    public void setAccountsParametersRepository(AccountsParametersRepository accountsParametersRepository) {
        this.accountsParametersRepository = accountsParametersRepository;
    }

    public List<AccountsParameters> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
