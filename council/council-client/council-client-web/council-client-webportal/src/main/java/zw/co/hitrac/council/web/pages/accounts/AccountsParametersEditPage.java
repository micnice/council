package zw.co.hitrac.council.web.pages.accounts;

import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Book;
import zw.co.hitrac.council.business.domain.accounts.AccountsParameters;
import zw.co.hitrac.council.business.service.accounts.AccountsParametersService;
import zw.co.hitrac.council.business.service.accounts.PaymentMethodService;
import zw.co.hitrac.council.business.service.accounts.TransactionTypeService;
import zw.co.hitrac.council.web.models.PaymentMethodListModel;
import zw.co.hitrac.council.web.models.TransactionTypeListModel;
import zw.co.hitrac.council.web.pages.configure.IAdministerDatabaseBasePage;

/**
 *
 * @author kelvin goredema
 */
public class AccountsParametersEditPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private AccountsParametersService accountsParametersService;
    @SpringBean
    private TransactionTypeService transactionTypeService;
    @SpringBean
    private PaymentMethodService paymentMethodService;

    public AccountsParametersEditPage() {
        setDefaultModel(new CompoundPropertyModel<AccountsParameters>(new LoadableDetachableAccountsParametersModel()));

        Form<AccountsParameters> form = new Form<AccountsParameters>("form", (IModel<AccountsParameters>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                accountsParametersService.save(getModelObject());
                setResponsePage(new AccountsParametersViewPage());

            }
        };
        form.add(new DropDownChoice("carryForwardTransactionType", new TransactionTypeListModel(transactionTypeService, Book.CB)));
        form.add(new DropDownChoice("invoiceTransactionType", new TransactionTypeListModel(transactionTypeService, Book.SL)));
        form.add(new DropDownChoice("manualTransactionType", transactionTypeService.findBankAccounts()));
        form.add(new DropDownChoice("creditNoteTransactionType", new TransactionTypeListModel(transactionTypeService, Book.SL)));
        form.add(new DropDownChoice("refundTransactionType", new TransactionTypeListModel(transactionTypeService, Book.CB)));
        form.add(new DropDownChoice("manualPaymentMethod", new PaymentMethodListModel(paymentMethodService)));
        form.add(new DropDownChoice("defaultPaymentMethod", new PaymentMethodListModel(paymentMethodService)));
        form.add(new CheckBox("studentRegistrationPayment"));
        form.add(new CheckBox("qualificationFee"));
        form.add(new CheckBox("registrationApplicationPayment"));
        form.add(new CheckBox("practisingCertificatePayment"));
        form.add(new CheckBox("payingMaximumAge"));
        form.add(new CheckBox("printPDFReceipt"));
        form.add(new TextField<Integer>("payingPractitionersMaximumAge"));
        form.add(new CheckBox("allowCashOverpayment"));
        form.add(new CheckBox("allowAccruals"));        
        form.add(new CheckBox("printJavaScriptReceipt"));
        form.add(new CheckBox("billEachPostBasicQualification"));
        form.add(new CheckBox("printReceiptNumberOnCertificate"));

        
        form.add(new BookmarkablePageLink<Void>("returnLink", AccountsParametersViewPage.class));
        add(form);
    }

    private final class LoadableDetachableAccountsParametersModel extends LoadableDetachableModel<AccountsParameters> {

        @Override
        protected AccountsParameters load() {
            return accountsParametersService.get();
        }
    }
}
