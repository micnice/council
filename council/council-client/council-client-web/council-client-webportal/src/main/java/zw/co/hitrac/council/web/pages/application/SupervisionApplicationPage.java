package zw.co.hitrac.council.web.pages.application;

//~--- non-JDK imports --------------------------------------------------------
import com.googlecode.wicket.jquery.core.JQueryBehavior;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.ApplicationPurpose;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.process.ApplicationProcess;
import zw.co.hitrac.council.business.service.ApplicationService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.certification.CertificationPage;
import zw.co.hitrac.council.web.utility.CustomDateTextField;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

//~--- JDK imports ------------------------------------------------------------

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import zw.co.hitrac.council.business.domain.EmploymentType;
import zw.co.hitrac.council.business.domain.RegistrantDisciplinary;
import zw.co.hitrac.council.business.domain.RegistrantQualification;
import zw.co.hitrac.council.business.domain.reports.ItemCase;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrantDisciplinaryService;
import zw.co.hitrac.council.business.service.RegistrantQualificationService;
import zw.co.hitrac.council.business.service.UserService;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.pages.accounts.documents.DebtComponentsPage;
import zw.co.hitrac.council.web.pages.registration.RegistrantPanel;
import zw.co.hitrac.council.web.pages.registration.RegistrantViewPage;

/**
 *
 * @author Michael Matiashe
 * @author Michael Matiashe
 */
public class SupervisionApplicationPage extends TemplatePage {

    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private ApplicationProcess applicationProcess;
    @SpringBean
    private ApplicationService applicationService;
    @SpringBean
    private RegistrationService registrationService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private RegistrantQualificationService registrantQualificationService;
    @SpringBean
    private UserService userService;
    Boolean show;
    @SpringBean
    private RegistrantDisciplinaryService registrantDisciplinaryService;
    @SpringBean
    private RegistrationProcess registrationProcess;

    public SupervisionApplicationPage(long id) {

        menuPanelManager.getApplicationPanel().setTopMenuCurrent(true);

        if (!generalParametersService.get().getRegistrationByApplication()) {
            show = Boolean.FALSE;
        } else {
            show = Boolean.TRUE;
        }
        final CompoundPropertyModel<Application> model =
                new CompoundPropertyModel<Application>(applicationService.get(id));

        setDefaultModel(model);
        add(new RegistrantPanel("registrantPanel", model.getObject().getRegistrant().getId()));;
        this.add(new JQueryBehavior("#tabs", "tabs"));
        Form<Application> form = new Form<Application>("form", (IModel<Application>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    Application application = applicationProcess.approve(getModelObject());
                    if (application.getApplicationStatus().equals(Application.APPLICATIONAPPROVED)) {
                        setResponsePage(new CertificationPage(model.getObject().getRegistrant().getId()));
                        applicationService.save(application);
                    } else {
                        setResponsePage(new SupervisionApplicationViewPage(getModelObject().getId()));
                    }

                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };
        String registerTypeStatement = " ";
        Set<String> registerTypes = new HashSet<String>();
        for (RegistrantQualification r : registrantQualificationService.getRegistrantQualifications(model.getObject().getRegistrant())) {
            if (r.getQualification().getRegisterType() != null && r.getQualification().getRegisterType().getName() != null) {
                registerTypes.add(r.getQualification().getRegisterType().getName());
            }
        }

        for (String s : registerTypes) {
            registerTypeStatement = registerTypeStatement.concat(s + ", ");
        }

        form.add(new Label("registerTypes", registerTypeStatement));
        form.add(new Label("currentStatus", model.getObject().getApplicationTextStatus()));
        form.add(new Label("approvedByLabel", "Approved By"));
        form.add(new DropDownChoice("approvedBy", userService.getCouncilProcessors()));
        form.add(new Label("processedByLabel", "Prossesed By"));
        form.add(new DropDownChoice("processedBy", userService.getApplicationpProcessors()));
        form.add(new Label("dateApprovedLabel", "Decision Date"));
        form.add(new Label("displineryCasesLabel", "Applicant Has Disciplinary Cases Pending"));
        form.add(new Label("misconductTypeLabel", "Applicant Was Found Guilty of the following"));
        form.add(new Label("dateProcessedLabel", "Final Processing Date"));
        form.add(new CustomDateTextField("dateApproved").add(DatePickerUtil.getDatePicker()));
        form.add(new CustomDateTextField("dateProcessed").add(DatePickerUtil.getDatePicker()));
       

        form.add(new DropDownChoice("applicationStatus", Application.getApplicationStates).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("employmentType", Arrays.asList(EmploymentType.values())));
        form.add(new Label("applicationPurpose", model.getObject().getApplicationPurpose()));
        form.add(new Label("applicationStatusLabel", "Application Decision"));
        form.add(new Label("applicationDate", DateUtil.getDate(model.getObject().getApplicationDate())));
        form.add(new TextArea("comment"));
        form.add(new Label("commentLabel", "Comment") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.TRUE);
            }
        });
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(model.getObject().getRegistrant().getId()));
            }
        });

        form.add(new Label("anyPendingCases", registrantDisciplinaryService.getPendingCases(model.getObject().getRegistrant())));
        Set<ItemCase> items = new HashSet<ItemCase>();
        for (RegistrantDisciplinary d : registrantDisciplinaryService.getGuiltyResolvedCases(model.getObject().getRegistrant())) {
            ItemCase itemCase = new ItemCase();
            itemCase.setName(d.getMisconductType().getName()); //   misconduct type                   
            items.add(itemCase);
        }
        String foundGuiltyOf = "";
        for (ItemCase i : items) {
            foundGuiltyOf = foundGuiltyOf + " " + i.getName();
        }
        if (foundGuiltyOf.isEmpty()) {
            foundGuiltyOf = "N/A";
        }
        form.add(new Label("anyPendingCasesMisconductTypes", foundGuiltyOf));
        add(form);

        add(new FeedbackPanel("feedback"));
    }

    public SupervisionApplicationPage(final IModel<Registrant> registrantModel) {
        add(new RegistrantPanel("registrantPanel", registrantModel.getObject().getId()));

        menuPanelManager.getApplicationPanel().setTopMenuCurrent(true);
        this.add(new JQueryBehavior("#tabs", "tabs"));
        CompoundPropertyModel<Application> model = new CompoundPropertyModel<Application>(new Application());
        if (!generalParametersService.get().getRegistrationByApplication()) {
            show = Boolean.FALSE;
        } else {
            show = Boolean.TRUE;
        }

        setDefaultModel(model);
        model.getObject().setApplicationDate(new Date());
        model.getObject().setRegistrant(registrantModel.getObject());

        model.getObject().setApplicationPurpose(ApplicationPurpose.SUPERVISION);

        Registration r = registrationProcess.activeRegisterNotStudentRegister(registrantModel.getObject());

        if (r != null) {
            model.getObject().setCourse(r.getCourse());
        }

        Form<Application> form = new Form<Application>("form", (IModel<Application>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    getModelObject().setCreatedBy(CouncilSession.get().getUser());
                    applicationProcess.apply(getModelObject());
                    //Select items to pay for
                    setResponsePage(new DebtComponentsPage(registrantModel.getObject().getId()));
                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };

        form.add(new Label("approvedByLabel", "Approved By") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });

        form.add(new Label("commentLabel", "Comment") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new DropDownChoice("approvedBy", userService.getCouncilProcessors()) {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new Label("processedByLabel", "Prossesed By") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new DropDownChoice("processedBy", userService.getApplicationpProcessors()) {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new Label("dateApprovedLabel", "Decision Date") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });

        form.add(new Label("displineryCasesLabel", "Applicant Has Disciplinary Cases Pending") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new Label("misconductTypeLabel", "Misconduct Types") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });

        form.add(new Label("dateProcessedLabel", "Final Processing Date") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new CustomDateTextField("dateApproved") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new CustomDateTextField("dateProcessed") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
                
        final List applicationStates = Arrays.asList(new String[]{Application.APPLICATIONPENDING, Application.APPLICATIONAPPROVED,
            Application.APPLICATION_IN_CIRCULATION, Application.APPLICATION_AWAITING_COUNCIL_APPROVAL, Application.APPLICATIONUNDERREVIEW});

        form.add(new DropDownChoice("applicationStatus", applicationStates) {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });

        String registerTypeStatement = " ";
        Set<String> registerTypes = new HashSet<String>();
        for (RegistrantQualification q : registrantQualificationService.getRegistrantQualifications(registrantModel.getObject())) {
            if (q.getQualification().getRegisterType() != null && q.getQualification().getRegisterType().getName() != null) {
                registerTypes.add(q.getQualification().getRegisterType().getName());
            }
        }

        for (String s : registerTypes) {
            registerTypeStatement = registerTypeStatement.concat(s + ", ");
        }

        form.add(new Label("registerTypes", registerTypeStatement));
        form.add(new Label("applicationPurpose", model.getObject().getApplicationPurpose()));
        form.add(new Label("currentStatus", model.getObject().getApplicationTextStatus()));
        form.add(new Label("applicationStatusLabel", "Application Decision") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new Label("applicationDate", DateUtil.getDate(model.getObject().getApplicationDate())));
        form.add(new DropDownChoice("employmentType", Arrays.asList(EmploymentType.values())));

        form.add(new TextArea("comment") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });
        form.add(new Label("anyPendingCases", "") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new Label("anyPendingCasesMisconductTypes", "") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        add(form);

        add(new FeedbackPanel("feedback"));
    }
}

//~ Formatted by Jindent --- http://www.jindent.com
