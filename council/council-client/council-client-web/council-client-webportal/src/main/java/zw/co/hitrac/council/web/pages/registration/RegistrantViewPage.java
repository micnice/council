package zw.co.hitrac.council.web.pages.registration;

import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.domain.TypeOfService;
import zw.co.hitrac.council.business.domain.integration.Employee;
import zw.co.hitrac.council.business.process.*;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.business.service.accounts.*;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.menu.PracticeCertifcateCardPanel;
import zw.co.hitrac.council.web.models.EmployeeModel;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.application.*;
import zw.co.hitrac.council.web.pages.certification.CertificateDuplicatePage;
import zw.co.hitrac.council.web.pages.certification.CertificationEditPage;
import zw.co.hitrac.council.web.pages.examinations.ExamRegistrationPage;
import zw.co.hitrac.council.web.pages.examinations.ExamSupplementaryPage;
import zw.co.hitrac.council.web.pages.examinations.RegistrantExaminationsPage;
import zw.co.hitrac.council.web.pages.examinations.StudentRegistrationPage;
import zw.co.hitrac.council.web.pages.institution.InstitutionEditPage;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;
import zw.co.hitrac.council.web.utility.DetachableRegistrationModel;
import zw.co.hitrac.council.business.process.PaymentProcess;
import java.util.List;

import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;

;

/**
 * @author Takunda Dhlakama
 * @author Matiashe Michael
 * @author Edwin Magodi
 */
public class RegistrantViewPage extends TemplatePage {

    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    AccountService accountService;
    @SpringBean
    CustomerService customerAccountService;
    @SpringBean
    RegistrationService registrationService;
    @SpringBean
    private RegistrationProcess registrationProcess;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private SupExamRegistrationProcess supExamRegistrationProcess;
    @SpringBean
    private DurationService durationService;
    @SpringBean
    private RegistrantActivityService registrantActivityService;
    @SpringBean
    private DebtComponentService debtComponentService;
    private static String UP_GRADE = "UP_GRADE";
    private static String EXAM_UP_GRADE = "EXAM_UP_GRADE";
    @SpringBean
    private InvoiceProcess invoiceProcess;
    @SpringBean
    private ReceiptItemService receiptItemService;
    private DetachableRegistrantModel registrantModel;
    @SpringBean
    private RegistrantDisciplinaryService disciplinaryService;
    @SpringBean
    private AccountsParametersService accountsParametersService;
    @SpringBean
    private PaymentProcess paymentProcess;
    @SpringBean
    private RenewalProcess renewalProcess;

    public RegistrantViewPage(PageParameters parameters) {
        this(parameters.get("registrantId").toLong());
    }

    public RegistrantViewPage(final Long id) {
        menuPanelManager.getRegistrantMenuPanel().setTopMenuCurrent(true);
        add(new RegistrantSnapShot("registrantSnapShot", id));
        add(new RegistrantPanel("registrantPanel", id));
        add(new PracticeCertifcateCardPanel("practiceCertifcateCardPanel", id));
        registrantModel = new DetachableRegistrantModel(id, registrantService);

        final CompoundPropertyModel<Registrant> model = new CompoundPropertyModel<Registrant>(registrantModel);

        setDefaultModel(model);

        final Boolean currentRegistration = registrationService.hasCurrentRegistration(registrantModel.getObject());

        final Boolean studentNoneRegistration = registrationProcess.studentNoneRegistration(registrantModel.getObject());

        final Boolean studentRegistration =
                registrationProcess.studentRegistration(registrantModel.getObject());

        final Boolean disciplinaryDisabled = disciplinaryService.registrantSuspendedStatus(registrantModel.getObject());

        //if deRegistered = True disables Links
        final Boolean deRegistered = registrationService.hasBeenDeRegistered(registrantModel.getObject());
        //if hasPenalty = true No Penalties 
        final Boolean hasPenalty = debtComponentService.hasAccountDebtComponents(registrantModel.getObject().getCustomerAccount(), TypeOfService.PENALTY_FEES);

        Boolean disable = Boolean.FALSE;
        if (deRegistered || hasPenalty || disciplinaryDisabled || registrantModel.getObject().getVoided() || registrantModel.getObject().getDead()) {
            disable = Boolean.TRUE;
        }

        final Boolean disableLinks = disable;

        final boolean hasCurrentRenewal = renewalProcess.hasCurrentRenewal(registrantModel.getObject());

        add(new Link<Registrant>("registrantEditPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantEditPage(registrantModel.getObject().getId(), Boolean.TRUE));
            }

            @Override
            protected void onConfigure() {
                setVisibilityAllowed(CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK))));
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                return enabled;
            }
        });
        //method to add a new Registration - Id is null
        if (!generalParametersService.get().getRegistrationByApplication() || generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
            add(new Link<Registrant>("registrationPage") {
                @Override
                public void onClick() {
                    setResponsePage(new RegistrationPage(null, registrantModel, null, null, null));
                }

                @Override
                public boolean isEnabled() {
                    Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                    return enabled;
                }

                @Override
                protected void onConfigure() {
                    Registration registration = registrationProcess.activeStudentRegister(registrantModel.getObject());
                    if (disableLinks) {
                        setVisible(Boolean.FALSE);
                    } else if (registration != null && !supExamRegistrationProcess.checkSupplementaryStatus(registration)) {
                        setVisible(Boolean.TRUE);
                    } else if (registrantModel.getObject().getRegistrantQualificationList().isEmpty()) {
                        setVisible(Boolean.FALSE);
                    } else if (studentNoneRegistration) {
                        setVisible(Boolean.FALSE);
                    } else {
                        setVisible(Boolean.TRUE);
                    }
                }
            });
        } else {
            add(new Link<Registrant>("registrationPage") {
                @Override
                public void onClick() {
                    setResponsePage(new ApplicationPage(registrantModel));
                }

                @Override
                protected void onConfigure() {
                    setVisible(Boolean.FALSE);
                }
            });
        }
        add(new Link<Registrant>("application") {
            @Override
            public void onClick() {
                setResponsePage(new ApplicationPage(registrantModel));
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                return enabled;
            }

            @Override
            protected void onConfigure() {
                if (disableLinks) {
                    setVisible(Boolean.FALSE);
                } else if (registrantModel.getObject().getRegistrantQualificationList().isEmpty()) {
                    setVisible(Boolean.FALSE);
                } else if (currentRegistration) {
                    setVisible(Boolean.FALSE);
                } else {
                    if (generalParametersService.get().getRegistrationByApplication() || !generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                        setVisibilityAllowed(Boolean.TRUE);
                    } else {
                        setVisible(Boolean.FALSE);
                    }
                }

            }
        });

        add(new Link<Registrant>("applicationForCGS") {
            @Override
            public void onClick() {
                setResponsePage(new CgsApplicationPage(registrantModel));
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                return enabled;
            }

            @Override
            protected void onConfigure() {
                if (disableLinks) {
                    setVisible(Boolean.FALSE);
                } else if (registrantModel.getObject().getRegistrantQualificationList().isEmpty()) {
                    setVisible(Boolean.FALSE);
                } else {
                    if (studentNoneRegistration) {
                        setVisible(!debtComponentService.hasAccountDebtComponents(registrantModel.getObject().getCustomerAccount(), TypeOfService.APPLICATION_FOR_CGS_FEES));
                    } else {
                        setVisible(Boolean.FALSE);
                    }
                }
            }
        });

        add(new Link<Registration>("examRegistrationPage") {
            // new student registration
            @Override
            public void onClick() {
                setResponsePage(new ExamRegistrationPage(registrantModel));
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                return enabled;
            }

            @Override
            protected void onConfigure() {
                if (disableLinks) {
                    setVisible(Boolean.FALSE);
                } else if (!generalParametersService.get().getHasProcessedByBoard() && studentRegistration) {
                    setVisible(Boolean.FALSE);
                } else if (!generalParametersService.get().getExaminationModule()) {
                    setVisible(Boolean.FALSE);
                } else if (registrantModel.getObject().getRegistrantQualificationList().isEmpty() && !currentRegistration) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(Boolean.FALSE);
                }

            }
        });

        add(new Link<Registration>("studentRegistrationPage") {
            // new student registration
            @Override
            public void onClick() {
                setResponsePage(new StudentRegistrationPage(registrantModel));
            }

            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getHasProcessedByBoard()) {
                    setVisible(Boolean.FALSE);
                }
            }
        });

        add(new Link<Registration>("studentExamRegistrationPage") {
            // new student registration
            @Override
            public void onClick() {
                Registration studentRegistration = registrationProcess.activeStudentRegister(registrantModel.getObject());
                setResponsePage(new ExamRegistrationPage(registrantModel, new DetachableRegistrationModel(studentRegistration.getId(), registrationService)));
            }

            @Override
            protected void onConfigure() {
                if (!generalParametersService.get().getExaminationModule()) {
                    setVisible(Boolean.FALSE);
                } else if (!generalParametersService.get().getHasProcessedByBoard() && studentNoneRegistration) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(Boolean.FALSE);
                }
            }
        });

        Link<Registration> applicationLink = new Link<Registration>("applicationPage") {
            @Override
            public void onClick() {
                setResponsePage(new ApplicationPage(registrantModel));
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                return enabled;
            }

            @Override
            protected void onConfigure() {
                if (disableLinks) {
                    setVisible(Boolean.FALSE);
                } else {
                    setVisible(registrantModel.getObject().getRegistrantQualificationList().isEmpty() && accountsParametersService.get().getStudentRegistrationPayment());
                }

            }
        };

        if (generalParametersService.get().getRegistrationByApplication()) {
            applicationLink.add(new Label("myLabel", "Apply"));
        } else {
            applicationLink.add(new Label("myLabel", "Process Provisional Registration"));
        }
        add(applicationLink);

        add(new Link<Registration>("registerPBQExam") {
            // new student registration
            @Override
            public void onClick() {
                Registration registration = registrationProcess.activeRegisterNotStudentRegister(registrantModel.getObject());

                setResponsePage(new ExamRegistrationPage(registrantModel,
                        new DetachableRegistrationModel(registration.getId(), registrationService)));

            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                return enabled;
            }

            @Override
            protected void onConfigure() {
                Registration registration = registrationProcess.activeStudentRegister(registrantModel.getObject());
                if (disableLinks) {
                    setVisible(Boolean.FALSE);
                } else if (!generalParametersService.get().getExaminationModule()) {
                    setVisible(Boolean.FALSE);
                } else if (registration != null && supExamRegistrationProcess.checkSupplementaryStatus(registration)) {
                    setVisible(Boolean.FALSE);
                } else {
                    setVisible(hasCurrentRenewal);
                }
            }
        });
        add(new Link<Registration>("examSupplementaryPage") {
            @Override
            public void onClick() {
                setResponsePage(new ExamSupplementaryPage(registrationProcess.activeStudentRegister(registrantModel.getObject())));

            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                return enabled;
            }

            @Override
            protected void onConfigure() {
                Registration registration = registrationProcess.activeStudentRegister(registrantModel.getObject());
                if (disableLinks) {
                    setVisible(Boolean.FALSE);
                } else if (!generalParametersService.get().getExaminationModule()) {
                    setVisible(Boolean.FALSE);
                } else if (supExamRegistrationProcess.checkOpenExamRegistration(registration)) {
                    setVisible(Boolean.FALSE);
                } else {
                    if (supExamRegistrationProcess.checkSupplementaryStatus(registration)) {
                        setVisible(Boolean.TRUE);
                    } else {
                        setVisible(Boolean.FALSE);
                    }
                }
            }
        });

        //method to add a new Registration - Id is null
        add(new Link<Registrant>("newRegistrationPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrationPage(null, registrantModel, null, null, null));
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                return enabled;
            }

            @Override
            protected void onConfigure() {
                if (disableLinks) {
                    setVisible(Boolean.FALSE);
                } else if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.FALSE);
                } else if (!generalParametersService.get().getRegistrationByApplication()) {
                    setVisible(!registrantModel.getObject().getRegistrantQualificationList().isEmpty());
                } else {
                    setVisible(Boolean.FALSE);
                }
            }
        });

        add(new Link<Registrant>("lastRenewal") {
            @Override
            public void onClick() {
                setResponsePage(new LastRenewalPage(registrantModel));
            }

            @Override
            protected void onConfigure() {
                setVisible(!registrationService.getRegistrations(registrantModel.getObject()).isEmpty());
            }

        });

        add(new Link<Registrant>("captureFromFile") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrationPage(null, registrantModel, null, null, Boolean.TRUE));
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK, Role.IT_OFFICER)));
                return enabled;
            }

            @Override
            protected void onConfigure() {
                if (deRegistered) {
                    setVisible(Boolean.FALSE);
                } else if (registrationService.getActiveRegistrations(registrantModel.getObject()).isEmpty()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(Boolean.FALSE);
                }
            }
        });

        add(new Link<Registrant>("preRegistrationPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrationPage(null, registrantModel, null, null, Boolean.FALSE));
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                return enabled;
            }

            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.FALSE);
                } else if (registrationService.getActiveRegistrations(registrantModel.getObject()).isEmpty()) {
                    setVisible(generalParametersService.get().getAllowPreRegistrantion());
                } else {
                    setVisible(Boolean.FALSE);
                }
            }
        });

        add(new Link<Registrant>("upGradeRegistrationPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrationPage(null, registrantModel, null, Boolean.TRUE, null));
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                return enabled;
            }

            @Override
            protected void onConfigure() {
                if (disableLinks) {
                    setVisible(Boolean.FALSE);
                } else if (registrantModel.getObject().getRegistrantQualificationList().isEmpty()) {
                    setVisible(Boolean.FALSE);
                } else if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.FALSE);
                } else {
                    if (studentNoneRegistration) {
                        setVisible(Boolean.TRUE);
                    } else {
                        setVisible(Boolean.FALSE);
                    }
                }
            }
        });

        add(new Link<Registrant>("applicationQualification") {
            @Override
            public void onClick() {
                setResponsePage(new QualificationApplicationPage(registrantModel));
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                return enabled;
            }

            @Override
            protected void onConfigure() {
                if (disableLinks) {
                    setVisible(Boolean.FALSE);
                } else {
                    setVisible(hasCurrentRenewal);
                }
            }
        });

        add(new Link<Registrant>("registerNewQualification") {
            @Override
            public void onClick() {
                List<Registration> registrations = registrationService.getActiveRegistrations(registrantModel.getObject());
                if (!registrations.isEmpty() && registrations.size() > 0) {
                    setResponsePage(new RegistrantQualificationPage(registrations.get(0)));
                } else {
                    setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
                }
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                return enabled;
            }

            @Override
            protected void onConfigure() {
                if (disableLinks) {
                    setVisible(Boolean.FALSE);
                } else if (currentRegistration) {
                    setVisible(studentNoneRegistration);
                } else {
                    setVisible(Boolean.FALSE);
                }
            }
        });

        add(new Link<Registrant>("transcriptOfTrainingApplicationPage") {
            @Override
            public void onClick() {
                setResponsePage(new TranscriptOfTrainingApplicationPage(registrantModel));
            }

            @Override
            protected void onConfigure() {
                if (disableLinks) {
                    setVisible(Boolean.FALSE);
                } else if (generalParametersService.get().getRegistrationByApplication()) {
                    setVisibilityAllowed(Boolean.TRUE);
                } else {
                    setVisible(Boolean.FALSE);
                }
            }
        });

        add(new Link<Registrant>("registrantRegistrationListPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantRegistrationListPage(registrantModel));
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.IT_OFFICER)));
                return enabled;
            }

        });

        add(new Link<Registrant>("renewalLink") {
            @Override
            public void onClick() {
                if (generalParametersService.get().getHasQuaterRenewal()) {
                    setResponsePage(new RenewalQuatarPage(registrantModel));
                } else {
                    setResponsePage(new RenewalPage(registrantModel));
                }
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                return enabled;
            }

            @Override
            protected void onConfigure() {
                if (disableLinks) {
                    setVisible(Boolean.FALSE);
                } else if (currentRegistration) {
                    setVisible(studentNoneRegistration);
                } else {
                    setVisible(Boolean.TRUE);
                }
            }
        });

        add(new Link<Registrant>("updatePhotoLink") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantImageUploadPage(Model.of(registrantModel.getObject())));
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                return enabled;
            }
        });

        add(new RegistrantCpdPanelList("registrantCpdPanelList", registrantModel));
        add(new EmploymentPanelList("employmentPanelList", registrantModel));
        add(new RegistrantDisciplinaryPanelListPage("registrantDisciplinaryPanelList", registrantModel));
        add(new RegistrantQualificationPanelList("registrantQualificationPanelList", registrantModel));
        add(new RegistrantAccountPanelList("registrantAccountPanelList", registrantModel));

        //supporting documents
        add(new RegistrantSupportingDocumentPanel("supportingDocumentPanel", registrantModel.getObject(), null, null, null, null, null, null));

        //supporting documents
        add(new RegistrantIdentificationPanelList("registrantIdentificationPanelList", registrantModel));
        //supporting documents
        add(new RegistrantAssessmentPanelList("registrantAssessmentPanelList", registrantModel));

        //supporting documents
        add(new RegistrantConditionPanelList("registrantConditionPanelList", registrantModel));

        //supporting documents
        add(new RegistrantContactPanelList("registrantContactPanelList", registrantModel));

        //supporting documents
        add(new RegistrantAddressPanelList("registrantAddressPanelList", registrantModel));

        add(new Link<Registrant>("registrantDocumentPage") {
            @Override
            public void onClick() {
                setResponsePage(new RequirementSelectPage(Model.of(registrantModel.getObject())));
            }
        });


        add(new Link<Registrant>("registrantExaminationsPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantExaminationsPage(registrantModel));
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                return enabled;
            }
        });

        add(new Link<Registrant>("certificationEditPage") {
            @Override
            public void onClick() {
                setResponsePage(new CertificationEditPage(null, registrantModel));
            }

            @Override
            protected void onConfigure() {
                if (disableLinks) {
                    setVisible(Boolean.FALSE);
                } else {
                    setVisibilityAllowed(CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR))));
                }

            }
        });

        add(new Link<Registrant>("registrantActivityRenewal") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantActivityListPage(registrantModel.getObject().getId()));
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.IT_OFFICER)));
                return enabled;
            }
        });

        add(new Link<Registrant>("institutionApplicationPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionEditPage(null, registrantModel.getObject()));
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                return enabled;
            }

            @Override
            protected void onConfigure() {
                if (disableLinks) {
                    setVisible(Boolean.FALSE);
                } else if (hasCurrentRenewal) {
                    setVisible(!debtComponentService.hasAccountDebtComponents(registrantModel.getObject().getCustomerAccount(), TypeOfService.INSTITUTION__REGISTRATION_FEES));
                }
            }
        });

        add(new Link<Registrant>("applicationForTransfer") {
            @Override
            public void onClick() {

                setResponsePage(new TransferApplicationPage(registrantModel));

            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                return enabled;
            }

            @Override
            protected void onConfigure() {
                if (disableLinks) {
                    setVisible(Boolean.FALSE);
                } else {
                    setVisible(!debtComponentService.hasAccountDebtComponents(registrantModel.getObject().getCustomerAccount(), TypeOfService.TRANSFER_FEES));
                }
            }
        });

        add(new Link<Registrant>("applicationForChangeOfName") {
            @Override
            public void onClick() {
                setResponsePage(new ChangeOfNameApplicationPage(registrantModel));

            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                return enabled;
            }

            @Override
            protected void onConfigure() {
                if (disableLinks) {
                    setVisible(Boolean.FALSE);
                } else {
                    setVisible(!debtComponentService.hasAccountDebtComponents(registrantModel.getObject().getCustomerAccount(), TypeOfService.CHANGE_OF_NAME_FEES));
                }
            }
        });

        add(new Link<Registrant>("registrantClearanceEditPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantClearanceEditPage(registrantModel));
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                return enabled;
            }

            @Override
            protected void onConfigure() {
                if (disableLinks) {
                    setVisible(Boolean.FALSE);
                }
            }
        });

        add(new Link<Registrant>("supervisionApplication", registrantModel) {
            @Override
            public void onClick() {
                setResponsePage(new SupervisionApplicationPage(registrantModel));
            }

            @Override
            protected void onConfigure() {
                if (disableLinks) {
                    setVisible(Boolean.FALSE);
                } else {
                    setVisible(!debtComponentService.hasAccountDebtComponents(registrantModel.getObject().getCustomerAccount(), TypeOfService.APPLICATION_FOR_SUPERVISION));
                }
            }
        });

        add(new Link<Registrant>("reportDeathPage") {
            @Override
            public void onClick() {
                setResponsePage(new ReportDeathtPage(registrantModel.getObject().getId()));
            }

            @Override
            protected void onConfigure() {
                setVisibilityAllowed(CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.IT_OFFICER))));
            }
        });

        add(new Link<Registrant>("registrantClearanceDateEditPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantClearanceDateEditPage(registrantModel));
            }

            @Override
            protected void onConfigure() {
                setVisibilityAllowed(CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.IT_OFFICER))));
            }
        });

        add(new Link<Registrant>("deRegisterPage") {
            @Override
            public void onClick() {
                setResponsePage(new DeRegisterPage(registrantModel.getObject().getId()));
            }

            @Override
            protected void onConfigure() {
                setVisibilityAllowed(CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.IT_OFFICER))));
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK, Role.IT_OFFICER)));
                return enabled;
            }
        });

        add(new Link<Registrant>("voidRegistrantPage") {
            @Override
            public void onClick() {
                setResponsePage(new VoidRegistrantPage(registrantModel.getObject().getId()));
            }

            @Override
            protected void onConfigure() {
                setVisibilityAllowed(CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.IT_OFFICER))));
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK, Role.IT_OFFICER)));
                return enabled;
            }
        });

        add(new Link<Registrant>("retireRegistrantPage") {
            @Override
            public void onClick() {
                setResponsePage(new RetireRegistrantPage(registrantModel.getObject().getId()));
            }

            @Override
            protected void onConfigure() {
                setVisibilityAllowed(CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.IT_OFFICER))));
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK, Role.IT_OFFICER)));
                return enabled;
            }
        });


        add(new Link<Registrant>("payDuplicate") {
            @Override
            public void onClick() {
                setResponsePage(new CertificateDuplicatePage(registrantModel.getObject().getId()));

            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                return enabled;
            }

            @Override
            protected void onConfigure() {
                if (disableLinks) {
                    setVisible(Boolean.FALSE);
                } else {
                    setVisible(!debtComponentService.hasAccountDebtComponents(registrantModel.getObject().getCustomerAccount(), TypeOfService.CHANGE_OF_NAME_FEES));
                }
            }
        });

        EmployeeModel employeeModel = new EmployeeModel(registrantModel);
        Employee employee = employeeModel.getObject();
        add(new Label("statusLabel", employee.getStatus()));
        add(new Label("firstnameLabel", employee.getFirstname()));
        add(new Label("lastnameLabel", employee.getLastname()));
        add(new Label("middlenameLabel", employee.getMiddlename()));
        add(new Label("stationLabel", employee.getStation()));
        add(new Label("genderLabel", employee.getGender()));
        add(new Label("dateOfBirthLabel", employee.getDateOfBirth()));
        add(new Label("nationalIdLabel", employee.getNationalId()));
        add(new Label("postLabel", employee.getPost()));


    }

}
