package zw.co.hitrac.council.web.pages.reports;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.sql.DataSource;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.business.domain.RegistrantActivityType;
import zw.co.hitrac.council.business.domain.reports.RegisterStatistic;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.process.RenewalProcess;
import zw.co.hitrac.council.business.service.CouncilDurationService;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.DurationService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegisterService;
import zw.co.hitrac.council.business.service.RegistrantActivityService;
import zw.co.hitrac.council.business.service.RegistrantCpdItemConfigService;
import zw.co.hitrac.council.business.service.RegistrantCpdService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.reports.CpdPointsAndPaymentReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.models.CouncilDurationListModel;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 * @author hitrac
 */
public class CPDPointsDetailedReportPage extends IReportPage {
    
    private Integer numberOfInActiveRegistrantsInMainRegister;
    private CouncilDuration councilDuration;
    private CouncilDurationListModel councilDurationListModel;
    @SpringBean
    private CouncilDurationService councilDurationService;
    @SpringBean
    private DurationService durationService;
    @SpringBean
    private RegistrantCpdService registrantCpdService;
    @SpringBean
    private RegistrantCpdItemConfigService registrantCpdItemConfigService;
    private Course course;
    @SpringBean
    private CourseService courseService;
    @SpringBean
    private RegistrantActivityService registrantActivityService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private RegisterService registerService;
    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private DataSource dataSource;
    @SpringBean
    private RegistrationService registrationService;
    
    Register main = generalParametersService.get().getMainRegister();
    Register provisional = generalParametersService.get().getProvisionalRegister();
    Register student = generalParametersService.get().getStudentRegister();
    Register intern = generalParametersService.get().getInternRegister();
    Register maintaince = generalParametersService.get().getMaintenanceRegister();
    Register specialit = generalParametersService.get().getSpecialistRegister();
    
    @SpringBean
    RenewalProcess renewalProcess;
    
    public CPDPointsDetailedReportPage() {
        
        add(new FeedbackPanel("feedback"));
        PropertyModel<CouncilDuration> councilDurationModel = new PropertyModel<CouncilDuration>(this, "councilDuration");
        
        Form<?> form = new Form("form");
        form.add(new DropDownChoice("councilDuration", councilDurationModel, new CouncilDurationListModel(councilDurationService)).setRequired(true));
        
        form.add(new Button("summarisedReportCpdPointsButNotPaid") {
            
            @Override
            public void onSubmit() {
                Map parameters = new HashMap();
                
                CpdPointsAndPaymentReport cpdPointsAndPaymentReport = new CpdPointsAndPaymentReport();
                
                List<RegisterStatistic> registerStatisticsWithCPDsButNotPaid = new ArrayList<RegisterStatistic>();
                RegistrantActivityType registrantActivityType = RegistrantActivityType.RENEWAL;
                
                parameters.put("comment", " Detailed Register. INACTIVE FOR- " + councilDuration.getName() + " PERIOD WITH CPD POINTS & NO PAYMENT");
                Set<Register> registers = new HashSet<Register>();
                
                registers.addAll(registerService.findAll());
                registers.remove(generalParametersService.get().getStudentRegister());
                
                List<Duration> durations = new ArrayList<Duration>(councilDurationService.get(councilDuration.getId()).getDurations());
                
                for (Course c : registrationService.getDistinctCourseFromRegistration()) {
                    Set<RegistrantData> registrants = new HashSet<RegistrantData>();
                    
                    List<RegistrantData> registrantsWithCPDsButNotPaid = new ArrayList<RegistrantData>();
                    
                    for (Register r : registers) {
                        
                        registrants.addAll(registrantActivityService.getListPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(c, r, durations, RegistrantActivityType.RENEWAL, Boolean.FALSE, null, null));
                        
                    }
                    String course = registrationService.getDistinctCourseFromRegistration().toString();
                    System.out.println(course);
                    int test1 = registers.size();
                    System.out.println("Register------Number-----------size----" + test1);
                    
                    Duration duration = durationService.getCurrentCourseDuration(c);
                    
                    for (RegistrantData rd : registrants) {
                        boolean adequatePoints = registrantCpdService.getCurrentCPDs(registrantService.get(rd.getId()), duration).compareTo(registrantCpdItemConfigService.get().getTotalCpdPointsPermissable()) >= 0;
                        if (adequatePoints) {
                            registrantsWithCPDsButNotPaid.add(rd);
                        }
                        
                    }
                    int test = registrants.size();
                    System.out.println("Registrant---Number-----------size ------" + test);
                    
                    RegisterStatistic statistic = new RegisterStatistic();
                    statistic.setCourse(c);
                    for (RegistrantData data : registrantsWithCPDsButNotPaid) {
                        if (data.getCourseName() == null ? c.getName() != null : !data.getCourseName().equals(c.getName())) {
                            statistic.setNumberOfInactiveRegistrants(statistic.getNumberOfInactiveRegistrants() + 1);
                        }
                    }
                    registerStatisticsWithCPDsButNotPaid.add(statistic);
                }
                try {
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource
                            = ReportResourceUtils.getReportResource(cpdPointsAndPaymentReport, contentType, registerStatisticsWithCPDsButNotPaid, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);
                    
                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
                
            }
            
        });
        
        form.add(new Button("summarisedReportNoCpdPointsButPaid") {
            
            @Override
            public void onSubmit() {
                Map parameters = new HashMap();
                
                CpdPointsAndPaymentReport cpdPointsAndPaymentReport = new CpdPointsAndPaymentReport();
                
                List<RegisterStatistic> registerStatisticsNoCpdPointsButPaid = new ArrayList<RegisterStatistic>();
                RegistrantActivityType registrantActivityType = RegistrantActivityType.RENEWAL;
                
                parameters.put("comment", " Detailed Register. ACTIVE FOR- " + councilDuration.getName() + " PERIOD WITH INADEQUATE/NO CPD POINTS BUT PAYMENT DONE");
                Set<Register> registers = new HashSet<Register>();
                
                registers.addAll(registerService.findAll());
                registers.remove(generalParametersService.get().getStudentRegister());
                
                List<Duration> durations = new ArrayList<Duration>(councilDurationService.get(councilDuration.getId()).getDurations());
                
                for (Course c : courseService.findAll()) {
                    Set<RegistrantData> registrants = new HashSet<RegistrantData>();
                    List<RegistrantData> registrantsWithNoCPDsButPaid = new ArrayList<RegistrantData>();
                    
                    for (Register r : registers) {
                        
                        registrants.addAll(registrantActivityService.getListPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(c, r, durations, RegistrantActivityType.RENEWAL, Boolean.TRUE, null, null));
                        
                    }
                    int test1 = registers.size();
                    System.out.println("Register------Number-----------size----" + test1);;
                    Duration duration = durationService.getCurrentCourseDuration(c);
                    
                    for (RegistrantData rd : registrants) {
                        boolean adequatePoints = registrantCpdService.getCurrentCPDs(registrantService.get(rd.getId()), duration).compareTo(registrantCpdItemConfigService.get().getTotalCpdPointsPermissable()) >= 0;
                        if (!adequatePoints) {
                            registrantsWithNoCPDsButPaid.add(rd);
                            
                        }
                    }
                    int test = registrants.size();
                    System.out.println("Registrant---Number-----------size ------" + test);
                    
                    RegisterStatistic statistic = new RegisterStatistic();
                    statistic.setCourse(c);
                    for (RegistrantData data : registrantsWithNoCPDsButPaid) {
                        if (data.getCourseName() == null ? c.getName() != null : !data.getCourseName().equals(c.getName())) {
                            statistic.setNumberOfInactiveRegistrants(statistic.getNumberOfInactiveRegistrants() + 1);
                        }
                    }
                    registerStatisticsNoCpdPointsButPaid.add(statistic);
                }
                try {
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource
                            = ReportResourceUtils.getReportResource(cpdPointsAndPaymentReport, contentType, registerStatisticsNoCpdPointsButPaid,
                                    parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);
                    
                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }

                // To make Wicket stop processing form after sending response
                RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                
            }
            
        });
        
        form.add(new Button("summarisedReportNoCPDPointsNoPaymentMade") {
            
            @Override
            public void onSubmit() {
                Map parameters = new HashMap();
                
                CpdPointsAndPaymentReport cpdPointsAndPaymentReport = new CpdPointsAndPaymentReport();
                
                List<RegisterStatistic> registerStatisticsNoCpdPointsNoPayments = new ArrayList<RegisterStatistic>();
                RegistrantActivityType registrantActivityType = RegistrantActivityType.RENEWAL;
                
                parameters.put("comment", " Detailed Register. INACTIVE FOR - " + councilDuration.getName() + " PERIOD WITH INADEQUATE/NO CPD POINTS & NO PAYMENTS");
                Set<Register> registers = new HashSet<Register>();
                
                registers.addAll(registerService.findAll());
                registers.remove(generalParametersService.get().getStudentRegister());
                
                List<Duration> durations = new ArrayList<Duration>(councilDurationService.get(councilDuration.getId()).getDurations());
                
                for (Course c : registrationService.getDistinctCourseFromRegistration()) {
                    Set<RegistrantData> registrants = new HashSet<RegistrantData>();
                    List<RegistrantData> registrantsWithCPDsNoPayment = new ArrayList<RegistrantData>();
                    
                    for (Register r : registers) {
                        
                        registrants.addAll(registrantActivityService.getListPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(c, r, durations, RegistrantActivityType.RENEWAL, Boolean.FALSE, null, null));
                        
                    }
                    int test1 = registers.size();
                    System.out.println("Register------Number-----------size----" + test1);
                    Duration duration = durationService.getCurrentCourseDuration(c);
                    
                    for (RegistrantData rd : registrants) {
                        boolean adequatePoints = registrantCpdService.getCurrentCPDs(registrantService.get(rd.getId()), duration).compareTo(registrantCpdItemConfigService.get().getTotalCpdPointsPermissable()) >= 0;
                        if (!adequatePoints) {
                            registrantsWithCPDsNoPayment.add(rd);
                            
                        }
                    }
                    
                    int test = registrants.size();
                    System.out.println("Registrant---Number-----------size ------" + test);
                    RegisterStatistic statistic = new RegisterStatistic();
                    statistic.setCourse(c);
                    for (RegistrantData data : registrantsWithCPDsNoPayment) {
                        if (data.getCourseName() == null ? c.getName() != null : !data.getCourseName().equals(c.getName())) {
                            statistic.setNumberOfInactiveRegistrants(statistic.getNumberOfInactiveRegistrants() + 1);
                        }
                    }
                    registerStatisticsNoCpdPointsNoPayments.add(statistic);
                }
                
                try {
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource
                            = ReportResourceUtils.getReportResource(cpdPointsAndPaymentReport, contentType, registerStatisticsNoCpdPointsNoPayments,
                                    parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);
                    
                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
                
            }
            
        });
        add(form);
        
    }
    
}
