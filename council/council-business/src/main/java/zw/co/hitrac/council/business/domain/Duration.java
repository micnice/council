package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Michael Matiashe
 */
@Entity
@Table(name = "duration")
@Audited
public class Duration extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    private Date startDate;
    private Date endDate;
    private Boolean active;
    private Set<Course> courses = new HashSet<Course>();
    private CouncilDuration councilDuration;
    private boolean selected;

    @ManyToOne
    @JoinColumn
    public CouncilDuration getCouncilDuration() {

        return councilDuration;
    }

    public void setCouncilDuration(CouncilDuration councilDuration) {

        this.councilDuration = councilDuration;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getStartDate() {

        return startDate;
    }

    public void setStartDate(Date startDate) {

        this.startDate = startDate;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getEndDate() {

        return endDate;
    }

    public void setEndDate(Date endDate) {

        this.endDate = endDate;
    }

    public Boolean getActive() {

        return active;
    }

    public void setActive(Boolean active) {

        this.active = active;
    }

    @ManyToMany
    public Set<Course> getCourses() {

        return courses;
    }

    public void setCourses(Set<Course> courses) {

        this.courses = courses;
    }

    @Transient
    public String getTextStatus() {

        if (active == null) {
            return "InActive";
        }
        return active ? "Active" : "InActive";
    }

    @Transient
    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
