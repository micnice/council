package zw.co.hitrac.council.web.pages.accounts;

import zw.co.hitrac.council.web.pages.configure.*;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.PaymentType;
import zw.co.hitrac.council.business.service.accounts.PaymentTypeService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Michael Matiashe
 */
public class PaymentTypeEditPage extends IBankBookPage{
    
    @SpringBean
    private PaymentTypeService paymentTypeService;

    public PaymentTypeEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<PaymentType>(new LoadableDetachablePaymentTypeModel(id)));
        Form<PaymentType> form=new Form<PaymentType>("form",(IModel<PaymentType>)getDefaultModel()) {
            @Override
            public void onSubmit(){
                paymentTypeService.save(getModelObject());
                setResponsePage(new PaymentTypeViewPage(getModelObject().getId()));
            }
        };
        
        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior())); 
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink",PaymentTypeListPage.class));
        add(form);
    }
    
    
    private final class LoadableDetachablePaymentTypeModel extends LoadableDetachableModel<PaymentType> {

        private Long id;

        public LoadableDetachablePaymentTypeModel(Long id) {
            this.id = id;
        }

        @Override
        protected PaymentType load() {
            if(id==null){
                return new PaymentType();
            }
            return paymentTypeService.get(id);
        }
    }
    
    
}
