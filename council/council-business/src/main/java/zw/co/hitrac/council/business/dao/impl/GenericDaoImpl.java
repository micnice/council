package zw.co.hitrac.council.business.dao.impl;

import org.springframework.stereotype.Repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import zw.co.hitrac.council.business.dao.GenericDAO;

/**
 *
 * @author Charles Chigoriwa
 * @author Daniel Nkhoma
 */
@Repository
public class GenericDaoImpl implements GenericDAO {

    @PersistenceContext
    EntityManager entityManager;

    public static void main(String[] args) {

    }

    public List<Long> getAllLean(String ClassName) {
        return entityManager.createQuery("SELECT DISTINCT c.id FROM " + ClassName + " c").getResultList();
    }

    public Object getLean(String ClassName, Long id) {
        return entityManager.createQuery("SELECT DISTINCT c FROM " + ClassName + " c where c.id=:id").setParameter("id", id).getSingleResult();
    }
}
