/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.repo.examinations;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.examinations.ExamPeriod;

/**
 *
 * @author tidza
 */
public interface ExamPeriodRepository extends CrudRepository<ExamPeriod, Long> {
public List<ExamPeriod> findAll();
}
