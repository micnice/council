/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.examinations;

import java.util.List;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.domain.examinations.ExamYear;
import zw.co.hitrac.council.business.service.IGenericService;

/**
 *
 * @author tidza
 */
public interface ExamSettingService extends IGenericService<ExamSetting> {
    public List<ExamSetting> getExamSettings(Boolean active);
    public List<ExamSetting> findByExamYear(ExamYear examYear);
}
