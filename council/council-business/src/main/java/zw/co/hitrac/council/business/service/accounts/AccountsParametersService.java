
package zw.co.hitrac.council.business.service.accounts;

import zw.co.hitrac.council.business.domain.accounts.AccountsParameters;
import zw.co.hitrac.council.business.service.IGenericService;

/**
 *
 * @author Kelvin Goredema
 */
public interface AccountsParametersService extends IGenericService<AccountsParameters>{
    public AccountsParameters get();
}
