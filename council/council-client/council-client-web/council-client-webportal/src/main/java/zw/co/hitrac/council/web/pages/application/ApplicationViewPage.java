package zw.co.hitrac.council.web.pages.application;

//~--- non-JDK imports --------------------------------------------------------
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.ApplicationPurpose;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.Requirement;
import zw.co.hitrac.council.business.domain.SupportDocument;
import zw.co.hitrac.council.business.process.ApplicationProcess;
import zw.co.hitrac.council.business.service.ApplicationService;
import zw.co.hitrac.council.business.service.RegisterService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.SupportDocumentService;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.registration.ConditionConfirmationPage;
import zw.co.hitrac.council.web.pages.registration.RegistrantDocumentPage;
import zw.co.hitrac.council.web.pages.registration.RegistrantQualificationPanelList;
import zw.co.hitrac.council.web.pages.registration.RegistrantSupportingDocumentPanel;
import zw.co.hitrac.council.web.pages.registration.RegistrantViewPage;
import zw.co.hitrac.council.web.pages.registration.RequirementConfirmationPage;
import zw.co.hitrac.council.web.utility.CustomDateLabel;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;
import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;

public class ApplicationViewPage extends TemplatePage {

    @SpringBean
    private ApplicationService applicationService;
    @SpringBean
    private RegistrantService registrantService;
    private Application application;
    @SpringBean
    private SupportDocumentService supportDocumentService;
    @SpringBean
    private ApplicationProcess applicationProcess;
    @SpringBean
    private RegisterService registerService;

    public ApplicationViewPage(long id) {

        menuPanelManager.getApplicationPanel().setTopMenuCurrent(true);
        final CompoundPropertyModel<Application> model = new CompoundPropertyModel<Application>(applicationService.get(id));
        final CompoundPropertyModel<Registrant> registrantModel = new CompoundPropertyModel<Registrant>(new DetachableRegistrantModel(model.getObject().getRegistrant().getId(), registrantService));

        setDefaultModel(model);
        add(new Label("approvedBy"));
        add(new Label("processedBy"));
        add(new CustomDateLabel("dateApproved"));
        add(new Label("applicationStatus"));
        add(new Label("course"));
        add(new Label("approvedCourse"));
        add(new Label("applicationPurpose"));
        add(new Label("institution"));
        add(new CustomDateLabel("applicationDate"));
        add(new CustomDateLabel("dateProcessed"));
        add(new Label("registrant.conditions"));
        add(new Label("comment"));
        add(new Link<Void>("registrationOptionPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });
        add(new Link<Application>("applicationApprovalPage", model) {
            @Override
            public void onClick() {
                setResponsePage(new ApplicationPage(getModelObject().getId()));
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.EDUCATION_OFFICER)));
                return enabled;
            }

            @Override
            protected void onConfigure() {
                if (getModel().getObject().getApplicationStatus().equals(Application.APPLICATIONAPPROVED) || getModel().getObject().getApplicationStatus().equals(Application.APPLICATIONDECLINED)) {
                    setVisible(Boolean.FALSE);
                }
            }
        });

        add(new Link<Application>("registrantDocumentPage", model) {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantDocumentPage(getModelObject(), registrantModel));
            }
        });

        add(new Link<Application>("conditionConfirmationPage", model) {
            @Override
            public void onClick() {
                setResponsePage(new ConditionConfirmationPage(getModelObject()));
            }

            @Override
            protected void onConfigure() {
                setVisibilityAllowed(Boolean.FALSE);
            }
        });

        add(new Label("registrant.fullname"));
        add(new FeedbackPanel("feedback"));

        //supporting documents
        add(new RegistrantSupportingDocumentPanel("supportingDocumentPanel", null, model.getObject(), null, null, null, null, null));

        final Boolean isProvisional = model.getObject().getApplicationPurpose().equals(ApplicationPurpose.PROVISIONAL_REGISTRATION);

        Set<Requirement> totalRequirements = new HashSet<Requirement>();
        List<Requirement> registerRequirements = new ArrayList<Requirement>(registerService.get(applicationProcess.getApplicationRegister(model.getObject()).getId()).getRequirements());
        totalRequirements.addAll(registerRequirements);
        Set<Requirement> requirements = new HashSet<Requirement>();
        for (Requirement r : totalRequirements) {
            if (r.getGeneralRequirement()) {
                SupportDocument document = supportDocumentService.getSupportDocument(registrantModel.getObject(), r);
                if (document == null) {
                    requirements.add(r);
                }
            }
        }
        final Set<Requirement> finalRequirementsSet = new HashSet<Requirement>(requirements);

        PropertyListView<Requirement> eachItem = new PropertyListView<Requirement>("requirementsList", new ArrayList<Requirement>(finalRequirementsSet)) {
            @Override
            protected void populateItem(final ListItem<Requirement> item) {
                Link<Requirement> viewLink = new Link<Requirement>("registrantDocumentPage", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new RegistrantDocumentPage(model.getObject(), registrantModel, getModelObject()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                item.add(new Label("name"));

                Form<SupportDocument> form = new Form<SupportDocument>("form", new CompoundPropertyModel<SupportDocument>(new SupportDocument())) {
                    @Override
                    protected void onSubmit() {
                        this.getModelObject().setRegistrant(registrantModel.getObject());
                        this.getModelObject().setRequirement(item.getModelObject());
                        this.getModelObject().setSubmitted(Boolean.TRUE);
                        supportDocumentService.save(getModelObject());
                        //setResponsePage(new RequirementConfirmationPage(model.getObject().getId()));
                        setResponsePage(new ApplicationViewPage(getModelObject().getId()));
                    }

                    @Override
                    protected void onConfigure() {
                        if (getModel().getObject().getSubmitted()) {
                            setVisible(Boolean.FALSE);
                        } else {
                            if (item.getModelObject().getUpload()) {
                                setVisible(Boolean.TRUE);
                            } else {
                                setVisible(Boolean.FALSE);
                            }
                        }
                    }
                };
                form.add(new CheckBox("submitted"));
                item.add(form);
            }

            @Override
            protected void onConfigure() {
                if (!finalRequirementsSet.isEmpty()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(Boolean.FALSE);
                }
            }
        };

        add(eachItem);
        add(new RegistrantQualificationPanelList("registrantQualificationPanelList", registrantModel));

    }
}


//~ Formatted by Jindent --- http://www.jindent.com
