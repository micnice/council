/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.accounts.payments;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.domain.accounts.PaymentDetails;
import zw.co.hitrac.council.business.domain.accounts.ReceiptItem;
import zw.co.hitrac.council.business.domain.accounts.TransactionType;
import zw.co.hitrac.council.business.process.InvoiceProcess;
import zw.co.hitrac.council.business.process.PaymentProcess;
import zw.co.hitrac.council.business.service.CouncilDurationService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.service.accounts.AccountService;
import zw.co.hitrac.council.business.service.accounts.AccountsParametersService;
import zw.co.hitrac.council.business.service.accounts.CustomerService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.business.service.accounts.ReceiptItemService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.CouncilDurationListModel;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.registration.RegistrantViewPage;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;

/**
 * @author micnice
 */

public class CarryFowardConfirmationPage extends TemplatePage {

    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    AccountService accountService;
    @SpringBean
    CustomerService customerAccountService;
    @SpringBean
    RegistrationService registrationService;
    @SpringBean
    DebtComponentService debtComponentService;
    @SpringBean
    GeneralParametersService generalParametersService;
    @SpringBean
    private InvoiceProcess invoiceProcess;
    @SpringBean
    private PaymentProcess paymentProcess;
    @SpringBean
    private AccountsParametersService accountsParametersService;
    @SpringBean
    private ReceiptItemService receiptItemService;
    @SpringBean
    private CouncilDurationService councilDurationService;
    private List<DebtComponent> selectedDebtComponents = new ArrayList<DebtComponent>();

    public CarryFowardConfirmationPage(Long id, final List<DebtComponent> itemToPay) {

        this.selectedDebtComponents.addAll(itemToPay);

        final CompoundPropertyModel<Registrant> registrantModel = new CompoundPropertyModel<Registrant>(new DetachableRegistrantModel(id, registrantService));
        setDefaultModel(registrantModel);
        add(new Label("fullname"));
        final Account account = accountService.get(registrantModel.getObject().getCustomerAccount().getAccount().getId());
        add(new Label("balance", account.getBalance().toString()));

        Form<Void> form = new Form<Void>("form") {
            @Override
            protected void onSubmit() {
                try {

                    if (!selectedDebtComponents.isEmpty()) {
                        Customer customer = customerAccountService.get(registrantModel.getObject().getCustomerAccount().getId());
                        TransactionType transactionType = accountsParametersService.get().getInvoiceTransactionType();
                        paymentProcess.payThroughCarryForward(account, customer, transactionType, selectedDebtComponents);
                        List<DebtComponent> debtComponents = debtComponentService.getDebtComponents(customer.getAccount());
                        List<ReceiptItem> receiptItemList = receiptItemService.getReceiptItemsWithNoReceiptHeader(customer.getAccount());
                        PaymentDetails paymentDetails = null;
                        if (debtComponents.isEmpty() && !receiptItemList.isEmpty()) {
                            Set<ReceiptItem> receiptItems = new HashSet<ReceiptItem>();

                            if (receiptItemList.isEmpty()) {
                                setResponsePage(new RegistrantViewPage(registrantService.getRegistrant(customer).getId()));
                            } else {
                                for (ReceiptItem receiptItem : receiptItemList) {
                                    receiptItems.add(receiptItem);
                                }
                                paymentDetails = paymentProcess.accountBallancePaysAll(receiptItems, CouncilSession.get().getUser(), customer, null);

                                setResponsePage(new PaymentConfirmationPage(paymentDetails, customer.getAccount()));
                            }
                        } else if (!debtComponents.isEmpty() && !receiptItemList.isEmpty()) {
                            Set<ReceiptItem> receiptItems = new HashSet<ReceiptItem>();
                            if (receiptItemList.isEmpty()) {
                                setResponsePage(new RegistrantViewPage(registrantService.getRegistrant(customer).getId()));
                            } else {
                                for (ReceiptItem receiptItem : receiptItemList) {
                                    receiptItems.add(receiptItem);
                                }

                                paymentDetails = paymentProcess.accountBalancePaysLess(receiptItems, CouncilSession.get().getUser(), customer, null);
                            }
                            //Select the items you want to pay for changed to direct to payment confirmation page
                            setResponsePage(new PaymentConfirmationPage(paymentDetails, customer.getAccount()));

                        } else {
                            //Select the items you want to pay for changed to direct to payment confirmation page
                            setResponsePage(new PaymentConfirmationPage(paymentDetails, customer.getAccount()));
                        }
                    }
                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };


        form.add(new DebtComponentListView("debtComponents", new LoadableDetachableModel<List<? extends DebtComponent>>() {
            @Override
            protected List<? extends DebtComponent> load() {
                return selectedDebtComponents;
            }
        }));
        form.add(new FeedbackPanel("feedback"));
        add(form);

    }

    private final class DebtComponentListView extends ListView<DebtComponent> {

        public DebtComponentListView(String id) {
            super(id);
            setReuseItems(true);
        }

        public DebtComponentListView(String id, IModel<? extends List<? extends DebtComponent>> model) {
            super(id, model);
            setReuseItems(true);
        }

        public DebtComponentListView(String id, List<? extends DebtComponent> list) {
            super(id, list);
            setReuseItems(true);
        }

        @Override
        protected void populateItem(final ListItem<DebtComponent> item) {
            item.setDefaultModel(new CompoundPropertyModel<DebtComponent>(item.getModelObject()));
            item.add(new Label("remainingBalance"));
            item.add(new DropDownChoice("paymentPeriod", new CouncilDurationListModel(councilDurationService)));
            item.add(new Label("transactionComponent.account.name"));
        }
    }
}


