
package zw.co.hitrac.council.business.dao;

import zw.co.hitrac.council.business.domain.PaymentConfiguration;

/**
 *
 * @author  Kelvin Goredema
 */
public interface PaymentConfigurationDAO extends IGenericDAO<PaymentConfiguration> {
    
}
