
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.MisconductType;
import zw.co.hitrac.council.business.service.MisconductTypeService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author hitrac
 */
public class MisconductTypeEditPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private MisconductTypeService misconductTypeService;

    public MisconductTypeEditPage(Long id) {
        setDefaultModel(
            new CompoundPropertyModel<MisconductType>(
                new MisconductTypeEditPage.LoadableDetachableMisconductTypeModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));

        Form<MisconductType> form = new Form<MisconductType>("form", (IModel<MisconductType>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                misconductTypeService.save(getModelObject());
                setResponsePage(new MisconductTypeViewPage(getModelObject().getId()));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink", MisconductTypeListPage.class));
        add(form);
    }

    private final class LoadableDetachableMisconductTypeModel extends LoadableDetachableModel<MisconductType> {
        private Long id;

        public LoadableDetachableMisconductTypeModel(Long id) {
            this.id = id;
        }

        @Override
        protected MisconductType load() {
            if (id == null) {
                return new MisconductType();
            }

            return misconductTypeService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
