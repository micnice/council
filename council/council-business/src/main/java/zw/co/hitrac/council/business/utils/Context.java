package zw.co.hitrac.council.business.utils;

import java.text.SimpleDateFormat;
import java.util.Locale;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Context {

    private static final Log log = LogFactory.getLog(Context.class);
    
    public static Locale getLocale() {
            return LocaleUtility.getDefaultLocale();
        }

    public static SimpleDateFormat getDateFormat() {
        return Util.getDateFormat(getLocale());
    }

}
