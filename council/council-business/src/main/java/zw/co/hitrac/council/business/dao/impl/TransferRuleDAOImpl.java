package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.TransferRuleDAO;
import zw.co.hitrac.council.business.dao.repo.GeneralParametersRepository;
import zw.co.hitrac.council.business.dao.repo.TransferRuleRepository;
import zw.co.hitrac.council.business.domain.GeneralParameters;
import zw.co.hitrac.council.business.domain.TransferRule;

/**
 *
 * @author Michael Matiashe
 */
@Repository
public class TransferRuleDAOImpl implements TransferRuleDAO {

    @Autowired
    private TransferRuleRepository transferRuleRepository;

    public TransferRule save(TransferRule transferRule) {
        return transferRuleRepository.save(transferRule);
    }

    public List<TransferRule> findAll() {
        return transferRuleRepository.findAll();
    }

    public TransferRule get(Long id) {
        return transferRuleRepository.findOne(id);
    }

    /**
     * A setter method that will make mocking repo object easier
     * @param transferRuleRopository 
     */
    public void setTransferRuleRepository(TransferRuleRepository transferRuleRepository) {
        this.transferRuleRepository = transferRuleRepository;
    }

    public List<TransferRule> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
}
