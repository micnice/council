/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.accounts.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.accounts.ProductPriceDAO;
import zw.co.hitrac.council.business.domain.accounts.ProductPrice;
import zw.co.hitrac.council.business.service.accounts.ProductPriceService;

import java.util.List;

/**
 *
 * @author tidza
 */
@Service
@Transactional
public class ProductPriceServiceImpl implements ProductPriceService {

    @Autowired
    private ProductPriceDAO productPriceDAO;
    
    @Transactional
    @Override
    public ProductPrice save(ProductPrice t) {
        return productPriceDAO.save(t);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ProductPrice> findAll() {
        return productPriceDAO.findAll();
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public ProductPrice get(Long id) {
        return productPriceDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ProductPrice> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setProductPriceDao(ProductPriceDAO productPriceDAO) {

        this.productPriceDAO = productPriceDAO;
    }

}
