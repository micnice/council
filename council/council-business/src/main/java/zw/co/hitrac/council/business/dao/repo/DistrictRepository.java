package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.District;

/**
 *
 * @author Constance Mabaso
 */
public interface DistrictRepository extends CrudRepository<District, Long> {

    public List<District> findAll();
    
}
