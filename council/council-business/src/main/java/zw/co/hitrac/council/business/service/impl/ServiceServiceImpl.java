/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.ServiceDAO;
import zw.co.hitrac.council.business.domain.Service;
import zw.co.hitrac.council.business.service.ServiceService;

import java.util.List;

//import org.springframework.stereotype.Service;

/**
 *
 * @author tidza
 */
@org.springframework.stereotype.Service
@Transactional
public class ServiceServiceImpl implements ServiceService {

    @Autowired
    private ServiceDAO serviceDao;

    @Transactional
    public Service save(Service t) {
        return serviceDao.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Service> findAll() {
        return serviceDao.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Service get(Long id) {
        return serviceDao.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Service> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setServiceDAO(ServiceDAO serviceDAO) {

        this.serviceDao = serviceDAO;
    }
}
