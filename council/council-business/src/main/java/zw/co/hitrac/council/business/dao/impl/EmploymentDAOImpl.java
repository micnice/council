package zw.co.hitrac.council.business.dao.impl;

import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.EmploymentDAO;
import zw.co.hitrac.council.business.dao.repo.EmploymentRepository;
import zw.co.hitrac.council.business.domain.City;
import zw.co.hitrac.council.business.domain.District;
import zw.co.hitrac.council.business.domain.Employment;
import zw.co.hitrac.council.business.domain.EmploymentArea;
import zw.co.hitrac.council.business.domain.EmploymentStatus;
import zw.co.hitrac.council.business.domain.EmploymentType;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Post;
import zw.co.hitrac.council.business.domain.Province;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;

/**
 * @author tidza
 */
@Repository
public class EmploymentDAOImpl implements EmploymentDAO {

    @Autowired
    private EmploymentRepository employmentRepository;
    @PersistenceContext
    EntityManager entityManager;

    public Employment save(Employment t) {
        return employmentRepository.save(t);
    }

    public List<Employment> findAll() {
        return employmentRepository.findAll();
    }

    public Employment get(Long id) {
        return employmentRepository.findOne(id);
    }

    public void setEmploymentRepository(EmploymentRepository employmentRepository) {
        this.employmentRepository = employmentRepository;
    }

    public List<Employment> getEmployments(Registrant registrant) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Employment.class);
        criteria.add(Restrictions.eq("registrant", registrant));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    //code to get current Employment Status - Registration
    public EmploymentType getRegistrantEmployment(Registrant registrant) {

        List<Employment> emps = entityManager.createQuery("select e from Employment e left join fetch e.registrant where e.registrant=:registrant and e.active=TRUE").setParameter("registrant", registrant).getResultList();
       
        if (emps.isEmpty()) {
            return null;
        }
        if (emps.get(0).getEmploymentType() == null) {
            return null;
        }
        return emps.get(0).getEmploymentType();
    }

    public List<Employment> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<RegistrantData> getEmployments(String searchTerm, Province province, District district, EmploymentType employmentType, EmploymentArea employmentArea, Institution institution, EmploymentStatus employmentStatus, City city, Post post,
                                               Date startDate, Date endDate, Boolean active) {

        StringBuilder sb = new StringBuilder("");
        
        sb.append("select DISTINCT new zw.co.hitrac.council.business.domain.reports.RegistrantData(e.registrant.id,e.registrant.firstname,e.registrant.lastname, e.registrant.middlename,e.registrant.birthDate, e.registrant.gender,e.registrant.idNumber,e.registrant.registrationNumber) from Employment e where e.id !=null");
        
        if (!StringUtils.isEmpty(searchTerm)) {
            sb.append(" and e.employer like =:employer");
        }

        if (province != null) {
            sb.append(" and e.institution.district.province=:province ");
        }

        if (district != null) {
            sb.append(" and e.institution.district=:district ");
        }

        if (institution != null) {
            sb.append(" and e.institution=:institution ");
        }

        if (employmentType != null) {
            sb.append(" and e.employmentType=:employmentType ");
        }

        if (employmentArea != null) {
            sb.append(" and e.employmentArea=:employmentArea ");
        }

        if (institution != null) {
            sb.append(" and e.institution=:institution ");
        }

        if (employmentStatus != null) {
            sb.append(" and e.employmentStatus=:employmentStatus ");
        }

        if (city != null) {
            sb.append(" and e.city=:city ");
        }

        if (post != null) {
            sb.append(" and e.post=:post ");
        }

        if (startDate != null) {
            sb.append(" and e.startDate=:startDate ");
        }

        if (endDate != null) {
            sb.append(" and e.endDate=:endDate ");
        }

        if (active != null) {
            sb.append(" and e.active=:active ");
        }

        Query query = entityManager.createQuery(sb.toString());

        if (!StringUtils.isEmpty(searchTerm)) {
            query.setParameter("employer", searchTerm);
        }


        if (province != null) {
            query.setParameter("province", province);
        }

        if (district != null) {
            query.setParameter("district", district);
        }

        if (employmentType != null) {
            query.setParameter("employmentType", employmentType);
        }

        if (employmentArea != null) {
            query.setParameter("employmentArea", employmentArea);
        }

        if (institution != null) {
            query.setParameter("institution", institution);
        }

        if (employmentStatus != null) {
            query.setParameter("employmentStatus", employmentStatus);
        }

        if (city != null) {
            query.setParameter("city", city);
        }

        if (post != null) {
            query.setParameter("post", post);
        }

        if (startDate != null) {
            query.setParameter("startDate", startDate);
        }

        if (endDate != null) {
            query.setParameter("endDate", endDate);
        }

        if (active != null) {
            query.setParameter("active", active);
        }


        return query.getResultList();

    }

    //current employed place
    public Employment getCurrentEmployed(Registrant registrant) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Employment.class);
        criteria.add(Restrictions.eq("registrant", registrant));
        criteria.add(Restrictions.eq("active", Boolean.TRUE));
        List<Employment> emps = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        if (emps.isEmpty()) {
            return null;
        } else {
            return emps.get(0);
        }
    }
}
