/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.examinations;

import java.util.List;
import zw.co.hitrac.council.business.dao.IGenericDAO;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.examinations.OldExamResult;

/**
 *
 * @author tidza
 */
public interface OldExamResultDao extends IGenericDAO<OldExamResult> {
    public List<OldExamResult> examResults(Registrant registrant);
}
