/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.registration;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.Requirement;
import zw.co.hitrac.council.business.domain.SupportDocument;
import zw.co.hitrac.council.business.service.RequirementService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegisterService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.SupportDocumentService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author tdhlakama
 */
public class RequirementSelectPage extends TemplatePage {

    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private SupportDocumentService supportDocumentService;
    @SpringBean
    private RequirementService requirementService;
    @SpringBean
    private RegistrantService registrantService;
    private Requirement requirement;
    @SpringBean
    private RegisterService registerService;

    public RequirementSelectPage(final Model<Registrant> registrantModel) {
        PropertyModel<Requirement> requirementModel = new PropertyModel<Requirement>(this, "requirement");
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());
        add(new Label("fullname", registrantModel.getObject().getFullname()));
        // Create feedback panels
        add(new FeedbackPanel("feedback"));

        Form<?> form = new Form("form");
        form.add(new DropDownChoice("requirement", requirementModel, requirementService.findAll()).setRequired(true).add(new ErrorBehavior()));

        form.add(new Button("upload") {
            @Override
            public void onSubmit() {
                setResponsePage(new RegistrantDocumentPage(requirement, registrantModel));
            }
        });

        add(form);

        add(new Link<Registrant>("registrationOptionPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });

    }    
}