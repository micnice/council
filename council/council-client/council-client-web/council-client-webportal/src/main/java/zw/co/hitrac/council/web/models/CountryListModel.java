
package zw.co.hitrac.council.web.models;

import java.util.List;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.Country;
import zw.co.hitrac.council.business.service.CountryService;
import zw.co.hitrac.council.business.utils.HrisComparator;

/**
 *
 * @author kelvin
 */
public class CountryListModel extends LoadableDetachableModel<List<Country>> {

    private final CountryService countryService;
    private HrisComparator hrisComparator = new HrisComparator();

    public CountryListModel(CountryService countryService) {
        this.countryService = countryService;
    }

    @Override
    protected List<Country> load() {
        return (List<Country>) hrisComparator.sort(countryService.findAll());
    }
}
