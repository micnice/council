/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.accounts.payments;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.domain.accounts.ReceiptHeader;
import zw.co.hitrac.council.business.domain.accounts.ReceiptItem;
import zw.co.hitrac.council.business.domain.accounts.TransactionType;
import zw.co.hitrac.council.business.domain.reports.ProductItem;
import zw.co.hitrac.council.business.domain.reports.ProductSale;
import zw.co.hitrac.council.business.service.accounts.CustomerService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.business.service.accounts.PaymentDetailsService;
import zw.co.hitrac.council.business.service.accounts.PaymentMethodService;
import zw.co.hitrac.council.business.service.accounts.ProductService;
import zw.co.hitrac.council.business.service.accounts.ReceiptHeaderService;
import zw.co.hitrac.council.business.service.accounts.ReceiptItemService;
import zw.co.hitrac.council.business.service.accounts.TransactionTypeService;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.reports.ProductItemReport;
import zw.co.hitrac.council.reports.SaleBankReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.BankAccountsModel;
import zw.co.hitrac.council.web.pages.accounts.IAccountingPage;
import zw.co.hitrac.council.web.utility.DatePickerUtil;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 *
 * @author tdhlakama
 */
public class SaleByBankDepositPage extends IAccountingPage {

    private Date startDate, endDate;
    private TransactionType transactionType;
    @SpringBean
    ProductService productService;
    @SpringBean
    ReceiptHeaderService receiptHeaderService;
    @SpringBean
    DebtComponentService debtComponentService;
    @SpringBean
    private CustomerService customerService;
    @SpringBean
    PaymentDetailsService paymentDetailsService;
    @SpringBean
    PaymentMethodService paymentMethodService;
    @SpringBean
    TransactionTypeService transactionTypeService;
    @SpringBean
    ReceiptItemService receiptItemService;
    private HrisComparator hrisComparator = new HrisComparator();

    public SaleByBankDepositPage() {
        PropertyModel<Date> startDateModel = new PropertyModel<Date>(this, "startDate");
        PropertyModel<Date> endDateModel = new PropertyModel<Date>(this, "endDate");
        PropertyModel<TransactionType> transactionTypeModel = new PropertyModel<TransactionType>(this, "transactionType");

        Form<?> form = new Form("form");
        form.add(new TextField<Date>("startDate", startDateModel).add(DatePickerUtil.getDatePicker()));
        form.add(new TextField<Date>("endDate", endDateModel).add(DatePickerUtil.getDatePicker()));
        form.add(new DropDownChoice("transactionType", transactionTypeModel, new BankAccountsModel(transactionTypeService)).setRequired(true).add(new ErrorBehavior()));

        add(form);
        add(new FeedbackPanel("feedback"));
        form.add(new Button("printBankCashReport") {
            @Override
            public void onSubmit() {

                List<ProductSale> productSales = new ArrayList<ProductSale>();
                if (startDate != null && endDate == null) {
                    endDate = new Date();
                }
                List<ReceiptHeader> receiptHeaders = receiptHeaderService.getReceiptHeadersDeposit(null, null, null, startDate, endDate, null, transactionType, Boolean.FALSE);
                for (ReceiptHeader r : receiptHeaders) {
                    ProductSale productSale = new ProductSale();
                    productSale.setPaymentDetail(r.getPaymentDetails());
                    productSale.setAmountPaid(r.getTotalAmountPaid());
                    productSale.setCarryForward(r.getCarryForward());
                    //If properly configured depositDateRequired - reflects a bank transcation
                    final Registrant registrant = customerService.getRegistrant(r.getPaymentDetails().getCustomer().getAccount());
                    if (registrant != null) {
                        productSale.setRegistrant(registrant);
                    } else {
                        Institution institution = customerService.getInstitution(r.getPaymentDetails().getCustomer().getAccount());
                        productSale.setInstitution(institution);
                    }
                    productSales.add(productSale);
                }
                String commnet = "Bank Deposits ";
                if (startDate != null && endDate != null) {
                    commnet = commnet.concat("From - " + DateUtil.getDate(startDate) + " to " + DateUtil.getDate(endDate));
                }
                printBankReport(productSales, commnet, transactionType.getName());

            }
        });

        form.add(new Button("print") {
            @Override
            public void onSubmit() {
                try {
                    Set<ProductItem> productSales = new HashSet<ProductItem>();

                    if (startDate != null && endDate == null) {
                        endDate = new Date();
                    }

                    for (Product p : receiptItemService.getDistinctProductsInSales()) {
                        ProductItem productItem = new ProductItem();
                        productItem.setProduct(p);
                        productItem.setEndDate(endDate);
                        productItem.setStartDate(startDate);
                        BigDecimal totalPoints = new BigDecimal(0);
                        totalPoints = totalPoints.add(receiptItemService.getReceiptItemsDepositTotal(p, startDate, endDate, transactionType, null));
                        productItem.setAmountPaid(totalPoints);
                        productSales.add(productItem);
                    }

                    ProductItemReport productItemReport = new ProductItemReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();
                    //parameters.put("carryForward",  carryForwad.toString() + " *CF");
                    String commnet = "";
                    if (startDate != null && endDate != null) {
                        commnet = "Bank * " + transactionType + " * Sales Done form From - " + DateUtil.getDate(startDate) + " to " + DateUtil.getDate(endDate);
                    }
                    parameters.put("comment", commnet);
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(productItemReport,
                            contentType, hrisComparator.sortProductSale(new ArrayList<ProductItem>(productSales)), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    private void printBankReport(List<ProductSale> productSales, String comment, String bank) {
        try {
            SaleBankReport saleBankReport = new SaleBankReport();
            ContentType contentType = ContentType.PDF;
            Map parameters = new HashMap();

            parameters.put("comment", comment);
            parameters.put("bank", bank);
            ByteArrayResource resource = ReportResourceUtils.getReportResource(saleBankReport,
                    contentType, productSales, parameters);
            IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                    RequestCycle.get().getResponse(), null);

            resource.respond(a);

            // To make Wicket stop processing form after sending response
            RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
        } catch (JRException ex) {
            ex.printStackTrace();
        }
    }
}
