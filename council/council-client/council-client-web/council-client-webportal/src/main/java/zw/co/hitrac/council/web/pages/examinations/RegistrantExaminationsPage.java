
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.service.accounts.AccountService;
import zw.co.hitrac.council.business.service.accounts.CustomerService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationService;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;

/**
 *
 * @author tdhlakama
 */
public class RegistrantExaminationsPage extends IExaminationsPage {
    @SpringBean
    private RegistrantService       registrantService;
    @SpringBean
    AccountService                  accountService;
    @SpringBean
    CustomerService                 customerAccountService;
    @SpringBean
    RegistrationService             registrationService;
    @SpringBean
    DebtComponentService            debtComponentService;
    @SpringBean
    private ExamRegistrationService examRegistrationService;

    public RegistrantExaminationsPage(ExamRegistration examRegistration) {
        this(examRegistration.getRegistration().getId());
    }

    public RegistrantExaminationsPage(final IModel<Registrant> registrantModel) {
        final CompoundPropertyModel<Registrant> model =
            new CompoundPropertyModel<Registrant>(new DetachableRegistrantModel(registrantModel.getObject().getId(),
                registrantService));

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(model.getObject().getId());
        setDefaultModel(model);
        add(new Label("registrant.fullname").setVisible(false));
        add(new Label("fullname"));

        // - Used to Display a to list Registration Examinations
        final IModel<List<ExamRegistration>> model2 = new LoadableDetachableModel<List<ExamRegistration>>() {
            @Override
            protected List<ExamRegistration> load() {
                return examRegistrationService.getExamRegistrations(registrantModel.getObject());
            }
        };

        // Exam Registration List Data View Panel
        add(new ExamCandidatesDataViewPanel("examRegistrationDataListPanel", model2));
    }

    public RegistrantExaminationsPage(Long id) {
        final CompoundPropertyModel<Registration> model =
            new CompoundPropertyModel<Registration>(
                new RegistrantExaminationsPage.LoadableDetachableRegistrationModel(id));

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);

        final Long registrantId = model.getObject().getRegistrant().getId();

        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantId);
        setDefaultModel(model);
        add(new Label("registrant.fullname"));
        add(new Label("fullname").setVisible(false));

        // - Used to Display a to list Registration Examinations
        final IModel<List<ExamRegistration>> model2 = new LoadableDetachableModel<List<ExamRegistration>>() {
            @Override
            protected List<ExamRegistration> load() {
                return examRegistrationService.getExamRegistrations(model.getObject());
            }
        };

        // Exam Registration List Data View Panel
        add(new ExamCandidatesDataViewPanel("examRegistrationDataListPanel", model2));
    }

    public RegistrantExaminationsPage(PageParameters parameters) {
        this(parameters.get("registrantId").toLong());
    }

    private final class LoadableDetachableRegistrationModel extends LoadableDetachableModel<Registration> {
        private Long id;

        public LoadableDetachableRegistrationModel(Long id) {
            this.id = id;
        }

        @Override
        protected Registration load() {
            return registrationService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
