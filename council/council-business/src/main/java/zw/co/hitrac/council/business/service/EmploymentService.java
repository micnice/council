/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service;

import java.util.Date;
import java.util.List;
import zw.co.hitrac.council.business.domain.City;
import zw.co.hitrac.council.business.domain.District;
import zw.co.hitrac.council.business.domain.Employment;
import zw.co.hitrac.council.business.domain.EmploymentArea;
import zw.co.hitrac.council.business.domain.EmploymentStatus;
import zw.co.hitrac.council.business.domain.EmploymentType;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Post;
import zw.co.hitrac.council.business.domain.Province;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;

/**
 *
 * @author tidza
 */
public interface EmploymentService extends IGenericService<Employment> {
    List<Employment> getEmployments(Registrant registrant);
    
    public EmploymentType getRegistrantEmployment(Registrant registrant);
    
    List<RegistrantData> getEmployments(String searchTerm, Province province, District district, EmploymentType employmentType, EmploymentArea employmentArea, Institution institution, EmploymentStatus employmentStatus, City city, Post post,
                                        Date startDate, Date endDate, Boolean active);
     
     public Employment getCurrentEmployed(Registrant registrant);
}
