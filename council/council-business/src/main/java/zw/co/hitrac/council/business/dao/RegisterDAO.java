/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao;

import java.util.List;
import zw.co.hitrac.council.business.domain.Register;

/**
 *
 * @author kelvin
 */
public interface RegisterDAO extends IGenericDAO<Register> {

    public List<Register> getRegisterDisciplines();
}
