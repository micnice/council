package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.examinations.ExamPeriod;
import zw.co.hitrac.council.business.service.examinations.ExamPeriodService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.configure.AdministerDatabasePage;

/**
 *
 * @author Constance Mabaso
 */
public class ExamPeriodEditPage extends IExaminationsPage {
    @SpringBean
    private ExamPeriodService examPeriodService;

    public ExamPeriodEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<ExamPeriod>(new LoadableDetachableExamPeriodModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));

        Form<ExamPeriod> form = new Form<ExamPeriod>("form", (IModel<ExamPeriod>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                examPeriodService.save(getModelObject());
                setResponsePage(new ExamPeriodViewPage(getModelObject().getId()));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink", ExamPeriodListPage.class));
        add(form);
    }

    private final class LoadableDetachableExamPeriodModel extends LoadableDetachableModel<ExamPeriod> {
        private Long id;

        public LoadableDetachableExamPeriodModel(Long id) {
            this.id = id;
        }

        @Override
        protected ExamPeriod load() {
            if (id == null) {
                return new ExamPeriod();
            }

            return examPeriodService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
