/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.repo;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.RegisterType;

/**
 *
 * @author Kelvin Goredema
 */
public interface RegisterTypeRepository extends CrudRepository<RegisterType,Long> {
      public List<RegisterType> findAll();  
}
