package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import java.util.Arrays;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.QualificationType;

import zw.co.hitrac.council.business.domain.Requirement;
import zw.co.hitrac.council.business.service.QualificationService;
import zw.co.hitrac.council.business.service.RequirementService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.QualificationListModel;

/**
 *
 * @author Michael Matiashe
 */
public class RequirementEditPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private RequirementService requirementService;
    @SpringBean
    private QualificationService qualificationService;

    public RequirementEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<Requirement>(new LoadableDetachableRequirementModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));

        Form<Requirement> form = new Form<Requirement>("form", (IModel<Requirement>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                requirementService.save(getModelObject());
                setResponsePage(new RequirementViewPage(getModelObject().getId()));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new CheckBox("generalRequirement"));
        form.add(new CheckBox("examRequirement"));
        form.add(new DropDownChoice("qualificationType", Arrays.asList(QualificationType.values())));
        form.add(new BookmarkablePageLink<Void>("returnLink", RequirementListPage.class));
        add(form);
    }

    private final class LoadableDetachableRequirementModel extends LoadableDetachableModel<Requirement> {
        private Long id;

        public LoadableDetachableRequirementModel(Long id) {
            this.id = id;
        }

        @Override
        protected Requirement load() {
            if (id == null) {
                return new Requirement();
            }

            return requirementService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
