package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Country;
import zw.co.hitrac.council.business.service.CountryService;

/**
 *
 * @author charlesc
 */
public class CountryViewPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private CountryService countryService;

    public CountryViewPage(Long id) {
        CompoundPropertyModel<Country> model = new CompoundPropertyModel<Country>(new LoadableDetachableCountryModel(id));
        setDefaultModel(model);
        add(new Link<Country>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new CountryEditPage(getModelObject().getId(), getPageReference()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", CountryListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("country", model.bind("name")));
        add(new Label("isoCode1"));
        add(new Label("isoCode2"));
        add(new Label("iana"));
        add(new Label("ioc"));
        add(new Label("itu"));
        add(new Label("un"));
        add(new Label("iso"));
    }

    private final class LoadableDetachableCountryModel extends LoadableDetachableModel<Country> {

        private Long id;

        public LoadableDetachableCountryModel(Long id) {
            this.id = id;
        }

        @Override
        protected Country load() {

            return countryService.get(id);
        }
    }
}
