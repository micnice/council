package zw.co.hitrac.council.business.domain;


/**
 *
 * @author Matiashe Michael
 */
public enum InstitutionCategory {
    
    PUBLIC("Public"),
    PRIVATE("Private");

    private InstitutionCategory(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
    private final String name;
}
