package zw.co.hitrac.council.web.models;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.wicket.injection.Injector;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.integration.Employee;
import zw.co.hitrac.council.business.domain.integration.EmployeeDisciplinary;
import zw.co.hitrac.council.business.service.GeneralParametersService;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author tdhlakama
 */
public class EmployeeDisciplinaryModel extends LoadableDetachableModel<List<EmployeeDisciplinary>> {

    @SpringBean
    private GeneralParametersService generalParametersService;
    private final CompoundPropertyModel<Registrant> registrantModel;


    public EmployeeDisciplinaryModel(CompoundPropertyModel<Registrant> registrantModel) {
        this.registrantModel = registrantModel;
        Injector.get().inject(this);
    }

    @Override
    protected List<EmployeeDisciplinary> load() {

        List<EmployeeDisciplinary> employeeDisciplinaryList = new ArrayList<>();
        Registrant registrant = registrantModel.getObject();
        final String id = registrant.getFormattedIDNumber();

        try {

            if (!generalParametersService.get().isIntegrationAllowed()) {
                return employeeDisciplinaryList;
            }

            URL url = new URL(""+id);
            // read from the URL
            Scanner scan = new Scanner(url.openStream());
            String str = new String();
            while (scan.hasNext())
                str += scan.nextLine();
            scan.close();


            Type listType = new TypeToken<List<EmployeeDisciplinary>>() {
            }.getType();

            employeeDisciplinaryList = new Gson().fromJson(str, listType);
        } catch (MalformedURLException e) {
        } catch (IOException e) {

        }

        return employeeDisciplinaryList;

    }

}
