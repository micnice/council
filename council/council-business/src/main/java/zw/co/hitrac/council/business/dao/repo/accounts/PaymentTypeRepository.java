
package zw.co.hitrac.council.business.dao.repo.accounts;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.accounts.PaymentType;


/**
 *
 * @author Michael Matiashe
 */
public interface PaymentTypeRepository extends CrudRepository<PaymentType, Long> {

    public List<PaymentType> findAll();
}
