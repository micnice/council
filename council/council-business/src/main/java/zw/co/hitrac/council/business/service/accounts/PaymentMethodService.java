package zw.co.hitrac.council.business.service.accounts;

import java.util.List;
import zw.co.hitrac.council.business.domain.accounts.PaymentMethod;
import zw.co.hitrac.council.business.service.IGenericService;

/**
 *
 * @author Michael Matiashe
 *
 */
public interface PaymentMethodService extends IGenericService<PaymentMethod> {
}
