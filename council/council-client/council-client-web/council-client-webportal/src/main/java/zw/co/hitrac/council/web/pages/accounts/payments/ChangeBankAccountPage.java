package zw.co.hitrac.council.web.pages.accounts.payments;

//~--- non-JDK imports --------------------------------------------------------
import zw.co.hitrac.council.web.pages.accounts.IAccountingPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.ReceiptHeader;
import zw.co.hitrac.council.business.process.PaymentProcess;

import zw.co.hitrac.council.business.service.ModulePaperService;
import zw.co.hitrac.council.business.service.accounts.ReceiptHeaderService;
import zw.co.hitrac.council.business.service.accounts.TransactionTypeService;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.BankAccountsModel;

/**
 *
 * @author Michael Matiashe
 */
public class ChangeBankAccountPage extends IAccountingPage {

    @SpringBean
    private ReceiptHeaderService receiptHeaderService;
    @SpringBean
    ModulePaperService modulePaperService;
    @SpringBean
    private TransactionTypeService transactionTypeService;
    @SpringBean
    private PaymentProcess paymentProcess;

    public ChangeBankAccountPage(final ReceiptHeader receiptHeader) {
        CompoundPropertyModel<ReceiptHeader> model
                = new CompoundPropertyModel<ReceiptHeader>(
                        new ChangeBankAccountPage.LoadableDetachableReceiptHeaderModel(receiptHeader.getId()));

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);

        final Long paymentId = model.getObject().getPaymentDetails().getId();
        model.getObject().setTransactionTypeChange(receiptHeader.getTransactionType());
        setDefaultModel(model);

        Form<ReceiptHeader> form = new Form<ReceiptHeader>("form", (IModel<ReceiptHeader>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                getModelObject().setModifiedBy(CouncilSession.get().getUser());
                paymentProcess.getChangeTransactionType(receiptHeader.getTransactionTypeChange(), getModelObject());
                setResponsePage(new TransactionDetailHistory(paymentId));
            }
        };
        form.add(new TextArea<String>("comment").setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("transactionType", new BankAccountsModel(transactionTypeService)).setRequired(true).add(new ErrorBehavior()));

        form.add(new Link<Void>("transactionDetailHistory") {
            @Override
            public void onClick() {
                setResponsePage(new TransactionDetailHistory(paymentId));
            }
        });
        form.add(new Label("transactionTypeChange"));
        add(form);
        add(new Label("id"));
        add(new FeedbackPanel("feedback"));
    }

    private final class LoadableDetachableReceiptHeaderModel extends LoadableDetachableModel<ReceiptHeader> {

        private Long id;

        public LoadableDetachableReceiptHeaderModel(Long id) {
            this.id = id;
        }

        @Override
        protected ReceiptHeader load() {
            return receiptHeaderService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
