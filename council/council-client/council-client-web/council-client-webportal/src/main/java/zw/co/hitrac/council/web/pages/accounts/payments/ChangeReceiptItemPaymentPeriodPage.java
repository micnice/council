package zw.co.hitrac.council.web.pages.accounts.payments;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.ReceiptHeader;
import zw.co.hitrac.council.business.domain.accounts.ReceiptItem;
import zw.co.hitrac.council.business.service.CouncilDurationService;
import zw.co.hitrac.council.business.service.accounts.ReceiptItemService;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.CouncilDurationListModel;
import zw.co.hitrac.council.web.pages.accounts.IAccountingPage;
import zw.co.hitrac.council.web.pages.examinations.IExaminationsPage;

/**
 * @author Michael Matiashe
 */
public class ChangeReceiptItemPaymentPeriodPage extends IAccountingPage {

    @SpringBean
    private ReceiptItemService receiptItemService;
    @SpringBean
    private CouncilDurationService councilDurationService;

    public ChangeReceiptItemPaymentPeriodPage(final Long id) {
        CompoundPropertyModel<ReceiptItem> model
                = new CompoundPropertyModel<>(new ChangeReceiptItemPaymentPeriodPage.LoadableDetachableReceiptItemModel(id));

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);

        setDefaultModel(model);

        Form<ReceiptItem> form = new Form<ReceiptItem>("form", (IModel<ReceiptItem>) getDefaultModel()) {
            @Override
            public void onSubmit() {

                ReceiptItem receiptItem = model.getObject();
                receiptItemService.save(receiptItem);
                ReceiptHeader receiptHeader = receiptItem.getReceiptHeader();
                setResponsePage(new TransactionDetailHistory(receiptHeader.getPaymentDetails().getId()));
            }
        };
        form.add(new DropDownChoice("paymentPeriod", new CouncilDurationListModel(councilDurationService)).setRequired(true).add(new ErrorBehavior()));
        form.add(new Label("debtComponent.product.name"));
        add(form);
        add(new FeedbackPanel("feedback"));
    }

    private final class LoadableDetachableReceiptItemModel extends LoadableDetachableModel<ReceiptItem> {

        private Long id;

        public LoadableDetachableReceiptItemModel(Long id) {
            this.id = id;
        }

        @Override
        protected ReceiptItem load() {
            return receiptItemService.get(id);
        }
    }
}
