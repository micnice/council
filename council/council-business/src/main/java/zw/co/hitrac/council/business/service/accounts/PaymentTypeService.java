
package zw.co.hitrac.council.business.service.accounts;
import zw.co.hitrac.council.business.domain.accounts.PaymentType;
import zw.co.hitrac.council.business.service.IGenericService;

/**
 *
 * @author Michael Matiashe

 */
public interface PaymentTypeService extends IGenericService<PaymentType> {
}
