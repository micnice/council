package zw.co.hitrac.council.web.pages.security;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.validation.EqualPasswordInputValidator;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.User;
import zw.co.hitrac.council.business.service.UserService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

//~--- JDK imports ------------------------------------------------------------

import zw.co.hitrac.council.web.pages.HomePage;
import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author Charles Chigoriwa
 */
public class ChangePasswordPage extends TemplatePage {

    @SpringBean
    UserService userService;

    public ChangePasswordPage(Long id) {
        menuPanelManager.getChangePasswordMenuPanel().setTopMenuCurrent(true);
        
        setDefaultModel(new CompoundPropertyModel<User>(new ChangePasswordPage.LoadableDetachableUserModel(id)));
            
        PasswordTextField password = new PasswordTextField("password");
        PasswordTextField confirmPassword = new PasswordTextField("confirmPassword" , new Model(""));

        Form<User> form = new Form<User>("form", (IModel<User>) getDefaultModel()) {
            @Override
            
            public void onSubmit() {
                userService.save(getModelObject());
                setResponsePage(new UserViewPage(this.getModelObject().getId()));
            }
        };
        add(form);
        form.add(password.setRequired(true).add(new ErrorBehavior()));
        form.add(confirmPassword.setRequired(true).add(new ErrorBehavior()));
        form.add(new EqualPasswordInputValidator(password,confirmPassword));
        add(new FeedbackPanel("feedback"));
        form.add(new BookmarkablePageLink<Void>("returnLink", HomePage.class));
    }
        

    private final class LoadableDetachableUserModel extends LoadableDetachableModel<User> {

        private Long id;

        public LoadableDetachableUserModel(Long id) {
            this.id = id;
        }

        @Override
        protected User load() {
            if (id == null) {
                return new User();
            } else {
                return userService.get(id);
            }
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
