
package zw.co.hitrac.council.business.service.accounts;

import java.util.List;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.accounts.PaymentDetails;
import zw.co.hitrac.council.business.service.IGenericService;

/**
 *
 * @author Michael Matiashe
 */
public interface PaymentDetailsService extends IGenericService<PaymentDetails> {
    public List<PaymentDetails> transactionHistory(Customer customer);
    public List<PaymentDetails> getUnprintedPaymentDetails(Customer customer);
    public List<PaymentDetails> findByUid(String uuid);
}
