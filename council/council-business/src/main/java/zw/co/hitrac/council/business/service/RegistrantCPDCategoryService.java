package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.RegistrantCPDCategory;

/**
 *
 * @author Constance Mabaso
 */
public interface RegistrantCPDCategoryService extends IGenericService<RegistrantCPDCategory> {
    
    
}
