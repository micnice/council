/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao;

import java.util.List;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantDisciplinary;
import zw.co.hitrac.council.business.domain.RegistrantQualification;
import zw.co.hitrac.council.business.domain.Requirement;
import zw.co.hitrac.council.business.domain.SupportDocument;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;

/**
 *
 * @author kelvin
 */
public interface SupportDocumentDAO extends IGenericDAO<SupportDocument> {

    List<SupportDocument> getSupportDocuments(Registrant registrant, Application application, Requirement requirement, RegistrantDisciplinary registrantDisciplinary, RegistrantQualification registrantQualification, ExamRegistration examRegistration, Institution institution);

    public SupportDocument getSupportDocument(Registrant registrant, Requirement requirement);

    public SupportDocument getSupportDocument(Institution institution, Requirement requirement);

    public SupportDocument getClearanceDate(Registrant registrant, Course course);

    public SupportDocument getSupportDocument(ExamRegistration examRegistration, Requirement requirement);

    public SupportDocument getSupportDocument(Application application, Requirement requirement);
}
