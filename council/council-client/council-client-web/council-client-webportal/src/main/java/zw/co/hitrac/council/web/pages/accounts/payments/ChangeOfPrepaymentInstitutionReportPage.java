/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.accounts.payments;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.PrepaymentDetails;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.accounts.CustomerService;
import zw.co.hitrac.council.business.service.accounts.PrepaymentDetailsService;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.reports.ChangePrepaymentInstitutionReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.pages.accounts.IAccountingPage;
import zw.co.hitrac.council.web.utility.DatePickerUtil;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 *
 * @author tdhlakama
 */
public class ChangeOfPrepaymentInstitutionReportPage extends IAccountingPage {

    private Date startDate, endDate;
    private HrisComparator hrisComparator = new HrisComparator();
    @SpringBean
    private PrepaymentDetailsService prepaymentDetailsService;
    @SpringBean
    private CustomerService customerService;

    public ChangeOfPrepaymentInstitutionReportPage() {
        PropertyModel<Date> startDateModel = new PropertyModel<Date>(this, "startDate");
        PropertyModel<Date> endDateModel = new PropertyModel<Date>(this, "endDate");

        Form<?> form = new Form("form");
        form.add(new TextField<Date>("startDate", startDateModel).add(DatePickerUtil.getDatePicker()));
        form.add(new TextField<Date>("endDate", endDateModel).add(DatePickerUtil.getDatePicker()));
        add(form);
        add(new FeedbackPanel("feedback"));
        form.add(new Button("print") {
            @Override
            public void onSubmit() {
                try {
                    ChangePrepaymentInstitutionReport changePrepaymentInstitutionReport = new ChangePrepaymentInstitutionReport();
                    if (startDate != null && endDate == null) {
                        endDate = new Date();
                    }

                    List<PrepaymentDetails> prepaymentDetailsList = prepaymentDetailsService.findAllByDates(startDate, endDate);
                    for (PrepaymentDetails r : prepaymentDetailsList) {
                        Registrant registrant = customerService.getRegistrant(r.getPaymentDetails().getCustomer().getAccount());
                        if (registrant != null) {
                            r.setRegistrant(registrant);
                        }
                    }

                    Map parameters = new HashMap();
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource;

                    parameters.put("comment", "Summary Listing of Prepayments Done from " + DateUtil.getDate(startDate) + " to " + DateUtil.getDate(endDate));
                    parameters.put("printedby", CouncilSession.get().getUser().getUsername());
                    resource = ReportResourceUtils.getReportResource(changePrepaymentInstitutionReport, contentType, prepaymentDetailsList, parameters);

                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);
                    resource.respond(a);
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    Logger.getLogger(ChangeOfPrepaymentInstitutionReportPage.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

}
