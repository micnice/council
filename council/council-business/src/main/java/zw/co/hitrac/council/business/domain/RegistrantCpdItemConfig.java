/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author hitrac
 */
@Entity
@Table(name="registrantcpditemconfig")
@Audited
public class RegistrantCpdItemConfig extends BaseEntity implements Serializable{
   
    
    private BigDecimal totalCpdPointsPermissable= BigDecimal.ZERO;

    /**
     *
     * @return
     */
    public BigDecimal getTotalCpdPointsPermissable() {
        return totalCpdPointsPermissable;
    }

    public void setTotalCpdPointsPermissable(BigDecimal totalCpdPointsPermissable) {
        this.totalCpdPointsPermissable = totalCpdPointsPermissable;
    }
    
   
   
}
