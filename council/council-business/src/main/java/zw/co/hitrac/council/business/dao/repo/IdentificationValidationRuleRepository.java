package zw.co.hitrac.council.business.dao.repo;

import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.Citizenship;
import zw.co.hitrac.council.business.domain.IdentificationType;
import zw.co.hitrac.council.business.domain.IdentificationValidationRule;

import java.util.List;

/**
 * Created by clive on 6/16/15.
 */
public interface IdentificationValidationRuleRepository extends CrudRepository<IdentificationValidationRule, Long> {

    public IdentificationValidationRule findByRetired(Boolean retired);
    public List<IdentificationValidationRule> findByCitizenshipAndIdentificationTypeAndActiveTrue(Citizenship citizenship,
                                                                                                  IdentificationType identificationType);
}
