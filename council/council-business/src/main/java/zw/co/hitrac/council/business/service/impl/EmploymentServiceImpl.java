/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.EmploymentDAO;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.EmploymentService;

import java.util.Date;
import java.util.List;

/**
 *
 * @author tidza
 */
@Service
@Transactional
public class EmploymentServiceImpl implements EmploymentService {

    @Autowired
    private EmploymentDAO employmentDao;

    @Transactional
    public Employment save(Employment t) {
        return employmentDao.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Employment> findAll() {
        return employmentDao.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Employment get(Long id) {
        return employmentDao.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Employment> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setEmploymentDAO(EmploymentDAO employmentDAO) {
        this.employmentDao = employmentDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Employment> getEmployments(Registrant registrant) {
        return employmentDao.getEmployments(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public EmploymentType getRegistrantEmployment(Registrant registrant) {
        return employmentDao.getRegistrantEmployment(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getEmployments(String searchTerm, Province province, District district, EmploymentType employmentType, EmploymentArea employmentArea, Institution institution, EmploymentStatus employmentStatus, City city, Post post,
                                               Date startDate, Date endDate, Boolean active) {
        return employmentDao.getEmployments(searchTerm,province, district, employmentType, employmentArea, institution, employmentStatus, city, post, startDate, endDate, active);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Employment getCurrentEmployed(Registrant registrant) {
        return employmentDao.getCurrentEmployed(registrant);
    }
}
