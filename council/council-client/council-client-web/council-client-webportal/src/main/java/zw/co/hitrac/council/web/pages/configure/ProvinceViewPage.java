
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Province;
import zw.co.hitrac.council.business.service.ProvinceService;

/**
 *
 * @author hitrac
 */
public class ProvinceViewPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private ProvinceService provinceService;

    public ProvinceViewPage(Long id) {
        CompoundPropertyModel<Province> model =
            new CompoundPropertyModel<Province>(new ProvinceViewPage.LoadableDetachableProvinceModel(id));

        setDefaultModel(model);
        add(new Link<Province>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new ProvinceEditPage(getModelObject().getId(), getPageReference()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", ProvinceListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("province", model.bind("name")));
    }

    private final class LoadableDetachableProvinceModel extends LoadableDetachableModel<Province> {
        private Long id;

        public LoadableDetachableProvinceModel(Long id) {
            this.id = id;
        }

        @Override
        protected Province load() {
            return provinceService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
