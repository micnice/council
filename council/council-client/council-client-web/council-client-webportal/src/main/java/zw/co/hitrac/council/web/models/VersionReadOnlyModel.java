package zw.co.hitrac.council.web.models;

import org.apache.wicket.model.AbstractReadOnlyModel;
import zw.co.hitrac.council.business.config.CouncilBusinessConfiguration;

/**
 *
 * @author Charles Chigoriwa
 */
public class VersionReadOnlyModel extends AbstractReadOnlyModel<String> {

    @Override
    public String getObject() {
        /*
         * Read the specification version from the council-business MANIFEST.MF file.
         */
        Package p = CouncilBusinessConfiguration.class.getPackage();

        String version = p.getSpecificationVersion();

        if (version == null || version.length() == 0) {
            return "Missing Version";
        } else {
            return version;
        }
    }
}
