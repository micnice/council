package zw.co.hitrac.council.business.dao;

import zw.co.hitrac.council.business.domain.RegistrantTransfer;

/**
 *
 * @author Michael Matiashe
 */
public interface RegistrantTransferDAO extends IGenericDAO<RegistrantTransfer> {
    
}
