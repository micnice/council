package zw.co.hitrac.council.business.dao.repo;

import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.GeneralParameters;
import zw.co.hitrac.council.business.domain.RequiredParameter;

import java.util.List;

/**
 * Created by tndangana on 2/13/17.
 */
public interface RequiredParameterRepository extends CrudRepository<RequiredParameter,Long> {
    public List<RequiredParameter> findAll();
}
