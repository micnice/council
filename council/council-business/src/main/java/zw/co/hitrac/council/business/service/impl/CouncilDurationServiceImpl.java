package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.CouncilDurationDAO;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.service.CouncilDurationService;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;

/**
 *
 * @author Charles Chigoriwa
 */
@Service
@Transactional
public class CouncilDurationServiceImpl implements CouncilDurationService {

    @PersistenceContext
    EntityManager entityManager;
    
    @Autowired
    private CouncilDurationDAO durationDAO;

    @Transactional
    public CouncilDuration save(CouncilDuration duration) {
        return durationDAO.save(duration);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<CouncilDuration> findAll() {
        return durationDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public CouncilDuration get(Long id) {
        return durationDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<CouncilDuration> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setDurationDAO(CouncilDurationDAO durationDAO) {
        this.durationDAO = durationDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public CouncilDuration getCouncilDurationFromDuration(Duration duration) {
        return this.durationDAO.getCouncilDurationFromDuration(duration);
    }
    
        public List<CouncilDuration> getActiveCouncilDurations() {
        
      
        return durationDAO.getActiveCouncilDurations();
    }

}
