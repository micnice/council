package zw.co.hitrac.council.business.dao;


import zw.co.hitrac.council.business.domain.TransferRule;

/**
 *
 * @author Michael Matiashe
 */
public interface TransferRuleDAO extends IGenericDAO<TransferRule> {
    
}
