package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Conditions;
import zw.co.hitrac.council.business.service.ConditionService;

/**
 *
 * @author Michael Matiashe
 */
public class ConditionViewPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private ConditionService conditionService;

    public ConditionViewPage(Long id) {
        CompoundPropertyModel<Conditions> model =
            new CompoundPropertyModel<Conditions>(new LoadableDetachableConditionModel(id));

        setDefaultModel(model);
        add(new Link<Conditions>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new ConditionEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", ConditionListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("generalCondition"));
        add(new Label("examCondition")); 
        add(new Label("registerCondition"));         
        add(new Label("condition", model.bind("name")));
    }

    private final class LoadableDetachableConditionModel extends LoadableDetachableModel<Conditions> {
        private Long id;

        public LoadableDetachableConditionModel(Long id) {
            this.id = id;
        }

        @Override
        protected Conditions load() {
            return conditionService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
