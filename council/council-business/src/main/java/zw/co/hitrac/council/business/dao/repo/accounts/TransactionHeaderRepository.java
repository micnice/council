
package zw.co.hitrac.council.business.dao.repo.accounts;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.accounts.TransactionHeader;

/**
 *
 * @author Edward Zengeni
 */
public interface TransactionHeaderRepository  extends CrudRepository<TransactionHeader, Long>{
    public List<TransactionHeader> findAll();
}
