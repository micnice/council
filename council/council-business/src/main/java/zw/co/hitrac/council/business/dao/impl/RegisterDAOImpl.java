package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.RegisterDAO;
import zw.co.hitrac.council.business.dao.repo.RegisterRepository;
import zw.co.hitrac.council.business.domain.Register;

/**
 *
 * @author kelvin
 */
@Repository
public class RegisterDAOImpl implements RegisterDAO {

    @Autowired
    private RegisterRepository registerRepository;
    @PersistenceContext
    private EntityManager entityManager;

    public Register save(Register register) {
        return registerRepository.save(register);
    }

    public List<Register> findAll() {
        return registerRepository.findAll();

    }

    public Register get(Long id) {
        return registerRepository.findOne(id);

    }

    public void setRegisterRepository(RegisterRepository registerRepository) {
        this.registerRepository = registerRepository;
    }

    public List<Register> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Register> getRegisterDisciplines() {
        return entityManager.createQuery("select r from Register r where r.discipline = TRUE ORDER BY r.name ASC ").getResultList();
    }

}
