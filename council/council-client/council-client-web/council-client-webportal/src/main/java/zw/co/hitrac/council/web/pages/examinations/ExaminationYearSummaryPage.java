package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------
import com.googlecode.wicket.jquery.ui.panel.JQueryFeedbackPanel;
import net.sf.jasperreports.engine.JRException;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.examinations.ExamPassRate;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationService;
import zw.co.hitrac.council.business.service.examinations.ExamSettingService;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

//~--- JDK imports ------------------------------------------------------------
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.wicket.markup.html.panel.FeedbackPanel;
import zw.co.hitrac.council.business.domain.examinations.ExamYear;
import zw.co.hitrac.council.business.service.examinations.ExamYearService;
import zw.co.hitrac.council.reports.ExaminationYearReport;

/**
 * @author tdhlakama
 */
public class ExaminationYearSummaryPage extends IExaminationsPage {

    @SpringBean
    private ExamRegistrationService examRegistrationService;
    @SpringBean
    private ExamSettingService examSettingService;
    @SpringBean
    private ExamYearService examYearService;
    @SpringBean
    private CourseService courseService;
    private ExamYear examYear;
    
    public ExaminationYearSummaryPage() {
        final FeedbackPanel feedback = new JQueryFeedbackPanel("feedback");
        add(feedback.setOutputMarkupId(true));
        PropertyModel<ExamYear> examYearModel = new PropertyModel<ExamYear>(this, "examYear");
        Form<?> form = new Form("form");

        form.add(new DropDownChoice("examYear", examYearModel, examYearService.findAll()).setRequired(true).add(new ErrorBehavior()));
        add(form);

        form.add(new Button("courseMasterTablePassRate") {
            @Override
            public void onSubmit() {

                ExaminationYearReport examinationYearReport = new ExaminationYearReport();

                try {
                    Map parameters = new HashMap();
                   
                    List<ExamPassRate> rates = new ArrayList<ExamPassRate>();

                    for (Course c : courseService.findAll()) {
                        ExamPassRate examPassRate = new ExamPassRate();
                        examPassRate.setCourse(c);
                        for (ExamSetting examSetting : examSettingService.findByExamYear(examYear)) {
                            for (Institution institution : examRegistrationService.getInstitutiuons(examSetting, c)) {
                                for (ExamRegistration e : examRegistrationService.getAllExamCandidates(examSetting, c, institution, null, null, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE, false, false)) {
                                    if (e.getPassStatus() && e.getCompletedStatus()) {
                                        examPassRate.setNumberOfCandidatesPassed(examPassRate.getNumberOfCandidatesPassed() + 1);
                                    }
                                    if (!e.getPassStatus() && e.getCompletedStatus()) {
                                        examPassRate.setNumberOfCandidatesFailed(examPassRate.getNumberOfCandidatesFailed() + 1);
                                    }
                                }
                            }
                        }
                        examPassRate.setTotalNumberOfCandidates(examPassRate.getNumberOfCandidatesPassed() + examPassRate.getNumberOfCandidatesFailed());
                        rates.add(examPassRate);
                    }
                    parameters.put("comment", examYear + "- Examination Course Pass Rate");

                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(examinationYearReport,
                            contentType, rates, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
