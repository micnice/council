
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.MisconductType;
import zw.co.hitrac.council.business.domain.MisconductType;
import zw.co.hitrac.council.business.domain.MisconductType;
import zw.co.hitrac.council.business.service.MisconductTypeService;
import zw.co.hitrac.council.business.service.MisconductTypeService;
import zw.co.hitrac.council.business.service.MisconductTypeService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;

/**
 *
 * @author hitrac
 */
public class MisconductTypeListPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private MisconductTypeService misconductTypeService;

    public MisconductTypeListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new MisconductTypeEditPage(null));
            }
        });

        IModel<List<MisconductType>> model = new LoadableDetachableModel<List<MisconductType>>() {
            @Override
            protected List<MisconductType> load() {
                return misconductTypeService.findAll();
            }
        };
        PropertyListView<MisconductType> eachItem = new PropertyListView<MisconductType>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<MisconductType> item) {
                Link<MisconductType> viewLink = new Link<MisconductType>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new MisconductTypeViewPage(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };

        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
