package zw.co.hitrac.council.webservices.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.Serializable;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.WebUser;

/**
 *
 * @author tdhlakama
 */
@Path("/webuser")
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public interface WebUserWebService extends Serializable {

    @GET
    @Path(value = "get")
    public WebUser get(@QueryParam(value = "username") String username, @QueryParam(value = "password") String password);

    @GET
    @Path(value = "create")
    @Produces("text/plain")
    public Response createWebUser(@QueryParam(value = "username") String username, @QueryParam(value = "password") String password, @QueryParam(value = "registrationNumber") String registrationNumber);

    @GET
    @Path(value = "login")
    @Produces("text/plain")
    public Response loginWebUser(@QueryParam(value = "username") String username, @QueryParam(value = "password") String password);

    @GET
    @Path("list")
    public List<WebUser> getWebUsers();
}
