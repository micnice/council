package zw.co.hitrac.council.business.service.accounts.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.accounts.PaymentDetailsDAO;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.accounts.PaymentDetails;
import zw.co.hitrac.council.business.service.accounts.PaymentDetailsService;

import java.util.List;

/**
 *
 * @author Michael Matiashe
 */
@Service
@Transactional
public class PaymentDetailsServiceImpl implements PaymentDetailsService {

    @Autowired
    private PaymentDetailsDAO paymentDetailsDAO;

    @Transactional
    public PaymentDetails save(PaymentDetails t) {
        return paymentDetailsDAO.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<PaymentDetails> findAll() {
        return paymentDetailsDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public PaymentDetails get(Long id) {
        return paymentDetailsDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<PaymentDetails> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setPaymentDetailsDao(PaymentDetailsDAO paymentDetailsDAO) {
        this.paymentDetailsDAO = paymentDetailsDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<PaymentDetails> transactionHistory(Customer customer) {
        return paymentDetailsDAO.transactionHistory(customer);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<PaymentDetails> getUnprintedPaymentDetails(Customer customer) {
        return paymentDetailsDAO.getUnprintedPaymentDetails(customer);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<PaymentDetails> findByUid(String uuid) {
        return paymentDetailsDAO.findByUid(uuid);
    }
}
