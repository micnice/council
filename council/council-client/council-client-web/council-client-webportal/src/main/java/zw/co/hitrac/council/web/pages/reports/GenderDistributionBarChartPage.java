
package zw.co.hitrac.council.web.pages.reports;

import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author tndangana
 */
public class GenderDistributionBarChartPage extends TemplatePage {

    
    
    public GenderDistributionBarChartPage() {
        
        add (new GenderAgeDistributionReport("genderAgeDistributionReport", 1000, 500));
    }
    
    
    
}
