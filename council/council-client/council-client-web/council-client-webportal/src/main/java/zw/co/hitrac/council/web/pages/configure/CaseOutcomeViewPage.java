/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CaseOutcome;
import zw.co.hitrac.council.business.service.CaseOutcomeService;
import zw.co.hitrac.council.web.utility.CustomDateLabel;

/**
 *
 * @author kelvin
 */
public class CaseOutcomeViewPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private CaseOutcomeService caseOutcomeService;

    public CaseOutcomeViewPage(Long id) {
        CompoundPropertyModel<CaseOutcome> model = new CompoundPropertyModel<CaseOutcome>(new CaseOutcomeViewPage.LoadableDetachableCaseOutcomeModel(id));
        setDefaultModel(model);
        add(new Link<CaseOutcome>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new CaseOutcomeEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", CaseOutcomeListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new CustomDateLabel("dates"));
        add(new Label("guilty"));
        add(new Label("caseOutcome", model.bind("name")));
    }

    private final class LoadableDetachableCaseOutcomeModel extends LoadableDetachableModel<CaseOutcome> {

        private Long id;

        public LoadableDetachableCaseOutcomeModel(Long id) {
            this.id = id;
        }

        @Override
        protected CaseOutcome load() {

            return caseOutcomeService.get(id);
        }
    }
}
