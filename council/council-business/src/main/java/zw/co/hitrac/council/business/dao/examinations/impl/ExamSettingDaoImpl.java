package zw.co.hitrac.council.business.dao.examinations.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.examinations.ExamSettingDao;
import zw.co.hitrac.council.business.dao.repo.examinations.ExamSettingRepository;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.domain.examinations.ExamYear;

/**
 *
 * @author tidza
 */
@Repository
public class ExamSettingDaoImpl implements ExamSettingDao {

    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    private ExamSettingRepository examSettingRepository;

    public ExamSetting save(ExamSetting t) {
        return examSettingRepository.save(t);
    }

    public List<ExamSetting> findAll() {
        return examSettingRepository.findAll();
    }

    public ExamSetting get(Long id) {
        return examSettingRepository.findOne(id);
    }

    public void setExamSettingRepository(ExamSettingRepository examSettingRepository) {
        this.examSettingRepository = examSettingRepository;
    }

    public List<ExamSetting> getExamSettings(Boolean active) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ExamSetting.class);
        if (active != null) {
            criteria.add(Restrictions.eq("active", active));
        }
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<ExamSetting> findByExamYear(ExamYear examYear) {
         return examSettingRepository.findByExamYear(examYear);
    }

    public List<ExamSetting> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
