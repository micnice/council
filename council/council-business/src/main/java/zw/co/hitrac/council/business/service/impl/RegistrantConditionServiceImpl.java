/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.RegistrantConditionDAO;
import zw.co.hitrac.council.business.domain.Conditions;
import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantCondition;
import zw.co.hitrac.council.business.service.RegistrantConditionService;

import java.util.List;

/**
 *
 * @author kelvin
 */
@Service
@Transactional
public class RegistrantConditionServiceImpl implements RegistrantConditionService {

    @Autowired
    private RegistrantConditionDAO registrantConditionDAO;

    @Transactional
    public RegistrantCondition save(RegistrantCondition registrantCondition) {
        return registrantConditionDAO.save(registrantCondition);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantCondition> findAll() {
        return registrantConditionDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantCondition get(Long id) {
        return registrantConditionDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantCondition> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setRegistrantConditionDAO(RegistrantConditionDAO registrantConditionDAO) {
        this.registrantConditionDAO = registrantConditionDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantCondition> getRegistrantActiveConditions(Registrant registrant) {
        return registrantConditionDAO.getRegistrantActiveConditions(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantCondition> getCondition(Registrant registrant, Conditions conditions) {
        return registrantConditionDAO.getCondition(registrant, conditions);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantCondition> getAllRegistrantConditions(Registrant registrant) {
        return registrantConditionDAO.getAllRegistrantConditions(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantCondition> getRegisterRegistrantConditions(Registrant registrant) {
        return registrantConditionDAO.getRegisterRegistrantConditions(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Integer getNumberOfConditionsExpired() {
        return registrantConditionDAO.getNumberOfConditionsExpired();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantCondition> getAllActiveRegistrantConditions() {
        return registrantConditionDAO.getAllActiveRegistrantConditions();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantCondition> getAllExpiredConditions() {
        return registrantConditionDAO.getAllExpiredConditions();
    }

    @Transactional(readOnly = false, propagation = Propagation.SUPPORTS)
    public  void saveRegisterConditions(Register register, Registrant registrant){
         registrantConditionDAO.saveRegisterConditions(register,registrant);
    }
}
