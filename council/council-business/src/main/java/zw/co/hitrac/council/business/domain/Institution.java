package zw.co.hitrac.council.business.domain;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.utils.CaseUtil;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Tatenda Chiwandire
 * @author Constance Mabaso
 * @author Charles Chigoriwa
 * @author Michael Matiashe
 */
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Table(name = "institution")
@Audited
@XmlRootElement
public class Institution extends BaseEntity implements Serializable, Comparable<Institution> {

    private InstitutionCategory institutionCategory;
    private InstitutionType institutionType;
    private District district;
    private Boolean registered = Boolean.FALSE;
    private Set<Service> services = new HashSet<Service>();
    private Customer customerAccount;
    private Boolean academic = Boolean.FALSE;
    private Boolean trainingInstitution = Boolean.FALSE;
    private Long oldExamCentreID;
    private Boolean examiniationCenter = Boolean.FALSE;
    private Integer trainingSchoolCapacity = 0;
    private Boolean council = Boolean.FALSE;
    private String schoolName;
    private Boolean foreignInstitution = Boolean.FALSE;
    private String code;
    private Registrant supervisor; //Supervisor
    private Registrant registrant; //In Charge - Owner
    private String registrationNumber = "";
    private City city;
    private List<Institution> institutions = new ArrayList<Institution>();
    private Institution duplicate;

    @Transient
    public Institution getDuplicate() {

        return duplicate;
    }

    public void setDuplicate(Institution duplicate) {

        this.duplicate = duplicate;
    }

    public String getRegistrationNumber() {

        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {

        this.registrationNumber = registrationNumber;
    }

    @ManyToOne
    @JoinColumn
    public Registrant getSupervisor() {

        return supervisor;
    }

    public void setSupervisor(Registrant supervisor) {

        this.supervisor = supervisor;
    }

    public Boolean getForeignInstitution() {

        return foreignInstitution;
    }

    public void setForeignInstitution(Boolean foreignInstitution) {

        this.foreignInstitution = foreignInstitution;
    }

    @ManyToOne
    @JoinColumn
    //Owner of the institution
    public Registrant getRegistrant() {

        return registrant;
    }

    public void setRegistrant(Registrant registrant) {

        this.registrant = registrant;
    }

    public Long getOldExamCentreID() {

        return oldExamCentreID;
    }

    public void setOldExamCentreID(Long oldExamCentreID) {

        this.oldExamCentreID = oldExamCentreID;
    }

    @ManyToOne
    public InstitutionType getInstitutionType() {

        return institutionType;
    }

    public void setInstitutionType(InstitutionType institutionType) {

        this.institutionType = institutionType;
    }

    @OneToOne
    @Cascade(CascadeType.PERSIST)
    public Customer getCustomerAccount() {

        return customerAccount;
    }

    public void setCustomerAccount(Customer customerAccount) {

        this.customerAccount = customerAccount;
    }

    /**
     * @return
     */
    @Enumerated(EnumType.STRING)
    public InstitutionCategory getInstitutionCategory() {

        return institutionCategory;
    }

    public void setInstitutionCategory(InstitutionCategory institutionCategory) {

        this.institutionCategory = institutionCategory;
    }

    @ManyToOne
    public District getDistrict() {

        return district;
    }

    public void setDistrict(District district) {

        this.district = district;
    }

    @ManyToOne
    public City getCity() {

        return city;
    }

    public void setCity(City city) {

        this.city = city;
    }

    @ManyToMany
    public Set<Service> getServices() {

        return services;
    }

    public void setServices(Set<Service> services) {

        this.services = services;
    }

    @Transient
    public String getInstitutionTextStatus() {

        return registered ? "Registered" : "Unregistered";
    }

    public Boolean getRegistered() {

        return registered;
    }

    public void setRegistered(Boolean registered) {

        this.registered = registered;
    }

    public Boolean getAcademic() {

        return academic;
    }

    public void setAcademic(Boolean academic) {

        this.academic = academic;
    }

    public Boolean getTrainingInstitution() {

        return trainingInstitution;
    }

    public void setTrainingInstitution(Boolean trainingInstitution) {

        this.trainingInstitution = trainingInstitution;
    }

    public Boolean getExaminiationCenter() {

        return examiniationCenter;
    }

    public void setExaminiationCenter(Boolean examiniationCenter) {

        this.examiniationCenter = examiniationCenter;
    }

    public Integer getTrainingSchoolCapacity() {

        return trainingSchoolCapacity;
    }

    public void setTrainingSchoolCapacity(Integer trainingSchoolCapacity) {

        this.trainingSchoolCapacity = trainingSchoolCapacity;
    }

    public Boolean getCouncil() {

        if (council == null) {
            return Boolean.FALSE;
        }
        return council;
    }

    public void setCouncil(Boolean council) {

        this.council = council;
    }

    //to be used for examination on the passlist
    public String getSchoolName() {

        if (schoolName == null) {
            return getName();
        }
        return CaseUtil.upperCase(schoolName);
    }

    public void setSchoolName(String schoolName) {

        this.schoolName = schoolName;
    }

    public String getCode() {

        return code;
    }

    public void setCode(String code) {

        this.code = code;
    }

    @Transient
    public List<Institution> getInstitutions() {

        return institutions;
    }

    public void setInstitutions(List<Institution> institutions) {

        this.institutions = institutions;
    }

    @Override
    public int compareTo(Institution o) {
        if (o.institutionType == null || o.getInstitutionType() == null) {
            return 0;
        }
        return this.getInstitutionType().getName().compareTo(o.getInstitutionType().getName());
    }



}
