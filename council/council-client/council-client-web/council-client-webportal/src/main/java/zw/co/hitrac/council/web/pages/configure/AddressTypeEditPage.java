package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.AddressType;
import zw.co.hitrac.council.business.service.AddressTypeService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Charles Chigoriwa
 */
public class AddressTypeEditPage extends IAdministerDatabaseBasePage{
    
    @SpringBean
    private AddressTypeService addressTypeService;

    public AddressTypeEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<AddressType>(new LoadableDetachableAddressTypeModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink",AdministerDatabasePage.class));
        Form<AddressType> form=new Form<AddressType>("form",(IModel<AddressType>)getDefaultModel()) {
            @Override
            public void onSubmit(){
                addressTypeService.save(getModelObject());
                setResponsePage(new AddressTypeViewPage(getModelObject().getId()));
            }
        };
        
        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior())); 
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink",AddressTypeListPage.class));
        add(form);
    }
    
    
    private final class LoadableDetachableAddressTypeModel extends LoadableDetachableModel<AddressType> {

        private Long id;

        public LoadableDetachableAddressTypeModel(Long id) {
            this.id = id;
        }

        @Override
        protected AddressType load() {
            if(id==null){
                return new AddressType();
            }
            return addressTypeService.get(id);
        }
    }
    
    
}
