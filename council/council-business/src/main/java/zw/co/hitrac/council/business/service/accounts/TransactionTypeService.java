package zw.co.hitrac.council.business.service.accounts;

import java.util.List;
import zw.co.hitrac.council.business.domain.Book;
import zw.co.hitrac.council.business.domain.accounts.PaymentMethod;
import zw.co.hitrac.council.business.domain.accounts.TransactionType;
import zw.co.hitrac.council.business.service.IGenericService;

/**
 *
 * @author Charles Chigoriwa
 */
public interface TransactionTypeService extends IGenericService<TransactionType> {
    public List<TransactionType> find(Book book);
    public List<TransactionType> find(Book book, PaymentMethod paymentMethod);
    public List<TransactionType> findBankAccounts();
}
