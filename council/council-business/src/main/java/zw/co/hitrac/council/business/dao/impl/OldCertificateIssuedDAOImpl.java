/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.OldCertificateIssuedDao;
import zw.co.hitrac.council.business.dao.repo.OldCertificateIssuedRepository;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.OldCertificateIssued;

/**
 *
 * @author kelvin
 */
@Repository
public class OldCertificateIssuedDAOImpl implements OldCertificateIssuedDao {

    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    private OldCertificateIssuedRepository oldCertificateIssuedRepository;

    public OldCertificateIssued save(OldCertificateIssued oldCertificateIssued) {
        return oldCertificateIssuedRepository.save(oldCertificateIssued);
    }

    public List<OldCertificateIssued> findAll() {
        return oldCertificateIssuedRepository.findAll();
    }

    public OldCertificateIssued get(Long id) {
        return oldCertificateIssuedRepository.findOne(id);
    }

    /**
     * A setter method that will make mocking repo object easier
     *
     * @param oldCertificateIssuedRepository
     */
    public void setOldCertificateIssuedRepository(OldCertificateIssuedRepository oldCertificateIssuedRepository) {
        this.oldCertificateIssuedRepository = oldCertificateIssuedRepository;
    }

    public List<OldCertificateIssued> getCertificates(Registrant registrant) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(OldCertificateIssued.class);
        if (registrant != null) {
            criteria.add(Restrictions.eq("registrant", registrant));
        }
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public OldCertificateIssued getCertificate(Long certificateProcessRecordID) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(OldCertificateIssued.class);
        criteria.add(Restrictions.eq("certificateProcessRecordID", certificateProcessRecordID));
        return (OldCertificateIssued)criteria.uniqueResult();
    }

    public List<OldCertificateIssued> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
