/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author tdhlakama
 */
@Entity
@Table(name = "registrantclearance")
@Audited
public class RegistrantClearance extends BaseIdEntity implements Serializable {

    private Date clearanceDate;
    private Course course;
    private SupportDocument supportDocument;
    private Registrant registrant;

    @ManyToOne
    public SupportDocument getSupportDocument() {
        return supportDocument;
    }

    public void setSupportDocument(SupportDocument supportDocument) {
        this.supportDocument = supportDocument;
    }

    @ManyToOne
    public Registrant getRegistrant() {
        return registrant;
    }

    public void setRegistrant(Registrant registrant) {
        this.registrant = registrant;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getClearanceDate() {
        return clearanceDate;
    }

    public void setClearanceDate(Date clearanceDate) {
        this.clearanceDate = clearanceDate;
    }

    @ManyToOne
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
}
