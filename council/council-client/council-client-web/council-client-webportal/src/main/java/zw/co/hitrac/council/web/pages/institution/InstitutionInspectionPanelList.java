package zw.co.hitrac.council.web.pages.institution;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Application;

import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.InstitutionInspection;
import zw.co.hitrac.council.business.service.DurationService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.InstitutionInspectionService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

/**
 *
 * @author Constance Mabaso
 */
public class InstitutionInspectionPanelList extends Panel {

    @SpringBean
    InstitutionInspectionService institutionInspectionService;
    @SpringBean
    GeneralParametersService generalParametersService;
    @SpringBean
    private DurationService durationService;
    @SpringBean
    private RegistrationService registrationService;

    /**
     * Constructor
     *
     * @param id
     * @param institution
     */
    public InstitutionInspectionPanelList(String id, final IModel<Application> applicationModel) {
        super(id);

        // method to add a new InstitutionInspection - Id is null
        add(new Link<Institution>("institutionInspectionPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionInspectionPage(applicationModel));
            }
        });
        add(new PropertyListView<InstitutionInspection>("institutionInspectionList", institutionInspectionService.get(applicationModel.getObject())) {
            @Override
            protected void populateItem(ListItem<InstitutionInspection> item) {
                Link<InstitutionInspection> viewLink = new Link<InstitutionInspection>("institutionInspectionPage",
                        item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new InstitutionInspectionPage(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(viewLink);
                item.add(new Label("inspectionDate"));
                item.add(new Label("expectedInspectionDate"));
                item.add(new Label("inspectionStatus"));
                item.add(new Label("decisionStatus"));
            }
        });
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
