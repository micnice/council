/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.registration;

import java.util.ArrayList;
import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.City;
import zw.co.hitrac.council.business.domain.Country;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantAddress;
import zw.co.hitrac.council.business.service.AddressTypeService;
import zw.co.hitrac.council.business.service.CityService;
import zw.co.hitrac.council.business.service.CountryService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.RegistrantAddressService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.CityListModel;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.configure.CityEditPage;
import zw.co.hitrac.council.web.pages.configure.CountryEditPage;
import zw.co.hitrac.council.web.pages.institution.InstitutionContactAddressPage;
import zw.co.hitrac.council.web.pages.institution.InstitutionViewPage;

/**
 *
 * @author kelvin
 */
public class RegistrantAddressEditPage extends TemplatePage {

    @SpringBean
    private RegistrantAddressService registrantAddressService;
    @SpringBean
    private AddressTypeService addressTypeService;
    @SpringBean
    private CityService cityService;
    private Country country;
    @SpringBean
    private CountryService countryService;
    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private InstitutionService institutionService;

    public RegistrantAddressEditPage(final IModel<Registrant> registrantModel) {

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());

        CompoundPropertyModel<RegistrantAddress> model = new CompoundPropertyModel<RegistrantAddress>(
                new RegistrantAddressEditPage.LoadableDetachableRegistrantAddressServiceModel(
                registrantModel.getObject()));

        setDefaultModel(model);

        Form<RegistrantAddress> form = new Form<RegistrantAddress>("form", (IModel<RegistrantAddress>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    registrantAddressService.save(getModelObject());
                    setResponsePage(new RegistrantContactAddressPage(getModelObject().getRegistrant().getId()));
                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };

        form.add(new DropDownChoice("addressType", addressTypeService.findAll()).setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("address1").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("address2").setRequired(true).add(new ErrorBehavior()));
        form.add(new CheckBox("status"));
        DropDownChoice<Country> countries = new DropDownChoice<Country>("country",
                new PropertyModel<Country>(this, "country"), new RegistrantAddressEditPage.CountriesModel(),
                new ChoiceRenderer<Country>("name", "id")) {
            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }

            @Override
            protected void onSelectionChanged(Country newSelection) {
                RegistrantAddress registrantAddress = (RegistrantAddress) RegistrantAddressEditPage.this.getDefaultModelObject();

                registrantAddress.setCity(null);
            }
        };

        countries.setRequired(true).add(new ErrorBehavior());
        form.add(countries);

        DropDownChoice<City> cities = new DropDownChoice<City>("city", new RegistrantAddressEditPage.CitiesModel(),
                new ChoiceRenderer<City>("name", "id"));

        cities.setRequired(true).add(new ErrorBehavior());
        form.add(cities);

        add(form);
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantContactAddressPage(registrantModel.getObject().getId()));
            }
        });

        add(new Link<Void>("addCountry") {
            @Override
            public void onClick() {
                setResponsePage(new CountryEditPage(null, registrantModel, null));
            }
        });

        add(new Link<Void>("addCity") {
            @Override
            public void onClick() {
                setResponsePage(new CityEditPage(null, registrantModel, null));
            }
        });


        add(new Label("registrant.fullname"));
        add(new Label("institution.name") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
    }

    public RegistrantAddressEditPage(final Institution institution) {

        CompoundPropertyModel<RegistrantAddress> model = new CompoundPropertyModel<RegistrantAddress>(
                new RegistrantAddressEditPage.LoadableDetachableInstitutionAddressServiceModel(institution));

        setDefaultModel(model);

        Form<RegistrantAddress> form = new Form<RegistrantAddress>("form", (IModel<RegistrantAddress>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    registrantAddressService.save(getModelObject());
                    setResponsePage(new InstitutionContactAddressPage(institution.getId()));
                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }

            }
        };

        form.add(new DropDownChoice("addressType", addressTypeService.findAll()).setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("address1").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("address2").setRequired(true).add(new ErrorBehavior()));
        form.add(new CheckBox("status"));

        DropDownChoice<Country> countries = new DropDownChoice<Country>("country",
                new PropertyModel<Country>(this, "country"), new RegistrantAddressEditPage.CountriesModel(),
                new ChoiceRenderer<Country>("name", "id")) {
            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }

            @Override
            protected void onSelectionChanged(Country newSelection) {
                RegistrantAddress registrantAddress = (RegistrantAddress) RegistrantAddressEditPage.this.getDefaultModelObject();

                registrantAddress.setCity(null);
            }
        };

        countries.setRequired(true).add(new ErrorBehavior());
        form.add(countries);

        DropDownChoice<City> cities = new DropDownChoice<City>("city", new RegistrantAddressEditPage.CitiesModel(),
                new ChoiceRenderer<City>("name", "id"));

        cities.setRequired(true).add(new ErrorBehavior());
        form.add(cities);

        add(form);
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionViewPage(institution.getId()));
            }
        });

        add(new Label("registrant.fullname") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });

        add(new Link<Void>("addCountry") {
            @Override
            public void onClick() {
                setResponsePage(new CountryEditPage(null, null, Model.of(institution)));
            }
        });

        add(new Link<Void>("addCity") {
            @Override
            public void onClick() {
                setResponsePage(new CityEditPage(null, null, Model.of(institution)));
            }
        });

        add(new Label("institution.name"));
    }

    public RegistrantAddressEditPage(long id) {

        CompoundPropertyModel<RegistrantAddress> model = new CompoundPropertyModel<RegistrantAddress>(
                new RegistrantAddressEditPage.LoadableDetachableRegistrantAddressServiceModel(
                id));
        Long rId = null;
        Long iId = null;
        if (model.getObject().getRegistrant() != null) {
            rId = model.getObject().getRegistrant().getId();
        } else {
            iId = model.getObject().getInstitution().getId();
        }

        final Long registrantId = rId;
        final Long instituionId = iId;
        setDefaultModel(model);

        Form<RegistrantAddress> form = new Form<RegistrantAddress>("form", (IModel<RegistrantAddress>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    registrantAddressService.save(getModelObject());
                    if (registrantId != null) {
                        setResponsePage(new RegistrantContactAddressPage(registrantId));
                    }
                    if (instituionId != null) {
                        setResponsePage(new InstitutionViewPage(instituionId));
                    }
                } catch (CouncilException ex) {
                    error(ex.getMessage());

                }

            }
        };

        form.add(new DropDownChoice("addressType", addressTypeService.findAll()).setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("address1").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("address2").setRequired(true).add(new ErrorBehavior()));
        form.add(new CheckBox("status"));

        DropDownChoice<Country> countries = new DropDownChoice<Country>("country",
                new PropertyModel<Country>(this, "country"), new RegistrantAddressEditPage.CountriesModel(),
                new ChoiceRenderer<Country>("name", "id")) {
            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }

            @Override
            protected void onSelectionChanged(Country newSelection) {
                RegistrantAddress registrantAddress = (RegistrantAddress) RegistrantAddressEditPage.this.getDefaultModelObject();

                registrantAddress.setCity(null);
            }
        };

        countries.setRequired(true).add(new ErrorBehavior());
        form.add(countries);

        DropDownChoice<City> cities = new DropDownChoice<City>("city", new RegistrantAddressEditPage.CitiesModel(),
                new ChoiceRenderer<City>("name", "id"));

        cities.setRequired(true).add(new ErrorBehavior());
        form.add(cities);


        add(form);
        if (registrantId != null) {
            add(new Label("registrant.fullname"));
            add(new Label("institution.name") {
                @Override
                protected void onConfigure() {
                    setVisible(Boolean.FALSE);
                }
            });
        }
        if (instituionId != null) {
            add(new Label("registrant.fullname") {
                @Override
                protected void onConfigure() {
                    setVisible(Boolean.FALSE);
                }
            });
            add(new Label("institution.name"));
        }

        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                if (registrantId != null) {
                    setResponsePage(new RegistrantContactAddressPage(registrantId));
                }
                if (instituionId != null) {
                    setResponsePage(new InstitutionViewPage(instituionId));
                }
            }
        });

        add(new Link<Void>("addCountry") {
            @Override
            public void onClick() {
                if (registrantId != null) {
                    setResponsePage(new CountryEditPage(null, Model.of(registrantService.get(registrantId)), null));
                }
                if (instituionId != null) {
                    setResponsePage(new CountryEditPage(null, null, Model.of(institutionService.get(instituionId))));
                }

            }
        });

        add(new Link<Void>("addCity") {
            @Override
            public void onClick() {
                if (registrantId != null) {
                    setResponsePage(new CityEditPage(null, Model.of(registrantService.get(registrantId)), null));
                }
                if (instituionId != null) {
                    setResponsePage(new CityEditPage(null, null, Model.of(institutionService.get(instituionId))));
                }
            }
        });

    }

    private class CitiesModel extends LoadableDetachableModel<List<? extends City>> {

        protected List<? extends City> load() {
            if (country != null) {
                return new CityListModel(cityService, country).getObject();
            } else {
                return new ArrayList<City>();
            }
        }
    }

    private final class CountriesModel extends LoadableDetachableModel<List<? extends Country>> {

        protected List<? extends Country> load() {
            return countryService.findAll();
        }
    }

    private final class LoadableDetachableRegistrantAddressServiceModel extends LoadableDetachableModel<RegistrantAddress> {

        private Long id;
        private Registrant registrant;

        public LoadableDetachableRegistrantAddressServiceModel(Long id) {
            this.id = id;
        }

        public LoadableDetachableRegistrantAddressServiceModel(Registrant registrant) {
            this.registrant = registrant;
        }

        @Override
        protected RegistrantAddress load() {
            RegistrantAddress registrantAddress = null;

            if (id == null) {
                registrantAddress = new RegistrantAddress();
                registrantAddress.setRegistrant(registrant);
            } else {
                registrantAddress = registrantAddressService.get(id);
            }

            // if country is not selected by the user usually for the first time of loading the page
            if (country == null) {
                if (registrantAddress.getCity() != null) {
                    country = registrantAddress.getCity().getCountry();
                }
            }

            return registrantAddress;
        }
    }

    private final class LoadableDetachableInstitutionAddressServiceModel extends LoadableDetachableModel<RegistrantAddress> {

        private Long id;
        private Institution institution;

        public LoadableDetachableInstitutionAddressServiceModel(Long id) {
            this.id = id;
        }

        public LoadableDetachableInstitutionAddressServiceModel(Institution institution) {
            this.institution = institution;
        }

        @Override
        protected RegistrantAddress load() {
            RegistrantAddress registrantAddress = null;

            if (id == null) {
                registrantAddress = new RegistrantAddress();
                registrantAddress.setInstitution(institution);
            } else {
                registrantAddress = registrantAddressService.get(id);
            }

            // if country is not selected by the user usually for the first time of loading the page
            if (country == null) {
                if (registrantAddress.getCity() != null) {
                    country = registrantAddress.getCity().getCountry();
                }
            }

            return registrantAddress;
        }
    }
}
