/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao;

import zw.co.hitrac.council.business.domain.CaseOutcome;
import zw.co.hitrac.council.business.domain.MisconductType;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantDisciplinary;

import java.util.Date;
import java.util.List;

/**
 *
 * @author hitrac
 */
public interface RegistrantDisciplinaryDAO extends IGenericDAO<RegistrantDisciplinary> {

    public List<RegistrantDisciplinary> find(MisconductType misconductType, int startYear, int endYear);

    public List<RegistrantDisciplinary> find(MisconductType misconductType, int yearReported);

    public List<RegistrantDisciplinary> getCurrentUnResolved(Registrant registrant);
    
    public List<RegistrantDisciplinary> getGuiltyResolvedCases(Registrant registrant);
    
    public String getPendingCases(Registrant registrant);

    public List<RegistrantDisciplinary> getRegistrantDisciplinaries(Registrant registrant);

    public List<RegistrantDisciplinary> getDisciplinariesByDates(Date startDate, Date endDate, CaseOutcome caseOutcome, MisconductType misconductType, String caseStatus);
    
    public Boolean registrantSuspendedStatus(Registrant registrant);
}
