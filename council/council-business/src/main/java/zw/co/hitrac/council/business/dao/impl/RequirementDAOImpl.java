package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.RequirementDAO;
import zw.co.hitrac.council.business.dao.repo.RequirementRepository;
import zw.co.hitrac.council.business.domain.Requirement;

/**
 *
 * @author Michael Matiashe
 */
@Repository
public class RequirementDAOImpl implements RequirementDAO {

    @Autowired
    private RequirementRepository requirementRepository;
    @PersistenceContext
    EntityManager entityManager;

    public Requirement save(Requirement requirement) {
        return requirementRepository.save(requirement);
    }

    public List<Requirement> findAll() {
        return requirementRepository.findAll();
    }

    public Requirement get(Long id) {
        return requirementRepository.findOne(id);
    }

    /**
     * A setter method that will make mocking repo object easier
     *
     * @param requirementRepository
     */
    public void setRequirementRepository(RequirementRepository requirementRepository) {
        this.requirementRepository = requirementRepository;
    }

    public List<Requirement> findAll(Boolean examRequirement) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Requirement.class);
        criteria.add(Restrictions.eq("examRequirement", examRequirement));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }
}
