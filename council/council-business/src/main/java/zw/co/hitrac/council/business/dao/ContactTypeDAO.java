package zw.co.hitrac.council.business.dao;

import zw.co.hitrac.council.business.domain.ContactType;



/**
 *
 * @author Charles Chigoriwa
 */
public interface ContactTypeDAO extends IGenericDAO<ContactType> {
    
}
