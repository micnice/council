package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.RegistrantCPDCategoryDAO;
import zw.co.hitrac.council.business.dao.repo.RegistrantCPDCategoryRepository;
import zw.co.hitrac.council.business.domain.RegistrantCPDCategory;

/**
 *
 * @author user Constance Mabaso
 */
@Repository
public class RegistrantCPDCategoryDAOImpl implements RegistrantCPDCategoryDAO{

    @Autowired
    private RegistrantCPDCategoryRepository registrantCPDCategoryRepository;
    
    public RegistrantCPDCategory save(RegistrantCPDCategory registrantCPDCategory) {
        return registrantCPDCategoryRepository.save(registrantCPDCategory);
    }

    public List<RegistrantCPDCategory> findAll() {
       return registrantCPDCategoryRepository.findAll();
    }

    public RegistrantCPDCategory get(Long id) {
        return registrantCPDCategoryRepository.findOne(id);
    }

    public void setRegistrantCPDCategoryRepository(RegistrantCPDCategoryRepository registrantCPDCategoryRepository) {
        this.registrantCPDCategoryRepository=registrantCPDCategoryRepository;
    }

    public List<RegistrantCPDCategory> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
