
package zw.co.hitrac.council.business.service.accounts;

import zw.co.hitrac.council.business.domain.accounts.TransactionHeader;
import zw.co.hitrac.council.business.service.IGenericService;

/**
 *
 * @author Edward Zengeni
 */
public interface TransactionHeaderService extends IGenericService<TransactionHeader>{
    
}
