package zw.co.hitrac.council.business.dao.accounts;

import zw.co.hitrac.council.business.dao.IGenericDAO;
import zw.co.hitrac.council.business.domain.accounts.TransactionComponent;

/**
 *
 * @author Takunda Dhlakama
 * @author Constance Mabaso
 */
public interface TransactionComponentDAO extends IGenericDAO<TransactionComponent>{
    
}
