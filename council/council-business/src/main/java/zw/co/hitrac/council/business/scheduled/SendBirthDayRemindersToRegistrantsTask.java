package zw.co.hitrac.council.business.scheduled;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import zw.co.hitrac.council.business.domain.GeneralParameters;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.EmailMessageService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrantService;

import java.util.List;

/**
 * Created by clive on 7/10/15.
 */

/**
 * Please note that for this to work the email contact type in GeneralParameters must be set
 */
@Component
public class SendBirthDayRemindersToRegistrantsTask implements ScheduledTask {

    //Syntax UNIX Cron with second  -------------   s m h  dom mon dow
    public static final String NOTIFICATION_SCHEDULE_EXPRESSION = "0 0 7 * * *"; //Run Every day at 7am

    @Autowired
    private RegistrantService registrantService;
    @Autowired
    private EmailMessageService emailMessageService;
    @Autowired
    private GeneralParametersService generalParametersService;

    @Override
    @Async
    @Scheduled(cron = NOTIFICATION_SCHEDULE_EXPRESSION)
    public void execute() {

        GeneralParameters generalParameters = generalParametersService.get();

        if (generalParameters.getAllowSendingBirthDayRemindersToRegistrants()) {

            List<RegistrantData> registrantDataList = registrantService.getRegistrantsWithBirthDays();

            try {

                for (RegistrantData registrantData : registrantDataList) {

                    String subject = String.format("Happy birthday %s", registrantData.getFullname());
                    String bodyMessage = String.format("The %s wishes you a happy birthday on turning %d. May you have a blessed and entertaining day\n\nRegards\n\n\n%s",
                            generalParameters.getCouncilName(), registrantData.getIntegerValueOfAge(), generalParameters.getEmailSignature());

                    emailMessageService.send(registrantData, subject, bodyMessage);

                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
