package zw.co.hitrac.council.business.domain.accounts;

import org.apache.commons.lang.BooleanUtils;
import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.domain.BaseIdEntity;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;

import javax.persistence.*;

/**
 * @author Matiashe Michael
 */
@Entity
@Table(name = "prepaymentdetails", uniqueConstraints = @UniqueConstraint(columnNames = {"uid"}))
@Audited
public class PrepaymentDetails extends BaseIdEntity {

    private Customer customer;
    private Prepayment prepayment;
    private Registrant registrant;
    private Institution institution;
    private PaymentDetails paymentDetails;
    private Boolean cancelled = Boolean.FALSE;
    private Institution changedInstitution;
    private Prepayment changedPrepayment;
    private CouncilDuration councilDuration;

    @ManyToOne
    public CouncilDuration getCouncilDuration() {
        return councilDuration;
    }

    public void setCouncilDuration(CouncilDuration councilDuration) {
        this.councilDuration = councilDuration;
    }

    @ManyToOne
    public Institution getChangedInstitution() {
        return changedInstitution;
    }

    public void setChangedInstitution(Institution changedInstitution) {
        this.changedInstitution = changedInstitution;
    }

    @ManyToOne
    public Prepayment getChangedPrepayment() {
        return changedPrepayment;
    }

    public void setChangedPrepayment(Prepayment changedPrepayment) {
        this.changedPrepayment = changedPrepayment;
    }

    public Boolean isCancelled() {
        return BooleanUtils.toBoolean(cancelled);
    }

    public PrepaymentDetails() {
    }

    public void setCancelled(Boolean cancelled) {
        this.cancelled = cancelled;
    }

    public PrepaymentDetails(Customer customer) {
        this.customer = customer;
    }

    @ManyToOne
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    /**
     * @return
     */
    @OneToOne
    public Prepayment getPrepayment() {
        return prepayment;
    }

    public void setPrepayment(Prepayment prepayment) {
        this.prepayment = prepayment;
    }

    /**
     * @return
     */
    @OneToOne
    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    @OneToOne
    public PaymentDetails getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(PaymentDetails paymentDetails) {
        this.paymentDetails = paymentDetails;
    }

    @Transient
    public Registrant getRegistrant() {
        return registrant;
    }

    public void setRegistrant(Registrant registrant) {
        this.registrant = registrant;
    }
}
