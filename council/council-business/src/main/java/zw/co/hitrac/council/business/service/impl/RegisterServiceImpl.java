/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.RegisterDAO;
import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.business.service.RegisterService;

import java.util.List;

/**
 * @author Kelvin Goredema
 */
@Service
@Transactional
public class RegisterServiceImpl implements RegisterService {

    @Autowired
    private RegisterDAO registerDAO;

    @Transactional
    public Register save(Register register) {
        return registerDAO.save(register);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Register> findAll() {
        return registerDAO.findAll();

    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Register get(Long id) {
        return registerDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Register> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Register> getRegisterDisciplines() {
        return registerDAO.getRegisterDisciplines();
    }

}
