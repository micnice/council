package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.City;
import zw.co.hitrac.council.business.service.CityService;

import java.util.List;

/**
 *
 * @author Matiashe Michael
 */
public class CityListPage extends IAdministerDatabaseBasePage{
    
    @SpringBean
    private CityService cityService;

    public CityListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink",AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink",AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {

            @Override
            public void onClick() {
               setResponsePage(new CityEditPage(null, getPageReference()));
            }
        });
        
        IModel<List<City>> model=new LoadableDetachableModel<List<City>>() {

            @Override
            protected List<City> load() {
               return cityService.findAll();
            }
        };
        
        PropertyListView<City> eachItem=new PropertyListView<City>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<City> item) {
                Link<City> viewLink=new Link<City>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                       setResponsePage(new CityViewPage(getModelObject().getId()));
                    }
                };
                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };
        
        add(eachItem);
    }
    
    
    
}
