
package zw.co.hitrac.council.business.service.accounts;

import zw.co.hitrac.council.business.domain.accounts.PaymentData;
import zw.co.hitrac.council.business.service.IGenericService;

/**
 *
 * @author Michael Matiashe
 */
public interface PaymentDataService extends IGenericService<PaymentData> {
    
}
