/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author tdhlakama
 */
@Entity
@Table(name = "institutiontype")
@Audited
@XmlRootElement
public class InstitutionType extends BaseEntity implements Serializable {

    private Set<Requirement> requirements = new HashSet<Requirement>();

    @XmlTransient
    @ManyToMany
    public Set<Requirement> getRequirements() {
        return requirements;
    }

    public void setRequirements(Set<Requirement> requirements) {
        this.requirements = requirements;
    }
}
