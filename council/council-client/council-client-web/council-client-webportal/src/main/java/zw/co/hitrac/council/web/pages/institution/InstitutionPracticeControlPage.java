package zw.co.hitrac.council.web.pages.institution;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.InstitutionPracticeControl;
import zw.co.hitrac.council.business.service.InstitutionPracticeControlService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;

//~--- JDK imports ------------------------------------------------------------

import java.util.Arrays;
import java.util.List;
import zw.co.hitrac.council.business.domain.Application;

/**
 *
 * @author Constance Mabaso
 */
public class InstitutionPracticeControlPage extends TemplatePage {

    @SpringBean
    private InstitutionPracticeControlService institutionPracticeControlService;

    public InstitutionPracticeControlPage(final IModel<Application> applicationModel) {
        menuPanelManager.getApplicationPanel().setTopMenuCurrent(true);
        CompoundPropertyModel<InstitutionPracticeControl> model = new CompoundPropertyModel<InstitutionPracticeControl>(
                new InstitutionPracticeControlPage.LoadableDetachableInstitutionPracticeControlModel(applicationModel.getObject()));

        model.getObject().setApplication(applicationModel.getObject());
        model.getObject().setInstitution(applicationModel.getObject().getInstitution());
        setDefaultModel(model);

        Form<InstitutionPracticeControl> form = new Form<InstitutionPracticeControl>("form",
                (IModel<InstitutionPracticeControl>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                institutionPracticeControlService.save(getModelObject());
                setResponsePage(new InstitutionApplicationViewPage(applicationModel.getObject().getId()));
            }
        };

        form.add(new TextArea<String>("comment"));
        form.add(new CheckBox("institutionMeetsStandards"));
        form.add(new CheckBox("constructionLocation"));
        form.add(new CheckBox("equipmentFacility"));
        form.add(new CheckBox("employedAndQualified"));
        form.add(new CheckBox("registerdInstitution"));
        form.add(new CheckBox("recommendationSentTOHPA"));
        List decisionlist = Arrays.asList(new String[]{InstitutionPracticeControl.DECISIONPENDING,
            InstitutionPracticeControl.DECISIONAPPROVED, InstitutionPracticeControl.DECISIONDECLINED, Application.APPLICATIONUNDERREVIEW});

        form.add(new DropDownChoice("decisionStatus", decisionlist));

        form.add(new Link<Void>("institutionViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionApplicationViewPage(applicationModel.getObject().getId()));
            }
        });
        add(form);
        add(new Label("application.institution.name"));
    }

    public InstitutionPracticeControlPage(Long id) {
        menuPanelManager.getApplicationPanel().setTopMenuCurrent(true);
        CompoundPropertyModel<InstitutionPracticeControl> model =
                new CompoundPropertyModel<InstitutionPracticeControl>(
                new InstitutionPracticeControlPage.LoadableDetachableInstitutionPracticeControlModel(id));
        final Long applicationId = model.getObject().getApplication().getId();
        setDefaultModel(model);

        Form<InstitutionPracticeControl> form = new Form<InstitutionPracticeControl>("form",
                (IModel<InstitutionPracticeControl>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                institutionPracticeControlService.save(getModelObject());
                setResponsePage(new InstitutionApplicationViewPage(applicationId));
            }
        };

        form.add(new Link<Void>("institutionViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionApplicationViewPage(applicationId));
            }
        });
        form.add(new TextArea<String>("comment"));
        form.add(new CheckBox("institutionMeetsStandards"));
        form.add(new CheckBox("constructionLocation"));
        form.add(new CheckBox("equipmentFacility"));
        form.add(new CheckBox("employedAndQualified"));
        form.add(new CheckBox("registerdInstitution"));
        form.add(new CheckBox("recommendationSentTOHPA"));
        List decisionlist = Arrays.asList(new String[]{InstitutionPracticeControl.DECISIONPENDING,
            InstitutionPracticeControl.DECISIONAPPROVED, InstitutionPracticeControl.DECISIONDECLINED, Application.APPLICATIONUNDERREVIEW});

        form.add(new DropDownChoice("decisionStatus", decisionlist).setRequired(true).add(new ErrorBehavior()));
        add(form);
        add(new Label("application.institution.name"));
    }

    private final class LoadableDetachableInstitutionPracticeControlModel
            extends LoadableDetachableModel<InstitutionPracticeControl> {

        private Long id;
        private Application application;

        public LoadableDetachableInstitutionPracticeControlModel(Application application) {
            this.application = application;
        }

        public LoadableDetachableInstitutionPracticeControlModel(Long id) {
            this.id = id;
        }

        @Override
        protected InstitutionPracticeControl load() {
            InstitutionPracticeControl institutionPracticeControl = null;

            if (id == null) {
                institutionPracticeControl = new InstitutionPracticeControl();
                institutionPracticeControl.setApplication(application);
            } else {
                institutionPracticeControl = institutionPracticeControlService.get(id);
            }

            return institutionPracticeControl;
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
