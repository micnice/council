/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.reports;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.domain.Qualification;
import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.business.domain.RegistrantActivityType;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.CouncilDurationService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegisterService;
import zw.co.hitrac.council.business.service.RegistrantActivityService;
import zw.co.hitrac.council.business.service.RegistrantQualificationService;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.reports.RegistrantDataReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 * @author tdhlakama
 */
public class RegistrantsByQualificationAndDurationPage extends TemplatePage {

    @SpringBean
    private CouncilDurationService councilDurationService;
    @SpringBean
    private RegistrantActivityService registrantActivityService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    private CouncilDuration councilDuration;
    private Qualification qualification;
    @SpringBean
    private RegisterService registerService;
    @SpringBean
    private RegistrantQualificationService registrantQualificationService;
    private ContentType contentType;

    public RegistrantsByQualificationAndDurationPage() {
        menuPanelManager.getSearchPanel().setTopMenuCurrent(true);
        menuPanelManager.getSearchPanel().setSearchAdvanced(true);

        PropertyModel<CouncilDuration> councilDurationModel = new PropertyModel<CouncilDuration>(this, "councilDuration");
        PropertyModel<Qualification> qualificationModel = new PropertyModel<Qualification>(this, "qualification");
        PropertyModel<ContentType> contentTypeModel = new PropertyModel<ContentType>(this, "contentType");
        Form<?> form = new Form("form");
        form.add(new DropDownChoice("councilDuration", councilDurationModel, councilDurationService.findAll()).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("qualification", qualificationModel, registrantQualificationService.getDistinctQualificationsInRegistrantQualification()));
        form.add(new DropDownChoice("contentType", contentTypeModel, Arrays.asList(ContentType.values())));
        add(form);

        form.add(new Button("active") {
            @Override
            public void onSubmit() {
                Set<RegistrantData> registrants = new HashSet<RegistrantData>();
                Map parameters = new HashMap();
                parameters.put("comment", qualification.getName() + ". Active Registrants for Period " + councilDuration.getName());
                parameters.put("activityType", RegistrantActivityType.RENEWAL);
                Set<Register> registers = new HashSet<Register>();
                registers.addAll(registerService.findAll());
                registers.remove(generalParametersService.get().getStudentRegister());

                for (Register r : registers) {
                    for (Duration d : councilDurationService.get(councilDuration.getId()).getDurations()) {
                        registrants.addAll(registrantActivityService.getListPerQualificationAndDurationAndRegisterAndRegistrantActivity(qualification, r, d, RegistrantActivityType.RENEWAL));
                    }
                }
                List<RegistrantData> registrantDataList = new ArrayList<RegistrantData>(registrants);
                registrantDataList = (List<RegistrantData>) HrisComparator.sortRegistrantData(registrantDataList);
                try {
                    RegistrantDataReport registrantDataReport = new RegistrantDataReport();
                    
                    ByteArrayResource resource
                            = ReportResourceUtils.getReportResource(registrantDataReport, contentType, registrantDataList,
                                    parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

        add(new FeedbackPanel("feedback"));
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
