/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.AssessmentTypeDAO;
import zw.co.hitrac.council.business.dao.repo.AssessmentTypeRepository;
import zw.co.hitrac.council.business.domain.AssessmentType;

/**
 *
 * @author kelvin
 */
@Repository
public class AssessmentTypeDAOImpl implements AssessmentTypeDAO{ 
    @Autowired
   private AssessmentTypeRepository addressTypeRepository;
    
   public AssessmentType save(AssessmentType addressType){
       return addressTypeRepository.save(addressType);
   }
   
   public List<AssessmentType> findAll(){
       return addressTypeRepository.findAll();
   }
   
   public AssessmentType get(Long id){
       return addressTypeRepository.findOne(id);
   }
 /**
     * A setter method that will make mocking repo object easier
     * @param addressTypeRepository 
     */
   
   public void setAssessmentRepository(AssessmentTypeRepository addressTypeRepository) {
         this.addressTypeRepository = addressTypeRepository;
    }

    public List<AssessmentType> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
