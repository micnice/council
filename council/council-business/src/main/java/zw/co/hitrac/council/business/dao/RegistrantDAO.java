package zw.co.hitrac.council.business.dao;

import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Takunda Dhlakama
 * @author Charles Chigoriwa
 */
public interface RegistrantDAO extends IGenericDAO<Registrant> {

    public List<RegistrantData> getRegistrantsWithBirthDays(Date date);

    public List<RegistrantData> getRegistrantsWhoOwnInstitutions();

    public List<RegistrantData> getRegistrantsWhoSuperviseInstitutions();

    public List<RegistrantData> getRegistrantsWithFirstRenewalWithoutSecondRenewal(Course course,
                                                                                   CouncilDuration firstDuration,
                                                                                   CouncilDuration secondDuration,
                                                                                   AddressType addressType);

    public List<RegistrantData> getRegistrantsWithFirstRenewalWithoutSecondRenewalInRegistration(Course course,
                                                                                   CouncilDuration firstDuration,
                                                                                   CouncilDuration secondDuration,
                                                                                   AddressType addressType);

    public boolean hasValidIdentifications(Registrant registrant, List<IdentificationValidationRule> validationRules);

    public List<Registrant> findByCitizenship(Citizenship citizenship);

    public Registrant findByIdNumber(String idNumber);

    public List<Registrant> getDeRegistrants();
    
    public List<RegistrantData> getVoidedRegistrants();
    
    public List<RegistrantData> getDeceasedRegistrants();
    
    public List<RegistrantData> getRetiredRegistrants();
    
    public List<RegistrantData> getRegistrants();
    
    public void createRegistrantAccounts();
    
    public List<Registrant> getApprovingRegistrants();

    public Registrant getRegistrant(Customer customer);

    public Registrant createUids(Registrant registrant);

    public List<RegistrantData> getRegistrantAgeByParameter(Integer lowerLimit, Integer upperLimit);

    public List<Registrant> getRegistrants(String query, String gender, Citizenship citizenship, Boolean dead);
    
    public List<RegistrantData> getRegistrantList(String query, String gender, Citizenship citizenship, Boolean dead);
    
    public List<RegistrantData> getSupervisorList(String query, Boolean dead);

    public Registrant getRegistrantByAccount(Customer customer);
    
    public Registrant getRegistrantByAccountData(Account account);

    public Boolean getUniqueRegistrationNumber(String registrationNumber);

    public Boolean getRegistrantIdentificationNumber(String idNumber);

    public Registrant getRegistrantByRegistrationNumber(String registrationNumber);

    public Registrant getRegistrantByIdentificationNumber(String idNumber);

    public List<Long> getIds();
    
    public List<Registrant> getRegistrantWithDebts();

    public List<Registrant> getRegistrantWithCarryForwards();
    
    public List<RegistrantData> getRegistrantCorrections();
    
    public List<RegistrantData> getRegistrantDateCorrections();

    public List<Registrant> getRegistrantsWithNoOrInvalidNationalIDs(Citizenship citizenship,
                                                                     IdentificationType nationalIdentificationType);

    public List<RegistrantData> getRegistrantsWithConditions(CouncilDuration councilDuration);
    
    public List<RegistrantData> getRegistrantsWithConditions(CouncilDuration councilDuration, Conditions condition);
}
