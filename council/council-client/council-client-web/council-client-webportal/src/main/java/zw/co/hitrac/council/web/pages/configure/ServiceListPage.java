package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Service;
import zw.co.hitrac.council.business.service.ServiceService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;

/**
 *
 * @author Charles Chigoriwa
 */
public class ServiceListPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private ServiceService serviceService;

    public ServiceListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new ServiceEditPage(null));
            }
        });

        IModel<List<Service>> model = new LoadableDetachableModel<List<Service>>() {
            @Override
            protected List<Service> load() {
                return serviceService.findAll();
            }
        };
        PropertyListView<Service> eachItem = new PropertyListView<Service>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<Service> item) {
                Link<Service> viewLink = new Link<Service>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new ServiceViewPage(getModelObject().getId()));
                    }
                };

                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };

        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
