package zw.co.hitrac.council.business.service.accounts;

import zw.co.hitrac.council.business.domain.accounts.TransactionComponent;
import zw.co.hitrac.council.business.service.IGenericService;

/**
 *
 * @author Judge Muzinda
 * @author Constance Mabaso
 */
public interface TransactionComponentService extends IGenericService<TransactionComponent> {
    
}
