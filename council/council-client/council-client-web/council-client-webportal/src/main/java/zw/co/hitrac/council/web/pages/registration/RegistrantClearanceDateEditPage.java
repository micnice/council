package zw.co.hitrac.council.web.pages.registration;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.RegistrantClearanceService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Takunda Dhlakama
 */
public class RegistrantClearanceDateEditPage extends TemplatePage {

    @SpringBean
    private RegistrantClearanceService registrantClearanceService;
    @SpringBean
    private RegistrationService registrationService;
    @SpringBean
    private RegistrationProcess registrationProcess;
    @SpringBean
    private CourseService courseService;
    private Course course;
    private Date clearanceDate;

    public RegistrantClearanceDateEditPage(final IModel<Registrant> registrantModel) {

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());
        add(new RegistrantPanel("registrantPanel", registrantModel.getObject().getId()));
        PropertyModel<Course> courseModel = new PropertyModel<Course>(this, "course");
        PropertyModel<Date> clearanceDateModel = new PropertyModel<Date>(this, "clearanceDate");

        Form<?> form = new Form("form");

        form.add(new DropDownChoice("course", courseModel, courseService.findAll()));
        form.add(new TextField<Date>("clearanceDate", clearanceDateModel).setRequired(true).add(DatePickerUtil.getDatePicker()).add(new ErrorBehavior()));

        form.add(new Button("process") {
            @Override
            public void onSubmit() {
                try {

                    registrationProcess.editClearanceDate(registrantModel.getObject(), course, clearanceDate);

                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        });

        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });

        add(new FeedbackPanel("feedback"));
        add(form);

    }
}
