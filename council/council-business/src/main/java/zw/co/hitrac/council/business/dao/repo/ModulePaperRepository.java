package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.ModulePaper;

/**
 *
 * @author Charles Chigoriwa
 */
public interface ModulePaperRepository extends CrudRepository<ModulePaper, Long> {

    public List<ModulePaper> findAll();
}
