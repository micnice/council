
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.examinations;

import com.googlecode.wicket.jquery.ui.panel.JQueryFeedbackPanel;

import java.util.ArrayList;

import net.sf.jasperreports.engine.JRException;

import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationService;
import zw.co.hitrac.council.business.service.examinations.ExamSettingService;
import zw.co.hitrac.council.reports.*;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

//~--- JDK imports ------------------------------------------------------------
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.ModulePaper;
import zw.co.hitrac.council.business.domain.examinations.Exam;
import zw.co.hitrac.council.business.domain.examinations.ExamPassRate;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.business.domain.examinations.ExamResult;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.ModulePaperService;
import zw.co.hitrac.council.business.service.examinations.ExamResultService;
import zw.co.hitrac.council.business.service.examinations.ExamService;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.web.models.CourseListModel;
import zw.co.hitrac.council.web.models.ExamSettingListModel;
import zw.co.hitrac.council.web.models.TrainingInstitutionstModel;

/**
 * @author tdhlakama
 */
public class ExaminationMasterPage extends IExaminationsPage {

    @SpringBean
    private ExamRegistrationService examRegistrationService;
    @SpringBean
    private ExamResultService examResultService;
    @SpringBean
    private ExamSettingService examSettingService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private CourseService courseService;
    private Institution institution;
    private ExamSetting examSetting;
    private String candidateNumber;
    private Course course;
    private HrisComparator hrisComparator = new HrisComparator();
    @SpringBean
    private ModulePaperService modulePaperService;
    @SpringBean
    private ExamService examService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    private String paper;
    private List<String> typesOfPapers;

    private static String ALL = "All";
    private static String PAPERONE = "PAPER ONE";
    private static String PAPERTWO = "PAPER TWO";
    private static String PAPERTHREE = "PAPER THREE";

    public ExaminationMasterPage() {
        // FeedbackPanel //
        initPapers();
        final FeedbackPanel feedback = new JQueryFeedbackPanel("feedback");
        add(feedback.setOutputMarkupId(true));
        PropertyModel<Institution> institutionModel = new PropertyModel<Institution>(this, "institution");
        PropertyModel<ExamSetting> examSettingModel = new PropertyModel<ExamSetting>(this, "examSetting");
        PropertyModel<Course> courseModel = new PropertyModel<Course>(this, "course");
        PropertyModel<String> paperModel = new PropertyModel<String>(this, "paper");
        Form<?> form = new Form("form");
        form.add(new DropDownChoice("institution", institutionModel, new InstitutionsModel()));
        form.add(new DropDownChoice("examSetting", examSettingModel, new ExamSettingListModel(examSettingService)));
        form.add(new DropDownChoice("course", courseModel, new CourseListModel(courseService), new ChoiceRenderer<Course>()) {
            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }
        });
        form.add(new DropDownChoice("paper", paperModel, typesOfPapers) {
            @Override
            public boolean isNullValid() {
                return true;
            }
        });
        form.add(new Button("masterlist") {
            @Override
            public void onSubmit() {
                try {
                    if (examSetting == null) {
                        error("Select Exam Period");
                        return;
                    }
                    List<ExamRegistration> examRegistrations = examRegistrationService.getAllExamCandidates(examSetting, null, null, null, null, null, null, null, null, null);

                    ExaminationMasterList examinationMasterList = new ExaminationMasterList();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    parameters.put("comment", "EXAMINATION MASTER LIST - List of all Candidates who registered for " + examSetting.toString() + " Final Examination");

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(examinationMasterList,
                            contentType, examRegistrations, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("supplementarylist") {
            @Override
            public void onSubmit() {
                try {
                    if (examSetting == null) {
                        error("Select Exam Period");
                        return;
                    }
                    List<ExamRegistration> examRegistrations = examRegistrationService.getAllExamCandidates(examSetting, null, null, null, null, Boolean.TRUE, null, null, Boolean.FALSE, null);

                    ExaminationSupplementaryList examinationSupplementaryList = new ExaminationSupplementaryList();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    parameters.put("comment", "List of candidates who are writing " + examSetting.toString() + " Supplementary Examination");

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(examinationSupplementaryList,
                            contentType, examRegistrations, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("supplementarySummary") {
            @Override
            public void onSubmit() {
                try {
                    if (examSetting == null) {
                        error("Select Exam Period");
                        return;
                    }
                    List<ExamPassRate> rates = new ArrayList<ExamPassRate>();
                    for (Course c : courseService.findAll()) {
                        ExamPassRate examPassRate = new ExamPassRate();
                        examPassRate.setTotalNumberOfCandidates(examRegistrationService.getTotalNumberOfCandidates(examSetting, c, institution, null, null, Boolean.TRUE, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE));
                        examPassRate.setExamSetting(examSetting);
                        examPassRate.setCourse(c);
                        //take only supplementary registrations
                        for (ExamRegistration e : examRegistrationService.getAllExamCandidates(examSetting, c, null, null, null, Boolean.TRUE, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, null)) {
                            if (e.getPassStatus() && e.getCompletedStatus()) {
                                examPassRate.setNumberOfCandidatesPassed(examPassRate.getNumberOfCandidatesPassed() + 1);
                            }
                            if (!e.getPassStatus() && e.getCompletedStatus()) {
                                examPassRate.setNumberOfCandidatesFailed(examPassRate.getNumberOfCandidatesFailed() + 1);
                            }
                        }
                        rates.add(examPassRate);
                    }
                    CoursePassRateReport supplementaryPassRateReport = new CoursePassRateReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    parameters.put("comment", "Supplemenatry Examination Pass Rate ");

                    if (examSetting != null) {
                        parameters.put("examSetting", examSetting.toString());
                    }

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(supplementaryPassRateReport,
                            contentType, rates, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("supplementaryCourselist") {
            @Override
            public void onSubmit() {
                try {
                    if (examSetting == null) {
                        error("Select Exam Period");
                        return;
                    }
                    if (course == null) {
                        error("Select Exam Course");
                        return;
                    }
                    List<ExamRegistration> examRegistrations = examRegistrationService.getAllExamCandidates(examSetting, course, null, null, null, Boolean.TRUE, null, null, Boolean.FALSE, null);

                    ExaminationSupplementaryList examinationSupplementaryList = new ExaminationSupplementaryList();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    parameters.put("comment", "List of candidates who are writing " + examSetting.toString() + " Supplementary Examination");

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(examinationSupplementaryList,
                            contentType, examRegistrations, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("supplementPerSchool") {
            @Override
            public void onSubmit() {
                try {
                    if (examSetting == null) {
                        error("Select Exam Period");
                        return;
                    }
                    if (course == null) {
                        error("Select Exam Course");
                        return;
                    }

                    if (institution == null) {
                        error("Select Institution");
                        return;
                    }
                    List<ExamRegistration> examRegistrations = examRegistrationService.getAllExamCandidates(examSetting, course, institution, null, null, Boolean.TRUE, null, null, Boolean.FALSE, null);

                    ExaminationSupplementaryList examinationSupplementaryList = new ExaminationSupplementaryList();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    parameters.put("comment", "List of candidates who are writing " + examSetting.toString() + " Supplementary Examination at " + institution);

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(examinationSupplementaryList,
                            contentType, examRegistrations, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("fullExam") {
            @Override
            public void onSubmit() {
                try {
                    if (examSetting == null) {
                        error("Select Exam Period");
                        return;
                    }
                    List<ExamRegistration> examRegistrations = examRegistrationService.getAllExamCandidates(examSetting, null, null, null, null, Boolean.FALSE, null, null, Boolean.FALSE, null);

                    ExaminationMasterList examinationMasterList = new ExaminationMasterList();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    parameters.put("comment", "List of candidates who are writing " + examSetting.toString() + " Full Examination");

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(examinationMasterList,
                            contentType, examRegistrations, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("passlist") {
            @Override
            public void onSubmit() {
                try {
                    if (examSetting == null) {
                        error("Select Exam Period");
                        return;
                    }
                    List<ExamRegistration> passed = new ArrayList<ExamRegistration>();

                    for (ExamRegistration e : examRegistrationService.getAllExamCandidates(examSetting, null, null, null, null, null, null, null, Boolean.FALSE, null)) {
                        if (e.getPassStatus() && e.getCompletedStatus()) {
                            passed.add(e);
                        }
                    }

                    passed = passed.stream().filter(e -> e.getDisqualified()).collect(Collectors.toList());

                    ExaminationMasterList examinationMasterList = new ExaminationMasterList();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    parameters.put("comment", "EXAMINATION MASTER PASS LIST - List of all Candidates who passed. ( " + examSetting.toString() + " )");

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(examinationMasterList,
                            contentType, passed, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("unclosedlist") {
            @Override
            public void onSubmit() {
                try {
                    if (examSetting == null) {
                        error("Select Exam Period");
                        return;
                    }
                    List<ExamRegistration> unclosed = examRegistrationService.getAllExamCandidates(examSetting, null, null, null, null, null, null, Boolean.FALSE, Boolean.FALSE, null);

                    ExaminationMasterList examinationMasterList = new ExaminationMasterList();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    parameters.put("comment", "Examination Candidates not Closed  ( " + examSetting.toString() + " )");

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(examinationMasterList,
                            contentType, unclosed, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("unclosedByCourse") {
            @Override
            public void onSubmit() {
                try {
                    if (examSetting == null) {
                        error("Select Exam Period");
                        return;
                    }

                    if (course == null) {
                        error("Select Course");
                        return;
                    }
                    List<ExamRegistration> unclosed = examRegistrationService.getAllExamCandidates(examSetting, course, null, null, null, null, null, Boolean.FALSE, Boolean.FALSE, null);

                    ExaminationMasterList examinationMasterList = new ExaminationMasterList();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    parameters.put("comment", "Examination Candidates not Closed  ( " + examSetting.toString() + " )" + course);

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(examinationMasterList,
                            contentType, unclosed, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("failedlist") {
            @Override
            public void onSubmit() {
                try {
                    if (examSetting == null) {
                        error("Select Exam Period");
                        return;
                    }

                    List<ExamRegistration> supplementlist = new ArrayList<ExamRegistration>();

                    for (ExamRegistration e : examRegistrationService.getAllExamCandidates(examSetting, null, null, null, null, null, null, null, Boolean.FALSE, null)) {
                        if (!e.getPassStatus() && e.getCompletedStatus()) {
                            supplementlist.add(e);
                        }
                    }

                    ExaminationMasterList examinationMasterList = new ExaminationMasterList();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    parameters.put("comment", "List of all Candidates who Failed. ( " + examSetting.toString() + " )");

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(examinationMasterList,
                            contentType, supplementlist, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("supplementlist") {
            @Override
            public void onSubmit() {
                try {
                    if (examSetting == null) {
                        error("Select Exam Period");
                        return;
                    }

                    List<ExamRegistration> supplementlist = new ArrayList<ExamRegistration>();

                    for (ExamRegistration e : examRegistrationService.getAllExamCandidates(examSetting, null, null, null, null, null, null, null, Boolean.FALSE, null)) {
                        if (!e.getPassStatus() && e.getCompletedStatus()) {
                            supplementlist.add(e);
                        }
                    }

                    List<ExamRegistration> councilAprroval = new ArrayList<ExamRegistration>();
                    for (ExamRegistration e : supplementlist) {
                        if (examRegistrationService.getNumberofRewrites(e.getRegistration()) > generalParametersService.get().getNumberOfTimesToWriteSupExam()) {
                            councilAprroval.add(e);
                        }
                    }
                    supplementlist.removeAll(councilAprroval);

                    ExaminationMasterList examinationMasterList = new ExaminationMasterList();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    parameters.put("comment", "List of all Candidates who Failed and need to supplement ( " + examSetting.toString() + " )");

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(examinationMasterList,
                            contentType, supplementlist, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("suppressed") {
            @Override
            public void onSubmit() {
                try {
                    if (examSetting == null) {
                        error("Select Exam Period");
                        return;
                    }

                    List<ExamRegistration> supplementlist = new ArrayList<ExamRegistration>();

                    supplementlist = examRegistrationService.getAllExamCandidates(examSetting, null, null, null, null, null, Boolean.TRUE, null, Boolean.FALSE, null);

                    ExaminationMasterList examinationMasterList = new ExaminationMasterList();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    parameters.put("comment", "List of all Candidates who have Results Suppressed . ( " + examSetting.toString() + " )");

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(examinationMasterList,
                            contentType, supplementlist, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("supplementApprovallist") {
            @Override
            public void onSubmit() {
                try {
                    if (examSetting == null) {
                        error("Select Exam Period");
                        return;
                    }

                    List<ExamRegistration> failed = new ArrayList<ExamRegistration>();
                    List<ExamRegistration> councilAprroval = new ArrayList<ExamRegistration>();
                    for (ExamRegistration e : examRegistrationService.getAllExamCandidates(examSetting, course, null, null, null, null, null, null, Boolean.FALSE, null)) {
                        if (!e.getPassStatus() && e.getCompletedStatus()) {
                            failed.add(e);
                        }
                    }

                    for (ExamRegistration e : failed) {
                        if (examRegistrationService.getNumberofRewrites(e.getRegistration()) > generalParametersService.get().getNumberOfTimesToWriteSupExam()) {
                            councilAprroval.add(e);
                        }
                    }

                    ExaminationMasterList examinationMasterList = new ExaminationMasterList();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    parameters.put("comment", "List of all Candidates who need Council Approval to Supplement . ( " + examSetting.toString() + " )");

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(examinationMasterList,
                            contentType, councilAprroval, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("confirmationList") {
            @Override
            public void onSubmit() {
                try {
                    if (examSetting == null) {
                        error("Select Exam Period");
                        return;
                    }

                    if (course == null) {
                        error("Select Course");
                        return;
                    }

                    if (institution == null) {
                        error("Select Institution");
                        return;
                    }
                    ConfirmationListReport confirmationListReport = new ConfirmationListReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    if (course.getQualification() != null) {
                        parameters.put("course", course.getQualification().getName());
                    }
                    if (examSetting != null) {
                        parameters.put("examSetting", examSetting.toString());
                    }

                    if (institution != null) {
                        parameters.put("institution", institution.toString());
                    }

                    ByteArrayResource resource;
                    List<ExamRegistration> examRegistrations = (List<ExamRegistration>) hrisComparator.sortExamRegistrationBySurname(examRegistrationService.getAllExamCandidates(examSetting, course, institution, candidateNumber, null, null, Boolean.FALSE, null, Boolean.FALSE, null));
                    resource = ReportResourceUtils.getReportResource(confirmationListReport, contentType, examRegistrations, parameters);

                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

        form.add(new Button("entryList") {
            @Override
            public void onSubmit() {
                try {
                    if (examSetting == null) {
                        error("Select Exam Period");
                        return;
                    }
                    if (course == null) {
                        error("Select Course");
                        return;
                    }
                    if (institution == null) {
                        error("Select Institution");
                        return;
                    }
                    if (paper == null) {
                        error("Select Paper");
                        return;
                    }

                    List<ExamRegistration> examRegistrations = (List<ExamRegistration>) hrisComparator.sortExamRegistrationBySurname(examRegistrationService.getAllExamCandidates(examSetting, course, institution, candidateNumber, null, null, Boolean.FALSE, null, Boolean.FALSE, null));

                    if (paper.equals(PAPERONE)) {
                        examRegistrations = examRegistrations.stream().filter(e -> e.getWritingOne()).collect(Collectors.toList());
                    }
                    if (paper.equals(PAPERTWO)) {
                        examRegistrations = examRegistrations.stream().filter(e -> e.getWritingPaperTwo()).collect(Collectors.toList());
                    }
                    if (paper.equals(PAPERTHREE)) {
                        examRegistrations = examRegistrations.stream().filter(e -> e.getWritingPaperThree()).collect(Collectors.toList());
                    }

                    ExamEntryListReport entryListReport = new ExamEntryListReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    if (course.getQualification() != null) {
                        parameters.put("course", course.getQualification().getName());
                    } else {
                        parameters.put("course", course.getName());
                    }

                    if (examSetting != null) {
                        parameters.put("examSetting", examSetting.toString());
                    }

                    if (institution != null) {
                        parameters.put("institution", institution.toString());
                    }

                    if (paper != null) {
                        String title = "EXAMINATION ENTRY LIST";
                        if (paper.equals(ALL)) {
                            parameters.put("title", title);
                        }
                        if (paper.equals(PAPERONE)) {
                            title = title.concat(" - " + PAPERONE);
                            parameters.put("title", title);
                        }
                        if (paper.equals(PAPERTWO)) {
                            title = title.concat(" - " + PAPERTWO);
                            parameters.put("title", title);
                        }
                        if (paper.equals(PAPERTHREE)) {
                            title = title.concat(" - " + PAPERTHREE);
                            parameters.put("title", title);
                        }
                    }

                    String comment = "Candidates to sign on the day of examination at the venue";
                    parameters.put("comment", comment);

                    ByteArrayResource resource;

                    resource = ReportResourceUtils.getReportResource(entryListReport, contentType,
                            examRegistrations, parameters);

                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

        form.add(new Button("examinationCards") {
            @Override
            public void onSubmit() {
                try {
                    if (examSetting == null) {
                        error("Select Exam Period");
                        return;
                    }
                    if (course == null) {
                        error("Select Exam Course");
                        return;
                    }
                    if (institution == null) {
                        error("Select Institution");
                        return;
                    }
                    Set<ExamResult> examResults = new HashSet<ExamResult>();
                    for (ExamResult er : examRegistrationService.getExamResults(examSetting, course, institution, candidateNumber, null, null)) {
                        if (er.getPreviousExamRegistration() == null) {
                            examResults.add(er);
                        }
                    }
                    List<ExamResult> results = (List<ExamResult>) hrisComparator.sortExaminationCards(new ArrayList<ExamResult>(examResults));

                    ExaminationCardReport examinationCardReport = new ExaminationCardReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(examinationCardReport, contentType, results, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

        form.add(new Button("markeredList") {
            @Override
            public void onSubmit() {
                try {
                    if (examSetting == null) {
                        error("Select Exam Period");
                        return;
                    }
                    if (course == null) {
                        error("Select Course");
                        return;
                    }
                    List<ExamRegistration> examRegistrations = examRegistrationService.getAllExamCandidates(examSetting, course, null, null, null, null, null, null, Boolean.FALSE, null);
                    ExaminationMarkeredReport examinationMarkeredReport = new ExaminationMarkeredReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    if (course.getQualification() != null) {
                        parameters.put("course", course.getQualification().getName());
                    } else {
                        parameters.put("course", course.getName());
                    }

                    if (examSetting != null) {
                        parameters.put("examSetting", examSetting.toString());
                    }

                    ByteArrayResource resource;

                    resource = ReportResourceUtils.getReportResource(examinationMarkeredReport, contentType,
                            examRegistrations, parameters);

                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    Logger.getLogger(ExaminationMarkerPage.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        form.add(new Button("markeredGroupBySchoolList") {
            @Override
            public void onSubmit() {
                try {
                    if (examSetting == null) {
                        error("Select Exam Period");
                        return;
                    }
                    if (course == null) {
                        error("Select Course");
                        return;
                    }
                    List<ExamRegistration> examRegistrations = examRegistrationService.getAllExamCandidates(examSetting, course, null, null, null, null, null, null, Boolean.FALSE, null);
                    List<ExamRegistration> bySchoolRegistrations = (List<ExamRegistration>) hrisComparator.sortExamRegistrationBySchoolName(examRegistrations);
                    ExaminationMarkeredBySchoolReport examinationMarkeredBySchoolReport = new ExaminationMarkeredBySchoolReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    if (course.getQualification() != null) {
                        parameters.put("course", course.getQualification().getName());
                    } else {
                        parameters.put("course", course.getName());
                    }

                    if (examSetting != null) {
                        parameters.put("examSetting", examSetting.toString());
                    }

                    ByteArrayResource resource;

                    resource = ReportResourceUtils.getReportResource(examinationMarkeredBySchoolReport, contentType,
                            bySchoolRegistrations, parameters);

                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    Logger.getLogger(ExaminationMarkerPage.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        form.add(new Button("markeredGroupBySchoolListSheet") {
            @Override
            public void onSubmit() {
                try {
                    if (examSetting == null) {
                        error("Select Exam Period");
                        return;
                    }
                    if (course == null) {
                        error("Select Course");
                        return;
                    }
                    List<ExamRegistration> examRegistrations = examRegistrationService.getAllExamCandidates(examSetting, course, null, null, null, null, null, null, Boolean.FALSE, null);
                    List<ExamRegistration> bySchoolRegistrations = (List<ExamRegistration>) hrisComparator.sortExamRegistrationBySchoolName(examRegistrations);
                    ExaminationMarkeredBySchoolSheetReport examinationMarkeredBySchoolReport = new ExaminationMarkeredBySchoolSheetReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    if (course.getQualification() != null) {
                        parameters.put("course", course.getQualification().getName());
                    } else {
                        parameters.put("course", course.getName());
                    }

                    if (examSetting != null) {
                        parameters.put("examSetting", examSetting.toString());
                    }

                    if (courseService.getNumberOfModulePapers(course) == 3) {
                        parameters.put("hasPaperThree", Boolean.TRUE);
                    }

                    ByteArrayResource resource;

                    resource = ReportResourceUtils.getReportResource(examinationMarkeredBySchoolReport, contentType,
                            bySchoolRegistrations, parameters);

                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    Logger.getLogger(ExaminationMarkerPage.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        form.add(new Button("markeredGroupBySchoolListSheetFull") {
            @Override
            public void onSubmit() {
                try {
                    if (examSetting == null) {
                        error("Select Exam Period");
                        return;
                    }
                    if (course == null) {
                        error("Select Course");
                        return;
                    }
                    List<ExamRegistration> examRegistrations = examRegistrationService.getAllExamCandidates(examSetting, course, null, null, null, null, null, null, Boolean.FALSE, null);
                    List<ExamRegistration> bySchoolRegistrations = (List<ExamRegistration>) hrisComparator.sortExamRegistrationBySchoolName(examRegistrations);
                    ExaminationMarkeredBySchoolSheetReportFull examinationMarkeredBySchoolReport = new ExaminationMarkeredBySchoolSheetReportFull();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    if (course.getQualification() != null) {
                        parameters.put("course", course.getQualification().getName());
                    } else {
                        parameters.put("course", course.getName());
                    }

                    if (examSetting != null) {
                        parameters.put("examSetting", examSetting.toString());
                    }

                    if (courseService.getNumberOfModulePapers(course) == 3) {
                        parameters.put("hasPaperThree", Boolean.TRUE);
                    }

                    ByteArrayResource resource;

                    resource = ReportResourceUtils.getReportResource(examinationMarkeredBySchoolReport, contentType,
                            bySchoolRegistrations, parameters);

                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    Logger.getLogger(ExaminationMarkerPage.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        form.add(new Button("markeredSchoolList") {
            @Override
            public void onSubmit() {
                try {
                    if (examSetting == null) {
                        error("Select Exam Period");
                        return;
                    }
                    if (course == null) {
                        error("Select Course");
                        return;
                    }

                    if (institution == null) {
                        error("Select Institution");
                        return;
                    }
                    List<ExamRegistration> examRegistrations = (List<ExamRegistration>) hrisComparator.sortExamRegistrationBySurname(examRegistrationService.getAllExamCandidates(examSetting, course, institution, null, null, null, Boolean.FALSE, null, Boolean.FALSE, null));
                    ExaminationMarkeredBySchoolReport examinationMarkeredBySchoolReport = new ExaminationMarkeredBySchoolReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    if (course.getQualification() != null) {
                        parameters.put("course", course.getQualification().getName());
                    } else {
                        parameters.put("course", course.getName());
                    }

                    if (examSetting != null) {
                        parameters.put("examSetting", examSetting.toString());
                    }

                    if (institution != null) {
                        parameters.put("institution", institution.getName());
                    }

                    if (courseService.getNumberOfModulePapers(course) == 3) {
                        parameters.put("hasPaperThree", Boolean.TRUE);
                    }

                    ByteArrayResource resource;

                    resource = ReportResourceUtils.getReportResource(examinationMarkeredBySchoolReport, contentType,
                            examRegistrations, parameters);

                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    Logger.getLogger(ExaminationMarkerPage.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        form.add(new Button("examinationMarkedCommentReport") {
            @Override
            public void onSubmit() {
                try {
                    if (examSetting == null) {
                        error("Select Exam Period");
                        return;
                    }
                    if (course == null) {
                        error("Select Course");
                        return;
                    }
                    List<ExamRegistration> examRegistrations = examRegistrationService.getAllExamCandidates(examSetting, course, null, null, null, null, null, null, Boolean.FALSE, null);
                    List<ExamRegistration> bySchoolRegistrations = (List<ExamRegistration>) hrisComparator.sortExamRegistrationBySchoolName(examRegistrations);
                    ExaminationMarkedCommentReport examinationMarkedCommentReport = new ExaminationMarkedCommentReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    if (course.getQualification() != null) {
                        parameters.put("course", course.getQualification().getName());
                    } else {
                        parameters.put("course", course.getName());
                    }

                    if (examSetting != null) {
                        parameters.put("examSetting", examSetting.toString());
                    }

                    ByteArrayResource resource;

                    resource = ReportResourceUtils.getReportResource(examinationMarkedCommentReport, contentType,
                            bySchoolRegistrations, parameters);

                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    Logger.getLogger(ExaminationMarkerPage.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        form.add(new Button("examinationMarkeredBySchoolSheet") {
            @Override
            public void onSubmit() {
                try {
                    if (examSetting == null) {
                        error("Select Exam Period");
                        return;
                    }
                    if (course == null) {
                        error("Select Course");
                        return;
                    }

                    if (institution == null) {
                        error("Select Institution");
                        return;
                    }

                    List<ExamRegistration> examRegistrations = examRegistrationService.getAllExamCandidates(examSetting, course, institution, null, null, null, null, null, Boolean.FALSE, null);
                    List<ExamRegistration> bySchoolRegistrations = (List<ExamRegistration>) hrisComparator.sortExamRegistrationBySchoolName(examRegistrations);
                    ExaminationMarkeredBySchoolSheet examinationMarkeredBySchoolSheet = new ExaminationMarkeredBySchoolSheet();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    if (course.getQualification() != null) {
                        parameters.put("course", course.getQualification().getName());
                    } else {
                        parameters.put("course", course.getName());
                    }

                    if (examSetting != null) {
                        parameters.put("examSetting", examSetting.toString());
                    }

                    if (institution != null) {
                        parameters.put("institution", institution.getSchoolName());
                    }

                    List<ExamRegistration> passed = new ArrayList<ExamRegistration>();

                    for (ExamRegistration e : bySchoolRegistrations) {
                        if (e.getPassStatus() && e.getCompletedStatus()) {
                            passed.add(e);
                        }
                    }
                    ///////////////////institution Top Candidates//////////////////
                    if (examRegistrationService.getTotalNumberOfCandidates(examSetting, course, institution, null, null, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE) == 0) {
                        //sort candidates marks in acsending order
                        List<ExamRegistration> finalInstituionPositions = new ArrayList<ExamRegistration>();
                        List<ExamRegistration> instituionPositions = ExamRegistration.sortInstitutionExamRegistrations(passed);

                        for (ExamRegistration ex : instituionPositions) {
                            if (ex.getInstitutionPosition() < 4) {
                                finalInstituionPositions.add(ex);
                            }
                        }
                        finalInstituionPositions = (List<ExamRegistration>) hrisComparator.sortExamCandidateMarks(finalInstituionPositions);
                        parameters.put("SUB_DATA_SOURCE", finalInstituionPositions);
                    }

                    ByteArrayResource resource;

                    resource = ReportResourceUtils.getReportResource(examinationMarkeredBySchoolSheet, contentType,
                            bySchoolRegistrations, parameters);

                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    Logger.getLogger(ExaminationMarkerPage.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        form.add(new Button("examinationCandidateMarkedComment") {
            @Override
            public void onSubmit() {
                try {
                    if (examSetting == null) {
                        error("Select Exam Period");
                        return;
                    }
                    if (course == null) {
                        error("Select Course");
                        return;
                    }
                    List<ExamRegistration> examRegistrations = examRegistrationService.getAllExamCandidates(examSetting, course, null, null, null, null, null, null, Boolean.FALSE, false);
                    List<ExamRegistration> bySchoolRegistrations = (List<ExamRegistration>) hrisComparator.sortExamRegistrationBySchoolName(examRegistrations);
                    ExaminationCandidateMarkedComment examinationCandidateMarkedComment = new ExaminationCandidateMarkedComment();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    if (course.getQualification() != null) {
                        parameters.put("course", course.getQualification().getName());
                    } else {
                        parameters.put("course", course.getName());
                    }

                    if (examSetting != null) {
                        parameters.put("examSetting", examSetting.toString());
                    }

                    ByteArrayResource resource;

                    resource = ReportResourceUtils.getReportResource(examinationCandidateMarkedComment, contentType,
                            bySchoolRegistrations, parameters);

                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    Logger.getLogger(ExaminationMarkerPage.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        form.add(new Button("allCandidatesWhoPassed") {
            @Override
            public void onSubmit() {
                if (examSetting == null) {
                    error("Select Exam Period");
                    return;
                }

                try {
                    ///////////////////Successful Candidates//////////////////                    
                    List<ExamRegistration> passed = new ArrayList<ExamRegistration>();
                    List<ExamRegistration> institutionsExamResults = examRegistrationService.getAllExamCandidates(examSetting, null, null, null, null, null, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, false);
                    for (ExamRegistration e : institutionsExamResults) {
                        if (e.getPassStatus() && e.getCompletedStatus()) {
                            passed.add(e);
                        }
                    }
                    ExamEntryListAllReport examEntryListAllReport = new ExamEntryListAllReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    parameters.put("comment", "All Candidates Who Passed " + examSetting.toString() + " Examinations");

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(examEntryListAllReport, contentType, (List<ExamRegistration>) hrisComparator.sortExamRegistrationBySurname(passed), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("allCandidatesWhoFailed") {
            @Override
            public void onSubmit() {
                if (examSetting == null) {
                    error("Select Exam Period");
                    return;
                }

                try {
                    ///////////////////Successful Candidates//////////////////                    
                    List<ExamRegistration> failed = new ArrayList<ExamRegistration>();
                    List<ExamRegistration> institutionsExamResults = examRegistrationService.getAllExamCandidates(examSetting, null, null, null, null, null, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, false);
                    for (ExamRegistration e : institutionsExamResults) {
                        if (!e.getPassStatus() && e.getCompletedStatus()) {
                            failed.add(e);
                        }

                    }
                    ExamEntryListAllReport examEntryListAllReport = new ExamEntryListAllReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    parameters.put("comment", "All Candidates Who Failed " + examSetting.toString() + " Examinations");

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(examEntryListAllReport, contentType, (List<ExamRegistration>) hrisComparator.sortExamRegistrationBySurname(failed), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("allCandidatesWhoRegisteredPerCourse") {
            @Override
            public void onSubmit() {
                if (examSetting == null) {
                    error("Select Exam Period");
                    return;
                }
                if (course == null) {
                    error("Select Course");
                    return;
                }

                try {
                    ///////////////////Successful Candidates//////////////////                    
                    List<ExamRegistration> registered = examRegistrationService.getAllExamCandidates(examSetting, course, null, null, null, null, Boolean.FALSE, null, Boolean.FALSE, null);

                    ExamEntryListAllReport examEntryListAllReport = new ExamEntryListAllReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    if (course.getQualification() != null) {
                        parameters.put("course", course.getQualification().getName());
                    } else {
                        parameters.put("course", course.getName());
                    }

                    parameters.put("comment", "All Candidates Who Registered " + examSetting.toString() + " Examinations");

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(examEntryListAllReport, contentType, (List<ExamRegistration>) hrisComparator.sortExamRegistrationBySurname(registered), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("allCandidatesWhoRegistered") {
            @Override
            public void onSubmit() {
                if (examSetting == null) {
                    error("Select Exam Period");
                    return;
                }

                try {
                    ///////////////////Successful Candidates//////////////////                    
                    List<ExamRegistration> registered = examRegistrationService.getAllExamCandidates(examSetting, null, null, null, null, null, Boolean.FALSE, null, Boolean.FALSE, null);

                    ExamEntryListAllReport examEntryListAllReport = new ExamEntryListAllReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    parameters.put("comment", "All Candidates Who Registered " + examSetting.toString() + " Examinations");

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(examEntryListAllReport, contentType, (List<ExamRegistration>) hrisComparator.sortExamRegistrationBySurname(registered), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("allWhoPassedPerCourse") {
            @Override
            public void onSubmit() {
                if (examSetting == null) {
                    error("Select Exam Period");
                    return;
                }
                if (course == null) {
                    error("Select Course");
                    return;
                }

                try {
                    ///////////////////Successful Candidates//////////////////                    
                    List<ExamRegistration> passed = new ArrayList<ExamRegistration>();
                    List<ExamRegistration> institutionsExamResults = examRegistrationService.getAllExamCandidates(examSetting, course, null, null, null, null, Boolean.FALSE, null, Boolean.FALSE, false);
                    for (ExamRegistration e : institutionsExamResults) {
                        if (e.getPassStatus() && e.getCompletedStatus()) {
                            passed.add(e);
                        }

                    }
                    ExamEntryListAllReport examEntryListAllReport = new ExamEntryListAllReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    if (course.getQualification() != null) {
                        parameters.put("course", course.getQualification().getName());
                    } else {
                        parameters.put("course", course.getName());
                    }

                    parameters.put("comment", "All Candidates Who Registered " + examSetting.toString() + " Examinations");

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(examEntryListAllReport, contentType, (List<ExamRegistration>) hrisComparator.sortExamRegistrationBySurname(passed), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("examinationDiplomaBySchoolReport") {
            @Override
            public void onSubmit() {
                if (examSetting == null) {
                    error("Select Exam Period");
                    return;
                }
                if (course == null) {
                    error("Select Course");
                    return;
                }

                try {
                    ///////////////////Successful Candidates//////////////////                    
                    List<ExamRegistration> passed = new ArrayList<ExamRegistration>();
                    List<ExamRegistration> institutionsExamResults = examRegistrationService.getAllExamCandidates(examSetting, course, institution, null, null, null, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, false);
                    for (ExamRegistration e : institutionsExamResults) {
                        if (e.getPassStatus() && e.getCompletedStatus()) {
                            passed.add(e);
                        }

                    }
                    ExaminationDiplomaBySchoolReport examinationDiplomaBySchoolReport = new ExaminationDiplomaBySchoolReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    List<ExamRegistration> bySchoolRegistrations = (List<ExamRegistration>) hrisComparator.sortExamRegistrationBySurname(passed);
                    bySchoolRegistrations = (List<ExamRegistration>) hrisComparator.sortExamRegistrationBySchoolName(bySchoolRegistrations);

                    if (course.getQualification() != null) {
                        parameters.put("course", course.getQualification().getName());
                    } else {
                        parameters.put("course", course.getName());
                    }

                    if (examSetting != null) {
                        parameters.put("examSetting", examSetting.toString());
                    }

                    if (courseService.getNumberOfModulePapers(course) == 3) {
                        parameters.put("hasPaperThree", Boolean.TRUE);
                    }

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(examinationDiplomaBySchoolReport, contentType, bySchoolRegistrations, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("passed") {
            @Override
            public void onSubmit() {
                if (examSetting == null) {
                    error("Select Exam Period");
                    return;
                }
                if (course == null) {
                    error("Select Course");
                    return;
                }

                if (institution == null) {
                    error("Select Institution");
                    return;
                }

                try {
                    ///////////////////Successful Candidates//////////////////                    
                    List<ExamRegistration> passed = new ArrayList<ExamRegistration>();
                    List<ExamRegistration> institutionsExamResults = examRegistrationService.getAllExamCandidates(examSetting, course, institution, null, null, null, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, false);
                    for (ExamRegistration e : institutionsExamResults) {
                        if (e.getPassStatus() && e.getCompletedStatus()) {
                            passed.add(e);
                        }
                    }

                    String statement = "";

                    Set<ModulePaper> modulePapers = new HashSet<ModulePaper>();

                    //filter module papers for course and exam settings
                    for (ModulePaper modulePaper : examService.findExamPapers(examSetting)) {
                        if (modulePaperService.findPapersInCourse(course).contains(modulePaper)) {
                            modulePapers.add(modulePaper);
                        }
                    }
                    //filter exams needed settings
                    for (Exam exam : examService.findAll(examSetting)) {
                        if (modulePapers.contains(exam.getModulePaper())) {
                            statement = statement.concat(exam.getExamDate().getDate() + DateUtil.getOrdinalSuffix(exam.getExamDate().getDate())) + " / ";
                        }
                    }
                    if (statement == null || statement.length() == 0) {
                        statement = " / ";
                    }
                    statement = statement.substring(0, statement.length() - 3);
                    statement = statement.concat(" " + examSetting.toString());

                    PassListReport passListReport = new PassListReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    //PASS LIST - List of SUCCESSFUL candidates in GENERAL NURSES DIPLOMA Final Examination held by the Nurses Council of Zimbabwe – 18 and 19 March 2013.
                    parameters.put("comment", "<b> SUCCESSFUL CANDIDATES </b> ");
                    parameters.put("institution", institution.getName());
                    parameters.put("title", course.getQualification() + " final Examination held by the Nurses Council of Zimbabwe " + statement);
                    //calculate pass rate and top schools if all marks have been entered

                    ///////////////////institution Top Candidates//////////////////
                    if (examRegistrationService.getTotalNumberOfCandidates(examSetting, course, institution, null, null, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE) > 5) {
                        if (examRegistrationService.getTotalNumberOfCandidates(examSetting, course, institution, null, null, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE) == 0) {
                            //sort candidates marks in acsending order
                            List<ExamRegistration> finalInstituionPositions = new ArrayList<ExamRegistration>();
                            List<ExamRegistration> instituionPositions = ExamRegistration.sortInstitutionExamRegistrations(passed);

                            for (ExamRegistration ex : instituionPositions) {
                                if (ex.getInstitutionPosition() < 4) {
                                    finalInstituionPositions.add(ex);
                                }
                            }
                            finalInstituionPositions = (List<ExamRegistration>) hrisComparator.sortExamCandidateMarks(finalInstituionPositions);
                            parameters.put("SUB_DATA_SOURCE", finalInstituionPositions);
                        }
                    }

                    ///////////////////Country Top Candidates//////////////////
                    if (examRegistrationService.getTotalNumberSchool(examSetting, course) >= 2) {

                        if (examRegistrationService.getTotalNumberOfCandidates(examSetting, course, institution, null, null, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE) == 0) {
                            //sort candidates marks in acsending order

                            List<ExamRegistration> countryExamResults = examRegistrationService.getAllExamCandidates(examSetting, course, null, null, null, null, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, false);

                            countryExamResults = ExamRegistration.sortCountryExamRegistrations(countryExamResults);
                            List<ExamRegistration> countryFinalPositions = new ArrayList<ExamRegistration>();

                            for (ExamRegistration ex : countryExamResults) {
                                if (ex.getCountryPosition() < 6) {
                                    countryFinalPositions.add(ex);
                                }
                            }
                            countryFinalPositions = (List<ExamRegistration>) hrisComparator.sortExamCandidateMarks(countryFinalPositions);
                            parameters.put("SUB_DATA_SOURCE2", countryFinalPositions);
                        }
                    }

                    parameters.put("status", "PASSED");
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(passListReport, contentType, (List<ExamRegistration>) hrisComparator.sortExamRegistrationBySurname(passed), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("failed") {
            @Override
            public void onSubmit() {
                if (examSetting == null) {
                    error("Select Exam Period");
                    return;
                }
                if (course == null) {
                    error("Select Course");
                    return;
                }

                if (institution == null) {
                    error("Select Institution");
                    return;
                }
                try {
                    List<ExamRegistration> failed = new ArrayList<ExamRegistration>();

                    Integer firstTimePass = 0;
                    Integer firstTimeFail = 0;
                    List<ExamRegistration> institutionsExamResults = examRegistrationService.getAllExamCandidates(examSetting, course, institution, null, null, null, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE);
                    Integer failedSupplementary = 0;
                    Integer passedSupplementary = 0;

                    Integer failedBothPapers = 0;
                    Integer failedSupplementaryBothPapers = 0;

                    Integer failedPaperOneRewrite = 0;
                    Integer failedPaperTwoRewrite = 0;

                    for (ExamRegistration e : institutionsExamResults) {
                        if (!e.getPassStatus() && e.getCompletedStatus()) {
                            failed.add(e);
                            if (!e.getSupplementaryExam()) {
                                firstTimeFail++;
                            } else {
                                failedSupplementary++;
                            }
                            if (e.getFirstTimeFailedBothPapers()) {
                                failedBothPapers++;
                            }
                            if (e.getSecondTimeFailedBothPapers()) {
                                failedSupplementaryBothPapers++;
                            }
                            if (e.getSecondTimeFaildPaperOne()) {
                                failedPaperOneRewrite++;
                            }
                            if (e.getSecondTimeFaildPaperTwo()) {
                                failedPaperTwoRewrite++;
                            }
                        } else if (e.getPassStatus() && e.getCompletedStatus()) {
                            if (!e.getSupplementaryExam()) {
                                firstTimePass++;
                            } else {
                                passedSupplementary++;
                            }
                        }
                    }

                    String statement = "";

                    Set<ModulePaper> modulePapers = new HashSet<ModulePaper>();

                    //filter module papers for course and exam settings
                    for (ModulePaper modulePaper : examService.findExamPapers(examSetting)) {
                        if (modulePaperService.findPapersInCourse(course).contains(modulePaper)) {
                            modulePapers.add(modulePaper);
                        }
                    }
                    //filter exams needed settings
                    for (Exam exam : examService.findAll(examSetting)) {
                        if (modulePapers.contains(exam.getModulePaper())) {
                            statement = statement.concat(exam.getExamDate().getDate() + DateUtil.getOrdinalSuffix(exam.getExamDate().getDate())) + " / ";
                        }
                    }
                    if (statement == null || statement.length() == 0) {
                        statement = " / ";
                    }
                    statement = statement.substring(0, statement.length() - 3);
                    statement = statement.concat(" " + examSetting.toString());

                    FinalExamSupComment finalExamSupComment = new FinalExamSupComment();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    parameters.put("comment", "<b> UNSUCCESSFUL CANDIDATES </b>  ");
                    parameters.put("title", course.getQualification() + " FINAL EXAMINATION " + statement);
                    parameters.put("status", "FAILED");
                    parameters.put("institution", institution.getName());

                    //calculate pass rate and top schools if all marks have been entered
                    if (examRegistrationService.getTotalNumberOfCandidates(examSetting, course, institution, null, null, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE) == 0) {

                        ///////////////////Institution Pass Rate//////////////////
                        ExamPassRate examPassRate = new ExamPassRate();

                        examPassRate.setCourse(course);
                        examPassRate.setExamSetting(examSetting);
                        examPassRate.setInstitution(institution);
                        //first time

                        examPassRate.setTotalNumberFirstTime(examRegistrationService.getTotalNumberOfCandidates(examSetting, course, institution, null, null, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE));
                        examPassRate.setFailedFirstTime(firstTimeFail);
                        examPassRate.setPassedFirstTime(firstTimePass);

                        //total number of candidates
                        examPassRate.setTotalNumberOfCandidates(examRegistrationService.getTotalNumberOfCandidates(examSetting, course, institution, null, null, null, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE));

                        //re-writes
                        examPassRate.setTotalNumberOfRewrites(examRegistrationService.getTotalNumberOfCandidates(examSetting, course, institution, null, null, Boolean.TRUE, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE));
                        examPassRate.setPassedRewrites(passedSupplementary);
                        examPassRate.setFailedRewrites(failedSupplementary);

                        //re-writes paper one and two
                        examPassRate.setFailedPaperOneRewrites(failedPaperOneRewrite);
                        examPassRate.setFailedPaperTwoRewrites(failedPaperTwoRewrite);

                        //both papers
                        examPassRate.setTotalFailedBothPapers(failedBothPapers);
                        examPassRate.setTotalFailedRewriteBothPapers(failedSupplementaryBothPapers);

                        List<ExamRegistration> disqualifiedList = examRegistrationService.getAllExamCandidates(examSetting, course, institution, null, null, null, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, Boolean.TRUE);
                        Long totalNumberOfCandidateDisqualified = disqualifiedList.stream().count();
                        examPassRate.setNumberOfCandidatesDisqualified(totalNumberOfCandidateDisqualified.intValue());
                        List<ExamPassRate> passRates = new ArrayList<ExamPassRate>();

                        passRates.add(examPassRate);

                        parameters.put("SUB_DATA_SOURCE", passRates);

                        ///////////////////End Print Report//////////////////
                    }

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(finalExamSupComment,
                            contentType, (List<ExamRegistration>) hrisComparator.sortExamRegistrationBySurname(failed), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("disqualified") {
            @Override
            public void onSubmit() {
                if (examSetting == null) {
                    error("Select Exam Period");
                    return;
                }
                if (course == null) {
                    error("Select Course");
                    return;
                }

                if (institution == null) {
                    error("Select Institution");
                    return;
                }
                try {
                    List<ExamRegistration> disqualifiedList = examRegistrationService.getAllExamCandidates(examSetting, course, institution, null, null, null, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, Boolean.TRUE);

                    String statement = "";

                    Set<ModulePaper> modulePapers = new HashSet<ModulePaper>();

                    //filter module papers for course and exam settings
                    for (ModulePaper modulePaper : examService.findExamPapers(examSetting)) {
                        if (modulePaperService.findPapersInCourse(course).contains(modulePaper)) {
                            modulePapers.add(modulePaper);
                        }
                    }
                    //filter exams needed settings
                    for (Exam exam : examService.findAll(examSetting)) {
                        if (modulePapers.contains(exam.getModulePaper())) {
                            statement = statement.concat(exam.getExamDate().getDate() + DateUtil.getOrdinalSuffix(exam.getExamDate().getDate())) + " / ";
                        }
                    }
                    if (statement == null || statement.length() == 0) {
                        statement = " / ";
                    }

                    statement = statement.substring(0, statement.length() - 3);
                    statement = statement.concat(" " + examSetting.toString());

                    FinalDisqualifedComment finalDisqualifedComment = new FinalDisqualifedComment();

                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    parameters.put("comment", "<b> DISQUALIFIED CANDIDATES </b>  ");
                    parameters.put("title", course.getQualification() + " FINAL EXAMINATION " + statement);
                    parameters.put("status", "DISQUALIFIED");

                    Long totalNumberOfCandidateDisqualified = disqualifiedList.stream().count();

                    parameters.put("numberOfCandidatesDisqualified", "" + totalNumberOfCandidateDisqualified);
                    parameters.put("institution", institution.getName());

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(finalDisqualifedComment,
                            contentType, (List<ExamRegistration>) hrisComparator.sortExamRegistrationBySurname(disqualifiedList), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("examEntryListTotal") {
            @Override
            public void onSubmit() {
                List<ExamPassRate> passRates = new ArrayList<ExamPassRate>();
                PaperEntryListReport paperEntryListReport = new PaperEntryListReport();

                if (examSetting == null) {
                    error("Select Exam Period");
                    return;
                }

                try {
                    Map parameters = new HashMap();

                    if (examSetting != null) {
                        parameters.put("examSetting", examSetting.toString());
                    }

                    for (Exam exam : examService.findAll(examSetting)) {
                        ExamPassRate examPassRate = new ExamPassRate();
                        examPassRate.setExam(exam);
                        examPassRate.setExamSetting(examSetting);
                        examPassRate.setCourse(exam.getModulePaper().getCourse());
                        examPassRate.setTotalNumberOfCandidates(examResultService.getTotalCandidates(exam, null));
                        passRates.add(examPassRate);
                    }

                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(paperEntryListReport,
                            contentType, passRates, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("institutionEntryList") {
            @Override
            public void onSubmit() {
                List<ExamPassRate> passRates = new ArrayList<ExamPassRate>();
                PaperEntryListReport paperEntryListReport = new PaperEntryListReport();

                if (examSetting == null) {
                    error("Select Exam Period");
                    return;
                }

                if (institution == null) {
                    error("Select Institution");
                    return;
                }

                try {
                    Map parameters = new HashMap();

                    if (examSetting != null) {
                        parameters.put("examSetting", examSetting.toString());
                    }

                    if (institution != null) {
                        parameters.put("institution", institution.toString());
                    }

                    for (Exam exam : examService.findAll(examSetting)) {
                        ExamPassRate examPassRate = new ExamPassRate();
                        examPassRate.setExam(exam);
                        examPassRate.setExamSetting(examSetting);
                        examPassRate.setInstitution(institution);
                        examPassRate.setTotalNumberOfCandidates(examResultService.getTotalCandidates(exam, examPassRate.getInstitution()));
                        examPassRate.setCourse(exam.getModulePaper().getCourse());
                        passRates.add(examPassRate);
                    }

                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(paperEntryListReport,
                            contentType, passRates, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("courseEntryList") {
            @Override
            public void onSubmit() {
                List<ExamPassRate> passRates = new ArrayList<ExamPassRate>();
                CourseEntryListReport courseEntryListReport = new CourseEntryListReport();
                if (examSetting == null) {
                    error("Select Exam Period");
                    return;
                }
                if (course == null) {
                    error("Select Exam Course");
                    return;
                }

                try {
                    Map parameters = new HashMap();

                    if (course.getQualification() != null) {
                        parameters.put("course", course.getQualification().getName());
                    } else {
                        parameters.put("course", course.getName());
                    }

                    if (examSetting != null) {
                        parameters.put("examSetting", examSetting.toString());
                    }

                    for (Institution institution : new TrainingInstitutionstModel(institutionService).getObject()) {
                        ExamPassRate examPassRate = new ExamPassRate();
                        examPassRate.setCourse(course);
                        examPassRate.setExamSetting(examSetting);
                        examPassRate.setInstitution(institution);
                        examPassRate.setTotalNumberOfCandidates(examRegistrationService.getTotalNumberOfCandidates(examSetting, course, institution, null, null, null, Boolean.FALSE, null, null, null));
                        passRates.add(examPassRate);
                    }

                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(courseEntryListReport,
                            contentType, passRates, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        add(form);
    }

    private class InstitutionsModel extends LoadableDetachableModel<List<? extends Institution>> {

        protected List<? extends Institution> load() {
            if (course != null) {
                return (List<Institution>) hrisComparator.sort((new ArrayList<Institution>(courseService.get(course.getId()).getTrainingInstitutions())));
            } else {
                return new ArrayList<Institution>();
            }
        }
    }

    public void initPapers() {
        typesOfPapers = new ArrayList<>();
        typesOfPapers.add(ALL);
        typesOfPapers.add(PAPERONE);
        typesOfPapers.add(PAPERTWO);
        typesOfPapers.add(PAPERTHREE);
    }
}
//~ Formatted by Jindent --- http://www.jindent.com
