/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CaseOutcome;
import zw.co.hitrac.council.business.domain.RegistrantStatus;
import zw.co.hitrac.council.business.service.CaseOutcomeService;
import zw.co.hitrac.council.business.service.RegistrantStatusService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.CountryListModel;

/**
 *
 * @author kelvin
 */
public class CaseOutcomeEditPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private CaseOutcomeService caseOutcomeService;
    @SpringBean
    private RegistrantStatusService registrantStatusService;

    public CaseOutcomeEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<CaseOutcome>(new CaseOutcomeEditPage.LoadableDetachableCaseOutcomeModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        Form<CaseOutcome> form = new Form<CaseOutcome>("form", (IModel<CaseOutcome>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                caseOutcomeService.save(getModelObject());
                setResponsePage(new CaseOutcomeViewPage(getModelObject().getId()));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new CheckBox("guilty"));
        form.add(new DropDownChoice("registrantStatus", registrantStatusService.findAll()));
        form.add(new BookmarkablePageLink<Void>("returnLink", CaseOutcomeListPage.class));
        form.add(new CheckBox("dates"));
        add(form);
    }

    private final class LoadableDetachableCaseOutcomeModel extends LoadableDetachableModel<CaseOutcome> {

        private Long id;

        public LoadableDetachableCaseOutcomeModel(Long id) {
            this.id = id;
        }

        @Override
        protected CaseOutcome load() {
            if (id == null) {
                return new CaseOutcome();
            }
            return caseOutcomeService.get(id);
        }
    }
}
