package zw.co.hitrac.council.web.pages.accounts;

import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.service.accounts.AccountService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author Tatenda Chiwandire
 */
public class AccountListPage extends IGeneralLedgerPage {

    @SpringBean
    private AccountService accountService;

    public AccountListPage() {
        add(new BookmarkablePageLink<Void>("returnLink", GeneralLedgerPage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new AccountEditPage(null));
            }
        });

        IModel<List<Account>> model = new LoadableDetachableModel<List<Account>>() {
            @Override
            protected List<Account> load() {
                return accountService.getGeneralLedgerAccounts();
            }
        };

        PropertyListView<Account> eachItem = new PropertyListView<Account>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<Account> item) {
                Link<Account> viewLink = new Link<Account>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new AccountViewPage(getModelObject().getId()));
                    }
                };
                item.add(viewLink);
                viewLink.add(new Label("name"));
                
            }
        };

        add(eachItem);
    }
}
