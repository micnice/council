package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by tdhla on 2/22/2017.
 */
@Entity
@Table(name = "institution_manager")
@Audited
public class InstitutionManager extends BaseIdEntity implements Serializable {

    private Institution institution;
    private Registrant registrant;
    boolean practitionerInCharge;
    boolean institutionSupervisor;
    private Date practitionerInChargeStartDate;
    private Date practitionerInChargeEndDate;
    private Date institutionSupervisorStartDate;
    private Date institutionSupervisorEndDate;


    @ManyToOne
    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    @ManyToOne
    public Registrant getRegistrant() {
        return registrant;
    }

    public void setRegistrant(Registrant registrant) {
        this.registrant = registrant;
    }

    public boolean isPractitionerInCharge() {
        return practitionerInCharge;
    }

    public void setPractitionerInCharge(boolean practitionerInCharge) {
        this.practitionerInCharge = practitionerInCharge;
    }

    public boolean isInstitutionSupervisor() {
        return institutionSupervisor;
    }

    public void setInstitutionSupervisor(boolean institutionSupervisor) {
        this.institutionSupervisor = institutionSupervisor;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getPractitionerInChargeStartDate() {
        return practitionerInChargeStartDate;
    }

    public void setPractitionerInChargeStartDate(Date practitionerInChargeStartDate) {
        this.practitionerInChargeStartDate = practitionerInChargeStartDate;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getPractitionerInChargeEndDate() {
        return practitionerInChargeEndDate;
    }

    public void setPractitionerInChargeEndDate(Date practitionerInChargeEndDate) {
        this.practitionerInChargeEndDate = practitionerInChargeEndDate;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getInstitutionSupervisorStartDate() {
        return institutionSupervisorStartDate;
    }

    public void setInstitutionSupervisorStartDate(Date institutionSupervisorStartDate) {
        this.institutionSupervisorStartDate = institutionSupervisorStartDate;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getInstitutionSupervisorEndDate() {
        return institutionSupervisorEndDate;
    }

    public void setInstitutionSupervisorEndDate(Date institutionSupervisorEndDate) {
        this.institutionSupervisorEndDate = institutionSupervisorEndDate;
    }

}
