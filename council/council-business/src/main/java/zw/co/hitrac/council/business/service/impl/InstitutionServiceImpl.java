/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.InstitutionDAO;
import zw.co.hitrac.council.business.domain.District;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.InstitutionCategory;
import zw.co.hitrac.council.business.domain.InstitutionType;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.service.InstitutionService;

import java.util.List;
import zw.co.hitrac.council.business.domain.City;
import zw.co.hitrac.council.business.domain.Province;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.utils.CouncilException;

/**
 *
 * @author tidza
 */
@Service
@Transactional
public class InstitutionServiceImpl implements InstitutionService {

    @Autowired
    private InstitutionDAO institutionDao;

    public Institution save(Institution t) {
        return institutionDao.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Institution> findAll() {
        return institutionDao.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Institution get(Long id) {
        return institutionDao.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Institution> findAll(Boolean retired) {

        return institutionDao.findAll(retired);
    }

    public void setInstitutionDAO(InstitutionDAO institutionDAO) {
        this.institutionDao = institutionDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Institution getInstitutionByAccount(Customer customer) {
        return institutionDao.getInstitutionByAccount(customer);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Institution> getAll(Boolean academic, Boolean school, Boolean council) {
        return institutionDao.getAll(academic, school, council);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Institution getInstitution(Customer customer) {
        return institutionDao.getInstitution(customer);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Institution> getInstitutions(InstitutionType institutionType) {
        return institutionDao.getInstitutions(institutionType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Institution> getInstitutionInDistrict(District district) {
        return institutionDao.getInstitutionInDistrict(district);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Institution> getInstitutions(String code, InstitutionType institutionType, InstitutionCategory institutionCategory, Boolean retired) {
        return institutionDao.getInstitutions(code, institutionType, institutionCategory, retired);
    }

    @Transactional
    public void removeDuplicates(Institution institution, List<Institution> institutions) {
        institutionDao.removeDuplicates(institution, institutions);
    }

    @Transactional
    public void retire(List<Institution> institutions) {
        institutionDao.retire(institutions);
    }

    public void addInstitutionSupervisor(Institution institution, Registrant newSupervisor) {
        if (checkInstitutionSupervisor(newSupervisor)) {
            throw new CouncilException("Supervisor already assigned to another Institution" + institutionDao.getInstitutionBySupervisor(institution.getSupervisor()));
        } else {
            institution.setSupervisor(newSupervisor);
            save(institution);
        }
    }

    public Boolean checkInstitutionSupervisor(Registrant registrant) {
        return institutionDao.checkInstitutionSupervisor(registrant);
    }

    public Institution getInstitutionBySupervisor(Registrant registrant) {
        return institutionDao.getInstitutionBySupervisor(registrant);
    }

    public List<Institution> getInstitutionByPractitionerInCharge(Registrant registrant) {
        return institutionDao.getInstitutionByPractitionerInCharge(registrant);
    }

    public Long getTotalCount(Province province, District district, City city, InstitutionType institutionType, InstitutionCategory institutionCategory) {
        return institutionDao.getTotalCount(province, district, city, institutionType, institutionCategory);
    }

    @Override
    public void createInstitutionAccounts() {
    institutionDao.createInstitutionAccounts();
    }

}
