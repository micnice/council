package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.IdentificationTypeDAO;
import zw.co.hitrac.council.business.dao.repo.IdentificationTypeRepository;
import zw.co.hitrac.council.business.domain.IdentificationType;

/**
 *
 * @author Charles Chigoriwa
 */
@Repository
public class IdentificationTypeDAOImpl implements IdentificationTypeDAO {

    @Autowired
    private IdentificationTypeRepository identificationTypeRepository;

    public IdentificationType save(IdentificationType identificationType) {
        return identificationTypeRepository.save(identificationType);
    }

    public List<IdentificationType> findAll() {
        return identificationTypeRepository.findAll();
    }

    public IdentificationType get(Long id) {
        return identificationTypeRepository.findOne(id);
    }

    /**
     * A setter method that will make mocking repo object easier
     * @param identificationTypeRepository 
     */
    public void setIdentificationTypeRepository(IdentificationTypeRepository identificationTypeRepository) {
        this.identificationTypeRepository = identificationTypeRepository;
    }

    public List<IdentificationType> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    
}
