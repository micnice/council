package zw.co.hitrac.council.web.utility;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.jackrabbit.JcrConstants;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.jackrabbit.value.BinaryImpl;
import sun.net.www.MimeTable;

import javax.jcr.*;
import java.io.InputStream;
import java.util.Calendar;

/**
 * @author clive
 *
 * New Convenience class to be used when dealing with JackRabbit operations
 */
public class JcrCouncilUtils {

    public static byte[] readJCRImage(String repositoryUrl, String imagePath) throws Exception {

        System.out.println("++++++++++++++++Reading Image from JCR+++++++");

        if (imagePath.startsWith("/")) {
            imagePath = imagePath.substring(1);
        }

        if (imagePath.endsWith("/jcr:content")) {
            imagePath = imagePath.replace("/jcr:content", "");
        }

        final String repositoryFolderPath = imagePath.substring(0, imagePath.lastIndexOf("/"));
        final String repositoryFileName = imagePath.substring(imagePath.lastIndexOf("/") + 1);

        Session session = openJCRRepositorySession(repositoryUrl);

        if (session == null) {
            throw new Exception("File selected cannot be uploaded JCR URL was not configured");
        } else {
            Node root = session.getRootNode();

            Node folderNode = JcrUtils.getNodeIfExists(root, repositoryFolderPath);

            if (folderNode != null && folderNode.hasNode(repositoryFileName)) {

                Node resultNode = folderNode.getNode(repositoryFileName).getNode(JcrConstants.JCR_CONTENT);
                InputStream inputStream = null;

                try {

                    inputStream = JcrUtils.readFile(resultNode);
                    byte[] bytes = IOUtils.toByteArray(inputStream);

                    return bytes;

                } finally {

                    session.logout();
                    IOUtils.closeQuietly(inputStream);
                }

            } else {
                return null;
            }

        }

    }

    public static void closeJCRRepositorySession(Session session){

        try {
            if(session.isLive()){
                session.logout();
            }
        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    public static Session openJCRRepositorySession(String url) throws RepositoryException {

        return openJCRRepositorySession(url, "admin", "admin");
    }

    public static Session openJCRRepositorySession(String url, String userName, String password) throws RepositoryException {

        Session session = null;

        if (StringUtils.isNotEmpty(url)) {

            Repository repository = JcrUtils.getRepository(url);
            session = repository.login(new SimpleCredentials(userName, password.toCharArray()));

            String user = session.getUserID();
            String name = repository.getDescriptor(Repository.REP_NAME_DESC);

            System.out.println("Logged in as " + user + " to a " + name + " repository.");

        }

        return session;

    }

    public static void uploadJCRImage(String repositoryUrl, InputStream inputStream,
                                      String imagePath, String imageFileName) throws Exception {

        System.out.println("++++++++++++++++Reading Image from JCR+++++++");

        final String repositoryFolderPath = imagePath.substring(0, imagePath.lastIndexOf("/"));
        final String repositoryFileName = imagePath.substring(imagePath.lastIndexOf("/") + 1);

        Session session = openJCRRepositorySession(repositoryUrl);

        if (session == null) {
            throw new Exception("File selected cannot be uploaded JCR URL was not configured");
        } else {

            try {
                Node root = session.getRootNode();
                String mimeType = getMimeTypeForFile(imageFileName);

                Node folderNode = JcrUtils.getOrCreateByPath(root, repositoryFolderPath, false, JcrConstants.NT_FOLDER,
                        JcrConstants.NT_FOLDER, true);

                session.save();

                if (folderNode.hasNodes()) {

                    for (NodeIterator iterator = folderNode.getNodes(); iterator.hasNext(); ) {

                        Node node = iterator.nextNode();
                        node.remove();

                        session.save();
                    }
                }

                Node fileNode = folderNode.addNode(repositoryFileName, JcrConstants.NT_FILE);

                // create the mandatory child node - jcr:content
                Node resultNode = fileNode.addNode(JcrConstants.JCR_CONTENT, JcrConstants.NT_RESOURCE);

                resultNode.setProperty(JcrConstants.JCR_MIMETYPE, mimeType);

                Binary binaryHandler = new BinaryImpl(inputStream);
                Calendar lastModified = Calendar.getInstance();

                resultNode.setProperty(JcrConstants.JCR_DATA, binaryHandler);
                lastModified.setTimeInMillis(System.currentTimeMillis());
                resultNode.setProperty(JcrConstants.JCR_LASTMODIFIED, lastModified);

                session.save();

                binaryHandler.dispose();

            } finally {
                session.logout();
                IOUtils.closeQuietly(inputStream);

            }
        }
    }

    public static String getMimeTypeForFile(String fileName) {

        MimeTable mimeTable = MimeTable.getDefaultTable();
        String mimeType = mimeTable.getContentTypeFor(fileName);

        if (mimeType == null) {
            mimeType = "application/octet-stream";
        }

        return mimeType;
    }
}