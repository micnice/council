package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Constance Mabaso
 * @author Charles Chigoriwa
 * @author Matiashe Michael
 */
@Entity
@Table(name="country")
@Audited
@XmlRootElement
public class Country extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    private Set<City> cities = new HashSet<City>();
    private String isoCode1, isoCode2, iana, ioc, itu, un, iso;

    @Column(name = "iana", nullable = true, unique = true)
    public String getIana() {
        return iana;
    }

    public void setIana(String iana) {
        this.iana = iana;
    }

    @Column(name = "ioc", nullable = true, unique = true)
    public String getIoc() {
        return ioc;
    }

    public void setIoc(String ioc) {
        this.ioc = ioc;
    }

    @Column(name = "iso", nullable = true, unique = true)
    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    @Column(name = "iso_code1", nullable = true, unique = true)
    public String getIsoCode1() {
        return isoCode1;
    }

    public void setIsoCode1(String isoCode1) {
        this.isoCode1 = isoCode1;
    }

    @Column(name = "iso_code2", nullable = true, unique = true)
    public String getIsoCode2() {
        return isoCode2;
    }

    public void setIsoCode2(String isoCode2) {
        this.isoCode2 = isoCode2;
    }

    @Column(name = "itu", nullable = true, unique = true)
    public String getItu() {
        return itu;
    }

    public void setItu(String itu) {
        this.itu = itu;
    }

    @Column(name = "un", nullable = true, unique = true)
    public String getUn() {
        return un;
    }

    public void setUn(String un) {
        this.un = un;
    }

    @OneToMany(mappedBy = "country")
    public Set<City> getCities() {
        return cities;
    }

    public void setCities(Set<City> cities) {
        this.cities = cities;
    }
}
