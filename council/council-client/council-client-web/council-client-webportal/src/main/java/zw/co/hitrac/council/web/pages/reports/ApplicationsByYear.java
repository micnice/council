/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.reports;

import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.PageReference;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.service.ApplicationService;
import zw.co.hitrac.council.business.utils.StringFormattingUtil;
import zw.co.hitrac.council.reports.ApplicationsByYearReport;
import zw.co.hitrac.council.reports.DefaultReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.GeneralUtils;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by clive on 7/1/15.
 */
public class ApplicationsByYear extends TemplatePage {

    @SpringBean
    private ApplicationService applicationService;
    private int yearApproved;
    private ContentType contentType;

    public ApplicationsByYear(final PageReference pageReference) {

        PropertyModel<Integer> yearModel = new PropertyModel<Integer>(this, "yearApproved");
        PropertyModel<ContentType> contentTypeModel = new PropertyModel<ContentType>(this, "contentType");
        Form<?> form = new Form("form");

        form.add(new DropDownChoice("yearApproved", yearModel, GeneralUtils.generateYearList()).setRequired(true).add(new ErrorBehavior()));
        add(form);

        form.add(new Link("returnLink") {

            public void onClick() {

                setResponsePage(pageReference.getPage());
            }
        });
        
        form.add(new DropDownChoice("contentType", contentTypeModel, Arrays.asList(ContentType.values())));
        
        form.add(new Button("showApplicationsByYear") {

                     @Override
                     public void onSubmit() {

                         List<Application> applicationsList = applicationService.getApplicationsByYearApproved(yearApproved);
                         DefaultReport report = new ApplicationsByYearReport();

                         Map parameters = new HashMap();
                         parameters.put("comment", StringFormattingUtil.join("Summary of All Applications Done In", yearApproved));

                         

                         try {
                             ByteArrayResource resource
                                     = ReportResourceUtils.getReportResource(report, contentType, applicationsList,
                                     parameters);
                             IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                     RequestCycle.get().getResponse(), null);

                             resource.respond(a);

                             // To make Wicket stop processing form after sending response
                             RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                         } catch (JRException ex) {
                             ex.printStackTrace(System.out);
                         }

                     }
                 }

        );

        add(new FeedbackPanel("feedback"));

    }

}
