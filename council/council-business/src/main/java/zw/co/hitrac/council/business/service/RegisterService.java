/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service;

import java.util.List;
import zw.co.hitrac.council.business.domain.Register;

/**
 *
 * @author Kelvin Goredema
 */
public interface RegisterService   extends IGenericService<Register> {
    List<Register> getRegisterDisciplines();
}
