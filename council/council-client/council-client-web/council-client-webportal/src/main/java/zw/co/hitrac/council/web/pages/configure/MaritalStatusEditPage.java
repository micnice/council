package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.MaritalStatus;
import zw.co.hitrac.council.business.service.MaritalStatusService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Edawrd Zengeni
 */
public class MaritalStatusEditPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private MaritalStatusService maritalStatusService;

    public MaritalStatusEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<MaritalStatus>(new LoadableDetachableMaritalStatusModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));

        Form<MaritalStatus> form = new Form<MaritalStatus>("form", (IModel<MaritalStatus>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                maritalStatusService.save(getModelObject());
                setResponsePage(new MaritalStatusViewPage(getModelObject().getId()));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink", MaritalStatusListPage.class));
        add(form);
    }

    private final class LoadableDetachableMaritalStatusModel extends LoadableDetachableModel<MaritalStatus> {
        private Long id;

        public LoadableDetachableMaritalStatusModel(Long id) {
            this.id = id;
        }

        @Override
        protected MaritalStatus load() {
            if (id == null) {
                return new MaritalStatus();
            }

            return maritalStatusService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
