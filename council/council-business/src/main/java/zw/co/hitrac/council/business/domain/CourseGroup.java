package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 *
 * @author Michael Matiashe
 */
@Entity
@Table(name="coursegroup")
@Audited
public class CourseGroup extends  BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;
     
}
