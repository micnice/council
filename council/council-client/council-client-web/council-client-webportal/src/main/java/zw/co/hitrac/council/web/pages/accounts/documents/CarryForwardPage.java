package zw.co.hitrac.council.web.pages.accounts.documents;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Check;
import org.apache.wicket.markup.html.form.CheckGroup;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.process.InvoiceProcess;
import zw.co.hitrac.council.business.process.PaymentProcess;
import zw.co.hitrac.council.business.service.CouncilDurationService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.service.accounts.AccountService;
import zw.co.hitrac.council.business.service.accounts.AccountsParametersService;
import zw.co.hitrac.council.business.service.accounts.CustomerService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.business.service.accounts.ReceiptItemService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.accounts.payments.CarryFowardConfirmationPage;
import zw.co.hitrac.council.web.pages.registration.RegistrantViewPage;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;

/**
 *
 * @author Takunda Dhlakama
 * @author Michael Matiashe
 */
public class CarryForwardPage extends TemplatePage {
    
    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    AccountService accountService;
    @SpringBean
    CustomerService customerAccountService;
    @SpringBean
    RegistrationService registrationService;
    @SpringBean
    DebtComponentService debtComponentService;
    @SpringBean
    GeneralParametersService generalParametersService;
    @SpringBean
    private InvoiceProcess invoiceProcess;
    @SpringBean
    private PaymentProcess paymentProcess;
    @SpringBean
    private AccountsParametersService accountsParametersService;
    @SpringBean
    private ReceiptItemService receiptItemService;
    @SpringBean
    private CouncilDurationService councilDurationService;
    private List<DebtComponent> selectedDebtComponents = new ArrayList<DebtComponent>();
    private Long registrantId;
    
    public CarryForwardPage(PageParameters parameters) {
        this(parameters.get("registrantId").toLong());
    }
    
    public CarryForwardPage(Long id) {
        registrantId = id;
        
        final CompoundPropertyModel<Registrant> registrantModel = new CompoundPropertyModel<Registrant>(new DetachableRegistrantModel(id, registrantService));
        setDefaultModel(registrantModel);
        add(new Label("fullname"));
        final Account account = accountService.get(registrantModel.getObject().getCustomerAccount().getAccount().getId());
        add(new Label("balance", account.getBalance().toString()));
        
        IModel<List<DebtComponent>> debtComponentsModel = new LoadableDetachableModel<List<DebtComponent>>() {
            @Override
            protected List<DebtComponent> load() {
                return debtComponentService.getDebtComponents(registrantModel.getObject().getCustomerAccount());
            }
        };
        add(new FeedbackPanel("feedback"));
        Form<Void> form = new Form<Void>("form") {
            @Override
            protected void onSubmit() {
                try {
                    if (!selectedDebtComponents.isEmpty()) {
                        setResponsePage(new CarryFowardConfirmationPage(registrantId, selectedDebtComponents));
                    }
                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };
        
        CheckGroup<DebtComponent> selectedDebtComponentsGroup = new CheckGroup<DebtComponent>("selectedDebtComponents", this.selectedDebtComponents);
        form.add(selectedDebtComponentsGroup);
        
        add(form);
        PropertyListView<DebtComponent> eachItem = new PropertyListView<DebtComponent>("eachItem", debtComponentsModel) {
            @Override
            protected void populateItem(final ListItem<DebtComponent> item) {
                
                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                
                item.add(new Check("selected", item.getModel()));
                item.add(new Label("product.name"));
                item.add(new Label("remainingBalance"));
            }
        };
        
        selectedDebtComponentsGroup.add(eachItem);
        
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });
        
        form.add(new Link<Registrant>("creditNoteLink") {
            @Override
            public void onClick() {
                setResponsePage(new CreditNotePage(registrantModel.getObject().getId()));
            }
        });
        
        form.add(new Link<Registrant>("registrantPaymentPage") {
            @Override
            public void onClick() {
                setResponsePage(new DebtComponentsPage(registrantModel.getObject().getId()));
                
            }
        });
        
    }
    
    @Override
    protected void onConfigure() {
        //if billing module disabled - direct to dashboard
        if (!generalParametersService.get().isBillingModule()) {
            if (registrantId != null) {
                setResponsePage(new RegistrantViewPage(registrantId));
            }
        }
        
    }
}
