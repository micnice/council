package zw.co.hitrac.council.webservices.api;

import java.io.Serializable;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantQualification;
import zw.co.hitrac.council.webservices.api.domain.LeanRegistrantQualification;

/**
 *
 * @author Daniel Nkhoma
 */
@Path("/registrantQualifications")
@Produces(MediaType.APPLICATION_XML)
@Consumes(MediaType.APPLICATION_XML)
public interface RegistrantQualificationWebService extends Serializable {

    @GET
    @Path("list")
    public List<RegistrantQualification> registrantQualifications(Registrant registrant);

    @GET
    @Path("qualificationlist")
    public List<RegistrantQualification> registrantQualifications(String registrationNumber);

    @GET
    @Path("listAll")
    public List<RegistrantQualification> registrantQualifications();

    @GET
    @Path("lean")
    public List<LeanRegistrantQualification> leanRegistrantQualifications();

    @GET
    @Path("registrantQualification/{id}")
    public RegistrantQualification registrantQualification(@PathParam(value = "id") Long id);

}
