
package zw.co.hitrac.council.web.menu;

import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.pages.examinations.ExaminationsPage;
import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;

/**
 *
 * @author Takunda Dhlakama
 */
public class ExamMenuPanel extends MenuPanel {

    private boolean examinationsPageCurrent;
    private Link<Void> examinationsLink;

    public ExamMenuPanel(String id) {
        super(id);
        add(examinationsLink = new BookmarkablePageLink<Void>("examinationsLink", ExaminationsPage.class){
        
            @Override
            protected void onConfigure() {
                setVisibilityAllowed(CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.EDUCATION_OFFICER))));
            }
        
        });
    }
    
    @Override
    protected void onConfigure() {
        addCurrentBehavior(examinationsLink, topMenuCurrent);
    }

    public Link<Void> getExaminationsLink() {
        return examinationsLink;
    }

    public void setExaminationsLink(Link<Void> examinationsLink) {
        this.examinationsLink = examinationsLink;
    }

}
