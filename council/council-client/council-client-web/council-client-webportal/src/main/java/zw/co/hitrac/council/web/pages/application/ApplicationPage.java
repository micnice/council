package zw.co.hitrac.council.web.pages.application;

//~--- non-JDK imports --------------------------------------------------------

import java.util.ArrayList;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.ApplicationPurpose;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.process.ApplicationProcess;
import zw.co.hitrac.council.business.service.ApplicationService;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.CourseListModel;
import zw.co.hitrac.council.web.models.InstitutionListModel;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.CustomDateTextField;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

//~--- JDK imports ------------------------------------------------------------
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import zw.co.hitrac.council.business.domain.QualificationType;
import zw.co.hitrac.council.business.domain.User;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.UserService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.pages.accounts.documents.DebtComponentsPage;
import zw.co.hitrac.council.web.pages.certification.CertificationPage;
import zw.co.hitrac.council.web.pages.registration.RegistrantViewPage;
import zw.co.hitrac.council.web.pages.registration.RegistrationPage;
import zw.co.hitrac.council.web.pages.registration.RequirementConfirmationPage;

/**
 * @author Michael Matiashe
 * @author Kelvin Goredema
 */
public class ApplicationPage extends TemplatePage {

    @SpringBean
    private CourseService courseService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private ApplicationProcess applicationProcess;
    @SpringBean
    private ApplicationService applicationService;
    @SpringBean
    private RegistrationService registrationService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private UserService userService;
    @SpringBean
    private RegistrationProcess registrationProcess;
    @SpringBean
    private DebtComponentService debtComponentService;
    Boolean show;

    public ApplicationPage(final IModel<Registrant> registrantModel) {
        menuPanelManager.getApplicationPanel().setTopMenuCurrent(true);

        show = generalParametersService.get().getRegistrationByApplication();
        CompoundPropertyModel<Application> model = new CompoundPropertyModel<Application>(new Application());

        setDefaultModel(model);
        model.getObject().setRegistrant(registrantModel.getObject());
        model.getObject().setApplicationDate(new Date());

        //Processed By is the Same as the Current User
        if (generalParametersService.get().getHasProcessedByBoard().equals(Boolean.FALSE)) {
            model.getObject().setProcessedBy(CouncilSession.get().getUser());
        }

        Registration r = registrationProcess.activeRegisterNotStudentRegister(registrantModel.getObject());

        if (r != null) {
            model.getObject().setCourse(r.getCourse());
        }

        Form<Application> form = new Form<Application>("form", (IModel<Application>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    getModelObject().setCreatedBy(CouncilSession.get().getUser());
                    applicationProcess.apply(getModelObject());
                    setResponsePage(new RequirementConfirmationPage(getModelObject()));

                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };
        form.add(new Label("approvedByLabel", "Decision By") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new Label("approvedCourseLabel", "Approved Discipline") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new DropDownChoice("approvedBy", userService.getAuthorisers()) {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new Label("processedByLabel", "Prossesed By") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });

        form.add(new Label("processedByCurrentUser", CouncilSession.get().getUser().getUsername()) {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });

        form.add(new DropDownChoice("processedBy", userService.getApplicationpProcessors()) {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });

        form.add(new Label("dateApprovedLabel", "Decision Date") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new CustomDateTextField("dateApproved") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });

        form.add(new CustomDateTextField("startDate").add(DatePickerUtil.getDatePicker()));
        form.add(new CustomDateTextField("endDate").add(DatePickerUtil.getDatePicker()));

        //
        form.add(new Label("approvedByboardLabel", "Approved By Council") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new DropDownChoice("approvedByboard", userService.getCouncilProcessors()) {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });

        form.add(new Label("dateApprovedByBoardLabel", "Council Decision Date") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new CustomDateTextField("dateApprovedByBoard") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        //
        form.add(new Label("dateProcessedLabel", "Final Processing Date") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new CustomDateTextField("dateProcessed") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });

        form.add(new DropDownChoice("applicationStatus", Application.getApplicationStates) {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new Label("qualificationTypeLabel", "Application Type") {
            @Override
            protected void onConfigure() {
                setVisible(generalParametersService.get().getRegistrationByApplication());
            }
        });

        form.add(new DropDownChoice("qualificationType", Arrays.asList(QualificationType.values())) {
            @Override
            protected void onConfigure() {
                setVisible(generalParametersService.get().getRegistrationByApplication());
            }
        });
        form.add(new DropDownChoice("course",
                new CourseListModel(courseService)).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("approvedCourse",
                new CourseListModel(courseService)) {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new Label("currentStatus", model.getObject().getApplicationTextStatus()));
        List<ApplicationPurpose> applicationPurposes = new ArrayList<ApplicationPurpose>();
        if (generalParametersService.get().getRegistrationByApplication()) {
            applicationPurposes.add(ApplicationPurpose.INTERN_REGISTRATION);
            applicationPurposes.add(ApplicationPurpose.MAIN_REGISTRATION);
            applicationPurposes.add(ApplicationPurpose.SPECIALIST_REGISTRATION);
            applicationPurposes.add(ApplicationPurpose.STUDENT_REGISTRATION);
            applicationPurposes.add(ApplicationPurpose.UNRESTRICTED_PRACTICING_CERTIFICATE);
        }
        applicationPurposes.add(ApplicationPurpose.PROVISIONAL_REGISTRATION);
        form.add(new DropDownChoice("applicationPurpose", applicationPurposes));
        form.add(new Label("institutionLabel", "Institution") {
            @Override
            protected void onConfigure() {
                setVisible(generalParametersService.get().getRegistrationByApplication());
            }
        });
        form.add(new DropDownChoice("institution", new InstitutionListModel(institutionService)) {
            @Override
            protected void onConfigure() {
                setVisible(generalParametersService.get().getRegistrationByApplication());
            }
        });
        form.add(new Label("applicationDate", DateUtil.getDate(model.getObject().getApplicationDate())));

        form.add(new Label("commentLabel", "Comment") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new TextArea("comment") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new Label("applicationStatusLabel", "Application Decision") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {

                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });
        add(form);
        add(new Label("registrant.fullname"));
        add(new FeedbackPanel("feedback"));
    }

    public ApplicationPage(long id) {
        menuPanelManager.getApplicationPanel().setTopMenuCurrent(true);

        if (!generalParametersService.get().getRegistrationByApplication()) {
            show = Boolean.FALSE;
        } else {
            show = Boolean.TRUE;
        }
        final CompoundPropertyModel<Application> model
                = new CompoundPropertyModel<Application>(applicationService.get(id));

        setDefaultModel(model);
        final Boolean isProvisional = model.getObject().getApplicationPurpose().equals(ApplicationPurpose.PROVISIONAL_REGISTRATION);

        Form<Application> form = new Form<Application>("form", (IModel<Application>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    Application application = applicationProcess.approve(getModelObject());
                    List<DebtComponent> debtComponents = debtComponentService.getDebtComponents(application.getRegistrant().getCustomerAccount());
                    if (!generalParametersService.get().getHasProcessedByBoard() && application.getApplicationStatus().equals(Application.APPLICATIONAPPROVED)) {
                        if (application.getApplicationPurpose().equals(ApplicationPurpose.PROVISIONAL_REGISTRATION) || application.getApplicationPurpose().equals(ApplicationPurpose.INTERN_REGISTRATION) || application.getApplicationPurpose().equals(ApplicationPurpose.MAIN_REGISTRATION) || application.getApplicationPurpose().equals(ApplicationPurpose.SPECIALIST_REGISTRATION)) {
                            if (debtComponents.isEmpty()) {
                                if (generalParametersService.get().getApproveAndRegister()) {
                                    CompoundPropertyModel<Application> applicationModel = new CompoundPropertyModel<Application>(application);
                                    setResponsePage(new RegistrationPage(null,null,applicationModel,null,null));
                                } else {
                                    setResponsePage(new ApplicationViewPage(getModelObject().getId()));
                                }
                            } else {
                                Registrant registrant = application.getRegistrant();
                                if (!debtComponents.isEmpty()) {
                                    setResponsePage(new DebtComponentsPage(registrant.getId()));
                                } else {
                                    setResponsePage(new RegistrantViewPage(getModelObject().getId()));
                                }
                            }
                        } else if (application.getApplicationPurpose().equals(ApplicationPurpose.STUDENT_REGISTRATION)) {
                            setResponsePage(new CertificationPage(application.getRegistrant().getId()));
                        } else {
                            setResponsePage(new ApplicationViewPage(getModelObject().getId()));
                        }
                    } else {
                        setResponsePage(new ApplicationViewPage(getModelObject().getId()));
                    }
                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };
        form.add(new Label("currentStatus", model.getObject().getApplicationTextStatus()));

        form.add(new Label("approvedCourseLabel", "Approved Discipline") {
            @Override
            protected void onConfigure() {
                setVisible(!model.getObject().getApplicationPurpose().equals(ApplicationPurpose.INSTITUTION_REGISTRATION));
            }
        });

        form.add(new Label("qualificationTypeLabel", "Qualification Type") {
            @Override
            protected void onConfigure() {
                setVisible(generalParametersService.get().getRegistrationByApplication());
            }
        });
        form.add(new DropDownChoice("qualificationType", Arrays.asList(QualificationType.values())) {
            @Override
            protected void onConfigure() {
                setVisible(generalParametersService.get().getRegistrationByApplication());
            }
        });
        //
        form.add(new Label("approvedByboardLabel", "Approved By Council") {
            @Override
            protected void onConfigure() {
                setVisible(isProvisional);
            }
        });
        form.add(new DropDownChoice("approvedByboard", userService.getCouncilProcessors()) {
            @Override
            protected void onConfigure() {
                setVisible(isProvisional);
            }
        });

        form.add(new Label("dateApprovedByBoardLabel", "Council Decision Date") {
            @Override
            protected void onConfigure() {
                setVisible(isProvisional);
            }
        });
        form.add(new CustomDateTextField("dateApprovedByBoard") {
            @Override
            protected void onConfigure() {
                setVisible(isProvisional);
            }
        });

        form.add(new Label("approvedByLabel", "Approved By"));
        form.add(new DropDownChoice("approvedBy", userService.getAuthorisers()));
        if (generalParametersService.get().getHasProcessedByBoard()) {
            form.add(new DropDownChoice("processedBy", userService.getApplicationpProcessors()));
        } else {
            List<User> users = new ArrayList<User>();
            users.add(CouncilSession.get().getUser());
            form.add(new DropDownChoice("processedBy", users));
        }
        form.add(new Label("dateApprovedLabel", "Decision Date"));
        form.add(new Label("dateProcessedLabel", "Final Processing Date"));
        form.add(new Label("processedByLabel", "Prossesed By"));
        form.add(new CustomDateTextField("dateApproved").add(DatePickerUtil.getDatePicker()));
        form.add(new CustomDateTextField("dateProcessed").add(DatePickerUtil.getDatePicker()));
        form.add(new CustomDateTextField("startDate").add(DatePickerUtil.getDatePicker()).setVisible(false));
        form.add(new CustomDateTextField("endDate").add(DatePickerUtil.getDatePicker()).setVisible(false));

        final List<String> applicationStates = Arrays.asList(new String[]{Application.APPLICATIONPENDING, Application.APPLICATIONAPPROVED,
                Application.APPLICATIONDECLINED, Application.APPLICATION_IN_CIRCULATION, Application.APPLICATION_AWAITING_COUNCIL_APPROVAL, Application.APPLICATIONUNDERREVIEW});

        form.add(new DropDownChoice("applicationStatus", applicationStates).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("course",
                new CourseListModel(courseService)).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("approvedCourse",
                new CourseListModel(courseService)));
        form.add(new DropDownChoice("applicationPurpose", Arrays.asList(ApplicationPurpose.values())));
        form.add(new Label("institutionLabel", "Institution") {
            @Override
            protected void onConfigure() {
                setVisible(generalParametersService.get().getRegistrationByApplication());
            }
        });

        form.add(new DropDownChoice("institution", new InstitutionListModel(institutionService)) {
            @Override
            protected void onConfigure() {
                setVisible(generalParametersService.get().getRegistrationByApplication());
            }
        });
        form.add(new Label("applicationStatusLabel", "Application Decision"));
        form.add(new Label("applicationDate", DateUtil.getDate(model.getObject().getApplicationDate())));
        form.add(new Label("commentLabel", "Comment") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new TextArea("comment"));
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(model.getObject().getRegistrant().getId()));
            }
        });
        add(form);
        add(new Label("registrant.fullname"));
        add(new FeedbackPanel("feedback"));
    }
}

//~ Formatted by Jindent --- http://www.jindent.com
