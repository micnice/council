package zw.co.hitrac.council.business.dao.repo;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.RegistrantCPDItem;

/**
 *
 * @author Constance Mabaso
 */
public interface RegistrantCPDItemRepository extends CrudRepository<RegistrantCPDItem, Serializable> {
    
    public List<RegistrantCPDItem> findAll();
}
