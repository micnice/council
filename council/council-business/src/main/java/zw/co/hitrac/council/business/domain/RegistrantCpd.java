package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Constance Mabaso
 * @author tdhlakama
 */
@Entity
@Table(name = "registrantcpd")
@Audited
public class RegistrantCpd extends BaseIdEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    private Registrant registrant;
    private Date dateAwarded;
    private RegistrantCPDItem registrantCPDItem;
    private Duration duration;
    private BigDecimal point;
    private Date expiryDate;
    private Date issuedDate;
    private Integer activityNo;
    private String organisation;
    private String activityAttended;

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDateAwarded() {
        return dateAwarded;
    }

    public Integer getActivityNo() {
        return activityNo;
    }

    public void setActivityNo(Integer activityNo) {
        this.activityNo = activityNo;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Date getIssuedDate() {
        return issuedDate;
    }

    public void setIssuedDate(Date issuedDate) {
        this.issuedDate = issuedDate;
    }

    public String getOrganisation() {
        return organisation;
    }

    public String getActivityAttended() {
        return activityAttended;
    }

    public void setActivityAttended(String activityAttended) {
        this.activityAttended = activityAttended;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    public void setDateAwarded(Date dateAwarded) {
        this.dateAwarded = dateAwarded;
    }

    @ManyToOne
    public Registrant getRegistrant() {
        return registrant;
    }

    public void setRegistrant(Registrant registrant) {
        this.registrant = registrant;
    }

    @ManyToOne
    public RegistrantCPDItem getRegistrantCPDItem() {
        return registrantCPDItem;
    }

    public void setRegistrantCPDItem(RegistrantCPDItem registrantCPDItem) {
        this.registrantCPDItem = registrantCPDItem;
    }

    @ManyToOne
    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    @Transient
    public BigDecimal getPoints() {
        return getRegistrantCPDItem() != null ? getRegistrantCPDItem().getPoints() : getPoint() != null ? getPoint() : BigDecimal.ZERO;
    }

    public BigDecimal getPoint() {
        return point;
    }

    public void setPoint(BigDecimal point) {
        this.point = point;
    }

    @Override
    public String toString() {
        return "RegistrantCpd{" +
                "registrant=" + registrant +
                ", dateAwarded=" + dateAwarded +
                ", registrantCPDItem=" + registrantCPDItem +
                ", duration=" + duration +
                ", point=" + point +
                ", expiryDate=" + expiryDate +
                ", issuedDate=" + issuedDate +
                ", activityNo=" + activityNo +
                ", organisation='" + organisation + '\'' +
                ", activityAttended='" + activityAttended + '\'' +
                '}';
    }
}
