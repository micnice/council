package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.IdentificationType;

/**
 *
 * @author Charles Chigoriwa
 */
public interface IdentificationTypeRepository extends CrudRepository<IdentificationType, Long> {

    public List<IdentificationType> findAll();
}
