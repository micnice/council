package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.TransferRule;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.RegisterService;
import zw.co.hitrac.council.business.service.TransferRuleService;
import zw.co.hitrac.council.business.service.accounts.ProductService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.CourseListModel;
import zw.co.hitrac.council.web.models.RegisterListModel;

/**
 *
 *
 * @author Michael Matiashe
 */
public class TransferRuleEditPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private TransferRuleService transferRuleService;
    @SpringBean
    private RegisterService registerService;
    @SpringBean
    private CourseService courseService;
    @SpringBean
    private ProductService productService;

    public TransferRuleEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<TransferRule>(new LoadableDetachableTransferRuleModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));

        Form<TransferRule> form = new Form<TransferRule>("form", (IModel<TransferRule>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                transferRuleService.save(getModelObject());
                setResponsePage(new TransferRuleViewPage(getModelObject().getId()));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("fromRegister", new RegisterListModel(registerService)).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("toRegister", new RegisterListModel(registerService)).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("fromCourse", new CourseListModel(courseService)));
        form.add(new DropDownChoice("toCourse", new CourseListModel(courseService)));
        form.add(new DropDownChoice("product", productService.findAll()));
        form.add(new BookmarkablePageLink<Void>("returnLink", TransferRuleViewPage.class));
        add(form);
    }

    private final class LoadableDetachableTransferRuleModel extends LoadableDetachableModel<TransferRule> {

        private Long id;

        public LoadableDetachableTransferRuleModel(Long id) {
            this.id = id;
        }

        @Override
        protected TransferRule load() {
            if (id == null) {
                return new TransferRule();
            }

            return transferRuleService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
