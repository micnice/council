
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------

import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author tdhlakama
 */
public abstract class IExaminationsPage extends TemplatePage {
    public IExaminationsPage() {
        menuPanelManager.getExamPanel().setTopMenuCurrent(true);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
