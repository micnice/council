package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.examinations.ExamYear;
import zw.co.hitrac.council.business.service.examinations.ExamYearService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.configure.AdministerDatabasePage;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;

/**
 *
 * @author Constance Mabaso
 */
public class ExamYearListPage extends IExaminationsPage {
    @SpringBean
    private ExamYearService examYearService;

    public ExamYearListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new ExamYearEditPage(null));
            }
        });

        IModel<List<ExamYear>> model = new LoadableDetachableModel<List<ExamYear>>() {
            @Override
            protected List<ExamYear> load() {
                return examYearService.findAll();
            }
        };
        PropertyListView<ExamYear> eachItem = new PropertyListView<ExamYear>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<ExamYear> item) {
                Link<ExamYear> viewLink = new Link<ExamYear>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new ExamYearViewPage(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(viewLink);
                item.add(new Label("name"));
            }
        };

        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
