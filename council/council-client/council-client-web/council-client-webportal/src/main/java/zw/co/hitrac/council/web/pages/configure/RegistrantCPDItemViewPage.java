package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.RegistrantCPDItem;
import zw.co.hitrac.council.business.service.RegistrantCPDItemService;

/**
 *
 * @author Constance Mabaso
 */
public class RegistrantCPDItemViewPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private RegistrantCPDItemService registrantCPDItemService;

    public RegistrantCPDItemViewPage(Long id) {
        CompoundPropertyModel<RegistrantCPDItem> model =
            new CompoundPropertyModel<RegistrantCPDItem>(new LoadableDetachableRegistrantCPDItemModel(id));

        setDefaultModel(model);
        add(new Link<RegistrantCPDItem>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantCPDItemEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", RegistrantCPDItemListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("points"));
        add(new Label("registrantCPDCategory"));
        add(new Label("registrantCPDItem", model.bind("name")));
    }

    private final class LoadableDetachableRegistrantCPDItemModel extends LoadableDetachableModel<RegistrantCPDItem> {
        private Long id;

        public LoadableDetachableRegistrantCPDItemModel(Long id) {
            this.id = id;
        }

        @Override
        protected RegistrantCPDItem load() {
            return registrantCPDItemService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
