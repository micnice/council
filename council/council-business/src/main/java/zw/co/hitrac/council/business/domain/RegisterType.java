package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 *
 * @author Kelvin Goredema
 */
@Entity
@Table(name = "registertype")
@Audited
@XmlRootElement
public class RegisterType extends BaseEntity implements Serializable {

    private String individualName;
    private String aliasName;

    public String getIndividualName() {
        return individualName;
    }

    public void setIndividualName(String individualName) {
        this.individualName = individualName;
    }

    public String getAliasName() {
        if (aliasName == null) {
            return getName();
        }
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }
}
