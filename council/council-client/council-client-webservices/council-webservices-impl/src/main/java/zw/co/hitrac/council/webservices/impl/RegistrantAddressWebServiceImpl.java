package zw.co.hitrac.council.webservices.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantAddress;
import zw.co.hitrac.council.business.service.GenericService;
import zw.co.hitrac.council.business.service.RegistrantAddressService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.webservices.api.RegistrantAddressWebService;
import zw.co.hitrac.council.webservices.api.domain.LeanRegistrantAddress;

/**
 *
 * @author Daniel Nkhoma
 */
@Component
public class RegistrantAddressWebServiceImpl implements RegistrantAddressWebService {

    @Autowired
    private RegistrantAddressService registrantAddressService;
    @Autowired
    private GenericService genericService;
    @Autowired
    private RegistrantService registrantService;

    @Override
    public List<RegistrantAddress> registrantAddresses(Long registrantId) {
        return registrantAddressService.getAddresses((Registrant) genericService.getLean("Registrant", registrantId), null);
    }

    @Override
    public List<RegistrantAddress> registrantAddresses(String registrationNumber) {
        Registrant registrant = registrantService.getRegistrantByRegistrationNumber(registrationNumber);
        return registrant != null ? registrantAddressService.getAddresses(registrant, null) : Collections.EMPTY_LIST;
    }

    @Override
    public List<RegistrantAddress> registrantAddresses() {
        return registrantAddressService.findAll();
    }

    public List<LeanRegistrantAddress> leanRegistrantAddresses() {
        List<LeanRegistrantAddress> leanRegistrantAddresses = new ArrayList<LeanRegistrantAddress>();
        for (Long registrantAddressId : genericService.getAllLean("RegistrantAddress")) {
            LeanRegistrantAddress leanRegistrantAddress = new LeanRegistrantAddress();
            leanRegistrantAddress.setRegistrantAddressId(registrantAddressId);
            leanRegistrantAddresses.add(leanRegistrantAddress);
        }
        return leanRegistrantAddresses;
    }

    @Override
    public RegistrantAddress registrantAddress(Long id) {
        return registrantAddressService.get(id);
    }
}
