
package zw.co.hitrac.council.business.service;

import java.util.List;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.InstitutionInspection;

/**
 *
 * @author Constance Mabaso
 */
public interface InstitutionInspectionService extends IGenericService<InstitutionInspection> {
  public List<InstitutionInspection> get(Application application);
}
