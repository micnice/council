/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service;

import java.util.List;
import zw.co.hitrac.council.business.domain.ContactType;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantContact;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;

/**
 *
 * @author kelvin
 */
public interface RegistrantContactService extends IGenericService<RegistrantContact> {

    public List<RegistrantContact> getContacts(Registrant registrant, Institution institution);

    public List<RegistrantContact> getMailContacts();

    public List<RegistrantContact> getContactsByContactType(Registrant registrant, ContactType contactType);

    public RegistrantContact getActiveContactbyContactType(Registrant registrant, ContactType contactType);

    public List<RegistrantData> getRegistrantDataAddress(ContactType contactType);

    public List<RegistrantData> getRegistrantDataAddressCorrections(ContactType contactType);

    public List<RegistrantData> getRegistrantDataContacts(ContactType contactType, Course course);

    List<RegistrantData> getAllRegistrantDataAndContacts(ContactType contactType);
}
