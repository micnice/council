package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.QualificationDAO;
import zw.co.hitrac.council.business.domain.ProductIssuanceType;
import zw.co.hitrac.council.business.domain.Qualification;
import zw.co.hitrac.council.business.service.QualificationService;

import java.util.List;

/**
 *
 * @author Michael Matiashe
 */
@Service
@Transactional
public class QualificationServiceImpl implements QualificationService {

    @Autowired
    private QualificationDAO qualificationDAO;

    @Transactional
    public Qualification save(Qualification qualification) {
        return qualificationDAO.save(qualification);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Qualification> findAll() {
        return qualificationDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Qualification get(Long id) {
        return qualificationDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Qualification> findAll(Boolean retired) {

        return qualificationDAO.findAll(retired);
    }

    public void setQualificationDAO(QualificationDAO qualificationDAO) {
        this.qualificationDAO = qualificationDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Qualification> getQualificationCertificateType(ProductIssuanceType productIssuanceType) {
        return qualificationDAO.getQualificationCertificateType(productIssuanceType);
    }

	@Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Qualification findByName(String name) {
		return qualificationDAO.findByName(name);
	}
}
