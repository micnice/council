package zw.co.hitrac.council.business.dao;

import zw.co.hitrac.council.business.domain.MaritalStatus;

/**
 *
 * @author Edward Zengeni
 */
public interface MaritalStatusDAO extends IGenericDAO<MaritalStatus> {
	public MaritalStatus findByName(String name);
}
