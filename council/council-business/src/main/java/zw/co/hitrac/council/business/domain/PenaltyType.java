/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 *
 * @author kelvin
 */
@Entity
@Table(name="penaltytype")
@Audited
public class PenaltyType extends BaseEntity implements Serializable{
    private static final long serialVersionUID = 1L;  
}
