package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.business.service.RegisterService;

/**
 *
 * @author Kelvin Goredema
 */
public class RegisterViewPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private RegisterService registerService;

    public RegisterViewPage(Long id) {
        CompoundPropertyModel<Register> model =
            new CompoundPropertyModel<Register>(new LoadableDetachableRegisterModel(id));

        setDefaultModel(model);
        add(new Link<Register>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new RegisterEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", RegisterListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("quarterlyRenewal"));
        add(new Label("numberOfMonthsBeforeRenewal"));
        add(new Label("register", model.bind("name")));
        add(new Label("requirements"));
        add(new Label("conditions"));
    }

    private final class LoadableDetachableRegisterModel extends LoadableDetachableModel<Register> {
        private Long id;

        public LoadableDetachableRegisterModel(Long id) {
            this.id = id;
        }

        @Override
        protected Register load() {
            return registerService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
