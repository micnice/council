package zw.co.hitrac.council.web.pages.certification;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.ProductIssuance;
import zw.co.hitrac.council.business.domain.ProductIssuanceType;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.ProductIssuanceService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;

import java.util.List;

import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.service.accounts.AccountService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.web.pages.accounts.documents.DebtComponentsPage;

/**
 * @author Edward Zengeni
 */
public class
CertificationPage extends TemplatePage {

    @SpringBean
    private ProductIssuanceService productIssuanceService;
    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    final CompoundPropertyModel<Registrant> registrantModel;
    @SpringBean
    private DebtComponentService debtComponentService;

    public CertificationPage(final Long id) {
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(id);
        registrantModel = new CompoundPropertyModel<Registrant>(new DetachableRegistrantModel(id, registrantService));
        setDefaultModel(registrantModel);
        add(new Label("fullname"));

        IModel<List<ProductIssuance>> productIssuanceModel = new LoadableDetachableModel<List<ProductIssuance>>() {
            @Override
            protected List<ProductIssuance> load() {
                return productIssuanceService.getProductAllIssuances(registrantModel.getObject());

            }
        };
        add(new CertificateDataListPanel("certificateDataListPanel", productIssuanceModel));
    }

    @Override
    protected void onConfigure() {
        final Registrant registrant = registrantModel.getObject();

        List<DebtComponent> debtComponents = debtComponentService.getDebtComponents(registrant.getCustomerAccount());
        if (!debtComponents.isEmpty()) {
            setResponsePage(new DebtComponentsPage(registrant.getId()));
        }
        super.onConfigure(); //To change body of generated methods, choose Tools | Templates.
    }
}
