package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.RequirementDAO;
import zw.co.hitrac.council.business.domain.Requirement;
import zw.co.hitrac.council.business.service.RequirementService;

import java.util.List;

/**
 *
 * @author Michael Matiashe
 */
@Service
@Transactional
public class RequirementServiceImpl implements RequirementService {

    @Autowired
    private RequirementDAO requirementDAO;

    @Transactional
    public Requirement save(Requirement requirement) {
        return requirementDAO.save(requirement);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Requirement> findAll() {
        return requirementDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Requirement get(Long id) {
        return requirementDAO.get(id);
    }

    public void setRequirementDAO(RequirementDAO requirementDAO) {
        this.requirementDAO = requirementDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Requirement> findAll(Boolean examRequirement) {
        return requirementDAO.findAll(examRequirement);
    }
}
