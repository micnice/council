package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CourseCondition;
import zw.co.hitrac.council.business.service.CourseConditionService;

import java.util.List;

/**
 *
 * @author tdhlakama
 */
public class CourseConditionListPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private CourseConditionService courseConditionService;

    public CourseConditionListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {

            @Override
            public void onClick() {
                setResponsePage(new CourseConditionEditPage(null, getPageReference()));
            }
        });

        IModel<List<CourseCondition>> model = new LoadableDetachableModel<List<CourseCondition>>() {

            @Override
            protected List<CourseCondition> load() {
                return courseConditionService.findAll();
            }
        };

        PropertyListView<CourseCondition> eachItem = new PropertyListView<CourseCondition>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<CourseCondition> item) {
                Link<CourseCondition> viewLink = new Link<CourseCondition>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new CourseConditionEditPage(getModelObject().getId(), getPageReference()));
                    }
                };
                item.add(viewLink);
                item.add(new Label("register"));
                item.add(new Label("course"));
                item.add(new Label("condition"));                
                item.add(new Label("retired"));
            }
        };

        add(eachItem);
    }

}
