
package zw.co.hitrac.council.business.dao.accounts;

import java.util.Date;
import java.util.List;
import zw.co.hitrac.council.business.dao.IGenericDAO;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.accounts.PaymentDetails;
import zw.co.hitrac.council.business.domain.accounts.Prepayment;
import zw.co.hitrac.council.business.domain.accounts.PrepaymentDetails;

/**
 *
 * @author Michael Matiashe
 */
public interface PrepaymentDetailsDAO extends IGenericDAO<PrepaymentDetails> {;
    public List<PrepaymentDetails> findByUid(String uid);
    public List<PrepaymentDetails> findByPrepayment(Prepayment prepayment);
    public PrepaymentDetails findByPaymentDetails(PaymentDetails paymentDetails);
    public List<PaymentDetails> findReceiptHeaders(Prepayment prepayment);
    public List<PrepaymentDetails> findByCustomerAndCancelled(Customer customer, Boolean cancelled);
    public List<PrepaymentDetails> findAllModifiedByDates(Date startDate, Date endDate);
    public List<PrepaymentDetails> findAllByDates(Date startDate, Date endDate);
}
