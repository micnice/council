package zw.co.hitrac.council.webservices.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zw.co.hitrac.council.business.domain.AddressType;
import zw.co.hitrac.council.business.service.AddressTypeService;
import zw.co.hitrac.council.business.service.GenericService;
import zw.co.hitrac.council.webservices.api.AddressTypeWebService;
import zw.co.hitrac.council.webservices.api.domain.LeanAddressType;

/**
 *
 * @author Daniel Nkhoma
 */
@Component
public class AddressTypeWebServiceImpl implements AddressTypeWebService {

    @Autowired
    private AddressTypeService addressTypeService;
    @Autowired
    private GenericService genericService;

    public void setAddressTypeService(AddressTypeService addressTypeService) {
        this.addressTypeService = addressTypeService;
    }

    @Override
    public List<AddressType> getAddressTypes() {
        return this.addressTypeService.findAll();
    }

    @Override
    public List<LeanAddressType> getAllLean() {
        List<LeanAddressType> leanAddressTypes = new ArrayList<LeanAddressType>();
        for (Long addressTypeId : genericService.getAllLean("AddressType")) {
            LeanAddressType leanAddressType = new LeanAddressType();
            leanAddressType.setAddressTypeId(addressTypeId);
            leanAddressTypes.add(leanAddressType);
        }
        return leanAddressTypes;
    }
}
