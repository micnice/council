/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.MisconductTypeDAO;
import zw.co.hitrac.council.business.dao.repo.MisconductTypeRepository;
import zw.co.hitrac.council.business.domain.MisconductType;

/**
 *
 * @author hitrac
 */
@Repository
public class MisconductTypeDAOImpl implements MisconductTypeDAO{
   @Autowired
   private MisconductTypeRepository misconductTypeRepository;
    
   public MisconductType save(MisconductType misconductType){
       return misconductTypeRepository.save(misconductType);
   }
   
   public List<MisconductType> findAll(){
       return misconductTypeRepository.findAll();
   }
   
   public MisconductType get(Long id){
       return misconductTypeRepository.findOne(id);
   }
 /**
     * A setter method that will make mocking repo object easier
     * @param misconductTypeRepository 
     */
   
   public void setMisconductTypeRepository(MisconductTypeRepository misconductTypeRepository) {
         this.misconductTypeRepository = misconductTypeRepository;
    }

    public List<MisconductType> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
