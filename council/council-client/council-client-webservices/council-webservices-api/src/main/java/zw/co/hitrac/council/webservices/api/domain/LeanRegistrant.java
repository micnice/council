package zw.co.hitrac.council.webservices.api.domain;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Daniel Nkhoma
 */
@XmlRootElement
public class LeanRegistrant {
    
    private Long registrantId;

    public LeanRegistrant() {
    }

    public LeanRegistrant(Long registrantId) {
        this.registrantId = registrantId;
    }

    public Long getRegistrantId() {
        return registrantId;
    }

    public void setRegistrantId(Long registrantId) {
        this.registrantId = registrantId;
    }
    
}
