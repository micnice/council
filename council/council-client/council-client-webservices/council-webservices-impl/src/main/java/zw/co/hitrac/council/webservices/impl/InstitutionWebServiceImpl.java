package zw.co.hitrac.council.webservices.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.GenericService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.webservices.api.InstitutionWebService;
import zw.co.hitrac.council.webservices.api.domain.LeanInstitution;

/**
 *
 * @author Daniel Nkhoma
 */
@Component
public class InstitutionWebServiceImpl implements InstitutionWebService {

    @Autowired
    private InstitutionService institutionService;
    @Autowired
    private GenericService genericService;
    @Autowired
    private GeneralParametersService generalParametersService;

    @Override
    public List<Institution> getInstitutions() {
        return institutionService.findAll();
    }
    
    @Override
    public List<Institution> getRegisteredInstitutions() {
        return institutionService.getInstitutions(generalParametersService.get().getHealthInstitutionType());
    }
    
    @Override
    public List<LeanInstitution> getLeanInstitution() {
        List<LeanInstitution> leanInstitutions = new ArrayList<LeanInstitution>();
        for (Long institutionId : genericService.getAllLean("Institution")) {
            LeanInstitution leanInstitution = new LeanInstitution();
            leanInstitution.setInstitutionId(institutionId);
            leanInstitutions.add(leanInstitution);
        }
        return leanInstitutions;
    }

    @Override
    public Institution getInstitution(Long id) {
        return institutionService.get(id);
    }
}
