package zw.co.hitrac.council.web.pages.accounts;

import zw.co.hitrac.council.web.pages.configure.*;
import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.ProductGroup;
import zw.co.hitrac.council.business.service.accounts.ProductGroupService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

/**
 *
 * @author Charles Chigoriwa
 */
public class ProductGroupListPage extends IBankBookPage{
    
    @SpringBean
    private ProductGroupService productGroupService;

    public ProductGroupListPage() {
        add(new BookmarkablePageLink<Void>("returnLink",BankBookPage.class));
        add(new Link<Void>("addNewLink") {

            @Override
            public void onClick() {
               setResponsePage(new ProductGroupEditPage(null));
            }
        });
        
        IModel<List<ProductGroup>> model=new LoadableDetachableModel<List<ProductGroup>>() {

            @Override
            protected List<ProductGroup> load() {
               return productGroupService.findAll();
            }
        };
        
        PropertyListView<ProductGroup> eachItem=new PropertyListView<ProductGroup>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<ProductGroup> item) {
                Link<ProductGroup> viewLink=new Link<ProductGroup>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                       setResponsePage(new ProductGroupViewPage(getModelObject().getId()));
                    }
                };
                if(item.getIndex()%2==0){
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };
        
        add(eachItem);
    }
    
    
    
}
