/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.EmploymentArea;
import zw.co.hitrac.council.business.service.EmploymentAreaService;

/**
 *
 * @author kelvin
 */
public class EmploymentAreaViewPage  extends IAdministerDatabaseBasePage {

    @SpringBean
    private EmploymentAreaService employmentAreaService;

    public EmploymentAreaViewPage(Long id) {
        CompoundPropertyModel<EmploymentArea> model=new CompoundPropertyModel<EmploymentArea>(new LoadableDetachableEmploymentAreaModel(id));
        setDefaultModel(model);
        add(new Link<EmploymentArea>("editLink",model){

            @Override
            public void onClick() {
                setResponsePage(new EmploymentAreaEditPage(getModelObject().getId()));
            }
            
        });
        add(new BookmarkablePageLink<Void>("returnLink", EmploymentAreaListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("employmentArea",model.bind("name")));
    }


    private final class LoadableDetachableEmploymentAreaModel extends LoadableDetachableModel<EmploymentArea> {

        private Long id;

        public LoadableDetachableEmploymentAreaModel(Long id) {
            this.id = id;
        }

        @Override
        protected EmploymentArea load() {

            return employmentAreaService.get(id);
        }
    }
}
