package zw.co.hitrac.council.business.dao.accounts.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.accounts.CustomerDAO;
import zw.co.hitrac.council.business.dao.repo.accounts.CustomerRepository;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.Customer;

/**
 *
 * @author tdhlakama
 */
@Repository
public class CustomerDAOImpl implements CustomerDAO {

    @Autowired
    private CustomerRepository customerAccountRepository;
    @PersistenceContext
    private EntityManager entityManager;

    public Customer save(Customer customerAccount) {
        return customerAccountRepository.save(customerAccount);
    }

    public List<Customer> findAll() {
        return customerAccountRepository.findAll();
    }

    public Customer get(Long id) {
        return customerAccountRepository.findOne(id);
    }

    public void setCustomerAccountRepository(CustomerRepository customerAccountRepository) {
        this.customerAccountRepository = customerAccountRepository;
    }

    public Customer get(Account account) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Customer.class);
        criteria.add(Restrictions.eq("account", account));
        return (Customer) criteria.uniqueResult();
    }

    public Registrant getRegistrant(Account account) {
        return getRegistrant(get(account));
    }

     public Institution getInstitution(Account account) {
        return getInstitution(get(account));
    }
    
    public Registrant getRegistrant(Customer customer) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registrant.class);
        criteria.add(Restrictions.eq("customerAccount", customer));
        return (Registrant) criteria.uniqueResult();
    }

    public Institution getInstitution(Customer customer) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Institution.class);
        criteria.add(Restrictions.eq("customerAccount", customer));
        return (Institution) criteria.uniqueResult();
    }

    public List<Customer> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
