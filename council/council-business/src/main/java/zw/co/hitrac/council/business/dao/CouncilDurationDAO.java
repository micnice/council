package zw.co.hitrac.council.business.dao;

import java.util.List;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Duration;

/**
 *
 * @author Michael Matiashe
 */
public interface CouncilDurationDAO extends IGenericDAO<CouncilDuration> {
     public CouncilDuration getCouncilDurationFromDuration(Duration duration) ;
     public List<CouncilDuration> getActiveCouncilDurations();

}
