package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------
import zw.co.hitrac.council.web.pages.registration.RequirementConfirmationPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.business.process.ExamRegistrationProcess;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.business.service.examinations.ExamSettingService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.registration.RegistrantViewPage;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.service.SupportDocumentService;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.web.models.BasicCourseListModel;
import zw.co.hitrac.council.web.models.CourseListModel;
import zw.co.hitrac.council.web.models.ExamSettingActiveListModel;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.CustomDateTextField;

/**
 *
 * @author Michael Matiashe
 * @author Judge Muzinda
 */
public class ExamRegistrationPage extends TemplatePage {

    @SpringBean
    private ExamSettingService examSettingService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private ExamRegistrationProcess examRegistrationProcess;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private CourseService courseService;
    @SpringBean
    private DebtComponentService debtComponentService;
    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private SupportDocumentService supportDocumentService;
    private Course course;
    private HrisComparator hrisComparator = new HrisComparator();

    public ExamRegistrationPage(final IModel<Registrant> registrantModel) {
        CompoundPropertyModel<ExamRegistration> model = new CompoundPropertyModel<ExamRegistration>(new ExamRegistration());

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());
        PropertyModel<Course> courseModel = new PropertyModel<Course>(this, "course");
        model.getObject().setRegistration(new Registration());
        model.getObject().getRegistration().setRegistrant(registrantModel.getObject());
        model.getObject().getRegistration().setRegister(generalParametersService.get().getStudentRegister());
        model.getObject().setDateCreated(new Date());
        setDefaultModel(model);

        Form<ExamRegistration> form = new Form<ExamRegistration>("form", (IModel<ExamRegistration>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    getModel().getObject().setInstitution(getModel().getObject().getRegistration().getInstitution());
                    getModel().getObject().getRegistration().setCourse(course);
                    ExamRegistration examRegistration = examRegistrationProcess.register(getModelObject());
                    setResponsePage(new RequirementConfirmationPage(examRegistration));
                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });
        form.add(new DropDownChoice("course", courseModel, new BasicCourseListModel(courseService), new ChoiceRenderer<Course>()) {
            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }

            @Override
            public boolean isRequired() {
                return true;
            }
        }.add(new ErrorBehavior()));

        form.add(new CustomDateTextField("registration.registrationDate").setRequired(true).add(new ErrorBehavior()).add(DatePickerUtil.getDatePicker()));
        form.add(new DropDownChoice("registration.institution", new InstitutionsModel()).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("examSetting", new ExamSettingActiveListModel(examSettingService)).setRequired(true).add(new ErrorBehavior()));
        add(form);
        add(new Label("registration.registrant.fullname"));
        add(new FeedbackPanel("feedback"));
    }

    public ExamRegistrationPage(final IModel<Registrant> registrantModel, final IModel<Registration> registrationIModel) {
        CompoundPropertyModel<ExamRegistration> model = new CompoundPropertyModel<ExamRegistration>(new ExamRegistration());

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());

        if (!generalParametersService.get().getHasProcessedByBoard()) {
            model.getObject().setRegistration(registrationIModel.getObject());
        } else {
            model.getObject().setRegistration(new Registration());
            model.getObject().getRegistration().setRegistrant(registrantModel.getObject());
            model.getObject().getRegistration().setRegister(generalParametersService.get().getStudentRegister());
        }
        PropertyModel<Course> courseModel = new PropertyModel<Course>(this, "course");
        setDefaultModel(model);

        Form<ExamRegistration> form = new Form<ExamRegistration>("form", (IModel<ExamRegistration>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    getModel().getObject().setInstitution(getModel().getObject().getRegistration().getInstitution());
                    if (generalParametersService.get().getHasProcessedByBoard()) {
                        getModel().getObject().getRegistration().setCourse(course);
                    }
                    ExamRegistration examRegistration = examRegistrationProcess.registerPBQ(getModel().getObject());
                    setResponsePage(new RequirementConfirmationPage(examRegistration));
                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };

        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });

        form.add(new DropDownChoice("course", courseModel, new CourseListModel(courseService), new ChoiceRenderer<Course>()) {
            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }

            @Override
            public boolean isRequired() {
                return true;
            }
        }.add(new ErrorBehavior()));

        form.add(new CustomDateTextField("registration.registrationDate").setRequired(true).add(new ErrorBehavior()).add(DatePickerUtil.getDatePicker()));
        form.add(new DropDownChoice("registration.institution", new InstitutionsModel()).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("examSetting", new ExamSettingActiveListModel(examSettingService)).setRequired(true).add(new ErrorBehavior()));
        add(form);
        add(new Label("registration.registrant.fullname"));
        add(new FeedbackPanel("feedback"));

    }

    private class InstitutionsModel extends LoadableDetachableModel<List<? extends Institution>> {

        protected List<? extends Institution> load() {
            if (course != null) {
                return (List<Institution>) hrisComparator.sort((new ArrayList<Institution>(courseService.get(course.getId()).getTrainingInstitutions())));
            } else {
                return new ArrayList<Institution>();
            }
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
