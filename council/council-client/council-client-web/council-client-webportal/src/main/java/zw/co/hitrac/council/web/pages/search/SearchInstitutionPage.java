package zw.co.hitrac.council.web.pages.search;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.QualificationService;
import zw.co.hitrac.council.web.pages.TemplatePage;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import zw.co.hitrac.council.business.domain.InstitutionCategory;
import zw.co.hitrac.council.business.domain.InstitutionType;
import zw.co.hitrac.council.business.service.InstitutionTypeService;
import zw.co.hitrac.council.web.pages.institution.InstitutionDataViewPanel;

/**
 *
 * @author takunda Dhlakama
 */
public class SearchInstitutionPage extends TemplatePage {

    private InstitutionCategory institutionCategory;
    private InstitutionType institutionType;
    private String code;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    QualificationService qualificationService;
    @SpringBean
    GeneralParametersService generalParametersService;
    @SpringBean
    private InstitutionTypeService institutionTypeService;
    private static final String TOP_LOGO = "zw/co/hitrac/council/reports/images/logo1.jpg";
    private static final String WATERMARK_LOGO = "zw/co/hitrac/council/reports/images/logo.png";

    public SearchInstitutionPage() {
        menuPanelManager.getSearchPanel().setTopMenuCurrent(true);
        menuPanelManager.getSearchPanel().setSearchAdvanced(true);

        PropertyModel<InstitutionType> institutionTypeModel = new PropertyModel<InstitutionType>(this, "institutionType");
        PropertyModel<InstitutionCategory> institutionCategoryModel = new PropertyModel<InstitutionCategory>(this, "institutionCategory");
        PropertyModel<String> codeModel = new PropertyModel<String>(this, "code");

        Form<?> form = new Form("form");

        form.add(new DropDownChoice("institutionCategory", institutionCategoryModel, Arrays.asList(InstitutionCategory.values())));
        form.add(new DropDownChoice("institutionType", institutionTypeModel, institutionTypeService.findAll()));
        form.add(new TextField<String>("code", codeModel));
        add(form);

        IModel<List<Institution>> model = new LoadableDetachableModel<List<Institution>>() {
            @Override
            protected List<Institution> load() {
                if ((code == null) && (institutionCategory == null) && (institutionType == null)) {
                    return new ArrayList<Institution>();
                } else {
                    return institutionService.getInstitutions(code, institutionType, institutionCategory, null);
                }
            }
        };
        //  List Data View Panel
        add(new InstitutionDataViewPanel("institutionDataViewPanel", model));
        add(new FeedbackPanel("feedback"));
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
