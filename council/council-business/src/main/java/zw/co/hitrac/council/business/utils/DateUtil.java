package zw.co.hitrac.council.business.utils;

import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Objects;

/**
 * Date Utility Class used to convert Strings to Dates and Timestamps
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a> Modified by
 *         <a href="mailto:dan@getrolling.com">Dan Kibler </a> to correct time pattern.
 *         Minutes should be mm not MM (MM is month).
 */
public final class DateUtil {

    public static final String BUNDLE_KEY = "ApplicationResources";
    private static final String TIME_PATTERN = "HH:mm";
    private static Log log = LogFactory.getLog(DateUtil.class);
    private static SimpleDateFormat formatter = new SimpleDateFormat(getDatePattern());
    private static final Logger logger = LoggerFactory.getLogger(DateUtil.class);


    /**
     * Checkstyle rule: utility classes should not have public constructor
     */
    private DateUtil() {

    }

    /**
     * @param date a non-null java.util.Date
     * @return an integer representing the year part of the date
     */
    public static int getYearFromDate(Date date) {

        if (date == null) {

            throw new IllegalArgumentException("Cannot get year from a null date");

        } else {

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return calendar.get(Calendar.YEAR);
        }
    }

    public static int getMonthFromDate(Date date) {

        if (date == null) {

            throw new IllegalArgumentException("Cannot get month from a null date");

        } else {

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);

            //First month is 0 in the Gregorian and Julian calendars
            return calendar.get(Calendar.MONTH) + 1;
        }
    }

    public static int getDayFromDate(Date date) {

        if (date == null) {

            throw new IllegalArgumentException("Cannot get day from a null date");

        } else {

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return calendar.get(Calendar.DAY_OF_MONTH);
        }
    }

    public static String getSQLDatePattern() {

        return "MM/dd/yyyy";
    }

    public static String getSQLDate(Date date) {

        return formatter.format(date);
    }

    public static String getDateTimePattern() {

        return DateUtil.getDatePattern() + " HH:mm:ss.S";
    }

    /**
     * Return default datePattern (MM/dd/yyyy)
     *
     * @return a string representing the date pattern on the UI
     */
    public static String getDatePattern() {

        return "dd/MM/yyyy";
    }

    /**
     * This method attempts to convert date - dd-MMM-yyyy
     *
     * @param aDate date from database as a string
     * @return formatted string for the ui
     */
    public static String getDate(Date aDate) {

        String returnValue = "";

        if (aDate != null) {
            returnValue = formatter.format(aDate);
        }

        return (returnValue);
    }

    /**
     * This method returns the current date time in the format: MM/dd/yyyy HH:MM
     * a
     *
     * @param theTime the current time
     * @return the current date/time
     */
    public static String getTimeNow(Date theTime) {

        return getDateTime(TIME_PATTERN, theTime);
    }

    /**
     * This method generates a string representation of a date's date/time in
     * the format you specify on input
     *
     * @param aMask the date pattern the string is in
     * @param aDate a date object
     * @return a formatted string representation of the date
     * @see java.text.SimpleDateFormat
     */
    public static String getDateTime(String aMask, Date aDate) {

        SimpleDateFormat df = null;
        String returnValue = "";

        if (aDate == null) {
            log.warn("aDate is null!");
        } else {
            df = new SimpleDateFormat(aMask);
            returnValue = df.format(aDate);
        }

        return (returnValue);
    }

    /**
     * This method returns the current date in the format: MM/dd/yyyy
     *
     * @return the current date
     * @throws ParseException when String doesn't match the expected format
     */
    public static Calendar getToday() throws ParseException {

        Date today = new Date();

        // This seems like quite a hack (date -> string -> date),
        // but it works ;-)
        String todayAsString = formatter.format(today);
        Calendar cal = new GregorianCalendar();
        cal.setTime(convertStringToDate(todayAsString));
        return cal;
    }

    /**
     * This method converts a String to a date using the datePattern
     *
     * @param strDate the date to convert (in format MM/dd/yyyy)
     * @return a date object
     * @throws ParseException when String doesn't match the expected format
     */
    public static Date convertStringToDate(final String strDate) throws ParseException {

        return convertStringToDate(getDatePattern(), strDate);
    }

    /**
     * This method generates a string representation of a date/time in the
     * format you specify on input
     *
     * @param aMask   the date pattern the string is in
     * @param strDate a string representation of a date
     * @return a converted Date object
     * @throws ParseException when String doesn't match the expected format
     * @see java.text.SimpleDateFormat
     */
    public static Date convertStringToDate(String aMask, String strDate)
            throws ParseException {

        SimpleDateFormat df;
        Date date;
        df = new SimpleDateFormat(aMask);

        if (log.isDebugEnabled()) {
            log.debug("converting '" + strDate + "' to date with mask '" + aMask + "'");
        }

        try {
            date = df.parse(strDate);
        } catch (ParseException pe) {
            //log.error("ParseException: " + pe);
            throw new ParseException(pe.getMessage(), pe.getErrorOffset());
        }

        return (date);
    }

    /**
     * This method generates a string representation of a date based on the
     * System Property 'dateFormat' in the format you specify on input
     *
     * @param aDate A date to convert
     * @return a string representation of the date
     */
    public static String convertDateToString(Date aDate) {

        return getDateTime(getDatePattern(), aDate);
    }

    public static double DifferenceInMonths(Date date1, Date date2) {

        return DifferenceInYears(date1, date2) * 12;
    }

    public static double DifferenceInYears(Date date1, Date date2) {

        double days = DifferenceInDays(date1, date2);
        return days / 365.2425;
    }

    public static double DifferenceInDays(Date date1, Date date2) {

        return DifferenceInHours(date1, date2) / 24.0;
    }

    public static double DifferenceInHours(Date date1, Date date2) {

        return DifferenceInMinutes(date1, date2) / 60.0;
    }

    public static double DifferenceInMinutes(Date date1, Date date2) {

        return DifferenceInSeconds(date1, date2) / 60.0;
    }

    public static double DifferenceInSeconds(Date date1, Date date2) {

        return DifferenceInMilliseconds(date1, date2) / 1000.0;
    }

    private static double DifferenceInMilliseconds(Date date1, Date date2) {

        return Math.abs(GetTimeInMilliseconds(date1) - GetTimeInMilliseconds(date2));
    }

    private static long GetTimeInMilliseconds(Date date) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.getTimeInMillis() + cal.getTimeZone().getOffset(cal.getTimeInMillis());
    }

    public static Date getInitialDateTime(Date date) throws ParseException {

        SimpleDateFormat sdf = null;
        sdf = new SimpleDateFormat("dd/MM/yyyy");
        String test = sdf.format(date) + " 09:00:00";
        sdf = new SimpleDateFormat("dd/MM/yyyy H:m:s");
        return sdf.parse(test);
    }

    /**
     * Return ordinal suffix (e.g. 'st', 'nd', 'rd', or 'th') for a given number
     *
     * @param value a number
     * @return Ordinal suffix for the given number
     */
    public static String getOrdinalSuffix(int value) {

        int hunRem = value % 100;
        int tenRem = value % 10;

        if (hunRem - tenRem == 10) {
            return "th";
        }
        switch (tenRem) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    private static String getCurrentDateInSpecificFormat(Calendar currentCalDate) {

        String dayNumberSuffix = getDayNumberSuffix(currentCalDate.get(Calendar.DAY_OF_MONTH));
        DateFormat dateFormat = new SimpleDateFormat(" d'" + dayNumberSuffix + "' MMMM yyyy");
        return dateFormat.format(currentCalDate.getTime());
    }

    public static String getDayNumberSuffix(int day) {

        if (day >= 11 && day <= 13) {
            return "th";
        }
        switch (day % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    public static Date getDateFromAge(Integer age) {

        return DateUtils.addYears(new Date(), -age);
    }

    /////////////////
    public static long getPeriodLengthBetweenDates(final ChronoUnit chronoUnit,
                                                   LocalDate startDate, LocalDate endDate) {

        Objects.requireNonNull(startDate, "Start date is required");
        Objects.requireNonNull(endDate, "End date is required");
        Objects.requireNonNull(chronoUnit, "Chrono Unit is required");

        final long periodLength = startDate.until(endDate, chronoUnit);

        logger.debug("CouncilPeriod No of {} between dates {} to {} is {}", chronoUnit, startDate,
                endDate, periodLength);

        return Math.abs(periodLength);

    }

    public static LocalDate convertToLocalDate(Date date) {

        Objects.requireNonNull(date, "Date is required");

        return convertToLocalDateTime(date).toLocalDate();
    }

    public static LocalDateTime convertToLocalDateTime(Date date) {

        Objects.requireNonNull(date, "Date is required");

        final Instant instant = Instant.ofEpochMilli(date.getTime());

        return LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
    }
}
