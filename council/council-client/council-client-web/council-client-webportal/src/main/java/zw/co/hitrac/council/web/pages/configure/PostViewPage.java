package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Post;
import zw.co.hitrac.council.business.service.PostService;

/**
 *
 * @author charlesc
 */
public class PostViewPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private PostService postService;

    public PostViewPage(Long id) {
        CompoundPropertyModel<Post> model = new CompoundPropertyModel<Post>(new LoadableDetachablePostModel(id));

        setDefaultModel(model);
        add(new Link<Post>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new PostEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", PostListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("post", model.bind("name")));
    }

    private final class LoadableDetachablePostModel extends LoadableDetachableModel<Post> {
        private Long id;

        public LoadableDetachablePostModel(Long id) {
            this.id = id;
        }

        @Override
        protected Post load() {
            return postService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
