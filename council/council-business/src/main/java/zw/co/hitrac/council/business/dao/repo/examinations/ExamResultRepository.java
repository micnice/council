/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.repo.examinations;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.examinations.ExamResult;

/**
 *
 * @author tidza
 */
public interface ExamResultRepository extends CrudRepository<ExamResult, Long> {
public List<ExamResult> findAll();
}
