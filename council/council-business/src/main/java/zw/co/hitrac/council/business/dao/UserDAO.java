/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao;

import java.util.List;
import java.util.Optional;

import zw.co.hitrac.council.business.domain.User;

/**
 * @author Clive Gurure
 * @author Michael Matiashe
 */
public interface UserDAO extends IGenericDAO<User> {

    List<User> getAuthorisers();

    List<User> getApplicationpProcessors();

    List<User> getCouncilProcessors();

    void encryptAllPasswords();

    Optional<User> findByNameIgnoreCase(String userName);

}
