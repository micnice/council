
package zw.co.hitrac.council.business.service.accounts.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.accounts.PaymentDataDAO;
import zw.co.hitrac.council.business.domain.accounts.PaymentData;
import zw.co.hitrac.council.business.service.accounts.PaymentDataService;

import java.util.List;

/**
 *
 * @author Tatenda Chiwandire
 */
@Service
@Transactional
public class PaymentDataServiceImpl implements PaymentDataService {

    @Autowired
    private PaymentDataDAO paymentDataDAO;

    @Transactional
    public PaymentData save(PaymentData t) {
        return paymentDataDAO.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<PaymentData> findAll() {
        return paymentDataDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public PaymentData get(Long id) {
        return paymentDataDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<PaymentData> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setPaymentDataDao(PaymentDataDAO paymentDataDAO) {

        this.paymentDataDAO = paymentDataDAO;
    }


}
