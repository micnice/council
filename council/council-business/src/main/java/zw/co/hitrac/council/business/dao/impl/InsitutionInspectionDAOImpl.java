package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.InstitutionInspectionDAO;
import zw.co.hitrac.council.business.dao.repo.InstitutionInspectionRepository;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.InstitutionInspection;

/**
 *
 * @author Constance Mabaso
 */
@Repository
public class InsitutionInspectionDAOImpl implements InstitutionInspectionDAO {

    @Autowired
    private InstitutionInspectionRepository institutionInspectionRepository;
    @PersistenceContext
    EntityManager entityManager;

    public InstitutionInspection save(InstitutionInspection institutionInspection) {
        return institutionInspectionRepository.save(institutionInspection);
    }

    public List<InstitutionInspection> findAll() {
        return institutionInspectionRepository.findAll();
    }

    public InstitutionInspection get(Long id) {
        return institutionInspectionRepository.findOne(id);
    }

    public void setInstitutionInspectionRepository(InstitutionInspectionRepository institutionInspectionRepository) {
        this.institutionInspectionRepository = institutionInspectionRepository;
    }

    public List<InstitutionInspection> get(Application application) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(InstitutionInspection.class);
        criteria.add(org.hibernate.criterion.Expression.eq("application", application));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<InstitutionInspection> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
