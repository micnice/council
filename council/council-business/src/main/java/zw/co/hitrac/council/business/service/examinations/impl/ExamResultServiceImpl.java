/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.examinations.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.examinations.ExamResultDao;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.examinations.Exam;
import zw.co.hitrac.council.business.domain.examinations.ExamResult;
import zw.co.hitrac.council.business.service.examinations.ExamResultService;

import java.util.List;

/**
 *
 * @author tidza
 */
@Service
@Transactional
public class ExamResultServiceImpl implements ExamResultService {

    @Autowired
    private ExamResultDao examResultDao;

    @Transactional
    public ExamResult save(ExamResult t) {
        return examResultDao.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ExamResult> findAll() {
        return examResultDao.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public ExamResult get(Long id) {
        return examResultDao.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ExamResult> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setExamResultDao(ExamResultDao examResultDao) {
        this.examResultDao = examResultDao;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ExamResult> examResults(Exam exam) {
        return examResultDao.examResults(exam);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Integer getTotalCandidates(Exam exam, Institution institution) {
        return examResultDao.getTotalCandidates(exam, institution);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Integer getTotalCandidates(Exam exam, Institution institution, Boolean supplementaryExam, Boolean pass) {
        return examResultDao.getTotalCandidates(exam, institution, supplementaryExam, pass);
    }
}
