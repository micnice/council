package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;

import java.util.Date;
import java.util.List;

/**
 * @author Kelvin Goredema
 */
public interface RegistrationService extends IGenericService<Registration> {

    List<Registration> getRegistrations(Registrant registrant);

    Registration getRegistrationByApplication(Application application);

    Registration getCurrentRegistration(Institution institution);

    Registration getCurrentRegistrationByRegistrant(Registrant registrant);

    Boolean getRegistrationIsDeRegistered(Registrant registrant, RegistrantStatus registrantStatus);

    Boolean hasBeenDeRegistered(Registrant registrant);

    List<Registration> getRegistrations(Course course, Register register, Institution institution);

    List<Registrant> getRegistrants(Course course, Register register, Institution institution, String searchText, String gender, Citizenship citizenship, Date startDate, Date endDate);

    List<RegistrantData> getRegistrantList(Course course, Register register, Institution institution, RegistrantStatus registrantStatus, String searchText, String gender, Citizenship citizenship, Date startDate, Date endDate);

    List<Registration> getActiveRegistrations(Registrant registrant);

    Integer getActiveRegistrations(Course course, Register register);

    Registration getCurrentRegistrationByRegister(Register register, Registrant registrant);

    List<Registration> getRegistrations(Course course, Register register, Institution institution, String searchText, String gender, Citizenship citizenship);

    Boolean hasCurrentRegistration(Registrant registrant);

    Registration getCurrentRegistrationByCourseAndRegister(Registrant registrant, Register register, Course course);

    List<Registration> getRegistrationsAll(Course course, Register register, Registrant registrant);

    List<Registration> getActiveRegistrationByRegister(Course course, Register register);

    List<Course> getDistinctCourseFromRegistration();

    List<Registration> getActiveRegistrations(Course course);

    List<Registration> getActiveRegistrations(String idList);

    List<Registration> getRegisteredInstitutionsRegistrations(Boolean closed);

    Long getNumberOfYearsInRegister(Registration registration);

    boolean isDueForTransferFromProvisionalToMainRegister(Registration registration);

    List<RegistrantData> getRegistrantsDueForTransfer();

    boolean hasBeenRegisteredBeforeCouncilDuration(CouncilDuration councilDuration, Long registrantId);

    List<Long> getRegistrantIdsRegisteredBeforeCouncilDuration(CouncilDuration councilDuration);


}
