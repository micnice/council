package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.RegistrantStatusDAO;
import zw.co.hitrac.council.business.domain.RegistrantStatus;
import zw.co.hitrac.council.business.service.RegistrantStatusService;

import java.util.List;

/**
 *
 * @author Charles Chigoriwa
 */
@Service
@Transactional
public class RegistrantStatusServiceImpl implements RegistrantStatusService{
    
    @Autowired
    private RegistrantStatusDAO registrantStatusDAO;
    

    @Transactional
    public RegistrantStatus save(RegistrantStatus registrantStatus) {
       return registrantStatusDAO.save(registrantStatus);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantStatus> findAll() {
        return registrantStatusDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantStatus get(Long id) {
       return registrantStatusDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantStatus> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setRegistrantStatusDAO(RegistrantStatusDAO registrantStatusDAO) {

        this.registrantStatusDAO = registrantStatusDAO;
    }
    
}
