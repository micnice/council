package zw.co.hitrac.council.webservices.api;

import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.webservices.api.domain.LeanRegistrant;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Daniel Nkhoma
 */
@Path("/")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public interface RegistrantWebService extends Serializable {

    @GET
    @Path("registrants")
    public List<Registrant> getRegistrants();

    @GET
    @Path("lean-registrants")
    public List<LeanRegistrant> getAllLean();

    @GET
    @Path(value = "registrant2")
    public Registrant getRegistrant(@QueryParam(value = "id") Long id);

    @GET
    @Path(value = "registrant")
    public Registrant getRegistrant(@QueryParam(value = "registrationNumber") String registrationNumber);

    @GET
    @Path("renewalPeriods")
    public List<CouncilDuration> getRegisteredRenewalPeriods(@QueryParam(value = "registrationNumber") String registrationNumber);

    @GET
    @Path("lastCouncilDurationRenewal")
    public CouncilDuration getLastCouncilDurationRenewal(@QueryParam(value = "registrationNumber") String registrationNumber);
}
