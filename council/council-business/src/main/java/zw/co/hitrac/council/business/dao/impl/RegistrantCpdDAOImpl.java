package zw.co.hitrac.council.business.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.RegistrantCpdDAO;
import zw.co.hitrac.council.business.dao.repo.RegistrantCpdRepository;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantCpd;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrantCpdItemConfigService;
import zw.co.hitrac.council.business.service.RegistrationService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Constance Mabaso
 */
@Repository
public class RegistrantCpdDAOImpl implements RegistrantCpdDAO {

    @Autowired
    private RegistrantCpdRepository registrantCpdRepository;
    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    private GeneralParametersService generalParametersService;
    @Autowired
    private RegistrantCpdItemConfigService registrantCpdItemConfigService;
    @Autowired
    private RegistrationService registrationService;

    public RegistrantCpd save(RegistrantCpd registrantCpd) {
        return registrantCpdRepository.save(registrantCpd);
    }

    public List<RegistrantCpd> findAll() {
        return registrantCpdRepository.findAll();
    }

    public RegistrantCpd get(Long id) {
        return registrantCpdRepository.findOne(id);
    }

    /**
     * A setter method that will make mocking repo object easier
     *
     * @param RegistrantCpdRepository
     */
    public void setRegistrantCpdRepository(RegistrantCpdRepository registrantCpdRepository) {
        this.registrantCpdRepository = registrantCpdRepository;
    }

    public List<RegistrantCpd> getCurrentCPDsList(Registrant registrant, Duration duration) {
        return entityManager.createQuery("SELECT r FROM RegistrantCpd r where r.registrant=:registrant AND r.duration=:duration AND r.retired = false ORDER BY r.registrantCPDItem.registrantCPDCategory.name ").setParameter("registrant", registrant).setParameter("duration", duration).getResultList();
    }

    public List<RegistrantCpd> getCurrentCPDsList(Registrant registrant) {
        return entityManager.createQuery("SELECT r FROM RegistrantCpd r where r.registrant=:registrant AND r.retired = false ORDER BY r.registrantCPDItem.registrantCPDCategory.name").setParameter("registrant", registrant).getResultList();
    }

    public List<RegistrantCpd> getCPDsList(Registrant registrant) {
        return entityManager.createQuery("SELECT r FROM RegistrantCpd r where r.registrant=:registrant AND r.retired = false").setParameter("registrant", registrant).getResultList();

    }

    @Override
    public BigDecimal getCurrentCPDs(Registrant registrant, Duration duration) {
        BigDecimal points = null;

        if (generalParametersService.get().getCaptureCPDsDirectly()) {
            points = (BigDecimal) entityManager.createQuery("SELECT SUM(r.point) FROM RegistrantCpd r where r.registrant=:registrant AND r.duration=:duration AND r.retired = false").setParameter("registrant", registrant).setParameter("duration", duration).getSingleResult();
        } else {
            points = (BigDecimal) entityManager.createQuery("SELECT SUM(r.registrantCPDItem.points) FROM RegistrantCpd r where r.registrant=:registrant AND r.duration=:duration AND r.retired = false").setParameter("registrant", registrant).setParameter("duration", duration).getSingleResult();
        }
        if (generalParametersService.get().getCaptureCPDsDirectlyAndInDirectly()) {
            points = BigDecimal.ZERO;
            BigDecimal one = (BigDecimal) entityManager.createQuery("SELECT SUM(r.point) FROM RegistrantCpd r where r.registrant=:registrant AND r.duration=:duration AND r.retired = false").setParameter("registrant", registrant).setParameter("duration", duration).getSingleResult();

            if (one == null) {
                one = BigDecimal.ZERO;
            }
            BigDecimal two = (BigDecimal) entityManager.createQuery("SELECT SUM(r.registrantCPDItem.points) FROM RegistrantCpd r where r.registrant=:registrant AND r.duration=:duration AND r.retired = false").setParameter("registrant", registrant).setParameter("duration", duration).getSingleResult();

            if (two == null) {
                two = BigDecimal.ZERO;
            }
            points = points.add(one);
            points = points.add(two);
        }
        if (points == null) {
            return BigDecimal.ZERO;
        } else {
            return points;
        }
    }

    @Override
    public BigDecimal getCurrentCPDs(Registrant registrant, CouncilDuration duration) {
        BigDecimal points = null;
        if (generalParametersService.get().getCaptureCPDsDirectlyAndInDirectly()) {
            points = BigDecimal.ZERO;
            BigDecimal one = (BigDecimal) entityManager.createQuery("SELECT SUM(r.point) FROM RegistrantCpd r where r.registrant=:registrant AND r.duration.councilDuration=:duration AND r.retired = false").setParameter("registrant", registrant).setParameter("duration", duration).getSingleResult();

            if (one == null) {
                one = BigDecimal.ZERO;
            }
            BigDecimal two = (BigDecimal) entityManager.createQuery("SELECT SUM(r.registrantCPDItem.points) FROM RegistrantCpd r where r.registrant=:registrant AND r.duration.councilDuration=:duration AND r.retired = false").setParameter("registrant", registrant).setParameter("duration", duration).getSingleResult();

            if (two == null) {
                two = BigDecimal.ZERO;
            }

            points = points.add(one);
            points = points.add(two);
        } else if (generalParametersService.get().getCaptureCPDsDirectly()) {
            points = (BigDecimal) entityManager.createQuery("SELECT SUM(r.point) FROM RegistrantCpd r where r.registrant=:registrant AND r.duration.councilDuration=:duration AND r.retired = false").setParameter("registrant", registrant).setParameter("duration", duration).getSingleResult();
        } else {
            points = (BigDecimal) entityManager.createQuery("SELECT SUM(r.registrantCPDItem.points) FROM RegistrantCpd r where r.registrant=:registrant AND r.duration.councilDuration=:duration AND r.retired = false").setParameter("registrant", registrant).setParameter("duration", duration).getSingleResult();
        }

        if (points == null) {
            return BigDecimal.ZERO;
        } else {
            return points;
        }
    }

    public List<RegistrantCpd> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<RegistrantData> getPaidAnnualFeeAndInadequateCPDS(CouncilDuration councilDuration) {
        List<Object[]> results = this.entityManager.createNativeQuery("SELECT DISTINCT reg.id as id, (SELECT sum(rp.points) FROM registrantcpditem rp INNER JOIN registrantcpd rd on rd.registrantCPDItem_id = rp.id INNER JOIN duration dd on dd.id =rd.duration_id WHERE rd.registrant_id= reg.id and dd.councilDuration_id= " + councilDuration.getId() + ") as points FROM receiptitem r INNER JOIN receiptheader rh on rh.id = r.receiptHeader_id INNER JOIN paymentdetails pd on pd.id = rh.paymentDetails_id INNER JOIN debtcomponent d on d.id = r.debtComponent_id INNER JOIN product p on p.id = d.product_id INNER JOIN registrant reg on reg.customerAccount_id = pd.customer_id WHERE r.paymentPeriod_id= " + councilDuration.getId() + " and p.typeOfService='ANNUAL_FEES'").getResultList();
        final BigDecimal totalCpdPointsPermissable = registrantCpdItemConfigService.get().getTotalCpdPointsPermissable();

        List<Long> registrantIdsRegistered = registrationService.getRegistrantIdsRegisteredBeforeCouncilDuration(councilDuration);

        return results.stream().map((record) -> {
            BigInteger id = (BigInteger) record[0];
            BigDecimal points = BigDecimal.ZERO;

            if (record[1] != null)
                points = (BigDecimal) record[1];


            if (registrantIdsRegistered.contains(id.longValue())) {
                return new RegistrantData(id.longValue(), points);
            } else {
                return null;
            }


        }).filter(record -> record != null).filter(record -> record.getPoints().compareTo(totalCpdPointsPermissable) == -1).collect(Collectors.toList());

    }
}
