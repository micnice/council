package zw.co.hitrac.council.business.service;

import java.util.List;
import zw.co.hitrac.council.business.domain.District;
import zw.co.hitrac.council.business.domain.Province;

/**
 *
 * @author Constance Mabaso
 */
public interface DistrictService extends IGenericService<District> {
    
    public List<District> getDistrictsInProvince(Province province);
}
