package zw.co.hitrac.council.business.domain.reports;

import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Registration;

import java.math.BigDecimal;

public class PaymentDue {

    private Registration registration;
    private BigDecimal totalAmountForRenewal = BigDecimal.ZERO;
    private BigDecimal totalAmountForPenalty = BigDecimal.ZERO;
    private BigDecimal totalAmountDue = BigDecimal.ZERO;
    private CouncilDuration LastRenewal;
    private Integer periodsDue;

    public BigDecimal getTotalAmountForRenewal() {
        return totalAmountForRenewal;
    }

    public void setTotalAmountForRenewal(BigDecimal totalAmountForRenewal) {
        this.totalAmountForRenewal = totalAmountForRenewal;
    }

    public BigDecimal getTotalAmountForPenalty() {
        return totalAmountForPenalty;
    }

    public void setTotalAmountForPenalty(BigDecimal totalAmountForPenalty) {
        this.totalAmountForPenalty = totalAmountForPenalty;
    }

    public Registration getRegistration() {
        return registration;
    }

    public void setRegistration(Registration registration) {
        this.registration = registration;
    }

    public BigDecimal getTotalAmountDue() {
        totalAmountDue = totalAmountForRenewal.add(totalAmountForPenalty);
        return totalAmountDue;
    }

    public void setTotalAmountDue(BigDecimal totalAmountDue) {
        this.totalAmountDue = totalAmountDue;
    }

    public CouncilDuration getLastRenewal() {
        return LastRenewal;
    }

    public void setLastRenewal(CouncilDuration lastRenewal) {
        LastRenewal = lastRenewal;
    }

    public Integer getPeriodsDue() {
        return periodsDue;
    }

    public void setPeriodsDue(Integer periodsDue) {
        this.periodsDue = periodsDue;
    }
}
