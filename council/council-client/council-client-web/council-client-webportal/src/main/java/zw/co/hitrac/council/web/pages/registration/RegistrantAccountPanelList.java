/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.registration;

import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.domain.accounts.PaymentDetails;
import zw.co.hitrac.council.business.domain.accounts.ReceiptItem;
import zw.co.hitrac.council.business.process.*;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.business.service.accounts.*;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.pages.accounts.documents.*;
import zw.co.hitrac.council.web.pages.accounts.payments.PaymentConfirmationPage;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;
import zw.co.hitrac.council.business.process.PaymentProcess;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;

/**
 * @author kelvin
 */
public class RegistrantAccountPanelList extends Panel {


    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    AccountService accountService;
    @SpringBean
    CustomerService customerAccountService;
    @SpringBean
    RegistrationService registrationService;
    @SpringBean
    private RegistrationProcess registrationProcess;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private SupExamRegistrationProcess supExamRegistrationProcess;
    @SpringBean
    private DurationService durationService;
    @SpringBean
    private RegistrantActivityService registrantActivityService;
    @SpringBean
    private DebtComponentService debtComponentService;
    private static String UP_GRADE = "UP_GRADE";
    private static String EXAM_UP_GRADE = "EXAM_UP_GRADE";
    @SpringBean
    private InvoiceProcess invoiceProcess;
    @SpringBean
    private ReceiptItemService receiptItemService;
    private DetachableRegistrantModel detachableRegistrantModel;
    @SpringBean
    private RegistrantDisciplinaryService disciplinaryService;
    @SpringBean
    private AccountsParametersService accountsParametersService;
    @SpringBean
    private PaymentProcess paymentProcess;
    @SpringBean
    private BillingProcess billingProcess;


    public RegistrantAccountPanelList(String id, final IModel<Registrant> registrantModel) {
        super(id);

        final Boolean deRegistered = registrationService.hasBeenDeRegistered(registrantModel.getObject());

        add(new Label("customerAccount.account.balance"));

        add(new Link<Registrant>("invoiceLink") {
            @Override
            public void onClick() {
                setResponsePage(new InvoicePage(registrantModel.getObject().getCustomerAccount(), Boolean.FALSE));
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                return enabled;
            }
        });

        add(new Link<Registrant>("invoiceAmountLink") {
            @Override
            public void onClick() {
                setResponsePage(new InvoiceAmountPage(registrantModel.getObject().getCustomerAccount()));
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                return enabled;
            }
        });

        add(new Link<Registrant>("creditBalance") {
            @Override
            public void onClick() {
                setResponsePage(new TakeOnBalanceEditPage(registrantModel.getObject().getId()));
            }
        });

        add(new Link<Registrant>("paythroughCarryForward") {
            @Override
            public void onClick() {
                setResponsePage(new CarryForwardPage(registrantModel.getObject().getId()));
            }

            @Override
            protected void onConfigure() {
                Customer customer = registrantService.get(registrantModel.getObject().getId()).getCustomerAccount();
                if (customer.getAccount().getBalance().compareTo(new BigDecimal("0")) == -1) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(Boolean.FALSE);
                }
            }
        });

        add(new Link<Registrant>("verifyAccount") {
            @Override
            public void onClick() {
                Customer customer = registrantService.get(registrantModel.getObject().getId()).getCustomerAccount();
                if (generalParametersService.get().getRegistrationByApplication()) {

                    List<DebtComponent> debtComponents = debtComponentService.getDebtComponents(customer);

                    if (debtComponents.isEmpty()) {

                        setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
                    } else {
                        //selet items to pay for
                        setResponsePage(new DebtComponentsPage(registrantService.getRegistrant(customer).getId()));
                    }
                } else {
                    List<DebtComponent> debtComponents = debtComponentService.getDebtComponents(customer.getAccount());
                    List<ReceiptItem> receiptItemList = receiptItemService.getReceiptItemsWithNoReceiptHeader(customer.getAccount());

                    if (debtComponents.isEmpty() && !receiptItemList.isEmpty()) {
                        Set<ReceiptItem> receiptItems = new HashSet<ReceiptItem>();

                        if (receiptItemList.isEmpty()) {
                            setResponsePage(new RegistrantViewPage(registrantService.getRegistrant(customer).getId()));
                        } else {
                            for (ReceiptItem receiptItem : receiptItemList) {
                                receiptItems.add(receiptItem);
                            }
                            PaymentDetails paymentDetails = paymentProcess.accountBallancePaysAll(receiptItems, CouncilSession.get().getUser(), customer, null);

                            setResponsePage(new PaymentConfirmationPage(paymentDetails, customer.getAccount()));
                        }
                    } else if (!debtComponents.isEmpty() && !receiptItemList.isEmpty()) {
                        Set<ReceiptItem> receiptItems = new HashSet<ReceiptItem>();
                        if (receiptItemList.isEmpty()) {
                            setResponsePage(new RegistrantViewPage(registrantService.getRegistrant(customer).getId()));
                        } else {
                            for (ReceiptItem receiptItem : receiptItemList) {
                                receiptItems.add(receiptItem);
                            }

                            paymentProcess.accountBalancePaysLess(receiptItems, CouncilSession.get().getUser(), customer, null);
                        }
                        setResponsePage(new DebtComponentsPage(registrantService.getRegistrant(customer).getId()));

                    } else {
                        //Select the items you want to pay for
                        setResponsePage(new DebtComponentsPage(registrantService.getRegistrant(customer).getId()));
                    }
                }
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.IT_OFFICER, Role.REGISTRY_CLERK)));
                return enabled;
            }

            @Override
            protected void onConfigure() {
                if (registrantModel.getObject().getDead()) {
                    setVisible(Boolean.FALSE);
                }
            }
        });

        add(new Link<Registrant>("registrantPaymentPage") {
            @Override
            public void onClick() {
                setResponsePage(new DebtComponentsPage(registrantModel.getObject().getId()));
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                return enabled;
            }

            @Override
            protected void onConfigure() {
                if (deRegistered) {
                    setVisible(Boolean.FALSE);
                }
            }
        });

        add(new Link<Registrant>("creditNoteLink") {
            @Override
            public void onClick() {
                setResponsePage(new CreditNotePage(registrantModel.getObject().getId()));
            }

            @Override
            protected void onConfigure() {
                if (deRegistered) {
                    setVisible(Boolean.FALSE);
                } else {
                    if (registrantModel.getObject().getDead()) {
                        setVisible(Boolean.FALSE);
                    } else {
                        setVisibilityAllowed(CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.ACCOUNTS_OFFICER))));
                    }
                }
            }
        });

        add(new Link<Registrant>("viewRemovedNotesPage") {
            @Override
            public void onClick() {
                setResponsePage(new ViewRemovedNotesPage(registrantModel.getObject().getId()));
            }

            @Override
            protected void onConfigure() {
                if (deRegistered) {
                    setVisible(Boolean.FALSE);
                } else {
                    if (registrantModel.getObject().getDead()) {
                        setVisible(Boolean.FALSE);
                    } else {
                        setVisibilityAllowed(CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.ACCOUNTS_OFFICER))));
                    }
                }
            }
        });

        add(new PropertyListView<RegistrantActivity>("registrantActivityList", registrantActivityService.getRegistrantActivities(registrantModel.getObject(), RegistrantActivityType.RENEWAL)) {
            @Override
            protected void populateItem(ListItem<RegistrantActivity> item) {
                item.add(new Label("duration.councilDuration"));
            }
        });

        add(new PropertyListView<CouncilDuration>("durationList", registrantActivityService.getRenewalPeriodsNotRenewed(registrantModel.getObject(), RegistrantActivityType.RENEWAL)) {
            @Override
            protected void populateItem(ListItem<CouncilDuration> item) {

                item.add(new Label("name"));

                item.add(new Link<Registrant>("billAnnualFee") {
                    @Override
                    public void onClick() {
                        billingProcess.billAnnualProduct(registrationProcess.activeRegisterNotStudentRegister(registrantModel.getObject()), new DebtComponent());
                        setResponsePage(new DebtComponentsPage(registrantModel.getObject().getId()));
                    }

                    @Override
                    public boolean isEnabled() {
                        Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                        return enabled;
                    }

                    @Override
                    protected void onConfigure() {
                        if (deRegistered) {
                            setVisible(Boolean.FALSE);
                        }
                    }
                });
            }
        });

        add(new Link("prepayments") {

            @Override
            public void onClick() {
                setResponsePage(new PrepaymentDetailListPage(registrantModel.getObject()));
            }

        });

    }

}
