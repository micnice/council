package zw.co.hitrac.council.web.pages.accounts.documents;

//~--- non-JDK imports --------------------------------------------------------
import zw.co.hitrac.council.business.domain.accounts.PaymentData;
import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.lang.Bytes;

import zw.co.hitrac.council.business.service.accounts.PaymentDataService;

import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author Michael Matiashe
 */
public class PastelUploadPage extends TemplatePage {

    @SpringBean
    private PaymentDataService paymentDataService;
 
    public PastelUploadPage() {

        // Create feedback panels
        final FeedbackPanel uploadFeedback = new FeedbackPanel("uploadFeedback");

        // Add uploadFeedback to the page itself
        add(uploadFeedback);

        final FileUploadForm simpleUploadForm = new FileUploadForm("simpleUpload");

        add(simpleUploadForm);
    }

    private class FileUploadForm extends Form<Void> {

        FileUploadField fileUploadField;

        /**
         * Construct.
         *
         * @param name Component name
         */
        public FileUploadForm(String name) {
            super(name);

            // set this form to multipart mode (allways needed for uploads!)
            setMultiPart(true);

            // Add one file input field
            add(fileUploadField = new FileUploadField("fileInput"));

            // Set maximum size to 100K for demo purposes
            setMaxSize(Bytes.kilobytes(900));
        }

        /**
         * @see org.apache.wicket.markup.html.form.Form#onSubmit()
         */
        @Override
        protected void onSubmit() {
            final FileUpload upload = fileUploadField.getFileUpload();

            try {
                if (upload != null) {

                    // write to a new file
                    File newFile = new File("/home/micnice/uploads/" + upload.getClientFileName());

                    if (newFile.exists()) {
                        newFile.delete();
                    }

                    try {
                        newFile.createNewFile();
                        upload.writeTo(newFile);

                        info("saved file: " + upload.getClientFileName());
                    } catch (Exception e) {
                        throw new IllegalStateException("Error");
                    }
                    List<PaymentData> datas = readSheetWithFormula(newFile);

                    setResponsePage(new PaymentDataListPage(datas));
                }

            } catch (Exception ex) {
                error(ex.getMessage());
                ex.printStackTrace(System.out);
            }

        }
    }

    public List<PaymentData> readSheetWithFormula(File file1) {
        List<PaymentData> paymentDatas = new ArrayList<PaymentData>();
        try {

            FileInputStream file = new FileInputStream(file1);

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first/desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            for (int i = 0; i < 4; i++) {
                Row row = rowIterator.next();
            }
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                PaymentData paymentData = new PaymentData();

                Iterator<Cell> cellIterator = row.cellIterator();
                Cell cell = cellIterator.next();
                paymentData.setReceiptNumber(cell.getStringCellValue());
                if (!paymentData.getReceiptNumber().contains("RC")) {
                    continue;
                }
                System.out.println("=====" + cell.getStringCellValue());
                cell = cellIterator.next();
                if (!cell.getStringCellValue().contains("/")) {
                    continue;
                }
                paymentData.setDateOfPayment(new Date(cell.getStringCellValue()));
                cell = cellIterator.next();
                String[] regNumNameArray = cell.getStringCellValue().split("-");
                paymentData.setRegNumber(regNumNameArray[0]);
                paymentData.setName(regNumNameArray[1]);
                cell = cellIterator.next();
                paymentData.setDescription(cell.getStringCellValue());
                cell = cellIterator.next();
                paymentData.setCode(cell.getStringCellValue());
                cell = cellIterator.next();
                cell = cellIterator.next();
                System.out.println("================ First");
                paymentData.setAmount(new BigDecimal(cell.getNumericCellValue()));
                cell = cellIterator.next();
                System.out.println("================ Second");
                paymentData.setTotalAmount(new BigDecimal(cell.getNumericCellValue()));
                System.out.println("=================== " + paymentData.getTotalAmount() + " Ndiyoyi ");
                paymentData = paymentDataService.save(paymentData);
                paymentDatas.add(paymentData);
            }
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return paymentDatas;
    }

}


//~ Formatted by Jindent --- http://www.jindent.com
