package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author Michael Matiashe
 */
@Entity
@Table(name = "conditions")
@Audited
public class Conditions extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    private Boolean generalCondition = Boolean.FALSE;
    private Boolean examCondition = Boolean.FALSE; // if true exam requirement    
    private Boolean registerCondition = Boolean.FALSE; // if true register requirement
    private ProductIssuanceType productIssuanceType;
    private String comments ="";

    @Column(length = 1024)
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Boolean getGeneralCondition() {
        return generalCondition;
    }

    public void setGeneralCondition(Boolean generalCondition) {
        this.generalCondition = generalCondition;
    }

    public Boolean getExamCondition() {
        if (examCondition == null) {
            return Boolean.FALSE;
        }
        return examCondition;
    }

    public void setExamCondition(Boolean examCondition) {
        this.examCondition = examCondition;
    }

    public Boolean getRegisterCondition() {
        if (registerCondition == null) {
            return Boolean.FALSE;
        }
        return registerCondition;
    }

    public void setRegisterCondition(Boolean registerCondition) {
        this.registerCondition = registerCondition;
    }

    @Enumerated(EnumType.STRING)
    public ProductIssuanceType getProductIssuanceType() {
        return productIssuanceType;
    }

    public void setProductIssuanceType(ProductIssuanceType productIssuanceType) {
        this.productIssuanceType = productIssuanceType;
    }
}
