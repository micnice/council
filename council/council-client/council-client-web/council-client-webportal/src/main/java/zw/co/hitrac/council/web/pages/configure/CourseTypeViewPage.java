package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CourseType;
import zw.co.hitrac.council.business.service.CourseTypeService;

/**
 *
 * @author charlesc
 */
public class CourseTypeViewPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private CourseTypeService courseTypeService;

    public CourseTypeViewPage(Long id) {
        CompoundPropertyModel<CourseType> model=new CompoundPropertyModel<CourseType>(new LoadableDetachableCourseTypeModel(id));
        setDefaultModel(model);
        add(new Link<CourseType>("editLink",model){

            @Override
            public void onClick() {
                setResponsePage(new CourseTypeEditPage(getModelObject().getId()));
            }
            
        });
        add(new BookmarkablePageLink<Void>("returnLink", CourseTypeListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("courseType",model.bind("name")));
    }


    private final class LoadableDetachableCourseTypeModel extends LoadableDetachableModel<CourseType> {

        private Long id;

        public LoadableDetachableCourseTypeModel(Long id) {
            this.id = id;
        }

        @Override
        protected CourseType load() {

            return courseTypeService.get(id);
        }
    }
}
