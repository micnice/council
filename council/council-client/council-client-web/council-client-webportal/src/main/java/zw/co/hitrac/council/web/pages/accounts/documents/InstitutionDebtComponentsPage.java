package zw.co.hitrac.council.web.pages.accounts.documents;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Check;
import org.apache.wicket.markup.html.form.CheckGroup;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.service.accounts.AccountService;
import zw.co.hitrac.council.business.service.accounts.CustomerService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.accounts.payments.PaymentPage;
import zw.co.hitrac.council.web.pages.institution.InstitutionViewPage;
import zw.co.hitrac.council.web.utility.DetachableInstitutionModel;

/**
 *
 * @author Takunda Dhlakama
 * @author Charles Chigoriwa
 */
public class InstitutionDebtComponentsPage extends TemplatePage {

    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    AccountService accountService;
    @SpringBean
    CustomerService customerAccountService;
    @SpringBean
    RegistrationService registrationService;
    @SpringBean
    DebtComponentService debtComponentService;
    @SpringBean
    GeneralParametersService generalParametersService;
    private List<DebtComponent> selectedDebtComponents = new ArrayList<DebtComponent>();
    private Long institutionId;

    public InstitutionDebtComponentsPage(PageParameters parameters) {
        this(parameters.get("institutionId").toLong());
    }

    public InstitutionDebtComponentsPage(Long id) {
        institutionId = id;
        final CompoundPropertyModel<Institution> institutionModel = new CompoundPropertyModel<Institution>(new DetachableInstitutionModel(id, institutionService));
        setDefaultModel(institutionModel);
        add(new Label("name"));

        IModel<List<DebtComponent>> debtComponentsModel = new LoadableDetachableModel<List<DebtComponent>>() {
            @Override
            protected List<DebtComponent> load() {
                return debtComponentService.getDebtComponents(institutionModel.getObject().getCustomerAccount());
            }
        };

        Form<Void> form = new Form<Void>("form") {
            @Override
            protected void onSubmit() {
                if (!selectedDebtComponents.isEmpty()) {
                    setResponsePage(new PaymentPage(institutionModel.getObject().getCustomerAccount(), selectedDebtComponents));
                }
            }
        };

        CheckGroup<DebtComponent> selectedDebtComponentsGroup = new CheckGroup<DebtComponent>("selectedDebtComponents", this.selectedDebtComponents);
        form.add(selectedDebtComponentsGroup);
        form.add(new Link<Void>("institutionViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionViewPage(institutionModel.getObject().getId()));
            }
        });

        form.add(new Link<Registrant>("invoiceLink") {
            @Override
            public void onClick() {
                setResponsePage(new InvoicePage(institutionModel.getObject().getCustomerAccount(), Boolean.TRUE));
            }
        });

        form.add(new Link<Registrant>("paythroughCarryForward") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionCarryFowardPage(institutionModel.getObject().getId()));
            }

            @Override
            protected void onConfigure() {
                Customer customer = institutionService.get(institutionModel.getObject().getId()).getCustomerAccount();
                if (customer.getAccount().getBalance().compareTo(new BigDecimal("0")) == -1) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(Boolean.FALSE);
                }
            }

        });

        form.add(new Link<Registrant>("invoiceAmountLink") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionInvoiceAmountPage(institutionModel.getObject().getCustomerAccount()));
            }
        });
        
        form.add(new Link<Registrant>("creditNoteLink") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionCreditNotePage(institutionModel.getObject().getId()));
            }
        });

        add(form);
        PropertyListView<DebtComponent> eachItem = new PropertyListView<DebtComponent>("eachItem", debtComponentsModel) {
            @Override
            protected void populateItem(final ListItem<DebtComponent> item) {

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(new Check("selected", item.getModel()));
                item.add(new Label("transactionComponent.account.name"));
                item.add(new Label("remainingBalance"));
            }
        };

        selectedDebtComponentsGroup.add(eachItem);

    }

    @Override
    protected void onConfigure() {
        //if billing module disabled - direct to dashboard
        if (!generalParametersService.get().isBillingModule()) {
            if (institutionId != null) {
                setResponsePage(new InstitutionViewPage(institutionId));
            }
        }

    }
}
