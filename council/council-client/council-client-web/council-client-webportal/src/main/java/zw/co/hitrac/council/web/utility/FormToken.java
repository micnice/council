/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.utility;

import java.io.Serializable;
import org.apache.wicket.PageReference;

/**
 *
 * @author micnice
 */
public class FormToken implements Serializable {

    private final PageReference reference;
    private final String pathToForm;

    public FormToken(PageReference reference, String pathToForm) {
        this.reference = reference;
        this.pathToForm = pathToForm;
    }

    @Override
    public int hashCode() {
        return 31 * reference.hashCode()
                * pathToForm.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (obj instanceof FormToken) {
            FormToken other = (FormToken) obj;
            return other.reference.equals(reference) && other.pathToForm.equals(pathToForm);
        }
        return false;
    }
}
