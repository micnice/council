package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.service.CouncilDurationService;
import zw.co.hitrac.council.business.service.DurationService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Michael Matiashe
 */
public class CouncilDurationEditPage extends IAdministerDatabaseBasePage {
    
    @SpringBean
    private CouncilDurationService councilDurationService;
    @SpringBean
    private DurationService durationService;
    
    public CouncilDurationEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<CouncilDuration>(new LoadableDetachableCouncilDurationModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        Form<CouncilDuration> form = new Form<CouncilDuration>("form", (IModel<CouncilDuration>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                councilDurationService.save(getModelObject());
                setResponsePage(new CouncilDurationViewPage(getModelObject().getId()));
            }
        };
        
        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink", DurationListPage.class));
        form.add(new CheckBox("active"));
        add(form);

    }
    
    private final class LoadableDetachableCouncilDurationModel extends LoadableDetachableModel<CouncilDuration> {
        
        private Long id;
        
        public LoadableDetachableCouncilDurationModel(Long id) {
            this.id = id;
        }
        
        @Override
        protected CouncilDuration load() {
            if (id == null) {
                return new CouncilDuration();
            }
            return councilDurationService.get(id);
        }
    }
}
