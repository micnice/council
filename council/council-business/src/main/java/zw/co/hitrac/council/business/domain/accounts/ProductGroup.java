/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain.accounts;

import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.domain.BaseEntity;
import zw.co.hitrac.council.business.domain.Course;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

/**
 *
 * @author tdhlakama
 */
@Entity
@Table(name="productgroup")
@Audited
public class ProductGroup extends BaseEntity implements Serializable {

    //class created to group products - for example (GN Paper 1 and GN Paper 2) = GN Exam
    // Easier to find total figures for group items
    @ManyToOne
    private Course course;

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
}
