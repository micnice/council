
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------
import com.googlecode.wicket.jquery.ui.form.button.AjaxButton;
import static com.googlecode.wicket.jquery.ui.widget.dialog.AbstractDialog.LBL_YES;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButton;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButtons;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogIcon;
import com.googlecode.wicket.jquery.ui.widget.dialog.MessageDialog;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationService;

//~--- JDK imports ------------------------------------------------------------
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.form.Form;
import zw.co.hitrac.council.web.config.CouncilSession;

/**
 *
 * @author tdhlakama
 */
public class RegistrantExamResultPage extends IExaminationsPage {

    @SpringBean
    private ExamRegistrationService examRegistrationService;
  
    public RegistrantExamResultPage(Long id) {
        final CompoundPropertyModel<ExamRegistration> model
                = new CompoundPropertyModel<ExamRegistration>(
                        new RegistrantExamResultPage.LoadableDetachableExamRegistrationModel(id));

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(model.getObject().getRegistration().getRegistrant().getId());
        setDefaultModel(model);
        add(new RegistrantExamResultPanelList("examResultsPanelList"));
        add(new Label("registration.registrant.fullname"));
        add(new Label("candidateNumber"));
        add(new Label("examSetting"));
        add(new Label("registration.course"));

        final MessageDialog warningDialog = new MessageDialog("warningDialog", "Warning", "Are you sure these are the correct Results?", DialogButtons.YES_NO, DialogIcon.WARN) {
            private static final long serialVersionUID = 1L;

            @Override
            public void onClose(AjaxRequestTarget target, DialogButton button) {
                if (button != null && button.equals(LBL_YES)) {
                    if (model.getObject().getPassComment() == null || model.getObject().getPassComment().isEmpty() || model.getObject().getPassComment().length() == 0) {
                        setResponsePage(new ExamRegistrationCommentPage(model.getObject().getId()));
                    } else {
                        if (CouncilSession.get().getUser() != null) {
                            model.getObject().setClosed(Boolean.TRUE); // Exam cannot be edited
                            model.getObject().setCreatedBy(CouncilSession.get().getUser());
                            examRegistrationService.save(model.getObject());
                        }
                        setResponsePage(new RegistrantExamResultPage(model.getObject().getId()));
                    }
                }
            }
        };

        this.add(warningDialog);

        final Form<Void> form = new Form<Void>("form");
        this.add(form);
        form.add(new AjaxButton("closeExam") {
            private static final long serialVersionUID = 1L;

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                warningDialog.open(target);
            }

            @Override
            protected void onConfigure() {

                if (model.getObject().getPassStatus() && !model.getObject().getClosed() || model.getObject().getAtLeastOneMarkStatus() && !model.getObject().getPassStatus() && !model.getObject().getClosed()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(Boolean.FALSE);
                }

            }
        });

        form.add(new AjaxButton("registrantExaminationsPage") {
            private static final long serialVersionUID = 1L;

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                setResponsePage(new RegistrantExaminationsPage(model.getObject().getRegistration().getId()));
            }
        });

        form.add(new AjaxButton("examRegistrationCommentPage") {
            private static final long serialVersionUID = 1L;

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                setResponsePage(new ExamRegistrationCommentPage(model.getObject().getId()));
            }

            @Override
            protected void onConfigure() {
                setVisible(!model.getObject().getClosed());
            }
        });

        form.add(new AjaxButton("examRegistrationSearchPage") {
            private static final long serialVersionUID = 1L;

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                setResponsePage(ExamRegistrationSearchPage.class);
            }
        });

    }

    private final class LoadableDetachableExamRegistrationModel extends LoadableDetachableModel<ExamRegistration> {

        private Long id;
        private Registration registration;

        public LoadableDetachableExamRegistrationModel(Long id) {
            this.id = id;
        }

        public LoadableDetachableExamRegistrationModel(Registration registration) {
            this.registration = registration;
        }

        @Override
        protected ExamRegistration load() {
            ExamRegistration examRegistration = null;

            if (id == null) {
                examRegistration = new ExamRegistration();
                examRegistration.setRegistration(registration);
            } else {
                examRegistration = examRegistrationService.get(id);
            }

            return examRegistration;
        }
    }

}


//~ Formatted by Jindent --- http://www.jindent.com
