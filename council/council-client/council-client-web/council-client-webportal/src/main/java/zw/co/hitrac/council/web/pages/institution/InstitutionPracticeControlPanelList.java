package zw.co.hitrac.council.web.pages.institution;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Application;

import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.InstitutionPracticeControl;
import zw.co.hitrac.council.business.service.DurationService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.InstitutionPracticeControlService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.utility.CustomDateLabel;

/**
 *
 * @author Constance Mabaso
 */
public class InstitutionPracticeControlPanelList extends Panel {

    @SpringBean
    InstitutionPracticeControlService institutionPracticeControlService;
    @SpringBean
    GeneralParametersService generalParametersService;
    @SpringBean
    private DurationService durationService;
    @SpringBean
    private RegistrationService registrationService;

    /**
     * Constructor
     *
     * @param id
     * @param institution
     */
    public InstitutionPracticeControlPanelList(String id, final IModel<Application> applicationModel) {
        super(id);

        // method to add a new InstitutionPracticeControl - Id is null
        add(new Link<Institution>("institutionPracticeControlPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionPracticeControlPage(applicationModel));
            }
        });
        add(new PropertyListView<InstitutionPracticeControl>("institutionPracticeControlList", institutionPracticeControlService.get(applicationModel.getObject())) {
            @Override
            protected void populateItem(ListItem<InstitutionPracticeControl> item) {
                Link<InstitutionPracticeControl> viewLink = new Link<InstitutionPracticeControl>("institutionPracticeControlPage",
                        item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new InstitutionPracticeControlPage(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(viewLink);
                item.add(new CustomDateLabel("dateCreated"));
                item.add(new Label("meetsAllRequirementsStatus"));
                item.add(new Label("decisionStatus"));
            }
        });
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
