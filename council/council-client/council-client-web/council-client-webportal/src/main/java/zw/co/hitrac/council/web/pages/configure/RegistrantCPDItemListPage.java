package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.RegistrantCPDItem;
import zw.co.hitrac.council.business.service.RegistrantCPDItemService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;

/**
 *
 * @author Constance Mabaso
 */
public class RegistrantCPDItemListPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private RegistrantCPDItemService registrantCPDItemService;

    public RegistrantCPDItemListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantCPDItemEditPage(null));
            }
        });

        IModel<List<RegistrantCPDItem>> model = new LoadableDetachableModel<List<RegistrantCPDItem>>() {
            @Override
            protected List<RegistrantCPDItem> load() {
                return registrantCPDItemService.findAll();
            }
        };
        PropertyListView<RegistrantCPDItem> eachItem = new PropertyListView<RegistrantCPDItem>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<RegistrantCPDItem> item) {
                Link<RegistrantCPDItem> viewLink = new Link<RegistrantCPDItem>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new RegistrantCPDItemViewPage(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };

        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
