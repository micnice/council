package zw.co.hitrac.council.business.domain;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.utils.CaseUtil;
import zw.co.hitrac.council.business.utils.StringFormattingUtil;
import zw.co.hitrac.council.business.utils.Util;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Takunda Dhlakama
 * @author Charles Chigoriwa
 */
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Table(name = "registrant")
@Audited
@XmlRootElement
public class Registrant extends BaseIdEntity implements Serializable {

    private static final Pattern coursePattern = Pattern.compile("[a-zA-Z]+");
    private static final Pattern registrationNumberPattern = Pattern.compile("\\d+");
    public static String ACTIVE = "ACTIVE";
    public static String INACTIVE = "INACTIVE";
    boolean order = false;
    private String firstname;
    private String lastname;
    private String middlename;
    private String maidenname;
    private Date birthDate;
    private String gender;
    private String idNumber;
    private String registrationNumber;
    private MaritalStatus maritalStatus;
    private Title title;
    private Citizenship citizenship;
    private Boolean dead = Boolean.FALSE;
    private String placeOfBirth;
    private Customer customerAccount;
    private IdentificationType identificationType;
    private Set<Registration> registrations = new HashSet<>();
    private Set<Employment> employments = new HashSet<Employment>();
    private Set<RegistrantQualification> registrantQualifications = new HashSet<RegistrantQualification>();
    private Set<RegistrantCpd> registrantCpds = new HashSet<RegistrantCpd>();
    private Set<RegistrantCondition> registrantConditions = new HashSet<RegistrantCondition>();
    private Set<Conditions> conditions = new HashSet<Conditions>();
    private Set<RegistrantDisciplinary> registrantDisciplinary = new HashSet<RegistrantDisciplinary>();
    private Set<SupportDocument> supportDocuments = new HashSet<SupportDocument>();
    private String activeStatus;
    private String activeRegisters;
    private Set<RegistrantContact> registrantContacts = new HashSet<RegistrantContact>();
    private Set<RegistrantAddress> registrantAddresses = new HashSet<RegistrantAddress>();
    private Boolean supervisor = Boolean.FALSE;
    private String comment;
    private Boolean voided = Boolean.FALSE; //(*) indicate delete record (*) retire indicate retired individual
    private Set<Institution> institutionsSupervised;
    private Set<Institution> institutionsOwned;

    @OneToMany(mappedBy = "supervisor", cascade = javax.persistence.CascadeType.ALL)
    public Set<Institution> getInstitutionsSupervised() {

        return institutionsSupervised;
    }

    public void setInstitutionsSupervised(Set<Institution> institutionsSupervised) {

        this.institutionsSupervised = institutionsSupervised;
    }

    @OneToMany(mappedBy = "registrant", cascade = javax.persistence.CascadeType.ALL)
    public Set<Institution> getInstitutionsOwned() {

        return institutionsOwned;
    }

    public void setInstitutionsOwned(Set<Institution> institutionsOwned) {

        this.institutionsOwned = institutionsOwned;
    }

    public Boolean getSupervisor() {

        return supervisor;
    }

    public void setSupervisor(Boolean supervisor) {

        this.supervisor = supervisor;
    }

    @XmlElement
    public String getPlaceOfBirth() {

        return CaseUtil.upperCase(placeOfBirth);
    }

    public void setPlaceOfBirth(String placeOfBirth) {

        this.placeOfBirth = placeOfBirth;
    }

    @OneToMany(mappedBy = "registrant")
    public Set<SupportDocument> getSupportDocuments() {

        return supportDocuments;
    }

    public void setSupportDocuments(Set<SupportDocument> supportDocuments) {

        this.supportDocuments = supportDocuments;
    }

    @Transient
    public BigDecimal getTotalPoints() {

        BigDecimal totalPoints = new BigDecimal(0);
        for (RegistrantCpd registrantCpd : getRegistrantCpds()) {
            if (registrantCpd != null && registrantCpd.getRegistrantCPDItem() != null) {
                totalPoints = totalPoints.add(registrantCpd.getRegistrantCPDItem().getPoints());
            }
        }

        return totalPoints;
    }

    @XmlTransient
    @OneToMany(mappedBy = "registrant")
    public Set<RegistrantCpd> getRegistrantCpds() {

        return registrantCpds;
    }

    public void setRegistrantCpds(Set<RegistrantCpd> registrantCpds) {

        this.registrantCpds = registrantCpds;
    }

    @XmlElement
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getBirthDate() {

        return birthDate;
    }

    public void setBirthDate(Date birthDate) {

        this.birthDate = birthDate;
    }

    @XmlElement
    @ManyToOne
    public IdentificationType getIdentificationType() {

        return identificationType;
    }

    public void setIdentificationType(IdentificationType identificationType) {

        this.identificationType = identificationType;
    }

    @XmlElement
    public String getGender() {

        return gender;
    }

    public void setGender(String gender) {

        this.gender = gender;
    }

    @XmlElement
    @Column(unique = true)
    public String getIdNumber() {

        return idNumber;
    }

    public void setIdNumber(String idNumber) {

        this.idNumber = idNumber;
    }

    @Transient
    public String getFormattedRegistrationNumber() {

        try {

            StringBuilder formattedRegistrationNumber = new StringBuilder();

            Matcher courseMatcher = coursePattern.matcher(getRegistrationNumber());
            Matcher registrationNumberMatcher = registrationNumberPattern.matcher(getRegistrationNumber());

            if (courseMatcher.find()) {

                formattedRegistrationNumber.append(courseMatcher.group(0));
            }

            formattedRegistrationNumber.append(" ");  //Separator

            if (registrationNumberMatcher.find()) {

                formattedRegistrationNumber.append(registrationNumberMatcher.group(0));
            }

            return formattedRegistrationNumber.toString().toUpperCase();
        } catch (Exception ex) {
            System.err.println("Cannot split the format of the registration number " + getRegistrationNumber());
            return getRegistrationNumber();
        }

    }

    @XmlElement
    @Column(unique = true)
    public String getRegistrationNumber() {

        return CaseUtil.upperCase(registrationNumber);
    }

    public void setRegistrationNumber(String registrationNumber) {

        this.registrationNumber = registrationNumber;
    }

    @XmlElement
    @ManyToOne
    public MaritalStatus getMaritalStatus() {

        return maritalStatus;
    }

    public void setMaritalStatus(MaritalStatus maritalStatus) {

        this.maritalStatus = maritalStatus;
    }

    @XmlElement
    @ManyToOne
    public Title getTitle() {

        return title;
    }

    public void setTitle(Title title) {

        this.title = title;
    }

    @XmlElement
    @ManyToOne
    public Citizenship getCitizenship() {

        return citizenship;
    }

    public void setCitizenship(Citizenship citizenship) {

        this.citizenship = citizenship;
    }

    @XmlTransient
    @OneToMany(mappedBy = "registrant")
    public Set<Registration> getRegistrations() {

        return registrations;
    }

    public void setRegistrations(Set<Registration> registrations) {

        this.registrations = registrations;
    }

    @XmlTransient
    @Transient
    public Set<Conditions> getConditions() {

        return conditions;
    }

    public void setConditions(Set<Conditions> conditions) {

        this.conditions = conditions;
    }

    public Boolean getDead() {

        if (dead == null) {
            return Boolean.FALSE;
        }
        return dead;
    }

    public void setDead(Boolean dead) {

        this.dead = dead;
    }

    @XmlElement
    @Transient
    public String getFullname() {

        return StringFormattingUtil.join(getFirstname(), getMiddlename(), getLastname());
    }

    @XmlElement
    @Transient
    public String getFormattedFullname() {


        return StringFormattingUtil.upperCaseFirst(CaseUtil.lowerCase(getFirstname())) + " " +
                StringFormattingUtil.upperCaseFirst(CaseUtil.lowerCase(getMiddlename())) + " " +
                StringFormattingUtil.upperCaseFirst(CaseUtil.lowerCase(getLastname()));
    }


    @XmlElement
    public String getFirstname() {

        return CaseUtil.upperCase(firstname);
    }

    public void setFirstname(String firstname) {

        this.firstname = firstname;
    }

    @XmlElement
    public String getMiddlename() {

        if (middlename == null) {
            return "";
        }
        return CaseUtil.upperCase(middlename);
    }

    public void setMiddlename(String middlename) {

        this.middlename = middlename;
    }

    @XmlElement
    public String getLastname() {

        return CaseUtil.upperCase(lastname);
    }

    public void setLastname(String lastname) {

        this.lastname = lastname;
    }

    @XmlElement
    @Transient
    public String getName() {

        return getLastname() + " " + getFirstname() + " " + getMiddlename();
    }

    @XmlElement
    public String getMaidenname() {

        if (maidenname == null) {
            return "";
        }
        return CaseUtil.upperCase(maidenname);
    }

    public void setMaidenname(String maidenname) {

        this.maidenname = maidenname;
    }

    @XmlTransient
    @Transient
    public List<RegistrantCpd> getRegistrantCpdList() {

        List<RegistrantCpd> registrantCpdList = new ArrayList<RegistrantCpd>();
        if (getRegistrantCpds() != null && !getRegistrantCpds().isEmpty()) {
            registrantCpdList.addAll(registrantCpds);
        }
        return registrantCpdList;
    }

    @XmlTransient
    @Transient
    public List<RegistrantDisciplinary> getRegistrantDisciplinaryList() {

        List<RegistrantDisciplinary> registrantDisciplinaryList = new ArrayList<RegistrantDisciplinary>();
        if (getRegistrantDisciplinary() != null && !getRegistrantDisciplinary().isEmpty()) {
            registrantDisciplinary.size();
            registrantDisciplinaryList.addAll(registrantDisciplinary);
        }
        return registrantDisciplinaryList;
    }

    @OneToMany(mappedBy = "registrant")
    public Set<RegistrantDisciplinary> getRegistrantDisciplinary() {

        return registrantDisciplinary;
    }

    public void setRegistrantDisciplinary(Set<RegistrantDisciplinary> registrantDisciplinary) {

        this.registrantDisciplinary = registrantDisciplinary;
    }

    @Transient
    public Employment getLatestEmployment() {

        Employment employment = null;
        for (Employment e : getEmploymentList()) {
            if (e.getEndDate() == null) {
                employment = e;
            }
        }
        return employment;
    }

    @XmlTransient
    @Transient
    public List<Employment> getEmploymentList() {

        List<Employment> employmentList = new ArrayList<Employment>();
        if (getEmployments() != null && !getEmployments().isEmpty()) {
            employmentList.addAll(employments);
        }
        return employmentList;
    }

    @XmlTransient
    @OneToMany(mappedBy = "registrant")
    public Set<Employment> getEmployments() {

        return employments;
    }

    public void setEmployments(Set<Employment> employments) {

        this.employments = employments;
    }

    @Transient //Defined for Zimbabwe with Retirement Age 65
    public String getStatusDueForRetirement() {

        if (getIntegerValueOfAge() > 65) {
            Integer years = getIntegerValueOfAge() - 65;
            return "Due for Retirement";
        } else {
            Integer years = 65 - getIntegerValueOfAge();
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.YEAR, years);
            return "Due for Retirement in " + years + " years. Year " + cal.get(Calendar.YEAR);
        }
    }

    @Transient
    public int getIntegerValueOfAge() {
        return Util.getIntegerValueOfAge(birthDate);
    }

    @XmlElement
    @Transient
    public String getActiveStatus() {

        return activeStatus;
    }

    public void setActiveStatus(String activeStatus) {

        this.activeStatus = activeStatus;
    }

    @XmlElement
    @Transient
    public String getActiveRegisters() {

        return activeRegisters;
    }

    public void setActiveRegisters(String activeRegisters) {

        this.activeRegisters = activeRegisters;
    }

    @XmlElement
    @Transient
    public String getDeadStatus() {

        if (dead == null) {
            return "";
        } else {
            return dead ? "Deceased" : "";
        }
    }

    @XmlElement
    @Transient
    public String getRetiredStatus() {

        if (getRetired() == null) {
            return "";
        } else {
            return getRetired() ? "Retired" : "";
        }
    }

    @XmlElement
    @Transient
    public String getVoidedStatus() {

        if (voided == null) {
            return "";
        } else {
            return voided ? "Voided Record" : "";
        }
    }

    @Transient
    public String getComment() {

        return comment;
    }

    public void setComment(String comment) {

        this.comment = comment;
    }

    @Transient
    public List<RegistrantCondition> getRegisterActiveConditions() {

        List<RegistrantCondition> registrantConditionList = new ArrayList<RegistrantCondition>();
        if (getRegistrantConditions() != null && !getRegistrantConditions().isEmpty()) {
            for (RegistrantCondition condition : getRegistrantConditions()) {
                if (condition.getCondition().getRegisterCondition() && condition.getStatus()) {
                    registrantConditionList.add(condition);
                }
            }
        }
        return registrantConditionList;
    }

    @OneToMany(mappedBy = "registrant")
    public Set<RegistrantCondition> getRegistrantConditions() {

        return registrantConditions;
    }

    public void setRegistrantConditions(Set<RegistrantCondition> registrantConditions) {

        this.registrantConditions = registrantConditions;
    }

    @Transient
    public List<RegistrantContact> getRegistrantContactList() {

        List<RegistrantContact> registrantContactslist = new ArrayList<RegistrantContact>();
        if (getRegistrantContacts() != null && !getRegistrantContacts().isEmpty()) {
            for (RegistrantContact r : getRegistrantContacts()) {
                if (!r.getRetired()) {
                    registrantContactslist.add(r);
                }
            }
        }
        return registrantContactslist;
    }

    @OneToMany(mappedBy = "registrant")
    public Set<RegistrantContact> getRegistrantContacts() {

        return registrantContacts;
    }

    public void setRegistrantContacts(Set<RegistrantContact> registrantContacts) {

        this.registrantContacts = registrantContacts;
    }

    @Transient
    public List<RegistrantAddress> getRegistrantAddressList() {

        List<RegistrantAddress> registrantAddresslist = new ArrayList<RegistrantAddress>();
        if (getRegistrantContacts() != null && !getRegistrantContacts().isEmpty()) {
            for (RegistrantAddress r : getRegistrantAddresses()) {
                if (!r.getRetired()) {
                    registrantAddresslist.add(r);
                }
            }
        }
        return registrantAddresslist;
    }

    @OneToMany(mappedBy = "registrant")
    public Set<RegistrantAddress> getRegistrantAddresses() {

        return registrantAddresses;
    }

    public void setRegistrantAddresses(Set<RegistrantAddress> registrantAddresses) {

        this.registrantAddresses = registrantAddresses;
    }

    @Transient
    public List<RegistrantCondition> getActiveConditions() {

        List<RegistrantCondition> registrantConditionList = new ArrayList<RegistrantCondition>();
        if (getRegistrantConditions() != null && !getRegistrantConditions().isEmpty()) {
            for (RegistrantCondition condition : getRegistrantConditions()) {
                if (condition.getStatus()) {
                    registrantConditionList.add(condition);
                }
            }
        }
        return registrantConditionList;
    }

    @Transient
    public Set<Course> getDisciplines() {

        Set<Course> courses = new HashSet<Course>();
        for (RegistrantQualification q : getRegistrantQualificationList()) {
            if (q.getCourse() != null) {
                courses.add(q.getCourse());
            }
        }
        return courses;
    }

    @XmlTransient
    @Transient
    public List<RegistrantQualification> getRegistrantQualificationList() {

        List<RegistrantQualification> registrantQualificationList = new ArrayList<RegistrantQualification>();
        if (getRegistrantQualifications() != null && !getRegistrantQualifications().isEmpty()) {
            for (RegistrantQualification qualification : getRegistrantQualifications()) {
                if (!qualification.getRetired()) {
                    registrantQualificationList.add(qualification);
                }
            }
        }
        return registrantQualificationList;
    }

    @XmlTransient
    @OneToMany(mappedBy = "registrant")
    public Set<RegistrantQualification> getRegistrantQualifications() {

        return registrantQualifications;
    }

    public void setRegistrantQualifications(Set<RegistrantQualification> registrantQualifications) {

        this.registrantQualifications = registrantQualifications;
    }

    public Boolean getVoided() {

        if (voided == null) {
            return Boolean.FALSE;
        }
        return voided;
    }

    public void setVoided(Boolean voided) {

        this.voided = voided;
    }

    @Transient
    public BigDecimal getAccountBalance() {

        if (getCustomerAccount() != null) {
            if (getCustomerAccount().getAccount() != null) {
                return getCustomerAccount().getAccount().getBalance();
            }
        }
        return BigDecimal.ZERO;
    }

    @OneToOne
    @Cascade(CascadeType.PERSIST)
    public Customer getCustomerAccount() {

        return customerAccount;
    }

    public void setCustomerAccount(Customer customerAccount) {

        this.customerAccount = customerAccount;
    }

    @Transient
    public String getFormattedIDNumber() {

        String value = getIdNumber();
        value = StringUtils.defaultString(getIdNumber());
        value = value.replaceAll("-", "").trim();
        value = value.replaceAll("/", "").trim();
        return value;

    }
}
