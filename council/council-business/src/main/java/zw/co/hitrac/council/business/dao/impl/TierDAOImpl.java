package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.TierDAO;
import zw.co.hitrac.council.business.dao.repo.TierRepository;
import zw.co.hitrac.council.business.domain.Tier;

/**
 *
 * @author Edward Zengeni
 */
@Repository
public class TierDAOImpl implements TierDAO{
   @Autowired
   private TierRepository tierRepository;
    
   public Tier save(Tier tier){
       return tierRepository.save(tier);
   }
   
   public List<Tier> findAll(){
       return tierRepository.findAll();
   }
   
   public Tier get(Long id){
       return tierRepository.findOne(id);
   }
 
   
   public void setTierRepository(TierRepository tierRepository) {
         this.tierRepository = tierRepository;
    }

    public List<Tier> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
