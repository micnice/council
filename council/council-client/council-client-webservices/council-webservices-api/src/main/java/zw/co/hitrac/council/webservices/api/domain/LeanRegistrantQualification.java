package zw.co.hitrac.council.webservices.api.domain;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Daniel Nkhoma
 */
@XmlRootElement
public class LeanRegistrantQualification {
    
    private Long registrantQualificationId;

    public LeanRegistrantQualification() {
    }

    public LeanRegistrantQualification(Long registrantQualificationId) {
        this.registrantQualificationId = registrantQualificationId;
    }

    public Long getRegistrantQualificationId() {
        return registrantQualificationId;
    }

    public void setRegistrantQualificationId(Long registrantQualificationId) {
        this.registrantQualificationId = registrantQualificationId;
    }
}
