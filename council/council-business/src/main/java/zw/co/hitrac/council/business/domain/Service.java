/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;

/**
 *
 * @author tidza
 */
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Table(name="service")
@Audited
public class Service extends BaseEntity implements Serializable {

}
