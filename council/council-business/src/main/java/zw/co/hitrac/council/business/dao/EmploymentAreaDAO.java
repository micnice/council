/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao;

import zw.co.hitrac.council.business.domain.EmploymentArea;

/**
 *
 * @author kelvin
 */
public interface EmploymentAreaDAO extends IGenericDAO<EmploymentArea> {
    
}
