
package zw.co.hitrac.council.web.pages.certification;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.GeneralParameters;
import zw.co.hitrac.council.business.domain.ProductIssuance;
import zw.co.hitrac.council.business.domain.ProductIssuanceType;
import zw.co.hitrac.council.business.domain.User;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.ProductIssuanceService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.accounts.ReceiptHeaderService;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.pages.TemplatePage;

import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;

/**
 * @author tdhlakama
 */
public class SearchCertificatePage extends TemplatePage {

    private String productNumber;
    private String searchText;
    @SpringBean
    private ReceiptHeaderService receiptHeaderService;
    @SpringBean
    private ProductIssuanceService productIssuanceService;
    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private GeneralParametersService generalParametersService;

    public SearchCertificatePage() {
        menuPanelManager.getViewReportsPanel().setTopMenuCurrent(true);
        PropertyModel<String> productNumberModel = new PropertyModel<String>(this, "productNumber");
        PropertyModel<String> searchTextModel = new PropertyModel<String>(this, "searchText");

        Form<?> form = new Form("form");
        form.add(new TextField<String>("searchText", searchTextModel));
        form.add(new TextField<String>("productNumber", productNumberModel));
        add(form);
        IModel<List<ProductIssuance>> model = new LoadableDetachableModel<List<ProductIssuance>>() {
            @Override
            protected List<ProductIssuance> load() {

                List<ProductIssuance> list = new ArrayList<>();

                if (productNumber == null && searchText == null) {
                    list = new ArrayList<>();
                } else {
                    list = productIssuanceService.getProductIssuanceList(searchText, productNumber);
                }

                if (generalParametersService.get().getAllowRenewalBeforeCPDs()) {

                    Boolean filtered = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.REGISTRY_CLERK)));
                    if (filtered) {
                        list = list.stream().filter(p -> p.getProductIssuanceType().equals(ProductIssuanceType.PRACTICING_CERTIFICATE) && !p.getIssued()).collect(Collectors.toList());
                    }
                }

                return list;


            }
        };

        add(new CertificateDataListPanel("certificateDataListPanel", model));

    }
}
