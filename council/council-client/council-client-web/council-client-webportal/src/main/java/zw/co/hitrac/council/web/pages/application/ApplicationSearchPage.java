package zw.co.hitrac.council.web.pages.application;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.service.ApplicationService;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

//~--- JDK imports ------------------------------------------------------------
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import zw.co.hitrac.council.business.domain.ApplicationPurpose;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.business.utils.StringFormattingUtil;
import zw.co.hitrac.council.reports.ApplicationListReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 * @author Michael Matiashe
 */
public class ApplicationSearchPage extends TemplatePage {

    private String searchtxt;
    @SpringBean
    private ApplicationService applicationService;
    private Date startDate, endDate;
    private String applicationStatus;
    private ApplicationPurpose applicationPurpose;
    @SpringBean
    private GeneralParametersService generalParametersService;

    // private ApplicationPurpose applicationPurpose;
    public ApplicationSearchPage() {
        menuPanelManager.getApplicationPanel().setTopMenuCurrent(true);
        menuPanelManager.getApplicationPanel().setApplicationSimpleSearch(true);

        PropertyModel<String> messageModel = new PropertyModel<String>(this, "searchtxt");
        PropertyModel<String> applicationStatusModel = new PropertyModel<String>(this, "applicationStatus");
        PropertyModel<Date> startDateModel = new PropertyModel<Date>(this, "startDate");
        PropertyModel<Date> endDateModel = new PropertyModel<Date>(this, "endDate");
        PropertyModel<ApplicationPurpose> applicationPurposeModel = new PropertyModel<ApplicationPurpose>(this, "applicationPurpose");
        Form<?> form = new Form("form");

        form.add(new TextField<String>("searchtxt", messageModel));

        form.add(new DropDownChoice("applicationStatus", applicationStatusModel, Application.getApplicationStates));
        form.add(new TextField<Date>("startDate", startDateModel).add(DatePickerUtil.getDatePicker()));
        form.add(new TextField<Date>("endDate", endDateModel).add(DatePickerUtil.getDatePicker()));

        List<ApplicationPurpose> applicationPurposes = new ArrayList<ApplicationPurpose>(Arrays.asList(ApplicationPurpose.values()));
        if (!generalParametersService.get().getRegistrationByApplication()) {
            applicationPurposes.remove(ApplicationPurpose.INTERN_REGISTRATION);
            applicationPurposes.remove(ApplicationPurpose.MAIN_REGISTRATION);
            applicationPurposes.remove(ApplicationPurpose.SPECIALIST_REGISTRATION);
            applicationPurposes.remove(ApplicationPurpose.STUDENT_REGISTRATION);
            applicationPurposes.remove(ApplicationPurpose.UNRESTRICTED_PRACTICING_CERTIFICATE);
        }
        form.add(new DropDownChoice("applicationPurpose", applicationPurposeModel, applicationPurposes));
        add(form);

        IModel<List<Application>> model = new LoadableDetachableModel<List<Application>>() {
            @Override
            protected List<Application> load() {
                if ((searchtxt == null) && (startDate == null) && (endDate == null) && (applicationStatus == null) && (applicationPurpose == null)) {
                    return new ArrayList<Application>();
                } else {
                    return applicationService.getApplications(searchtxt, applicationStatus, startDate, endDate, applicationPurpose, null, null);
                }
            }
        };

        // Registrant List Data View Panel
        add(new UnApprovedApplicationsDataViewPanel("unApprovedApplicationsDataListPanel", model));
        add(new FeedbackPanel("feedback"));

        form.add(new Button("report") {
            @Override
            public void onSubmit() {
                Map parameters = new HashMap();
                try {
                    if (startDate != null && endDate == null) {
                        endDate = new Date();
                    }

                    final String reportTitle = StringFormattingUtil.join(
                            applicationPurpose != null ? "Application Purpose -" : applicationPurpose.getName(),
                            applicationStatus != null ? "Application Status -" : applicationStatus,
                            startDate != null && endDate != null ? " From -" : DateUtil.getDate(startDate) + " to" + DateUtil.getDate(endDate)
                    );

                    parameters.put("comment", reportTitle);

                    ApplicationListReport applicationListReport = new ApplicationListReport();
                    ContentType contentType = ContentType.PDF;
                    if (!applicationPurpose.equals(ApplicationPurpose.INSTITUTION_REGISTRATION)) {
                        parameters.put("showField", true);
                    } else {
                        parameters.put("showField", false);
                    }
                    
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(applicationListReport, contentType, applicationService.getApplications(searchtxt, applicationStatus, startDate, endDate, applicationPurpose, null, null), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);
                    resource.respond(a);
                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

    }
}


//~ Formatted by Jindent --- http://www.jindent.com
