package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.CourseGroup;

/**
 *
 * @author Michael Matiashe
 */
public interface CourseGroupRepository extends CrudRepository<CourseGroup, Long> {
    public List<CourseGroup> findAll();
}
