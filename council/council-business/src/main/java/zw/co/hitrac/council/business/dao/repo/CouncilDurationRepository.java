package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.CouncilDuration;

/**
 *
 * @author Michael Matiashe
 */
public interface CouncilDurationRepository extends CrudRepository<CouncilDuration, Long> {
    public List<CouncilDuration> findAll();
}
