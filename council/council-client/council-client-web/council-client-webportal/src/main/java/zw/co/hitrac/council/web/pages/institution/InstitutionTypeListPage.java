package zw.co.hitrac.council.web.pages.institution;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.InstitutionType;
import zw.co.hitrac.council.business.service.InstitutionTypeService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;
import zw.co.hitrac.council.web.pages.configure.AdministerDatabasePage;
import zw.co.hitrac.council.web.pages.configure.IAdministerDatabaseBasePage;

/**
 *
 * @author Kelvin Goredema
 */
public class InstitutionTypeListPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private InstitutionTypeService institutionTypeService;

    public InstitutionTypeListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionTypeEditPage(null));
            }
        });

        IModel<List<InstitutionType>> model = new LoadableDetachableModel<List<InstitutionType>>() {
            @Override
            protected List<InstitutionType> load() {
                return institutionTypeService.findAll();
            }
        };
        PropertyListView<InstitutionType> eachItem = new PropertyListView<InstitutionType>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<InstitutionType> item) {
                Link<InstitutionType> viewLink = new Link<InstitutionType>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new InstitutionTypeViewPage(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };

        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
