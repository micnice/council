/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.repo.examinations;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.examinations.OldExamResult;

/**
 *
 * @author tidza
 */
public interface OldExamResultRepository extends CrudRepository<OldExamResult, Long> {
public List<OldExamResult> findAll();
}
