package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PageableListView;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigator;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;

import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

//~--- JDK imports ------------------------------------------------------------
import java.util.List;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.web.config.CouncilSession;
import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;

/**
 *
 * @author tdhlakama
 */
public class ExamCandidatesDataViewPanel extends Panel {

    public ExamCandidatesDataViewPanel(String id, IModel<List<ExamRegistration>> model) {
        super(id);

        PageableListView<ExamRegistration> eachItem = new PageableListView<ExamRegistration>("examRegistrationsList",
                model, 20) {
                    @Override
                    protected void populateItem(ListItem<ExamRegistration> item) {
                        Link<ExamRegistration> viewLink = new Link<ExamRegistration>("examResultPage", item.getModel()) {
                            @Override
                            public void onClick() {
                                setResponsePage(new RegistrantExamResultPage(getModelObject().getId()));
                            }
                        };
                        Link<ExamRegistration> editLink = new Link<ExamRegistration>("examRegistrationEditPage",
                                item.getModel()) {
                            @Override
                            public void onClick() {
                                setResponsePage(new ExamRegistrationEditPage(getModelObject().getId()));
                            }

                            @Override
                            protected void onConfigure() {
                                setVisibilityAllowed(CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.EDUCATION_OFFICER))));
                            }
                        };

                        if (item.getIndex() % 2 == 0) {
                            item.add(new EvenTableRowBehavior());
                        }

                        item.setModel(new CompoundPropertyModel<ExamRegistration>(item.getModel()));
                        item.add(viewLink);
                        item.add(editLink);
                        item.add(new Label("registration.course"));
                        item.add(new Label("examSetting"));
                        item.add(new Label("candidateNumber"));
                        item.add(new Label("institution"));
                        item.add(new Label("passTextStatus"));
                    }
                };

        add(new PagingNavigator("navigator", eachItem));
        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
