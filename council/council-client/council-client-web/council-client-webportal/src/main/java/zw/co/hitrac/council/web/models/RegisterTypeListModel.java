package zw.co.hitrac.council.web.models;

import java.util.List;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.RegisterType;
import zw.co.hitrac.council.business.service.RegisterTypeService;
import zw.co.hitrac.council.business.utils.HrisComparator;

/**
 *
 * @author charlesc
 */
public class RegisterTypeListModel extends LoadableDetachableModel<List<RegisterType>> {

    private final RegisterTypeService registerTypeService;
    private HrisComparator hrisComparator = new HrisComparator();

    public RegisterTypeListModel(RegisterTypeService registerTypeService) {
        this.registerTypeService = registerTypeService;
    }

    @Override
    protected List<RegisterType> load() {
        return (List<RegisterType>) hrisComparator.sort(registerTypeService.findAll());
    }
}
