package zw.co.hitrac.council.business.dao.accounts.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.security.sasl.SaslServer;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.accounts.ProductDAO;
import zw.co.hitrac.council.business.dao.repo.accounts.ProductRepository;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.business.domain.TypeOfService;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.AccountType;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.domain.accounts.ProductGroup;

/**
 *
 * @author Tatenda Chiwandire
 * @author Michael Matiashe
 * @author Charles Chigoriwa
 */
@Repository
public class ProductDAOImpl implements ProductDAO {

    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private ProductRepository productRepository;

    public Product save(Product t) {
        if (t.getSalesAccount() == null || t.getSalesAccount().getId() == null) {
            t.setSalesAccount(new Account());
            t.getSalesAccount().setBalance(BigDecimal.ZERO);
            t.getSalesAccount().setAccountType(AccountType.ACCOUNTS_RECEIVABLE);
            t.getSalesAccount().setCode(UUID.randomUUID().toString());
            t.getSalesAccount().setName(t.getName());
            t.getSalesAccount().setPersonalAccount(Boolean.FALSE);
        }
        return productRepository.save(t);
    }

    public List<Product> findAll() {
        return productRepository.findAll();
    }

    public Product get(Long id) {
        return productRepository.findOne(id);
    }

    public ProductRepository getProductRepository() {
        return productRepository;
    }

    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> findProducts(TypeOfService typeOfService) {
        return entityManager.createQuery("select p from Product p where p.typeOfService=:typeOfService and p.retired=:retired")
                .setParameter("typeOfService", typeOfService).setParameter("retired", Boolean.FALSE).getResultList();
    }

    public List<Product> findProducts(ProductGroup productGroup) {
        return entityManager.createQuery("select p from Product p where p.productGroup=:productGroup")
                .setParameter("productGroup", productGroup).getResultList();
    }

    public List<Product> findProducts(Product product) {
        StringBuilder sb = new StringBuilder("");
        sb.append("select p from Product p where ");
        if (product.getCourse() == null) {
            sb.append(" p.course IS NULL ");
        } else {
            sb.append(" p.course=:course ");
        }

        if (product.getTypeOfService() == null) {
            sb.append(" and p.typeOfService IS NULL ");
        } else {
            sb.append(" and p.typeOfService=:typeOfService");
        }

        if (product.getEmploymentType() == null) {
            sb.append(" and p.employmentType IS NULL ");
        } else {
            sb.append(" and p.employmentType=:employmentType");
        }

        if (product.getInstitutionCategory() == null) {
            sb.append(" and p.institutionCategory IS NULL ");
        } else {
            sb.append(" and p.institutionCategory=:institutionCategory");
        }

        if (product.getRegister() == null) {
            sb.append(" and p.register IS NULL ");
        } else {
            sb.append(" and p.register=:register");
        }

        Query query = entityManager.createQuery(sb.toString());
        if (product.getCourse() != null) {
            query.setParameter("course", product.getCourse());
        }

        if (product.getTypeOfService() != null) {
            query.setParameter("typeOfService", product.getTypeOfService());
        }

        if (product.getEmploymentType() != null) {
            query.setParameter("employmentType", product.getEmploymentType());
        }

        if (product.getInstitutionCategory() != null) {
            query.setParameter("institutionCategory", product.getInstitutionCategory());
        }

        if (product.getRegister() != null) {
            query.setParameter("register", product.getRegister());
        }

        return query.getResultList();

    }

    public List<Product> examProducts() {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Product.class);
        criteria.add(Restrictions.eq("examProduct", Boolean.TRUE));
        criteria.addOrder(Order.asc("name"));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();

    }

    public List<Product> findProducts(ProductGroup productGroup, TypeOfService typeOfService, Register register) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Product.class);
        criteria.add(Restrictions.eq("productGroup", productGroup));
        criteria.add(Restrictions.eq("typeOfService", typeOfService));
        criteria.add(Restrictions.eq("register", register));
        criteria.addOrder(Order.asc("name"));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<Product> findProducts(TypeOfService typeOfService, Register register, Course course) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Product.class);
        criteria.add(Restrictions.eq("course", course));
        criteria.add(Restrictions.eq("typeOfService", typeOfService));
        criteria.add(Restrictions.eq("register", register));
        criteria.addOrder(Order.asc("name"));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<Product> findNoneExamProducts() {
        Boolean examProduct = Boolean.FALSE;
        return entityManager.createQuery("SELECT p FROM Product p WHERE p.examProduct=:examProduct").setParameter("examProduct", examProduct).getResultList();
    }

    public List<Product> findAll(Boolean retired, Boolean directlyInvoicable) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Product.class);
        if (retired != null) {
            criteria.add(Restrictions.eq("retired", retired));
        }
        if (directlyInvoicable != null) {
            criteria.add(Restrictions.eq("directlyInvoicable", directlyInvoicable));
        }
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.addOrder(Order.asc("name"));
        return criteria.list();
    }

    public List<Product> findAll(Boolean retired) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Product.class);
        criteria.add(Restrictions.eq("retired", retired));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.addOrder(Order.asc("name"));
        return criteria.list();
    }

    @Override
    public Product getProductByCode(String productCode) {
        List<Product> products  = entityManager.createQuery("select p FROM Product p WHERE p.code=:code").setParameter("code", productCode).getResultList();
        if(!products.isEmpty()){
            return products.get(0);
        }        
        return null;
    }

    @Override
    public List<Product> findProductsWithoutPrices() {
        return entityManager.createQuery("SELECT p FROM Product p WHERE p.id NOT IN (SELECT a.product.id FROM ProductPrice a)").getResultList();
    }

    @Override
    public void createProductAccounts() {
        List<Product> products =new ArrayList<Product>();
        Session session =entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Product.class);
        criteria.add(Restrictions.isNull("salesAccount"));
        products = (List<Product>) criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
       for (Product product : products){
           save(product);
        }


    }

}
