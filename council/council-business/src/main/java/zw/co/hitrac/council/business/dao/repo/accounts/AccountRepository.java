
package zw.co.hitrac.council.business.dao.repo.accounts;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.accounts.Account;

/**
 *
 * @author Tatenda Chiwandire
 * @author Edward Zengeni
 * @author Judge Muzinda
 */
public interface AccountRepository extends CrudRepository<Account, Long> {

    public List<Account> findAll();
    
    public List<Account> findByPersonalAccount(Boolean personalAccount);
}
