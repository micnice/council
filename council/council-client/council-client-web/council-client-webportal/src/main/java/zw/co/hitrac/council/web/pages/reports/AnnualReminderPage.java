/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.reports;

import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.PageReference;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.AddressType;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.reports.ItemCase;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.process.ProductFinder;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.reports.DefaultReport;
import zw.co.hitrac.council.reports.RegistrantDataDBReport;
import zw.co.hitrac.council.reports.RegistrantDataDBReportGroupedByEmployer;
import zw.co.hitrac.council.reports.RegistrantDataDBReportNoEmployment;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

/**
 * @author tdhlakama
 */
public class AnnualReminderPage extends TemplatePage {

    private static final String SEPARATOR = ",";
    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private CourseService courseService;
    @SpringBean
    private CouncilDurationService councilDurationService;
    @SpringBean
    private RegistrantActivityService registrantActivityService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    private CouncilDuration previousCouncilDuration;
    private CouncilDuration currentCouncilDuration;
    @SpringBean
    private RegisterService registerService;
    @SpringBean
    private RegistrationService registrationService;
    @SpringBean
    private AddressTypeService addressTypeService;
    private Course course;
    private AddressType addressType;
    private HrisComparator hrisComparator = new HrisComparator();
    @SpringBean
    private ProductFinder productFinder;
    @SpringBean
    private DataSource dataSource;
    private ContentType contentType;

    public AnnualReminderPage(final PageReference pageReference) {

        menuPanelManager.getSearchPanel().setTopMenuCurrent(true);
        menuPanelManager.getSearchPanel().setSearchAdvanced(true);

        PropertyModel<CouncilDuration> previousCouncilDurationModel = new PropertyModel<CouncilDuration>(this, "previousCouncilDuration");
        PropertyModel<CouncilDuration> currentCouncilDurationModel = new PropertyModel<CouncilDuration>(this, "currentCouncilDuration");
        PropertyModel<Course> courseModel = new PropertyModel<Course>(this, "course");
        PropertyModel<AddressType> addressTypePropertyModel = new PropertyModel<AddressType>(this, "addressType");
        PropertyModel<ContentType> contentTypeModel = new PropertyModel<ContentType>(this, "contentType");
        Form<?> form = new Form("form");
        form.add(new DropDownChoice("previousCouncilDuration", previousCouncilDurationModel, councilDurationService.findAll()).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("currentCouncilDuration", currentCouncilDurationModel, councilDurationService.findAll()).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("course", courseModel, registrationService.getDistinctCourseFromRegistration()).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("addressType", addressTypePropertyModel, addressTypeService.findAll()));
        form.add(new DropDownChoice("contentType", contentTypeModel, Arrays.asList(ContentType.values())));
        add(form);

        form.add(new Link("returnLink") {
            public void onClick() {
                setResponsePage(pageReference.getPage());
            }
        });

        form.add(new Button("registrantDataDBExport") {

            @Override
            public void onSubmit() {

                final Boolean groupByEmployer = false;
                exportAnnualFeeReminder(groupByEmployer);
            }
        });

        form.add(new Button("registrantDataDBExportGroupByEmployer") {

            @Override
            public void onSubmit() {

                final Boolean groupByEmployer = true;
                exportAnnualFeeReminder(groupByEmployer);
            }
        });

        form.add(new Button("noEmploymentDataDBExport") {

            @Override
            public void onSubmit() {

                final Boolean groupByEmployer = null;
                exportAnnualFeeReminder(groupByEmployer);
            }
        });


//        form.add(new Button("registrantDataExport") {
//
//            @Override
//            public void onSubmit() {
//
//                Map param = new HashMap();
//                Product annualProduct = productFinder.searchAnnualProduct(course, generalParametersService.get().getMainRegister(), null, TypeOfService.ANNUAL_FEES);
//                Product practiceCertificateProduct = productFinder.searchAnnualProduct(course, generalParametersService.get().getMainRegister(), null, TypeOfService.PRACTISING_CERTIFICATE_FEES);
//                BigDecimal totalAmount = BigDecimal.ZERO;
//
//                if (annualProduct != null || practiceCertificateProduct != null) {
//                    totalAmount = annualProduct.getProductPrice().getPrice().add(practiceCertificateProduct.getProductPrice().getPrice());
//                    param.put("lineOne", "(a) To remain on the register for the period <b> January to December " + currentCouncilDuration.getName() + ":	    US $   " + annualProduct.getProductPrice().getPrice() + " </b>");
//                    param.put("lineTwo", "(b) Renewal of Practising Certificate for the period <b> January to December " + currentCouncilDuration.getName() + ": US $  " + practiceCertificateProduct.getProductPrice().getPrice() + " </b>");
//                    param.put("lineThree", "Total Amount Due								    US$   " + totalAmount);
//                    param.put("lineFour", "Your Practising Certificate authorising you to practise as a <br>" + course + "</br>  expires on <b> 31 December " + previousCouncilDuration.getName() + "</b>.  If you wish to continue to practise after this date, RETURN this form with the appropriate fees as soon as possible before <b> 31 January " + currentCouncilDuration.getName() + " </b>. We urge you to complete all the details on the form to enable Council to update its records.");
//                }
//
//                List<RegistrantData> registrantDataList
//                        = registrantService.getRegistrantsWithFirstRenewalWithoutSecondRenewalInRegistration(course, previousCouncilDuration, currentCouncilDuration, addressType);
//
//                registrantDataList = (List<RegistrantData>) HrisComparator.sortRegistrantData(registrantDataList);
//
//                try {
//                    AnnualReminderForm registrantDataReport = new AnnualReminderForm();
//
//                    ByteArrayResource resource
//                            = ReportResourceUtils.getReportResource(registrantDataReport, contentType, registrantDataList,
//                            param);
//                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
//                            RequestCycle.get().getResponse(), null);
//
//                    resource.respond(a);
//
//                    // To make Wicket stop processing form after sending response
//                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
//                } catch (JRException ex) {
//                    ex.printStackTrace(System.out);
//                }
//            }
//        });

        add(new FeedbackPanel("feedback"));
    }

    private void exportAnnualFeeReminder(Boolean groupByEmployer) {

        try {

            List<RegistrantData> registrantDataList = new ArrayList<RegistrantData>();
            ItemCase PCN = new ItemCase("PCN");
            ItemCase GN = new ItemCase("GN");
            ItemCase SCN = new ItemCase("SCN");
            ItemCase SCTN = new ItemCase("SCTN");
            ItemCase SCMN = new ItemCase("SCMN");
            if (course.getPrefixName() != null && (
                    course.getPrefixName().equals(PCN.getName()) ||
                            course.getPrefixName().equals(SCN.getName()) ||
                            course.getPrefixName().equals(SCTN.getName()) ||
                            course.getPrefixName().equals(SCMN.getName())
            )) {
                List registrants = registrantService.getRegistrantsWithFirstRenewalWithoutSecondRenewal(course, previousCouncilDuration, currentCouncilDuration, null);
                List registrantsWithGN = registrantService.getRegistrantsWithFirstRenewalWithoutSecondRenewal(courseService.findByPrefixName(GN.getName()), previousCouncilDuration, currentCouncilDuration, null);
                registrants.removeAll(registrantsWithGN);
                registrantDataList.addAll(registrants);
            } else {
                registrantDataList = registrantService.getRegistrantsWithFirstRenewalWithoutSecondRenewal(course, previousCouncilDuration, currentCouncilDuration, addressType);
            }

            DefaultReport registrantDataDBReport;

            String employer ="";
            if (groupByEmployer == null) {
                registrantDataDBReport = new RegistrantDataDBReportNoEmployment();
                employer ="No Employment Status. ";
            } else if (groupByEmployer) {
                registrantDataDBReport = new RegistrantDataDBReportGroupedByEmployer();
                employer ="Grouped By Employer. ";
            } else {
                registrantDataDBReport = new RegistrantDataDBReport();
            }

            Map parameters = new HashMap();

            parameters.put("comment", "Detailed Register for " + course + "S' who registered for " + previousCouncilDuration.getName()
                    + " but have not yet registered for " + currentCouncilDuration.getName() + ". " + employer);

            StringBuilder registrantIdList = new StringBuilder();

            if (registrantDataList != null && !registrantDataList.isEmpty()) {

                for (RegistrantData registrantData : registrantDataList) {
                    registrantIdList.append(registrantData.getId()).append(SEPARATOR);
                }

                parameters.put("list", registrantIdList.substring(0, registrantIdList.lastIndexOf(SEPARATOR)));
            }


            Connection connection = dataSource.getConnection(); //DBConnect.getConnection();

            ByteArrayResource resource = null;
            try {
                resource = ReportResourceUtils.getReportResource(registrantDataDBReport, contentType, connection, parameters);
            } finally {
                connection.close();
            }
            IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                    RequestCycle.get().getResponse(), null);

            resource.respond(a);

            // To make Wicket stop processing form after sending response
            RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
        } catch (JRException ex) {
            ex.printStackTrace(System.out);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }

    }

}

//~ Formatted by Jindent --- http://www.jindent.com
