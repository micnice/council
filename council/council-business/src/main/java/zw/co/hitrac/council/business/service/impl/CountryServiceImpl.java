package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.CountryDAO;
import zw.co.hitrac.council.business.domain.Country;
import zw.co.hitrac.council.business.service.CountryService;

import java.util.List;

/**
 *
 * @author Charles Chigoriwa
 */
@Service
@Transactional
public class CountryServiceImpl implements CountryService {

    @Autowired
    private CountryDAO countryDAO;
    

    @Transactional
    public Country save(Country country) {
        return countryDAO.save(country);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Country> findAll() {
        return countryDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Country get(Long id) {
        return countryDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Country> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setCountryDAO(CountryDAO countryDAO) {

        this.countryDAO = countryDAO;
    }

   
}
