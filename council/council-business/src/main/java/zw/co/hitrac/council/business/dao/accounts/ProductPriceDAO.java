
package zw.co.hitrac.council.business.dao.accounts;

import java.util.List;
import zw.co.hitrac.council.business.dao.IGenericDAO;
import zw.co.hitrac.council.business.domain.accounts.ProductPrice;

/**
 *
 * @author Tatenda Chiwandire
 * @author Michael Matiashe
 */
public interface ProductPriceDAO extends IGenericDAO<ProductPrice> {
    
}
