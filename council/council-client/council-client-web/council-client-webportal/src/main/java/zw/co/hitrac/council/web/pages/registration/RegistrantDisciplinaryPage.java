/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.registration;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantDisciplinary;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.CaseOutcomeService;
import zw.co.hitrac.council.business.service.MisconductTypeService;
import zw.co.hitrac.council.business.service.RegistrantDisciplinaryService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.CustomDateTextField;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

/**
 * @author hitrac
 */
public class RegistrantDisciplinaryPage extends TemplatePage {

    @SpringBean
    private RegistrantDisciplinaryService registrantDisciplinaryService;
    @SpringBean
    private MisconductTypeService misconductTypeService;
    @SpringBean
    private CaseOutcomeService caseOutcomeService;
    @SpringBean
    private RegistrationProcess registrationProcess;

    public RegistrantDisciplinaryPage(final Long id, final IModel<Registrant> registrantModel) {

        CompoundPropertyModel<RegistrantDisciplinary> model =
                new CompoundPropertyModel<RegistrantDisciplinary>(
                        new RegistrantDisciplinaryPage.LoadableDetachableRegistrantDisciplinaryModel(id,
                                registrantModel));

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        final Long registrantId = model.getObject().getRegistrant().getId();
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantId);

        setDefaultModel(model);

        Form<RegistrantDisciplinary> form = new Form<RegistrantDisciplinary>("form",
                (IModel<RegistrantDisciplinary>) getDefaultModel()) {
            @Override
            public void onSubmit() {

                if (getModel().getObject().getCaseOutcome() != null) {
                    if (getModel().getObject().getCaseOutcome().isDates()) {
                        if (getModel().getObject().getPenaltyStartDate() == null) {
                            throw new CouncilException("Penalty Start date is required");
                        }
                    }
                }
                registrationProcess.processDisciplinaryCase(getModelObject());
                setResponsePage(new RegistrantViewPage(getModelObject().getRegistrant().getId()));
            }
        };

        form.add(new CustomDateTextField("caseDate").setRequired(true).add(new ErrorBehavior()).add(DatePickerUtil.getDatePicker()));
        form.add(new CustomDateTextField("dateResolved").add(new ErrorBehavior()).add(DatePickerUtil.getDatePicker()));
        form.add(new DropDownChoice("caseOutcome", caseOutcomeService.findAll()));
        form.add(new DropDownChoice("misconductType", misconductTypeService.findAll()));
        form.add(new CustomDateTextField("penaltyStartDate").add(DatePickerUtil.getDatePicker()));
        form.add(new CustomDateTextField("penaltyEndDate").add(DatePickerUtil.getDatePicker()));
        form.add(new DropDownChoice("caseStatus", RegistrantDisciplinary.getCaseStatusList));

        form.add(new TextArea<String>("comment"));
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantId));
            }
        });
        add(form);
        add(new Label("registrant.fullname"));
        add(new FeedbackPanel("feedback"));
    }

    private final class LoadableDetachableRegistrantDisciplinaryModel
            extends LoadableDetachableModel<RegistrantDisciplinary> {

        private Long id;
        private IModel<Registrant> registrantIModel;

        public LoadableDetachableRegistrantDisciplinaryModel(Long id, IModel<Registrant> registrantIModel) {
            this.registrantIModel = registrantIModel;
            this.id = id;
        }

        @Override
        protected RegistrantDisciplinary load() {
            RegistrantDisciplinary registrantDisciplinary = null;

            if (id == null) {
                registrantDisciplinary = new RegistrantDisciplinary();
                final Registrant registrant = registrantIModel.getObject();
                registrantDisciplinary.setRegistrant(registrant);
            } else {
                registrantDisciplinary = registrantDisciplinaryService.get(id);
            }
            return registrantDisciplinary;
        }
    }
}



