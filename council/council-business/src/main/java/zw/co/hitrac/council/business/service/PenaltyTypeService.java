/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.PenaltyType;

/**
 *
 * @author kelvin
 */
public interface PenaltyTypeService extends IGenericService<PenaltyType>{
    
}
