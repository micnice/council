/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.City;
import zw.co.hitrac.council.business.domain.EmploymentArea;

/**
 *
 * @author kelvin
 */
public interface EmploymentAreaService  extends IGenericService<EmploymentArea>{
    
}
