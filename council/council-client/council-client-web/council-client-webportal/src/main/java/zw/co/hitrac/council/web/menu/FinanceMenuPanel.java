/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.menu;

import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.pages.accounts.payments.FinancePage;
import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;

/**
 *
 * @author jimmy
 */
public class FinanceMenuPanel extends MenuPanel{
    
    
    private Link<Void> financeLink;

    public FinanceMenuPanel(String id) {
        super(id);
        add(financeLink = new BookmarkablePageLink<Void>("financeLink", FinancePage.class) {
            @Override
            protected void onConfigure() {
                setVisibilityAllowed(CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.ACCOUNTS_OFFICER))));
            }
        });
             
}
     @Override
        protected void onConfigure() {
            addCurrentBehavior(financeLink, topMenuCurrent);
        }

    public Link<Void> getFinanceLink() {
        return financeLink;
    }

    public void setFinanceLink(Link<Void> financeLink) {
        this.financeLink = financeLink;
    }       
}
        
        

