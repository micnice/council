/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.RegistrantAddressDAO;
import zw.co.hitrac.council.business.domain.AddressType;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantAddress;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.RegistrantAddressService;

import java.util.List;

/**
 *
 * @author kelvin
 */
@Service
@Transactional
public class RegistrantAddressServiceImpl implements RegistrantAddressService {

    @Autowired
    private RegistrantAddressDAO registrantAddressDAO;

    @Transactional
    public RegistrantAddress save(RegistrantAddress registrantAddress) {
        return registrantAddressDAO.save(registrantAddress);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantAddress> findAll() {
        return registrantAddressDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantAddress get(Long id) {
        return registrantAddressDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantAddress> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setRegistrantAddressDAO(RegistrantAddressDAO registrantAddressDAO) {
        this.registrantAddressDAO = registrantAddressDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantAddress> getAddresses(Registrant registrant, Institution instituion) {
        return registrantAddressDAO.getAddresses(registrant, instituion);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantAddress getActiveAddress(Registrant registrant) {
        return registrantAddressDAO.getActiveAddress(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantAddress getActiveAddressbyAddressType(Registrant registrant, AddressType addressType) {
        return registrantAddressDAO.getActiveAddressbyAddressType(registrant, addressType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getRegistrantDataAddress(AddressType addressType) {
        return registrantAddressDAO.getRegistrantDataAddress(addressType);
    }
}
