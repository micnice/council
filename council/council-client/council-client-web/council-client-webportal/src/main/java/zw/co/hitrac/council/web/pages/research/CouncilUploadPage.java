package zw.co.hitrac.council.web.pages.research;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.jackrabbit.value.BinaryImpl;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.lang.Bytes;

import sun.net.www.MimeTable;

import zw.co.hitrac.council.business.service.GeneralParametersService;

import zw.co.hitrac.council.web.pages.TemplatePage;

//~--- JDK imports ------------------------------------------------------------

import java.util.Calendar;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.Repository;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;

/**
 *
 * @author Constance Mabaso
 */
public class CouncilUploadPage extends TemplatePage {


    @SpringBean
    private GeneralParametersService generalParametersService;


    public CouncilUploadPage() {

        // Create feedback panels
        final FeedbackPanel uploadFeedback = new FeedbackPanel("uploadFeedback");

        // Add uploadFeedback to the page itself
        add(uploadFeedback);

        final FileUploadForm simpleUploadForm = new FileUploadForm("simpleUpload");

        add(simpleUploadForm);
        add(new Image("image", new CouncilSiteImageResource(generalParametersService)));
    }

    private class FileUploadForm extends Form<Void> {
        FileUploadField fileUploadField;

        /**
         * Construct.
         *
         * @param name Component name
         */
        public FileUploadForm(String name) {
            super(name);

            // set this form to multipart mode (allways needed for uploads!)
            setMultiPart(true);

            // Add one file input field
            add(fileUploadField = new FileUploadField("fileInput"));

            // Set maximum size to 100K for demo purposes
            setMaxSize(Bytes.kilobytes(9000000));
        }

        /**
         * @see org.apache.wicket.markup.html.form.Form#onSubmit()
         */
        @Override
        protected void onSubmit() {
            final List<FileUpload> uploads = fileUploadField.getFileUploads();

            if (uploads != null) {
                for (FileUpload upload : uploads) {

                    // UploadPage.this.info("saved file: " + upload.getClientFileName());
                    try {

                        Repository repository = JcrUtils.getRepository(generalParametersService.get().getContent_Url());
                        Session session = repository.login(new SimpleCredentials("admin", "admin".toCharArray()));

                        String user = session.getUserID();
                        String name = repository.getDescriptor(Repository.REP_NAME_DESC);

                        try {
                            Node root = session.getRootNode();
                            String fileName = upload.getClientFileName();
                            MimeTable mt = MimeTable.getDefaultTable();
                            String mimeType = mt.getContentTypeFor(fileName);


                            if (mimeType == null || !mimeType.toLowerCase().startsWith("image/")) {
                                error("File selected cannot be uploaded because it is not an image");
                                return;

                            }

                            String profileImagePath = "content/council/logos/data/images/councilPicture/profileImage";

                            if (root.hasNode(profileImagePath)) {
                                root.getNode(profileImagePath).remove();
                                session.save();
                            }

                            Node contentNode;

                            if (root.hasNode("content")) {
                                contentNode = root.getNode("content");
                            } else {
                                contentNode = root.addNode("content");
                            }

                            Node councilNode;

                            if (contentNode.hasNode("council")) {
                                councilNode = contentNode.getNode("council");
                            } else {
                                councilNode = contentNode.addNode("council");
                            }

                            Node registrantsNode;

                            if (councilNode.hasNode("logos")) {
                                registrantsNode = councilNode.getNode("logos");
                            } else {
                                registrantsNode = councilNode.addNode("logos");
                            }

                            Node idNode;

                            if (registrantsNode.hasNode("data")) {
                                idNode = registrantsNode.getNode("data");
                            } else {
                                idNode = registrantsNode.addNode("data");
                            }

                            Node imagesNode;

                            if (idNode.hasNode("images")) {
                                imagesNode = idNode.getNode("images");
                            } else {
                                imagesNode = idNode.addNode("images");
                            }

                            Node profileImagesNode;

                            if (imagesNode.hasNode("councilPicture")) {
                                profileImagesNode = imagesNode.getNode("councilPicture");
                            } else {
                                profileImagesNode = imagesNode.addNode("councilPicture");
                            }

                            Node profileImageNode = profileImagesNode.addNode("profileImage", "nt:file");
                            Node profileImageContentNode = profileImageNode.addNode("jcr:content", "nt:resource");

                            BinaryImpl binaryImpl = new BinaryImpl(upload.getInputStream());
                            profileImageContentNode.setProperty("jcr:data", binaryImpl);
                            profileImageContentNode.setProperty("jcr:lastModified", Calendar.getInstance());
                            profileImageContentNode.setProperty("jcr:mimeType", mimeType);
                            session.save();
                            binaryImpl.dispose();
                            //setResponsePage(new RegistrationOptionPage(this.getModelObject().getId()));
                        } finally {
                            session.logout();
                            
                        }
                    } catch (Exception ex) {
                        System.err.println(ex.getMessage());
                    }
                }
            }
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
