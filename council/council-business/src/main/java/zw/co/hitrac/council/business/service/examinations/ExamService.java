/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.examinations;

import java.util.List;
import zw.co.hitrac.council.business.domain.ModulePaper;
import zw.co.hitrac.council.business.domain.examinations.Exam;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.service.IGenericService;

/**
 *
 * @author tidza
 */
public interface ExamService extends IGenericService<Exam> {
    
        public List<Exam> findAll(ExamSetting examSetting);
        
        public List<ModulePaper> findExamPapers(ExamSetting examSetting);
        
        public Exam getExam(ExamSetting examSetting, ModulePaper modulePaper);
}
