package zw.co.hitrac.council.business.service;

import java.util.List;
import zw.co.hitrac.council.business.domain.City;
import zw.co.hitrac.council.business.domain.Country;

/**
 *
 * @author Charles Chigoriwa
 */
public interface CountryService extends IGenericService<Country> {
    
    
}
