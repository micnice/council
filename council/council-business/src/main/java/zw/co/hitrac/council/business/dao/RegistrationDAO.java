package zw.co.hitrac.council.business.dao;

import java.util.Date;
import java.util.List;

import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.Citizenship;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Qualification;
import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantStatus;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;

/**
 * @author kelvin goredema
 * @author Michael Matiashe
 */
public interface RegistrationDAO extends IGenericDAO<Registration> {

    List<Registration> getRegistrations(Registrant registrant);

    Registration getRegistrationByApplication(Application application);

    Registration getCurrentRegistration(Institution institution);

    Registration getCurrentRegistration(Registrant registrant);

    Boolean getRegistrationIsDeRegistered(Registrant registrant, RegistrantStatus registrantStatus);

    List<Registration> getRegistrations(Course course, Register register, Institution institution);

    List<Registration> getRegistrationsAll(Course course, Register register, Registrant registrant);

    Registration getCurrentRegistrationByCourseAndRegister(Registrant registrant, Register register, Course course);

    List<Registrant> getRegistrants(Course course, Register register, Institution institution, String searchText, String gender, Citizenship citizenship, Date startDate, Date endDate);

    List<RegistrantData> getRegistrantList(Course course, Register register, Institution institution, RegistrantStatus registrantStatus, String searchText, String gender, Citizenship citizenship, Date startDate, Date endDate);

    List<Registration> getActiveRegistrations(Registrant registrant);

    Integer getActiveRegistrations(Course course, Register register);

    Integer getSuspendedRegistrations(Course course, Register register);

    Registration getCurrentRegistrationByRegister(Register register, Registrant registrant);

    List<Registration> getRegistrations(Course course, Register register, Institution institution, String searchText, String gender, Citizenship citizenship);

    Boolean hasCurrentRegistration(Registrant registrant);

    List<Registration> getActiveRegistrationByRegister(Course course, Register register);

    List<Course> getDistinctCourseFromRegistration();

    List<Registration> getActiveRegistrations(Course course);

    List<Registration> getRegisteredInstitutionsRegistrations(Boolean closed);

    List<Registration> getActiveRegistrations(String idList);

    boolean hasBeenRegisteredBeforeCouncilDuration(CouncilDuration councilDuration, Long registrantId);

    List<Long> getRegistrantIdsRegisteredBeforeCouncilDuration(CouncilDuration councilDuration);

}
