
package zw.co.hitrac.council.business.service.accounts;

import zw.co.hitrac.council.business.domain.accounts.Allocation;
import zw.co.hitrac.council.business.service.IGenericService;

/**
 *
 * @author Constance Mabaso
 */
public interface AllocationService extends IGenericService<Allocation> {
    
}
