package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.RegistrantTransfer;

/**
 *
 * @author Michael Matiashe
 */
public interface RegistrantTransferService extends IGenericService<RegistrantTransfer> {
    
}
