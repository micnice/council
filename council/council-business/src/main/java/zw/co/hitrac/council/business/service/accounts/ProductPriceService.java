/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.accounts;

import java.util.List;
import zw.co.hitrac.council.business.domain.accounts.ProductPrice;
import zw.co.hitrac.council.business.service.IGenericService;

/**
 *
 * @author tidza
 */
public interface ProductPriceService extends IGenericService<ProductPrice> {
    
}
