package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.CertificateReprintDAO;
import zw.co.hitrac.council.business.dao.repo.CertificateReprintRepository;
import zw.co.hitrac.council.business.domain.CertificateReprint;

/**
 *
 * @author Michael Matiashe
 */
@Repository
public class CertificateReprintDAOImpl implements CertificateReprintDAO{
   @Autowired
   private CertificateReprintRepository certificateReprintRepository;
    
   public CertificateReprint save(CertificateReprint certificateReprint){
       return certificateReprintRepository.save(certificateReprint);
   }
   
   public List<CertificateReprint> findAll(){
       return certificateReprintRepository.findAll();
   }
   
   public CertificateReprint get(Long id){
       return certificateReprintRepository.findOne(id);
   }
 /**
     * A setter method that will make mocking repo object easier
     * @param certificateReprintRepository 
     */
   
   public void setCertificateReprintRepository(CertificateReprintRepository certificateReprintRepository) {
         this.certificateReprintRepository = certificateReprintRepository;
    }

    public List<CertificateReprint> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
