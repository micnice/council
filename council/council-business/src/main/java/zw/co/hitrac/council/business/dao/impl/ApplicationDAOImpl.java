package zw.co.hitrac.council.business.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.*;

import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.ApplicationDAO;
import zw.co.hitrac.council.business.dao.repo.ApplicationRepository;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.ApplicationPurpose;
import zw.co.hitrac.council.business.domain.QualificationType;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.utils.DateUtil;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import zw.co.hitrac.council.business.service.accounts.AccountsParametersService;

/**
 * @author Michael Matiashe
 */
@Repository
public class ApplicationDAOImpl implements ApplicationDAO {

    @Autowired
    private ApplicationRepository applicationRepository;
    @Autowired
    private GeneralParametersService generalParametersService;
    @Autowired
    private AccountsParametersService accountsParametersService;

    @PersistenceContext
    EntityManager entityManager;

    public Application save(Application application) {
        return applicationRepository.save(application);
    }

    public List<Application> findAll() {
        return applicationRepository.findAll();
    }

    public Application get(Long id) {
        return applicationRepository.findOne(id);
    }

    /**
     * A setter method that will make mocking repo object easier
     *
     * @param
     */
    public void setApplicationRepository(ApplicationRepository applicationRepository) {
        this.applicationRepository = applicationRepository;
    }

    /**
     * @param query
     * @return
     */
    public List<Application> getApplications(String query, String applicationStatus, Date startDate, Date endDate, ApplicationPurpose applicationPurpose, Boolean used, Boolean expired) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Application.class);
        if (query != null) {
            criteria.createAlias("registrant", "r");
            criteria.add(Restrictions.or(Restrictions.or(Restrictions.like("r.firstname", query, MatchMode.START), Restrictions.or(Restrictions.like("r.middlename", query, MatchMode.START), Restrictions.like("r.lastname", query), Restrictions.like("r.idNumber", query)),
                    Restrictions.or(Restrictions.like("r.registrationNumber", query, MatchMode.START)))));
        }
        if (startDate != null && endDate != null) {
            criteria.add(Restrictions.between("applicationDate", startDate, endDate));
        } else if (startDate != null && endDate == null) {
            criteria.add(Restrictions.between("applicationDate", startDate, new Date()));
        }
        if (applicationStatus != null) {
            criteria.add(Restrictions.eq("applicationStatus", applicationStatus).ignoreCase());
        }
        if (applicationPurpose != null) {
            criteria.add(Restrictions.eq("applicationPurpose", applicationPurpose));
        }
        if (accountsParametersService.get().getRegistrationApplicationPayment()) {
            criteria.add(Restrictions.isNotNull("debtComponent"));
            criteria.createAlias("debtComponent", "d");
            criteria.add(Restrictions.eq("d.remainingBalance", BigDecimal.ZERO));
        }
        if (used != null) {
            criteria.add(Restrictions.eq("used", used));
        }
        if (expired != null) {
            criteria.add(Restrictions.eq("expired", expired));
        }

        criteria.addOrder(Order.desc("applicationDate"));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<Application> getApplicationsWithoutPayment() {
        return entityManager.createQuery("SELECT a FROM Application a where a.debtComponent.remainingBalance <> 0 and a.retired = false")
                .getResultList();
    }

    public Boolean hasPendingApplication(Registrant registrant, ApplicationPurpose applicationPurpose) {
        List<Application> list = entityManager.createQuery("SELECT a FROM Application a where a.registrant=:registrant and a.applicationPurpose=:applicationPurpose and a.approvedBy IS NOT NULL")
                .setParameter("registrant", registrant)
                .setParameter("applicationPurpose", applicationPurpose)
                .getResultList();
        return list.isEmpty() ? Boolean.FALSE : Boolean.TRUE;

    }

    public Integer getTotalApplicationsByStatus(String status) {
        Long count =0L;
        if (accountsParametersService.get().getRegistrationApplicationPayment()) {
            count = (long) entityManager.createQuery("SELECT count(a) FROM Application a WHERE a.debtComponent.remainingBalance=:remainingBalance AND a.applicationStatus=:applicationStatus")
                    .setParameter("remainingBalance", BigDecimal.ZERO)
                    .setParameter("applicationStatus", status).getSingleResult();
        } else {
           count =(Long) entityManager.createQuery("SELECT count(a) FROM Application a WHERE a.applicationStatus=:applicationStatus")
                   .setParameter("applicationStatus", status)
                   .getSingleResult();
        }

        return count.intValue();
    }

    public List<Application> getPaidPendingApplications() {
        if (accountsParametersService.get().getRegistrationApplicationPayment()) {
            return entityManager.createQuery("SELECT a FROM Application a WHERE a.debtComponent.remainingBalance=:remainingBalance AND a.applicationStatus=:applicationStatus").setParameter("remainingBalance", BigDecimal.ZERO).setParameter("applicationStatus", Application.APPLICATIONPENDING).getResultList();
        } else {
            return entityManager.createQuery("SELECT a FROM Application a WHERE a.applicationStatus=:applicationStatus").setParameter("applicationStatus", Application.APPLICATIONPENDING).getResultList();

        }
    }

    public List<Application> getRegistrantApplicationStatus(String applicationStatus) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Application.class);
        criteria.add(Restrictions.eq("applicationStatus", applicationStatus).ignoreCase());
        criteria.addOrder(Order.desc("applicationStatus"));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();

    }

    public List<Application> getExpiredApplications() {

        for (Application application : getApplications(null, null, null, null, null, Boolean.FALSE, Boolean.FALSE)) {
            if (((int) DateUtil.DifferenceInYears(application.getApplicationDate(), new Date()) >= 1)) {
                application.setExpired(Boolean.TRUE);
                applicationRepository.save(application);
            }
        }
        return getApplications(null, null, null, null, null, Boolean.FALSE, Boolean.TRUE);
    }

    public List<Application> findAll(Boolean retired) {
        return entityManager.createQuery("SELECT a from Application a WHERE a.retired=:retired").setParameter("retired", retired).getResultList();
    }

    private int getDateDifference(Date applicationDate) {
        LocalDate appDate = LocalDate.fromDateFields(applicationDate);
        LocalDate now = LocalDate.fromDateFields(new Date());
        return Days.daysBetween(appDate, now).getDays();
    }

    public Integer getNumberOfPendingApplications() {
        int count = 0;
        int difference = 0;
        for (Application application : getPaidPendingApplications()) {
            difference = getDateDifference(application.getApplicationDate());
            if (difference >= generalParametersService.get().getMinimumDaysPendingApplicationsCGS() && application.getApplicationPurpose().equals(ApplicationPurpose.CERTIFICATE_OF_GOOD_STANDING)) {
                count++;
            } else {
                if (application.getQualificationType() != null) {
                    if (difference >= generalParametersService.get().getMinimumDaysPendingApplicationsLocal() && application.getQualificationType().equals(QualificationType.LOCAL)) {
                        count++;
                    }

                    if (difference >= generalParametersService.get().getMinimumDaysPendingApplicationsForeign() && application.getQualificationType().equals(QualificationType.FOREIGN)) {
                        count++;
                    }
                }
            }
        }
        return count;
    }

    public List<Application> getFilteredPendingApplications() {
        int count = 0;
        int difference = 0;
        List<Application> applications = new ArrayList<Application>();
        for (Application application : getPaidPendingApplications()) {
            difference = getDateDifference(application.getApplicationDate());
            if (difference >= generalParametersService.get().getMinimumDaysPendingApplicationsCGS() && application.getApplicationPurpose().equals(ApplicationPurpose.CERTIFICATE_OF_GOOD_STANDING)) {
                applications.add(application);
            } else {
                if (application.getQualificationType() != null) {
                    if (difference >= generalParametersService.get().getMinimumDaysPendingApplicationsLocal() && application.getQualificationType().equals(QualificationType.LOCAL)) {
                        applications.add(application);
                    }

                    if (difference >= generalParametersService.get().getMinimumDaysPendingApplicationsForeign() && application.getQualificationType().equals(QualificationType.FOREIGN)) {
                        applications.add(application);
                    }
                }
            }
        }
        return applications;
    }

    public List<Application> getRegistrantApplicationStatus(String applicationStatus, ApplicationPurpose applicationPurpose) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Application.class);
        criteria.add(Restrictions.eq("applicationStatus", applicationStatus).ignoreCase());
        criteria.add(Restrictions.eq("applicationPurpose", applicationPurpose));
        if (accountsParametersService.get().getRegistrationApplicationPayment()) {
            criteria.add(Restrictions.isNotNull("debtComponent"));
            criteria.createAlias("debtComponent", "d");
            criteria.add(Restrictions.eq("d.remainingBalance", BigDecimal.ZERO));
        }
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();

    }

    @Override
    public List<Application> getApplicationsByYearApproved(int year) {

        Session session = entityManager.unwrap(Session.class);

        //Standard JPQL does not have a year function
        org.hibernate.Query query = session.createQuery("select a from Application a "
                + "where YEAR(a.applicationDate) = :yearApproved order by a.applicationPurpose, a.applicationStatus")
                .setParameter("yearApproved", year);

        return query.list();

    }
}
