
package zw.co.hitrac.council.business.service;

import java.util.List;
import java.util.Optional;

import zw.co.hitrac.council.business.domain.User;

/**
 *
 * @author Clive Gurure
 * @author Michael Matiashe
 */

public interface UserService extends IGenericService<User> {

    Optional<User> findByNameIgnoreCase(String userName);
    Optional<User> getCurrentUser();

    List<User> getAuthorisers ();
    List<User> getApplicationpProcessors();
    List<User> getCouncilProcessors();
    void encryptAllPasswords();
}
