package zw.co.hitrac.council.web.pages.configure;

import zw.co.hitrac.council.web.pages.accounts.*;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.PenaltyParameters;
import zw.co.hitrac.council.business.service.PenaltyParametersService;

/**
 *
 * @author Matiashe Michael
 */
public class PenaltyParametersViewPage extends IGeneralLedgerPage {

    @SpringBean
    private PenaltyParametersService penaltyParametersService;

    public PenaltyParametersViewPage() {
        CompoundPropertyModel<PenaltyParameters> model = new CompoundPropertyModel<PenaltyParameters>(new LoadableDetachablePenaltyParametersModel());
        setDefaultModel(model);
        add(new Link<PenaltyParameters>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new PenaltyParametersEditPage());
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Label("penaltyPeriod"));
        add(new Label("percentagePenalty"));
        add(new Label("charge"));
        add(new Label("reRegistrationPeriod"));
        

    }

    private final class LoadableDetachablePenaltyParametersModel extends LoadableDetachableModel<PenaltyParameters> {

        @Override
        protected PenaltyParameters load() {

            return penaltyParametersService.get();
        }
    }
}
