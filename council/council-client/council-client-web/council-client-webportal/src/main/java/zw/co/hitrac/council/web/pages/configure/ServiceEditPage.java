package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Service;
import zw.co.hitrac.council.business.service.ServiceService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Charles Chigoriwa
 */
public class ServiceEditPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private ServiceService serviceService;

    public ServiceEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<Service>(new LoadableDetachableServiceModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));

        Form<Service> form = new Form<Service>("form", (IModel<Service>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                serviceService.save(getModelObject());
                setResponsePage(new ServiceViewPage(getModelObject().getId()));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink", ServiceListPage.class));
        add(form);
    }

    private final class LoadableDetachableServiceModel extends LoadableDetachableModel<Service> {
        private Long id;

        public LoadableDetachableServiceModel(Long id) {
            this.id = id;
        }

        @Override
        protected Service load() {
            if (id == null) {
                return new Service();
            }

            return serviceService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
