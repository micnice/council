/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.repo;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.Institution;

/**
 *
 * @author tidza
 */
public interface InstitutionRepository extends CrudRepository<Institution, Long> {

    public List<Institution> findAll();
}
