package zw.co.hitrac.council.business.process;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.hitrac.council.business.domain.TypeOfService;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.service.accounts.ProductService;

import java.util.List;

/**
 * @author Charles Chigoriwa
 */
@Service
public class ProductGoogler {

    @Autowired
    private ProductService productService;


    public Product searchProduct(Product criteriaProduct) {
        Product productFound = null;
        List<Product> products = productService.findProducts(criteriaProduct.getTypeOfService());
        if (products.isEmpty()) {
            productFound = null;
        } else {
            productFound = searchProduct(criteriaProduct, products);
        }
        return productFound;
    }

    public Product searchByTypeOfService(TypeOfService typeOfService) {
        Product productFound = null;
        List<Product> products = productService.findProducts(typeOfService);
        if (products.isEmpty()) {
            productFound = null;
        } else {
            productFound = products.get(0);
        }
        return productFound;
    }

    private Product searchProduct(Product criteriaProduct, List<Product> products) {

        int currentPriority = 0;
        Product currentProduct = null;

        for (Product product : products) {
            int priority = 0;

            if (product.getCourse() != null && !product.getCourse().equals(criteriaProduct.getCourse())
                    || product.getCourseType() != null && !product.getCourseType().equals(criteriaProduct.getCourseType())
                    || product.getTier() != null && !product.getTier().equals(criteriaProduct.getTier())
                    || product.getEmploymentType() != null && !product.getEmploymentType().equals(criteriaProduct.getEmploymentType())
                    || product.getInstitutionCategory() != null && !product.getInstitutionCategory().equals(criteriaProduct.getInstitutionCategory())
                    || product.getQualificationType() != null && !product.getQualificationType().equals(criteriaProduct.getQualificationType())
                    || product.getRegister() != null && !product.getRegister().equals(criteriaProduct.getRegister())) {
                continue;
            }

            //Course
            if (product.getCourse() == null) {
                priority += 1;
            } else if (product.getCourse().equals(criteriaProduct.getCourse())) {
                priority += 2;
            }
            //CourseType
            if (product.getCourseType() == null) {
                priority += 1;
            } else if (product.getCourseType().equals(criteriaProduct.getCourseType())) {
                priority += 2;
            }

            //Tier
            if (product.getTier() == null) {
                priority += 1;
            } else if (product.getTier().equals(criteriaProduct.getTier())) {
                priority += 2;
            }

            //EmploymentType
            if (product.getEmploymentType() == null) {
                priority += 1;
            } else if (product.getEmploymentType().equals(criteriaProduct.getEmploymentType())) {
                priority += 2;
            }

            //InstitutionCategory
            if (product.getInstitutionCategory() == null) {
                priority += 1;
            } else if (product.getInstitutionCategory().equals(criteriaProduct.getInstitutionCategory())) {
                priority += 2;
            }

            //QualificationType
            if (product.getQualificationType() == null) {
                priority += 1;
            } else if (product.getQualificationType().equals(criteriaProduct.getQualificationType())) {
                priority += 2;
            }

            //Register
            if (product.getRegister() == null) {
                priority += 1;
            } else if (product.getRegister().equals(criteriaProduct.getRegister())) {
                priority += 2;
            }
            if (product.getTypeOfService() == null) {
                priority += 1;
            } else if (product.getTypeOfService().equals(criteriaProduct.getTypeOfService())) {
                priority += 2;
            }

            if (priority > currentPriority) {
                currentPriority = priority;
                currentProduct = product;
            }
        }
        return currentProduct;
    }
}
