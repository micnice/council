package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.MaritalStatusDAO;
import zw.co.hitrac.council.business.domain.MaritalStatus;
import zw.co.hitrac.council.business.service.MaritalStatusService;

import java.util.List;

/**
 *
 * @author Edaward Zengeni
 */
@Service
@Transactional
public class MaritalStatusServiceImpl implements MaritalStatusService{
    @Autowired
    private MaritalStatusDAO maritalStatusDAO;
    

    @Transactional
    public MaritalStatus save(MaritalStatus maritalStatus) {
       return maritalStatusDAO.save(maritalStatus);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<MaritalStatus> findAll() {
        return maritalStatusDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public MaritalStatus get(Long id) {
       return maritalStatusDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<MaritalStatus> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setMaritalStatusDAO(MaritalStatusDAO maritalStatusDAO) {

        this.maritalStatusDAO = maritalStatusDAO;
    }

	@Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public MaritalStatus findByName(String name) {
		return maritalStatusDAO.findByName(name);
	}
}
