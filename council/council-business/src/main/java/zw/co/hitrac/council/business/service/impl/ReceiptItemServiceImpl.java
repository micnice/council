package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.accounts.ReceiptItemDAO;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.domain.TypeOfService;
import zw.co.hitrac.council.business.domain.User;
import zw.co.hitrac.council.business.domain.accounts.*;
import zw.co.hitrac.council.business.service.accounts.ReceiptItemService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author Michael Matiashe
 */
@Service
@Transactional
public class ReceiptItemServiceImpl implements ReceiptItemService {

    @Autowired
    private ReceiptItemDAO receiptItemDAO;

    @Transactional
    public ReceiptItem save(ReceiptItem t) {
        return receiptItemDAO.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ReceiptItem> findAll() {
        return receiptItemDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public ReceiptItem get(Long id) {
        return receiptItemDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ReceiptItem> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setReceiptItemDao(ReceiptItemDAO receiptItemDAO) {
        this.receiptItemDAO = receiptItemDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ReceiptItem> getReceiptItems(Product product, Date startDate, Date endDate, TransactionType transactionType, User user) {
        return receiptItemDAO.getReceiptItems(product, startDate, endDate, transactionType, user);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ReceiptItem> getReceiptItemsDeposit(Product product, Date startDate, Date endDate, TransactionType transactionType, User user) {
        return receiptItemDAO.getReceiptItemsDeposit(product, startDate, endDate, transactionType, user);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ReceiptItem> get(DebtComponent debtComponent) {
        return receiptItemDAO.get(debtComponent);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ReceiptItem> getReceiptItemsWithNoReceiptHeader(Account account) {
        return receiptItemDAO.getReceiptItemsWithNoReceiptHeader(account);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Account> getReceiptItemsWithNoReceiptHeaders() {
        return receiptItemDAO.getReceiptItemsWithNoReceiptHeaders();
    }

    @Transactional
    public void correctReceiptItems() {
        receiptItemDAO.correctReceiptItems();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ReceiptItem> getIncomeGenerated(Date startDate, Date endDate) {
        return receiptItemDAO.getIncomeGenerated(startDate, endDate);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ReceiptItem> getReceiptItems(Product product, PaymentMethod paymentMethod, PaymentType paymentType, Date startDate, Date endDate, TransactionType transactionType, User user) {

        return receiptItemDAO.getReceiptItems(product, paymentMethod, paymentType, startDate, endDate, transactionType, user);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public BigDecimal getTotalPaidForDebtComponent(DebtComponent debtComponent) {

        return receiptItemDAO.getTotalPaidForDebtComponent(debtComponent);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public BigDecimal getReceiptItemsDepositTotal(Product product, Date startDate, Date endDate, TransactionType transactionType, User user) {
        return receiptItemDAO.getReceiptItemsDepositTotal(product, startDate, endDate, transactionType, user);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public BigDecimal getCalcualtedReceiptTotal(Product product, Date startDate, Date endDate, TransactionType transactionType, User user) {
        return receiptItemDAO.getCalcualtedReceiptTotal(product, startDate, endDate, transactionType, user);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public BigDecimal getCalcualtedReceiptTotal(Product product, PaymentMethod paymentMethod, PaymentType paymentType, Date startDate, Date endDate, TransactionType transactionType, User user) {
        return receiptItemDAO.getCalcualtedReceiptTotal(product, paymentMethod, paymentType, startDate, endDate, transactionType, user);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Product> getDistinctProductsInSales(Date startDate, Date endDate) {
        return receiptItemDAO.getDistinctProductsInSales(startDate, endDate);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Product> getDistinctProductsInSales() {
        return receiptItemDAO.getDistinctProductsInSales();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public BigDecimal getTotalPaidForDebtComponent(DebtComponent debtComponent, TransactionType transactionType) {
        return receiptItemDAO.getTotalPaidForDebtComponent(debtComponent, transactionType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public BigDecimal getReceiptItemsDepositTotal(Product product, Date startDate, Date endDate, Date depositStartDate, Date depositEndDate, TransactionType transactionType, User user) {
        return receiptItemDAO.getReceiptItemsDepositTotal(product, startDate, endDate, depositStartDate, depositEndDate, transactionType, user);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<CouncilDuration> getCouncilDurationsPaidForByAccountAndTypeOfService(Account account, TypeOfService typeOfService) {
        return receiptItemDAO.getCouncilDurationsPaidForByAccountAndTypeOfService(account, typeOfService);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public boolean hasPaidAnnualFeeForCouncilDuration(Account account, CouncilDuration duration) {
        return receiptItemDAO.hasPaidAnnualFeeForCouncilDuration(account, duration);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Long> ListOfPaidAnnualFeeForCouncilDuration(Account account, CouncilDuration duration) {
        return receiptItemDAO.ListOfPaidAnnualFeeForCouncilDuration(account, duration);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Customer> ListOfCustomersPaidAnnualFeeForCouncilDuration(CouncilDuration duration) {
        return receiptItemDAO.ListOfCustomersPaidAnnualFeeForCouncilDuration(duration);
    }

    public BigDecimal getTotalAmountPaidForByAccountAndTypeOfServiceAndCouncilDuration(Account account, TypeOfService typeOfService, CouncilDuration duration) {
        return receiptItemDAO.getTotalAmountPaidForByAccountAndTypeOfServiceAndCouncilDuration(account, typeOfService, duration);
    }

}
