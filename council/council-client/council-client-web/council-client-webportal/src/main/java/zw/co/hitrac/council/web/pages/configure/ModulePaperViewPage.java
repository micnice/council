package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.ModulePaper;
import zw.co.hitrac.council.business.service.ModulePaperService;

/**
 *
 * @author Matiashe Michael
 */
public class ModulePaperViewPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private ModulePaperService ModulePaperService;

    public ModulePaperViewPage(Long id) {
        CompoundPropertyModel<ModulePaper> model =
            new CompoundPropertyModel<ModulePaper>(new LoadableDetachableModulePaperModel(id));

        setDefaultModel(model);
        add(new Link<ModulePaper>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new ModulePaperEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", ModulePaperListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("course"));
        add(new Label("product.name"));
        add(new Label("modulePaper", model.bind("name")));
    }

    private final class LoadableDetachableModulePaperModel extends LoadableDetachableModel<ModulePaper> {
        private Long id;

        public LoadableDetachableModulePaperModel(Long id) {
            this.id = id;
        }

        @Override
        protected ModulePaper load() {
            return ModulePaperService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
