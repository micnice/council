
package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.Province;

/**
 *
 * @author Kelvin Goredema
 */
public interface ProvinceRepository extends CrudRepository<Province, Long>{
    
        public List<Province> findAll();

    
    
    
}
