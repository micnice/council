package zw.co.hitrac.council.web.models;

import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.business.service.RegisterService;
import zw.co.hitrac.council.business.utils.HrisComparator;

import java.util.List;

/**
 *
 * @author charlesc
 */
public class RegisterListModel extends LoadableDetachableModel<List<Register>> {

    private final RegisterService registerService;
    private HrisComparator hrisComparator = new HrisComparator();

    public RegisterListModel(RegisterService registerService) {
        this.registerService = registerService;

    }

    @Override
    protected List<Register> load() {

        List<Register> registers = registerService.findAll();

        return (List<Register>) hrisComparator.sort(registers);
    }
}
