package zw.co.hitrac.council.business.service.accounts.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.accounts.TransactionTypeDAO;
import zw.co.hitrac.council.business.domain.Book;
import zw.co.hitrac.council.business.domain.accounts.PaymentMethod;
import zw.co.hitrac.council.business.domain.accounts.TransactionType;
import zw.co.hitrac.council.business.service.accounts.TransactionTypeService;

import java.util.List;

/**
 *
 * @author Charles Chigoriwa
 * @author Michael Matiashe
 */
@Service
@Transactional
public class TransactionTypeServiceImpl implements TransactionTypeService {

    @Autowired
    private TransactionTypeDAO transactionTypeDAO;

    @Transactional
    public TransactionType save(TransactionType transactionType) {
        return transactionTypeDAO.save(transactionType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<TransactionType> findAll() {
        return transactionTypeDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public TransactionType get(Long id) {
        return transactionTypeDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<TransactionType> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setTransactionTypeDAO(TransactionTypeDAO transactionTypeDAO) {
        this.transactionTypeDAO = transactionTypeDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<TransactionType> find(Book book) {
        return this.transactionTypeDAO.find(book);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<TransactionType> find(Book book, PaymentMethod paymentMethod) {
        return transactionTypeDAO.find(book, paymentMethod);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<TransactionType> findBankAccounts() {

        return transactionTypeDAO.findBankAccounts();
    }
}
