package zw.co.hitrac.council.business.dao.accounts.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.accounts.PaymentDetailsDAO;
import zw.co.hitrac.council.business.dao.repo.accounts.PaymentDetailsRepository;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.accounts.PaymentDetails;

/**
 *
 * @author Tatenda Chiwandire
 * @author Michael Matiashe
 */
@Repository
public class PaymentDetailsDAOImpl implements PaymentDetailsDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private PaymentDetailsRepository paymentDetailsRepository;

    public PaymentDetails save(PaymentDetails t) {
        return paymentDetailsRepository.save(t);
    }

    public List<PaymentDetails> findAll() {
        return paymentDetailsRepository.findAll();
    }

    public PaymentDetails get(Long id) {
        return paymentDetailsRepository.findOne(id);
    }

    public PaymentDetailsRepository getPaymentDetailsRepository() {
        return paymentDetailsRepository;
    }

    public void setPaymentDetailsRepository(PaymentDetailsRepository paymentDetailsRepository) {
        this.paymentDetailsRepository = paymentDetailsRepository;
    }

    public List<PaymentDetails> transactionHistory(Customer customer) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(PaymentDetails.class);
        criteria.add(org.hibernate.criterion.Restrictions.eq("customer", customer));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<PaymentDetails> getUnprintedPaymentDetails(Customer customer) {
        return entityManager.createQuery("select p from PaymentDetails p where p.customer=:customer and p.receiptHeader.printed=:printed").setParameter("customer", customer).setParameter("printed", Boolean.FALSE).getResultList();
    }

    public List<PaymentDetails> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<PaymentDetails> findByUid(String uid) {
        return paymentDetailsRepository.findByUid(uid);
    }
}
