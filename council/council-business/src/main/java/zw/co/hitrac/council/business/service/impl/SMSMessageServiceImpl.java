package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.SMSMessageDAO;
import zw.co.hitrac.council.business.domain.SMSMessage;
import zw.co.hitrac.council.business.service.SMSMessageService;

import java.util.List;

/**
 *
 * @author Michael Matiashe
 */
@Service
@Transactional
public class SMSMessageServiceImpl implements SMSMessageService{
    
    @Autowired
    private SMSMessageDAO sMSMessageDAO;
    

    @Transactional
    public SMSMessage save(SMSMessage sMSMessage) {
       return sMSMessageDAO.save(sMSMessage);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<SMSMessage> findAll() {
        return sMSMessageDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public SMSMessage get(Long id) {
       return sMSMessageDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<SMSMessage> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setSMSMessageDAO(SMSMessageDAO sMSMessageDAO) {

        this.sMSMessageDAO = sMSMessageDAO;
    }

}
