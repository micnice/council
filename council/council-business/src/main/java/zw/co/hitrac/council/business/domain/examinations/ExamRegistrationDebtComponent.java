/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain.examinations;

import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.domain.BaseIdEntity;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

/**
 *
 * @author tidza
 */
@Entity
@Table(name="examRegistrationdebtcomponent")
@Audited
public class ExamRegistrationDebtComponent extends BaseIdEntity implements Serializable {

    private ExamRegistration examRegistration;
    private DebtComponent debtComponent;

    @ManyToOne
    public ExamRegistration getExamRegistration() {
        return examRegistration;
    }

    public void setExamRegistration(ExamRegistration examRegistration) {
        this.examRegistration = examRegistration;
    }

    @ManyToOne
    public DebtComponent getDebtComponent() {
        return debtComponent;
    }

    public void setDebtComponent(DebtComponent debtComponent) {
        this.debtComponent = debtComponent;
    }
}
