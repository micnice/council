package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.ModulePaper;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.process.ExamRegistrationProcess;
import zw.co.hitrac.council.business.process.SupExamRegistrationProcess;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationService;
import zw.co.hitrac.council.business.service.examinations.ExamSettingService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;
import zw.co.hitrac.council.web.models.CourseListModel;
import zw.co.hitrac.council.web.models.ExamSettingActiveListModel;
import zw.co.hitrac.council.web.models.TrainingInstitutionstModel;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.accounts.documents.DebtComponentsPage;
import zw.co.hitrac.council.web.pages.registration.RegistrantViewPage;
import zw.co.hitrac.council.web.utility.CustomDateTextField;

/**
 *
 * @author tdhlakama
 */
public class ExamSupplementaryPage extends TemplatePage {
    
    @SpringBean
    private ExamSettingService examSettingService;
    private ExamSetting examSetting;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private ExamRegistrationProcess examRegistrationProcess;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private CourseService courseService;
    @SpringBean
    private DebtComponentService debtComponentService;
    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private ExamRegistrationService examRegistrationService;
    @SpringBean
    private SupExamRegistrationProcess supExamRegistrationProcess;
    
    public ExamSupplementaryPage(final Registration registration) {
        CompoundPropertyModel<ExamRegistration> model =
                new CompoundPropertyModel<ExamRegistration>(new ExamRegistration());
        
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registration.getRegistrant().getId());
        model.getObject().setRegistration(registration);
        model.getObject().setSupplementaryExam(Boolean.TRUE);
        model.getObject().setInstitution(model.getObject().getRegistration().getInstitution());
        setDefaultModel(model);
        
        Form<ExamRegistration> form = new Form<ExamRegistration>("form", (IModel<ExamRegistration>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    getModel().getObject().setInstitution(getModel().getObject().getRegistration().getInstitution());
                    supExamRegistrationProcess.checkDuplicateRegistration(getModelObject());
                    supExamRegistrationProcess.register(getModelObject());
                    
                    Customer customer =
                            registrantService.get(registration.getRegistrant().getId()).getCustomerAccount();
                    List<DebtComponent> debtComponents = debtComponentService.getDebtComponents(customer);
                    
                    if (debtComponents.isEmpty()) {
                        setResponsePage(new RegistrantViewPage(registration.getRegistrant().getId()));
                    } else {
                        //setResponsePage(new PaymentPage(customer, debtComponents));
                        
                        //Select items to pay for 
                        setResponsePage(new DebtComponentsPage(registration.getRegistrant().getId()));
                    }
                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };
        
        if (registration.getRegister().equals(generalParametersService.get().getStudentRegister())) {
            form.add(new Label("numberOfRewrites", examRegistrationService.getNumberofRewrites(registration)));
        } else {
            form.add(new Label("numberOfRewrites", "Student not found in Student Register"));
        }
     
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registration.getRegistrant().getId()));
            }
        });
        form.add(new DropDownChoice("registration.course", new CourseListModel(courseService)));
        form.add(new CustomDateTextField("registration.registrationDate").setRequired(true).add(new ErrorBehavior()).add(DatePickerUtil.getDatePicker()));
        form.add(new DropDownChoice("registration.institution", new TrainingInstitutionstModel(institutionService)));
        form.add(new DropDownChoice("examSetting",new ExamSettingActiveListModel(examSettingService)).setRequired(true).add(new ErrorBehavior()));
        add(form);
        add(new Label("registration.registrant.fullname"));
        add(new FeedbackPanel("feedback"));
        
        final IModel<List<ModulePaper>> modelExams = new LoadableDetachableModel<List<ModulePaper>>() {
            @Override
            protected List<ModulePaper> load() {
                return supExamRegistrationProcess.getFailedModulePapers(registration);
            }
        };
        
        add(new ExamPaperPanelList("examList", modelExams));
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
