package zw.co.hitrac.council.web.pages.reports;

import net.sf.jasperreports.engine.JRException;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.business.utils.StringFormattingUtil;
import zw.co.hitrac.council.reports.RegistrantDataDBReport;
import zw.co.hitrac.council.reports.RegistrantDataReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.CourseListModel;
import zw.co.hitrac.council.web.models.RegisterListModel;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author tdhlakama
 */
public class RegistrantsByCourseAndDurationPage extends TemplatePage {

    @SpringBean
    private CouncilDurationService councilDurationService;
    @SpringBean
    private RegistrantActivityService registrantActivityService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private ConditionService conditionService;
    private CouncilDuration councilDuration;
    private Course course;
    private Register register;
    @SpringBean
    private RegisterService registerService;
    @SpringBean
    private CourseService courseService;
    @SpringBean
    private DataSource dataSource;
    @SpringBean
    private ProvinceService provinceService;

    private EmploymentType employmentType;
    private String status;
    private Boolean detail = Boolean.FALSE;
    private ContentType contentType;
    private Conditions condition;
    private Integer lowerLimit;
    private Integer upperLimit;
    private String gender;
    private Province province;

    public RegistrantsByCourseAndDurationPage() {

        menuPanelManager.getSearchPanel().setTopMenuCurrent(true);
        menuPanelManager.getSearchPanel().setSearchAdvanced(true);

        Register institutionRegister = generalParametersService.get().getInstitutionRegister();

        PropertyModel<ContentType> contentTypeModel = new PropertyModel<ContentType>(this, "contentType");
        PropertyModel<CouncilDuration> councilDurationModel = new PropertyModel<CouncilDuration>(this, "councilDuration");
        PropertyModel<Course> courseModel = new PropertyModel<Course>(this, "course");
        PropertyModel<EmploymentType> employmentTypeModel = new PropertyModel<EmploymentType>(this, "employmentType");
        PropertyModel<Register> registerModel = new PropertyModel<Register>(this, "register");
        PropertyModel<String> statusModel = new PropertyModel<String>(this, "status");
        PropertyModel<Boolean> detailModel = new PropertyModel<Boolean>(this, "detail");
        PropertyModel<Conditions> conditionModel = new PropertyModel<Conditions>(this, "condition");
        PropertyModel<Integer> lowerLimitModel = new PropertyModel<Integer>(this, "lowerLimit");
        PropertyModel<Integer> upperLimitModel = new PropertyModel<Integer>(this, "upperLimit");
        PropertyModel<String> genderModel = new PropertyModel<String>(this, "gender");
        PropertyModel<Province> provinceModel = new PropertyModel<Province>(this, "province");

        Form<?> form = new Form("form");
        form.add(new DropDownChoice("province", provinceModel, provinceService.findAll()));
        form.add(new DropDownChoice("councilDuration", councilDurationModel, councilDurationService.findAll()).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("course", courseModel, new CourseListModel(courseService)).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("register", registerModel, new RegisterListModel(registerService)));
        form.add(new DropDownChoice("condition", conditionModel, conditionService.findAll()));
        form.add(new CheckBox("detail", detailModel));
        form.add(new DropDownChoice("employmentType", employmentTypeModel, Arrays.asList(EmploymentType.values())));
        List statuslist = Arrays.asList(new String[]{"Active", "InActive"});
        form.add(new DropDownChoice("status", statusModel, statuslist).setRequired(true).add(new ErrorBehavior()));
        List genderlist = Arrays.asList(new String[]{"M", "F"});
        form.add(new DropDownChoice("gender", genderModel, genderlist));
        form.add(new TextField<Integer>("lowerLimit", lowerLimitModel));
        form.add(new TextField<Integer>("upperLimit", upperLimitModel));
        form.add(new DropDownChoice("contentType", contentTypeModel, Arrays.asList(ContentType.values())));
        add(form);

        form.add(new Button("process") {

            @Override
            public void onSubmit() {

                register = registerModel.getObject();

                if (StringUtils.isEmpty(status)) {
                    error("Select Status of Report - Active or In Active");
                    return;
                }

                boolean isActive = status.contentEquals("Active");

                if (employmentType != null) {
                    printEmploymentReport(employmentType, isActive, register);
                } else {

                    if (detail) {
                        printDetailReport(isActive, register);

                    } else {

                        printReport(isActive, register);
                    }
                }

            }
        });
        add(new FeedbackPanel("feedback"));
    }

    private void printReport(Boolean active, Register register) {

        Set<RegistrantData> registrants = new HashSet<RegistrantData>();
        Map parameters = new HashMap();

        String conditionSelected = condition != null ? "With Condition : " + condition : "";
        String genderSelected = gender != null ? " Gender : " + gender : "";
        String ageSelected = lowerLimit != null && upperLimit != null ? " Age : Bewteen " + lowerLimit + " -  " + upperLimit : "";
        String provinceSelected = province != null ? " Province : " + province.getName() : "";
        final String reportTitle = StringFormattingUtil.join(course.getName(),
                register == null ? "Detailed Register." : register.getName(),
                genderSelected, ageSelected, provinceSelected, status, " - ", councilDuration.getName(), " Period. " + conditionSelected);

        parameters.put("comment", reportTitle);


        Set<Register> registers = new HashSet<Register>();

        if (register == null) {
            registers.addAll(registerService.findAll());
            registers.remove(generalParametersService.get().getStudentRegister());

        } else {
            registers.add(register);
        }

        List<Duration> durations = new ArrayList<Duration>(councilDurationService.get(councilDuration.getId()).getDurations());

        for (Register r : registers) {
            registrants.addAll(registrantActivityService.getListPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(course, r, durations, RegistrantActivityType.RENEWAL, active, employmentType, condition));
        }

        if (gender != null) {
            registrants = registrants.stream().filter(r -> r.getGender().equals(gender)).collect(Collectors.toSet());
        }

        if (lowerLimit != null && upperLimit != null) {
            registrants = registrants.stream().filter(r -> r.getIntegerValueOfAge() >= lowerLimit && r.getIntegerValueOfAge() <= upperLimit).collect(Collectors.toSet());
        }

        if (province != null) {
            registrants = registrants.stream().filter(r -> r.getProvinceName() != null && r.getProvinceName().equalsIgnoreCase(province.getName())).collect(Collectors.toSet());
        }

        List<RegistrantData> registrantDataList = new ArrayList<RegistrantData>(registrants);
        registrantDataList = (List<RegistrantData>) HrisComparator.sortRegistrantData(registrantDataList);

        try {

            RegistrantDataReport registrantDataReport = new RegistrantDataReport();
            ByteArrayResource resource
                    = ReportResourceUtils.getReportResource(registrantDataReport, contentType, registrantDataList,
                    parameters);
            IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                    RequestCycle.get().getResponse(), null);

            resource.respond(a);

            // To make Wicket stop processing form after sending response
            RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
        } catch (JRException ex) {
            ex.printStackTrace(System.out);
        }
    }

    private void printDetailReport(Boolean active, Register register) {

        Map parameters = new HashMap();
        RegistrantDataDBReport registrantDataDBReport = new RegistrantDataDBReport();
        try {

            Set<RegistrantData> registrants = new HashSet<RegistrantData>();
            Set<Register> registers = new HashSet<Register>();

            if (register == null) {
                registers.addAll(registerService.findAll());
                registers.remove(generalParametersService.get().getStudentRegister());

            } else {
                registers.add(register);
            }

            List<Duration> durations = new ArrayList<Duration>(councilDurationService.get(councilDuration.getId()).getDurations());

            for (Register r : registers) {

                registrants.addAll(registrantActivityService.getListPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(course, r, durations, RegistrantActivityType.RENEWAL, active, employmentType, condition));

            }

            if (gender != null) {
                registrants = registrants.stream().filter(r -> r.getGender().equals(gender)).collect(Collectors.toSet());
            }

            if (lowerLimit != null && upperLimit != null) {
                registrants = registrants.stream().filter(r -> r.getIntegerValueOfAge() >= lowerLimit && r.getIntegerValueOfAge() <= upperLimit).collect(Collectors.toSet());
            }

            if (province != null) {
                registrants = registrants.stream().filter(r -> r.getProvinceName() != null && r.getProvinceName().equalsIgnoreCase(province.getName())).collect(Collectors.toSet());
            }

            String idList = ReportResourceUtils.getRegistrantIdListFromRegistrantList(registrants);

            String conditionSelected = condition != null ? "With Condition : " + condition : "";
            String genderSelected = gender != null ? " Gender : " + gender : "";
            String ageSelected = lowerLimit != null && upperLimit != null ? " Age : Bewteen " + lowerLimit + " -  " + upperLimit : "";
            String provinceSelected = province != null ? " Province : " + province.getName() : "";
            final String reportTitle = StringFormattingUtil.join(course.getName(),
                    register == null ? "Detailed Register." : register.getName(),
                    genderSelected, ageSelected, provinceSelected,
                    status, " - ", councilDuration.getName(), " Period. " + conditionSelected);

            parameters.put("comment", reportTitle);
            parameters.put("list", idList);


            final Connection connection = dataSource.getConnection(); //DBConnect.getConnection();
            ByteArrayResource resource = null;
            try {
                resource = ReportResourceUtils.getReportResource(registrantDataDBReport, contentType, connection, parameters);
            } finally {
                connection.close();
            }
            IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                    RequestCycle.get().getResponse(), null);

            resource.respond(a);

            // To make Wicket stop processing form after sending response
            RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
        } catch (JRException ex) {
            ex.printStackTrace(System.out);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }

    }

    public void printEmploymentReport(EmploymentType employmentType, Boolean active, Register register) {

        Map parameters = new HashMap();
        RegistrantDataDBReport registrantDataDBReport = new RegistrantDataDBReport();
        try {

            Set<RegistrantData> registrants = new HashSet<RegistrantData>();
            Set<Register> registers = new HashSet<Register>();

            if (register == null) {
                registers.addAll(registerService.findAll());
                registers.remove(generalParametersService.get().getStudentRegister());

            } else {
                registers.add(register);
            }

            List<Duration> durations = new ArrayList<Duration>(councilDurationService.get(councilDuration.getId()).getDurations());

            for (Register r : registers) {

                registrants.addAll(registrantActivityService.getListPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(course, r, durations, RegistrantActivityType.RENEWAL, active, employmentType, condition));

            }

            if (gender != null) {
                registrants = registrants.stream().filter(r -> r.getGender().equals(gender)).collect(Collectors.toSet());
            }

            if (lowerLimit != null && upperLimit != null) {
                registrants = registrants.stream().filter(r -> r.getIntegerValueOfAge() >= lowerLimit && r.getIntegerValueOfAge() <= upperLimit).collect(Collectors.toSet());
            }

            if (province != null) {
                registrants = registrants.stream().filter(r -> r.getProvinceName() != null && r.getProvinceName().equalsIgnoreCase(province.getName())).collect(Collectors.toSet());
            }

            String idList = ReportResourceUtils.getRegistrantIdListFromRegistrantList(registrants);
            parameters.put("list", idList);

            String conditionSelected = condition != null ? "With Condition : " + condition : "";
            String genderSelected = gender != null ? " Gender : " + gender : "";
            String ageSelected = lowerLimit != null && upperLimit != null ? " Age : Bewteen " + lowerLimit + " -  " + upperLimit : "";
            String provinceSelected = province != null ? " Province : " + province.getName() : "";
            final String reportTitle = StringFormattingUtil.join(course.getName(),
                    register == null ? "Detailed Register." : register.getName(),
                    genderSelected, ageSelected, provinceSelected,
                    status, " - ", councilDuration.getName(), " Period. " + conditionSelected);

            parameters.put("comment", reportTitle);

            final Connection connection = dataSource.getConnection(); //DBConnect.getConnection();
            ByteArrayResource resource = null;
            try {
                resource = ReportResourceUtils.getReportResource(registrantDataDBReport, contentType, connection, parameters);
            } finally {
                connection.close();
            }
            IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                    RequestCycle.get().getResponse(), null);

            resource.respond(a);

            // To make Wicket stop processing form after sending response
            RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
        } catch (JRException ex) {
            ex.printStackTrace(System.out);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }

    }

}
