/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.AddressType;
import zw.co.hitrac.council.business.domain.SupportDocument;

/**
 *
 * @author kelvin
 */
public interface SupportDocumentRepository extends CrudRepository<SupportDocument,Long>{
        public List<SupportDocument> findAll();
}
