package zw.co.hitrac.council.web.pages.configure;

import com.googlecode.wicket.jquery.ui.form.CheckChoice;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Conditions;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.domain.GeneralParameters;
import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.RequirementListModel;

/**
 * @author Edward Zengeni
 * @author Michael Matiashe
 */
public class GeneralParametersEditPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private RegisterService registerService;
    @SpringBean
    private CouncilDurationService durationService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private InstitutionTypeService institutionTypeService;
    @SpringBean
    private RegistrantStatusService registrantStatusService;
    @SpringBean
    private ContactTypeService contactTypeService;
    @SpringBean
    private RequirementService requirementService;
    @SpringBean
    private AddressTypeService addressTypeService;
    @SpringBean
    private ConditionService conditionService;

    public GeneralParametersEditPage() {
        setDefaultModel(new CompoundPropertyModel<GeneralParameters>(new LoadableDetachableGeneralParametersModel()));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        Form<GeneralParameters> form = new Form<GeneralParameters>("form", (IModel<GeneralParameters>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                generalParametersService.save(getModelObject());
                setResponsePage(new AdministerDatabasePage());
            }
        };
        form.add(new TextField<Integer>("unrestrictedPCPeriod"));
        form.add(new TextField<String>("email").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("emailPassword").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextArea<String>("emailSignature").setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("emailContactType", contactTypeService.findAll()));
        form.add(new CheckBox("allowSendingBirthDayRemindersToRegistrants"));
        form.add(new TextField<String>("content_Url").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("content_UserName").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("content_Password").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("councilName").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("councilPrefixForRegistrationNumber").add(new ErrorBehavior()));
        form.add(new TextField<String>("minimumPeriodForSup").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("registrantName").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<Integer>("minimumYearsForInstitutionApplication").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<Integer>("minimumRegistrationAge").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<Integer>("minimumPeriodToStartPBQ").setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("mainRegister", registerService.findAll(), new ChoiceRenderer<Register>("name", "id")));
        form.add(new DropDownChoice("studentRegister", registerService.findAll(), new ChoiceRenderer<Register>("name", "id")));
        form.add(new DropDownChoice("provisionalRegister", registerService.findAll(), new ChoiceRenderer<Register>("name", "id")));
        form.add(new DropDownChoice("specialistRegister", registerService.findAll(), new ChoiceRenderer<Register>("name", "id")));
        form.add(new DropDownChoice("internRegister", registerService.findAll(), new ChoiceRenderer<Register>("name", "id")));
        form.add(new DropDownChoice("institutionRegister", registerService.findAll(), new ChoiceRenderer<Register>("name", "id")));
        form.add(new DropDownChoice("maintenanceRegister", registerService.findAll(), new ChoiceRenderer<Register>("name", "id")));
        form.add(new DropDownChoice("councilDuration", durationService.findAll(), new ChoiceRenderer<Duration>("name", "id")));
        form.add(new DropDownChoice("institution", institutionService.findAll(), new ChoiceRenderer<Duration>("name", "id")));
        form.add(new DropDownChoice("mainRegisterCondition", conditionService.findAll(), new ChoiceRenderer<Conditions>("name", "id")));
        form.add(new TextField<Integer>("lastRegistrationNumber").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<Integer>("lastCandidateNumber").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<Integer>("lastCGSCertificateNumber").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<Integer>("lastCertificateNumber").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<Integer>("lastPracticeCertificateNumber").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<Integer>("minimumYearsForCGSApplication").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<Integer>("incrementCertificateNumber").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<Integer>("numberOfTimesToWriteSupExam").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("councilAbbreviation").setRequired(true).add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        form.add(new CheckBox("examinationModule"));
        form.add(new CheckBox("registrationByApplication"));
        form.add(new CheckBox("studentRegistrationWithRenewal"));
        form.add(new CheckBox("allowMultipleRegistrations"));
        form.add(new TextField<Integer>("minimumYearsInInternRegister"));
        form.add(new TextField<Integer>("minimumYearsInProvisionalRegister"));
        form.add(new DropDownChoice("institutionType", institutionTypeService.findAll(), new ChoiceRenderer<Duration>("name", "id")));
        form.add(new CheckBox("hasReminderOnApplications"));
        form.add(new TextField<Integer>("minimumDaysPendingApplicationsLocal"));
        form.add(new TextField<Integer>("minimumDaysPendingApplicationsForeign"));
        form.add(new TextField<Integer>("minimumDaysPendingApplicationsCGS"));
        form.add(new CheckBox("hasProcessedByBoard"));
        form.add(new CheckBox("registrationNumberHasPrefix"));
        form.add(new CheckBox("registrationNumberHasZeroPrefix"));
        form.add(new CheckBox("generateRegnumberOnRegistrant"));
        form.add(new DropDownChoice("healthInstitutionType", institutionTypeService.findAll()));
        form.add(new CheckBox("approveAndRegister"));
        form.add(new TextField("registrationNumberIncrement"));
        form.add(new CheckBox("allowPreRegistrantion"));
        form.add(new CheckBox("registrationnumberIsByCourse"));
        form.add(new CheckBox("directApproval"));
        form.add(new DropDownChoice("suspendedStatus", registrantStatusService.findAll()));
        form.add(new DropDownChoice("deRegisteredStatus", registrantStatusService.findAll()));
        form.add(new DropDownChoice("voidedStatus", registrantStatusService.findAll()));
        form.add(new DropDownChoice("affidavit", new RequirementListModel(requirementService)));
        form.add(new CheckBox("requireAffidavit"));
        form.add(new CheckBox("billingModule"));
        form.add(new CheckBox("reportsByCourse"));
        form.add(new TextField<Integer>("monthsToRequireAffidavit"));
        form.add(new DropDownChoice("businessAddressType", addressTypeService.findAll()));
        form.add(new DropDownChoice("physicalAddressType", addressTypeService.findAll()));
        form.add(new DropDownChoice("contactAddressType", addressTypeService.findAll()));
        form.add(new DropDownChoice("businessNumberContactType", contactTypeService.findAll()));
        form.add(new DropDownChoice("mobileNumberContactType", contactTypeService.findAll()));
        form.add(new DropDownChoice("faxNumberContactType", contactTypeService.findAll()));
        form.add(new CheckBox("directRegistration"));
        form.add(new CheckBox("registrationCertificateSerialIsIncrimental"));
        form.add(new CheckBox("courseCertificateNumber"));
        form.add(new CheckBox("registrationNumberByYear"));
        form.add(new CheckBox("registerQualificationBeforeTransfer"));
        form.add(new CheckBox("examClearance"));
        form.add(new CheckBox("allowCapitalizationOfFirstLetters"));
        form.add(new CheckBox("allowChangeOfRegistrationNumberAfterTransfer"));
        form.add(new CheckBox("captureCPDsDirectly"));
        form.add(new CheckBox("captureCPDsDirectlyAndInDirectly"));
        form.add(new CheckBox("dynamicDisciplineReport"));
        form.add(new CheckBox("allowRenewalBeforeCPDs"));
        form.add(new CheckBox("showActiveDurationsOnly"));
        form.add(new CheckBox("issueDiplomaWithoutExams"));
        form.add(new CheckBox("hasQuaterRenewal"));
       // form.add(new CheckBox("lastRenewalPeriodShowsCouncilPeriod"));
        form.add(new TextField<Integer>("minimumYearsForPBQCGSApplication"));
        form.add(new TextField<Integer>("registrationCertificateSerial"));
        add(form);
    }

    private final class LoadableDetachableGeneralParametersModel extends LoadableDetachableModel<GeneralParameters> {

        @Override
        protected GeneralParameters load() {

            return generalParametersService.get();

        }
    }
}
