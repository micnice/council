/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.EmploymentStatusDAO;
import zw.co.hitrac.council.business.domain.EmploymentStatus;
import zw.co.hitrac.council.business.service.EmploymentStatusService;

import java.util.List;

/**
 *
 * @author kelvin
 */
@Service
@Transactional
public class EmploymentStatusServiceImpl  implements EmploymentStatusService{
  @Autowired
    private EmploymentStatusDAO employmentDao;

    @Transactional
    public EmploymentStatus save(EmploymentStatus t) {
        return employmentDao.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<EmploymentStatus> findAll() {
        return employmentDao.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public EmploymentStatus get(Long id) {
        return employmentDao.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<EmploymentStatus> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setEmploymentStatusDAO(EmploymentStatusDAO employmentDAO) {

        this.employmentDao = employmentDAO;
    }
}