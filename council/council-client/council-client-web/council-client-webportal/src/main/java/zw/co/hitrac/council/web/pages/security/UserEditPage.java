package zw.co.hitrac.council.web.pages.security;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.User;
import zw.co.hitrac.council.business.service.UserService;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.configure.IAdministerDatabaseBasePage;

//~--- JDK imports ------------------------------------------------------------
import java.util.Arrays;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.CheckBoxMultipleChoice;

/**
 *
 * @author Clive Gurure
 * @author Michael Matiashe
 */
public class UserEditPage extends IAdministerDatabaseBasePage {
    @SpringBean
    UserService userService;

    UserEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<User>(new UserEditPage.LoadableDetachableUserModel(id)));

        Form<User> form = new Form<User>("form", (IModel<User>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                User user = getModelObject();
                user.setIsEncrypted(Boolean.FALSE);
                userService.save(user);
                setResponsePage(new UserViewPage(user.getId()));
            }
        };

        form.add(new TextField<String>("firstName").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("username").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("surname").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("email").setRequired(false).add(new ErrorBehavior()));
        form.add(new TextField<String>("printerName").setRequired(false).add(new ErrorBehavior()));
        form.add(new CheckBoxMultipleChoice("roles", Arrays.asList(Role.values())));
        form.add(new PasswordTextField("password").setRequired(true).add(new ErrorBehavior()));
        form.add(new PasswordTextField("confirmPassword", new Model("")).setRequired(true).add(new ErrorBehavior()));
        form.add(new CheckBox("authoriser"));
        form.add(new CheckBox("applicationProcessor"));
        form.add(new CheckBox("councilProcessor"));
        add(form);
        form.add(new BookmarkablePageLink<Void>("returnLink", UserListPage.class));
    }

    private final class LoadableDetachableUserModel extends LoadableDetachableModel<User> {
        private Long id;

        public LoadableDetachableUserModel(Long id) {
            this.id = id;
        }

        @Override
        protected User load() {
            if (id == null) {
                return new User();
            } else {
                return userService.get(id);
            }
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
