package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.extensions.yui.calendar.DatePicker;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.PaymentConfiguration;
import zw.co.hitrac.council.business.domain.TypeOfService;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.PaymentConfigurationService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

//~--- JDK imports ------------------------------------------------------------

import java.util.Arrays;
import zw.co.hitrac.council.web.utility.CustomDateTextField;

/**
 *
 * @author Kelvin  Goredema
 */
public class PaymentConfigurationEditPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private PaymentConfigurationService paymentConfigurationService;
    @SpringBean
    private CourseService               courseService;

    public PaymentConfigurationEditPage(Long id) {
        setDefaultModel(
            new CompoundPropertyModel<PaymentConfiguration>(
                new PaymentConfigurationEditPage.LoadableDetachablePaymentConfigurationModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));

        Form<PaymentConfiguration> form = new Form<PaymentConfiguration>("form",
                                              (IModel<PaymentConfiguration>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                paymentConfigurationService.save(getModelObject());
                setResponsePage(new PaymentConfigurationViewPage(getModelObject().getId()));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new CustomDateTextField("dueDate").setRequired(true).add(new DatePicker()));
        form.add(new DropDownChoice<Course>("course", courseService.findAll()));
        form.add(new DropDownChoice("typeOfService", Arrays.asList(TypeOfService.values())));
        form.add(new BookmarkablePageLink<Void>("returnLink", PaymentConfigurationListPage.class));
        add(form);
    }

    private final class LoadableDetachablePaymentConfigurationModel
            extends LoadableDetachableModel<PaymentConfiguration> {
        private Long id;

        public LoadableDetachablePaymentConfigurationModel(Long id) {
            this.id = id;
        }

        @Override
        protected PaymentConfiguration load() {
            if (id == null) {
                return new PaymentConfiguration();
            }

            return paymentConfigurationService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
