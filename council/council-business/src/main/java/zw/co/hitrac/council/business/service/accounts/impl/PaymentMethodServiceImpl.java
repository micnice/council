
package zw.co.hitrac.council.business.service.accounts.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.accounts.PaymentMethodDAO;
import zw.co.hitrac.council.business.domain.accounts.PaymentMethod;
import zw.co.hitrac.council.business.service.accounts.PaymentMethodService;

import java.util.List;

/**
 *
 * @author Tatenda Chiwandire
 */
@Service
@Transactional
public class PaymentMethodServiceImpl implements PaymentMethodService {

    @Autowired
    private PaymentMethodDAO paymentMethodDAO;

    @Transactional
    public PaymentMethod save(PaymentMethod t) {
        return paymentMethodDAO.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<PaymentMethod> findAll() {
        return paymentMethodDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public PaymentMethod get(Long id) {
        return paymentMethodDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<PaymentMethod> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setPaymentMethodDao(PaymentMethodDAO paymentMethodDAO) {

        this.paymentMethodDAO = paymentMethodDAO;
    }
}
