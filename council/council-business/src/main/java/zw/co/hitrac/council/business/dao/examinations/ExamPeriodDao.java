/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.examinations;

import zw.co.hitrac.council.business.dao.IGenericDAO;
import zw.co.hitrac.council.business.domain.examinations.ExamPeriod;

/**
 *
 * @author tidza
 */
public interface ExamPeriodDao extends IGenericDAO<ExamPeriod> {
    
}
