package zw.co.hitrac.council.business.service.accounts.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.accounts.CarryForwardLogDAO;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.CarryForwardLog;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.service.accounts.CarryForwardLogService;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Charles Chigoriwa
 */
@Service
@Transactional
public class CarryForwardLogServiceImpl implements CarryForwardLogService{
    
    @Autowired
    private CarryForwardLogDAO carryForwardLogDAO;
    

    @Transactional
    public CarryForwardLog save(CarryForwardLog carryForwardLog) {
       return carryForwardLogDAO.save(carryForwardLog);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<CarryForwardLog> findAll() {
        return carryForwardLogDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public CarryForwardLog get(Long id) {
       return carryForwardLogDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<CarryForwardLog> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setCarryForwardLogDAO(CarryForwardLogDAO carryForwardLogDAO) {

        this.carryForwardLogDAO = carryForwardLogDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<CarryForwardLog> getRegistrantCarryForwardLogs(Customer customer) {
        return carryForwardLogDAO.getRegistrantCarryForwardLogs(customer);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<CarryForwardLog> carryForwardLogs(Registrant registrant, Date startDate, Date endDate) {
       return carryForwardLogDAO.carryForwardLogs(registrant, startDate, endDate);
    }
    
}
