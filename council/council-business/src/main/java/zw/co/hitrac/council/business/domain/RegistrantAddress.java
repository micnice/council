package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.utils.CaseUtil;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.io.Serializable;

/**
 *
 * @author Constance Mabaso
 */
@Entity
@Table(name = "registrantaddress")
@Audited
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class RegistrantAddress extends BaseIdEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    private City city;
    private String address1;
    private String address2;
    private Boolean status = Boolean.FALSE;
    private AddressType addressType;
    private Registrant registrant;
    private Institution institution;
    private Country country;

    public RegistrantAddress() {
    }

    @ManyToOne(optional = true)
    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    @Basic(optional = true)
    @Column(name = "address1", nullable = true, length = 255)
    public String getAddress1() {
        return CaseUtil.upperCase(address1);
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    @Basic(optional = true)
    @Column(name = "address2", nullable = true, length = 50)
    public String getAddress2() {
        return CaseUtil.upperCase(address2);
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    @XmlTransient
    @Transient
    public String getPreferred() {
        return getStatus() ? "Active" : " ";
    }

    @Basic(optional = true)
    @Column(name = "status", nullable = true)
    public Boolean getStatus() {
        if (status == null) {
            return Boolean.FALSE;
        }
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @ManyToOne(optional = true)
    public AddressType getAddressType() {
        return addressType;
    }

    public void setAddressType(AddressType addressType) {
        this.addressType = addressType;
    }

    @ManyToOne
    public Registrant getRegistrant() {
        return registrant;
    }

    public void setRegistrant(Registrant registrant) {
        this.registrant = registrant;
    }

    @ManyToOne
    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    @Transient
    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @Transient
    public String getFullAddress() {
        return getAddress1() + " " + getAddress2() + " " + getCity();
    }

    @XmlElement
    @Transient
    public String getFullDetail() {
        return getAddressType() + " : " + getAddress1() + " " + getAddress2() + " " + getCity();
    }

    @Override
    public String toString() {
        return getAddress1() + " " + getAddress2() + " " + getCity();
    }
}
