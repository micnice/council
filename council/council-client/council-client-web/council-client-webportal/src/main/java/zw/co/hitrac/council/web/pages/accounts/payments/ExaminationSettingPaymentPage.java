
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.accounts.payments;

//~--- non-JDK imports --------------------------------------------------------
import java.math.BigDecimal;
import java.util.ArrayList;
import zw.co.hitrac.council.web.pages.examinations.*;
import net.sf.jasperreports.engine.JRException;

import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.examinations.ExamSettingService;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

//~--- JDK imports ------------------------------------------------------------
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.ModulePaper;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistrationDebtComponent;
import zw.co.hitrac.council.business.domain.reports.ProductItem;
import zw.co.hitrac.council.business.service.ModulePaperService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.service.accounts.ReceiptItemService;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationDebtComponentService;
import zw.co.hitrac.council.business.service.examinations.ExamService;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.reports.ProductItemReport;
import zw.co.hitrac.council.web.models.ExamSettingListModel;

/**
 *
 * @author tdhlakama
 */
public class ExaminationSettingPaymentPage extends IExaminationsPage {

    @SpringBean
    private ExamSettingService examSettingService;
    @SpringBean
    private CourseService courseService;
    private ExamSetting examSetting;
    @SpringBean
    ReceiptItemService receiptItemService;
    @SpringBean
    RegistrationService registrationService;
    @SpringBean
    private ExamRegistrationDebtComponentService examRegistrationDebtComponentService;
    @SpringBean
    private ExamService examService;
    @SpringBean
    private ModulePaperService modulePaperService;
    private HrisComparator hrisComparator;

    public ExaminationSettingPaymentPage() {

        PropertyModel<ExamSetting> examSettingModel = new PropertyModel<ExamSetting>(this, "examSetting");
        Form<?> form = new Form("form") {
            @Override
            protected void onSubmit() {
                try {

                    ProductItemReport productItemReport = new ProductItemReport();
                    Map parameters = new HashMap();

                    parameters.put("comment", "Examination Payments - " + examSetting.toString());
                    Set<ModulePaper> modulePapers = new HashSet<ModulePaper>(examService.findExamPapers(examSetting));
                    
                    List<ProductItem> productSales = new ArrayList<ProductItem>();
                    for (ModulePaper modulePaper : modulePapers) {
                        ProductItem productSale = new ProductItem();
                        productSale.setProduct(modulePaper.getProduct());
                        BigDecimal totalAmount = new BigDecimal(0);
                        for (ExamRegistrationDebtComponent component : examRegistrationDebtComponentService.getDebtComponents(examSetting, productSale.getProduct(), null)) {
                            totalAmount = totalAmount.add(receiptItemService.getTotalPaidForDebtComponent(component.getDebtComponent()));
                        }
                        productSale.setAmountPaid(totalAmount);
                        productSales.add(productSale);
                    }
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(productItemReport,
                            contentType, productSales, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    error(ex.getMessage());
                }
            }
        };

        form.add(new DropDownChoice("examSetting", examSettingModel, new ExamSettingListModel(examSettingService)).setRequired(true).add(new ErrorBehavior()));

        add(form);
    }
}



//~ Formatted by Jindent --- http://www.jindent.com
