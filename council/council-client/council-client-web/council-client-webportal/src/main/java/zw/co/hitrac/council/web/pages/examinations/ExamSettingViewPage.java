package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.service.examinations.ExamSettingService;
import zw.co.hitrac.council.web.utility.CustomDateLabel;

/**
 *
 * @author Constance Mabaso
 */
public class ExamSettingViewPage extends IExaminationsPage {
    @SpringBean
    private ExamSettingService examSettingService;

    public ExamSettingViewPage(Long id) {
        CompoundPropertyModel<ExamSetting> model =
            new CompoundPropertyModel<ExamSetting>(new LoadableDetachableExamSettingModel(id));

        setDefaultModel(model);
        add(new Link<ExamSetting>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new ExamSettingEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", ExamSettingListPage.class));
        add(new Label("examPeriod"));
        add(new CustomDateLabel("startDate"));
        add(new CustomDateLabel("endDate"));
        add(new CustomDateLabel("markingStartDate"));
        add(new CustomDateLabel("markingEndDate"));
        add(new Label("examYear"));
        add(new Label("activePeriod"));
        add(new ExamPanelList("examPanelList"));
    }

    private final class LoadableDetachableExamSettingModel extends LoadableDetachableModel<ExamSetting> {
        private Long id;

        public LoadableDetachableExamSettingModel(Long id) {
            this.id = id;
        }

        @Override
        protected ExamSetting load() {
            return examSettingService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
