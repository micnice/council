/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.configure;

import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.PenaltyType;
import zw.co.hitrac.council.business.service.PenaltyTypeService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

/**
 *
 * @author kelvin
 */
public class PenaltyTypeListPage extends IAdministerDatabaseBasePage{
    @SpringBean 
    
     private PenaltyTypeService penaltyTypeService;

    public PenaltyTypeListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink",AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink",AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {

            @Override
            public void onClick() {
               setResponsePage(new PenaltyTypeEditPage(null));
            }
        });
        
        IModel<List<PenaltyType>> model=new LoadableDetachableModel<List<PenaltyType>>() {

            @Override
            protected List<PenaltyType> load() {
               return penaltyTypeService.findAll();
            }
        };
        
        PropertyListView<PenaltyType> eachItem=new PropertyListView<PenaltyType>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<PenaltyType> item) {
                Link<PenaltyType> viewLink=new Link<PenaltyType>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                       setResponsePage(new PenaltyTypeViewPage(getModelObject().getId()));
                    }
                };
                if(item.getIndex()%2==0){
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };
        
        add(eachItem);
    }
    
    
    
}


