/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.RegistrantDisciplineDAO;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantDiscipline;
import zw.co.hitrac.council.business.service.RegistrantDisciplineService;

import java.util.Date;
import java.util.List;

/**
 *
 * @author kelvin
 */
@Service
@Transactional
public class RegistrantDisciplineServiceImpl implements RegistrantDisciplineService {

    @Autowired
    private RegistrantDisciplineDAO registrantDisciplineDAO;

    @Transactional
    public RegistrantDiscipline save(RegistrantDiscipline registrantDiscipline) {
        return registrantDisciplineDAO.save(registrantDiscipline);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantDiscipline> findAll() {
        return registrantDisciplineDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantDiscipline get(Long id) {
        return registrantDisciplineDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantDiscipline> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setRegistrantDisciplineDAO(RegistrantDisciplineDAO registrantDisciplineDAO) {
        this.registrantDisciplineDAO = registrantDisciplineDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantDiscipline> getDisciplines(Registrant registrant) {
        return registrantDisciplineDAO.getDisciplines(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Date getRegistrantDisciplineDate(Registrant registrant, Course course) {
        return registrantDisciplineDAO.getRegistrantDisciplineDate(registrant, course);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantDiscipline getRegistrantDiscipline(Registrant registrant, Course course) {
        return registrantDisciplineDAO.getRegistrantDiscipline(registrant, course);
    }
}
