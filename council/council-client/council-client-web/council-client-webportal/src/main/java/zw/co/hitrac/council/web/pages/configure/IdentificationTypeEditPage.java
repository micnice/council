package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.IdentificationType;
import zw.co.hitrac.council.business.service.IdentificationTypeService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Charles Chigoriwa
 */
public class IdentificationTypeEditPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private IdentificationTypeService identificationTypeService;

    public IdentificationTypeEditPage(Long id) {
        setDefaultModel(
            new CompoundPropertyModel<IdentificationType>(new LoadableDetachableIdentificationTypeModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));

        Form<IdentificationType> form = new Form<IdentificationType>("form",
                                            (IModel<IdentificationType>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                identificationTypeService.save(getModelObject());
                setResponsePage(new IdentificationTypeViewPage(getModelObject().getId()));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink", IdentificationTypeListPage.class));
        form.add(new CheckBox("expire"));
        add(form);
    }

    private final class LoadableDetachableIdentificationTypeModel extends LoadableDetachableModel<IdentificationType> {
        private Long id;

        public LoadableDetachableIdentificationTypeModel(Long id) {
            this.id = id;
        }

        @Override
        protected IdentificationType load() {
            if (id == null) {
                return new IdentificationType();
            }

            return identificationTypeService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
