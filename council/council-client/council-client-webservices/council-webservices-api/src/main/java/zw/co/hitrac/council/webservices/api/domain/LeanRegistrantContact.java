package zw.co.hitrac.council.webservices.api.domain;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Daniel Nkhoma
 */
@XmlRootElement
public class LeanRegistrantContact {
    
    private Long registrantContactId;

    public LeanRegistrantContact() {
    }

    public LeanRegistrantContact(Long registrantContactId) {
        this.registrantContactId = registrantContactId;
    }

    public Long getRegistrantContactId() {
        return registrantContactId;
    }

    public void setRegistrantContactId(Long registrantContactId) {
        this.registrantContactId = registrantContactId;
    }
}
