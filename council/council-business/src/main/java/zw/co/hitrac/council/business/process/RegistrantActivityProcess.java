/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.process;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.domain.accounts.OldReceipt;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.service.DurationService;
import zw.co.hitrac.council.business.service.RegistrantActivityService;
import zw.co.hitrac.council.business.service.RegistrantQualificationService;
import zw.co.hitrac.council.business.utils.CouncilException;

import java.util.Date;
import java.util.List;

/**
 * @author Matiashe Michael
 */
@Service
public class RegistrantActivityProcess {

    @Autowired
    private RegistrantActivityService registrantActivityService;
    @Autowired
    private DurationService durationService;
    @Autowired
    private RegistrantQualificationService registrantQualificationService;

    public void registrationAndRenewalActivity(Registration registration, DebtComponent debtComponent, Duration duration) {

        RegistrantActivity registrantActivity = new RegistrantActivity();
        registrantActivity.setTransactionDebtComponent(debtComponent);
        registrantActivity.setStartDate(new Date());
        if (duration != null) {
            registrantActivity.setEndDate(duration.getEndDate());
        }
        registrantActivity.setDuration(duration);
        registrantActivity.setRegister(registration.getRegister());
        registrantActivity.setRegistrant(registration.getRegistrant());
        registrantActivity.setRegistrantActivityType(RegistrantActivityType.REGISTRATION);
        registrantActivityService.save(registrantActivity);

        registration.setDuration(duration);
        registrantActivity = new RegistrantActivity();
        registrantActivity.setTransactionDebtComponent(debtComponent);
        registrantActivity.setStartDate(new Date());
        if (duration != null) {
            registrantActivity.setEndDate(duration.getEndDate());
        }
        registrantActivity.setDuration(duration);
        registrantActivity.setRegister(registration.getRegister());
        registrantActivity.setRegistrant(registration.getRegistrant());
        registrantActivity.setRegistrantActivityType(RegistrantActivityType.RENEWAL);
        registrantActivityService.save(registrantActivity);
    }

    public void transferRegistrantActivity(Registration registration, DebtComponent debtComponent, Duration duration) {
        RegistrantActivity registrantActivity = new RegistrantActivity();
        registrantActivity.setTransactionDebtComponent(debtComponent);
        registrantActivity.setStartDate(new Date());
        registrantActivity.setDuration(duration);
        registrantActivity.setRegister(registration.getRegister());
        registrantActivity.setRegistrant(registration.getRegistrant());
        registrantActivity.setRegistrantActivityType(RegistrantActivityType.TRANSFER);
        registrantActivityService.save(registrantActivity);
    }

    public void registrationActivity(Registration registration, DebtComponent debtComponent, Duration duration) {

        RegistrantActivity registrantActivity = new RegistrantActivity();
        registrantActivity.setTransactionDebtComponent(debtComponent);
        registrantActivity.setStartDate(new Date());
        if (duration != null) {
            registrantActivity.setEndDate(duration.getEndDate());
        }
        registrantActivity.setDuration(duration);
        registrantActivity.setRegister(registration.getRegister());
        registrantActivity.setRegistrant(registration.getRegistrant());
        registrantActivity.setRegistrantActivityType(RegistrantActivityType.REGISTRATION);
        registrantActivityService.save(registrantActivity);
    }

    public void renewalRegistrantActivity(Registration registration, DebtComponent debtComponent, Duration duration) {
        RegistrantActivity registrantActivity = new RegistrantActivity();
        registrantActivity.setTransactionDebtComponent(debtComponent);
        if (duration != null) {
            registrantActivity.setStartDate(duration.getStartDate());
            registrantActivity.setEndDate(duration.getEndDate());
        }
        registrantActivity.setDuration(duration);
        registrantActivity.setRegister(registration.getRegister());
        registrantActivity.setRegistrant(registration.getRegistrant());
        registrantActivity.setRegistrantActivityType(RegistrantActivityType.RENEWAL);
        registrantActivityService.save(registrantActivity);
    }

    public void applicationRegistrantActivity(Registration registration, DebtComponent debtComponent) {

        RegistrantActivity registrantActivity = new RegistrantActivity();
        registrantActivity.setTransactionDebtComponent(debtComponent);
        registrantActivity.setStartDate(new Date());
        registrantActivity.setRegister(registration.getRegister());
        if (durationService.getCurrentCourseDuration(registration.getCourse()) != null) {
            registrantActivity.setEndDate(durationService.getCurrentCourseDuration(registration.getCourse()).getEndDate());
        } else {
            throw new CouncilException("Course duration not set");
        }
        registrantActivity.setRegistrant(registration.getRegistrant());
        registrantActivity.setRegistrantActivityType(RegistrantActivityType.REGISTRATION);
        //save registrant activity
        registrantActivityService.save(registrantActivity);
    }

    public void changeOfNameRegistrantActivity(Application application, DebtComponent debtComponent) {
        RegistrantActivity registrantActivity = new RegistrantActivity();
        registrantActivity.setTransactionDebtComponent(debtComponent);
        registrantActivity.setStartDate(new Date());
        registrantActivity.setRegistrant(application.getRegistrant());
        registrantActivity.setFirstName(application.getRegistrant().getFirstname());
        registrantActivity.setMiddleName(application.getRegistrant().getMiddlename());
        registrantActivity.setLastName(application.getRegistrant().getLastname());
        registrantActivity.setRegistrantActivityType(RegistrantActivityType.CHANGE_OF_NAME);
        //save registrant activity
        registrantActivityService.save(registrantActivity);
    }

    public void examRegistrationRegistrantActivity(Registration registration, ExamSetting examSetting) {

        RegistrantActivity registrantActivity = new RegistrantActivity();
        registrantActivity.setStartDate(examSetting.getStartDate());
        registrantActivity.setEndDate(examSetting.getEndDate()); //Exam Setting Dates
        registrantActivity.setRegistrant(registration.getRegistrant());
        registrantActivity.setRegistrantActivityType(RegistrantActivityType.EXAM_REGISTRATION);
        registrantActivityService.save(registrantActivity);

    }

    public void suspenedRegistrationRegistrantActivity(Registration registration, RegistrantDisciplinary registrantDisciplinary) {
        RegistrantActivity registrantActivity = new RegistrantActivity();
        registrantActivity.setStartDate(registrantDisciplinary.getPenaltyStartDate()); //suspension period
        registrantActivity.setEndDate(registrantDisciplinary.getPenaltyEndDate()); //suspension period
        registrantActivity.setRegistrant(registration.getRegistrant());
        registrantActivity.setRegistrantActivityType(RegistrantActivityType.SUSPENDED);
        registrantActivityService.save(registrantActivity);

    }

    public void deathRegistrationRegistrantActivity(Registrant registrant, String comment) {
        RegistrantActivity registrantActivity = new RegistrantActivity();
        registrantActivity.setStartDate(new Date()); //reported date
        registrantActivity.setRegistrant(registrant);
        registrantActivity.setComment(comment);
        registrantActivity.setRegistrantActivityType(RegistrantActivityType.DEATH);
        registrantActivityService.save(registrantActivity);
    }

    public void retiredRegistrantActivity(Registrant registrant, String comment) {
        RegistrantActivity registrantActivity = new RegistrantActivity();
        registrantActivity.setStartDate(new Date()); //retired date
        registrantActivity.setRegistrant(registrant);
        registrantActivity.setComment(comment);
        registrantActivity.setRegistrantActivityType(RegistrantActivityType.RETIRED);
        registrantActivityService.save(registrantActivity);
    }

    public void voidedRegistrantActivity(Registrant registrant, String comment) {
        RegistrantActivity registrantActivity = new RegistrantActivity();
        registrantActivity.setStartDate(new Date());
        registrantActivity.setRegistrant(registrant);
        registrantActivity.setComment(comment);
        registrantActivity.setRegistrantActivityType(RegistrantActivityType.VIODED);
        registrantActivityService.save(registrantActivity);
    }

    public void deRegistrationRegistrantActivity(Registrant registrant, String comment) {
        RegistrantActivity registrantActivity = new RegistrantActivity();
        registrantActivity.setStartDate(new Date());
        registrantActivity.setRegistrant(registrant);
        registrantActivity.setComment(comment);
        registrantActivity.setRegistrantActivityType(RegistrantActivityType.DEREGISTRATION);
        registrantActivityService.save(registrantActivity);

    }

    public void oldReceiptRegistrantActivity(OldReceipt oldReceipt) {
        RegistrantActivity registrantActivity = new RegistrantActivity();
        registrantActivity.setRegistrant(oldReceipt.getRegistrant());
        registrantActivity.setRegistrantActivityType(RegistrantActivityType.RENEWAL);
        registrantActivity.setStartDate(oldReceipt.getDateOfReceipt());
        registrantActivity.setEndDate(oldReceipt.getDateOfReceipt());
        registrantActivity.setDateCreated(new Date());
        registrantActivityService.save(registrantActivity);
    }

    public void penaltyRegistrantActivity(Registration registration, DebtComponent debtComponent, Duration duration) {
        RegistrantActivity registrantActivity = new RegistrantActivity();
        registrantActivity.setTransactionDebtComponent(debtComponent);
        registrantActivity.setStartDate(new Date());
        registrantActivity.setEndDate(duration.getEndDate());
        registrantActivity.setDuration(duration);
        registrantActivity.setRegister(registration.getRegister());
        registrantActivity.setRegistrant(registration.getRegistrant());
        registrantActivity.setRegistrantActivityType(RegistrantActivityType.PENALTY);
        registrantActivityService.save(registrantActivity);
    }

    public void lastRenewalActivity(Registration registration, RegistrantActivity registrantActivity) {
        List<RegistrantQualification> registrantQualifications = registrantQualificationService.getRegistrantQualifications(registration.getRegistrant());
        RegistrantQualification registrantQualification = null;
        if (registrantQualifications.isEmpty()) {
            throw new CouncilException(
                    "No qualifications captured");
        } else {
            registrantQualification = registrantQualifications.get(0);

            registrantActivity.setDuration(durationService.getLastRenewalDuration(registrantQualification.getCourse(), registrantActivity.getStartDate()));

        }
        registrantActivity.setRegistrantActivityType(RegistrantActivityType.RENEWAL);
        registrantActivity.setDateCreated(new Date());
        registrantActivity.setComment("Registrant activity created to mark last renewal date on record update");
        registrantActivity.setPaid(Boolean.TRUE);
        Date endDate = registrantActivity.getStartDate();
        int month = endDate.getMonth();
        int year = endDate.getYear();
        endDate.setMonth(month + 1);
        if (month != 01)
            endDate.setYear(year + 1);
        registrantActivity.setEndDate(endDate);
        registrantActivity.setRegister(registration.getRegister());
        registrantActivity.setRegistrant(registration.getRegistrant());
        registrantActivityService.save(registrantActivity);
    }

}
