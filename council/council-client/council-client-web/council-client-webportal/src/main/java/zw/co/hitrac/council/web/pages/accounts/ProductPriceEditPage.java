package zw.co.hitrac.council.web.pages.accounts;

import java.math.BigDecimal;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.ProductPrice;
import zw.co.hitrac.council.business.service.accounts.ProductPriceService;
import zw.co.hitrac.council.business.service.accounts.ProductService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Charles Chigoriwa
 */
public class ProductPriceEditPage extends IInventoryModulePage {
    
    @SpringBean
    private ProductPriceService productPriceService;
    
    @SpringBean
    private ProductService productService;

    public ProductPriceEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<ProductPrice>(new LoadableDetachableProductPriceModel(id)));
        Boolean idIsNull = Boolean.FALSE;
        if(id == null){
            idIsNull = Boolean.TRUE;
        }
        Form<ProductPrice> form=new Form<ProductPrice>("form",(IModel<ProductPrice>)getDefaultModel()) {
            @Override
            public void onSubmit(){
                productPriceService.save(getModelObject());
                setResponsePage(new ProductPriceViewPage(getModelObject().getId()));
            }
        };
        
        form.add(new TextField<BigDecimal>("price").setRequired(true).add(new ErrorBehavior())); 
        if(idIsNull){
        form.add(new DropDownChoice("product", productService.findProductsWithoutPrices()));
        } else {
        form.add(new DropDownChoice("product", productService.findAll()));
        }
        form.add(new BookmarkablePageLink<Void>("returnLink",ProductPriceListPage.class));
        add(form);
    }
    
    
    private final class LoadableDetachableProductPriceModel extends LoadableDetachableModel<ProductPrice> {

        private Long id;

        public LoadableDetachableProductPriceModel(Long id) {
            this.id = id;
        }

        @Override
        protected ProductPrice load() {
            if(id==null){
                return new ProductPrice();
            }
            return productPriceService.get(id);
        }
    }
    
    
}
