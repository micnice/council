
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------

import com.googlecode.wicket.jquery.ui.panel.JQueryFeedbackPanel;
import net.sf.jasperreports.engine.JRException;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.examinations.ExamPassRate;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationService;
import zw.co.hitrac.council.business.service.examinations.ExamSettingService;
import zw.co.hitrac.council.reports.CountryPassRateReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

//~--- JDK imports ------------------------------------------------------------
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.wicket.markup.html.panel.FeedbackPanel;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.reports.CoursePassRateReport;
import zw.co.hitrac.council.web.models.CourseListModel;
import zw.co.hitrac.council.web.models.ExamSettingListModel;

/**
 * @author tdhlakama
 */
public class ExaminationPassRatePage extends IExaminationsPage {

    @SpringBean
    private ExamRegistrationService examRegistrationService;
    @SpringBean
    private ExamSettingService examSettingService;
    @SpringBean
    private CourseService courseService;
    private Course course;
    private ExamSetting examSetting;
    private HrisComparator hrisComparator = new HrisComparator();

    public ExaminationPassRatePage() {
        final FeedbackPanel feedback = new JQueryFeedbackPanel("feedback");
        add(feedback.setOutputMarkupId(true));
        PropertyModel<ExamSetting> examSettingModel = new PropertyModel<ExamSetting>(this, "examSetting");
        PropertyModel<Course> courseModel = new PropertyModel<Course>(this, "course");
        Form<?> form = new Form("form");

        form.add(new DropDownChoice("examSetting", examSettingModel, new ExamSettingListModel(examSettingService)).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("course", courseModel, new CourseListModel(courseService)));
        add(form);

        form.add(new Button("countryPassRate") {
            @Override
            public void onSubmit() {
                List<ExamPassRate> passRates = new ArrayList<ExamPassRate>();
                CountryPassRateReport countryPassRateReport = new CountryPassRateReport();

                if (course == null) {
                    error("Select Course");
                    return;
                }
                try {
                    Map parameters = new HashMap();

                    if (course.getQualification() != null) {
                        parameters.put("course", course.getQualification().getName());
                    } else {
                        parameters.put("course", course.getName());
                    }

                    if (examSetting != null) {
                        parameters.put("examSetting", examSetting.toString());
                    }

                    for (Institution institution : examRegistrationService.getInstitutiuons(examSetting, course)) {
                        Integer firstTimePass = 0;
                        Integer firstTimeFail = 0;

                        Integer failedRewrites = 0;
                        Integer passedRewrites = 0;

                        ExamPassRate examPassRate = new ExamPassRate();
                        examPassRate.setCourse(course);
                        examPassRate.setExamSetting(examSetting);
                        examPassRate.setInstitution(institution);

                        for (ExamRegistration e : examRegistrationService.getAllExamCandidates(examSetting, course, institution, null, null, null, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, false)) {
                            if (!e.getPassStatus() && e.getCompletedStatus()) {
                                if (!e.getSupplementaryExam()) {
                                    firstTimeFail++;
                                } else {
                                    failedRewrites++;
                                }
                            } else if (e.getPassStatus() && e.getCompletedStatus()) {
                                if (!e.getSupplementaryExam()) {
                                    firstTimePass++;
                                } else {
                                    passedRewrites++;
                                }

                            }

                            examPassRate.setTotalNumberOfCandidates(examRegistrationService.getTotalNumberOfCandidates(examSetting, course, institution, null, null, null, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE));

                            examPassRate.setTotalNumberFirstTime(examRegistrationService.getTotalNumberOfCandidates(examSetting, course, institution, null, null, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE));
                            examPassRate.setFailedFirstTime(firstTimeFail);
                            examPassRate.setPassedFirstTime(firstTimePass);

                            //re-writes
                            examPassRate.setTotalNumberOfRewrites(examRegistrationService.getTotalNumberOfCandidates(examSetting, course, institution, null,null,Boolean.TRUE, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE,Boolean.FALSE));
                            examPassRate.setPassedRewrites(passedRewrites);
                            examPassRate.setFailedRewrites(failedRewrites);

                        }
                        passRates.add(examPassRate);
                    }

                    parameters.put("comment", "Country Examination Pass Rate");

                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(countryPassRateReport,
                            contentType, hrisComparator.sortExamPassRates(passRates), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });
        

        form.add(new Button("coursePassRate") {
            @Override
            public void onSubmit() {

                CoursePassRateReport coursePassRateReport = new CoursePassRateReport();

                try {
                    Map parameters = new HashMap();
                    if (examSetting != null) {
                        parameters.put("examSetting", examSetting.toString());
                    }
                    List<ExamPassRate> rates = new ArrayList<ExamPassRate>();

                    for (Course c : courseService.findAll()) {
                        ExamPassRate examPassRate = new ExamPassRate();
                        examPassRate.setExamSetting(examSetting);
                        examPassRate.setCourse(c);
                        for (Institution institution : examRegistrationService.getInstitutiuons(examSetting, c)) {
                            if (examRegistrationService.getTotalNumberOfCandidates(examSetting, c, institution, null,null,Boolean.FALSE, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE,Boolean.FALSE) >= 6) {
                                for (ExamRegistration e : examRegistrationService.getAllExamCandidates(examSetting, c, institution, null, null, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, Boolean.FALSE)) {
                                    if (e.getPassStatus() && e.getCompletedStatus()) {
                                        examPassRate.setNumberOfCandidatesPassed(examPassRate.getNumberOfCandidatesPassed() + 1);
                                    }
                                    if (!e.getPassStatus() && e.getCompletedStatus()) {
                                        examPassRate.setNumberOfCandidatesFailed(examPassRate.getNumberOfCandidatesFailed() + 1);
                                    }
                                }
                            }
                        }
                        examPassRate.setTotalNumberOfCandidates(examPassRate.getNumberOfCandidatesPassed() + examPassRate.getNumberOfCandidatesFailed());
                        rates.add(examPassRate);
                    }
                    parameters.put("comment", "Examination Course Pass Rate - Excluding re-writes and schools with candidates with less than 6 candidates");

                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(coursePassRateReport,
                            contentType, rates, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("courseMasterTablePassRate") {
            @Override
            public void onSubmit() {

                CoursePassRateReport coursePassRateReport = new CoursePassRateReport();

                try {
                    Map parameters = new HashMap();
                    if (examSetting != null) {
                        parameters.put("examSetting", examSetting.toString());
                    }
                    List<ExamPassRate> rates = new ArrayList<ExamPassRate>();

                    for (Course c : courseService.findAll()) {
                        ExamPassRate examPassRate = new ExamPassRate();
                        examPassRate.setExamSetting(examSetting);
                        examPassRate.setCourse(c);
                        for (Institution institution : examRegistrationService.getInstitutiuons(examSetting, c)) {
                            for (ExamRegistration e : examRegistrationService.getAllExamCandidates(examSetting, c, institution, null, null, Boolean.FALSE, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, false)) {
                                if (e.getPassStatus() && e.getCompletedStatus()) {
                                    examPassRate.setNumberOfCandidatesPassed(examPassRate.getNumberOfCandidatesPassed() + 1);
                                }
                                if (!e.getPassStatus() && e.getCompletedStatus()) {
                                    examPassRate.setNumberOfCandidatesFailed(examPassRate.getNumberOfCandidatesFailed() + 1);
                                }
                            }
                        }
                        examPassRate.setTotalNumberOfCandidates(examPassRate.getNumberOfCandidatesPassed() + examPassRate.getNumberOfCandidatesFailed());
                        rates.add(examPassRate);
                    }
                    parameters.put("comment", "Examination Course Pass Rate - Excluding re-writes and includes schools with candidates with less than 6 candidates");

                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(coursePassRateReport,
                            contentType, rates, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
