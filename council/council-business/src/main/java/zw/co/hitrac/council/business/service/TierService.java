package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.Tier;

/**
 *
 * @author Edward Zengeni
 */
public interface TierService extends IGenericService<Tier> {
    
}
