
package zw.co.hitrac.council.business.dao.accounts;

import zw.co.hitrac.council.business.dao.IGenericDAO;
import zw.co.hitrac.council.business.domain.accounts.DocumentItem;

/**
 *
 * @author Michael Matiashe
 */
public interface DocumentItemDAO extends IGenericDAO<DocumentItem> {
    
   
    
}
