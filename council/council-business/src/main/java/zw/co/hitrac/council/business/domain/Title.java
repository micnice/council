/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.utils.Gender;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author tidza
 */
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Table(name = "title")
@Audited
@XmlRootElement
public class Title extends BaseEntity implements Serializable {

    private Set<Gender> genders = new HashSet<Gender>();

    public Title() {
    }

    @ElementCollection(targetClass = Gender.class, fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING) 
    @CollectionTable(name = "gendertitle")
    @Column(name = "gender") 
    public Set<Gender> getGenders() {
        return genders;
    }

    public void setGenders(Set<Gender> genders) {
        this.genders = genders;
    }
}

