
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.registration;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantDisciplinary;
import zw.co.hitrac.council.business.service.RegistrantDisciplinaryService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.CustomDateLabel;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;

/**
 *
 * @author hitrac
 */
public class RegistrantDisciplinaryViewPage extends TemplatePage {

    @SpringBean
    private RegistrantDisciplinaryService registrantDisciplinaryService;
    @SpringBean
    private RegistrantService registrantService;

    public RegistrantDisciplinaryViewPage(Long id) {
       final CompoundPropertyModel<RegistrantDisciplinary> model = new CompoundPropertyModel<RegistrantDisciplinary>(registrantDisciplinaryService.get(id));
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        final Long registrantId = model.getObject().getRegistrant().getId();
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantId);
        setDefaultModel(model);

        final CompoundPropertyModel<Registrant> registrantModel = new CompoundPropertyModel<Registrant>(new DetachableRegistrantModel(id, registrantService));

        add(new CustomDateLabel("caseDate"));
        add(new CustomDateLabel("dateResolved"));
        add(new Label("caseOutcome"));
        add(new Label("misconductType"));
        add(new CustomDateLabel("penaltyStartDate"));
        add(new CustomDateLabel("penaltyEndDate"));
        add(new Link<Void>("registrationOptionPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantId));
            }
        });
        add(new Link<RegistrantDisciplinary>("registrantDisciplinaryPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantDisciplinaryPage(model.getObject().getId(), null));
            }
        });
        add(new Link<RegistrantDisciplinary>("registrantDocumentPage", model) {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantDocumentPage(getModelObject(), registrantModel));
            }
        });

        add(new Label("registrant.fullname"));
        add(new FeedbackPanel("feedback"));
        //supporting documents
        add(new RegistrantSupportingDocumentPanel("supportingDocumentPanel", null, null, null, model.getObject(),null,null,null));

    }
}


//~ Formatted by Jindent --- http://www.jindent.com
