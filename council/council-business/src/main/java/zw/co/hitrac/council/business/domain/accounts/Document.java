package zw.co.hitrac.council.business.domain.accounts;

import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.domain.BaseIdEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Michael Matiashe
 */
@Entity
@Table(name="document")
@Audited
public class Document extends BaseIdEntity implements Serializable {

    private DocumentType documentType;
    private Date date;
    private Customer customer;
    private Set<DocumentItem> documentItems= new HashSet<DocumentItem>();
    

    

    @Enumerated(EnumType.STRING)
    public DocumentType getDocumentType() {
        return documentType;
    }
    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }
    
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @ManyToOne
    public Customer getCustomer() {
        return customer;
    }
    
    //To clarify
   
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @OneToMany(mappedBy = "document")
    public Set<DocumentItem> getDocumentItems() {
        return documentItems;
    }

    public void setDocumentItems(Set<DocumentItem> documentItems) {
        this.documentItems = documentItems;
    }
   
}
