package zw.co.hitrac.council.business.dao;

import java.util.List;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.InstitutionInspection;

/**
 *
 * @author Constance Mabaso
 */
public interface InstitutionInspectionDAO extends IGenericDAO<InstitutionInspection>{
    public List<InstitutionInspection> get(Application application);
}
