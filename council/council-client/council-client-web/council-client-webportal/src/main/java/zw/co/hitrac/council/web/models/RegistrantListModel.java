package zw.co.hitrac.council.web.models;

import java.util.List;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.service.RegistrantService;

/**
 *
 * @author Matiashe Michael
 */
public class RegistrantListModel extends LoadableDetachableModel<List<Registrant>> {
    
    private final RegistrantService registrantService;

    public RegistrantListModel(RegistrantService registrantService) {
        this.registrantService = registrantService;
    }

    @Override
    protected List<Registrant> load() {
      return this.registrantService.findAll();
    }
    
}
