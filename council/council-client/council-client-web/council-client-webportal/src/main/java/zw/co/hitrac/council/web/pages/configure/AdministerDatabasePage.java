package zw.co.hitrac.council.web.pages.configure;

import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.dao.impl.InstitutionManagerDAOImpl;
import zw.co.hitrac.council.business.service.ApplicationService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.accounts.ProductService;
import zw.co.hitrac.council.business.service.accounts.ReceiptHeaderService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.reports.ApplicationListReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.pages.certification.AdminSearchCertificatePage;
import zw.co.hitrac.council.web.pages.examinations.*;
import zw.co.hitrac.council.web.pages.institution.InstitutionListPage;
import zw.co.hitrac.council.web.pages.institution.InstitutionTypeListPage;
import zw.co.hitrac.council.web.pages.research.CouncilUploadPage;
import zw.co.hitrac.council.web.pages.research.UploadPage;
import zw.co.hitrac.council.web.pages.security.UserListPage;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Charles Chigoriwa
 * @author Goredema Kelvin
 * @author Tatenda Chiwandire
 * @author Michael Matiashe
 */
public class AdministerDatabasePage extends IAdministerDatabaseBasePage {

    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private ApplicationService applicationService;
    @SpringBean
    private InstitutionManagerDAOImpl institutionManagerDAOImpl;
    @SpringBean
    private ProductService productService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private ReceiptHeaderService receiptHeaderService;

    public AdministerDatabasePage() {
        /*
         * Manage
         */
        add(new BookmarkablePageLink<Void>("identificationTypeLink", IdentificationTypeListPage.class));
        add(new BookmarkablePageLink<Void>("titleLink", TitleListPage.class));
        add(new BookmarkablePageLink<Void>("contactTypeLink", ContactTypeListPage.class));
        add(new BookmarkablePageLink<Void>("maritalStatusLink", MaritalStatusListPage.class));
        add(new BookmarkablePageLink<Void>("addressTypeLink", AddressTypeListPage.class));
        add(new BookmarkablePageLink<Void>("citizenshipLink", CitizenshipListPage.class));
        add(new BookmarkablePageLink<Void>("registerLink", RegisterListPage.class));
        add(new BookmarkablePageLink<Void>("registerTypeLink", RegisterTypeListPage.class));
        add(new BookmarkablePageLink<Void>("registrantStatusLink", RegistrantStatusListPage.class));
        add(new BookmarkablePageLink<Void>("registrantCPDCategoryLink", RegistrantCPDCategoryListPage.class));
        add(new BookmarkablePageLink<Void>("registrantCPDItemLink", RegistrantCPDItemListPage.class));
        add(new BookmarkablePageLink<Void>("misconductTypeLink", MisconductTypeListPage.class));
        add(new BookmarkablePageLink("uploadPageLink", UploadPage.class));
        add(new BookmarkablePageLink("uploadCouncilPageLink", CouncilUploadPage.class));
        add(new BookmarkablePageLink("courseCertificateNumberPage", CourseCertificateNumberListPage.class));
        add(new Link("copyInstitutions") {

            public void onClick() {

                institutionManagerDAOImpl.copyDataFromInstitutionToInstitutionManager();
            }
        });

        /*
         /*
         * End of Manage
         */
        add(new BookmarkablePageLink<Void>("postLink", PostListPage.class));
        add(new BookmarkablePageLink<Void>("institutionLink", InstitutionListPage.class));
        add(new BookmarkablePageLink<Void>("institutionTypeLink", InstitutionTypeListPage.class));
        add(new BookmarkablePageLink<Void>("serviceLink", ServiceListPage.class));
        add(new BookmarkablePageLink<Void>("employmentStatusLink", EmploymentStatusListPage.class));
        add(new BookmarkablePageLink<Void>("employmentAreaLink", EmploymentAreaListPage.class));
        /*
         * Geographic Information
         */
        add(new BookmarkablePageLink<Void>("districtLink", DistrictListPage.class));
        add(new BookmarkablePageLink<Void>("cityLink", CityListPage.class));
        add(new BookmarkablePageLink<Void>("provinceLink", ProvinceListPage.class));
        add(new BookmarkablePageLink<Void>("countryLink", CountryListPage.class));
        add(new Link("identificationValidationLink") {

            public void onClick() {

                setResponsePage(new IdentificationValidationRuleListPage(getPageReference()));
            }
        });

        /*
         * End Of Geographic Information
         */

        /*
         * Email Information
         */
        add(new BookmarkablePageLink<Void>("emailConfigurationPage", EmailConfigurationPage.class));
        /*
         * Examinations
         */
        add(new BookmarkablePageLink<Void>("courseLink", CourseListPage.class));
        add(new BookmarkablePageLink<Void>("courseTypeLink", CourseTypeListPage.class));
        add(new BookmarkablePageLink<Void>("requirementLink", RequirementListPage.class));
        add(new BookmarkablePageLink<Void>("conditionLink", ConditionListPage.class));
        add(new BookmarkablePageLink<Void>("courseConditionLink", CourseConditionListPage.class));
        add(new BookmarkablePageLink<Void>("modulePaperLink", ModulePaperListPage.class));
        add(new BookmarkablePageLink<Void>("examSettingsLink", ExamSettingListPage.class));
        add(new BookmarkablePageLink<Void>("qualificationLink", QualificationListPage.class));
        add(new BookmarkablePageLink<Void>("courseGroupLink", CourseGroupListPage.class));
        add(new BookmarkablePageLink<Void>("examListPage", ExamListPage.class));
        add(new BookmarkablePageLink<Void>("examYearListPage", ExamYearListPage.class));
        add(new BookmarkablePageLink<Void>("examPeriodListPage", ExamPeriodListPage.class));
        add(new BookmarkablePageLink<Void>("examChangesSettingPage", ExamChangesSettingPage.class));
        add(new BookmarkablePageLink<Void>("tierLink", TierListPage.class));

        /*
         * End Of Examinations
         */
        /*Accounts Information
         *
         */
        add(new BookmarkablePageLink<Void>("durationLink", DurationListPage.class));
        add(new BookmarkablePageLink<Void>("councilDurationLink", CouncilDurationListPage.class));
        add(new BookmarkablePageLink<Void>("generalParametersLink", GeneralParametersEditPage.class));
        add(new BookmarkablePageLink<Void>("requiredParametersLink", RequiredParameterEditPage.class));
        add(new BookmarkablePageLink<Void>("caseOutcomeLink", CaseOutcomeListPage.class));
        add(new BookmarkablePageLink<Void>("transferRuleLink", TransferRuleListPage.class));

        /*
         * End Of Accounting Information
         */
        //CPD points Configuration
        /*
         *
         /*Integration Configuration Information
         * 
         */
        add(new BookmarkablePageLink<Void>("integrationConfigurationPage", IntegrationConfigurationPage.class));

        add(new BookmarkablePageLink<Void>("registrantCpdItemConfigLink", RegistrantCpdItemConfigListPage.class));
        add(new BookmarkablePageLink<Void>("userLink", UserListPage.class));
        add(new BookmarkablePageLink<Void>("adminSearchCertificatePage", AdminSearchCertificatePage.class));
        add(new BookmarkablePageLink<Void>("penaltyParametersLink", PenaltyParametersViewPage.class));
        add(new Link("createCustomerAccounts") {
            @Override
            public void onClick() {
                registrantService.createRegistrantAccounts();
            }
        });
        add(new Link("createProductAccounts") {
            @Override
            public void onClick() {
                productService.createProductAccounts();
            }


        });
        add(new Link("createInstitutionAccounts") {
            @Override
            public void onClick() {
                institutionService.createInstitutionAccounts();
            }


        });

        add(new Link("deleteApplicationsWithoutPayment") {
            @Override
            public void onClick() {
                try {
                    applicationService.removeDebtApplicationsWithoutPayment();
                } catch (CouncilException e) {
                    throw new CouncilException("Removing Items Failed");
                }

            }
        });


        add(new Link("applicationsWithoutPayment") {

            @Override
            public void onClick() {

                try {
                    ApplicationListReport applicationListReport = new ApplicationListReport();
                    ContentType contentType = ContentType.PDF;

                    Map parameters = new HashMap();
                    parameters.put("comment", "All Applications Without Payments");

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(applicationListReport, contentType, applicationService.getApplicationsWithoutPayment(), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);
                    resource.respond(a);
                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

        add(new Link("updateCustomerAndAccountDetails") {
            @Override
            public void onClick() {
                registrantService.updateCustomerAndAccountDetails();
            }
        });

        add(new Link("correctMissingChangeOfBankReport") {
            @Override
            public void onClick() {
                receiptHeaderService.correctMissingChangeOfBankReport();
            }
        });

    }
}
