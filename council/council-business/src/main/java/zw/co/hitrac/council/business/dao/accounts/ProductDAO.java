package zw.co.hitrac.council.business.dao.accounts;

import java.util.List;
import zw.co.hitrac.council.business.dao.IGenericDAO;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.business.domain.TypeOfService;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.domain.accounts.ProductGroup;

/**
 *
 * @author Tatenda Chiwandire
 * @author Michael Matiashe
 */
public interface ProductDAO extends IGenericDAO<Product> {

    public List<Product> findProducts(Product product);

    public List<Product> findProducts(TypeOfService typeOfService);

    public List<Product> examProducts();
    
    public List<Product> findNoneExamProducts();

    public List<Product> findProducts(ProductGroup productGroup);

    public List<Product> findProducts(ProductGroup productGroup, TypeOfService typeOfService, Register register);

    public List<Product> findProducts(TypeOfService typeOfService, Register register, Course course);
    
    public List<Product> findAll(Boolean retired, Boolean directlyInvoicable);
    
    public Product getProductByCode(String productCode);
    
    public List<Product> findProductsWithoutPrices();
    public void createProductAccounts();

}
