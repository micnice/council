package zw.co.hitrac.council.web.pages.registration;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.GeneralParameters;
import zw.co.hitrac.council.business.domain.RegistrantTransfer;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.web.models.RegistrantTransferDataListModel;
import zw.co.hitrac.council.web.pages.TemplatePage;

import java.util.List;

public class RegistrantTransferListPage extends TemplatePage {

    @SpringBean
    private RegistrationService registrationService;
    @SpringBean
    private GeneralParametersService generalParametersService;

    public RegistrantTransferListPage() {
        menuPanelManager.getRegistrantionPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantionPanel().setRegistrantTransferListCurrent(true);

        IModel<List<RegistrantData>> model = new RegistrantTransferDataListModel(registrationService);

        GeneralParameters generalParameters = generalParametersService.get();
        String name = generalParameters.getRegistrantName();

        if ((name == null) || name.trim().equals("")) {
            name = "Registrant";
        }

        add(new Label("name", name));

        // Registrant List Data View Panel
        add(new RegistrantDataViewPanel("registrantDataListPanel", model));
    }
}
