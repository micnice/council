package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.RegistrantStatus;
import zw.co.hitrac.council.business.service.RegistrantStatusService;

/**
 *
 * @author charlesc
 */
public class RegistrantStatusViewPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private RegistrantStatusService registrantStatusService;

    public RegistrantStatusViewPage(Long id) {
        CompoundPropertyModel<RegistrantStatus> model =
            new CompoundPropertyModel<RegistrantStatus>(new LoadableDetachableRegistrantStatusModel(id));

        setDefaultModel(model);
        add(new Link<RegistrantStatus>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantStatusEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", RegistrantStatusListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("registrantStatus", model.bind("name")));
    }

    private final class LoadableDetachableRegistrantStatusModel extends LoadableDetachableModel<RegistrantStatus> {
        private Long id;

        public LoadableDetachableRegistrantStatusModel(Long id) {
            this.id = id;
        }

        @Override
        protected RegistrantStatus load() {
            return registrantStatusService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
