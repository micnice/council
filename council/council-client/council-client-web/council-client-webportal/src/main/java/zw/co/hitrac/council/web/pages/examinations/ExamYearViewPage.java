package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.examinations.ExamYear;
import zw.co.hitrac.council.business.service.examinations.ExamYearService;

/**
 *
 * @author Constance Mabaso
 */
public class ExamYearViewPage extends IExaminationsPage {
    @SpringBean
    private ExamYearService examYearService;

    public ExamYearViewPage(Long id) {
        CompoundPropertyModel<ExamYear> model =
            new CompoundPropertyModel<ExamYear>(new LoadableDetachableExamYearModel(id));

        setDefaultModel(model);
        add(new Link<ExamYear>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new ExamYearEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", ExamYearListPage.class));
        add(new Label("name"));
    }

    private final class LoadableDetachableExamYearModel extends LoadableDetachableModel<ExamYear> {
        private Long id;

        public LoadableDetachableExamYearModel(Long id) {
            this.id = id;
        }

        @Override
        protected ExamYear load() {
            return examYearService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
