package zw.co.hitrac.council.business.domain;

/**
 *
 * @author Edward Zengeni
 * 
 */
public enum QualificationType {

   
    FOREIGN("Foreign Qualification"),
    LOCAL("Local Qualification");

    private QualificationType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    final private String name;

   
}

