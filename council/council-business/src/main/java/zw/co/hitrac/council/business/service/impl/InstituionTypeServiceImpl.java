/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.InstitutionTypeDAO;
import zw.co.hitrac.council.business.domain.InstitutionType;
import zw.co.hitrac.council.business.service.InstitutionTypeService;

import java.util.List;

/**
 *
 * @author Kelvin Goredema
 */
@Service
@Transactional
public class InstituionTypeServiceImpl implements InstitutionTypeService {

    @Autowired
    private InstitutionTypeDAO institutionTypeDAO;

    @Transactional
    public InstitutionType save(InstitutionType institutionType) {
        return institutionTypeDAO.save(institutionType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<InstitutionType> findAll() {
        return institutionTypeDAO.findAll();

    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public InstitutionType get(Long id) {
        return institutionTypeDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<InstitutionType> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setInstitutionTypeDAO(InstitutionTypeDAO institutionTypeDAO) {

        this.institutionTypeDAO = institutionTypeDAO;
    }
}
