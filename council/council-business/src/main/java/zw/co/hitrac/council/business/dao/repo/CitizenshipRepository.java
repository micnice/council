
package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.Citizenship;

/**
 *
 * @author Edward Zengeni
 */
public interface CitizenshipRepository  extends CrudRepository<Citizenship, Long> {
    public List<Citizenship> findAll();
}
