package zw.co.hitrac.council.business.dao.examinations.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.examinations.ExamRegistrationDao;
import zw.co.hitrac.council.business.dao.repo.examinations.ExamRegistrationRepository;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.business.domain.examinations.ExamResult;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;

/**
 * @author tidza
 */
@Repository
public class ExamRegistrationDaoImpl implements ExamRegistrationDao {

    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    private ExamRegistrationRepository examRegistrationRepository;

    @Override
    public ExamRegistration save(ExamRegistration t) {
        return examRegistrationRepository.save(t);
    }

    @Override
    public List<ExamRegistration> findAll() {
        return examRegistrationRepository.findAll();
    }

    @Override
    public ExamRegistration get(Long id) {
        return examRegistrationRepository.findOne(id);
    }

    public void setExamRegistrationRepository(ExamRegistrationRepository examRegistrationRepository) {
        this.examRegistrationRepository = examRegistrationRepository;
    }

    @Override
    public List<ExamRegistration> getAllExamCandidates(ExamSetting examSetting, Course course, Institution institution, String candidateNumber, String idNumber, Boolean supplementaryExam, Boolean suppressed, Boolean closed, Boolean reversedResult, Boolean disqualified) {
        StringBuilder sb = new StringBuilder("");
        sb.append("select p from ExamRegistration p where p.id !=null");

        if (examSetting != null) {
            sb.append(" and p.examSetting=:examSetting ");
        }

        if (institution != null) {
            sb.append(" and p.institution=:institution");
        }

        if (candidateNumber != null) {
            sb.append(" and p.candidateNumber=:candidateNumber");
        }

        if (suppressed != null) {
            sb.append(" and p.suppressed=:suppressed");
        }

        if (closed != null) {
            sb.append(" and p.closed=:closed");
        }

        if (reversedResult != null) {
            sb.append(" and p.reversedResult=:reversedResult");
        }

        if (supplementaryExam != null) {
            sb.append(" and p.supplementaryExam=:supplementaryExam");
        }

        if (course != null) {
            sb.append(" and p.registration.course=:course");
        }

        if (idNumber != null) {
            sb.append(" and p.registration.registrant.idNumber=:idNumber or p.registration.registrant.registrationNumber=:idNumber");
        }

        Query query = entityManager.createQuery(sb.toString());

        if (examSetting != null) {
            query.setParameter("examSetting", examSetting);
        }

        if (institution != null) {
            query.setParameter("institution", institution);
        }

        if (candidateNumber != null) {
            query.setParameter("candidateNumber", candidateNumber);
        }

        if (suppressed != null) {
            query.setParameter("suppressed", suppressed);
        }

        if (closed != null) {
            query.setParameter("closed", closed);
        }

        if (reversedResult != null) {
            query.setParameter("reversedResult", reversedResult);
        }

        if (supplementaryExam != null) {
            query.setParameter("supplementaryExam", supplementaryExam);
        }

        if (idNumber != null) {
            query.setParameter("idNumber", idNumber);
        }

        if (course != null) {
            query.setParameter("course", course);
        }

        List<ExamRegistration> examRegistrationList = query.getResultList();

        if (disqualified != null) {
            examRegistrationList = examRegistrationList.stream().filter(e -> e.getDisqualified().equals(disqualified)).collect(Collectors.toList());
        }

        return examRegistrationList;

    }

    @Override
    public Integer getTotalNumberOfCandidates(ExamSetting examSetting, Course course, Institution institution, String candidateNumber, String idNumber, Boolean supplementaryExam, Boolean suppressed, Boolean closed, Boolean reversedResult, Boolean disqualified) {

        final List<ExamRegistration> examRegistrationList = getAllExamCandidates(examSetting, course, institution, candidateNumber, idNumber, supplementaryExam, suppressed, closed, reversedResult, disqualified);
        final Long count = examRegistrationList.stream().count();

        return count.intValue();
    }

    @Override
    public ExamRegistration get(ExamSetting examSetting, Course course, Registrant registrant) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ExamResult.class);
        Criteria e = criteria.createCriteria("examRegistration");
        e.add(Restrictions.eq("examSetting", examSetting));
        e.createAlias("registration", "r");
        e.add(Restrictions.eq("r.course", course));
        e.add(Restrictions.eq("r.registrant", registrant));
        return (ExamRegistration) criteria.uniqueResult();
    }

    @Override
    public ExamRegistration getExaminationInformation(ExamSetting examSetting, Course course) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ExamRegistration.class);
        criteria.add(Restrictions.eq("examSetting", examSetting));
        criteria.createAlias("registration", "r");
        criteria.add(Restrictions.eq("r.course", course));
        criteria.setMaxResults(1);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        if (criteria.list().isEmpty()) {
            return null;
        } else {
            return (ExamRegistration) criteria.list().get(0);
        }
    }

    @Override
    public List<ExamRegistration> getExamRegistrations(Registrant registrant) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ExamRegistration.class);
        Criteria p = criteria.createCriteria("registration");
        p.add(Restrictions.eq("registrant", registrant));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    @Override
    public List<ExamRegistration> getExamRegistrations(Registration registration) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ExamRegistration.class);
        criteria.add(Restrictions.eq("registration", registration));
        criteria.addOrder(Order.desc("dateCreated"));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    @Override
    public List<Institution> getInstitutiuons(ExamSetting examSetting, Course course) {
        return entityManager.createQuery("select DISTINCT e.institution from ExamRegistration e where e.registration.course =:course and e.examSetting=:examSetting").setParameter("examSetting", examSetting).setParameter("course", course).getResultList();
    }

    @Override
    public List<ExamResult> getExamResults(ExamSetting examSetting, Course course, Institution institution, String candidateNumber, String idNumber, Boolean supplementaryExam) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ExamResult.class);
        Criteria e = criteria.createCriteria("examRegistration");
        if (examSetting != null) {
            e.add(Restrictions.eq("examSetting", examSetting));
        }
        if (institution != null) {
            e.add(Restrictions.eq("institution", institution));
        }
        if (candidateNumber != null) {
            e.add(Restrictions.eq("candidateNumber", candidateNumber));
        }
        if (supplementaryExam != null) {
            e.add(Restrictions.eq("supplementaryExam", supplementaryExam));
        }
        e.add(Restrictions.eq("reversedResult", Boolean.FALSE));
        e.createAlias("registration", "r");
        if (course != null) {
            e.add(Restrictions.eq("r.course", course));
        }
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        return criteria.list();
    }

    @Override
    public Integer getNumberofRewrites(Registration registration) {
        return ((Long) entityManager.createQuery("SELECT COUNT(e) from ExamRegistration e where e.registration=:registration and e.reversedResult = false").setParameter("registration", registration).getSingleResult()).intValue();
    }

    @Override
    public Integer getTotalNumberSchool(ExamSetting examSetting, Course course) {
        return ((Long) entityManager.createQuery("SELECT COUNT(distinct e.institution) from ExamRegistration e where e.examSetting=:examSetting and e.registration.course=:course").setParameter("examSetting", examSetting).setParameter("course", course).getSingleResult()).intValue();
    }

    @Override
    public Boolean getExamRegistration(String candidateNumber) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ExamRegistration.class);
        criteria.add(Restrictions.eq("candidateNumber", candidateNumber));
        ExamRegistration e = (ExamRegistration) criteria.uniqueResult();
        if (e == null) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }
    }

    @Override
    public Boolean getCheckExamRegistration(ExamSetting examSetting, Registration registration) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ExamRegistration.class);
        criteria.add(Restrictions.eq("examSetting", examSetting));
        criteria.add(Restrictions.eq("registration", registration));
        ExamRegistration e = (ExamRegistration) criteria.uniqueResult();
        if (e == null) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }
    }

    @Override
    public ExamRegistration getExamRegistration(ExamSetting examSetting, Registrant registrant) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ExamRegistration.class);
        criteria.add(Restrictions.eq("examSetting", examSetting));
        criteria.createAlias("registration", "r");
        criteria.add(Restrictions.eq("r.registrant", registrant));
        return (ExamRegistration) criteria.uniqueResult();
    }

    @Override
    public List<ExamRegistration> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    //code will check if person has fully paid examination fees
    private Criteria getFullyPaidExamRegistration(Criteria criteria) {
        criteria.add(Restrictions.eq("remainingBalance", BigDecimal.ZERO));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria;
    }

    //code will check if person has not full paid examination fees  
    private Criteria getUnderPaidExamRegistration(Criteria criteria) {
        criteria.add(Restrictions.gt("remainingBalance", BigDecimal.ZERO));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria;
    }
}
