package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.RequiredParameter;

/**
 * Created by tndangana on 2/13/17.
 */
public interface RequiredParameterService extends IGenericService<RequiredParameter> {

    public RequiredParameter get();

}
