/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain.reports;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author tdhlakama
 */
public class ItemCase implements Serializable {

    private String name;
    
    private Date dateAwarded;

    public ItemCase() {
    }

    public ItemCase(String name) {
        this.name = name;
    }
    
    public Date getDateAwarded() {
        return dateAwarded;
    }

    public void setDateAwarded(Date dateAwarded) {
        this.dateAwarded = dateAwarded;
    }

    
    public String getName() {
        if (name != null) {
            return name.toUpperCase();
        }
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ItemCase other = (ItemCase) obj;
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        return true;
    }
    
    
}
