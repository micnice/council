/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.models;

import java.util.List;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.City;
import zw.co.hitrac.council.business.domain.Country;
import zw.co.hitrac.council.business.service.CityService;
import zw.co.hitrac.council.business.utils.HrisComparator;

/**
 *
 * @author kelvin
 */
public class CityListModel extends LoadableDetachableModel<List<City>> {

    private final CityService cityService;
    private Country country;
    private HrisComparator hrisComparator = new HrisComparator();

    public CityListModel(CityService cityService) {
        this.cityService = cityService;
    }

    public CityListModel(CityService cityService, Country country) {
        this.cityService = cityService;
        this.country = country;
    }

    @Override
    protected List<City> load() {
        if (country != null) {
            return (List<City>) hrisComparator.sort(cityService.getCities(country));
        } else {
            return (List<City>) hrisComparator.sort(cityService.findAll());
        }
    }
}
