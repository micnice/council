/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.examinations;

import java.util.List;

import zw.co.hitrac.council.business.dao.IGenericDAO;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.business.domain.examinations.ExamResult;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;

/**
 * @author tidza
 */
public interface ExamRegistrationDao extends IGenericDAO<ExamRegistration> {

    public List<ExamRegistration> getAllExamCandidates(ExamSetting examSetting, Course course, Institution institution, String candidateNumber, String idNumber, Boolean supplementaryExam, Boolean suppressed, Boolean closed, Boolean reversedResult, Boolean disqualified);

    public List<ExamRegistration> getExamRegistrations(Registrant registrant);

    public List<ExamRegistration> getExamRegistrations(Registration registration);

    public List<ExamResult> getExamResults(ExamSetting examSetting, Course course, Institution institution, String candidateNumber, String idNumber, Boolean supplementaryExam);

    public ExamRegistration get(ExamSetting examSetting, Course course, Registrant registrant);

    public Integer getTotalNumberOfCandidates(ExamSetting examSetting, Course course, Institution institution, String candidateNumber, String idNumber, Boolean supplementaryExam, Boolean suppressed, Boolean closed, Boolean reversedResult, Boolean disqualified);

    public Integer getNumberofRewrites(Registration registration);

    public Boolean getExamRegistration(String candidateNumber);

    public Boolean getCheckExamRegistration(ExamSetting examSetting, Registration registration);

    public ExamRegistration getExamRegistration(ExamSetting examSetting, Registrant registrant);

    public ExamRegistration getExaminationInformation(ExamSetting examSetting, Course course);

    public List<Institution> getInstitutiuons(ExamSetting examSetting, Course course);

    public Integer getTotalNumberSchool(ExamSetting examSetting, Course course);
}
