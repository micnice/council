package zw.co.hitrac.council.business.config;

import java.io.Serializable;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

/**
 * @author Daniel Nkhoma
 */
@Configuration
public class EmailConfiguration implements Serializable {

    @Configuration
    @Profile("dev")
    @PropertySource("classpath:zw/co/hitrac/council/business/config/mail_dev.properties")
    static class Development {
    }

}
