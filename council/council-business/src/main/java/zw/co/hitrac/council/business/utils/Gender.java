package zw.co.hitrac.council.business.utils;

import org.apache.commons.lang.StringUtils;

/**
 * @author Takunda Dhlakama
 */
public enum Gender {

    MALE("M"),
    FEMALE("F");

    private final String gender;

    private Gender(String gender) {

        this.gender = gender;
    }

    public static Gender getGenderFromString(String genderString) {

        if (genderString == null || genderString.isEmpty() || genderString.equals("")) {
            return null;
        } else if (genderString.equalsIgnoreCase("M")) {
                return Gender.MALE;
            } else if (genderString.equalsIgnoreCase("F")) {
                return Gender.FEMALE;
            } else {
                return Gender.MALE;
            }



    }

    public String getName() {

        return gender;
    }

    @Override
    public String toString() {

        return gender;
    }
}
