package zw.co.hitrac.council.web.pages.configure;

import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Tier;
import zw.co.hitrac.council.business.service.TierService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

/**
 *
 * @author Edward Zengeni
 */
public class TierListPage extends IAdministerDatabaseBasePage{
    
    @SpringBean
    private TierService tierService;

    public TierListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink",AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink",AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {

            @Override
            public void onClick() {
               setResponsePage(new TierEditPage(null));
            }
        });
        
        IModel<List<Tier>> model=new LoadableDetachableModel<List<Tier>>() {

            @Override
            protected List<Tier> load() {
               return tierService.findAll();
            }
        };
        
        PropertyListView<Tier> eachItem=new PropertyListView<Tier>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<Tier> item) {
                Link<Tier> viewLink=new Link<Tier>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                       setResponsePage(new TierViewPage(getModelObject().getId()));
                    }
                };
                if(item.getIndex()%2==0){
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };
        
        add(eachItem);
    }
    
    
    
}
