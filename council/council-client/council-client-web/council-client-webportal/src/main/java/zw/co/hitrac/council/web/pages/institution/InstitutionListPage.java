package zw.co.hitrac.council.web.pages.institution;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.service.InstitutionService;

//~--- JDK imports ------------------------------------------------------------
import java.util.List;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.utils.HrisComparator;

/**
 *
 * @author Charles Chigoriwa
 */
public class InstitutionListPage extends IInstitutionView {

    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    private HrisComparator hrisComparator = new HrisComparator();

    public InstitutionListPage() {
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionEditPage(null));
            }
        });
        Link<Void> viewLink = new Link<Void>("health") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionListPage(Boolean.TRUE, Boolean.FALSE, Boolean.FALSE));
            }
        };
        add(viewLink);
        viewLink.add(new Label("institutionType", generalParametersService.get().getHealthInstitutionType()));
        add(new Link("training") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionListPage(Boolean.FALSE, Boolean.TRUE, Boolean.FALSE));
            }
        });
        add(new Link("council") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionListPage(Boolean.FALSE, Boolean.FALSE, Boolean.TRUE));
            }
        });
        add(new Link("all") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionListPage());
            }
        });

        IModel<List<Institution>> model = new LoadableDetachableModel<List<Institution>>() {
            @Override
            protected List<Institution> load() {
                return (List<Institution>) hrisComparator.sort(institutionService.findAll());
            }
        };
        PropertyListView<Institution> eachItem = new PropertyListView<Institution>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<Institution> item) {
                Link<Institution> viewLink = new Link<Institution>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new InstitutionViewPage(getModelObject().getId()));
                    }
                };

                item.add(viewLink);
                viewLink.add(new Label("name"));
                item.add(new Label("institutionType"));
                item.add(new Label("institutionCategory"));
                item.add(new Label("retired"));
            }
        };

        add(eachItem);
    }

    public InstitutionListPage(final Boolean health, final Boolean training, final Boolean council) {
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionEditPage(null));
            }
        });
        Link<Void> viewLink = new Link<Void>("health") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionListPage(Boolean.TRUE, Boolean.FALSE, Boolean.FALSE));
            }
        };
        add(viewLink);
        viewLink.add(new Label("institutionType", generalParametersService.get().getHealthInstitutionType()));
        add(new Link("training") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionListPage(Boolean.FALSE, Boolean.TRUE, Boolean.TRUE));
            }
        });
        add(new Link("council") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionListPage(Boolean.FALSE, Boolean.FALSE, Boolean.TRUE));
            }
        });
        add(new Link("all") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionListPage());
            }
        });
       
        IModel<List<Institution>> model = new LoadableDetachableModel<List<Institution>>() {
            @Override
            protected List<Institution> load() {
                if (health) {
                    return (List<Institution>) hrisComparator.sort(institutionService.getInstitutions(generalParametersService.get().getHealthInstitutionType()));
                } else if (training) {
                    return (List<Institution>) hrisComparator.sort(institutionService.getAll(training, health, council));
                } else if (council) {
                    return (List<Institution>) hrisComparator.sort(institutionService.getAll(training, health, council));
                } else {
                    return (List<Institution>) hrisComparator.sort(institutionService.findAll());
                }
            }
        };
        PropertyListView<Institution> eachItem = new PropertyListView<Institution>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<Institution> item) {
                Link<Institution> viewLink = new Link<Institution>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new InstitutionViewPage(getModelObject().getId()));
                    }
                };

                item.add(viewLink);
                viewLink.add(new Label("name"));
                item.add(new Label("institutionType"));
                item.add(new Label("institutionCategory"));
                item.add(new Label("retired"));
            }
        };

        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
