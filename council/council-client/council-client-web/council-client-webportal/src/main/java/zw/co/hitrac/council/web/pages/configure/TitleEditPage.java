package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import java.util.Arrays;
import org.apache.wicket.markup.html.form.CheckBoxMultipleChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Title;
import zw.co.hitrac.council.business.service.TitleService;
import zw.co.hitrac.council.business.utils.Gender;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Charles Chigoriwa
 */
public class TitleEditPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private TitleService titleService;

    public TitleEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<Title>(new LoadableDetachableTitleModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));

        Form<Title> form = new Form<Title>("form", (IModel<Title>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                titleService.save(getModelObject());
                setResponsePage(new TitleViewPage(getModelObject().getId()));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
         form.add(new CheckBoxMultipleChoice("genders", Arrays.asList(Gender.values())));
        form.add(new BookmarkablePageLink<Void>("returnLink", TitleListPage.class));
        add(form);
    }

    private final class LoadableDetachableTitleModel extends LoadableDetachableModel<Title> {
        private Long id;

        public LoadableDetachableTitleModel(Long id) {
            this.id = id;
        }

        @Override
        protected Title load() {
            if (id == null) {
                return new Title();
            }

            return titleService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
