package zw.co.hitrac.council.business.dao;

import java.util.List;
import zw.co.hitrac.council.business.domain.RegistrantCPDCategory;
import zw.co.hitrac.council.business.domain.RegistrantCPDItem;

/**
 *
 * @author Constance Mabaso
 */
public interface RegistrantCPDItemDAO extends IGenericDAO<RegistrantCPDItem> {
    public List<RegistrantCPDItem> getRegistrantCPDItems (RegistrantCPDCategory registrantCPDCategory);
            
}
