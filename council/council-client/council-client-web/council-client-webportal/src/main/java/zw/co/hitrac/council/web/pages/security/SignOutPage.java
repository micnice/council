/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.security;

import org.apache.wicket.markup.html.WebPage;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * @author Clive Gurure
 */
public class SignOutPage extends WebPage {

  public SignOutPage() {
    //TODO : End the current request cycle to prevent access of previous pages
    SecurityContextHolder.clearContext();
    getSession().invalidateNow();
    setResponsePage(LoginPage.class);
  }
}
