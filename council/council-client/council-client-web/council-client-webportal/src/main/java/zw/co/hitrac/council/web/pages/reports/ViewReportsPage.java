package zw.co.hitrac.council.web.pages.reports;

//~--- non-JDK imports --------------------------------------------------------

import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.reports.InstitutionItem;
import zw.co.hitrac.council.business.domain.reports.RegisterStatistic;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.process.RenewalProcess;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.reports.*;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.examinations.ExaminationPaperPage;
import zw.co.hitrac.council.web.pages.examinations.ExaminationPassRatePage;
import zw.co.hitrac.council.web.pages.examinations.ExaminationTopCandidatestPage;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;
import zw.co.hitrac.council.web.pages.examinations.ExaminationYearSummaryPage;

public class ViewReportsPage extends TemplatePage {

    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private RegistrantQualificationService registrantQualificationService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private RegistrationService registrationService;
    @SpringBean
    private CourseService courseService;
    @SpringBean
    private DataSource dataSource;
    private HrisComparator hrisComparator = new HrisComparator();
    @SpringBean
    private ProvinceService provinceService;
    @SpringBean
    private DistrictService districtService;
    @SpringBean
    private CityService cityService;
    @SpringBean
    private InstitutionTypeService institutionTypeService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private InstitutionManagerService institutionManagerService;
    @SpringBean
    private PivotDataService pivotDataService;
    @SpringBean
    private RegistrantActivityService registrantActivityService;
    @SpringBean
    private RenewalProcess renewalProcess;
    @SpringBean
    private CouncilDurationService councilDurationService;
    @SpringBean
    private RegistrationProcess registrationProcess;

    public ViewReportsPage() {

        menuPanelManager.getViewReportsPanel().setTopMenuCurrent(true);

        final InstitutionType healthInstitutionType;

        healthInstitutionType = generalParametersService.get().getHealthInstitutionType(); // Council Health Institution

        final String healthInstitutionTypeName = healthInstitutionType != null ? healthInstitutionType.getName() : "Set Health Institution Type ";

        add(new BookmarkablePageLink<Void>("registrantReportLink", RegistrantReportPage.class));
        add(new BookmarkablePageLink<Void>("registrantStatusReportPage", RegistrantStatusReportPage.class));
        add(new BookmarkablePageLink<Void>("disciplinaryReportPage", DisciplinaryReportPage.class));

        add(new Link("disciplinarySummaryByMisconductTypeReportPage") {

            @Override
            public void onClick() {

                setResponsePage(new DisciplinarySummaryByMisconductTypeReportPage(getPageReference()));
            }
        });

        add(new BookmarkablePageLink<Void>("registrantAgePage", RegistrantAgePage.class));
        add(new BookmarkablePageLink<Void>("registrantQualificationReportPage", RegistrantQualificationReportPage.class));
        add(new BookmarkablePageLink<Void>("certificateTypeReportPage", CertificateTypeReportPage.class));
        add(new BookmarkablePageLink<Void>("registerTypeReportPage", RegisterTypeReportPage.class));
        add(new BookmarkablePageLink<Void>("applicationStatusReportPage", ApplicationStatusReportPage.class));
        add(new BookmarkablePageLink<Void>("employmentReportPage", EmploymentReportPage.class));
        //duration reports
        add(new Link("reportDurationPage") {

            @Override
            public void onClick() {
                setResponsePage(new ReportDurationPage(getPageReference()));
            }
        });

        add(new BookmarkablePageLink<Void>("registrantsByCourseAndDurationPage", RegistrantsByCourseAndDurationPage.class));
        add(new BookmarkablePageLink<Void>("registrantsByQualificationAndDurationPage", RegistrantsByQualificationAndDurationPage.class));
        add(new BookmarkablePageLink<Void>("webRegisterExportPage", WebRegisterExportReportPage.class));
        add(new Link("applicationsByYearApprovedPage") {

            @Override
            public void onClick() {

                setResponsePage(new ApplicationsByYear(getPageReference()));
            }
        });

        add(new Link("annualReminderPage") {

            @Override
            public void onClick() {

                setResponsePage(new AnnualReminderPage(getPageReference()));
            }
        });

        add(new Link("registrantsWithConditionsPage") {

            @Override
            public void onClick() {

                setResponsePage(new RegistrantsWithConditionsPage(getPageReference()));
            }
        });

        add(new Link("employmentByCourseAndDurationPage") {

            @Override
            public void onClick() {

                setResponsePage(new EmploymentByCourseAndDurationPage(getPageReference()));
            }
        });

        add(new Link("employmentTypeByDisciplineTotalSummaryReportPage") {

            @Override
            public void onClick() {

                setResponsePage(new EmploymentTypeByDisciplineTotalSummaryReportPage(getPageReference()));
            }
        });

        //Examinations
        add(new BookmarkablePageLink<Void>("examinationPaperPage", ExaminationPaperPage.class));
        add(new BookmarkablePageLink<Void>("examinationPassRatePage", ExaminationPassRatePage.class));
        add(new BookmarkablePageLink<Void>("examinationTopCandidatestPage", ExaminationTopCandidatestPage.class));
        add(new BookmarkablePageLink<Void>("examinationYearSummaryPage", ExaminationYearSummaryPage.class));

        //contact Details
        add(new BookmarkablePageLink<Void>("registrantAddressReportPage", RegistrantAddressReportPage.class));
        add(new BookmarkablePageLink<Void>("registrantContactReportPage", RegistrantContactReportPage.class));

        add(new BookmarkablePageLink<Void>("courseMappedCourseReport", CourseMappedCourseReport.class));
        add(new BookmarkablePageLink<Void>("qualificationMappedQualificationReport", QualificationMappedQualificationReport.class));

        add(new Link("statisticReport") {

            @Override
            public void onClick() {

                List<RegisterStatistic> registerStatistics = new ArrayList<RegisterStatistic>();
                for (Qualification qualification : registrantQualificationService.getDistinctQualificationsInRegistrantQualification()) {
                    RegisterStatistic registerStatistic = new RegisterStatistic();
                    registerStatistic.setQualification(qualification);
                    registerStatistics.add(registerStatistic);
                }

                for (RegisterStatistic registerStatistic : registerStatistics) {
                    registerStatistic.setNumberOfRegistrants(registrantQualificationService.getRegistrantQualifications(registerStatistic.getQualification()));
                }

                registerStatistics = (List<RegisterStatistic>) hrisComparator.sortRegisterStaticQualification(registerStatistics);

                try {
                    RegisterStatisticsReport registerStatisticsReport = new RegisterStatisticsReport();
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource
                            = ReportResourceUtils.getReportResource(registerStatisticsReport, contentType, registerStatistics,
                            new HashMap<String, Object>());
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

        add(new Link("showRegistersSummaryReportByCourse") {

            @Override
            public void onClick() {

                try {

                    boolean isReportByQualification = false;
                    List<RegisterStatistic> registerStatistics = generateRegisterStats(isReportByQualification);
                    registerStatistics = (List<RegisterStatistic>) hrisComparator.sortRegisterStaticCourse(registerStatistics);

                    Map parameters = new HashMap();
                    parameters.put("comment", "Summary Register for all Disciplines - Ever Registered");

                    DisciplineReport disciplineReport = new DisciplineReport();
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource
                            = ReportResourceUtils.getReportResource(disciplineReport, contentType, registerStatistics,
                            parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

        add(new Link("showRegistersSummaryReportByQualification") {

            @Override
            public void onClick() {

                boolean isReportByQualification = true;
                List<RegisterStatistic> registerStatistics = generateRegisterStats(isReportByQualification);
                registerStatistics = (List<RegisterStatistic>) hrisComparator.sortRegisterStaticCourse(registerStatistics);

                try {
                    Map parameters = new HashMap();
                    parameters.put("comment", "Summary Register for all Disciplines - Ever Registered");

                    DisciplineReport disciplineReport = new DisciplineReport();
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource
                            = ReportResourceUtils.getReportResource(disciplineReport, contentType, registerStatistics,
                            parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

        add(new Link("incorrectIdNumbers") {

            @Override
            public void onClick() {

                setResponsePage(new RegistrantsWithInvalidIdentificationNumbersPage(getPageReference()));

            }
        });

        add(new Link("incorrectDateOfBirth") {

            @Override
            public void onClick() {

                Map parameters = new HashMap();
                RegistrantDataDBReport registrantDataDBReport = new RegistrantDataDBReport();
                try {
                    parameters.put("comment", "In Correct Date of Birth(01/01/1900)");

                    String list = "";
                    for (RegistrantData d : registrantService.getRegistrantDateCorrections()) {
                        list += d.getId().toString() + ",";
                    }
                    if (list.isEmpty()) {
                        return;
                    }
                    list = list.substring(0, list.length() - 1);
                    parameters.put("list", list);
                    ContentType contentType = ContentType.PDF;
                    final Connection connection = dataSource.getConnection(); //DBConnect.getConnection();
                    ByteArrayResource resource = null;
                    try {
                        resource = ReportResourceUtils.getReportResource(registrantDataDBReport, contentType, connection, parameters);
                    } finally {
                        connection.close();
                    }
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                } catch (SQLException ex) {
                    ex.printStackTrace(System.out);
                }

            }
        });

        add(new Link("de-registeredList") {

            @Override
            public void onClick() {

                Map parameters = new HashMap();
                parameters.put("comment", "Deregistration Individuals as of " + DateUtil.getDate(new Date()));

                try {
                    RegistrantCommentDataReport registrantCommentDataReport = new RegistrantCommentDataReport();
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource
                            = ReportResourceUtils.getReportResource(registrantCommentDataReport, contentType, registrantActivityService.getRegistrantsByRegistrantActivityType(RegistrantActivityType.DEREGISTRATION), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

        add(new Link("deceasedList") {

            @Override
            public void onClick() {

                Map parameters = new HashMap();
                parameters.put("comment", "Deceased Individuals as of " + DateUtil.getDate(new Date()));

                try {

                    RegistrantDataReport registrantDataReport = new RegistrantDataReport();
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource
                            = ReportResourceUtils.getReportResource(registrantDataReport, contentType, registrantService.getDeceasedRegistrants(), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

        add(new Link("retiredList") {

            @Override
            public void onClick() {

                Map parameters = new HashMap();
                parameters.put("comment", "Retired Individuals as of " + DateUtil.getDate(new Date()));

                try {
                    RegistrantDataReport registrantDataReport = new RegistrantDataReport();
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource
                            = ReportResourceUtils.getReportResource(registrantDataReport, contentType, registrantService.getRetiredRegistrants(), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

        add(new Link("voidedList") {

            @Override
            public void onClick() {

                Map parameters = new HashMap();
                parameters.put("comment", "Take of List as of " + DateUtil.getDate(new Date()));

                try {
                    RegistrantCommentDataReport registrantCommentDataReport = new RegistrantCommentDataReport();
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource
                            = ReportResourceUtils.getReportResource(registrantCommentDataReport, contentType, registrantActivityService.getRegistrantsByRegistrantActivityType(RegistrantActivityType.VIODED), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

        add(new Link("listOfIndividualsWhoHaveNeverRenewed") {

            @Override
            public void onClick() {

                Map parameters = new HashMap();

                List<Registrant> registrants = registrantActivityService.getRegistrantsWhoNeverRenewed();
                registrants = registrants.stream().filter(registrant -> !registrant.getVoided()).collect(Collectors.toList());
                RegistrantData registrantData = new RegistrantData();

                try {

                    parameters.put("comment", "Individuals who have never renewed and are still registered");

                    String list = "";
                    for (Registrant d : registrants) {
                        list += d.getId().toString() + ",";
                    }
                    if (list.isEmpty()) {
                        return;
                    }
                    list = list.substring(0, list.length() - 1);
                    parameters.put("list", list);
                    ContentType contentType = ContentType.PDF;
                    final Connection connection = dataSource.getConnection(); //DBConnect.getConnection();
                    ByteArrayResource resource = null;
                    try {
                        RegistrantDataDBReport registrantDataDBReport = new RegistrantDataDBReport();
                        resource = ReportResourceUtils.getReportResource(registrantDataDBReport, contentType, connection, parameters);
                    } finally {
                        connection.close();
                    }
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                } catch (SQLException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

        add(new Link("listOfIndividualsWhoHaveNeverRenewedCSV") {

            @Override
            public void onClick() {

                List<Registration> registrationList = new ArrayList<>();
                Map parameters = new HashMap();

                parameters.put("comment", "Individuals who have never renewed and are still registered");

                Thread t = new Thread() {
                    @Override
                    public synchronized void start()

                    {
                        List<Registrant> registrants = registrantActivityService.getRegistrantsWhoNeverRenewed();
                        registrants = registrants.stream().filter(registrant -> !registrant.getVoided()).collect(Collectors.toList());
                        for (Registrant registrant : registrants) {
                            Registration registration = registrationProcess.activeRegisterNotStudentRegister(registrant);
                            if (registration != null)
                                registrationList.add(registration);
                        }
                    }

                };
                t.start();

                try {

                    RegistrationReport report = new RegistrationReport();
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource
                            = ReportResourceUtils.getReportResource(report, contentType, HrisComparator.sortCourseAndNameSurname(registrationList), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }


            }
        });


        add(new Link("InstitutionManagerReport") {

            @Override
            public void onClick() {

                Map parameters = new HashMap();
                parameters.put("comment", "Register of Institution Managers by StartDate And EndDate");

                try {

                    List<InstitutionManager> InstitutionManagerDataList = institutionManagerService.findAll();
                    DefaultReport report = new RegistrantsWhoManageInstitutionsReport();

                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource
                            = ReportResourceUtils.getReportResource(report, contentType, InstitutionManagerDataList, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

        add(new Link("institutionProvinceSummaryReport") {

            @Override
            public void onClick() {

                Map parameters = new HashMap();
                parameters.put("comment", healthInstitutionTypeName + " Province Summary");

                try {
                    List<InstitutionItem> items = new ArrayList<InstitutionItem>();

                    for (Province province : provinceService.findAll()) {
                        InstitutionItem item = new InstitutionItem();
                        item.setProvince(province);
                        item.setTotal(institutionService.getTotalCount(province, null, null, generalParametersService.get().getHealthInstitutionType(), null));
                        if (item.getTotal() != 0L) {
                            items.add(item);
                        }
                    }
                    InstitutionItemReport report = new InstitutionItemReport();

                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource
                            = ReportResourceUtils.getReportResource(report, contentType, items, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

        add(new Link("institutionDistrictSummaryReport") {

            @Override
            public void onClick() {

                Map parameters = new HashMap();
                parameters.put("comment", healthInstitutionTypeName + " District Summary");

                try {
                    List<InstitutionItem> items = new ArrayList<InstitutionItem>();

                    for (District district : districtService.findAll()) {
                        InstitutionItem item = new InstitutionItem();
                        item.setDistrict(district);
                        item.setTotal(institutionService.getTotalCount(null, district, null, healthInstitutionType, null));
                        if (item.getTotal() != 0L) {
                            items.add(item);
                        }
                    }
                    InstitutionItemReport report = new InstitutionItemReport();

                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource
                            = ReportResourceUtils.getReportResource(report, contentType, items, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

        add(new Link("institutionCitySummaryReport") {

            @Override
            public void onClick() {

                Map parameters = new HashMap();
                parameters.put("comment", healthInstitutionTypeName + " City Summary");

                try {
                    List<InstitutionItem> items = new ArrayList<InstitutionItem>();

                    for (City city : cityService.findAll()) {
                        InstitutionItem item = new InstitutionItem();
                        item.setCity(city);
                        item.setTotal(institutionService.getTotalCount(null, null, city, healthInstitutionType, null));
                        if (item.getTotal() != 0L) {
                            items.add(item);
                        }
                    }
                    InstitutionItemReport report = new InstitutionItemReport();

                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource
                            = ReportResourceUtils.getReportResource(report, contentType, items, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

        add(new Link("institutionTypeSummaryReport") {

            @Override
            public void onClick() {

                Map parameters = new HashMap();
                parameters.put("comment", "Institutions Type Summary");

                try {
                    List<InstitutionItem> items = new ArrayList<InstitutionItem>();

                    for (InstitutionType type : institutionTypeService.findAll()) {
                        InstitutionItem item = new InstitutionItem();
                        item.setInstitutionType(type);
                        item.setTotal(institutionService.getTotalCount(null, null, null, type, null));
                        if (item.getTotal() != 0L) {
                            items.add(item);
                        }
                    }
                    InstitutionItemReport report = new InstitutionItemReport();

                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource
                            = ReportResourceUtils.getReportResource(report, contentType, items, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

        add(new Link("institutionCategorySummaryReport") {

            @Override
            public void onClick() {

                Map parameters = new HashMap();
                parameters.put("comment", "Institution Category Summary");

                try {
                    List<InstitutionItem> items = new ArrayList<InstitutionItem>();

                    for (InstitutionCategory category : InstitutionCategory.values()) {
                        InstitutionItem item = new InstitutionItem();
                        item.setInstitutionCategory(category);
                        item.setTotal(institutionService.getTotalCount(null, null, null, null, category));
                        if (item.getTotal() != 0L) {
                            items.add(item);
                        }
                    }
                    InstitutionItemReport report = new InstitutionItemReport();

                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource
                            = ReportResourceUtils.getReportResource(report, contentType, items, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

        add(new Link("retiredInstitutionsReport") {

            @Override
            public void onClick() {

                Map parameters = new HashMap();
                parameters.put("comment", "Retired Institutions");

                try {
                    List<Institution> items = institutionService.getInstitutions(null, healthInstitutionType, null, Boolean.TRUE);
                    Collections.sort(items);
                    InstitutionReport report = new InstitutionReport();
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource
                            = ReportResourceUtils.getReportResource(report, contentType, items, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }

            @Override
            public boolean isVisible() {
                return healthInstitutionType != null;
            }
        });

        add(new Link("allIinstitutions") {

            @Override
            public void onClick() {

                Map parameters = new HashMap();
                parameters.put("comment", "All " + healthInstitutionTypeName + " Institutions.");

                try {
                    List<Institution> items = institutionService.getInstitutions(null, healthInstitutionType, null, Boolean.FALSE);
                    Collections.sort(items);

                    InstitutionReport report = new InstitutionReport();

                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource
                            = ReportResourceUtils.getReportResource(report, contentType, items, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }

            @Override
            public boolean isVisible() {
                return healthInstitutionType != null;
            }
        });

        add(new Link("allRegisteredInstitutions") {

            @Override
            public void onClick() {

                Map parameters = new HashMap();
                parameters.put("comment", "All Current Registered " + healthInstitutionTypeName + " Institutions.");

                try {
                    List<Registration> items = registrationService.getRegisteredInstitutionsRegistrations(false);

                    RegisteredInstitutionReport report = new RegisteredInstitutionReport();

                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource
                            = ReportResourceUtils.getReportResource(report, contentType, items, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }

            @Override
            public boolean isVisible() {
                return healthInstitutionType != null;
            }
        });


        add(new Link("registrationPivotData") {

            @Override
            public void onClick() {

                byte[] bytes = pivotDataService.exportRegistrations();
                ReportResourceUtils.respond(bytes, "RegistrationPivotData.csv", getRequestCycle(), ContentType.CSV);
            }

        });

        add(new Link("renewalPivotData") {

            @Override
            public void onClick() {
                byte[] bytes = pivotDataService.exportRenewals(registrantActivityService.findAll());
                ReportResourceUtils.respond(bytes, "RenewalPivotData.csv", getRequestCycle(), ContentType.EXCEL);


            }

        });

        add(new Link("allClosedRegisteredInstitutions") {

            @Override
            public void onClick() {

                Map parameters = new HashMap();
                parameters.put("comment", "All Closed Registered " + healthInstitutionTypeName + " Institutions.");

                try {
                    List<Registration> items = registrationService.getRegisteredInstitutionsRegistrations(true);

                    RegisteredInstitutionReport report = new RegisteredInstitutionReport();

                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource
                            = ReportResourceUtils.getReportResource(report, contentType, items, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }

            @Override
            public boolean isVisible() {
                return healthInstitutionType != null;
            }
        });


    }

    private List<RegisterStatistic> generateRegisterStats(boolean reportIsByQualification) {

        List<Register> neededRegisters = new ArrayList<Register>();
        List<RegisterStatistic> registerStatistics = new ArrayList<RegisterStatistic>();

        GeneralParameters generalParameters = generalParametersService.get();

        Register mainRegister = generalParameters.getMainRegister();
        Register provisionalRegister = generalParameters.getProvisionalRegister();
        Register studentRegister = generalParameters.getStudentRegister();
        Register internRegister = generalParameters.getInternRegister();
        Register maintenanceRegister = generalParameters.getMaintenanceRegister();
        Register specialistRegister = generalParameters.getSpecialistRegister();

        if (mainRegister != null) {
            neededRegisters.add(mainRegister);
        }

        if (provisionalRegister != null) {
            neededRegisters.add(provisionalRegister);
        }

        if (studentRegister != null) {
            neededRegisters.add(studentRegister);
        }

        if (internRegister != null) {
            neededRegisters.add(internRegister);
        }

        if (maintenanceRegister != null) {
            neededRegisters.add(maintenanceRegister);
        }

        if (specialistRegister != null) {
            neededRegisters.add(specialistRegister);
        }

        int count = 0;

        for (Course course : courseService.findAll()) {

            for (Register register : neededRegisters) {

                RegisterStatistic registerStatistic = new RegisterStatistic();
                registerStatistic.setCourse(course);
                registerStatistic.setRegister(register);

                if (!reportIsByQualification
                        || (register.equals(studentRegister)
                        && !course.getName().equals("BACHELOR OF SCIENCE DEGREE")
                        && !course.getName().equals("MASTERS OF SCIENCE"))) {

                    count = registrationService.getActiveRegistrations(course, register);
                } else {

                    count = registrantQualificationService.getTotalPerQualificationMappedCourseAndRegister(course, register);
                }

                registerStatistic.setNumberOfRegistrants(count);

                if (registerStatistic.getNumberOfRegistrants() != null && registerStatistic.getNumberOfRegistrants() > 0) {
                    registerStatistics.add(registerStatistic);
                }
            }
        }

        return registerStatistics;
    }
}
