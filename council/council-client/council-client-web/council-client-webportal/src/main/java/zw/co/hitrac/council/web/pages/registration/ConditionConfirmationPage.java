package zw.co.hitrac.council.web.pages.registration;

//~--- non-JDK imports --------------------------------------------------------
import com.googlecode.wicket.jquery.core.JQueryBehavior;
import java.util.Date;
import java.util.List;
import org.apache.wicket.markup.html.form.CheckBoxMultipleChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.Conditions;

import zw.co.hitrac.council.web.pages.TemplatePage;

//~--- JDK imports ------------------------------------------------------------

import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantCondition;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.service.ConditionService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrantConditionService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.models.ConditionsListModel;
import zw.co.hitrac.council.web.pages.accounts.documents.DebtComponentsPage;
import zw.co.hitrac.council.web.pages.application.ApplicationViewPage;
import zw.co.hitrac.council.web.pages.certification.CertificationPage;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;

/**
 *
 * @author Takunda Dhlakama
 * @author Michael Matiashe
 */
public class ConditionConfirmationPage extends TemplatePage {

    @SpringBean
    RegistrantService registrantService;
    @SpringBean
    private DebtComponentService debtComponentService;
    @SpringBean
    private ConditionService conditionService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private RegistrantConditionService registrantConditionService;

    public ConditionConfirmationPage(final Long id) {
        this.add(new JQueryBehavior("#tabs", "tabs"));
        final CompoundPropertyModel<Registrant> registrantModel = new CompoundPropertyModel<Registrant>(new DetachableRegistrantModel(id, registrantService));
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(id);
        add(new RegistrantPanel("registrantPanel", id));

        setDefaultModel(new CompoundPropertyModel<Registrant>(new LoadableDetachableRegistrantModel(id)));

        Form<Registrant> form = new Form<Registrant>("form", (IModel<Registrant>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    Registrant registrant = registrantService.save(getModelObject());
                    for (Conditions c : registrant.getConditions()) {
                        if (registrantConditionService.getCondition(registrant, c).isEmpty()) {
                            RegistrantCondition registrantCondition = new RegistrantCondition();
                            registrantCondition.setCondition(c);
                            registrantCondition.setRegistrant(registrant);
                            registrantCondition.setDateCreated(new Date());
                            registrantCondition.setStatus(Boolean.TRUE);
                            registrantConditionService.save(registrantCondition);
                        }
                    }

                    List<DebtComponent> debtComponents = debtComponentService.getDebtComponents(registrantModel.getObject().getCustomerAccount());
                    if (debtComponents.isEmpty()) {
                        setResponsePage(new RegistrantViewPage(id));
                    } else {
                        setResponsePage(new DebtComponentsPage(id));
                    }
                } catch (CouncilException e) {
                    error(e.getMessage());
                }
            }
        };
        
        form.add(new CheckBoxMultipleChoice("conditions", new ConditionsListModel(conditionService)));
        add(new FeedbackPanel("feedback"));
        form.add(new Link<Void>("certificationPage") {
            @Override
            public void onClick() {
                setResponsePage(new CertificationPage(id));
            }

            @Override
            protected void onConfigure() {
                if (!generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisibilityAllowed(Boolean.FALSE);
                }
            }
        });
        form.add(new Link<Registrant>("next") {
            @Override
            public void onClick() {
                List<DebtComponent> debtComponents = debtComponentService.getDebtComponents(registrantModel.getObject().getCustomerAccount());
                if (debtComponents.isEmpty()) {
                    setResponsePage(new RegistrantViewPage(id));
                } else {
                    setResponsePage(new DebtComponentsPage(id));
                }
            }
        });
        add(form);

    }

    public ConditionConfirmationPage(final Application application) {
        this.add(new JQueryBehavior("#tabs", "tabs"));
        final Long id = application.getRegistrant().getId();
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(id);
        add(new RegistrantPanel("registrantPanel", id));

        setDefaultModel(new CompoundPropertyModel<Registrant>(new LoadableDetachableRegistrantModel(id)));

        Form<Registrant> form = new Form<Registrant>("form", (IModel<Registrant>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    Registrant registrant = registrantService.save(getModelObject());
                    for (Conditions c : registrant.getConditions()) {
                        if (registrantConditionService.getCondition(registrant, c).isEmpty()) {
                            RegistrantCondition registrantCondition = new RegistrantCondition();
                            registrantCondition.setCondition(c);
                            registrantCondition.setRegistrant(registrant);
                            registrantCondition.setDateCreated(new Date());
                            registrantCondition.setStatus(Boolean.TRUE);
                            registrantConditionService.save(registrantCondition);
                        }
                    }
                    setResponsePage(new ApplicationViewPage(application.getId()));
                } catch (CouncilException e) {
                    error(e.getMessage());
                }
            }
        };

        form.add(new CheckBoxMultipleChoice("conditions", new ConditionsListModel(conditionService)));
        add(new FeedbackPanel("feedback"));
        form.add(new Link<Void>("certificationPage") {
            @Override
            public void onClick() {
                setResponsePage(new ApplicationViewPage(application.getId()));
            }
        });
        form.add(new Link<Registrant>("next") {
            @Override
            public void onClick() {
                setResponsePage(new ApplicationViewPage(application.getId()));

            }

            @Override
            protected void onConfigure() {
                setVisibilityAllowed(Boolean.FALSE);
            }
        });
        add(form);

    }

    private final class LoadableDetachableRegistrantModel extends LoadableDetachableModel<Registrant> {

        private Long id;

        public LoadableDetachableRegistrantModel(Long id) {
            this.id = id;
        }

        @Override
        protected Registrant load() {
            Registrant registrant = null;

            if (id == null) {
                registrant = new Registrant();
            } else {
                registrant = registrantService.get(id);
            }

            return registrant;
        }
    }
}

//~ Formatted by Jindent --- http://www.jindent.com
