/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao;

import java.util.List;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.CourseGroup;
import zw.co.hitrac.council.business.domain.CourseType;
import zw.co.hitrac.council.business.domain.ModulePaper;
import zw.co.hitrac.council.business.domain.Qualification;
import zw.co.hitrac.council.business.domain.Tier;

/**
 *
 * @author tdhlakama
 */
public interface CourseDAO extends IGenericDAO<Course> {

    public List<Course> getCourses(Boolean basic, CourseGroup courseGroup, Qualification qualification, CourseType courseType, Tier tier);
    /**
     *
     * Get Number total number of Module Papers
     *
     * @param course
     * @return
     */
    public Long getNumberOfModulePapers(Course course);

    public Course findByPrefixName(String prefixName);

}
