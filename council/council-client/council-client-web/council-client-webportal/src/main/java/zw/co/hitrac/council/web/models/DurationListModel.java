package zw.co.hitrac.council.web.models;

import java.util.List;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.DurationService;

/**
 *
 * @author Charles Chigoriwa
 */
public class DurationListModel extends LoadableDetachableModel<List<Duration>> {
    
    private final DurationService durationService;

    public DurationListModel(DurationService durationService) {
        this.durationService = durationService;
    }
    
    

    @Override
    protected List<Duration> load() {
      return this.durationService.findAll();
    }
    
}
