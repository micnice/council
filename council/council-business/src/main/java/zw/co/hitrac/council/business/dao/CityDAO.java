/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao;

import java.util.List;
import zw.co.hitrac.council.business.domain.City;
import zw.co.hitrac.council.business.domain.Country;

/**
 *
 * @author tdhlakama
 */
public interface CityDAO extends IGenericDAO<City> {
    
    public List<City> getCities(Country country);
    
}
