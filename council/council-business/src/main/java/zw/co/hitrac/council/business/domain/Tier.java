package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 *
 * @author Edward Zengeni
 */
@Entity
@Table(name="tier")
@Audited
@XmlRootElement
public class Tier extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
