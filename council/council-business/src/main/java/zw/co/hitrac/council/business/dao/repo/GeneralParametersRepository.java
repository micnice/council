package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.GeneralParameters;
import zw.co.hitrac.council.business.domain.RequiredParameter;

/**
 *
 * @author Edward Zengeni
 */
public interface GeneralParametersRepository extends CrudRepository<GeneralParameters, Long> {

    public List<GeneralParameters> findAll();


}
