package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.InstitutionPracticeControlDAO;
import zw.co.hitrac.council.business.dao.repo.InstitutionPracticeControlRepository;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.InstitutionPracticeControl;

/**
 *
 * @author Constance Mabaso
 */
@Repository
public class InsitutionPracticeControlDAOImpl implements InstitutionPracticeControlDAO {

    @Autowired
    private InstitutionPracticeControlRepository institutionPracticeControlRepository;
    @PersistenceContext
    EntityManager entityManager;

    public InstitutionPracticeControl save(InstitutionPracticeControl institutionPracticeControl) {
        return institutionPracticeControlRepository.save(institutionPracticeControl);
    }

    public List<InstitutionPracticeControl> findAll() {
        return institutionPracticeControlRepository.findAll();
    }

    public InstitutionPracticeControl get(Long id) {
        return institutionPracticeControlRepository.findOne(id);
    }

    public void setInstitutionPracticeControlRepository(InstitutionPracticeControlRepository institutionPracticeControlRepository) {
        this.institutionPracticeControlRepository = institutionPracticeControlRepository;
    }

    public List<InstitutionPracticeControl> get(Application application) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(InstitutionPracticeControl.class);
        criteria.add(Restrictions.eq("application", application));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<InstitutionPracticeControl> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
