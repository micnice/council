/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.accounts.payments;

import org.apache.wicket.PageReference;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.domain.reports.PaymentDue;
import zw.co.hitrac.council.business.process.ProductFinder;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.CouncilDurationService;
import zw.co.hitrac.council.business.service.RegistrantActivityService;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.reports.PaymentDueReport;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.accounts.IAccountingPage;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Michael Matiashe
 */
public class IncomeExpectedPage extends IAccountingPage {

    private CouncilDuration councilDuration;

    @SpringBean
    private CouncilDurationService councilDurationService;
    @SpringBean
    private RegistrantActivityService registrantActivityService;
    @SpringBean
    private DataSource dataSource;
    @SpringBean
    private RegistrationProcess registrationProcess;
    @SpringBean
    ProductFinder productFinder;

    public IncomeExpectedPage(final PageReference pageReference) {

        menuPanelManager.getSearchPanel().setTopMenuCurrent(true);
        menuPanelManager.getSearchPanel().setSearchAdvanced(true);

        PropertyModel<CouncilDuration> councilDurationModel = new PropertyModel<CouncilDuration>(this, "councilDuration");
        Form<?> form = new Form("form");
        form.add(new DropDownChoice("councilDuration", councilDurationModel, councilDurationService.findAll()).setRequired(true).add(new ErrorBehavior()));
        add(form);

        form.add(new Link("returnLink") {
            public void onClick() {
                setResponsePage(pageReference.getPage());
            }
        });

        form.add(new Button("process") {

            @Override
            public void onSubmit() {

                Thread t = new Thread() {
                    @Override
                    public synchronized void start()

                    {
                        printReport();
                    }

                };
                t.start();


            }
        });

        add(new FeedbackPanel("feedback"));

    }

    private void printReport() {

        final Integer year = Integer.valueOf(councilDuration.getName()) + 1;

        Map parameters = new HashMap();
        parameters.put("comment", "Income Expected for period " + year);

        List<Registrant> registrants = registrantActivityService.getRegistrantsRenewedInCouncilDuration(councilDuration, null);
        registrants = registrants.stream().filter(registrant -> !registrant.getVoided() || !registrant.getDead()).collect(Collectors.toList());
        List<PaymentDue> paymentDues = new ArrayList<>();

        for (Registrant registrant : registrants) {
            PaymentDue paymentDue = new PaymentDue();
            paymentDue.setRegistration(registrationProcess.activeRegisterNotStudentRegister(registrant));
            paymentDue.setLastRenewal(councilDuration);
            paymentDue.setPeriodsDue(1);
            if (paymentDue.getRegistration() != null && paymentDue.getRegistration().getCourse() != null) {
                try {
                    Product renewalProduct = productFinder.searchAnnualProduct(paymentDue.getRegistration());
                    paymentDue.setTotalAmountForRenewal(calculateCost(1, renewalProduct.getProductPrice().getPrice()));
                    Product penaltyProduct = productFinder.searchPenaltyProduct(paymentDue.getRegistration());
                    paymentDue.setTotalAmountForPenalty(calculateCost(1, penaltyProduct.getProductPrice().getPrice()));
                    paymentDues.add(paymentDue);

                } catch (Exception e) {
                    System.out.print("-----------------------" + registrant.getFullname());
                    e.printStackTrace();
                }
            }
        }

        showPaymentDues(parameters, (List<PaymentDue>) HrisComparator.sortPaymentsDueByCourseAndNameSurname(paymentDues));
    }

    private void showPaymentDues(Map parameters, List<PaymentDue> paymentDueList) {

        PaymentDueReport paymentDueReport = new PaymentDueReport();
        try {

            ReportResourceUtils.processJavaBeanReport(paymentDueReport, paymentDueList, parameters);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public BigDecimal calculateCost(int itemQuantity, BigDecimal itemPrice) {
        BigDecimal itemCost = BigDecimal.ZERO;
        BigDecimal totalCost = BigDecimal.ZERO;

        itemCost = itemPrice.multiply(new BigDecimal(itemQuantity));
        totalCost = totalCost.add(itemCost);
        return totalCost;
    }

}