package zw.co.hitrac.council.web.pages.accounts;

import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.ProductGroup;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.accounts.ProductGroupService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Michael Matiashe
 */
public class ProductGroupEditPage extends IBankBookPage{
    
    @SpringBean
    private ProductGroupService paymentTypeService;
    @SpringBean
    private CourseService courseService;

    public ProductGroupEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<ProductGroup>(new LoadableDetachableProductGroupModel(id)));
        Form<ProductGroup> form=new Form<ProductGroup>("form",(IModel<ProductGroup>)getDefaultModel()) {
            @Override
            public void onSubmit(){
                paymentTypeService.save(getModelObject());
                setResponsePage(new ProductGroupViewPage(getModelObject().getId()));
            }
        };
        
        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior())); 
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
                form.add(new DropDownChoice("course", courseService.findAll()));
        form.add(new BookmarkablePageLink<Void>("returnLink",ProductGroupListPage.class));
        add(form);
    }
    
    
    private final class LoadableDetachableProductGroupModel extends LoadableDetachableModel<ProductGroup> {

        private Long id;

        public LoadableDetachableProductGroupModel(Long id) {
            this.id = id;
        }

        @Override
        protected ProductGroup load() {
            if(id==null){
                return new ProductGroup();
            }
            return paymentTypeService.get(id);
        }
    }
    
    
}
