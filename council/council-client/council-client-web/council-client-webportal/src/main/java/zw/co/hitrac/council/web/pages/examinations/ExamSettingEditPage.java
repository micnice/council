package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.service.examinations.ExamPeriodService;
import zw.co.hitrac.council.business.service.examinations.ExamSettingService;
import zw.co.hitrac.council.business.service.examinations.ExamYearService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.configure.AdministerDatabasePage;
import zw.co.hitrac.council.web.utility.CustomDateTextField;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

/**
 *
 * @author Constance Mabaso
 */
public class ExamSettingEditPage extends IExaminationsPage {
    @SpringBean
    private ExamSettingService examSettingService;
    @SpringBean
    private ExamYearService    examYearService;
    @SpringBean
    private ExamPeriodService  examPeriodService;

    public ExamSettingEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<ExamSetting>(new LoadableDetachableExamSettingModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));

        Form<ExamSetting> form = new Form<ExamSetting>("form", (IModel<ExamSetting>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                examSettingService.save(getModelObject());
                setResponsePage(new ExamSettingViewPage(getModelObject().getId()));
            }
        };

        form.add(
            new  CustomDateTextField("startDate").setRequired(true).add(new ErrorBehavior()).add(
                DatePickerUtil.getDatePicker()));
        form.add(new  CustomDateTextField("endDate").add(new ErrorBehavior()).add(DatePickerUtil.getDatePicker()));
        form.add(
            new  CustomDateTextField("markingStartDate").add(new ErrorBehavior()).add(DatePickerUtil.getDatePicker()));
        form.add(new  CustomDateTextField("markingEndDate").add(new ErrorBehavior()).add(DatePickerUtil.getDatePicker()));
        form.add(new DropDownChoice("examYear", examYearService.findAll()).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("examPeriod", examPeriodService.findAll()).setRequired(true).add(new ErrorBehavior()));
        form.add(new CheckBox("active"));
        form.add(new BookmarkablePageLink<Void>("returnLink", ExamSettingListPage.class));
        add(form);
    }

    private final class LoadableDetachableExamSettingModel extends LoadableDetachableModel<ExamSetting> {
        private Long id;

        public LoadableDetachableExamSettingModel(Long id) {
            this.id = id;
        }

        @Override
        protected ExamSetting load() {
            if (id == null) {
                return new ExamSetting();
            }

            return examSettingService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
