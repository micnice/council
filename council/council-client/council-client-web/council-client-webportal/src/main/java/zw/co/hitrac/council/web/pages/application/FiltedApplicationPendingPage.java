/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.application;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.ApplicationPurpose;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.service.ApplicationService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author artwell
 */
public class FiltedApplicationPendingPage extends TemplatePage {

    @SpringBean
    private ApplicationService applicationService;
    @SpringBean
    private GeneralParametersService generalParametersService;

    public FiltedApplicationPendingPage() {

        menuPanelManager.getApplicationPanel().setTopMenuCurrent(true);
        menuPanelManager.getApplicationPanel().setApplicationPending(true);

        IModel<List<Application>> model = new LoadableDetachableModel<List<Application>>() {
            @Override
            protected List<Application> load() {
                if (!generalParametersService.get().getRegistrationByApplication() && !generalParametersService.get().getAllowPreRegistrantion()) {
                    return applicationService.getApplications(null, Application.APPLICATIONPENDING, null, null, null, Boolean.FALSE, Boolean.FALSE);
                }
                return applicationService.getFilteredPendingApplications();
            }
        };
        // Registrant List Data View Panel
        add(new UnApprovedApplicationsDataViewPanel("applicationPendingAlertPage", model));
        add(new FeedbackPanel("feedback"));
    }
}
