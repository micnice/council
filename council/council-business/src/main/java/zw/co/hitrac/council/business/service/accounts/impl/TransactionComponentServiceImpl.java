package zw.co.hitrac.council.business.service.accounts.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.accounts.TransactionComponentDAO;
import zw.co.hitrac.council.business.domain.accounts.TransactionComponent;
import zw.co.hitrac.council.business.service.accounts.TransactionComponentService;

import java.util.List;

/**
 *
 * @author Judge Muzinda
 * @author Constance Mabaso
 */
@Service
@Transactional
public class TransactionComponentServiceImpl implements TransactionComponentService {

    @Autowired
    private TransactionComponentDAO transactionDao;

    @Transactional
    public TransactionComponent save(TransactionComponent t) {
        return transactionDao.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<TransactionComponent> findAll() {
        return transactionDao.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public TransactionComponent get(Long id) {
        return transactionDao.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<TransactionComponent> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setTransactionDao(TransactionComponentDAO transactionDao) {

        this.transactionDao = transactionDao;
    }
}