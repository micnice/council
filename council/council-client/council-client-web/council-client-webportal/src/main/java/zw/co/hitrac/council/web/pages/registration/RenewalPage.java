package zw.co.hitrac.council.web.pages.registration;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.process.RenewalProcess;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.accounts.documents.DebtComponentsPage;
import zw.co.hitrac.council.business.process.PaymentProcess;

import java.util.List;
import java.util.Set;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.model.PropertyModel;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.accounts.PaymentDetails;
import zw.co.hitrac.council.business.domain.accounts.ReceiptItem;
import zw.co.hitrac.council.business.process.InvoiceProcess;
import zw.co.hitrac.council.business.process.PaymentProcess;
import zw.co.hitrac.council.business.service.CouncilDurationService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.service.accounts.ReceiptItemService;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.pages.accounts.documents.CarryForwardPage;
import zw.co.hitrac.council.web.pages.accounts.payments.PaymentConfirmationPage;
import zw.co.hitrac.council.web.pages.certification.CertificationPage;

/**
 * @author Takunda Dhlakama
 * @author Michael Matiashe
 */
public class RenewalPage extends TemplatePage {

    @SpringBean
    private DebtComponentService debtComponentService;
    @SpringBean
    private RenewalProcess renewalProcess;
    @SpringBean
    private CouncilDurationService councilDurationService;
    @SpringBean
    private RegistrationService registrationService;
    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private InvoiceProcess invoiceProcess;
    @SpringBean
    private PaymentProcess paymentProcess;
    @SpringBean
    private ReceiptItemService receiptItemService;
    @SpringBean
    GeneralParametersService generalParametersService;
    private CouncilDuration councilDuration;
    private Course course;
    private Boolean notAccrued = Boolean.FALSE;

    public RenewalPage(final IModel<Registrant> registrantModel) {

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());
        add(new RegistrantPanel("registrantPanel", registrantModel.getObject().getId()));
        PropertyModel<CouncilDuration> councilDurationModel = new PropertyModel<CouncilDuration>(this, "councilDuration");
        PropertyModel<Course> courseModel = new PropertyModel<Course>(this, "course");
        Form<?> form = new Form("form");
        Set<Course> courses = new HashSet<Course>();
        for (Registration r : registrationService.getActiveRegistrations(registrantModel.getObject())) {
            courses.add(r.getCourse());
        }

        form.add(new DropDownChoice("course", courseModel, new ArrayList<Course>(courses)));

        if (generalParametersService.get().getShowActiveDurationsOnly()) {
            form.add(new DropDownChoice("councilDuration", councilDurationModel, councilDurationService.getActiveCouncilDurations()));
        } else {
            form.add(new DropDownChoice("councilDuration", councilDurationModel, councilDurationService.findAll()));
        }


        form.add(new Button("process") {
            @Override
            public void onSubmit() {
                try {

                    // Process to payment where necessary
                    Customer customer = registrantService.get(registrantModel.getObject().getId()).getCustomerAccount();
                    List<DebtComponent> debtComponents = debtComponentService.getDebtComponents(customer.getAccount());
                    List<ReceiptItem> receiptItemList = receiptItemService.getReceiptItemsWithNoReceiptHeader(customer.getAccount());
                    if (customer.getAccount().getBalance().compareTo(new BigDecimal("0")) == -1 && !debtComponents.isEmpty()) {
                        setResponsePage(new CarryForwardPage(registrantService.getRegistrant(customer).getId()));
                    } else if (debtComponents.isEmpty() && !receiptItemList.isEmpty()) {
                        Set<ReceiptItem> receiptItems = new HashSet<ReceiptItem>();
                        if (receiptItemList.isEmpty()) {
                            setResponsePage(new RegistrantViewPage(registrantService.getRegistrant(customer).getId()));
                        } else {
                            for (ReceiptItem receiptItem : receiptItemList) {
                                receiptItems.add(receiptItem);
                            }
                            PaymentDetails paymentDetails = paymentProcess.accountBallancePaysAll(receiptItems, CouncilSession.get().getUser(), customer, councilDuration);
                            setResponsePage(new PaymentConfirmationPage(paymentDetails, customer.getAccount()));
                        }
                    } else if (!debtComponents.isEmpty() && !receiptItemList.isEmpty()) {
                        Set<ReceiptItem> receiptItems = new HashSet<ReceiptItem>();
                        if (receiptItemList.isEmpty()) {
                            setResponsePage(new RegistrantViewPage(registrantService.getRegistrant(customer).getId()));
                        } else {
                            for (ReceiptItem receiptItem : receiptItemList) {
                                receiptItems.add(receiptItem);
                            }

                            paymentProcess.accountBalancePaysLess(receiptItems, CouncilSession.get().getUser(), customer, councilDuration);
                        }
                        setResponsePage(new DebtComponentsPage(registrantService.getRegistrant(customer).getId()));

                    } else {
                        if (debtComponents.isEmpty()) {

                            setResponsePage(new CertificationPage(registrantModel.getObject().getId()));
                        } else {
                            setResponsePage(new DebtComponentsPage(registrantService.getRegistrant(customer).getId()));
                        }
                    }
                    renewalProcess.renew(registrantModel.getObject(), councilDuration, course);

                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        });

        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });

        // method to add a new RegistrantCpd - Id is null
        form.add(new Button("registrantCpdPage") {
            @Override
            public void onSubmit() {
                setResponsePage(new RegistrantCpdPage(registrantModel));
            }

            @Override
            protected void onConfigure() {
                setVisible(!generalParametersService.get().getCaptureCPDsDirectlyAndInDirectly());
            }
        });


        add(new FeedbackPanel("feedback"));
        add(form);

    }
}


//~ Formatted by Jindent --- http://www.jindent.com
