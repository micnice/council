/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.examinations.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.examinations.ExamRegistrationDebtComponentDao;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistrationDebtComponent;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationDebtComponentService;

import java.util.List;

/**
 *
 * @author tidza
 */
@Service
@Transactional
public class ExamRegistrationDebtComponentServiceImpl implements ExamRegistrationDebtComponentService {

    @Autowired
    private ExamRegistrationDebtComponentDao examRegistrationDebtComponentDao;

    @Transactional
    public ExamRegistrationDebtComponent save(ExamRegistrationDebtComponent t) {
        return examRegistrationDebtComponentDao.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ExamRegistrationDebtComponent> findAll() {
        return examRegistrationDebtComponentDao.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public ExamRegistrationDebtComponent get(Long id) {
        return examRegistrationDebtComponentDao.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ExamRegistrationDebtComponent> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setExamRegistrationDebtComponentDao(ExamRegistrationDebtComponentDao examRegistrationDebtComponentDao) {

        this.examRegistrationDebtComponentDao = examRegistrationDebtComponentDao;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ExamRegistrationDebtComponent> getDebtComponents(ExamSetting examSetting, Course course, Institution institution) {
        return examRegistrationDebtComponentDao.getDebtComponents(examSetting, course, institution);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ExamRegistrationDebtComponent> getDebtComponents(ExamSetting examSetting, Product product, Institution institution) {
    return examRegistrationDebtComponentDao.getDebtComponents(examSetting, product, institution);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ExamRegistrationDebtComponent> getDebtComponents(ExamRegistration examRegistration) {
        return examRegistrationDebtComponentDao.getDebtComponents(examRegistration);
    }
}
