package zw.co.hitrac.council.web.models;

import java.util.List;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.accounts.Prepayment;
import zw.co.hitrac.council.business.service.accounts.PrepaymentService;

/**
 *
 * @author Michael Matiashe
 */
public class PrepaymentListModel extends LoadableDetachableModel<List<Prepayment>> {

    private final PrepaymentService prepaymentService;
    private Institution institution;

    public PrepaymentListModel(PrepaymentService prepaymentService) {
        this.prepaymentService = prepaymentService;
    }

    public PrepaymentListModel(PrepaymentService prepaymentService1, Institution institution) {
        this.prepaymentService = prepaymentService1;
        this.institution = institution;
    }

    @Override
    protected List<Prepayment> load() {
        if (institution == null) {
            return prepaymentService.findAll();
        } else {
            return prepaymentService.findByInstitution(institution);
        }
    }
}
