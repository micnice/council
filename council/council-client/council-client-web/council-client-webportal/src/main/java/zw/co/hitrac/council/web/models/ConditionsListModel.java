package zw.co.hitrac.council.web.models;

import java.util.List;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.Conditions;
import zw.co.hitrac.council.business.service.ConditionService;

/**
 *
 * @author Matiashe Michael
 */
public class ConditionsListModel extends LoadableDetachableModel<List<Conditions>> {

    private final ConditionService conditionService;

    public ConditionsListModel(ConditionService conditionService) {
        this.conditionService = conditionService;
    }

    @Override
    protected List<Conditions> load() {
        return this.conditionService.findAll();
    }
}
