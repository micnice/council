package zw.co.hitrac.council.web.pages.registration;

import com.googlecode.wicket.jquery.core.JQueryBehavior;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.application.ApplicationPage;
import zw.co.hitrac.council.web.pages.examinations.ExamRegistrationPage;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;

/**
 *
 * @author Clive Gurure
 */
public class RegistrantActivityListPage extends TemplatePage {

    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private GeneralParametersService generalParametersService;

    public RegistrantActivityListPage(PageParameters parameters) {
        this(parameters.get("registrantId").toLong());
    }

    public RegistrantActivityListPage(Long id) {
        
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(id);
        final CompoundPropertyModel<Registrant> model = new CompoundPropertyModel<Registrant>(new DetachableRegistrantModel(id, registrantService));
        add(new RegistrantPanel("registrantPanel", id));
        setDefaultModel(model);
        
        add(new RegistrantActivityPanelList("registrantActivityPanelList", model));

    }
}
