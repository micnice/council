package zw.co.hitrac.council.web.pages.accounts;

import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.Bank;
import zw.co.hitrac.council.business.service.accounts.BankService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.configure.AdministerDatabasePage;

/**
 *
 * @author Michael Matiashe
 */
public class BankAccountListPage extends IBankBookPage {

    @SpringBean
    private BankService bankAccountService;

    public BankAccountListPage() {
        add(new BookmarkablePageLink<Void>("returnLink", BankBookPage.class));
        add(new Link<Void>("addNewLink") {

            @Override
            public void onClick() {
                setResponsePage(new BankAccountEditPage(null));
            }
        });

        IModel<List<Bank>> model = new LoadableDetachableModel<List<Bank>>() {

            @Override
            protected List<Bank> load() {
                return bankAccountService.findAll();
            }
        };

        PropertyListView<Bank> eachItem = new PropertyListView<Bank>("eachItem", model) {

            @Override
            protected void populateItem(ListItem<Bank> item) {
                Link<Bank> viewLink = new Link<Bank>("viewLink", item.getModel()) {

                    @Override
                    public void onClick() {
                        setResponsePage(new BankAccountViewPage(getModelObject().getId()));
                    }
                };
                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                viewLink.add(new Label("name"));
                item.add(new Label("accNumber"));
                item.add(new Label("branch"));
                item.add(new Label("bank"));
            }
        };

        add(eachItem);
    }
}
