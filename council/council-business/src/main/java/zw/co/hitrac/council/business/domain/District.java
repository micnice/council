package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Constance Mabaso
 */
@Entity
@Table(name="district")
@Audited
@XmlRootElement
public class District extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    private Set<Institution> institutions = new HashSet<Institution>();
    private Province province;

    @ManyToOne
    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }
    
    @OneToMany(mappedBy = "district")   
    public Set<Institution> getInstitutions() {
        return institutions;
    }

    public void setInstitutions(Set<Institution> institutions) {
        this.institutions = institutions;
    }
   
    
 
    
}
