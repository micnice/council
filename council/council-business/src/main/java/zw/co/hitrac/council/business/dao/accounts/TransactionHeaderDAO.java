
package zw.co.hitrac.council.business.dao.accounts;

import zw.co.hitrac.council.business.dao.IGenericDAO;
import zw.co.hitrac.council.business.domain.accounts.TransactionHeader;

/**
 *
 * @author Edawrd Zengeni
 */
public interface TransactionHeaderDAO extends IGenericDAO<TransactionHeader> {
    
}
