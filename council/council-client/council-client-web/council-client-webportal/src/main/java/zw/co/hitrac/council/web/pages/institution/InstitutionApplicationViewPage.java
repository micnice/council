package zw.co.hitrac.council.web.pages.institution;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.InstitutionCategory;
import zw.co.hitrac.council.business.domain.InstitutionInspection;
import zw.co.hitrac.council.business.domain.InstitutionPracticeControl;
import zw.co.hitrac.council.business.domain.InstitutionType;
import zw.co.hitrac.council.business.service.ApplicationService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.InstitutionInspectionService;
import zw.co.hitrac.council.business.service.InstitutionPracticeControlService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.application.ApplicationPage;
import zw.co.hitrac.council.web.pages.registration.RegistrantSupportingDocumentPanel;
import zw.co.hitrac.council.web.utility.CustomDateLabel;

/**
 *
 * @author charlesc
 */
public class InstitutionApplicationViewPage extends TemplatePage {

    @SpringBean
    private ApplicationService applicationService;
    @SpringBean
    private InstitutionInspectionService institutionInspectionService;
    @SpringBean
    private InstitutionPracticeControlService institutionPracticeControlService;
    @SpringBean
    private GeneralParametersService generalParametersService;

    public InstitutionApplicationViewPage(Long id) {
        menuPanelManager.getApplicationPanel().setTopMenuCurrent(true);
        final CompoundPropertyModel<Application> model = new CompoundPropertyModel<Application>(applicationService.get(id));
        add(new Label("currentStatus", model.getObject().getApplicationTextStatus()));
        setDefaultModel(model);

        add(new Label("institution.name"));
        add(new Label("institution.institutionType"));
        add(new Label("institution.institutionCategory"));
        add(new Label("institution.services"));
        add(new Label("institution.customerAccount"));
        add(new Label("institution.customerAccount.account.balance"));
        add(new FeedbackPanel("feedback"));
        add(new Link<Institution>("editLink") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionApplicationPage(model.getObject().getId()));
            }
        });
        add(new Link<Institution>("changeApplicationStatus") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionApplicationEditPage(model.getObject().getId()));
            }
        });

        add(new CustomDateLabel("applicationDate"));
        add(new Label("applicationTextStatus"));
        //supporting documents
        add(new RegistrantSupportingDocumentPanel("supportingDocumentPanel", null, null, null, null, null, null, model.getObject().getInstitution()));
        add(new InstitutionAssemmentPanelList("institutionAssemmentPanelList", model){

            @Override
            protected void onConfigure() {
                setVisible(!generalParametersService.get().getHasProcessedByBoard() || model.getObject().getInstitution().getTrainingInstitution()); //To change body of generated methods, choose Tools | Templates.
            }
            
        });
        add(new InstitutionPracticeControlPanelList("institutionPracticeControlPanelList", model){

            @Override
            protected void onConfigure() {
                setVisible(!generalParametersService.get().getHasProcessedByBoard() || model.getObject().getInstitution().getTrainingInstitution()); //To change body of generated methods, choose Tools | Templates.
            }
            
        });
        add(new InstitutionInspectionPanelList("institutionInspectionPanelList", model).setVisible(!generalParametersService.get().getHasProcessedByBoard() || model.getObject().getInstitution().getTrainingInstitution()));

        add(new Link<ApplicationPage>("applicationApprovalPage") {
            @Override
            public void onClick() {
                try {
                    boolean success = Boolean.FALSE;
                    boolean success2 = Boolean.FALSE;
                    if (!generalParametersService.get().getHasProcessedByBoard() || model.getObject().getInstitution().getTrainingInstitution()) {
                        for (InstitutionInspection ii : institutionInspectionService.get(model.getObject())) {
                            if (ii.getDecisionStatus().equals(InstitutionInspection.DECISIONAPPROVED)) {
                                success = Boolean.TRUE;
                            }
                        }
                        for (InstitutionPracticeControl ii : institutionPracticeControlService.get(model.getObject())) {
                            if (ii.getDecisionStatus().equals(InstitutionPracticeControl.DECISIONAPPROVED)) {
                                success2 = Boolean.TRUE;
                            }

                        }
                        if (success && success2) {
                            if (model.getObject().getApplicationStatus().equals(Application.APPLICATIONAPPROVED)) {
                                throw new CouncilException("Application Decision Already Made");
                            } else {
                                setResponsePage(new InstitutionApplicationPage(model.getObject().getId()));
                            }

                        } else {
                            throw new CouncilException("Inspection not successful or Practice Control not successful");
                        }
                    }else {
                         if (model.getObject().getApplicationStatus().equals(Application.APPLICATIONAPPROVED)) {
                                throw new CouncilException("Application Decision Already Made");
                            } else {
                                setResponsePage(new InstitutionApplicationPage(model.getObject().getId()));
                            }
                    }
                } catch (CouncilException e) {
                    error(e.getMessage());
                }
            }

            @Override
            protected void onConfigure() {
                if (model.getObject().getApplicationStatus().equals(Application.APPLICATIONAPPROVED) || model.getObject().getApplicationStatus().equals(Application.APPLICATIONDECLINED)) {
                    setVisible(Boolean.FALSE);
                }
            }
        });
    }

       private final class LoadableDetachableApplicationModel extends LoadableDetachableModel<Application> {

        private Long id;

        public LoadableDetachableApplicationModel(Long id) {
            this.id = id;
        }

        @Override
        protected Application load() {
            return applicationService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
