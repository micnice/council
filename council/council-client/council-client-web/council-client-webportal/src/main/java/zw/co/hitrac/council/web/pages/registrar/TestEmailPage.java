package zw.co.hitrac.council.web.pages.registrar;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.EmailMessageService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 * @author Takunda Dhlakama
 */
public class TestEmailPage extends TemplatePage {

    private String searchtxt;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private EmailMessageService emailMessageService;

    public TestEmailPage() {
        menuPanelManager.getSearchPanel().setTopMenuCurrent(true);
        menuPanelManager.getSearchPanel().setSearchSimple(true);

        PropertyModel<String> messageModel = new PropertyModel<String>(this, "searchtxt");
        Form<?> form = new Form("form");

        form.add(new TextField<String>("searchtxt", messageModel).setRequired(true).add(new ErrorBehavior()));
        form.add(new Button("process") {
            @Override
            public void onSubmit() {
                try {
                    RegistrantData data = new RegistrantData();
                    data.setEmailAddress(searchtxt);
                    String bodyMessage = String.format("This is a Test Email \n\n" + generalParametersService.get().getEmailSignature());
                    emailMessageService.send(data, "Test Email", bodyMessage);
                    info("Email Sent Successfully");
                } catch (Exception e) {
                    error("Test Failed " + e.getLocalizedMessage());
                }
            }
        });
        add(form);
        add(new FeedbackPanel("feedback"));
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
