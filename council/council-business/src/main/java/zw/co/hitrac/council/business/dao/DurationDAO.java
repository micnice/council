package zw.co.hitrac.council.business.dao;

import java.util.Date;
import java.util.List;

import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Duration;

/**
 * @author Michael Matiashe
 */
public interface DurationDAO extends IGenericDAO<Duration> {
    public Duration getCurrentCourseDuration(Course course);

    public List<Duration> getCourseDurations(Course course);

    public Duration getLastRenewalDuration(Course course, Date startDate);

    public Duration getDurationByCourseAndCouncilDuration(Course course, CouncilDuration councilDuration);

    public List<Duration> getDurations(CouncilDuration councilDuration);

    public List<Duration> getDurationsAfterDuration(Duration duration, Course course);

    public List<Duration> getDurationsBeforeDuration(Duration duration, Course course);

}
