package zw.co.hitrac.council.business.domain;

import org.apache.commons.lang.BooleanUtils;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Matiashe Michael
 */
@Entity
@Table(name="certificatereprint")
@Audited
public class CertificateReprint extends  BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private ProductIssuance rePrintedCertificate;
    
    private User rePrintedBy;
    
    private Date dateRePrinted;



    @ManyToOne
    public ProductIssuance getRePrintedCertificate() {
        return rePrintedCertificate;
    }

    public void setRePrintedCertificate(ProductIssuance rePrintedCertificate) {
        this.rePrintedCertificate = rePrintedCertificate;
    }
    
    @ManyToOne
    public User getRePrintedBy() {
        return rePrintedBy;
    }

    public void setRePrintedBy(User rePrintedBy) {
        this.rePrintedBy = rePrintedBy;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDateRePrinted() {
        return dateRePrinted;
    }

    public void setDateRePrinted(Date dateRePrinted) {
        this.dateRePrinted = dateRePrinted;
    }
    
     
}
