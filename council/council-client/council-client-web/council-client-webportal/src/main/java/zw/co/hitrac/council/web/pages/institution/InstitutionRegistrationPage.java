package zw.co.hitrac.council.web.pages.institution;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.ApplicationService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.pages.TemplatePage;

import java.util.Date;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.utility.CustomDateTextField;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

/**
 *
 * @author Michael Matiashe
 */
public class InstitutionRegistrationPage extends TemplatePage {

    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private RegistrationProcess registrationProcess;
    @SpringBean
    GeneralParametersService generalParametersService;
    @SpringBean
    DebtComponentService debtComponentService;
    @SpringBean
    RegistrantService registrantService;
    @SpringBean
    ApplicationService applicationService;

    public InstitutionRegistrationPage(final long id, final Boolean register) {
        Institution institution = institutionService.get(id);

        add(new Label("institution", institution.getName()));
        add(new FeedbackPanel("feedback"));

        Registration registration = new Registration();

        registration.setInstitution(institution);
        registration.setRegistrationDate(new Date());
        registration.setRegister(generalParametersService.get().getInstitutionRegister());
        setDefaultModel(
                new CompoundPropertyModel<Institution>(
                        new InstitutionRegistrationPage.LoadableDetachableInstitutionModel(institution.getId())));

        CompoundPropertyModel<Registration> model = new CompoundPropertyModel<Registration>(registration);

        setDefaultModel(model);

        Form<Registration> form = new Form<Registration>("form", (IModel<Registration>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    registrationProcess.registerInstitution(getModelObject());
                    setResponsePage(new InstitutionViewPage(id));
                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };

        form.add(new Link<Institution>("institutionViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionViewPage(id));
            }
        });

        form.add(
                new CustomDateTextField("registrationDate").setRequired(true).add(new ErrorBehavior()).add(
                        DatePickerUtil.getDatePicker()));

        form.add(new CustomDateTextField("deRegistrationDate") {
            @Override
            protected void onConfigure() {
                setVisible(!register);
            }

        }).add(DatePickerUtil.getDatePicker());
        form.add(new Label("deRegistrationLabelDate", "Registration Closed Date") {
            @Override
            protected void onConfigure() {
                setVisible(!register);
            }

        });       
        add(form);
    }

    private final class LoadableDetachableInstitutionModel extends LoadableDetachableModel<Institution> {

        private Long id;

        public LoadableDetachableInstitutionModel(Long id) {
            this.id = id;
        }

        @Override
        protected Institution load() {
            if (id == null) {
                return new Institution();
            }

            return institutionService.get(id);
        }
    }
}
