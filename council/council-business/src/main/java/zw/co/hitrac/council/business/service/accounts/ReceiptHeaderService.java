package zw.co.hitrac.council.business.service.accounts;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.TypeOfService;
import zw.co.hitrac.council.business.domain.User;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.accounts.PaymentMethod;
import zw.co.hitrac.council.business.domain.accounts.PaymentType;
import zw.co.hitrac.council.business.domain.accounts.ReceiptHeader;
import zw.co.hitrac.council.business.domain.accounts.TransactionType;
import zw.co.hitrac.council.business.service.IGenericService;

/**
 *
 * @author Michael Matiashe
 *
 */
 public interface ReceiptHeaderService extends IGenericService<ReceiptHeader> {

    List<ReceiptHeader> getReceiptHeaderByCouncilDurationAndAccountAndTypeOfService(CouncilDuration councilDuration, Account account, TypeOfService typeOfService);

     List<ReceiptHeader> getReceiptHeaders(Long receiptNumber, PaymentMethod paymentMethod, PaymentType paymentType, Date startDate, Date endDate, User user, TransactionType transactionType, Boolean reversed);

     Integer getTotalNumberOfReceipts(PaymentMethod paymentMethod, Date startDate, Date endDate, User user, TransactionType transactionType, Boolean reversed);

     List<ReceiptHeader> getReceiptHeadersDeposit(Long receiptNumber, PaymentMethod paymentMethod, PaymentType paymentType, Date startDate, Date endDate, User user, TransactionType transactionType, Boolean reversed);

     List<ReceiptHeader> getCarryForwardsList(Date startDate, Date endDate, User user, TransactionType transactionType, Boolean utilised);

      BigDecimal getCarryForwardBalance(Date endDate, Boolean utilised, Customer customer, TransactionType transactionType);

     List<ReceiptHeader> getCarryForwardBalanceList(Date endDate, Boolean utilised,TransactionType transactionType);

     List<ReceiptHeader> getChangedBanksList(TransactionType transactionTypeChange, Date startDate, Date endDate);

     List<ReceiptHeader> getReceiptListComments();

     List<ReceiptHeader> getReceiptHeadersAllDates(PaymentMethod paymentMethod, PaymentType paymentType, Date startDate, Date endDate, Date depositStartDate, Date depositEndDate, User user, TransactionType transactionType, Boolean reversed);

     List<ReceiptHeader> getRegistrantCarryForwardCollected(Date endDate, TransactionType transactionType);


    Long getNumberOfReceipts(Date endDate, User user);

    List<CouncilDuration> getCouncilDurationsfromRecieptHeaders(Account account, TypeOfService typeOfService);

    List<ReceiptHeader> getRegistrantCarryForwardCollectedForReport(Date startDate, Date endDate, TransactionType transactionType);

    List<ReceiptHeader> getCarryForwardBalanceListForReport(Date startDate, Date endDate, Boolean utilised, TransactionType transactionType);

    BigDecimal getCarryForwardBalanceForReport(Date startDate, Date endDate, Boolean utilised, Customer customer, TransactionType transactionType);

    void correctMissingChangeOfBankReport();
}
