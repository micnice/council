package zw.co.hitrac.council.web.pages.registration;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.web.pages.TemplatePage;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import zw.co.hitrac.council.business.domain.GeneralParameters;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.service.GeneralParametersService;

/**
 *
 * @author Takunda Dhlakama
 */
public class RegistrationListPage extends TemplatePage {

    @SpringBean
    private RegistrationService registrationService;
    @SpringBean
    private GeneralParametersService generalParametersService;

    public RegistrationListPage(final IModel<List<Registration>> model) {
        menuPanelManager.getRegistrantMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantionPanel().setRegistrantListPageCurrent(true);
        GeneralParameters generalParameters = generalParametersService.get();
        String name = generalParameters.getRegistrantName();

        if ((name == null) || name.trim().equals("")) {
            name = "Registration";
        }

        add(new Label("name", name));
      
        // Registration List Data View Panel
        add(new RegistrationsDataListPanel("registrationDataListPanel", model));
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
