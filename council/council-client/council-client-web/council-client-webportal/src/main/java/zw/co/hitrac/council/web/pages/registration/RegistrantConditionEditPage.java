/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.registration;

import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Conditions;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantCondition;
import zw.co.hitrac.council.business.service.ConditionService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrantConditionService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.accounts.documents.DebtComponentsPage;
import zw.co.hitrac.council.web.pages.certification.CertificationPage;
import zw.co.hitrac.council.web.pages.configure.ConditionEditPage;
import zw.co.hitrac.council.web.utility.CustomDateTextField;
import zw.co.hitrac.council.web.utility.DatePickerUtil;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;

/**
 *
 * @author kelvin
 */
public class RegistrantConditionEditPage extends TemplatePage {

    @SpringBean
    private RegistrantConditionService registrantConditionService;
    @SpringBean
    private ConditionService conditionService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private RegistrantService registrantService;
    private String description;
    private Conditions condition;

    public RegistrantConditionEditPage(final IModel<Registrant> registrantModel, final Boolean dashBoard) {

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());

        CompoundPropertyModel<RegistrantCondition> model = new CompoundPropertyModel<RegistrantCondition>(
                new RegistrantConditionEditPage.LoadableDetachableRegistrantConditionServiceModel(
                        registrantModel.getObject()));

        final Long registrantId = model.getObject().getRegistrant().getId();

        setDefaultModel(model);

        PropertyModel<String> descriptionModel = new PropertyModel<String>(this, "description");
        PropertyModel<Conditions> conditionModel = new PropertyModel<Conditions>(this, "condition");
        Form<RegistrantCondition> form = new Form<RegistrantCondition>("form", (IModel<RegistrantCondition>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    getModelObject().setCondition(condition);
                    registrantConditionService.save(getModelObject());
                    if (dashBoard) {
                        setResponsePage(new RegistrantViewPage(registrantId));
                    } else {
                        setResponsePage(new RegistrantConditionEditPage(registrantModel, dashBoard));
                    }

                } catch (CouncilException ex) {
                    error(ex.getMessage());

                }

            }
        };

        DropDownChoice<Conditions> conditionsAll = new DropDownChoice<Conditions>("condition", conditionModel, new ConditionsModel()) {
            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }

            @Override
            protected void onSelectionChanged(Conditions newSelection) {

                description = newSelection.getComments();

            }

        };

        form.add(conditionsAll.setRequired(true).add(new ErrorBehavior()));
        form.add(new Label("description", descriptionModel));
        form.add(new CheckBox("status"));
        form.add(new CustomDateTextField("startDate").add(DatePickerUtil.getDatePicker()));
        form.add(new CustomDateTextField("endDate").add(DatePickerUtil.getDatePicker()));

        add(form);
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new ConditionConfirmationPage(registrantModel.getObject().getId()));
            }
        });

        form.add(new Link<Void>("certificationPage") {
            @Override
            public void onClick() {
                setResponsePage(new CertificationPage(registrantModel.getObject().getId()));
            }
        });

        form.add(new Link<Void>("nextPage") {
            @Override
            public void onClick() {
                setResponsePage(new DebtComponentsPage(registrantId));
            }
        });

        add(new Label("registrant.fullname"));

        add(new Link<Void>("conditionEditPage") {
            @Override
            public void onClick() {
                setResponsePage(new ConditionEditPage(null, registrantModel));
            }
        });
    }

    private final class ConditionsModel extends LoadableDetachableModel<List<? extends Conditions>> {

        protected List<? extends Conditions> load() {
            return conditionService.findAll();
        }
    }

    public RegistrantConditionEditPage(long id) {

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        CompoundPropertyModel<RegistrantCondition> model = new CompoundPropertyModel<RegistrantCondition>(
                new RegistrantConditionEditPage.LoadableDetachableRegistrantConditionServiceModel(
                        id));

        final Long registrantId = model.getObject().getRegistrant().getId();

        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantId);
        setDefaultModel(model);

        PropertyModel<String> descriptionModel = new PropertyModel<String>(this, "description");
        PropertyModel<Conditions> conditionModel = new PropertyModel<Conditions>(this, "condition");
        Form<RegistrantCondition> form = new Form<RegistrantCondition>("form", (IModel<RegistrantCondition>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    getModelObject().setCondition(condition);
                    registrantConditionService.save(getModelObject());
                    if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                        setResponsePage(new CertificationPage(getModelObject().getRegistrant().getId()));
                    }else{
                    setResponsePage(new ConditionConfirmationPage(getModelObject().getRegistrant().getId()));
                    }
                } catch (CouncilException ex) {
                    error(ex.getMessage());

                }

            }
        };
        DropDownChoice<Conditions> conditionsAll = new DropDownChoice<Conditions>("condition", conditionModel, new ConditionsModel()) {
            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }

            @Override
            protected void onSelectionChanged(Conditions newSelection) {
                    description = newSelection.getComments();
            }

        };

        form.add(conditionsAll.setRequired(true).add(new ErrorBehavior()));
        form.add(new Label("description", descriptionModel));
        form.add(new CheckBox("status"));
        form.add(new CustomDateTextField("startDate").add(DatePickerUtil.getDatePicker()));
        form.add(new CustomDateTextField("endDate").add(DatePickerUtil.getDatePicker()));

        add(form);
        add(new Label("registrant.fullname"));

        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new ConditionConfirmationPage(registrantId));
            }
        });

        form.add(new Link<Void>("certificationPage") {
            @Override
            public void onClick() {
                setResponsePage(new CertificationPage(registrantId));
            }
        });

        form.add(new Link<Void>("nextPage") {
            @Override
            public void onClick() {
                setResponsePage(new DebtComponentsPage(registrantId));
            }
        });

        final CompoundPropertyModel<Registrant> registrantModel = new CompoundPropertyModel<Registrant>(new DetachableRegistrantModel(model.getObject().getRegistrant().getId(), registrantService));

        add(new Link<Void>("conditionEditPage") {
            @Override
            public void onClick() {
                setResponsePage(new ConditionEditPage(null, registrantModel));
            }
        });

    }

    private final class LoadableDetachableRegistrantConditionServiceModel extends LoadableDetachableModel<RegistrantCondition> {

        private Long id;
        private Registrant registrant;

        public LoadableDetachableRegistrantConditionServiceModel(Long id) {
            this.id = id;
        }

        public LoadableDetachableRegistrantConditionServiceModel(Registrant registrant) {
            this.registrant = registrant;
        }

        @Override
        protected RegistrantCondition load() {
            RegistrantCondition registrantCondition = null;

            if (id == null) {
                registrantCondition = new RegistrantCondition();
                registrantCondition.setRegistrant(registrant);
            } else {
                registrantCondition = registrantConditionService.get(id);
                if (registrantCondition.getCondition() != null) {
                    condition = registrantCondition.getCondition();
                }
            }
            return registrantCondition;
        }
    }
}
