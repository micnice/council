/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.models;

import java.util.List;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.service.examinations.ExamSettingService;

/**
 *
 * @author kelvin
 */
public class ExamSettingActiveListModel extends LoadableDetachableModel<List<ExamSetting>> {

    private final ExamSettingService examSettingService;

    public ExamSettingActiveListModel(ExamSettingService examSettingService) {
        this.examSettingService = examSettingService;
    }

    @Override
    protected List<ExamSetting> load() {
        return examSettingService.getExamSettings(Boolean.TRUE);
    }
}
