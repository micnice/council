
package zw.co.hitrac.council.business.dao.repo.accounts;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.accounts.CarryForwardLog;

/**
 *
 * @author Tatenda Chiwandire
 * @author Edward Zengeni
 * @author Judge Muzinda
 */
public interface CarryForwardLogRepository extends CrudRepository<CarryForwardLog, Long> {

    public List<CarryForwardLog> findAll();
    
}
