package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Constance Mabaso
 */
@Entity
@Table(name="institutioninspection")
@Audited
public class InstitutionInspection extends BaseIdEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    private Application application;
    private Date inspectionDate;
    private Date expectedInspectionDate;
    private Boolean inspectionDone = Boolean.FALSE;
    private String findings;
    public static String DECISIONAPPROVED = "DECISION APPROVED";
    public static String DECISIONDECLINED = "DECISION DECLINED";
    public static String DECISIONPENDING = "DESICION PENDING";
    public static String DECISIONTOBEINSPECTED = "DECISION TO BE INSPECTED";
    public static String DECISIONTOBEREINSPECTED = "DECISION TO BE REINSPECTED";
    
    private String decisionStatus = DECISIONPENDING;

    @ManyToOne
    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }
    
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getInspectionDate() {
        return inspectionDate;
    }

    public void setInspectionDate(Date inspectionDate) {
        this.inspectionDate = inspectionDate;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getExpectedInspectionDate() {
        return expectedInspectionDate;
    }

    public void setExpectedInspectionDate(Date expectedInspectionDate) {
        this.expectedInspectionDate = expectedInspectionDate;
    }

    public Boolean getInspectionDone() {
        return inspectionDone;
    }

    public void setInspectionDone(Boolean inspectionDone) {
        this.inspectionDone = inspectionDone;
    }
    
    @Column(length = 1024)
    public String getFindings() {
        return findings;
    }

    public void setFindings(String findings) {
        this.findings = findings;
    }

    public String getDecisionStatus() {
        return decisionStatus;
    }

    public void setDecisionStatus(String decisionStatus) {
        this.decisionStatus = decisionStatus;
    }

    @Transient
    public String getInspectionStatus() {
        if (inspectionDone == null) {
            return "PENDING";
        }
        return inspectionDone ? "DONE" : "PENDING";
    }
}
