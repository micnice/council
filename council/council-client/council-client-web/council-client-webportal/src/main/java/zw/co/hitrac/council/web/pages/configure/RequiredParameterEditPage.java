package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.RequiredParameter;
import zw.co.hitrac.council.business.domain.RequiredParameter;
import zw.co.hitrac.council.business.service.RequiredParameterService;

/**
 * Created by tndangana on 2/13/17.
 */
public class RequiredParameterEditPage extends  IAdministerDatabaseBasePage {


    @SpringBean
    private RequiredParameterService  requiredParameterService;

    public RequiredParameterEditPage() {

        setDefaultModel(new CompoundPropertyModel<RequiredParameter>(new RequiredParameterEditPage.LoadableDetachableRequiredParameterModel()));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        Form<RequiredParameter> form = new Form<RequiredParameter>("form", (IModel<RequiredParameter>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                requiredParameterService.save(getModelObject());
                setResponsePage(new AdministerDatabasePage());
            }
        };

        form.add(new CheckBox("registrantBirthDate"));
        form.add(new CheckBox("registrantIdNumber"));
        form.add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(form);
    }


    private final class LoadableDetachableRequiredParameterModel extends LoadableDetachableModel<RequiredParameter> {

        @Override
        protected RequiredParameter load() {

            return requiredParameterService.get();

        }
    }
}
