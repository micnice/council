/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service;

import java.util.Date;
import java.util.List;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantDiscipline;

/**
 *
 * @author kelvin
 */
public interface RegistrantDisciplineService extends IGenericService<RegistrantDiscipline> {

    public List<RegistrantDiscipline> getDisciplines(Registrant registrant);
    
    public Date getRegistrantDisciplineDate(Registrant registrant, Course course);
    
    public RegistrantDiscipline getRegistrantDiscipline(Registrant registrant, Course course);
}
