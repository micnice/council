/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import java.util.Date;

/**
 *
 * @author tdhlakama
 */
@Entity
@Table(name="registrantdiscipline")
@Audited
public class RegistrantDiscipline extends BaseIdEntity {
    
    private Registrant registrant;
    private Course course;
    private Date registrationDate;
    private Date deregistrationDate;
    
    @ManyToOne
    public Registrant getRegistrant() {
        return registrant;
    }

    public void setRegistrant(Registrant registrant) {
        this.registrant = registrant;
    }

    @ManyToOne
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
    
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDeregistrationDate() {
        return deregistrationDate;
    }

    public void setDeregistrationDate(Date deregistrationDate) {
        this.deregistrationDate = deregistrationDate;
    }
    
    
    
}
