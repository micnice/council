package zw.co.hitrac.council.web.pages.configure;

import java.util.Arrays;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxCheckBox;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.CheckBoxMultipleChoice;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Groups;
import zw.co.hitrac.council.business.domain.ProductIssuanceType;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.CourseTypeService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.QualificationService;
import zw.co.hitrac.council.business.service.RegisterTypeService;
import zw.co.hitrac.council.business.service.RequirementService;
import zw.co.hitrac.council.business.service.TierService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.QualificationListModel;
import zw.co.hitrac.council.web.models.RequirementListModel;
import zw.co.hitrac.council.web.models.TrainingInstitutionstModel;

/**
 *
 * @author Charles Chigoriwa
 * @author Michael Matiashe
 */
public class CourseEditPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private CourseService courseService;
    @SpringBean
    private RegisterTypeService registerTypeService;
    @SpringBean
    private CourseTypeService courseTypeService;
    @SpringBean
    private QualificationService qualificationService;
    @SpringBean
    private RequirementService requirementService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private TierService tierService;

    public CourseEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<Course>(new LoadableDetachableCourseModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        Form<Course> form = new Form<Course>("form", (IModel<Course>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                courseService.save(getModelObject());
                setResponsePage(new CourseViewPage(getModelObject().getId()));
            }
        };

        form.add(new DropDownChoice("courseType", courseTypeService.findAll()));
        form.add(new DropDownChoice("registerType", registerTypeService.findAll()));
        form.add(new DropDownChoice("productIssuanceType", Arrays.asList(ProductIssuanceType.values())));
        form.add(new DropDownChoice("qualification", qualificationService.findAll()));
        form.add(new DropDownChoice("courseGroup", Arrays.asList(Groups.values())));
        form.add(new DropDownChoice("tier", tierService.findAll()));
        form.add(new TextField<Integer>("weight").add(new ErrorBehavior()));
        form.add(new TextField<Integer>("courseDuration").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("prefixName").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("isoCode"));
        form.add(new TextField<String>("certificatePrefix"));
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new CheckBox("basic"));
        form.add(new CheckBox("special"));
        form.add(new CheckBox("hasCGSApplication"));
        form.add(new TextField<String>("lastCourseRegistrationNumber"));
        form.add(new BookmarkablePageLink<Void>("returnLink", CourseListPage.class));
        form.add(new CheckBox("retired"));
        add(form);

        final WebMarkupContainer wmc1 = new WebMarkupContainer("wmc1");
        final WebMarkupContainer wmc2 = new WebMarkupContainer("wmc2");
        final WebMarkupContainer wmc3 = new WebMarkupContainer("wmc3");
        wmc1.setVisible(false);
        wmc2.setVisible(false);
        wmc3.setVisible(false);
        wmc1.setOutputMarkupPlaceholderTag(true);
        wmc2.setOutputMarkupPlaceholderTag(true);
        wmc3.setOutputMarkupPlaceholderTag(true);
        form.add(wmc1);
        form.add(wmc2);
        form.add(wmc3);
        form.add(new AjaxCheckBox("advanced1", new PropertyModel(wmc1, "visible")) {
            @Override
            protected void onUpdate(AjaxRequestTarget art) {
                art.add(wmc1);
            }
        });
        form.add(new AjaxCheckBox("advanced2", new PropertyModel(wmc2, "visible")) {
            @Override
            protected void onUpdate(AjaxRequestTarget art) {
                art.add(wmc2);
            }
        });
        form.add(new AjaxCheckBox("advanced3", new PropertyModel(wmc3, "visible")) {
            @Override
            protected void onUpdate(AjaxRequestTarget art) {
                art.add(wmc3);
            }
        });
        wmc1.add(new CheckBoxMultipleChoice("requirements", new RequirementListModel(requirementService)));
        wmc2.add(new CheckBoxMultipleChoice("trainingInstitutions", new TrainingInstitutionstModel(institutionService)));
        wmc3.add(new CheckBoxMultipleChoice("qualifications", new QualificationListModel(qualificationService)));
    }

    private final class LoadableDetachableCourseModel extends LoadableDetachableModel<Course> {

        private Long id;

        public LoadableDetachableCourseModel(Long id) {
            this.id = id;
        }

        @Override
        protected Course load() {
            if (id == null) {
                return new Course();
            }
            return courseService.get(id);
        }
    }
}
