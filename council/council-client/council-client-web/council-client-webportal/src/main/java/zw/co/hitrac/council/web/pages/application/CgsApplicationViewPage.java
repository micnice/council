package zw.co.hitrac.council.web.pages.application;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.service.ApplicationService;
import zw.co.hitrac.council.business.service.RegistrantService;

import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.registration.RegistrantAddressPanelList;
import zw.co.hitrac.council.web.pages.registration.RegistrantDocumentPage;
import zw.co.hitrac.council.web.pages.registration.RegistrantSupportingDocumentPanel;
import zw.co.hitrac.council.web.pages.registration.RegistrantViewPage;
import zw.co.hitrac.council.web.utility.CustomDateLabel;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;

/**
 *
 * @author Michael Matiashe
 */
public class CgsApplicationViewPage extends TemplatePage {

    @SpringBean
    private ApplicationService applicationService;
    @SpringBean
    private RegistrantService registrantService;

    public CgsApplicationViewPage(long id) {

        menuPanelManager.getApplicationPanel().setTopMenuCurrent(true);
        final CompoundPropertyModel<Application> model = new CompoundPropertyModel<Application>(applicationService.get(id));
        final CompoundPropertyModel<Registrant> registrantModel = new CompoundPropertyModel<Registrant>(new DetachableRegistrantModel(model.getObject().getRegistrant().getId(), registrantService));

        setDefaultModel(model);

        add(new Label("approvedBy"));
        add(new Label("processedBy"));
        add(new CustomDateLabel("dateApproved"));
        add(new CustomDateLabel("dateProcessed"));
        add(new Label("applicationStatus"));
        add(new Label("applicationPurpose"));
        add(new Label("institution"));
        add(new CustomDateLabel("datePosted"));
        add(new CustomDateLabel("applicationDate"));
        add(new Label("comment"));

        add(new RegistrantAddressPanelList("registrantAddressPanelList", model.getObject().getInstitution(), Boolean.FALSE));

        add(new Link<Void>("registrationOptionPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });
        add(new Link<Application>("applicationApprovalPage", model) {
            @Override
            public void onClick() {
                setResponsePage(new CgsApplicationPage(getModel().getObject().getId()));
            }

            @Override
            protected void onConfigure() {
                if (getModel().getObject().getApplicationStatus().equals(Application.APPLICATIONAPPROVED) || getModel().getObject().getApplicationStatus().equals(Application.APPLICATIONDECLINED)) {
                    setVisible(Boolean.FALSE);
                }
            }
        });

        add(new Link<Application>("applicationDatePage", model) {
            @Override
            public void onClick() {
                setResponsePage(new ApplicationDatePage(getModel().getObject().getId()));
            }

            @Override
            protected void onConfigure() {
                if (getModel().getObject().getApplicationStatus().equals(Application.APPLICATIONAPPROVED) || getModel().getObject().getApplicationStatus().equals(Application.APPLICATIONDECLINED)) {
                    setVisible(Boolean.TRUE);
                }
            }
        });

        add(new Link<Application>("registrantDocumentPage", model) {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantDocumentPage(getModelObject(), registrantModel));
            }
        });


        add(new Label("registrant.fullname"));
        add(new FeedbackPanel("feedback"));

        //supporting documents
        add(new RegistrantSupportingDocumentPanel("supportingDocumentPanel", null, model.getObject(), null, null, null, null, null));

    }
}


//~ Formatted by Jindent --- http://www.jindent.com
