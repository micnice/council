package zw.co.hitrac.council.business.domain.reports;

import zw.co.hitrac.council.business.domain.Registrant;
/**
 *
 * @author user
 */
public class RenewalLetter {

    private Registrant registrant;
    private String conditions;
    private String totalAmount;
    private String practiseFee;
    private String annualFee;
  
    public Registrant getRegistrant() {
        return registrant;
    }

    public void setRegistrant(Registrant registrant) {
        this.registrant = registrant;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    

    public String getTotalAmount() {
        return totalAmount;
    }

    public String getPractiseFee() {
        return practiseFee;
    }

    public String getAnnualFee() {
        return annualFee;
    }

   
    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public void setPractiseFee(String practiseFee) {
        this.practiseFee = practiseFee;
    }

    public void setAnnualFee(String annualFee) {
        this.annualFee = annualFee;
    }
}
