
package zw.co.hitrac.council.business.dao.repo.accounts;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.accounts.PaymentData;

/**
 *
 * Michael Matiashe
 */
public interface PaymentDataRepository extends CrudRepository<PaymentData, Long> {

    public List<PaymentData> findAll();
    
}
