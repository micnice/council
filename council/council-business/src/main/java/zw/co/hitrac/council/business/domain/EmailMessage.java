package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 *
 * @author charlesc
 */
@Entity
@Table(name = "emailMessage")
@Audited
public class EmailMessage extends BaseIdEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    private String subject;
    private String paragraphOne;
    private String paragraphTwo;
    private Boolean sent;
    
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Column(columnDefinition = "TEXT")
    public String getParagraphOne() {
        return paragraphOne;
    }

    public void setParagraphOne(String paragraphOne) {
        this.paragraphOne = paragraphOne;
    }

    @Column(columnDefinition = "TEXT")
    public String getParagraphTwo() {
        return paragraphTwo;
    }

    public void setParagraphTwo(String paragraphTwo) {
        this.paragraphTwo = paragraphTwo;
    }

    public Boolean isSent() {
        if (sent == null) {
            return Boolean.FALSE;
        }
        return sent;
    }

    public void setSent(Boolean sent) {
        this.sent = sent;
    }
    
}
