package zw.co.hitrac.council.web.pages.accounts;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.service.accounts.ProductService;

/**
 *
 * @author Charles Chigoriwa
 */
public class ProductViewPage extends IInventoryModulePage {

    @SpringBean
    private ProductService productService;

    public ProductViewPage(Long id) {
        CompoundPropertyModel<Product> model = new CompoundPropertyModel<Product>(new LoadableDetachableProductModel(id));
        setDefaultModel(model);
        add(new Link<Product>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new ProductEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", ProductListPage.class));
        add(new Label("salesAccount"));
        add(new Label("course"));
        add(new Label("register"));
        add(new Label("employmentType.name"));
        add(new Label("typeOfService.name"));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("courseType"));
        add(new Label("tier"));
        add(new Label("qualificationType"));
        add(new Label("examProduct"));
        add(new Label("productGroup"));
    }

    private final class LoadableDetachableProductModel extends LoadableDetachableModel<Product> {

        private Long id;

        public LoadableDetachableProductModel(Long id) {
            this.id = id;
        }

        @Override
        protected Product load() {

            return productService.get(id);
        }
    }
}
