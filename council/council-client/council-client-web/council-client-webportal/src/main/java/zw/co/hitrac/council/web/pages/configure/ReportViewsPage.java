package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author Charles Chigoriwa
 */
public class ReportViewsPage extends TemplatePage {
    public ReportViewsPage() {
        menuPanelManager.getConfigureMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getConfigureMenuPanel().setReportViewsMenuCurrent(true);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
