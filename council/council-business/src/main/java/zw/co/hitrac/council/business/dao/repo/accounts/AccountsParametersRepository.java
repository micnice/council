
package zw.co.hitrac.council.business.dao.repo.accounts;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.accounts.AccountsParameters;

/**
 *
 * @author Kelvin Goredema
 */
public interface AccountsParametersRepository   extends CrudRepository<AccountsParameters, Long>{
    
    public List<AccountsParameters> findAll();
}
