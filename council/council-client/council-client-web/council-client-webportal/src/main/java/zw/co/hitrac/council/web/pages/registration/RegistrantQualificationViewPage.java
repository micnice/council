/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.registration;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantDisciplinary;
import zw.co.hitrac.council.business.domain.RegistrantQualification;
import zw.co.hitrac.council.business.service.RegistrantQualificationService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.CustomDateLabel;
import zw.co.hitrac.council.web.utility.CustomDateTimeLabel;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;

/**
 *
 * @author Clive Gurure
 */
public class RegistrantQualificationViewPage extends TemplatePage {

    @SpringBean
    private RegistrantQualificationService registrantQualificationService;
    @SpringBean
    private RegistrantService registrantService;

    public RegistrantQualificationViewPage(Long id) {
        CompoundPropertyModel<RegistrantQualification> model =
                new CompoundPropertyModel<RegistrantQualification>(
                new RegistrantQualificationViewPage.LoadableDetachableRegistrantQualificationModel(id));

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);

        final Long registrantId = model.getObject().getRegistrant().getId();

        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantId);
        setDefaultModel(model);

        final CompoundPropertyModel<Registrant> registrantModel = new CompoundPropertyModel<Registrant>(new DetachableRegistrantModel(id, registrantService));

        add(new Label("qualification"));
        add(new Label("institution"));
        add(new CustomDateLabel("dateAwarded"));
        add(new Label("awardingInstitution"));
        add(new CustomDateLabel("startDate"));
        add(new CustomDateLabel("endDate"));
        add(new Label("registrant.fullname"));
        add(new Label("qualificationType"));
        add(new Label("createdBy"));
        add(new Label("modifiedBy"));
        add(CustomDateTimeLabel.forDate("dateCreated", new PropertyModel<>(model, "dateCreated")));
        add(CustomDateTimeLabel.forDate("dateModified", new PropertyModel<>(model, "dateModified")));


        add(new FeedbackPanel("feedback"));
        add(new RegistrantSupportingDocumentPanel("supportingDocumentPanel", null, null, null, null, model.getObject(), null, null));

        add(new Link<Registrant>("registrantOptionPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantId));
            }
        });


        add(new Link<RegistrantQualification>("registrantQualification", model) {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantQualificationPage(getModelObject().getId()));
                
            }
        });
        add(new Link<RegistrantQualification>("registrantDocumentPage", model) {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantDocumentPage(getModelObject(), registrantModel));
            }
        });
    }

    private final class LoadableDetachableRegistrantQualificationModel
            extends LoadableDetachableModel<RegistrantQualification> {

        private Long id;
        private Registrant registrant;

        public LoadableDetachableRegistrantQualificationModel(Long id) {
            this.id = id;
        }

        public LoadableDetachableRegistrantQualificationModel(Registrant registrant) {
            this.registrant = registrant;
        }

        @Override
        protected RegistrantQualification load() {
            RegistrantQualification registrantQualification = null;

            if (id == null) {
                registrantQualification = new RegistrantQualification();
                registrantQualification.setRegistrant(registrant);
            } else {
                registrantQualification = registrantQualificationService.get(id);
            }

            return registrantQualification;
        }
    }
}
