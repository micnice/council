package zw.co.hitrac.council.business.service;

import java.util.List;
import zw.co.hitrac.council.business.domain.ProductIssuanceType;
import zw.co.hitrac.council.business.domain.Qualification;

/**
 *
 * @author Michael Matiashe
 */

public interface QualificationService extends IGenericService<Qualification> {
    
    public List<Qualification> getQualificationCertificateType(ProductIssuanceType productIssuanceType);
	public Qualification findByName(String name);
}
