/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.impl;

import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.RegistrantConditionDAO;
import zw.co.hitrac.council.business.dao.repo.RegistrantConditionRepository;
import zw.co.hitrac.council.business.domain.Conditions;
import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantCondition;

/**
 *
 * @author kelvin
 */
@Repository
public class RegistrantConditionDAOImpl implements RegistrantConditionDAO {

    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    private RegistrantConditionRepository registrantConditionRepository;

    public RegistrantCondition save(RegistrantCondition registrantCondition) {
        return registrantConditionRepository.save(registrantCondition);
    }

    public List<RegistrantCondition> findAll() {
        return registrantConditionRepository.findAll();
    }

    public RegistrantCondition get(Long id) {
        return registrantConditionRepository.findOne(id);
    }

    /**
     * A setter method that will make mocking repo object easier
     *
     * @param registrantConditionRepository
     */
    public void setRegistrantConditionRepository(RegistrantConditionRepository registrantConditionRepository) {
        this.registrantConditionRepository = registrantConditionRepository;
    }

    //conditions currently imposed on an individual
    public List<RegistrantCondition> getRegistrantActiveConditions(Registrant registrant) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantCondition.class);
        criteria.add(Restrictions.eq("registrant", registrant));
        criteria.add(Restrictions.eq("status", Boolean.TRUE));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }
    
    //history of all conditions imposed on an individual
    public List<RegistrantCondition> getAllRegistrantConditions(Registrant registrant) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantCondition.class);
        criteria.add(Restrictions.eq("registrant", registrant));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<RegistrantCondition> getRegisterRegistrantConditions(Registrant registrant) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantCondition.class);
        criteria.add(Restrictions.eq("registrant", registrant));
        criteria.createAlias("condition", "c");
        criteria.add(Restrictions.eq("c.registerCondition", Boolean.TRUE));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<RegistrantCondition> getCondition(Registrant registrant, Conditions condition) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantCondition.class);
        criteria.add(Restrictions.eq("registrant", registrant));
        criteria.add(Restrictions.eq("condition", condition));
        criteria.add(Restrictions.eq("status", Boolean.TRUE));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<RegistrantCondition> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Integer getNumberOfConditionsExpired() {
        Long l = (Long) entityManager.createQuery("select count(r) from RegistrantCondition r where r.status=:status and r.endDate <=:endDate").setParameter("status", Boolean.TRUE).setParameter("endDate", new Date()).getSingleResult();
        return l.intValue();
    }

    public List<RegistrantCondition> getAllActiveRegistrantConditions() {
        return entityManager.createQuery("select r from RegistrantCondition r where r.status=:status").setParameter("status", Boolean.TRUE).getResultList();
    }

    public List<RegistrantCondition> getAllExpiredConditions() {
        return entityManager.createQuery("select r from RegistrantCondition r where r.status=:status and r.endDate <=:endDate").setParameter("status", Boolean.TRUE).setParameter("endDate", new Date()).getResultList();
    }

    @Transactional(readOnly = false, propagation = Propagation.SUPPORTS)
    public void saveRegisterConditions(Register register, Registrant registrant){
        for (Conditions c : register.getConditions()) {
            RegistrantCondition rc = new RegistrantCondition();
            rc.setCondition(c);
            rc.setDateCreated(new Date());
            rc.setStatus(Boolean.TRUE);
            rc.setRegistrant(registrant);
            save(rc);
        }
    }
}
