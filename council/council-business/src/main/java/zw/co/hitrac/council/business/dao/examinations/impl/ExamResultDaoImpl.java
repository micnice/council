package zw.co.hitrac.council.business.dao.examinations.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.examinations.ExamResultDao;
import zw.co.hitrac.council.business.dao.repo.examinations.ExamResultRepository;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.examinations.Exam;
import zw.co.hitrac.council.business.domain.examinations.ExamResult;

/**
 *
 * @author tidza
 */
@Repository
public class ExamResultDaoImpl implements ExamResultDao {

    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    private ExamResultRepository examResultRepository;

    public ExamResult save(ExamResult t) {
        return examResultRepository.save(t);
    }

    public List<ExamResult> findAll() {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ExamResult.class);
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public ExamResult get(Long id) {
        return examResultRepository.findOne(id);
    }

    public void setExamResultRepository(ExamResultRepository examResultRepository) {
        this.examResultRepository = examResultRepository;
    }

    public List<ExamResult> examResults(Exam exam) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ExamResult.class);
        criteria.add(Restrictions.eq("exam", exam));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public Integer getTotalCandidates(Exam exam, Institution institution) {
        if (institution != null) {
            Long l = (Long) entityManager.createQuery("select count(e) from ExamResult e where e.exam=:exam AND e.examRegistration.institution=:institution").setParameter("exam", exam).setParameter("institution", institution).getSingleResult();
            return l.intValue();
        } else {
            Long l = (Long) entityManager.createQuery("select count(e) from ExamResult e WHERE e.exam=:exam").setParameter("exam", exam).getSingleResult();
            return l.intValue();
        }
    }

    @Override
    public Integer getTotalCandidates(Exam exam, Institution institution, Boolean supplementaryExam, Boolean pass) {
        if (pass) {
            if (institution != null) {
                Long l = (Long) entityManager.createQuery("select count(e) from ExamResult e where e.examRegistration.institution=:institution AND e.examRegistration.suppressed=FALSE and e.examRegistration.reversedResult=FALSE and e.examRegistration.closed=TRUE AND e.exam=:exam AND e.mark >= e.exam.passMark AND e.disqualified=FALSE AND e.examAttendance=TRUE AND e.supplementaryExam=:supplementaryExam").setParameter("exam", exam).setParameter("institution", institution).setParameter("supplementaryExam", supplementaryExam).getSingleResult();
                return l.intValue();
            } else {
                Long l = (Long) entityManager.createQuery("select count(e) from ExamResult e where e.examRegistration.suppressed=FALSE and e.examRegistration.reversedResult=FALSE and e.examRegistration.closed=TRUE AND e.exam=:exam AND e.mark >= e.exam.passMark AND e.disqualified=FALSE AND e.examAttendance=TRUE AND e.supplementaryExam=:supplementaryExam").setParameter("exam", exam).setParameter("supplementaryExam", supplementaryExam).getSingleResult();
                return l.intValue();
            }
        } else {
            if (institution != null) {
                Long l = (Long) entityManager.createQuery("select count(e) from ExamResult e where e.examRegistration.institution=:institution AND e.examRegistration.suppressed=FALSE and e.examRegistration.reversedResult=FALSE and e.examRegistration.closed=TRUE AND e.exam=:exam AND e.mark < e.exam.passMark AND e.disqualified=FALSE AND e.examAttendance=TRUE AND e.supplementaryExam=:supplementaryExam").setParameter("exam", exam).setParameter("institution", institution).setParameter("supplementaryExam", supplementaryExam).getSingleResult();
                return l.intValue();
            } else {
                Long l = (Long) entityManager.createQuery("select count(e) from ExamResult e where e.examRegistration.suppressed=FALSE and e.examRegistration.reversedResult=FALSE and e.examRegistration.closed=TRUE AND e.exam=:exam AND e.mark < e.exam.passMark AND e.disqualified=FALSE AND e.examAttendance=TRUE AND e.supplementaryExam=:supplementaryExam").setParameter("exam", exam).setParameter("supplementaryExam", supplementaryExam).getSingleResult();
                return l.intValue();
            }
        }
    }

    public List<ExamResult> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
