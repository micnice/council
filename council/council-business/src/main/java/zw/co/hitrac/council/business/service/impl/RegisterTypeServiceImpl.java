/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.RegisterTypeDAO;
import zw.co.hitrac.council.business.domain.RegisterType;
import zw.co.hitrac.council.business.service.RegisterTypeService;

import java.util.List;

/**
 *
 * @author Kelvin Goredema
 */
@Service
@Transactional
public class RegisterTypeServiceImpl implements RegisterTypeService {

    @Autowired
    private RegisterTypeDAO registerTypeDAO;

    @Transactional
    public RegisterType save(RegisterType registerType) {
        return registerTypeDAO.save(registerType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegisterType> findAll() {
        return registerTypeDAO.findAll();

    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegisterType get(Long id) {
        return registerTypeDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegisterType> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setRegisterTypeDAO(RegisterTypeDAO registerTypeDAO) {

        this.registerTypeDAO = registerTypeDAO;
    }
}
