/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.accounts.payments;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.User;
import zw.co.hitrac.council.business.domain.accounts.OldReceipt;
import zw.co.hitrac.council.business.domain.accounts.PaymentMethod;
import zw.co.hitrac.council.business.domain.accounts.PaymentType;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.service.UserService;
import zw.co.hitrac.council.business.service.accounts.OldReceiptService;
import zw.co.hitrac.council.business.service.accounts.PaymentMethodService;
import zw.co.hitrac.council.business.service.accounts.PaymentTypeService;
import zw.co.hitrac.council.business.service.accounts.ProductService;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.reports.OldErrorReceiptReport;
import zw.co.hitrac.council.reports.OldReceiptReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.models.ProductListModel;
import zw.co.hitrac.council.web.pages.accounts.IAccountingPage;
import zw.co.hitrac.council.web.utility.DatePickerUtil;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 *
 * @author tdhlakama
 */
public class SearchOldReceiptPaymentPage extends IAccountingPage {

    private Long generatedReceiptNumber;
    private Date startDate, endDate;
    private User user;
    private String searchText;
    private PaymentMethod paymentMethod;
    private PaymentType paymentType;
    private Product product;
    @SpringBean
    private UserService userService;
    @SpringBean
    private OldReceiptService oldReceiptService;
    @SpringBean
    private PaymentMethodService paymentMethodService;
    @SpringBean
    private PaymentTypeService paymentTypeService;
    @SpringBean
    private ProductService productService;
    private String sourceReference;
    private String include;

    public SearchOldReceiptPaymentPage() {

        PropertyModel<Date> startDateModel = new PropertyModel<Date>(this, "startDate");
        PropertyModel<Date> endDateModel = new PropertyModel<Date>(this, "endDate");
        PropertyModel<Long> generatedReceiptNumberModel = new PropertyModel<Long>(this, "generatedReceiptNumber");
        PropertyModel<String> userModel = new PropertyModel<String>(this, "user");
        PropertyModel<String> productModel = new PropertyModel<String>(this, "product");
        PropertyModel<String> paymentMethodModel = new PropertyModel<String>(this, "paymentMethod");
        PropertyModel<String> paymentTypeModel = new PropertyModel<String>(this, "paymentType");
        PropertyModel<String> sourceReferenceModel = new PropertyModel<String>(this, "sourceReference");
        PropertyModel<String> includeModel = new PropertyModel<String>(this, "include");

        Form<?> form = new Form("form");
        form.add(new TextField<Long>("generatedReceiptNumber", generatedReceiptNumberModel));
        form.add(new TextField<Date>("startDate", startDateModel).add(DatePickerUtil.getDatePicker()));
        form.add(new TextField<Date>("endDate", endDateModel).add(DatePickerUtil.getDatePicker()));
        form.add(new DropDownChoice("user", userModel, userService.findAll()));
        form.add(new DropDownChoice("product", productModel, new ProductListModel(productService)));
        form.add(new DropDownChoice("paymentMethod", paymentMethodModel, paymentMethodService.findAll()));
        form.add(new DropDownChoice("paymentType", paymentTypeModel, paymentTypeService.findAll()));
        form.add(new DropDownChoice("sourceReference", sourceReferenceModel, oldReceiptService.getAccountSourceReferences()));
        List includelist = Arrays.asList(new String[]{"YES", "NO"});

        form.add(new DropDownChoice("include", includeModel, includelist));

        add(form);
        final IModel<List<OldReceipt>> model = new LoadableDetachableModel<List<OldReceipt>>() {
            @Override
            protected List<OldReceipt> load() {
                if (generatedReceiptNumber == null && startDate == null && endDate == null && user == null && paymentMethod == null && paymentType == null && product == null && sourceReference == null) {
                    return new ArrayList<OldReceipt>();
                } else {
                    Boolean canceled = null;
                    if (include != null) {
                        if (include.equals("YES")) {
                            canceled = Boolean.TRUE;
                        } else {
                            canceled = Boolean.FALSE;
                        }
                    }
                    return oldReceiptService.getReceipts(generatedReceiptNumber, product, paymentMethod, paymentType, startDate, endDate, user, canceled, null, sourceReference);
                }
            }
        };
        //Registrant List Data View Panel
        add(new OldPaymentDataListPanel("paymentDataListPanel", model));
        add(new FeedbackPanel("feedback"));

        form.add(new Button("print") {
            @Override
            public void onSubmit() {
                try {

                    OldReceiptReport oldReceiptReport = new OldReceiptReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    String query = "";
                    String dates = "";
                    String doneBy = "";
                    String method = "";
                    String type = "";
                    String cancelled = "";
                    if (generatedReceiptNumber != null) {
                        query = ("Receipt Number - " + generatedReceiptNumber + ". ");
                    }
                    if (startDate != null && endDate != null) {
                        dates = "From - " + DateUtil.getDate(startDate) + " to " + DateUtil.getDate(endDate) + ". ";
                    }
                    if (user != null) {
                        doneBy = "Receipted By - " + user.getSurname() + " " + user.getFirstName() + ". ";
                    }
                    if (paymentMethod != null) {
                        method = "Payment Method - " + paymentMethod.getName() + ". ";
                    }
                    if (paymentType != null) {
                        type = "Payment Type - " + paymentType.getName() + ". ";
                    }
                    if (include == null) {
                        cancelled = " Report Includes - 'CANCELED' and NON CANCELED Receipts  ";
                    } else if (include.equals("YES")) {
                        cancelled = " Report Includes - 'CANCELED' Receipts ONLY ";
                    } else if (include.equals("NO")) {
                        cancelled = " Report Includes - NON CANCELED Receipts ONLY  ";
                    }
                    Boolean canceled = null;
                    if (include != null) {
                        if (include.equals("YES")) {
                            canceled = Boolean.TRUE;
                        } else {
                            canceled = Boolean.FALSE;
                        }
                    }
                    String sourceReferenceLabel = "";
                    if (sourceReference != null) {
                        sourceReferenceLabel = " Bank " + sourceReference;
                    }
                    parameters.put("comments", "Payment Details * " + query.concat(method).concat(type).concat(doneBy).concat(dates).concat(cancelled).concat(sourceReferenceLabel));
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(oldReceiptReport,
                            contentType, oldReceiptService.getReceipts(generatedReceiptNumber, product, paymentMethod, paymentType, startDate, endDate, user, canceled, null, sourceReference), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("printError") {
            @Override
            public void onSubmit() {
                try {

                    OldErrorReceiptReport oldReceiptReport = new OldErrorReceiptReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    String query = "";
                    String dates = "";
                    String doneBy = "";
                    String method = "";
                    String type = "";
                    String canceled = "";
                    if (generatedReceiptNumber != null) {
                        query = ("Receipt Number - " + generatedReceiptNumber + ". ");
                    }
                    if (startDate != null && endDate != null) {
                        dates = "From - " + DateUtil.getDate(startDate) + " to " + DateUtil.getDate(endDate) + ". ";
                    }
                    if (user != null) {
                        doneBy = "Receipted By - " + user.getSurname() + " " + user.getFirstName() + ". ";
                    }
                    if (paymentMethod != null) {
                        method = "Payment Method - " + paymentMethod.getName() + ". ";
                    }
                    if (paymentType != null) {
                        type = "Payment Type - " + paymentType.getName() + ". ";
                    }
                    if (include == null) {
                        canceled = " Report Includes - 'CANCELED' and NON CANCELED Receipts  ";
                    } else if (include.equals("YES")) {
                        canceled = " Report Includes - 'CANCELED' Receipts ONLY ";
                    } else if (include.equals("NO")) {
                        canceled = " Report Includes - NON CANCELED Receipts ONLY  ";
                    }
                    Boolean cancelled = null;
                    if (include != null) {
                        if (include.equals("YES")) {
                            cancelled = Boolean.TRUE;
                        } else {
                            cancelled = Boolean.FALSE;
                        }
                    }
                    String sourceReferenceLabel = "";
                    if (sourceReference != null) {
                        sourceReferenceLabel = " Bank " + sourceReference;
                    }
                    parameters.put("comments", "Payment Details * " + query.concat(method).concat(type).concat(doneBy).concat(dates).concat(canceled).concat(sourceReferenceLabel));
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(oldReceiptReport,
                            contentType, oldReceiptService.getReceiptErrors(generatedReceiptNumber, product, paymentMethod, paymentType, startDate, endDate, user, cancelled, null, sourceReference), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    public Long getGeneratedReceiptNumber() {
        return generatedReceiptNumber;
    }

    public void setGeneratedReceiptNumber(Long generatedReceiptNumber) {
        this.generatedReceiptNumber = generatedReceiptNumber;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public PaymentMethodService getPaymentMethodService() {
        return paymentMethodService;
    }

    public void setPaymentMethodService(PaymentMethodService paymentMethodService) {
        this.paymentMethodService = paymentMethodService;
    }

    public PaymentTypeService getPaymentTypeService() {
        return paymentTypeService;
    }

    public void setPaymentTypeService(PaymentTypeService paymentTypeService) {
        this.paymentTypeService = paymentTypeService;
    }

    public String getInclude() {
        return include;
    }

    public void setInclude(String include) {
        this.include = include;
    }
}
