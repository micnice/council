/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.PenaltyTypeDAO;
import zw.co.hitrac.council.business.domain.PenaltyType;
import zw.co.hitrac.council.business.service.PenaltyTypeService;

import java.util.List;

/**
 *
 * @author kelvin
 */
@Service
@Transactional
public class PenaltyTypeServiceImpl  implements PenaltyTypeService{
  @Autowired
    private PenaltyTypeDAO penaltyTypeDAO;
    

    @Transactional
    public PenaltyType save(PenaltyType penaltyType) {
       return penaltyTypeDAO.save(penaltyType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<PenaltyType> findAll() {
        return penaltyTypeDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public PenaltyType get(Long id) {
       return penaltyTypeDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<PenaltyType> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setPenaltyTypeDAO(PenaltyTypeDAO penaltyTypeDAO) {

        this.penaltyTypeDAO = penaltyTypeDAO;
    }
    
}
