package zw.co.hitrac.council.business.process;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.User;
import zw.co.hitrac.council.business.domain.accounts.*;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.accounts.*;
import zw.co.hitrac.council.business.utils.CouncilException;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author Charles Chigoriwa
 * @author Matiashe Michael
 */
@Service
public class InvoiceProcess {

    @Autowired
    private TransactionHeaderService transactionHeaderService;
    @Autowired
    private TransactionComponentService transactionComponentService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private DocumentService documentService;
    @Autowired
    private DocumentItemService documentItemService;
    @Autowired
    private DebtComponentService debtComponentService;
    @Autowired
    private AccountsParametersService accountsParametersService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private ReceiptItemService receiptItemService;
    @Autowired
    private ReceiptHeaderService receiptHeaderService;
    @Autowired
    private PaymentDetailsService paymentDetailsService;
    @Autowired
    private RenewalProcess renewalProcess;
    @Autowired
    private GeneralParametersService generalParametersService;

    @Transactional
    public void invoice(Customer customer, Document document, List<DocumentItem> documentItems) {
        TransactionType invoiceTransactionType = this.accountsParametersService.get().getInvoiceTransactionType();
        saveInvoiceDocument(customer, document, documentItems);
        for (DocumentItem documentItem : documentItems) {
            customer = this.customerService.get(customer.getId());
            Account account = accountService.get(customer.getAccount().getId());
            BigDecimal initialBalance = account.getBalance();
            BigDecimal price = documentItem.getPrice();
            //Just Value
            price = price.multiply(new BigDecimal(documentItem.getQuantity()));
            Account salesAccount = documentItem.getAccount();
            processCommonInvoiceOperations(invoiceTransactionType, salesAccount, customer, price, initialBalance, documentItem.getProduct());
        }
    }

    @Transactional
    public DebtComponent invoice(BigDecimal price, Customer customer, Account salesAccount, Product product) {

        TransactionType invoiceTransactionType = this.accountsParametersService.get().getInvoiceTransactionType();

        //Save invoice document now
        saveInvoiceDocument(price, customer, salesAccount, product);

        //Check whether the client has credit/extra funds in his/her account
        Account account = accountService.get(customer.getAccount().getId());
        BigDecimal initialBalance = account.getBalance();

        DebtComponent debtComponent = processCommonInvoiceOperations(invoiceTransactionType, salesAccount, customer, price, initialBalance, product);
        debtComponent.setDateCreated(new Date());
        return debtComponent;
    }

    private DebtComponent processCommonInvoiceOperations(TransactionType invoiceTransactionType, Account salesAccount, Customer customer, BigDecimal price, BigDecimal initialBalance, Product product) {

        //Save new balances
        saveNewBalances(invoiceTransactionType, salesAccount, customer, price);
        if (!(customer.getAccount().getBalance().compareTo(new BigDecimal("0")) == -1)) {
            //saveCustomerNewBalance(customer, invoiceTransactionType, price);
        }

        //Post now to effect double entry
        // Create DebtComponent
        DebtComponent debtComponent = post(customer, salesAccount, invoiceTransactionType, price, product);

        //alterDebtComponentRemainingBalance(debtComponent, initialBalance);
        return debtComponent;
    }

    private DebtComponent post(Customer customerAccount, Account salesAccount, TransactionType invoiceTransactionType, BigDecimal price, Product product) {

        Account drAccount = invoiceTransactionType.getDrLedger();

        TransactionHeader transactionHeader = postTransactionHeader(invoiceTransactionType);

        postTransactionComponent(transactionHeader, customerAccount.getAccount(), price, new Date());

        postTransactionComponent(transactionHeader, drAccount, price, new Date());

        TransactionComponent salesTransactionComponent = postTransactionComponent(transactionHeader, salesAccount, new BigDecimal("-1").multiply(price), new Date());

        //Done with posting but let us mark the sales tran component for debt allocation
        return saveDebtComponent(salesTransactionComponent, price, customerAccount.getAccount(), product);

    }

    private TransactionHeader postTransactionHeader(TransactionType invoiceTransactionType) {
        TransactionHeader transactionHeader = new TransactionHeader();
        transactionHeader.setDueDate(new Date());
        transactionHeader.setTransactionType(invoiceTransactionType);
        transactionHeader.setDescription(invoiceTransactionType.getDescription());
        transactionHeader = transactionHeaderService.save(transactionHeader);
        return transactionHeader;
    }

    private TransactionComponent postTransactionComponent(TransactionHeader transactionHeader, Account account, BigDecimal amount, Date dueDate) {
        TransactionComponent transactionComponent = new TransactionComponent();
        transactionComponent.setTransactionHeader(transactionHeader);
        transactionComponent.setDueDate(dueDate);
        transactionComponent.setAccount(account);
        transactionComponent.setAmount(amount);
        transactionComponentService.save(transactionComponent);
        return transactionComponent;
    }

    private DebtComponent saveDebtComponent(TransactionComponent salesTransactionComponent, BigDecimal amount, Account personalAccount, Product product) {
        DebtComponent debtComponent = new DebtComponent();
        debtComponent.setRemainingBalance(amount);
        debtComponent.setTransactionComponent(salesTransactionComponent);
        debtComponent.setAccount(personalAccount);
        debtComponent.setProduct(product);
        debtComponent.setDateCreated(new Date());
        debtComponentService.save(debtComponent);
        return debtComponent;
    }

    public void saveNewBalances(TransactionType invoiceTransactionType, Account salesAccount, Customer customerAccount, BigDecimal amount) {
        if (salesAccount == null) {
            throw new CouncilException("Contact Administrator - Sales Account not set");
        }
        Account drAccount = accountService.get(invoiceTransactionType.getDrLedger().getId());
        drAccount.debit(amount);

        accountService.save(drAccount);
        Account crAccount = accountService.get(invoiceTransactionType.getCrLedger().getId());
        crAccount.credit(amount);
        accountService.save(crAccount);
        Account refreshedSalesAccount = accountService.get(salesAccount.getId());
        refreshedSalesAccount.credit(amount);
        accountService.save(refreshedSalesAccount);
    }

    public void saveCustomerNewBalance(Customer customer, TransactionType invoiceTransactionType, BigDecimal price) {
        if (invoiceTransactionType == null) {
            throw new CouncilException("Invoice transaction type not set up !!!");
        }

        if (invoiceTransactionType.getEffect().equals(Effect.DR)) {
            Account account = accountService.get(customer.getAccount().getId());
            account.debit(price);
            accountService.save(account);
        } else {
            throw new CouncilException("Invalid effect on invoice transaction type");
        }

    }

    private void saveInvoiceDocument(BigDecimal price, Customer customer, Account salesAccount, Product product) {
        Document document = new Document();
        document.setCustomer(customer);
        document.setDate(new Date());
        document.setDocumentType(DocumentType.INVOICE);
        documentService.save(document);

        DocumentItem documentItem = new DocumentItem();
        documentItem.setAccount(salesAccount);
        documentItem.setDocument(document);
        documentItem.setPrice(price);
        documentItem.setProduct(product);
        documentItem.setQuantity(1);
        documentItemService.save(documentItem);
    }

    private void saveInvoiceDocument(Customer customer, Document document, List<DocumentItem> documentItems) {
        document.setCustomer(customer);
        document.setDate(new Date());
        document.setDocumentType(DocumentType.INVOICE);
        documentService.save(document);

        for (DocumentItem documentItem : documentItems) {
            documentItem.setDocument(document);
            documentItemService.save(documentItem);
        }
    }

    public ReceiptItem alterDebtComponentRemainingBalance(DebtComponent debtComponent, BigDecimal initialBalance, Boolean payThroughInstitution) {
        //need Current user to save
        ReceiptItem receiptItem = null;
        if (initialBalance.compareTo(new BigDecimal("0")) == -1) {

            final int compareResult = initialBalance.multiply(new BigDecimal("-1")).compareTo(debtComponent.getRemainingBalance());

            if (compareResult >= 0) {
                receiptItem = new ReceiptItem();
                receiptItem.setAmount(debtComponent.getRemainingBalance());
                receiptItem.setDateCreated(new Date());

                if (!payThroughInstitution) {
                    Account account = null;
                    account = accountService.get(debtComponent.getAccount().getId());
                    account.debit(receiptItem.getAmount());
                    accountService.save(account);
                }

                receiptItem.setDebtComponent(debtComponent);
                if (receiptItem.getPaymentPeriod() == null && debtComponent.getPaymentPeriod() != null) {
                    receiptItem.setPaymentPeriod(debtComponent.getPaymentPeriod());
                }
                receiptItemService.save(receiptItem);
                debtComponent.setRemainingBalance(new BigDecimal("0"));

            } else if (compareResult == -1) {
                receiptItem = new ReceiptItem();
                BigDecimal amount = debtComponent.getRemainingBalance();
                receiptItem.setDateCreated(new Date());
                BigDecimal remainingBalance = debtComponent.getRemainingBalance().subtract(initialBalance.multiply(new BigDecimal("-1")));
                debtComponent.setRemainingBalance(remainingBalance);

                receiptItem.setAmount(amount.subtract(remainingBalance));
                //////////////////////////////////////////////////////////////
                if (!payThroughInstitution) {
                    Account account = null;
                    account = accountService.get(debtComponent.getAccount().getId());
                    account.debit(receiptItem.getAmount());
                    accountService.save(account);
                }
                //////////////////////////////////////////////////////////////
                receiptItem.setDebtComponent(debtComponent);
                if (receiptItem.getPaymentPeriod() == null && debtComponent.getPaymentPeriod() != null) {
                    receiptItem.setPaymentPeriod(debtComponent.getPaymentPeriod());
                }
                receiptItemService.save(receiptItem);
            }

            debtComponentService.save(debtComponent);

        }

        return receiptItem;
    }

    @Transactional
    public PaymentDetails payThroughInstitution(Prepayment prepayment, Set<ReceiptItem> receiptItems, User user, Customer customer, Institution institution) {

        for (ReceiptItem receiptItem : receiptItems) {
            if (receiptItem.getPaymentPeriod() == null) {
                throw new CouncilException("Select Payment Period");
            }
        }
        BigDecimal amountUsed = new BigDecimal(0);
        for (ReceiptItem receiptItem : receiptItems) {
            receiptItem.setDateCreated(new Date());
            amountUsed = amountUsed.add(receiptItem.getAmount());
            receiptItem.setCreatedBy(user);
            receiptItemService.save(receiptItem);
        }
        PaymentDetails paymentDetails = new PaymentDetails();
        paymentDetails.setTransactionHeader(new TransactionHeader());
        paymentDetails.setReceiptHeader(new ReceiptHeader());
        paymentDetails.getReceiptHeader().setReceiptItems(receiptItems);
        paymentDetails.getReceiptHeader().setTotalAmount(amountUsed);

        paymentDetails.getReceiptHeader().setTotalAmountPaid(amountUsed);
        paymentDetails.getReceiptHeader().setDate(new Date());
        paymentDetails.getReceiptHeader().setCreatedBy(user);
        paymentDetails.getReceiptHeader().setPrinted(Boolean.FALSE);
        paymentDetails.setCustomer(customer);
        paymentDetails.setCreatedBy(user);
        paymentDetails.setDateCreated(new Date());
        paymentDetails.getReceiptHeader().setDateCreated(new Date());
        paymentDetails.setDateOfPayment(new Date());
        paymentDetails.setDateOfDeposit(prepayment.getDateOfDeposit());
        TransactionHeader transactionHeader = paymentDetails.getTransactionHeader();

        TransactionType receiptTransactionType = prepayment.getTransactionType();
        transactionHeader.setTransactionType(receiptTransactionType);
        transactionHeader.setDueDate(new Date());
        transactionHeader.setDescription(receiptTransactionType.getDescription());
        transactionHeaderService.save(transactionHeader);

        TransactionComponent transactionComponent = new TransactionComponent();
        transactionComponent.setAmount(amountUsed);
        transactionComponent.setTransactionHeader(transactionHeader);
        transactionComponent.setAccount(receiptTransactionType.getDrLedger());
        transactionComponent.setDueDate(new Date());
        transactionComponentService.save(transactionComponent);

        transactionComponent = new TransactionComponent();
        transactionComponent.setAccount(receiptTransactionType.getCrLedger());
        transactionComponent.setAmount(amountUsed.multiply(new BigDecimal("-1")));
        transactionComponent.setDueDate(new Date());
        transactionComponent.setTransactionHeader(transactionHeader);
        transactionComponentService.save(transactionComponent);

        transactionComponent = new TransactionComponent();
        transactionComponent.setAccount(receiptTransactionType.getCrLedger());
        transactionComponent.setAmount(amountUsed.multiply(new BigDecimal("-1")));
        transactionComponent.setDueDate(new Date());
        transactionComponent.setTransactionHeader(transactionHeader);
        transactionComponentService.save(transactionComponent);
        paymentDetails.setTransactionHeader(transactionHeader);
        paymentDetails.setPaymentMethod(prepayment.getTransactionType().getPaymentMethod());
        Account bankAccount = prepayment.getTransactionType().getDrLedger();
        paymentDetails.setBankAccount(bankAccount);
        paymentDetails.getReceiptHeader().setTransactionType(receiptTransactionType);
        receiptHeaderService.save(paymentDetails.getReceiptHeader());
        for (ReceiptItem receiptItem : receiptItems) {
            receiptItem.setReceiptHeader(paymentDetails.getReceiptHeader());
            receiptItemService.save(receiptItem);
        }
        transactionHeaderService.save(paymentDetails.getTransactionHeader());
        paymentDetails = paymentDetailsService.save(paymentDetails);
        paymentDetails.getReceiptHeader().setPaymentDetails(paymentDetails);
        paymentDetails.getReceiptHeader().setTransactionType(receiptTransactionType);
        receiptHeaderService.save(paymentDetails.getReceiptHeader());

        if (generalParametersService.get().getAllowRenewalBeforeCPDs()) {
            final Registrant registrant = customerService.getRegistrant(customer.getAccount());
            renewalProcess.createRenewalPaidForCurrentPeriod(registrant);
        }

        return paymentDetails;
    }

}
