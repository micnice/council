/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain;

import java.text.DateFormatSymbols;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author hitrac
 */
public enum Month {

    JANUARY("January"),
    FEBRUARY("February"),
    MARCH("March"),
    APRIL("April"),
    MAY("May"),
    JUNE("June"),
    JULY("July"),
    AUGUST("August"),
    SEPTEMBER("September"),
    OCTOBER("October"),
    NOVEMBER("November"),
    DECEMBER("December");
    private String name;

    private Month(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    
    public int getmonth() {

        List<String> monthNames = Arrays.asList(new DateFormatSymbols().getMonths());
        return monthNames.indexOf(name);
    }
}
