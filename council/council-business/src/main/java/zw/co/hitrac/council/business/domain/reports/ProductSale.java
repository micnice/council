/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain.reports;

import java.io.Serializable;
import java.math.BigDecimal;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.domain.accounts.PaymentDetails;
import zw.co.hitrac.council.business.domain.accounts.Product;

/**
 *
 * @author tdhlakama
 */
public class ProductSale implements Serializable {

    private Registrant registrant;
    private Course course;
    private Institution institution;
    private Product product;
    private DebtComponent debtComponent;
    private BigDecimal amountPaid = BigDecimal.ZERO;
    private PaymentDetails paymentDetail;
    private BigDecimal carryForward = BigDecimal.ZERO;
    private String customer;
    private BigDecimal paper1Amount = BigDecimal.ZERO;
    private BigDecimal paper2Amount = BigDecimal.ZERO;
    private String comment = "";

    public ProductSale() {
    }
    
    public Registrant getRegistrant() {
        return registrant;
    }

    public void setRegistrant(Registrant registrant) {
        this.registrant = registrant;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public DebtComponent getDebtComponent() {
        return debtComponent;
    }

    public void setDebtComponent(DebtComponent debtComponent) {
        this.debtComponent = debtComponent;
    }

    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    public BigDecimal getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }

    public PaymentDetails getPaymentDetail() {
        return paymentDetail;
    }

    public void setPaymentDetail(PaymentDetails paymentDetail) {
        this.paymentDetail = paymentDetail;
    }

    public BigDecimal getCarryForward() {
        return carryForward;
    }

    public void setCarryForward(BigDecimal carryForward) {
        this.carryForward = carryForward;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Boolean getShow() {
        if (amountPaid.equals(BigDecimal.ZERO)) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }
    }

    public String getCustomer() {
        if (registrant != null) {
            return registrant.getFullname();
        }
        if (institution != null) {
            return institution.getName();
        }
        return "";
    }

    public BigDecimal getPaper1Amount() {
        return paper1Amount;
    }

    public void setPaper1Amount(BigDecimal paper1Amount) {
        this.paper1Amount = paper1Amount;
    }

    public BigDecimal getPaper2Amount() {
        return paper2Amount;
    }

    public void setPaper2Amount(BigDecimal paper2Amount) {
        this.paper2Amount = paper2Amount;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    
}
