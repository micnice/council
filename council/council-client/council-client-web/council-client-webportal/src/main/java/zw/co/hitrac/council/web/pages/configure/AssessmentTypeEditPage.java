/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.AssessmentType;
import zw.co.hitrac.council.business.service.AssessmentTypeService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author kelvin
 */
public class AssessmentTypeEditPage extends IAdministerDatabaseBasePage{
    
    @SpringBean
    
    private AssessmentTypeService addressTypeService;

    public AssessmentTypeEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<AssessmentType>(new LoadableDetachableAssessmentModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink",AdministerDatabasePage.class));
        Form<AssessmentType> form=new Form<AssessmentType>("form",(IModel<AssessmentType>)getDefaultModel()) {
            @Override
            public void onSubmit(){
                addressTypeService.save(getModelObject());
                setResponsePage(new AssessmentTypeViewPage(getModelObject().getId()));
            }
        };
        
        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior())); 
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink",AssessmentTypeListPage.class));
        add(form);
    }
    
    
    private final class LoadableDetachableAssessmentModel extends LoadableDetachableModel<AssessmentType> {

        private Long id;

        public LoadableDetachableAssessmentModel(Long id) {
            this.id = id;
        }

        @Override
        protected AssessmentType load() {
            if(id==null){
                return new AssessmentType();
            }
            return addressTypeService.get(id);
        }
    }
}
    