package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.EmailMessage;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;

import java.util.Collection;

/**
 *
 * @author Charles Chigoriwa
 */
public interface EmailMessageService extends IGenericService<EmailMessage> {

    void send(RegistrantData registrantData, final String subject, final String message);

    void send(Collection<RegistrantData> registrantDataList, final String subject, final String message);

    void send(String email, final String subject, final String message);

}
