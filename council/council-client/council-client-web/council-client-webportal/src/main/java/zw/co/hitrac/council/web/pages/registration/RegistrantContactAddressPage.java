package zw.co.hitrac.council.web.pages.registration;

import org.apache.commons.lang.BooleanUtils;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.accounts.AccountsParametersService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.application.ApplicationPage;
import zw.co.hitrac.council.web.pages.examinations.ExamRegistrationPage;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;

/**
 *
 * @author Clive Gurure
 */
public class RegistrantContactAddressPage extends TemplatePage {

    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private AccountsParametersService accountsParametersService;

    public RegistrantContactAddressPage(PageParameters parameters) {
        this(parameters.get("registrantId").toLong());
    }

    public RegistrantContactAddressPage(Long id) {
        
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(id);
        final CompoundPropertyModel<Registrant> model = new CompoundPropertyModel<Registrant>(new DetachableRegistrantModel(id, registrantService));
        add(new RegistrantPanel("registrantPanel", id));
        setDefaultModel(model);
        
        add(new RegistrantContactPanelList("registrantContactPanelList", model));

        add(new RegistrantAddressPanelList("registrantAddressPanelList", model));
        // add(new RegistrantQualificationPanelList1("registrantQualificationPanelList1", model));

        add(new Link<Void>("qualificationLink") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantQualificationEmploymentPage(model.getObject().getId()));
            }
        });

        add(new Link<Registrant>("examRegistrationLink") {
            @Override
            public void onClick() {
                setResponsePage(new ExamRegistrationPage(model));
            }

            @Override
            protected void onConfigure() {

                if (BooleanUtils.isTrue(generalParametersService.get().getExaminationModule())
                                                || generalParametersService.get().getRegistrationByApplication()
                                                || generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.FALSE);
                } else {
                    setVisible(Boolean.TRUE);
                }
            }
        });

        add(new Link<Registrant>("applicationLink") {
            @Override
            public void onClick() {
                setResponsePage(new ApplicationPage(model));
            }

            @Override
            protected void onConfigure() {
                if (accountsParametersService.get() != null) {
                    setVisible(accountsParametersService.get().getStudentRegistrationPayment());
                } else {
                    throw new CouncilException("Check Accounts Parametrers (Student registration payment)");
                }
            }
        });

    }
}
