package zw.co.hitrac.council.web.pages.accounts;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.Bank;
import zw.co.hitrac.council.business.service.accounts.BankService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Michael Matiashe
 */
public class BankAccountEditPage extends IBankBookPage {

    @SpringBean
    private BankService bankAccountService;

    public BankAccountEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<Bank>(new LoadableDetachableBankAccountModel(id)));
        Form<Bank> form = new Form<Bank>("form", (IModel<Bank>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                bankAccountService.save(getModelObject());
                setResponsePage(new BankAccountViewPage(getModelObject().getId()));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("accNumber").add(new ErrorBehavior()));
        form.add(new TextField<String>("bank").add(new ErrorBehavior()));
        form.add(new TextField<String>("branch").add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink", BankAccountListPage.class));
        add(form);
    }

    private final class LoadableDetachableBankAccountModel extends LoadableDetachableModel<Bank> {

        private Long id;

        public LoadableDetachableBankAccountModel(Long id) {
            this.id = id;
        }

        @Override
        protected Bank load() {
            if (id == null) {
                return new Bank();
            }
            return bankAccountService.get(id);
        }
    }
}
