package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.Conditions;

/**
 *
 * @author Michael Matiashe
 */
public interface ConditionRepository extends CrudRepository<Conditions, Long> {
    public List<Conditions> findAll();
}
