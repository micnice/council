package zw.co.hitrac.council.web.pages.institution;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.service.RegistrantAssessmentService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

//~--- JDK imports ------------------------------------------------------------

import java.util.Arrays;
import java.util.List;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.RegistrantAssessment;
import zw.co.hitrac.council.web.utility.CustomDateTextField;

/**
 *
 * @author Constance Mabaso
 */
public class InstitutionAssessmentPage extends TemplatePage {

    @SpringBean
    private RegistrantAssessmentService registrantAssessmentService;

    public InstitutionAssessmentPage(final IModel<Application> applicationModel) {
        menuPanelManager.getApplicationPanel().setTopMenuCurrent(true);
        CompoundPropertyModel<RegistrantAssessment> model = new CompoundPropertyModel<RegistrantAssessment>(
                new InstitutionAssessmentPage.LoadableDetachableRegistrantAssessmentModel(applicationModel.getObject()));

        model.getObject().setApplication(applicationModel.getObject());
        model.getObject().setInstitution(applicationModel.getObject().getInstitution());
        setDefaultModel(model);

        Form<RegistrantAssessment> form = new Form<RegistrantAssessment>("form",
                (IModel<RegistrantAssessment>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                registrantAssessmentService.save(getModelObject());
                setResponsePage(new InstitutionApplicationViewPage(applicationModel.getObject().getId()));
            }
        };

        form.add(new TextField<String>("assessor").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextArea<String>("comments"));
        form.add(new CustomDateTextField("date").add(DatePickerUtil.getDatePicker()));
        
        List decisionlist = Arrays.asList(new String[]{RegistrantAssessment.DECISIONPENDING,
            RegistrantAssessment.DECISIONAPPROVED, RegistrantAssessment.DECISIONDECLINED, Application.APPLICATIONUNDERREVIEW});

        form.add(new DropDownChoice("decisionStatus", decisionlist));

        form.add(new Link<Void>("institutionViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionApplicationViewPage(applicationModel.getObject().getId()));
            }
        });
        add(form);
        add(new Label("application.institution.name"));
    }

    public InstitutionAssessmentPage(Long id) {
        menuPanelManager.getApplicationPanel().setTopMenuCurrent(true);
        CompoundPropertyModel<RegistrantAssessment> model =
                new CompoundPropertyModel<RegistrantAssessment>(
                new InstitutionAssessmentPage.LoadableDetachableRegistrantAssessmentModel(id));
        final Long applicationId = model.getObject().getApplication().getId();
        setDefaultModel(model);

        Form<RegistrantAssessment> form = new Form<RegistrantAssessment>("form",
                (IModel<RegistrantAssessment>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                registrantAssessmentService.save(getModelObject());
                setResponsePage(new InstitutionApplicationViewPage(applicationId));
            }
        };

        form.add(new Link<Void>("institutionViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionApplicationViewPage(applicationId));
            }
        });
       form.add(new TextField<String>("assessor").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextArea<String>("comments"));
        form.add(new CustomDateTextField("date").add(DatePickerUtil.getDatePicker()));
        
        List decisionlist = Arrays.asList(new String[]{RegistrantAssessment.DECISIONPENDING,
            RegistrantAssessment.DECISIONAPPROVED, RegistrantAssessment.DECISIONDECLINED, Application.APPLICATIONUNDERREVIEW});

        form.add(new DropDownChoice("decisionStatus", decisionlist));
        add(form);
        add(new Label("application.institution.name"));
    }

    private final class LoadableDetachableRegistrantAssessmentModel
            extends LoadableDetachableModel<RegistrantAssessment> {

        private Long id;
        private Application application;

        public LoadableDetachableRegistrantAssessmentModel(Application application) {
            this.application = application;
        }

        public LoadableDetachableRegistrantAssessmentModel(Long id) {
            this.id = id;
        }

        @Override
        protected RegistrantAssessment load() {
            RegistrantAssessment registrantAssessment = null;

            if (id == null) {
                registrantAssessment = new RegistrantAssessment();
                registrantAssessment.setApplication(application);
            } else {
                registrantAssessment = registrantAssessmentService.get(id);
            }

            return registrantAssessment;
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
