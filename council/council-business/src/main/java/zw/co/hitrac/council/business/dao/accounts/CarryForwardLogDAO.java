package zw.co.hitrac.council.business.dao.accounts;

import java.util.Date;
import java.util.List;
import zw.co.hitrac.council.business.dao.*;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.CarryForwardLog;
import zw.co.hitrac.council.business.domain.accounts.Customer;

/**
 *
 * @author Charles Chigoriwa
 */
public interface CarryForwardLogDAO extends IGenericDAO<CarryForwardLog> {
    public List<CarryForwardLog> carryForwardLogs();
    public List<CarryForwardLog> getRegistrantCarryForwardLogs(Customer customer);
    public List<CarryForwardLog> carryForwardLogs(Registrant registrant, Date startDate, Date endDate);
}
