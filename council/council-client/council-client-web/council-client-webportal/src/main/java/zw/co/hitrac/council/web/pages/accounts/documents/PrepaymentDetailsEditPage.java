package zw.co.hitrac.council.web.pages.accounts.documents;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.Prepayment;
import zw.co.hitrac.council.business.domain.accounts.PrepaymentDetails;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.accounts.PrepaymentDetailsService;
import zw.co.hitrac.council.business.service.accounts.PrepaymentService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.PrepaymentListModel;
import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author Michael Matiashe
 */
public class PrepaymentDetailsEditPage extends TemplatePage {

    @SpringBean
    private PrepaymentService prepaymentService;
    @SpringBean
    private PrepaymentDetailsService prepaymentDetailsService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private RegistrantService registrantService;
    private Institution institution;

    public PrepaymentDetailsEditPage(final Long id) {
        final PrepaymentDetails prepaymentDetails = prepaymentDetailsService.get(id);
        
        CompoundPropertyModel<PrepaymentDetails> prepaymentDetailsModel = new CompoundPropertyModel<PrepaymentDetails>(new LoadableDetachablePrepaymentDetailsModel(id));

        setDefaultModel(prepaymentDetailsModel);

        System.out.println("=================++++" + prepaymentDetailsModel.getObject().getRegistrant().getFullname());
        Form<PrepaymentDetails> form = new Form<PrepaymentDetails>("form", (IModel<PrepaymentDetails>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                System.out.println("not tried ====================");
                try {
                    
                    System.out.println("-----------"+ getModelObject().getPaymentDetails().getReceiptHeader().getTotalAmount());
                    System.out.println("++++++++++++"+getModelObject().getPrepayment().getAmount() );
                    if(getModelObject().getPaymentDetails().getReceiptHeader().getTotalAmount().compareTo(getModelObject().getPrepayment().getAmount())==1){
                        System.out.println("--------------------lost");
                        throw new CouncilException("The payment amount exceeds prepayment amount");
                        
                    }
                    if (getModelObject().getPrepayment().equals(prepaymentDetails.getPrepayment())){
                        System.out.println("--------------------++lost");
                        throw  new CouncilException("Prepayment Not Changed");
                    }
                    getModelObject().setChangedInstitution(prepaymentDetails.getInstitution());
                    getModelObject().setChangedPrepayment(prepaymentDetails.getPrepayment());
                    getModelObject().setDateModified(new Date());
                    getModelObject().setModifiedBy(CouncilSession.get().getUser());
                    prepaymentDetailsService.save(getModelObject());
                    setResponsePage(new PrepaymentDetailListPage(getModelObject().getRegistrant()));
                } catch (Exception e) {
                    System.out.println("-------------exception 1");
                    e.getMessage();
                }
            }
        };

        form.add(new Label("registrant.fullname"));
        DropDownChoice<Institution> institutions = new DropDownChoice<Institution>("institution",
                new PropertyModel<Institution>(this, "institution"), new PrepaymentDetailsEditPage.InstitutionModel(),
                new ChoiceRenderer<Institution>("name", "id")) {
                    @Override
                    protected boolean wantOnSelectionChangedNotifications() {
                        return true;
                    }

                    @Override
                    protected void onSelectionChanged(Institution newSelection) {
                        PrepaymentDetails prepaymentDetails = (PrepaymentDetails) PrepaymentDetailsEditPage.this.getDefaultModelObject();

                        prepaymentDetails.setPrepayment(null);
                    }
                };

        institutions.setRequired(true).add(new ErrorBehavior());
        form.add(institutions);

        DropDownChoice<Prepayment> prepayments = new DropDownChoice<Prepayment>("prepayment", new PrepaymentDetailsEditPage.PrepaymentModel());
        form.add(prepayments);
        add(new FeedbackPanel("feedback"));
        add(form);
    }

    private final class LoadableDetachablePrepaymentDetailsModel extends LoadableDetachableModel<PrepaymentDetails> {

        private Long id;

        public LoadableDetachablePrepaymentDetailsModel(Long id) {
            this.id = id;
        }

        @Override
        protected PrepaymentDetails load() {
            PrepaymentDetails prepaymentDetails = prepaymentDetailsService.get(id);
            prepaymentDetails.setRegistrant(registrantService.getRegistrant(prepaymentDetails.getCustomer()));
            return prepaymentDetails;
        }
    }

    private class PrepaymentModel extends LoadableDetachableModel<List<? extends Prepayment>> {

        protected List<? extends Prepayment> load() {
            if (institution != null) {
                return new PrepaymentListModel(prepaymentService, institution).getObject();
            } else {
                return new ArrayList<Prepayment>();
            }
        }
    }

    private final class InstitutionModel extends LoadableDetachableModel<List<? extends Institution>> {

        protected List<? extends Institution> load() {
            return prepaymentService.withPrepayments();
        }
    }

}
