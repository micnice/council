/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages;

import java.util.List;
import java.util.Optional;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.StatelessForm;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zw.co.hitrac.council.business.domain.WebUser;
import zw.co.hitrac.council.business.service.EmailMessageService;
import zw.co.hitrac.council.business.service.WebUserService;

/**
 *
 * @author ezinzombe
 */
public class WebUserForgotPasswordPage extends WebUserTemplatePage {

    @SpringBean
    private WebUserService webUserService;
    @SpringBean
    private EmailMessageService emailMessageService;

    private static final Logger logger = LoggerFactory.getLogger(WebUserForgotPasswordPage.class);

    public WebUserForgotPasswordPage() {
        add(new SignInForm("signInForm"));
    }

    /**
     * Sign in form
     */
    public final class SignInForm extends StatelessForm<Void> {

        private transient String email = "";

        /**
         * Constructor
         *
         * @param id id of the form component
         */
        public SignInForm(final String id) {
            super(id);
            add(new FeedbackPanel("errorMessage"));

            add(new TextField<String>("email", new PropertyModel<>(this, "email")));

        }

        /**
         * @see org.apache.wicket.markup.html.form.Form#onSubmit()
         */
        @Override
        public final void onSubmit() {
            // Sign the user in
            Optional<WebUser> userOptional = Optional.ofNullable(webUserService.getByUsername(email));
            if (userOptional.isPresent()) {
                final String password = "NewUsrX21";
                emailMessageService.send(email, "PCZ Registration", "Password : NewUsrX");
                webUserService.changeWebUserPassword(userOptional.get(), password);
            } else {
                // Get the error message from the properties file associated with the Component
                String errmsg = getString("loginError", null, "User name not found");
                // Register the error message with the feedback panel
                error(errmsg);
            }
        }

    }
}
