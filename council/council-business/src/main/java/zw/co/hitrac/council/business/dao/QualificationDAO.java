package zw.co.hitrac.council.business.dao;

import java.util.List;
import zw.co.hitrac.council.business.domain.ProductIssuanceType;
import zw.co.hitrac.council.business.domain.Qualification;

/**
 *
 * @author Michael Matiashe
 */
public interface QualificationDAO extends IGenericDAO<Qualification> {

    public List<Qualification> getQualificationCertificateType(ProductIssuanceType productIssuanceType);
	public Qualification findByName(String name);
}
