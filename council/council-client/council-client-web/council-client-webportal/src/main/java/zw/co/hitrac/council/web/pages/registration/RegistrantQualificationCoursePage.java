package zw.co.hitrac.council.web.pages.registration;

//~--- non-JDK imports --------------------------------------------------------

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.wicket.authroles.authorization.strategies.role.Roles;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.domain.ProductIssuanceType;
import zw.co.hitrac.council.business.domain.Qualification;
import zw.co.hitrac.council.business.domain.QualificationType;

import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantQualification;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.process.BillingProcess;
import zw.co.hitrac.council.business.process.ProductIssuer;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.process.RenewalProcess;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.DurationService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.QualificationService;
import zw.co.hitrac.council.business.service.RegistrantQualificationService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.AcademicInstitutionstModel;
import zw.co.hitrac.council.web.models.CourseListModel;
import zw.co.hitrac.council.web.models.ExternalInstitutionModel;
import zw.co.hitrac.council.web.models.QualificationListModel;
import zw.co.hitrac.council.web.models.TrainingInstitutionstModel;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.accounts.documents.DebtComponentsPage;
import zw.co.hitrac.council.web.pages.institution.InstitutionEditPage;
import zw.co.hitrac.council.web.utility.CustomDateTextField;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;

/**
 * @author Tatenda Chiwandire
 */
public class RegistrantQualificationCoursePage extends TemplatePage {

    @SpringBean
    private QualificationService qualificationService;
    @SpringBean
    private RegistrantQualificationService registrantQualificationService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private BillingProcess billingProcess;
    @SpringBean
    private CourseService courseService;
    @SpringBean
    private ProductIssuer productIssuer;
    @SpringBean
    private RegistrationProcess registrationProcess;
    @SpringBean
    private RenewalProcess renewalProcess;
    @SpringBean
    private DurationService durationService;
    private Course course;

    public RegistrantQualificationCoursePage(final IModel<Registrant> registrantModel, final Boolean foreign) {
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());

        CompoundPropertyModel<RegistrantQualification> model = new CompoundPropertyModel<RegistrantQualification>(
                new RegistrantQualificationCoursePage.LoadableDetachableRegistrantQualificationModel(
                        registrantModel.getObject()));
        if (foreign) {
            model.getObject().setQualificationType(QualificationType.FOREIGN);
        } else {
            model.getObject().setQualificationType(QualificationType.LOCAL);
        }
        setDefaultModel(model);
        Form<RegistrantQualification> form = new Form<RegistrantQualification>("form",
                (IModel<RegistrantQualification>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    //course implement discipline from qualification
                    if (getModelObject().getCourse() == null) {
                        if (getModelObject().getQualification().getCourse() != null) {
                            getModelObject().setCourse(getModelObject().getQualification().getCourse());
                        }
                    }
                    registrantQualificationService.save(getModelObject());
                    setResponsePage(new RegistrantQualificationEmploymentPage(getModelObject().getRegistrant().getId()));
                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };

        DropDownChoice<Qualification> qualifications = new DropDownChoice<Qualification>("qualification", new RegistrantQualificationCoursePage.QualifcationModel());

        qualifications.setRequired(true).add(new ErrorBehavior());
        form.add(qualifications);

        if (foreign) {
            form.add(new DropDownChoice("awardingInstitution", new ExternalInstitutionModel(institutionService, generalParametersService)));
            form.add(new DropDownChoice("institution", new ExternalInstitutionModel(institutionService, generalParametersService)));
        } else {
            form.add(new DropDownChoice("awardingInstitution", new AcademicInstitutionstModel(institutionService)));
            form.add(new DropDownChoice("institution", new TrainingInstitutionstModel(institutionService)));
        }
        form.add(new CustomDateTextField("dateAwarded").add(DatePickerUtil.getDatePicker()).add(new ErrorBehavior()));
        form.add(new CustomDateTextField("startDate").add(DatePickerUtil.getDatePicker()));
        form.add(new CustomDateTextField("endDate").add(DatePickerUtil.getDatePicker()));
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });
        form.add(new Label("courseLabel", "Course") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.TRUE);
            }
        });


        DropDownChoice<Course> courses = new DropDownChoice<Course>("course", new PropertyModel<Course>(this, "course"), new RegistrantQualificationCoursePage.CourseModel(),
                new ChoiceRenderer<Course>("name", "id")) {
            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }

            @Override
            protected void onSelectionChanged(Course newSelection) {

                RegistrantQualification registrantQualification = (RegistrantQualification) RegistrantQualificationCoursePage.this.getDefaultModelObject();

                registrantQualification.setQualification(null);

            }
        };


        courses.setRequired(true).add(new ErrorBehavior());
        form.add(courses);
        //form.add(courseModel);
        form.add(new DropDownChoice("qualificationType", Arrays.asList(QualificationType.values())));
        add(form);
        add(new FeedbackPanel("feedback"));
        add(new Label("registrant.fullname"));

        add(new Link<Void>("addInstitutionPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionEditPage(null, registrantModel, foreign));
            }
        });
    }

    public RegistrantQualificationCoursePage(Long id) {
        CompoundPropertyModel<RegistrantQualification> model = new CompoundPropertyModel<RegistrantQualification>(
                new RegistrantQualificationCoursePage.LoadableDetachableRegistrantQualificationModel(id));

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);

        final Long registrantId = model.getObject().getRegistrant().getId();

        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantId);
        setDefaultModel(model);

        Form<RegistrantQualification> form = new Form<RegistrantQualification>("form",
                (IModel<RegistrantQualification>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                //course implement discipline from qualification
                if (getModelObject().getCourse() == null) {
                    if (getModelObject().getQualification().getCourse() != null) {
                        getModelObject().setCourse(getModelObject().getQualification().getCourse());
                    }
                }
                registrantQualificationService.save(getModelObject());
                setResponsePage(new RegistrantQualificationEmploymentPage(getModelObject().getRegistrant().getId()));
            }
        };

        form.add(new DropDownChoice("qualification", new QualificationListModel(qualificationService)));
        form.add(new DropDownChoice("awardingInstitution", new AcademicInstitutionstModel(institutionService)));
        form.add(new DropDownChoice("institution", new TrainingInstitutionstModel(institutionService)));
        form.add(
                new CustomDateTextField("dateAwarded").setRequired(true).add(new ErrorBehavior()).add(
                        DatePickerUtil.getDatePicker()));
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantId));
            }
        });
        form.add(new CustomDateTextField("startDate").add(DatePickerUtil.getDatePicker()));
        form.add(new CustomDateTextField("endDate").add(DatePickerUtil.getDatePicker()));
        form.add(new Label("courseLabel", "Course") {
            @Override
            protected void onConfigure() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.IT_OFFICER)));
                setVisible(enabled);
            }
        });
        form.add(new DropDownChoice("course", new CourseListModel(courseService)) {
            @Override
            protected void onConfigure() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.IT_OFFICER)));
                setVisible(enabled);
            }
        });
        form.add(new DropDownChoice("qualificationType", Arrays.asList(QualificationType.values())));
        add(form);
        add(new FeedbackPanel("feedback"));
        add(new Label("registrant.fullname"));

        add(new Link<Void>("addInstitutionPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionEditPage(null, null, null));
            }

            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
    }

    public RegistrantQualificationCoursePage(final IModel<Registrant> registrantModel, final Application application) {
        CompoundPropertyModel<RegistrantQualification> model = new CompoundPropertyModel<RegistrantQualification>(
                new RegistrantQualificationCoursePage.LoadableDetachableRegistrantQualificationModel(
                        registrantModel.getObject()));

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);

        final Long registrantId = model.getObject().getRegistrant().getId();

        model.getObject().setQualification(application.getQualification());
        model.getObject().setInstitution(application.getTrainingInstitution());
        model.getObject().setAwardingInstitution(application.getInstitution());
        model.getObject().setDateAwarded(application.getDateAwarded());
        model.getObject().setStartDate(application.getQualificationStartDate());
        model.getObject().setEndDate(application.getQualificationEndDate());

        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantId);
        setDefaultModel(model);

        Form<RegistrantQualification> form = new Form<RegistrantQualification>("form",
                (IModel<RegistrantQualification>) getDefaultModel()) {
            @Override
            public void onSubmit() {

                RegistrantQualification registrantQualification = registrantQualificationService.get(registrantModel.getObject(), getModelObject().getQualification());
                if (registrantQualification != null) {
                    registrantQualification.setInstitution(getModelObject().getInstitution());
                    registrantQualification.setAwardingInstitution(getModelObject().getAwardingInstitution());
                    registrantQualification.setDateAwarded(getModelObject().getDateAwarded());
                    registrantQualification.setStartDate(getModelObject().getStartDate());
                    registrantQualification.setEndDate(getModelObject().getEndDate());
                    registrantQualificationService.save(registrantQualification);
                } else {
                    registrantQualificationService.save(getModelObject());
                }

                Registration newRegistration = new Registration();
                newRegistration.setRegistrant(getModelObject().getRegistrant());
                newRegistration.setRegister(generalParametersService.get().getProvisionalRegister());
                newRegistration.setCourse(getModelObject().getCourse());
                //create additional or registration certificate of course if any
                productIssuer.issueRegistrationOrAdditionalQualficationCertificate(newRegistration, null);

                setResponsePage(new RegistrantQualificationEmploymentPage(getModelObject().getRegistrant().getId()));
            }
        };

        form.add(new Label("courseLabel", "Qualification Course") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.TRUE);
            }
        });
        form.add(new DropDownChoice("course", new CourseListModel(courseService)) {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.TRUE);
            }
        });
        form.add(new DropDownChoice("qualification", new QualificationListModel(qualificationService)));
        form.add(new DropDownChoice("awardingInstitution", new AcademicInstitutionstModel(institutionService)));
        form.add(new DropDownChoice("institution", new TrainingInstitutionstModel(institutionService)));
        form.add(
                new CustomDateTextField("dateAwarded").setRequired(true).add(new ErrorBehavior()).add(
                        DatePickerUtil.getDatePicker()));
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantId));
            }
        });
        form.add(new CustomDateTextField("startDate").add(DatePickerUtil.getDatePicker()));
        form.add(new CustomDateTextField("endDate").add(DatePickerUtil.getDatePicker()));


        form.add(new DropDownChoice("qualificationType", Arrays.asList(QualificationType.values())));
        add(form);
        add(new Label("registrant.fullname"));
        add(new FeedbackPanel("feedback"));
        add(new Link<Void>("addInstitutionPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionEditPage(null, null, null));
            }

            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
    }

    public RegistrantQualificationCoursePage(final Registration registration) {

        CompoundPropertyModel<RegistrantQualification> model = new CompoundPropertyModel<RegistrantQualification>(
                new RegistrantQualificationCoursePage.LoadableDetachableRegistrantQualificationModel(
                        registration.getRegistrant()));

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);

        final Long registrantId = model.getObject().getRegistrant().getId();

        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantId);
        setDefaultModel(model);

        Form<RegistrantQualification> form = new Form<RegistrantQualification>("form",
                (IModel<RegistrantQualification>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                DebtComponent debtComponent = null;
                Registration newRegistration = registration;
                newRegistration.setCourse(getModelObject().getCourse());

                RegistrantQualification registrantQualification = registrantQualificationService.get(registration.getRegistrant(), getModelObject().getQualification());
                if (registrantQualification != null) {
                    registrantQualification.setInstitution(getModelObject().getInstitution());
                    registrantQualification.setAwardingInstitution(getModelObject().getAwardingInstitution());
                    registrantQualification.setDateAwarded(getModelObject().getDateAwarded());
                    registrantQualification.setStartDate(getModelObject().getStartDate());
                    registrantQualification.setEndDate(getModelObject().getEndDate());
                    if (registrantQualification.getQualification().getCourse() != null) {
                        registrantQualification.setCourse(registrantQualification.getCourse());
                    }
                    registrantQualificationService.save(registrantQualification);
                } else {
                    registrantQualificationService.save(getModelObject());
                }
                debtComponent = billingProcess.billRegistrationOfNewQualificationProduct(registration, getModelObject().getCourse());
                //create additional or registration certificate of course if any
                productIssuer.issueRegistrationOrAdditionalQualficationCertificate(newRegistration, null);
                Registration currentRegistration = registrationProcess.activeRegisterNotStudentRegister(registration.getRegistrant());
                Duration duration = durationService.getCurrentCourseDuration(currentRegistration.getCourse());
                if (duration != null) {
                    if (renewalProcess.hasCurrentRenewalActivity(currentRegistration.getRegistrant(), duration)) ;
                    {
                        productIssuer.issueProductPracticeCertificate(currentRegistration, debtComponent);
                    }
                }

                setResponsePage(new DebtComponentsPage(registrantId));
            }
        };

        form.add(new DropDownChoice("qualification", new QualificationListModel(qualificationService)));
        form.add(new DropDownChoice("awardingInstitution", new AcademicInstitutionstModel(institutionService)));
        form.add(new DropDownChoice("institution", new TrainingInstitutionstModel(institutionService)));
        form.add(
                new CustomDateTextField("dateAwarded").setRequired(true).add(new ErrorBehavior()).add(
                        DatePickerUtil.getDatePicker()));
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantId));
            }
        });
        form.add(new CustomDateTextField("startDate").add(DatePickerUtil.getDatePicker()));
        form.add(new CustomDateTextField("endDate").add(DatePickerUtil.getDatePicker()));
        form.add(new Label("courseLabel", "Course"));
        form.add(new DropDownChoice("course", new CourseListModel(courseService)));
        form.add(new DropDownChoice("qualificationType", Arrays.asList(QualificationType.values())));
        add(form);
        add(new Label("registrant.fullname"));
        add(new FeedbackPanel("feedback"));
        add(new Link<Void>("addInstitutionPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionEditPage(null, null, null));
            }

            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });

    }

    private class QualifcationModel extends LoadableDetachableModel<List<? extends Qualification>> {

        protected List<? extends Qualification> load() {
            if (course != null) {
                return new ArrayList<Qualification>(course.getQualifications());
            } else {
                return new ArrayList<Qualification>();
            }
        }
    }

    private final class CourseModel extends LoadableDetachableModel<List<? extends Course>> {

        protected List<? extends Course> load() {
            return courseService.findAll();
        }
    }

    private final class LoadableDetachableRegistrantQualificationModel
            extends LoadableDetachableModel<RegistrantQualification> {

        private Long id;
        private Registrant registrant;

        public LoadableDetachableRegistrantQualificationModel(Long id) {
            this.id = id;
        }

        public LoadableDetachableRegistrantQualificationModel(Registrant registrant) {
            this.registrant = registrant;
        }

        @Override
        protected RegistrantQualification load() {
            RegistrantQualification registrantQualification = null;

            if (id == null) {
                registrantQualification = new RegistrantQualification();
                registrantQualification.setRegistrant(registrant);
            } else {
                registrantQualification = registrantQualificationService.get(id);
            }

            return registrantQualification;
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
