package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.PageReference;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.IdentificationValidationRule;
import zw.co.hitrac.council.business.service.IdentificationValidationRuleService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

import java.util.List;

/**
 * Created by clive on 6/16/15.
 */
public class IdentificationValidationRuleListPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private IdentificationValidationRuleService identificationValidationRuleService;

    public IdentificationValidationRuleListPage(final PageReference pageReference) {

        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));

        add(new Link("returnLink") {

            public void onClick() {

                setResponsePage(pageReference.getPage());
            }
        });

        add(new Link<Void>("addNewLink") {

            @Override
            public void onClick() {

                setResponsePage(new IdentificationValidationRuleEditPage(null, getPageReference()));
            }
        });

        IModel<List<IdentificationValidationRule>> model = new LoadableDetachableModel<List<IdentificationValidationRule>>() {

            @Override
            protected List<IdentificationValidationRule> load() {

                return identificationValidationRuleService.findAll();
            }
        };

        PropertyListView<IdentificationValidationRule> eachItem = new PropertyListView<IdentificationValidationRule>("eachItem", model) {

            @Override
            protected void populateItem(ListItem<IdentificationValidationRule> item) {

                Link<IdentificationValidationRule> viewLink = new Link<IdentificationValidationRule>("viewLink", item.getModel()) {

                    @Override
                    public void onClick() {

                        setResponsePage(new IdentificationValidationRuleEditPage(getModelObject().getId(), getPageReference()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(new Label("citizenship.name"));
                item.add(new Label("identificationType.name"));
                item.add(new Label("example"));
                item.add(new Label("active"));

                item.add(viewLink);
            }
        };

        add(eachItem);

    }

}
