package zw.co.hitrac.council.business.utils;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.beans.support.PropertyComparator;

/**
 * @author charlesc
 */
public class HrisComparator implements Serializable {

    /**
     * Method to sort any list with property name
     *
     * @param list
     * @return
     */
    public List<?> sort(List<?> list) {
        if (list != null && !list.isEmpty()) {
            PropertyComparator.sort(list, new MutableSortDefinition("name", true, true));
        }
        return list;
    }

    /**
     * Method to sort Examination Pass Rate
     *
     * @param list
     * @return
     */
    public List<?> sortExamPassRates(List<?> list) {
        if (list != null && !list.isEmpty()) {
            PropertyComparator.sort(list, new MutableSortDefinition("percentagePassed", true, false));
        }
        return list;
    }

    /**
     * Method to sort Candidate Marks in ExamRegistration
     *
     * @param list
     * @return
     */
    public List<?> sortExamCandidateMarks(List<?> list) {
        if (list != null && !list.isEmpty()) {
            PropertyComparator.sort(list, new MutableSortDefinition("finalMark", true, false));
        }
        return list;
    }

    /**
     * Method to sort Candidates by Position
     *
     * @param list
     * @return
     */
    public List<?> sortExamPositionMarks(List<?> list) {
        if (list != null && !list.isEmpty()) {
            PropertyComparator.sort(list, new MutableSortDefinition("position", true, false));
        }
        return list;
    }

    /**
     * Method to sort Examination Marks
     *
     * @param list
     * @return
     */
    public List<?> sortExamMarks(List<?> list) {
        if (list != null && !list.isEmpty()) {
            PropertyComparator.sort(list, new MutableSortDefinition("mark", true, false));
        }
        return list;
    }

    /**
     * Method to sort Examination Cards by exam paper name Institution
     *
     * @param list
     * @return
     */
    public List<?> sortExaminationCards(List<?> list) {
        if (list != null && !list.isEmpty()) {
            PropertyComparator.sort(list, new MutableSortDefinition("examPaper", true, true));
        }
        return list;
    }

    /**
     * Method to sort Receipt Items by code value
     *
     * @param list
     * @return
     */
    public List<?> receiptItems(List<?> list) {
        if (list != null && !list.isEmpty()) {
            PropertyComparator.sort(list, new MutableSortDefinition("codeValue", true, true));
        }
        return list;
    }

    public List<?> sortRegistrants(List<?> list) {
        if (list != null && !list.isEmpty()) {
            PropertyComparator.sort(list, new MutableSortDefinition("name", true, true));
        }
        return list;
    }

    /**
     * Method to sort Candidates by Surname in ExamRegistration
     *
     * @param list
     * @return
     */
    public List<?> sortExamRegistrationBySurname(List<?> list) {
        if (list != null && !list.isEmpty()) {
            PropertyComparator.sort(list, new MutableSortDefinition("registration.registrant.name", true, true));
        }
        return list;
    }

    /**
     * Method to sort Qualifications by name
     *
     * @param list
     * @return
     */
    public List<?> sortRegisterStaticQualification(List<?> list) {
        if (list != null && !list.isEmpty()) {
            PropertyComparator.sort(list, new MutableSortDefinition("qualification.name", true, true));
        }
        return list;
    }

    /**
     * Method to sort Course by name
     *
     * @param list
     * @return
     */
    public List<?> sortRegisterStaticCourse(List<?> list) {
        if (list != null && !list.isEmpty()) {
            PropertyComparator.sort(list, new MutableSortDefinition("course.name", true, true));
        }
        return list;
    }

    /**
     * Method to sort Institutions by name in Registration
     *
     * @param list
     * @return
     */
    public List<?> sortExamRegistrationBySchoolName(List<?> list) {
        if (list != null && !list.isEmpty()) {
            PropertyComparator.sort(list, new MutableSortDefinition("registration.institution.name", true, true));
        }
        return list;
    }

    /**
     * Method to sort Registrant Data by full name
     *
     * @param list
     * @return
     */
    public static List<?> sortRegistrantData(List<?> list) {
        if (list != null && !list.isEmpty()) {
            PropertyComparator.sort(list, new MutableSortDefinition("fullname", true, true));
        }
        return list;
    }

    /**
     * Method to sort Registrant Balance by full name
     *
     * @param list
     * @return
     */
    public static List<?> sortRegistrantBalance(List<?> list) {
        if (list != null && !list.isEmpty()) {
            PropertyComparator.sort(list, new MutableSortDefinition("registrant.fullname", true, true));
        }
        return list;
    }

    /**
     * Method to sort Institution Balance by full name
     *
     * @param list
     * @return
     */
    public static List<?> sortInstitutionBalance(List<?> list) {
        if (list != null && !list.isEmpty()) {
            PropertyComparator.sort(list, new MutableSortDefinition("institution.name", true, true));
        }
        return list;
    }

    /**
     * Method to sort Products by name in product sale
     *
     * @param list
     * @return
     */
    public List<?> sortProductSale(List<?> list) {
        if (list != null && !list.isEmpty()) {
            PropertyComparator.sort(list, new MutableSortDefinition("product.name", true, true));
        }
        return list;
    }

    /**
     * Method to sort Product Sales by name registrant full name
     *
     * @param list
     * @return
     */
    public List<?> sortProductSaleByRegistrantName(List<?> list) {
        if (list != null && !list.isEmpty()) {
            PropertyComparator.sort(list, new MutableSortDefinition("registrant.name", true, true));
        }
        return list;
    }

    public static List<?> sortProductSaleByRegistrantFullName(List<?> list) {
        if (list != null && !list.isEmpty()) {
            PropertyComparator.sort(list, new MutableSortDefinition("registrant.fullname", true, true));
        }
        return list;
    }

    public static List<?> sortProductSaleByCustomerName(List<?> list) {
        if (list != null && !list.isEmpty()) {
            PropertyComparator.sort(list, new MutableSortDefinition("customer", true, true));
        }
        return list;
    }

    public static List<?> sortProductSaleByReceiptNumber(List<?> list) {
        if (list != null && !list.isEmpty()) {
            PropertyComparator.sort(list, new MutableSortDefinition("paymentDetail.receiptHeader.id", true, true));
        }
        return list;
    }

    /**
     * Method to sort Examination Paper by name
     *
     * @param list
     * @return
     */
    public List<?> sortExamniationPapers(List<?> list) {
        if (list != null && !list.isEmpty()) {
            PropertyComparator.sort(list, new MutableSortDefinition("name", true, true));
        }
        return list;
    }

    /**
     * Method to sort Exams by name in Exams
     *
     * @param list
     * @return
     */
    public List<?> sortExams(List<?> list) {
        if (list != null && !list.isEmpty()) {
            PropertyComparator.sort(list, new MutableSortDefinition("modulePaper.name", true, true));
        }
        return list;
    }

    /**
     * Method to sort Registrant Qualification by DateAwarded
     *
     * @param list
     * @return
     */
    public static List<?> sortRegistrantQualifications(List<?> list) {
        if (list != null && !list.isEmpty()) {
            PropertyComparator.sort(list, new MutableSortDefinition("dateAwarded", true, true));
        }
        return list;
    }

    /**
     * Method to sort ItemCase by DateAwarded
     *
     * @param list
     * @return
     */
    public static List<?> sortItemCasesByDate(List<?> list) {
        if (list != null && !list.isEmpty()) {
            PropertyComparator.sort(list, new MutableSortDefinition("dateAwarded", true, true));
        }
        return list;
    }

    /**
     * Method to sort Candidates by Surname in ExamRegistration
     *
     * @param list
     * @return
     */
    public static List<?> sortPaymentsDueByCourseAndNameSurname(List<?> list) {
        if (list != null && !list.isEmpty()) {
            PropertyComparator.sort(list, new MutableSortDefinition("registration.registrant.name", true, true));
            PropertyComparator.sort(list, new MutableSortDefinition("registration.course.name", true, true));
        }
        return list;
    }

    public static List<?> sortCourseAndNameSurname(List<?> list) {
        if (list != null && !list.isEmpty()) {
            PropertyComparator.sort(list, new MutableSortDefinition("registrant.name", true, true));
            PropertyComparator.sort(list, new MutableSortDefinition("course.name", true, true));
        }
        return list;
    }
}
