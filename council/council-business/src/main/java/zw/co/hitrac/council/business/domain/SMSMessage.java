package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 *
 * @author Michael Matiashe
 */
@Entity
@Table(name="smsmessage")
@Audited
@XmlRootElement
public class SMSMessage extends  BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    private String detail="";
    private Integer delivered;
    private Integer failed;
    private Boolean send= Boolean.FALSE;

    public Integer getDelivered() {
        return delivered;
    }

    public void setDelivered(Integer delivered) {
        this.delivered = delivered;
    }

    public Integer getFailed() {
        return failed;
    }

    public void setFailed(Integer failed) {
        this.failed = failed;
    }

    
    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public Boolean isSend() {
        return send;
    }

    public void setSend(Boolean send) {
        this.send = send;
    }
 
     
}
