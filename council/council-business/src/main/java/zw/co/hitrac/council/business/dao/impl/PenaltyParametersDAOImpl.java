package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.PenaltyParametersDAO;
import zw.co.hitrac.council.business.dao.repo.PenaltyParametersRepository;
import zw.co.hitrac.council.business.domain.PenaltyParameters;

/**
 *
 * @author Michael Matiashe
 */
@Repository
public class PenaltyParametersDAOImpl implements PenaltyParametersDAO {

    @Autowired
    private PenaltyParametersRepository penaltyParametersRepository;

    public PenaltyParameters save(PenaltyParameters penaltyParameters) {
        return penaltyParametersRepository.save(penaltyParameters);
    }

    public List<PenaltyParameters> findAll() {
        return penaltyParametersRepository.findAll();
    }

    public PenaltyParameters get(Long id) {
        return penaltyParametersRepository.findOne(id);
    }
    /*
     * A setter method that will make mocking repo object easier
     * @param  TransactionType
     */

    public void setPenaltyParametersRepository(PenaltyParametersRepository penaltyParametersRepository) {
        this.penaltyParametersRepository = penaltyParametersRepository;
    }

    public List<PenaltyParameters> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
