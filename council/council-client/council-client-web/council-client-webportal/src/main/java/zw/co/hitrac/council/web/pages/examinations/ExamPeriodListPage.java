package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.examinations.ExamPeriod;
import zw.co.hitrac.council.business.service.examinations.ExamPeriodService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.configure.AdministerDatabasePage;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;

/**
 *
 * @author Constance Mabaso
 */
public class ExamPeriodListPage extends IExaminationsPage {
    @SpringBean
    private ExamPeriodService examPeriodService;

    public ExamPeriodListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new ExamPeriodEditPage(null));
            }
        });

        IModel<List<ExamPeriod>> model = new LoadableDetachableModel<List<ExamPeriod>>() {
            @Override
            protected List<ExamPeriod> load() {
                return examPeriodService.findAll();
            }
        };
        PropertyListView<ExamPeriod> eachItem = new PropertyListView<ExamPeriod>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<ExamPeriod> item) {
                Link<ExamPeriod> viewLink = new Link<ExamPeriod>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new ExamPeriodViewPage(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(viewLink);
                item.add(new Label("name"));
            }
        };

        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
