/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.accounts.payments;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Book;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.User;
import zw.co.hitrac.council.business.domain.accounts.PaymentMethod;
import zw.co.hitrac.council.business.domain.accounts.ReceiptHeader;
import zw.co.hitrac.council.business.domain.accounts.TransactionType;
import zw.co.hitrac.council.business.domain.reports.CustomerProductSale;
import zw.co.hitrac.council.business.service.UserService;
import zw.co.hitrac.council.business.service.accounts.CustomerService;
import zw.co.hitrac.council.business.service.accounts.PaymentMethodService;
import zw.co.hitrac.council.business.service.accounts.ReceiptHeaderService;
import zw.co.hitrac.council.business.service.accounts.TransactionTypeService;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.reports.CanceledReceipts;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.models.BankAccountsModel;
import zw.co.hitrac.council.web.models.TransactionTypeListModel;
import zw.co.hitrac.council.web.pages.accounts.IAccountingPage;
import zw.co.hitrac.council.web.utility.DatePickerUtil;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 *
 * @author tdhlakama
 */
public class SearchCanceledPaymentPage extends IAccountingPage {

    private Long generatedReceiptNumber;
    private Date startDate, endDate;
    private User user;
    private TransactionType transactionType;
    private String searchText;
    private PaymentMethod paymentMethod;
    @SpringBean
    UserService userService;
    @SpringBean
    ReceiptHeaderService receiptHeaderService;
    @SpringBean
    private TransactionTypeService transactionTypeService;
    @SpringBean
    private PaymentMethodService paymentMethodService;
    @SpringBean
    private CustomerService customerService;

    public SearchCanceledPaymentPage() {
        PropertyModel<Date> startDateModel = new PropertyModel<Date>(this, "startDate");
        PropertyModel<Date> endDateModel = new PropertyModel<Date>(this, "endDate");
        PropertyModel<Long> generatedReceiptNumberModel = new PropertyModel<Long>(this, "generatedReceiptNumber");
        PropertyModel<String> userModel = new PropertyModel<String>(this, "user");
        PropertyModel<TransactionType> transactionTypeModel = new PropertyModel<TransactionType>(this, "transactionType");
        PropertyModel<String> paymentMethodModel = new PropertyModel<String>(this, "paymentMethod");
      
        Form<?> form = new Form("form");
        form.add(new TextField<Long>("generatedReceiptNumber", generatedReceiptNumberModel));
        form.add(new TextField<Date>("startDate", startDateModel).add(DatePickerUtil.getDatePicker()));
        form.add(new TextField<Date>("endDate", endDateModel).add(DatePickerUtil.getDatePicker()));
        form.add(new DropDownChoice("user", userModel, userService.findAll()));
        form.add(new DropDownChoice("paymentMethod", paymentMethodModel, paymentMethodService.findAll()));
        form.add(new DropDownChoice("transactionType", transactionTypeModel, new BankAccountsModel(transactionTypeService)));
        add(form);
        final IModel<List<ReceiptHeader>> model = new LoadableDetachableModel<List<ReceiptHeader>>() {
            @Override
            protected List<ReceiptHeader> load() {
                if (generatedReceiptNumber == null && startDate == null && endDate == null && user == null && transactionType == null && paymentMethod == null) {
                    return new ArrayList<ReceiptHeader>();
                } else {
                    Boolean canceled = Boolean.TRUE;
                    return receiptHeaderService.getReceiptHeaders(generatedReceiptNumber, paymentMethod, null, startDate, endDate, user, transactionType, canceled);
                }
            }
        };
        //Registrant List Data View Panel
        add(new PaymentDataListPanel("paymentDataListPanel", model));
        add(new FeedbackPanel("feedback"));
        form.add(new Button("receiptHeaderReport") {
            @Override
            public void onSubmit() {
                try {

                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    String query = "";
                    String dates = "";
                    String doneBy = "";
                    String method = "";
                    String type = "";
                    String bank = "";
                    String canceled = "";
                    if (generatedReceiptNumber != null) {
                        query = ("Receipt Number - " + generatedReceiptNumber + ". ");
                    }
                    if (startDate != null && endDate != null) {
                        dates = "From - " + DateUtil.getDate(startDate) + " to " + DateUtil.getDate(endDate) + ". ";
                    }
                    if (user != null) {
                        doneBy = "Receipted By - " + user.getSurname() + " " + user.getFirstName() + ". ";
                    }
                    if (paymentMethod != null) {
                        method = "Payment Method - " + paymentMethod.getName() + ". ";
                    }
                    if (transactionType != null) {
                        bank = "Bank Account - " + transactionType.getName() + ". ";
                    }
                    canceled = " Report Includes - CANCELED Receipts ONLY  ";
               
                    List<CustomerProductSale> productSales = new ArrayList<CustomerProductSale>();
                    if (startDate != null && endDate == null) {
                        endDate = new Date();
                    }

                    for (ReceiptHeader r : model.getObject()) {
                        CustomerProductSale productSale = new CustomerProductSale();
                        productSale.setReceiptHeader(r);
                        productSale.setAmountPaid(r.getTotalAmountPaid());
                        productSale.setCarryForward(r.getCarryForward());

                        final Registrant registrant = customerService.getRegistrant(r.getPaymentDetails().getCustomer().getAccount());
                        if (registrant != null) {
                            productSale.setRegistrant(registrant);
                        }
                        if (registrant == null) {
                            Institution institution = customerService.getInstitution(r.getPaymentDetails().getCustomer().getAccount());
                            productSale.setInstitution(institution);
                        }
                        productSales.add(productSale);
                    }

                    CanceledReceipts cashReceiptHeader = new CanceledReceipts();
                    parameters.put("comment", "Paymemt Details * " + query.concat(method).concat(type).concat(doneBy).concat(bank).concat(dates).concat(canceled));
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(cashReceiptHeader,
                            contentType, productSales, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);
                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    public Long getGeneratedReceiptNumber() {
        return generatedReceiptNumber;
    }

    public void setGeneratedReceiptNumber(Long generatedReceiptNumber) {
        this.generatedReceiptNumber = generatedReceiptNumber;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    private class TransactionTypeModel extends LoadableDetachableModel<List<? extends TransactionType>> {

        protected List<? extends TransactionType> load() {
            if (paymentMethod != null) {
                return new TransactionTypeListModel(transactionTypeService, paymentMethod, Book.CB).getObject();
            } else {
                return new ArrayList<TransactionType>();
            }
        }
    }
}
