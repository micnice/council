package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 *
 * @author Michael Matiashe
 */
@Entity
@Table(name = "requirement")
@Audited
public class Requirement extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    private Boolean generalRequirement = Boolean.FALSE;
    private Boolean examRequirement = Boolean.FALSE; // if true exam requirement
    private Boolean upload = Boolean.FALSE;
    private QualificationType qualificationType;

    public QualificationType getQualificationType() {
        return qualificationType;
    }

    public void setQualificationType(QualificationType qualificationType) {
        this.qualificationType = qualificationType;
    }

    public Boolean getGeneralRequirement() {
        return generalRequirement;
    }

    public void setGeneralRequirement(Boolean generalRequirement) {
        this.generalRequirement = generalRequirement;
    }

    public Boolean getExamRequirement() {
        if (examRequirement == null) {
            return Boolean.FALSE;
        }
        return examRequirement;
    }

    public void setExamRequirement(Boolean examRequirement) {
        this.examRequirement = examRequirement;
    }

    public Boolean getUpload() {
        if (upload == null) {
            return Boolean.FALSE;
        }
        return upload;
    }

    public void setUpload(Boolean upload) {
        this.upload = upload;
    }
}
