package zw.co.hitrac.council.web.pages.registrar;

import zw.co.hitrac.council.web.pages.configure.*;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.SMSMessage;
import zw.co.hitrac.council.business.service.SMSMessageService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author Michael Matiashe
 */
public class SMSMessageEditPage extends TemplatePage {

    @SpringBean
    private SMSMessageService sMSMessageService;

    public SMSMessageEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<SMSMessage>(new LoadableDetachableSMSMessageModel(id)));
        Form<SMSMessage> form = new Form<SMSMessage>("form", (IModel<SMSMessage>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                sMSMessageService.save(getModelObject());
                setResponsePage(SMSMessageListPage.class);
            }
        };

        form.add(new TextField<String>("detail").setRequired(true).add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink", SMSMessageListPage.class));
        add(form);
    }

    private final class LoadableDetachableSMSMessageModel extends LoadableDetachableModel<SMSMessage> {

        private Long id;

        public LoadableDetachableSMSMessageModel(Long id) {
            this.id = id;
        }

        @Override
        protected SMSMessage load() {
            if (id == null) {
                return new SMSMessage();
            }
            return sMSMessageService.get(id);
        }
    }

}
