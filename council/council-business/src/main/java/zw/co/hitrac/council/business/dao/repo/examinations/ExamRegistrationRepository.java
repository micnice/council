/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.repo.examinations;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;

/**
 *
 * @author tidza
 */
public interface ExamRegistrationRepository extends CrudRepository<ExamRegistration, Long> {
public List<ExamRegistration> findAll();
}
