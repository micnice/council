package zw.co.hitrac.council.web.pages.registration;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.RegistrantTransfer;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author Takunda Dhlakama
 */
public class RegistrationViewPage extends TemplatePage {

    @SpringBean
    private GeneralParametersService generalParametersService;

    public RegistrationViewPage() {
        menuPanelManager.getRegistrantionPanel().setTopMenuCurrent(true);
        add(new Link<Void>("registrantEditPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantEditPage(null, Boolean.FALSE));
            }
        });
        add(new Link<Void>("existingRegistrantEditPage") {
            @Override
            public void onClick() {
                  setResponsePage(new RegistrantEditPage(null, Boolean.TRUE));
            }
        });
        add(new Label("label1", "Add an existing Registrant to system"));

        add(new BookmarkablePageLink<Void>("registrantListPage", RegistrantListPage.class));

        add(new BookmarkablePageLink<Void>("registrantDueToTransferListPage", RegistrantTransferListPage.class));

        add(new Link<Void>("registrantPreRegEditPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantPreRegEditPage(null));
            }

            @Override
            protected void onConfigure() {
                setVisible(generalParametersService.get().getAllowPreRegistrantion());
            }
        });


    }
}


//~ Formatted by Jindent --- http://www.jindent.com
