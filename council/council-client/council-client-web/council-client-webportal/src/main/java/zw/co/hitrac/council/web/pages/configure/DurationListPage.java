package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.service.DurationService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

import java.util.List;

/**
 *
 * @author Charles Chigoriwa
 */
public class DurationListPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private DurationService durationService;

    public DurationListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new DurationEditPage(null));
            }
        });

        IModel<List<Duration>> model = new LoadableDetachableModel<List<Duration>>() {
            @Override
            protected List<Duration> load() {
                return durationService.findAll();
            }
        };

        PropertyListView<Duration> eachItem = new PropertyListView<Duration>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<Duration> item) {
                Link<Duration> viewLink = new Link<Duration>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new DurationViewPage(getModelObject().getId()));
                    }
                };
                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                viewLink.add(new Label("name"));
                 item.add(new Label("councilDuration"));
                 item.add(new Label("startDate"));
                 item.add(new Label("endDate"));
                 item.add(new Label("textStatus"));
            }
        };

        add(eachItem);
    }
}
