package zw.co.hitrac.council.web.pages.reports;

import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.PageReference;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Citizenship;
import zw.co.hitrac.council.business.domain.IdentificationType;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.service.CitizenshipService;
import zw.co.hitrac.council.business.service.IdentificationTypeService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.reports.RegistrantDataDBReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author clive
 */
public class RegistrantsWithInvalidIdentificationNumbersPage extends TemplatePage {

    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private CitizenshipService citizenshipService;
    @SpringBean
    private IdentificationTypeService identificationTypeService;
    @SpringBean
    private DataSource dataSource;

    private Citizenship citizenship;
    private IdentificationType identificationType;
    private ContentType contentType;

    private static final String SEPARATOR = ",";


    public RegistrantsWithInvalidIdentificationNumbersPage(final PageReference pageReference) {

        menuPanelManager.getSearchPanel().setTopMenuCurrent(true);
        menuPanelManager.getSearchPanel().setSearchAdvanced(true);

        final PropertyModel<Citizenship> citizenshipPropertyModel
                = new PropertyModel<Citizenship>(this, "citizenship");
        final PropertyModel<IdentificationType> identificationTypePropertyModel
                = new PropertyModel<IdentificationType>(this, "identificationType");
        PropertyModel<ContentType> contentTypeModel = new PropertyModel<ContentType>(this, "contentType");
        Form<?> form = new Form("form");
        form.add(new DropDownChoice("citizenship", citizenshipPropertyModel, citizenshipService.findAll()));
        form.add(new DropDownChoice("identificationType", identificationTypePropertyModel, identificationTypeService.findAll()));
        form.add(new DropDownChoice("contentType", contentTypeModel, Arrays.asList(ContentType.values())));form.add(new Link("returnLink") {

            public void onClick() {

                setResponsePage(pageReference.getPage());
            }
        });
        add(form);

        form.add(new Button("process") {

            @Override
            public void onSubmit() {

                Map parameters = new HashMap();
                RegistrantDataDBReport registrantDataDBReport = new RegistrantDataDBReport();
                try {
                    parameters.put("comment", "Those without ID Numbers or Incorrect ID Numbers");

                    StringBuilder registrantIdList = new StringBuilder();

                    for (Registrant registrant
                            : registrantService.getRegistrantsWithNoOrInvalidNationalIDs(citizenship, identificationType)) {

                        registrantIdList.append(registrant.getId()).append(SEPARATOR);

                    }

                    parameters.put("list", registrantIdList.substring(0, registrantIdList.lastIndexOf(SEPARATOR)));

                    
                    final Connection connection = dataSource.getConnection(); //DBConnect.getConnection();

                    ByteArrayResource resource = null;
                    try {
                        resource = ReportResourceUtils.getReportResource(registrantDataDBReport, contentType, connection, parameters);
                    } finally {
                        connection.close();
                    }
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                } catch (SQLException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });
        add(new FeedbackPanel("feedback"));
    }

}
