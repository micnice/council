package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.business.service.RegisterService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;

/**
 *
 * @author Kelvin Goredema
 */
public class RegisterListPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private RegisterService registerService;

    public RegisterListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new RegisterEditPage(null));
            }
        });

        IModel<List<Register>> model = new LoadableDetachableModel<List<Register>>() {
            @Override
            protected List<Register> load() {
                return registerService.findAll();
            }
        };
        PropertyListView<Register> eachItem = new PropertyListView<Register>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<Register> item) {
                Link<Register> viewLink = new Link<Register>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new RegisterViewPage(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };

        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
