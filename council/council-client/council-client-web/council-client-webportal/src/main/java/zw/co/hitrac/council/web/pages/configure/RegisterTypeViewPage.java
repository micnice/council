package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.RegisterType;
import zw.co.hitrac.council.business.service.RegisterTypeService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Kelvin Goredema
 */
public class RegisterTypeViewPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private RegisterTypeService registerService;

    public RegisterTypeViewPage(Long id) {
        CompoundPropertyModel<RegisterType> model =
                new CompoundPropertyModel<RegisterType>(new LoadableDetachableRegisterTypeModel(id));

        setDefaultModel(model);
        add(new Link<RegisterType>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new RegisterTypeEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", RegisterTypeListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("individualName"));
        add(new Label("register", model.bind("name")));
    }

    private final class LoadableDetachableRegisterTypeModel extends LoadableDetachableModel<RegisterType> {

        private Long id;

        public LoadableDetachableRegisterTypeModel(Long id) {
            this.id = id;
        }

        @Override
        protected RegisterType load() {
            return registerService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
