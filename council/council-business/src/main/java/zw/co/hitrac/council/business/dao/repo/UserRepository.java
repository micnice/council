/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package zw.co.hitrac.council.business.dao.repo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.User;

import java.util.List;


/**
 *
 * @author Clive Gurure
 */

public interface UserRepository extends CrudRepository<User, Long>{
    public List<User> findAll();

    User findByUsernameIgnoreCase(String userName);

    @Query("select u from User u where u.isEncrypted = false or u.isEncrypted is null")
    List<User> findUsersWithPlainTextPasswords();
}
