package zw.co.hitrac.council.web.models;

import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.RegistrationService;

import java.util.List;

public class RegistrantTransferDataListModel extends LoadableDetachableModel<List<RegistrantData>> {

    private final RegistrationService registrationService;

    public RegistrantTransferDataListModel(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @Override
    protected List<RegistrantData> load() {
        return this.registrationService.getRegistrantsDueForTransfer();
    }
}
