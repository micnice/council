package zw.co.hitrac.council.business.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author tdhlakama
 * @author Michael Matashe
 */
public enum ApplicationPurpose {

    PROVISIONAL_REGISTRATION("Provisional Registration"),
    MAIN_REGISTRATION("Main Registration"),
    SPECIALIST_REGISTRATION("Specialist Registration"),
    INTERN_REGISTRATION("Intern Registration"),
    STUDENT_REGISTRATION("Student Registration"),
    INSTITUTION_REGISTRATION("Institution Registration"),
    CERTIFICATE_OF_GOOD_STANDING("Certificate Of Good Standing"),
    CHANGE_OF_NAME("Change of Name"),
    UNRESTRICTED_PRACTICING_CERTIFICATE("unrestricted practicing certificate"),
    PROVISIONAL_QUALIIFICATION("Provisional Qualification"),
    ADDITIONAL_QUALIIFICATION("Additional Qualification"),
    TRANSFER("Transfer"),
    TRANSCRIPT_OF_TRAINING("Transcript Of Training"),
    SUPERVISION("Supervision");
    private final String name;

    private ApplicationPurpose(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static List<ApplicationPurpose> getQualificationApplications() {
        List<ApplicationPurpose> applicationPurposes = new ArrayList<ApplicationPurpose>();
        applicationPurposes.add(ADDITIONAL_QUALIIFICATION);
        applicationPurposes.add(PROVISIONAL_QUALIIFICATION);
        return applicationPurposes;
    }

}
