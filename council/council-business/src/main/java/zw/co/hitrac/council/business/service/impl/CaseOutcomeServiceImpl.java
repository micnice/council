/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.CaseOutcomeDAO;
import zw.co.hitrac.council.business.domain.CaseOutcome;
import zw.co.hitrac.council.business.service.CaseOutcomeService;

import java.util.List;

/**
 * @author kelvin
 */

@Service
@Transactional
public class CaseOutcomeServiceImpl implements CaseOutcomeService {

    @Autowired
    private CaseOutcomeDAO caseOutcomeDAO;

    @Transactional
    public CaseOutcome save(CaseOutcome caseOutcome) {

        return caseOutcomeDAO.save(caseOutcome);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<CaseOutcome> findAll() {

        return caseOutcomeDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public CaseOutcome get(Long id) {

        return caseOutcomeDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<CaseOutcome> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setCaseOutcomeDAO(CaseOutcomeDAO caseOutcomeDAO) {

        this.caseOutcomeDAO = caseOutcomeDAO;
    }

}
