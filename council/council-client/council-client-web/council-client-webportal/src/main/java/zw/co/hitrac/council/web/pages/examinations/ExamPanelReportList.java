
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------
import com.googlecode.wicket.jquery.ui.panel.JQueryFeedbackPanel;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

import zw.co.hitrac.council.business.domain.examinations.Exam;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.examinations.ExamResult;
import zw.co.hitrac.council.business.service.examinations.ExamResultService;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.reports.ExaminationPaperReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 * @author tdhlakama
 */
public class ExamPanelReportList extends Panel {

    @SpringBean
    private ExamResultService examResultService;
    HrisComparator hrisComparator = new HrisComparator();

    /**
     * Constructor
     *
     * @param id
     * @param model (Exam list)
     */
    public ExamPanelReportList(String id, IModel<List<Exam>> model) {
        super(id);
        final FeedbackPanel feedback = new JQueryFeedbackPanel("feedback");
        add(feedback.setOutputMarkupId(true));
        PropertyListView<Exam> eachItem = new PropertyListView<Exam>("examList", model) {
            @Override
            protected void populateItem(ListItem<Exam> item) {

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                
                item.add(new Label("total", examResultService.getTotalCandidates(item.getModelObject(), null)));
              
                Link<Exam> passlist = new Link<Exam>("passlist", item.getModel()) {
                    @Override
                    public void onClick() {
                        try {
                            List<ExamResult> results = examResultService.examResults(getModelObject());
                            List<ExamResult> resultsSet = new ArrayList<ExamResult>();

                            for (ExamResult e : results) {
                                if (e.getExamStatus()) {
                                    resultsSet.add(e);
                                }
                            }
                            if (results.isEmpty()) {
                                this.info("No Data was found");
                            } else {
                                this.info("Data Found");

                                ExaminationPaperReport examinationPaperReport = new ExaminationPaperReport();
                                ContentType contentType = ContentType.PDF;
                                Map parameters = new HashMap();
                                
                               parameters.put("comment", "SUCCESSFUL candidates - " + getModelObject().getModulePaper().getName() + " - " + results.get(0).getExaminationPeriod() + " examination ");

                                ByteArrayResource resource =
                                        ReportResourceUtils.getReportResource(examinationPaperReport, contentType, resultsSet, parameters);
                                IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                        RequestCycle.get().getResponse(), null);

                                resource.respond(a);

                                // To make Wicket stop processing form after sending response
                                RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                            }
                        } catch (JRException ex) {
                            ex.printStackTrace(System.out);
                        }
                    }
                };
                item.add(passlist);
                Link<Exam> failedlist = new Link<Exam>("failed", item.getModel()) {
                    @Override
                    public void onClick() {
                        try {
                            List<ExamResult> results = examResultService.examResults(getModelObject());
                            List<ExamResult> resultsSet = new ArrayList<ExamResult>();

                            for (ExamResult e : results) {
                                if (!e.getExamStatus()) {
                                    resultsSet.add(e);
                                }
                            }

                            if (results.isEmpty()) {
                                this.info("No Data was found");
                            } else {
                                this.info("Data Found");

                                ExaminationPaperReport examinationPaperReport = new ExaminationPaperReport();
                                ContentType contentType = ContentType.PDF;
                                Map parameters = new HashMap();

                               parameters.put("comment", "UNSUCCESSFUL candidates - " + getModelObject().getModulePaper().getName() + " - " + results.get(0).getExaminationPeriod() + " examination ");

                                ByteArrayResource resource =
                                        ReportResourceUtils.getReportResource(examinationPaperReport, contentType, resultsSet, parameters);
                                IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                        RequestCycle.get().getResponse(), null);

                                resource.respond(a);

                                // To make Wicket stop processing form after sending response
                                RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                            }
                        } catch (JRException ex) {
                            ex.printStackTrace(System.out);
                        }
                    }
                };
                item.add(failedlist);
                Link<Exam> masterlist = new Link<Exam>("masterlist", item.getModel()) {
                    @Override
                    public void onClick() {
                        try {

                            List<ExamResult> results = examResultService.examResults(getModelObject());
                            if (results.isEmpty()) {
                                this.info("No Data was found");
                            } else {
                                this.info("Data Found");
                                ExaminationPaperReport examinationPaperReport = new ExaminationPaperReport();
                                ContentType contentType = ContentType.PDF;
                                Map parameters = new HashMap();
                                parameters.put("comment", "Candidates who registered for " + getModelObject().getModulePaper().getName() + " - " + results.get(0).getExaminationPeriod() + " examination ");

                                ByteArrayResource resource =
                                        ReportResourceUtils.getReportResource(examinationPaperReport, contentType, results, parameters);
                                IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                        RequestCycle.get().getResponse(), null);

                                resource.respond(a);

                                // To make Wicket stop processing form after sending response
                                RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                            }
                        } catch (JRException ex) {
                            ex.printStackTrace(System.out);
                        }
                    }
                };
                item.add(masterlist);
                item.add(new Label("modulePaper"));
            }
        };

        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
