package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.service.CouncilDurationService;

/**
 *
 * @author charlesc
 */
public class CouncilDurationViewPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private CouncilDurationService durationService;

    public CouncilDurationViewPage(Long id) {
        CompoundPropertyModel<CouncilDuration> model = new CompoundPropertyModel<CouncilDuration>(new LoadableDetachableDurationModel(id));
        setDefaultModel(model);
        add(new Link<CouncilDuration>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new CouncilDurationEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", CouncilDurationListPage.class));
        add(new Label("name"));
        add(new Label("textStatus"));
        add(new Label("durations"));
        add(new Label("councilDuration", model.bind("name")));
    }

    private final class LoadableDetachableDurationModel extends LoadableDetachableModel<CouncilDuration> {

        private Long id;

        public LoadableDetachableDurationModel(Long id) {
            this.id = id;
        }

        @Override
        protected CouncilDuration load() {

            return durationService.get(id);
        }
    }
}
