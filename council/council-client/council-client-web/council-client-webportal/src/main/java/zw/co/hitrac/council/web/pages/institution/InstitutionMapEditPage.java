package zw.co.hitrac.council.web.pages.institution;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxCheckBox;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBoxMultipleChoice;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.RegistrantQualificationService;
import zw.co.hitrac.council.web.pages.configure.AdministerDatabasePage;
import zw.co.hitrac.council.web.pages.configure.IAdministerDatabaseBasePage;

/**
 *
 * @author Charles Chigoriwa
 * @author Michael Matiashe
 */
public class InstitutionMapEditPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private RegistrantQualificationService registrantQualificationService;

    public InstitutionMapEditPage() {
        setDefaultModel(new CompoundPropertyModel<Institution>(new LoadableDetachableInstitutionModel()));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        Form<Institution> form = new Form<Institution>("form", (IModel<Institution>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                institutionService.removeDuplicates(getModelObject().getDuplicate(), getModelObject().getInstitutions());
                setResponsePage(InstitutionListPage.class);
            }
        };
        form.add(new DropDownChoice("duplicate", registrantQualificationService.getDistinctInstitutionsInRegistrantQualification()));
        form.add(new BookmarkablePageLink<Void>("returnLink", InstitutionListPage.class));

        add(form);

        final WebMarkupContainer wmc2 = new WebMarkupContainer("wmc2");

        wmc2.setVisible(false);

        wmc2.setOutputMarkupPlaceholderTag(true);
        form.add(wmc2);

        form.add(new AjaxCheckBox("advanced2", new PropertyModel(wmc2, "visible")) {
            @Override
            protected void onUpdate(AjaxRequestTarget art) {
                art.add(wmc2);
            }
        });
        wmc2.add(new CheckBoxMultipleChoice("institutions", registrantQualificationService.getDistinctInstitutionsInRegistrantQualification()));
        form.add(new Label("labelTitle","Remove Duplicate Institutions"));
    }

    public InstitutionMapEditPage(Boolean retire) {
        setDefaultModel(new CompoundPropertyModel<Institution>(new LoadableDetachableInstitutionModel()));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        Form<Institution> form = new Form<Institution>("form", (IModel<Institution>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                institutionService.retire(getModelObject().getInstitutions());
                setResponsePage(InstitutionListPage.class);
            }
        };
        form.add(new DropDownChoice("duplicate", institutionService.findAll(Boolean.FALSE)) {

            @Override
            public boolean isVisible() {
                return Boolean.FALSE;
            }

        });
        form.add(new BookmarkablePageLink<Void>("returnLink", InstitutionListPage.class));

        add(form);

        final WebMarkupContainer wmc2 = new WebMarkupContainer("wmc2");

        wmc2.setVisible(false);

        wmc2.setOutputMarkupPlaceholderTag(true);
        form.add(wmc2);

        form.add(new AjaxCheckBox("advanced2", new PropertyModel(wmc2, "visible")) {
            @Override
            protected void onUpdate(AjaxRequestTarget art) {
                art.add(wmc2);
            }
        });
        wmc2.add(new CheckBoxMultipleChoice("institutions", institutionService.findAll(Boolean.FALSE)));
        form.add(new Label("labelTitle","Retire Mulitple Institutions"));
    }

    private final class LoadableDetachableInstitutionModel extends LoadableDetachableModel<Institution> {

        @Override
        protected Institution load() {
            return new Institution();
        }
    }
}
