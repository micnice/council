/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 *
 * @author tdhlakama
 */
@Entity
@Table(name="coursetype")
@Audited
@XmlRootElement
public class CourseType extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

}
