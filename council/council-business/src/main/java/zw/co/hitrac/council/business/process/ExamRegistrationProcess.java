package zw.co.hitrac.council.business.process;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.domain.examinations.*;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.business.service.accounts.AccountsParametersService;
import zw.co.hitrac.council.business.service.accounts.CustomerService;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationDebtComponentService;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationService;
import zw.co.hitrac.council.business.service.examinations.ExamService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.business.utils.DateUtil;

import java.math.BigDecimal;
import java.util.*;

/**
 *
 * @author Matiashe Michael
 * @author Takunda Dhlakama
 */
@Service
public class ExamRegistrationProcess {

    @Autowired
    private ExamService examService;
    @Autowired
    private ExamRegistrationService examRegistrationService;
    @Autowired
    private ModulePaperService modulePaperService;
    @Autowired
    private InvoiceProcess invoiceProcess;
    @Autowired
    private RegistrationService registrationService;
    @Autowired
    private RegistrantQualificationService registrantQualificationService;
    @Autowired
    private RegistrationProcess registrationProcess;
    @Autowired
    private GeneralParametersService generalParametersService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private ExamRegistrationDebtComponentService examRegistrationDebtComponentService;
    @Autowired
    private RegistrantActivityService registrantActivityService;
    @Autowired
    private RegistrantActivityProcess registrantActivityProcess;
    @Autowired
    private CourseService courseService;
    @Autowired
    private AccountsParametersService accountsParametersService;

    @Transactional
    public ExamRegistration register(ExamRegistration examRegistration) {
        DebtComponent debtComponent = null;
        //two methods removed

        if (DateUtil.DifferenceInMonths(examRegistration.getRegistration().getRegistrationDate(), new Date()) < examRegistration.getRegistration().getCourse().getCourseDuration()) {
            throw new CouncilException("Check Date Training period not complete");
        }

        if (!examRegistration.getRegistration().getCourse().getBasic()) {
            if (!hasBasicQualificationCheck(examRegistration)) {
                throw new CouncilException("Cannot Register for a Non Basic Course without a basic Qualification");
            }
        }

        Registration studentRegistration = new Registration();
        studentRegistration.setRegister(generalParametersService.get().getStudentRegister());
        studentRegistration.setRegistrationDate(examRegistration.getRegistration().getRegistrationDate());
        studentRegistration.setCourse(examRegistration.getRegistration().getCourse());
        studentRegistration.setInstitution(examRegistration.getRegistration().getInstitution());
        studentRegistration.setRegistrant(examRegistration.getRegistration().getRegistrant());

        ExamSetting examSetting = examRegistration.getExamSetting();
        Set<ModulePaper> modulePapers = new HashSet<ModulePaper>();

        Course course = courseService.get(examRegistration.getRegistration().getCourse().getId());

        //filter module papers for course and exam settings
        for (ModulePaper modulePaper : examService.findExamPapers(examSetting)) {
            if (modulePaperService.findPapersInCourse(course).contains(modulePaper)) {
                modulePapers.add(modulePaper);
            }
        }
        //filter exams needed settings
        for (Exam exam : examService.findAll(examSetting)) {
            if (modulePapers.contains(exam.getModulePaper())) {
                examRegistration.getExamResults().add(new ExamResult(examRegistration, exam));
            }
        }

        if (examRegistration.getExamResults().isEmpty()) {
            throw new CouncilException("No exams set for that period");
        }

        if (!accountsParametersService.get().getStudentRegistrationPayment()) {
            if (alreadyStudentRegistered(examRegistration)) {
                throw new CouncilException("Already Registered as a Student");
            }
        }

        if (!accountsParametersService.get().getStudentRegistrationPayment()) {
            if (alreadyRegistered(examRegistration)) {
                throw new CouncilException("Already Registered for this course");
            }
        }

        List<Product> productsFound = searchRegistrationProduct(examRegistration.getRegistration(), examRegistration.getExamSetting());
        //Look for the product now

        //Done with billing side
        //Register now
        if (generalParametersService.get().getLastCandidateNumber() == null) {
            throw new CouncilException("Administrator MUST Configure Candidate numbers");
        }

        studentRegistration = registrationProcess.register(studentRegistration);
        examRegistration.setRegistration(studentRegistration);
        examRegistration.setCandidateNumber(generateCandidateNumbers());
        saveGeneratedCandidateNumber();
        examRegistration = examRegistrationService.save(examRegistration);

        for (Product productFound : productsFound) {
            if (productFound.getProductPrice() == null) {
                throw new CouncilException("Product price for " + productFound.getName() + "not set");
            }
            BigDecimal price = productFound.getProductPrice().getPrice();
            Customer customer = customerService.get(examRegistration.getRegistration().getRegistrant().getCustomerAccount().getId());
            Account salesAccount = productFound.getSalesAccount();
            debtComponent = invoiceProcess.invoice(price, customer, salesAccount, productFound);
            ExamRegistrationDebtComponent examRegistrationDebtComponent = new ExamRegistrationDebtComponent();
            examRegistrationDebtComponent.setExamRegistration(examRegistration);
            examRegistrationDebtComponent.setDebtComponent(debtComponent);
            examRegistrationDebtComponentService.save(examRegistrationDebtComponent);
        }
        RegistrantActivity registrantActivity = new RegistrantActivity();
        registrantActivity.setStartDate(examSetting.getStartDate());
        registrantActivity.setEndDate(examSetting.getEndDate()); //Exam Setting Date
        registrantActivity.setRegistrant(examRegistration.getRegistration().getRegistrant());
        registrantActivity.setRegistrantActivityType(RegistrantActivityType.EXAM_REGISTRATION);
        registrantActivityService.save(registrantActivity);

        return examRegistration;
    }

    private List<Product> searchRegistrationProduct(Registration registration, ExamSetting examSetting) {
        List<Product> products = new ArrayList<Product>();
        Set<ModulePaper> modulePapers = new HashSet<ModulePaper>();

        Course course = courseService.get(registration.getCourse().getId());

        //filter module papers for course and exam settings
        for (ModulePaper modulePaper : examService.findExamPapers(examSetting)) {
            if (modulePaperService.findPapersInCourse(course).contains(modulePaper)) {
                modulePapers.add(modulePaper);
            }
        }
        //filter exams needed settings
        for (Exam exam : examService.findAll(examSetting)) {
            if (modulePapers.contains(exam.getModulePaper())) {
                if (exam.getModulePaper().getProduct() == null) {
                    throw new CouncilException("Product for " + exam.getModulePaper().getName() + " not set");
                } else {
                    products.add(exam.getModulePaper().getProduct());
                }
            }
        }

        if (products.isEmpty()) {
            throw new CouncilException("Product for " + registration.getCourse().getName() + " Exam registration not set");
        }
        return products;

    }

    @Transactional
    public ExamRegistration registerPBQ(ExamRegistration examRegistration) {
        DebtComponent debtComponent = null;
        //two methods removed

        if (!registrationProcess.studentNoneRegistration(examRegistration.getRegistration().getRegistrant())) {
            throw new CouncilException("Student Must be on current Renewal Register.");
        }

        if (generalParametersService.get().getHasProcessedByBoard()) {
            if (alreadyStudentRegistered(examRegistration)) {
                throw new CouncilException("Already In Student Register");
            }

            if (alreadyRegistered(examRegistration)) {
                throw new CouncilException("Already Registered for this course");
            }

            if (registrationDateValidation(examRegistration)) {
                throw new CouncilException("Registration date cannot be future date");
            }

        }

        if (examRegistrationService.getExamRegistration(examRegistration.getCandidateNumber())) {
            examRegistration.setCandidateNumber(generateCandidateNumbers());
        }

        if (generalParametersService.get().getLastCandidateNumber() == null) {
            throw new CouncilException("Administrator MUST Configure Candidate numbers");
        }
        if (generalParametersService.get().getHasProcessedByBoard()) {
            Registration studentRegistration = new Registration();
            studentRegistration.setRegister(generalParametersService.get().getStudentRegister());
            studentRegistration.setRegistrationDate(new Date());
            studentRegistration.setCourse(examRegistration.getRegistration().getCourse());
            studentRegistration.setInstitution(examRegistration.getRegistration().getInstitution());
            studentRegistration.setRegistrant(examRegistration.getRegistration().getRegistrant());
            studentRegistration = registrationService.save(studentRegistration);
            examRegistration.setRegistration(studentRegistration);
        }

        ExamSetting examSetting = examRegistration.getExamSetting();
        Set<ModulePaper> modulePapers = new HashSet<ModulePaper>();

        //filter module papers for course and exam settings
        for (ModulePaper modulePaper : examService.findExamPapers(examSetting)) {
            if (modulePaperService.findPapersInCourse(examRegistration.getRegistration().getCourse()).contains(modulePaper)) {
                modulePapers.add(modulePaper);
            }
        }
        //filter exams needed settings
        for (Exam exam : examService.findAll(examSetting)) {
            if (modulePapers.contains(exam.getModulePaper())) {
                examRegistration.getExamResults().add(new ExamResult(examRegistration, exam));
            }
        }

        if (examRegistration.getExamResults().isEmpty()) {
            throw new CouncilException("No exams set for that period");
        }
        List<Product> productsFound = searchRegistrationProduct(examRegistration.getRegistration(), examRegistration.getExamSetting());

        //Done with billing side
        //Register now
        //examRegistration.getRegistration().setDebtComponent(debtComponent);
        examRegistration.setCandidateNumber(generateCandidateNumbers());
        saveGeneratedCandidateNumber();
        examRegistration = examRegistrationService.save(examRegistration);

        //Look for the product now
        for (Product productFound : productsFound) {
            BigDecimal price = productFound.getProductPrice().getPrice();

            Customer customer = examRegistration.getRegistration().getRegistrant().getCustomerAccount();
            Account salesAccount = productFound.getSalesAccount();
            debtComponent = invoiceProcess.invoice(price, customer, salesAccount, productFound);

            ExamRegistrationDebtComponent examRegistrationDebtComponent = new ExamRegistrationDebtComponent();
            examRegistrationDebtComponent.setExamRegistration(examRegistration);
            examRegistrationDebtComponent.setDebtComponent(debtComponent);
            examRegistrationDebtComponentService.save(examRegistrationDebtComponent);
        }

        registrantActivityProcess.examRegistrationRegistrantActivity(examRegistration.getRegistration(), examSetting);

        return examRegistration;
    }

    public boolean registerPBQ(Registrant registrant) {
        /// Please Not for Registration with Null Value- Registration will allow
        boolean status = false;
        for (Registration r : registrationService.getRegistrations(registrant)) {
            if (r.getRegister().equals(generalParametersService.get().getMainRegister())) {
                if (r.getRegistrationDate() != null) {
                    if (DateUtil.DifferenceInYears(r.getRegistrationDate(), new Date()) > generalParametersService.get().getMinimumPeriodToStartPBQ()) {
                        status = true;
                    }
                    return Boolean.FALSE;
                } else {
                    return Boolean.TRUE;
                }
            }
        }
        return status;
    }

    public String generateCandidateNumbers() {
        int lastCandidateNumber = generalParametersService.get().getLastCandidateNumber();
        lastCandidateNumber = lastCandidateNumber + 3;
        Calendar calendar = Calendar.getInstance();
        return generalParametersService.get().getCouncilAbbreviation() + "/" + String.valueOf(calendar.get(Calendar.YEAR) + "/" + lastCandidateNumber);
    }

    @Transactional
    public void saveGeneratedCandidateNumber() {
        int lastCandidateNumber = generalParametersService.get().getLastCandidateNumber();
        GeneralParameters generalParameters = generalParametersService.get();
        lastCandidateNumber = lastCandidateNumber + 3;
        generalParameters.setLastCandidateNumber(lastCandidateNumber);
        generalParametersService.save(generalParameters);
    }

    public boolean registrationDateValidation(ExamRegistration examRegistration) {
        boolean status = false;
        if (examRegistration.getRegistration().getRegistrationDate().after(new Date())) {
            status = true;
        }
        return status;
    }

    public boolean alreadyRegistered(ExamRegistration examRegistration) {
        boolean status = false;
        Registration studentRegistration = registrationProcess.activeStudentRegister(examRegistration.getRegistration().getRegistrant());
        if (studentRegistration != null) {
            if (studentRegistration.getCourse().equals(examRegistration.getRegistration().getCourse())) {
                status = true;
            }
        }
        return status;
    }

    public boolean alreadyStudentRegistered(ExamRegistration examRegistration) {
        boolean status = false;
        Registration studentRegistration = registrationProcess.activeStudentRegister(examRegistration.getRegistration().getRegistrant());
        if (studentRegistration != null) {
            status = true;
        }
        return status;
    }

    public boolean hasBasicQualificationCheck(ExamRegistration examRegistration) {
        boolean status = false;
        for (RegistrantQualification q : registrantQualificationService.getRegistrantQualifications(examRegistration.getRegistration().getRegistrant())) {
            if (q.getQualification().getPbq()) {
                return Boolean.TRUE;
            }
        }
        return status;
    }

    @Transactional
    public ExamRegistration saveExamRegistrationCandidateNumber(ExamRegistration examRegistration) {
        examRegistration.setCandidateNumber(generateCandidateNumbers());
        saveGeneratedCandidateNumber();
        return (ExamRegistration) examRegistrationService.save(examRegistration);
    }
}
