/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.application;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.Page;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PageableListView;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigator;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.ApplicationPurpose;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.institution.InstitutionApplicationViewPage;

import java.util.List;

//~--- JDK imports ------------------------------------------------------------

/**
 * @author Michael Matiashe
 */
public class UnApprovedApplicationsDataViewPanel extends Panel {

    public UnApprovedApplicationsDataViewPanel(String id, IModel<List<Application>> model) {

        super(id);

        PageableListView<Application> eachItem = new PageableListView<Application>("eachItem", model, 20) {

            @Override
            protected void populateItem(ListItem<Application> item) {

                Link<Application> viewLink = new Link<Application>("applicationViewPage", item.getModel()) {

                    @Override
                    public void onClick() {

                        Application application = getModel().getObject();
                        ApplicationPurpose applicationPurpose = application.getApplicationPurpose();
                        Page nextPage;

                        switch (applicationPurpose) {

                            case INSTITUTION_REGISTRATION:

                                nextPage = new InstitutionApplicationViewPage(getModelObject().getId());
                                break;

                            case CERTIFICATE_OF_GOOD_STANDING:

                                nextPage = new CgsApplicationViewPage(application.getId());
                                break;

                            case CHANGE_OF_NAME:

                                nextPage = new ChangeOfNameApplicationViewPage(application.getId());
                                break;

                            case PROVISIONAL_QUALIIFICATION:

                                nextPage = new QualificationApplicationViewPage(application.getId());
                                break;

                            case ADDITIONAL_QUALIIFICATION:

                                nextPage = new QualificationApplicationViewPage(application.getId());
                                break;

                            case TRANSFER:

                                nextPage = new TransferApplicationViewPage(application.getId());
                                break;

                            default:

                                nextPage = new ApplicationViewPage(getModelObject().getId());
                        }

                        setResponsePage(nextPage);
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.setModel(new CompoundPropertyModel<Application>(item.getModel()));
                item.add(viewLink);
                item.add(new Label("registrant.fullname"));
                viewLink.add(new Label("applicationPurpose"));
                item.add(new Label("applicationDate"));
                item.add(new Label("institution"));
                item.add(new Label("course"));
                item.add(new Label("applicationTextStatus"));
            }
        };
        add(new PagingNavigator("navigator", eachItem));
        add(eachItem);
    }
}
