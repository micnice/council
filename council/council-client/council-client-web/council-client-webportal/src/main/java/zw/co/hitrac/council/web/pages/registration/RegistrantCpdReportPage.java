package zw.co.hitrac.council.web.pages.registration;

//~--- non-JDK imports ---------------------------------------

import java.math.BigDecimal;

import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.service.RegistrantCpdService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;

//~--- JDK imports ------------------------------------------------------------
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantCpd;
import zw.co.hitrac.council.business.service.DurationService;
import zw.co.hitrac.council.reports.RegistrantCPDHistory;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 * @author Constance Mabaso
 * @author Matiashe Michael
 * @author tdhlakama
 */
public class RegistrantCpdReportPage extends TemplatePage {

    @SpringBean
    private DurationService durationService;
    private List<RegistrantCpd> items = new ArrayList<RegistrantCpd>();
    private BigDecimal total = BigDecimal.ZERO;
    private Duration duration;
    @SpringBean
    private RegistrantCpdService registrantCpdService;

    public RegistrantCpdReportPage(final IModel<Registrant> registrantModel) {
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());
        add(new RegistrantPanel("registrantPanel", registrantModel.getObject().getId()));
        PropertyModel<Duration> durationModel = new PropertyModel<Duration>(this, "duration");

        Form<?> form = new Form("form");

        form.add(new DropDownChoice("duration", durationModel, durationService.findAll()).setRequired(true).add(new ErrorBehavior()));

        form.add(new Button("process") {
            @Override
            public void onSubmit() {
                try {
                    RegistrantCPDHistory registrantCPDHistory = new RegistrantCPDHistory();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();
                    List<RegistrantCpd> registrantCpdList = registrantCpdService.getCurrentCPDsList(registrantModel.getObject(), duration);
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(registrantCPDHistory, contentType,
                            registrantCpdList, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }

        });

        form.add(new Link<Registrant>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });

        add(form);
    }

}
