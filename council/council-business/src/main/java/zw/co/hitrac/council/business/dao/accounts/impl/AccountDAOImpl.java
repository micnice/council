package zw.co.hitrac.council.business.dao.accounts.impl;

import java.math.BigDecimal;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.accounts.AccountDAO;
import zw.co.hitrac.council.business.dao.repo.accounts.AccountRepository;
import zw.co.hitrac.council.business.domain.accounts.Account;

/**
 *
 * @author Tatenda Chiwandire
 * @author Michael Matiashe
 */
@Repository
public class AccountDAOImpl implements AccountDAO {

    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private AccountRepository accountRepository;

    public Account save(Account t) {
        return accountRepository.save(t);
    }

    public List<Account> findAll() {
        return accountRepository.findAll();
    }

    public Account get(Long id) {
        return accountRepository.findOne(id);
    }

    public AccountRepository getAccountRepository() {
        return accountRepository;
    }

    public void setAccountRepository(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public List<Account> getGLAccounts() {
        return entityManager.createQuery("select DISTINCT a from Account a where a.personalAccount = false").getResultList();
    }

    public List<Account> getPersonalAccountsErrors() {
      return entityManager.createQuery("select DISTINCT a from Account a where a.balance > 0.0 and a.personalAccount = true").getResultList();
    }

    public List<Account> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
