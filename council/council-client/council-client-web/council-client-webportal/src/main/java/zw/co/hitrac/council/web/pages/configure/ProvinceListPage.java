package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Province;
import zw.co.hitrac.council.business.service.ProvinceService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;

/**
 *
 * @author Kelvin Goredema
 */
public class ProvinceListPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private ProvinceService provinceService;

    public ProvinceListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new ProvinceEditPage(null, getPageReference()));
            }
        });

        IModel<List<Province>> model = new LoadableDetachableModel<List<Province>>() {
            @Override
            protected List<Province> load() {
                return provinceService.findAll();
            }
        };
        PropertyListView<Province> eachItem = new PropertyListView<Province>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<Province> item) {
                Link<Province> viewLink = new Link<Province>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new ProvinceViewPage(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };

        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
