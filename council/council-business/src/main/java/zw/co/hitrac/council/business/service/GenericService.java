/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author tdhlakama
 */
public interface GenericService extends Serializable {

    public List<Long> getAllLean(String ClassName);

    public Object getLean(String ClassName, Long id);
}
