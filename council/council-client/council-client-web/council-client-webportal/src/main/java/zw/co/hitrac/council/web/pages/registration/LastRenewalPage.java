package zw.co.hitrac.council.web.pages.registration;

//~--- non-JDK imports --------------------------------------------------------
import zw.co.hitrac.council.web.pages.reports.*;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantActivity;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.process.RegistrantActivityProcess;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

/**
 *
 * @author Matiashe
 */
public class LastRenewalPage extends IReportPage {

    @SpringBean
    private RegistrantActivityProcess registrantActivityProcess;
    @SpringBean
    private RegistrationProcess registrationProcess;
    @SpringBean
    private CourseService courseService;

    public LastRenewalPage(final IModel<Registrant> registrantModel) {
        Form<RegistrantActivity> form = new Form<RegistrantActivity>("form2",
                new CompoundPropertyModel<RegistrantActivity>(new RegistrantActivity())) {
            @Override
            protected void onSubmit() {
               Registration registration=registrationProcess.activeRegisterNotStudentRegister(registrantModel.getObject());
               registrantActivityProcess.lastRenewalActivity(registration, getModelObject());
               setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        };

        form.add(new TextField("startDate").add(DatePickerUtil.getDatePicker()).add(new ErrorBehavior()));
        add(form);

    }
}


//~ Formatted by Jindent --- http://www.jindent.com
