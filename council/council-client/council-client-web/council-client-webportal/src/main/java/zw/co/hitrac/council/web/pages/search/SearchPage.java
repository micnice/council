package zw.co.hitrac.council.web.pages.search;

//~--- non-JDK imports --------------------------------------------------------
import zw.co.hitrac.council.web.pages.reports.EmploymentReportPage;
import javax.sql.DataSource;
import zw.co.hitrac.council.web.pages.application.ApplicationSearchPage;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.service.GeneralParametersService;

import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author Takunda Dhlakama
 */
public class SearchPage extends TemplatePage {

    @SpringBean
    private DataSource dataSource;
    @SpringBean
    private GeneralParametersService generalParametersService;

    public SearchPage() {
        menuPanelManager.getSearchPanel().setTopMenuCurrent(true);
        add(new BookmarkablePageLink<Void>("searchSimplePage", SearchRegistrantPage.class));
        add(new BookmarkablePageLink<Void>("searchAdvancedPage", SearchRegistrantAdvancedPage.class));
        add(new BookmarkablePageLink<Void>("applicationSimpleSearchPage", ApplicationSearchPage.class));
        add(new BookmarkablePageLink<Void>("searchRegistrantActivityPage", SearchRegistrantActivityPage.class));
        
        
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
