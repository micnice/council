package zw.co.hitrac.council.web.pages.reports;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Qualification;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.QualificationService;
import zw.co.hitrac.council.business.service.RegistrantQualificationService;
import zw.co.hitrac.council.reports.RegistrantDataDBReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.models.QualificationListModel;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 * @author tdhlakama
 */
public class QualificationMappedQualificationReport extends TemplatePage {

    private Qualification qualification;
    private Qualification qualification2;
    @SpringBean
    private QualificationService qualificationService;
    @SpringBean
    private DataSource dataSource;
    @SpringBean
    private RegistrantQualificationService registrantQualificationService;
    private ContentType contentType;

    public QualificationMappedQualificationReport() {
        menuPanelManager.getSearchPanel().setTopMenuCurrent(true);
        menuPanelManager.getSearchPanel().setSearchAdvanced(true);

        PropertyModel<Qualification> qualificationModel = new PropertyModel<Qualification>(this, "qualification");
        PropertyModel<ContentType> contentTypeModel = new PropertyModel<ContentType>(this, "contentType");
        PropertyModel<Qualification> qualificationModel2 = new PropertyModel<Qualification>(this, "qualification2");
        Form<?> form = new Form("form");
        form.add(new DropDownChoice("qualification", qualificationModel, new QualificationListModel(qualificationService)));
        form.add(new DropDownChoice("qualification2", qualificationModel2, new QualificationListModel(qualificationService)));
        form.add(new DropDownChoice("contentType", contentTypeModel, Arrays.asList(ContentType.values())));
        
        add(form);

        form.add(new Button("process") {
            @Override
            public void onSubmit() {
                printReport();
            }
        });
        add(new FeedbackPanel("feedback"));
    }

    private void printReport() {
        Map parameters = new HashMap();
        RegistrantDataDBReport registrantDataDBReport = new RegistrantDataDBReport();
        try {

            List<RegistrantData> registrants = registrantQualificationService.getRegistrantDataListWithBoth(qualification, qualification2);

            parameters.put("comment", qualification + " Detailed Register for those with the " + qualification2);

            String list = "";
            for (RegistrantData d : registrants) {
                list += d.getId().toString() + ",";
            }
            if (!list.isEmpty()) {
                list = list.substring(0, list.length() - 1);
            }
            parameters.put("list", list);
            
            final Connection connection = dataSource.getConnection(); //DBConnect.getConnection();
            ByteArrayResource resource = null;
            try {
                resource = ReportResourceUtils.getReportResource(registrantDataDBReport, contentType, connection, parameters);
            } finally {
                connection.close();
            }
            IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                    RequestCycle.get().getResponse(), null);

            resource.respond(a);

            // To make Wicket stop processing form after sending response
            RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
        } catch (JRException ex) {
            ex.printStackTrace(System.out);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }

    }

}


//~ Formatted by Jindent --- http://www.jindent.com
