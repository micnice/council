package zw.co.hitrac.council.web.pages.reports;

//~--- non-JDK imports --------------------------------------------------------
import java.util.ArrayList;
import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.domain.EmploymentType;
import zw.co.hitrac.council.business.domain.InstitutionCategory;
import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.business.domain.RegistrantActivityType;

import zw.co.hitrac.council.business.service.InstitutionService;

//~--- JDK imports ------------------------------------------------------------
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.web.pages.institution.IInstitutionView;

/**
 *
 * @author Charles Chigoriwa
 */
public class InstitutionCustomViewPage extends IInstitutionView {

    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    private HrisComparator hrisComparator = new HrisComparator();

    public InstitutionCustomViewPage() {

        add(new Label("healthInstitutionType", generalParametersService.get().getHealthInstitutionType()));
        add(new Label("privateTotal", institutionService.getTotalCount(null, null, null, generalParametersService.get().getHealthInstitutionType(), InstitutionCategory.PRIVATE)));
        add(new Label("publicTotal", institutionService.getTotalCount(null, null, null, generalParametersService.get().getHealthInstitutionType(), InstitutionCategory.PUBLIC)));
        
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
