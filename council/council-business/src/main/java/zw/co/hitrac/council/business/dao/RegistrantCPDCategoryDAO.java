package zw.co.hitrac.council.business.dao;

import zw.co.hitrac.council.business.domain.RegistrantCPDCategory;

/**
 *
 * @author Constance Mabaso
 */
public interface RegistrantCPDCategoryDAO extends IGenericDAO<RegistrantCPDCategory> {
    
    
}
