/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.examinations;

import java.util.List;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistrationDebtComponent;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.service.IGenericService;

/**
 *
 * @author tidza
 */
public interface ExamRegistrationDebtComponentService extends IGenericService<ExamRegistrationDebtComponent> {

    public List<ExamRegistrationDebtComponent> getDebtComponents(ExamSetting examSetting, Course course, Institution institution);
    public List<ExamRegistrationDebtComponent> getDebtComponents(ExamSetting examSetting, Product product, Institution institution);
    public List<ExamRegistrationDebtComponent> getDebtComponents(ExamRegistration examRegistration);
}
