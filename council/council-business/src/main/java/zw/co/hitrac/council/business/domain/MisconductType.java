/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 *
 * @author hitrac
 */
@Entity
@Table(name="misconducttype")
@Audited
public class MisconductType extends BaseEntity implements Serializable, Comparable<MisconductType>{

    @Override
    @Transient
    public String toString() {

        return super.toString();
    }

    @Override
    @Transient
    //For use in jasper reports cross tabs and other uses
    public int compareTo(MisconductType o) {

        return ComparisonChain.start()
                .compare(this.getName(), o.getName(), Ordering.natural().nullsLast())
                .result();
    }
}
