package zw.co.hitrac.council.web.pages.registration;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.RegisterService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.CourseListModel;
import zw.co.hitrac.council.web.models.InstitutionListModel;
import zw.co.hitrac.council.web.models.RegisterListModel;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.CustomDateTextField;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

//~--- JDK imports ------------------------------------------------------------
import java.util.Date;
import java.util.List;

import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import zw.co.hitrac.council.business.process.ApplicationProcess;
import zw.co.hitrac.council.business.service.ApplicationService;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.pages.accounts.documents.DebtComponentsPage;
import zw.co.hitrac.council.web.pages.certification.CertificationPage;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;

import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;

/**
 * @author Takunda Dhlakama
 * @author Michael Matiashe
 * @author Daniel Nkhoma
 */
public class RegistrationPage extends TemplatePage {

    @SpringBean
    private RegistrationService registrationService;
    @SpringBean
    private RegisterService registerService;
    @SpringBean
    private CourseService courseService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private RegistrationProcess registrationProcess;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private DebtComponentService debtComponentService;
    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private ApplicationService applicationService;
    @SpringBean
    private ApplicationProcess applicationProcess;

    public LoadableDetachableRegistrationModel registrationModel;

    private CompoundPropertyModel<Registration> createCompoundPropertyModel() {
        CompoundPropertyModel<Registration> model = new CompoundPropertyModel<>(registrationModel);
        return model;
    }

    private void createRegistrationModel(Long id, IModel<Registrant> registrantModel, IModel<Application> applicationIModel, Boolean upgrade, Boolean captureFromFile) {

        Application application = null;
        if (applicationIModel != null) {
            application = applicationIModel.getObject();
        }

        Registrant registrant = null;
        if (registrantModel != null) {
            registrant = registrantModel.getObject();
        }

        registrationModel = new RegistrationPage.LoadableDetachableRegistrationModel(id, registrant, application, upgrade, captureFromFile);

    }

    public RegistrationPage(Long id, IModel<Registrant> registrantModel, IModel<Application> applicationIModel, Boolean upgrade, Boolean captureFromFile) {

        createRegistrationModel(id, registrantModel, applicationIModel, upgrade, captureFromFile);

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrationModel.getObject().getRegistrant().getId());
        add(new RegistrantPanel("registrantPanel", registrationModel.getObject().getRegistrant().getId()));
        setDefaultModel(registrationModel);

        add(createRegistrationForm(upgrade, captureFromFile));
        add(new FeedbackPanel("feedback"));
    }

    private Form<Registration> createRegistrationForm(Boolean upgrade, Boolean captureFromFile) {

        Boolean administratorEnabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR)));
        Form<Registration> form = new Form<Registration>("form", createCompoundPropertyModel()) {
            @Override
            public void onSubmit() {
                try {
                    Registration registration = registrationModel.getObject();
                    if (registration.getId() == null && registration.getApplication() != null) {
                        registrationProcess.register(getModelObject());
                        setResponsePage(new RegistrantPersonalConditionPage(Model.of(registration.getRegistrant()), Boolean.FALSE));
                    } else if (registration.getId() != null) {
                        registrationService.save(getModelObject());
                        setResponsePage(new RegistrantViewPage(registration.getRegistrant().getId()));
                    } else if (registration.getId() == null && upgrade == null && captureFromFile == null) {
                        try {
                            registration = registrationProcess.register(registration);
                            Registrant registrant = registrantService.get(registration.getRegistrant().getId());
                            List<DebtComponent> debtComponents = debtComponentService.getDebtComponents(registrant.getCustomerAccount());
                            if (!generalParametersService.get().getRegistrationByApplication()) {
                                setResponsePage(new RequirementConfirmationPage(registration.getId()));
                            } else {
                                if (debtComponents.isEmpty()) {
                                    setResponsePage(new RegistrantViewPage(registrant.getId()));
                                } else {
                                    setResponsePage(new DebtComponentsPage(registrant.getId()));
                                }
                            }
                        } catch (CouncilException ex) {
                            error(ex.getMessage());
                        }
                    } else if (registration.getId() == null && upgrade != null && upgrade) {
                        registration = registrationProcess.registerUpGrade(registration);
                        Registrant registrant = registrantService.get(registration.getRegistrant().getId());
                        List<DebtComponent> debtComponents
                                = debtComponentService.getDebtComponents(registrant.getCustomerAccount());

                        if (debtComponents.isEmpty()) {
                            setResponsePage(new RegistrantViewPage(registrant.getId()));
                        } else {

                            //select items to pay for
                            setResponsePage(new DebtComponentsPage(registrant.getId()));
                        }
                    } else if (registration.getId() == null && captureFromFile != null) {
                        if (captureFromFile) {
                            registration = registrationProcess.captureFromFile(registration);
                            setResponsePage(new RegistrantViewPage(registration.getRegistrant().getId()));
                        } else {
                            registration = registrationProcess.captureFromPreRegFile(registration);
                            if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                                setResponsePage(new RegistrantConditionEditPage(Model.of(registration.getRegistrant()), Boolean.FALSE));
                            } else {
                                setResponsePage(new CertificationPage(registration.getRegistrant().getId()));
                            }
                        }
                    }

                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };

        form.add(new DropDownChoice("course",
                new CourseListModel(courseService)).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("register",
                new RegisterListModel(registerService)).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("institution", new InstitutionListModel(institutionService)) {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.FALSE);
                }
            }

        });
        form.add(
                new CustomDateTextField("registrationDate").setRequired(true).add(new ErrorBehavior()).add(
                        DatePickerUtil.getDatePicker()));

        form.add(new CustomDateTextField("deRegistrationDate") {
            @Override
            protected void onConfigure() {
                setVisible(administratorEnabled);
            }

        }).add(DatePickerUtil.getDatePicker());
        form.add(new Label("deRegistrationLabelDate", "Registration Closed Date") {
            @Override
            protected void onConfigure() {
                setVisible(administratorEnabled);
            }

        });

        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrationModel.getObject().getRegistrant().getId()));
            }
        });

        return form;
    }


    private final class LoadableDetachableRegistrationModel extends LoadableDetachableModel<Registration> {

        private Long id;
        private Registrant registrant;
        private Boolean upgrade;
        private Boolean captureFromFile;
        private Application application;

        public LoadableDetachableRegistrationModel(Long id, Registrant registrant, Application application, Boolean upgrade, Boolean captureFromFile) {
            this.id = id;
            this.registrant = registrant;
            this.application = application;
            this.upgrade = upgrade;
            this.captureFromFile = captureFromFile;
        }

        @Override
        protected Registration load() {
            Registration registration = null;

            if (application != null) {
                registration = new Registration();
                registration.setRegistrant(application.getRegistrant());
                registration.setRegistrationDate(new Date());
                registration.setCourse(application.getApprovedCourse());
                registration.setApplication(application);
                registration.setInstitution(application.getInstitution());
                if (application.getApplicationPurpose().equals(ApplicationPurpose.TRANSFER)) {
                    registration.setRegister(application.getTransferRule().getToRegister());
                } else {
                    registration.setRegister(applicationProcess.getApplicationRegister(application));
                }
            } else if (registrant != null && upgrade == null && captureFromFile == null) {
                registration = new Registration();
                registration.setRegistrant(registrant);
                registration.setRegister(generalParametersService.get().getMainRegister());
                registration.setRegistrationDate(new Date());
            } else if (registrant != null && upgrade != null && upgrade) {
                registration = new Registration();
                registration.setRegistrant(registrant);
                registration.setRegister(generalParametersService.get().getMainRegister());
                registration.setRegistrationDate(new Date());
            } else if (registrant != null && captureFromFile != null && captureFromFile) {
                registration = new Registration();
                registration.setRegistrant(registrant);
            } else {
                registration = registrationService.get(id);
            }
            return registration;
        }
    }


}


//~ Formatted by Jindent --- http://www.jindent.com
