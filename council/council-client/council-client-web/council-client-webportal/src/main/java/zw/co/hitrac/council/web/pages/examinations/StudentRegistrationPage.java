package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.business.service.examinations.ExamSettingService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.registration.RegistrantViewPage;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.service.SupportDocumentService;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.web.models.BasicCourseListModel;
import zw.co.hitrac.council.web.models.CourseListModel;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.CustomDateTextField;

/**
 *
 * @author Michael Matiashe
 * @author Judge Muzinda
 */
public class StudentRegistrationPage extends TemplatePage {

    @SpringBean
    private ExamSettingService examSettingService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private RegistrationProcess registrationProcess;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private CourseService courseService;
    @SpringBean
    private DebtComponentService debtComponentService;
    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private SupportDocumentService supportDocumentService;
    private Course course;
    private HrisComparator hrisComparator = new HrisComparator();

    public StudentRegistrationPage(final IModel<Registrant> registrantModel) {
        CompoundPropertyModel<Registration> model = new CompoundPropertyModel<Registration>(new Registration());

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());
        PropertyModel<Course> courseModel = new PropertyModel<Course>(this, "course");
        PropertyModel<Institution> institutionModel = new PropertyModel<Institution>(this, "institution");
        model.getObject();
        model.getObject().setRegistrant(registrantModel.getObject());
        model.getObject().setRegister(generalParametersService.get().getStudentRegister());
        model.getObject().setDateCreated(new Date());
        setDefaultModel(model);
        
        Form<Registration> form = new Form<Registration>("form", (IModel<Registration>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    getModel().getObject().setCourse(course);
                    registrationProcess.captureFromFile(getModelObject());
                    setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });
        form.add(new DropDownChoice("course", courseModel, new BasicCourseListModel(courseService), new ChoiceRenderer<Course>()) {
            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }

            @Override
            public boolean isRequired() {
                return true;
            }
        }.add(new ErrorBehavior()));

        form.add(new CustomDateTextField("registrationDate").setRequired(true).add(new ErrorBehavior()).add(DatePickerUtil.getDatePicker()));
        form.add(new DropDownChoice("institution", new InstitutionsModel()).setRequired(true).add(new ErrorBehavior()));
        add(form);
        add(new Label("registrant.fullname"));
        add(new FeedbackPanel("feedback"));
    }

    public StudentRegistrationPage(final IModel<Registrant> registrantModel, final IModel<Registration> registrationIModel) {
        CompoundPropertyModel<Registration> model = new CompoundPropertyModel<Registration>(new Registration());

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());
        PropertyModel<Course> courseModel = new PropertyModel<Course>(this, "course");

        model.getObject().setRegistrant(registrantModel.getObject());
        model.getObject().setRegister(generalParametersService.get().getStudentRegister());
        setDefaultModel(model);

        Form<Registration> form = new Form<Registration>("form", (IModel<Registration>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    getModel().getObject().setInstitution(getModel().getObject().getInstitution());
                    getModel().getObject().setCourse(course);
                    Registration registration = registrationProcess.register(getModel().getObject());
                    setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };

        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });

        form.add(new DropDownChoice("course", courseModel, new CourseListModel(courseService), new ChoiceRenderer<Course>()) {
            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }

            @Override
            public boolean isRequired() {
                return true;
            }
        }.add(new ErrorBehavior()));

        form.add(new CustomDateTextField("registrationDate").setRequired(true).add(new ErrorBehavior()).add(DatePickerUtil.getDatePicker()));
        form.add(new DropDownChoice("institution", new InstitutionsModel()).setRequired(true).add(new ErrorBehavior()));
        add(form);
        add(new Label("registrant.fullname"));
        add(new FeedbackPanel("feedback"));

    }

    private class InstitutionsModel extends LoadableDetachableModel<List<? extends Institution>> {

        protected List<? extends Institution> load() {
            if (course != null) {
                return (List<Institution>) hrisComparator.sort((new ArrayList<Institution>(courseService.get(course.getId()).getTrainingInstitutions())));
            } else {
                return new ArrayList<Institution>();
            }
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
