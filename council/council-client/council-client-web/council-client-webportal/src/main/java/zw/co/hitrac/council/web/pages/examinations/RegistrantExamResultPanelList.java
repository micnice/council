
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.Panel;

import zw.co.hitrac.council.business.domain.examinations.ExamResult;

/**
 *
 * @author tdhlakama
 */
public class RegistrantExamResultPanelList extends Panel {

    /**
     * Constructor
     *
     * @param id
     */
    public RegistrantExamResultPanelList(String id) {
        super(id);

        // - Used to Display a to list of in Exam Result
        add(new PropertyListView<ExamResult>("examResultsList") {
            @Override
            protected void populateItem(ListItem<ExamResult> item) {
                Link<ExamResult> viewLink = new Link<ExamResult>("editExamResult", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new ExamResultEditPage(getModelObject().getId()));
                    }

                    @Override
                    protected void onConfigure() {
                        if (getModel().getObject().getPreviousExamRegistration() != null) {
                            setVisible(Boolean.FALSE);
                        } else {
                            if (getModel().getObject().getExamRegistration().getClosed()) {
                                setVisible(Boolean.FALSE);
                            }
                        }
                    }
                };

                item.add(viewLink);
                item.add(new Label("examinationPeriod"));
                item.add(new Label("mark"));
                item.add(new Label("status"));
                item.add(new Label("examAttendanceStatus"));
                item.add(new Label("exam"));
            }
        });
        add(new Label("passTextStatus"));
        add(new Label("passComment"));
        
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
