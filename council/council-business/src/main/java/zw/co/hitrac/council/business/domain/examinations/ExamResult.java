/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain.examinations;

import org.apache.commons.lang.BooleanUtils;
import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.domain.BaseIdEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author tdhlakama
 */
@Entity
@Table(name = "examresult")
@Audited
public class ExamResult extends BaseIdEntity implements Serializable {

    public static String PASS = "PASS";
    public static String FAIL = "FAIL";
    public static String PRESENT = "PRESENT";
    public static String NOTPRESENT = "NOTPRESENT";
    public static String RESULTPENDING = "RESULT PENDING";
    public static String DISQUALIFIED = "DISQUALIFIED";
    private Double mark;
    private ExamRegistration examRegistration;
    private Exam exam;
    private Boolean examAttendance = Boolean.FALSE;
    private Boolean supplementaryExam = Boolean.FALSE;
    private ExamRegistration previousExamRegistration;
    private Boolean disqualified = Boolean.FALSE;

    public ExamResult() {
    }

    public ExamResult(ExamRegistration examRegistration, Exam exam) {
        this.examRegistration = examRegistration;
        this.exam = exam;
    }

    public ExamResult(Double mark, ExamRegistration examRegistration, Exam exam) {
        this.mark = mark;
        this.examRegistration = examRegistration;
        this.exam = exam;
    }

    public Boolean getDisqualified() {
        return BooleanUtils.toBoolean(disqualified);
    }

    public void setDisqualified(Boolean disqualified) {
        this.disqualified = disqualified;
    }

    @Column
    public Double getMark() {
        return mark;
    }

    public void setMark(Double mark) {
        this.mark = mark;
    }

    @ManyToOne
    public ExamRegistration getExamRegistration() {
        return examRegistration;
    }

    public void setExamRegistration(ExamRegistration examRegistration) {
        this.examRegistration = examRegistration;
    }

    @ManyToOne
    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    public Boolean getExamAttendance() {
        return BooleanUtils.toBoolean(examAttendance);
   }

    public void setExamAttendance(Boolean examAttendance) {
        this.examAttendance = examAttendance;
    }

    @Override
    public String toString() {
        if (exam == null) {
            return "";
        }
        return getExam().getModulePaper().getName();
    }

    @Transient
    public String getStatus() {
        if (!getExamAttendance()) {
            return RESULTPENDING;
        }
        if (getDisqualified()) {
            return DISQUALIFIED;
        }
        if (mark != null) {
            return getExamStatus() ? PASS : FAIL;
        } else {
            return RESULTPENDING;
        }
    }

    @Transient
    public String getExamAttendanceStatus() {
        return getExamAttendance() ? PRESENT : NOTPRESENT;
    }

    @Transient //Pass Status
    public Boolean getExamStatus() {
        if (mark == null) {
            return Boolean.FALSE;
        }
        if (getExam().getPassMark() == null) {
            return Boolean.FALSE;
        }
        if (getExam().getPassMark() > mark) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + (this.exam != null ? this.exam.hashCode() : 0);
        hash = 37 * hash + (this.getId() != null ? this.getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ExamResult other = (ExamResult) obj;
        if (this.exam != other.exam && (this.exam == null || !this.exam.equals(other.exam))) {
            return false;
        }
        if (this.getId() != other.getId() && (this.getId() == null || !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Transient
    public String getSupplementaryExamStatus() {
        if (getSupplementaryExam() != null) {
            return getSupplementaryExam() ? "Supplementry" : "";
        } else {
            return "";
        }
    }

    @Transient
    public String getExaminationPeriod() {
        //Show that the paper in exam in question is a supplementary exam, show the registration period
        return getSupplementaryExam() ? previousExamRegistration.getExamSetting().toString() : this.getExamRegistration().getExamSetting().toString();
    }

    public Boolean getSupplementaryExam() {
        return BooleanUtils.toBoolean(supplementaryExam);
    }

    public void setSupplementaryExam(Boolean supplementaryExam) {
        this.supplementaryExam = supplementaryExam;
    }

    @ManyToOne
    public ExamRegistration getPreviousExamRegistration() {
        return previousExamRegistration;
    }

    public void setPreviousExamRegistration(ExamRegistration previousExamRegistration) {
        this.previousExamRegistration = previousExamRegistration;
    }

    @Transient
    public String getExamPaper() {
        if (exam == null) {
            return "";
        }
        return examRegistration.getCandidateNumber() + "-" + getExam().getModulePaper().getName();
    }

}
