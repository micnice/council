
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.RegistrantCpdItemConfig;
import zw.co.hitrac.council.business.domain.RegistrantCpdItemConfig;
import zw.co.hitrac.council.business.service.RegistrantCpdItemConfigService;
import zw.co.hitrac.council.business.service.RegistrantCpdItemConfigService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author hitrac
 */
public class RegistrantCpdItemConfigViewPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private RegistrantCpdItemConfigService addressTypeService;

    public RegistrantCpdItemConfigViewPage(Long id) {
        CompoundPropertyModel<RegistrantCpdItemConfig> model =
            new CompoundPropertyModel<RegistrantCpdItemConfig>(
                new RegistrantCpdItemConfigViewPage.LoadableDetachableRegistrantCpdItemConfigModel(id));

        setDefaultModel(model);
        add(new Link<RegistrantCpdItemConfig>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantCpdItemConfigEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", RegistrantCpdItemConfigListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("totalCpdPointsPermissable"));
        add(new Label("registrantCpdItemConfig", model.bind("name")));
    }

    private final class LoadableDetachableRegistrantCpdItemConfigModel
            extends LoadableDetachableModel<RegistrantCpdItemConfig> {
        private Long id;

        public LoadableDetachableRegistrantCpdItemConfigModel(Long id) {
            this.id = id;
        }

        @Override
        protected RegistrantCpdItemConfig load() {
            return addressTypeService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
