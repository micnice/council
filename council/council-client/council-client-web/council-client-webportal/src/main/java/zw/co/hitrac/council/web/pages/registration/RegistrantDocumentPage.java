
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.registration;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.jackrabbit.value.BinaryImpl;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.lang.Bytes;
import sun.net.www.MimeTable;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.service.SupportDocumentService;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationService;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.institution.InstitutionApplicationViewPage;

import javax.jcr.Node;
import javax.jcr.Repository;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author tdhlakama
 */
public class RegistrantDocumentPage extends TemplatePage {

    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private SupportDocumentService supportDocumentService;
    @SpringBean
    private RegistrationService registrationService;
    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private ExamRegistrationService examRegistrationService;
    @SpringBean
    private RegistrationProcess registrationProcess;

    public RegistrantDocumentPage(final Model<Registrant> registrantModel) {
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());
        add(new Label("fullname", registrantModel.getObject().getFullname()));
        // Create feedback panels
        final FeedbackPanel uploadFeedback = new FeedbackPanel("uploadFeedback");

        // Add uploadFeedback to the page itself
        add(uploadFeedback);
        final RegistrantDocumentPage.FileUploadForm simpleUploadForm = new RegistrantDocumentPage.FileUploadForm("simpleUpload", registrantModel);
        add(simpleUploadForm);

        add(new Link<Registrant>("registrationOptionPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });

    }

    public RegistrantDocumentPage(final RegistrantDisciplinary registrantDisciplinary, final IModel<Registrant> registrantModel) {
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());
        add(new Label("fullname", registrantModel.getObject().getFullname()));
        // Create feedback panels
        final FeedbackPanel uploadFeedback = new FeedbackPanel("uploadFeedback");

        // Add uploadFeedback to the page itself
        add(uploadFeedback);
        final RegistrantDocumentPage.FileUploadForm simpleUploadForm = new RegistrantDocumentPage.FileUploadForm("simpleUpload", Model.of(registrantModel.getObject()), registrantDisciplinary);
        add(simpleUploadForm);

        add(new Link<Registrant>("registrationOptionPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });

    }

    public RegistrantDocumentPage(final Application application, final IModel<Registrant> registrantModel, Requirement requirement) {
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());
        add(new Label("fullname", registrantModel.getObject().getFullname()));
        // Create feedback panels
        final FeedbackPanel uploadFeedback = new FeedbackPanel("uploadFeedback");

        // Add uploadFeedback to the page itself
        add(uploadFeedback);
        final RegistrantDocumentPage.FileUploadForm simpleUploadForm = new RegistrantDocumentPage.FileUploadForm("simpleUpload", Model.of(registrantModel.getObject()), application, requirement);
        add(simpleUploadForm);

        add(new Link<Registrant>("registrationOptionPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });
    }

    public RegistrantDocumentPage(final Application application, final IModel<Registrant> registrantModel) {
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());
        add(new Label("fullname", registrantModel.getObject().getFullname()));
        // Create feedback panels
        final FeedbackPanel uploadFeedback = new FeedbackPanel("uploadFeedback");

        // Add uploadFeedback to the page itself
        add(uploadFeedback);
        final RegistrantDocumentPage.FileUploadForm simpleUploadForm = new RegistrantDocumentPage.FileUploadForm("simpleUpload", Model.of(registrantModel.getObject()), application);
        add(simpleUploadForm);

        add(new Link<Registrant>("registrationOptionPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });
    }

    public RegistrantDocumentPage(final Institution institution, final IModel<Registrant> registrantModel, Requirement requirement) {
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());
        add(new Label("fullname", registrantModel.getObject().getFullname()));
        // Create feedback panels
        final FeedbackPanel uploadFeedback = new FeedbackPanel("uploadFeedback");

        // Add uploadFeedback to the page itself
        add(uploadFeedback);
        final RegistrantDocumentPage.FileUploadForm simpleUploadForm = new RegistrantDocumentPage.FileUploadForm("simpleUpload", Model.of(registrantModel.getObject()), institution, requirement);
        add(simpleUploadForm);

        add(new Link<Registrant>("registrationOptionPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });

    }

    public RegistrantDocumentPage(final Requirement requirement, final IModel<Registrant> registrantModel) {
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());
        add(new Label("fullname", registrantModel.getObject().getFullname()));
        // Create feedback panels
        final FeedbackPanel uploadFeedback = new FeedbackPanel("uploadFeedback");

        // Add uploadFeedback to the page itself
        add(uploadFeedback);
        final RegistrantDocumentPage.FileUploadForm simpleUploadForm = new RegistrantDocumentPage.FileUploadForm("simpleUpload", Model.of(registrantModel.getObject()), requirement);
        add(simpleUploadForm);

        add(new Link<Registrant>("registrationOptionPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });

    }

    public RegistrantDocumentPage(final ExamRegistration examRegistration, final IModel<Registrant> registrantModel, Requirement requirement) {
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());
        add(new Label("fullname", registrantModel.getObject().getFullname()));
        // Create feedback panels
        final FeedbackPanel uploadFeedback = new FeedbackPanel("uploadFeedback");

        // Add uploadFeedback to the page itself
        add(uploadFeedback);
        final RegistrantDocumentPage.FileUploadForm simpleUploadForm = new RegistrantDocumentPage.FileUploadForm("simpleUpload", Model.of(registrantModel.getObject()), examRegistration, requirement);
        add(simpleUploadForm);

        add(new Link<Registrant>("registrationOptionPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });

    }

    public RegistrantDocumentPage(final RegistrantQualification registrantQualification, final IModel<Registrant> registrantModel) {
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());
        add(new Label("fullname", registrantModel.getObject().getFullname()));
        // Create feedback panels
        final FeedbackPanel uploadFeedback = new FeedbackPanel("uploadFeedback");

        // Add uploadFeedback to the page itself
        add(uploadFeedback);
        final RegistrantDocumentPage.FileUploadForm simpleUploadForm = new RegistrantDocumentPage.FileUploadForm("simpleUpload", Model.of(registrantModel.getObject()), registrantQualification);
        add(simpleUploadForm);

        add(new Link<Registrant>("registrationOptionPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });

    }

    private class FileUploadForm extends Form<Registrant> {

        FileUploadField fileUploadField;
        SupportDocument supportDocument;

        /**
         * Construct.
         *
         * @param name Component name
         */
        public FileUploadForm(String name, final Model<Registrant> registrantModel) {
            super(name);

            // set this form to multipart mode (allways needed for uploads!)
            setMultiPart(true);

            // Add one file input field
            add(fileUploadField = new FileUploadField("fileInput"));
            supportDocument = new SupportDocument();
            setDefaultModel(registrantModel);

            // Set maximum size to 100K for demo purposes
            setMaxSize(Bytes.kilobytes(10000));
        }

        public FileUploadForm(String name, final Model<Registrant> registrantModel, Application application, Requirement requirement) {
            super(name);

            // set this form to multipart mode (allways needed for uploads!)
            setMultiPart(true);

            // Add one file input field
            add(fileUploadField = new FileUploadField("fileInput"));
            setDefaultModel(registrantModel);
            supportDocument = new SupportDocument();
            supportDocument.setApplication(application);
            supportDocument.setRequirement(requirement);

            // Set maximum size to 100K for demo purposes
            setMaxSize(Bytes.kilobytes(10000));
        }

        public FileUploadForm(String name, final Model<Registrant> registrantModel, Application application) {
            super(name);

            // set this form to multipart mode (allways needed for uploads!)
            setMultiPart(true);

            // Add one file input field
            add(fileUploadField = new FileUploadField("fileInput"));
            setDefaultModel(registrantModel);
            supportDocument = new SupportDocument();
            supportDocument.setApplication(application);
            // Set maximum size to 100K for demo purposes
            setMaxSize(Bytes.kilobytes(10000));
        }

        public FileUploadForm(String name, final Model<Registrant> registrantModel, final ExamRegistration examRegistration, Requirement requirement) {
            super(name);

            // set this form to multipart mode (allways needed for uploads!)
            setMultiPart(true);

            // Add one file input field
            add(fileUploadField = new FileUploadField("fileInput"));
            setDefaultModel(registrantModel);
            supportDocument = new SupportDocument();
            supportDocument.setExamRegistration(examRegistration);
            supportDocument.setRequirement(requirement);
            // Set maximum size to 100K for demo purposes
            setMaxSize(Bytes.kilobytes(10000));
        }

        public FileUploadForm(String name, final Model<Registrant> registrantModel, Institution institution, Requirement requirement) {
            super(name);

            // set this form to multipart mode (allways needed for uploads!)
            setMultiPart(true);

            // Add one file input field
            add(fileUploadField = new FileUploadField("fileInput"));
            setDefaultModel(registrantModel);
            supportDocument = new SupportDocument();
            supportDocument.setInstitution(institution);
            supportDocument.setRequirement(requirement);
            // Set maximum size to 100K for demo purposes
            setMaxSize(Bytes.kilobytes(10000));
        }

        public FileUploadForm(String name, final Model<Registrant> registrantModel, Requirement requirement) {
            super(name);

            // set this form to multipart mode (allways needed for uploads!)
            setMultiPart(true);

            // Add one file input field
            add(fileUploadField = new FileUploadField("fileInput"));
            setDefaultModel(registrantModel);
            supportDocument = new SupportDocument();
            supportDocument.setRequirement(requirement);
            // Set maximum size to 100K for demo purposes
            setMaxSize(Bytes.kilobytes(10000));
        }

        public FileUploadForm(String name, final Model<Registrant> registrantModel, RegistrantDisciplinary registrantDisciplinary) {
            super(name);

            // set this form to multipart mode (allways needed for uploads!)
            setMultiPart(true);

            // Add one file input field
            add(fileUploadField = new FileUploadField("fileInput"));
            setDefaultModel(registrantModel);
            supportDocument = new SupportDocument();
            supportDocument.setRegistrantDisciplinary(registrantDisciplinary);
            // Set maximum size to 100K for demo purposes
            setMaxSize(Bytes.kilobytes(10000));
        }

        public FileUploadForm(String name, final Model<Registrant> registrantModel, RegistrantQualification registrantQualification) {
            super(name);

            // set this form to multipart mode (allways needed for uploads!)
            setMultiPart(true);

            // Add one file input field
            add(fileUploadField = new FileUploadField("fileInput"));
            setDefaultModel(registrantModel);
            supportDocument = new SupportDocument();
            supportDocument.setRegistrantQualification(registrantQualification);
            // Set maximum size to 100K for demo purposes
            setMaxSize(Bytes.kilobytes(10000));
        }

        /**
         * @see org.apache.wicket.markup.html.form.Form#onSubmit()
         */
        @Override
        protected void onSubmit() {
            final List<FileUpload> uploads = fileUploadField.getFileUploads();

            if (uploads != null) {
                for (FileUpload upload : uploads) {

                    // UploadPage.this.info("saved file: " + upload.getClientFileName());
                    try {
                        System.out.println("================Supporting Documents====================");

                        Repository repository = JcrUtils.getRepository(generalParametersService.get().getContent_Url());

                        Session session = repository.login(new SimpleCredentials("admin", "admin".toCharArray()));
                        String user = session.getUserID();
                        String name = repository.getDescriptor(Repository.REP_NAME_DESC);

                        System.out.println("Logged in as " + user + " to a " + name + " repository.");

                        try {
                            Node root = session.getRootNode();
                            String fileName = upload.getClientFileName();
                            MimeTable mt = MimeTable.getDefaultTable();
                            String mimeType = mt.getContentTypeFor(fileName);

                            if (mimeType == null) {
                                mimeType = "application/octet-stream";
                            }

                            String id = String.valueOf(this.getModelObject().getId());

                            Node contentNode;

                            if (root.hasNode("content")) {
                                contentNode = root.getNode("content");
                            } else {
                                contentNode = root.addNode("content");
                            }

                            Node councilNode;

                            if (contentNode.hasNode("council")) {
                                councilNode = contentNode.getNode("council");
                            } else {
                                councilNode = contentNode.addNode("council");
                            }

                            Node registrantsNode;

                            if (councilNode.hasNode("registrants")) {
                                registrantsNode = councilNode.getNode("registrants");
                            } else {
                                registrantsNode = councilNode.addNode("registrants");
                            }

                            Node idNode;

                            if (registrantsNode.hasNode(id)) {
                                idNode = registrantsNode.getNode(id);
                            } else {
                                idNode = registrantsNode.addNode(id);
                            }

                            Node supportingDocumentsNode;

                            if (idNode.hasNode("supportingDocuments")) {
                                supportingDocumentsNode = idNode.getNode("supportingDocuments");
                            } else {
                                supportingDocumentsNode = idNode.addNode("supportingDocuments");
                            }

                            Node documentsNode = supportingDocumentsNode.addNode(upload.getClientFileName(), "nt:file");

                            Node profileContentNode = documentsNode.addNode("jcr:content", "nt:resource");

                            BinaryImpl binary = new BinaryImpl(upload.getInputStream());
                            profileContentNode.setProperty("jcr:data", binary);

                            profileContentNode.setProperty("jcr:lastModified", Calendar.getInstance());
                            profileContentNode.setProperty("jcr:mimeType", mimeType);


                            supportDocument.setName(upload.getClientFileName());
                            supportDocument.setPathName(profileContentNode.getPath());
                            supportDocument.setDocumentType(mimeType);
                            supportDocument.setDateCreated(new Date());
                            supportDocument.setRegistrant(getModelObject());
                            supportDocument.setCreatedBy(CouncilSession.get().getUser());
                            supportDocument.setSubmitted(Boolean.TRUE);
                            supportDocument = supportDocumentService.save(supportDocument);

                            session.save();

                            if (supportDocument.getInstitution() != null) {
                                setResponsePage(new RequirementConfirmationPage(supportDocument.getInstitution()));
                            } else if (supportDocument.getExamRegistration() != null) {
                                setResponsePage(new RequirementConfirmationPage(supportDocument.getExamRegistration()));
                            } else if (supportDocument.getApplication() != null) {
                                if (supportDocument.getApplication().getApplicationPurpose().equals(ApplicationPurpose.INSTITUTION_REGISTRATION)) {
                                    setResponsePage(new InstitutionApplicationViewPage(supportDocument.getApplication().getInstitution().getId()));
                                } else {
                                    setResponsePage(new RequirementConfirmationPage(supportDocument.getApplication()));
                                }
                            } else if (supportDocument.getRegistrantDisciplinary() != null) {
                                setResponsePage(new RegistrantDisciplinaryViewPage(supportDocument.getRegistrantDisciplinary().getId()));
                            } else if (supportDocument.getRegistrantQualification() != null) {
                                setResponsePage(new RegistrantQualificationViewPage(supportDocument.getRegistrantQualification().getId()));
                            } else if (supportDocument.getRequirement() != null) {
                                Registrant r = registrantService.get(getModel().getObject().getId());
                                Registration registration = registrationProcess.activeRegisterNotStudentRegister(r);
                                if (registration != null) {
                                    setResponsePage(new RequirementConfirmationPage(registration.getId()));
                                } else {
                                    setResponsePage(new RegistrantViewPage(this.getModelObject().getId()));
                                }
                            } else {
                                setResponsePage(new RegistrantViewPage(this.getModelObject().getId()));
                            }

                            //Always call this to release resources
                            binary.dispose();

                        } finally {
                            session.logout();
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace(System.out);
                    }
                }
            }
        }
    }
}
