package zw.co.hitrac.council.business.service;

import java.util.List;
import zw.co.hitrac.council.business.domain.Requirement;

/**
 *
 * @author Michael Matiashe
 */
public interface RequirementService extends IGenericService<Requirement> {
    public List<Requirement> findAll(Boolean examRequirement);
}
