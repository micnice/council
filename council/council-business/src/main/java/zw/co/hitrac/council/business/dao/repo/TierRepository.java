package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.Tier;

/**
 *
 * @author Edward Zengeni
 */
public interface TierRepository extends CrudRepository<Tier, Long> {
    public List<Tier> findAll();
}
