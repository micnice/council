package zw.co.hitrac.council.business.dao;

import zw.co.hitrac.council.business.domain.Tier;

/**
 *
 * @author Edward Zengeni
 */
public interface TierDAO extends IGenericDAO<Tier> {
    
}
