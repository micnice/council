package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 *
 * @author charlesc
 */
@Entity
@Table(name = "coursecertificatenumber")
@Audited
@XmlRootElement
public class CourseCertificateNumber extends BaseEntity
        implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long certificateNumber = 0L;
    private Course course;
    private Register register;
    private String coursePrefix;

    public String getCoursePrefix() {

        return coursePrefix;
    }

    public void setCoursePrefix(String coursePrefix) {

        this.coursePrefix = coursePrefix;
    }

    public Long getCertificateNumber() {
        return certificateNumber;
    }

    public void setCertificateNumber(Long certificateNumber) {
        this.certificateNumber = certificateNumber;
    }

    @ManyToOne
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @ManyToOne
    public Register getRegister() {
        return register;
    }

    public void setRegister(Register register) {
        this.register = register;
    }

    public void addOne() {
        certificateNumber += 1;
    }

}
