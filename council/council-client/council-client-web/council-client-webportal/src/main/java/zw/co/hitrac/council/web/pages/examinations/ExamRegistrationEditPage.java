package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.extensions.yui.calendar.DatePicker;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.ModulePaperService;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationService;
import zw.co.hitrac.council.business.service.examinations.ExamSettingService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.ExamSettingListModel;
import zw.co.hitrac.council.web.models.TrainingInstitutionstModel;
import zw.co.hitrac.council.web.pages.registration.RegistrantViewPage;
import zw.co.hitrac.council.web.utility.CustomDateTextField;

/**
 *
 * @author Michael Matiashe
 */
public class ExamRegistrationEditPage extends IExaminationsPage {

    @SpringBean
    private ExamRegistrationService examRegistrationService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    ModulePaperService modulePaperService;
    @SpringBean
    private ExamSettingService examSettingService;

    public ExamRegistrationEditPage(Long id) {
        CompoundPropertyModel<ExamRegistration> model =
                new CompoundPropertyModel<ExamRegistration>(
                new ExamRegistrationEditPage.LoadableDetachableExamRegistrationModel(id));

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);

        final Long registrantId = model.getObject().getRegistration().getRegistrant().getId();

        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantId);
        setDefaultModel(model);

        Form<ExamRegistration> form = new Form<ExamRegistration>("form", (IModel<ExamRegistration>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                examRegistrationService.save(getModelObject());
                setResponsePage(new RegistrantExaminationsPage(getModelObject().getRegistration().getId()));
            }
        };

        form.add(new TextField<String>("candidateNumber").setRequired(true).add(new ErrorBehavior()));
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantId));
            }
        });
        form.add(new Label("registration.course"));
        form.add(new CheckBox("suppressed"));
        form.add(new CheckBox("closed"));
        form.add(new CheckBox("reversedResult"));
        form.add(new CustomDateTextField("registration.registrationDate").setRequired(true).add(new ErrorBehavior()).add(new DatePicker()));
        form.add(new DropDownChoice("institution", new TrainingInstitutionstModel(institutionService)));
        form.add(new DropDownChoice("registration.institution", new TrainingInstitutionstModel(institutionService)));
        form.add(new DropDownChoice("examSetting", new ExamSettingListModel(examSettingService)));        
        form.add(new TextField<String>("comment").setRequired(true).add(new ErrorBehavior()));
        add(form);
        add(new Label("registration.registrant.fullname"));
        add(new FeedbackPanel("feedback"));
    }

    private final class LoadableDetachableExamRegistrationModel extends LoadableDetachableModel<ExamRegistration> {

        private Long id;

        public LoadableDetachableExamRegistrationModel(Long id) {
            this.id = id;
        }

        @Override
        protected ExamRegistration load() {
            return examRegistrationService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
