package zw.co.hitrac.council.web.pages.accounts.payments;

import java.util.List;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.PaymentDetails;
import zw.co.hitrac.council.business.service.accounts.PaymentDetailsService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.accounts.IAccountingPage;

/**
 *
 * @author Michael Matiashe
 */
public class PaymentsListPage extends IAccountingPage {

    @SpringBean
    private PaymentDetailsService paymentDetailsService;

    public PaymentsListPage() {

        menuPanelManager.getRegistrantionPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantionPanel().setRegistrantEditPageCurrent(true);

        IModel<List<PaymentDetails>> model = new LoadableDetachableModel<List<PaymentDetails>>() {
            @Override
            protected List<PaymentDetails> load() {
                return paymentDetailsService.findAll();
            }
        };

        PropertyListView<PaymentDetails> eachItem = new PropertyListView<PaymentDetails>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<PaymentDetails> item) {
                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
            }
        };

        add(eachItem);
    }
}
