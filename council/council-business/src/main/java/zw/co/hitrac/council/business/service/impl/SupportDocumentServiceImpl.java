/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.SupportDocumentDAO;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.business.service.SupportDocumentService;

import java.util.List;

/**
 * @author kelvin
 */
@Service
@Transactional
public class SupportDocumentServiceImpl implements SupportDocumentService {

    @Autowired
    private SupportDocumentDAO supportDocumentDAO;

    @Transactional
    public SupportDocument save(SupportDocument supportDocument) {

        return supportDocumentDAO.save(supportDocument);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<SupportDocument> findAll() {

        return supportDocumentDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public SupportDocument get(Long id) {

        return supportDocumentDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<SupportDocument> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setSupportDocumentDAO(SupportDocumentDAO supportDocumentDAO) {

        this.supportDocumentDAO = supportDocumentDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<SupportDocument> getSupportDocuments(Registrant registrant, Application application, Requirement requirement, RegistrantDisciplinary registrantDisciplinary, RegistrantQualification registrantQualification, ExamRegistration examRegistration, Institution institution) {

        return supportDocumentDAO.getSupportDocuments(registrant, application, requirement, registrantDisciplinary, registrantQualification, examRegistration, institution);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public SupportDocument getSupportDocument(Registrant registrant, Requirement requirement) {

        return supportDocumentDAO.getSupportDocument(registrant, requirement);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public SupportDocument getSupportDocument(Institution institution, Requirement requirement) {

        return supportDocumentDAO.getSupportDocument(institution, requirement);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public SupportDocument getSupportDocument(ExamRegistration examRegistration, Requirement requirement) {

        return supportDocumentDAO.getSupportDocument(examRegistration, requirement);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public SupportDocument getSupportDocument(Application application, Requirement requirement) {

        return supportDocumentDAO.getSupportDocument(application, requirement);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public SupportDocument getClearanceDate(Registrant registrant, Course course) {

        return supportDocumentDAO.getClearanceDate(registrant, course);
    }

}