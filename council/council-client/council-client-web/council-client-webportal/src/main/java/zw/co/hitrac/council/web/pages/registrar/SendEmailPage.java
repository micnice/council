package zw.co.hitrac.council.web.pages.registrar;

import java.util.ArrayList;
import java.util.List;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.EmailMessage;
import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.business.domain.RegistrantContact;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.EmailMessageService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegisterService;
import zw.co.hitrac.council.business.service.RegistrantActivityService;
import zw.co.hitrac.council.web.models.CourseListModel;
import zw.co.hitrac.council.web.models.RegisterListModel;
import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author Takunda Dhlakama
 */
public class SendEmailPage extends TemplatePage {

    @SpringBean
    private RegistrantActivityService registrantActivityService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    private Course course;
    private Register register;
    @SpringBean
    private RegisterService registerService;
    @SpringBean
    private CourseService courseService;
    @SpringBean
    private EmailMessageService emailMessageService;

    public SendEmailPage(final EmailMessage emailMessage) {
        PropertyModel<Course> courseModel = new PropertyModel<Course>(this, "course");
        PropertyModel<Register> registerModel = new PropertyModel<Register>(this, "register");
        Form<?> form = new Form("form");
        form.add(new DropDownChoice("course", courseModel, new CourseListModel(courseService)));
        form.add(new DropDownChoice("register", registerModel, new RegisterListModel(registerService)));
        add(form);

        form.add(new Button("send") {
            @Override
            public void onSubmit() {
                try {
                    String bodyMessage = String.format(emailMessage.getParagraphOne() + " " + emailMessage.getParagraphTwo() + "\n" + " " + generalParametersService.get().getEmailSignature());
                    List<RegistrantContact> contacts = registrantActivityService.getEmailAddressesPerQualificationMappedCourse(course, register, generalParametersService.get().getEmailContactType());
                    List<RegistrantData> list = new ArrayList<RegistrantData>();
                    //TODO: Set Directly to Email Address not Contact Detail
                    for (RegistrantContact data : contacts) {
                        RegistrantData rd = new RegistrantData();
                        rd.setEmailAddress(data.getContactDetail());
                        list.add(rd);
                    }
                    emailMessageService.send(list, emailMessage.getSubject(), bodyMessage);
                    info("Emails Sent Successfully");
                } catch (Exception e) {
                    error("Emails not sent -  Try Again");
                }
            }
        });

        add(new FeedbackPanel("feedback"));
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
