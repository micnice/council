/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.accounts.payments;

import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.PaymentDetails;
import zw.co.hitrac.council.business.domain.accounts.ReceiptHeader;
import zw.co.hitrac.council.business.domain.accounts.ReceiptItem;
import zw.co.hitrac.council.business.process.ReversePaymentProcess;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.service.accounts.*;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.reports.Receipt;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.utility.CustomDateLabel;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.web.utility.DateTimeUtils;

import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;

/**
 * @author tdhlakama
 */
public class TransactionDetailHistoryPanel extends Panel {

    @SpringBean
    private PaymentDetailsService paymentDetailsService;
    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    AccountService accountService;
    @SpringBean
    CustomerService customerAccountService;
    @SpringBean
    RegistrationService registrationService;
    @SpringBean
    ReceiptHeaderService receiptHeaderService;
    @SpringBean
    ReversePaymentProcess reversePaymentProcess;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    private HrisComparator hrisComparator = new HrisComparator();
    @SpringBean
    private AccountsParametersService accountsParametersService;
    private Institution institution = null;
    private Registrant registrant = null;
    private Account account = null;

    public TransactionDetailHistoryPanel(String panelId, final Long id) {
        super(panelId);

        final CompoundPropertyModel<PaymentDetails> model = new CompoundPropertyModel<PaymentDetails>(new TransactionDetailHistoryPanel.LoadableDetachablePaymentDetailsModel(id));
        setDefaultModel(model);

        ReceiptHeader receiptHeader = receiptHeaderService.get(model.getObject().getReceiptHeader().getId());

        add(new Label("councilName", generalParametersService.get().getCouncilName()));
        add(new Label("receiptHeader.id"));
        add(new CustomDateLabel("receiptHeader.date", DateTimeUtils.getReceiptConverter()));
        add(new Label("receiptHeader.carryForward", receiptHeader.getCalculatedCarryForward()));
        add(new Label("receiptHeader.transactionType"));
        add(new Label("receiptHeader.generatedReceiptNumber"));
        add(new Label("receiptHeader.status"));
        add(new Label("receiptHeader.createdBy.firstName"));
        add(new Label("receiptHeader.createdBy.surname"));
        add(new Label("total", receiptHeader.getCalculatedTotalAmountPaid()));
        add(new Label("paymentMethod"));

        add(new Label("showPaymentPeriod", new LoadableDetachableModel<String>() {
            @Override
            protected String load() {
                final PaymentDetails paymentDetails = model.getObject();
                return paymentDetails.getCouncilDuration() != null ? "Payment Period " + paymentDetails.getCouncilDuration() : " ";
            }
        }));

        add(new FeedbackPanel("feedback"));

        registrant = registrantService.getRegistrant(model.getObject().getCustomer());
        institution = institutionService.getInstitution(model.getObject().getCustomer());

        if (registrant != null) {
            account = registrant.getCustomerAccount().getAccount();
        } else {
            account = institution.getCustomerAccount().getAccount();
        }

        add(new Label("fullname", new LoadableDetachableModel<String>() {
            @Override
            protected String load() {

                if (institution != null) {
                    return institution.getName();
                }
                if (registrant != null) {
                    return registrant.getName();
                }
                return "";
            }

        }));

        add(new Label("registrationNumber", new LoadableDetachableModel<String>() {
            @Override
            protected String load() {

                if (institution != null && institution.getRegistrant() != null) {
                    return institution.getRegistrant().getRegistrationNumber();
                }

                if (registrant != null && registrant.getRegistrationNumber() != null) {
                    return registrant.getRegistrationNumber();
                }

                if (registrant != null) {
                    return registrant.getIdNumber();
                }

                return "";
            }

        }));

        add(new Label("comment", new LoadableDetachableModel<String>() {
            @Override
            protected String load() {

                if (receiptHeader.getCalculatedCarryForward().compareTo(new BigDecimal("0")) <= 0) {
                    return "";
                } else {
                    return "CARRY FORWARD ($) " + receiptHeader.getCalculatedCarryForward().toString();
                }

            }

        }));


        add(new Label("comment2", new LoadableDetachableModel<String>() {
            @Override
            protected String load() {

                if ((account.getBalance().compareTo(new BigDecimal("0")) != 0)) {
                    return "CARRY FORWARD BALANCE($) " + account.getBalance().multiply(new BigDecimal("-1")).toString();
                } else {
                    return "";
                }

            }

        }));

        add(new PropertyListView<ReceiptItem>("receiptItems", new ArrayList<>(model.getObject().getReceiptHeader().getReceiptItems())) {
            @Override
            protected void populateItem(ListItem<ReceiptItem> item) {
                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                item.add(new Label("debtComponent.product.code"));
                item.add(new Label("debtComponent.transactionComponent.account.name"));
                item.add(new Label("amount"));

                Link<ReceiptItem> viewLink = new Link("changePaymentPeriod", item.getModel()) {
                    @Override
                    public void onClick() {
                        ReceiptItem receiptItem = item.getModelObject();
                        setResponsePage(new ChangeReceiptItemPaymentPeriodPage(receiptItem.getId()));
                    }

                    @Override
                    public boolean isEnabled() {
                        Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.FINANCIAL_ADMIN_EXECUTIVE, Role.ACCOUNTS_OFFICER)));
                        return enabled;
                    }

                };

                viewLink.add(new Label("paymentPeriod"));
                item.add(viewLink);
            }
        });

        final Form<Void> form = new Form<Void>("form");
        this.add(form);
        form.add(new Button("print") {
            @Override
            public void onSubmit() {
                try {
                    print(model.getObject());
                } catch (CouncilException ex) {
                    error("Error cannot reprint Receipt, Contact Administrator" + ex.getMessage());
                    ex.printStackTrace();
                }
            }

            @Override
            protected void onConfigure() {
                if (!accountsParametersService.get().getPrintJavaScriptReceipt()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(Boolean.FALSE);
                }
            }
        });

        form.add(new Button("receiptDivPrint") { // idem as previous comment

            private static final long serialVersionUID = 1L;

            @Override
            protected String getOnClickScript() {
                return "printContent('receipt_div')";
            }

            @Override
            protected void onConfigure() {
                if (accountsParametersService.get().getPrintJavaScriptReceipt()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(Boolean.FALSE);
                }
            }

        });

    }

    private void print(PaymentDetails paymentDetails) {
        try {

            Receipt receipt = new Receipt();

            Map parameters = new HashMap();

            if (institution != null) {
                parameters.put("fullname", institution.getName());
                if (institution.getRegistrant() != null) {
                    if (institution.getRegistrant().getRegistrationNumber() != null) {
                        parameters.put("registrationNumber", institution.getRegistrant().getRegistrationNumber());
                    } else {
                        parameters.put("registrationNumber", "");
                    }
                }
            }

            if (registrant != null) {
                parameters.put("fullname", registrant.getFullname());
                parameters.put("firstname", registrant.getFirstname());
                parameters.put("surname", registrant.getLastname());
                if (registrant.getRegistrationNumber() != null) {
                    parameters.put("registrationNumber", registrant.getRegistrationNumber());
                } else {
                    parameters.put("registrationNumber", registrant.getIdNumber());
                }
            }

            ReceiptHeader receiptHeader = receiptHeaderService.get(paymentDetails.getReceiptHeader().getId());
            if (receiptHeader.getCarryForward().compareTo(new BigDecimal("0")) <= 0) {
                parameters.put("comment", "");
            } else {
                parameters.put("comment", "CARRY FORWARD ($) " + receiptHeader.getCarryForward().toString());
            }

            if (receiptHeader.getPaymentDetails().getPaymentMethod().equals(accountsParametersService.get().getDefaultPaymentMethod())) {
                if ((account.getBalance().compareTo(new BigDecimal("0")) != 0)) {
                    parameters.put("comment2", "CARRY FORWARD BALANCE($) " + account.getBalance().multiply(new BigDecimal("-1")).toString());
                } else {
                    parameters.put("comment2", "");
                }
            } else {
                parameters.put("comment2", "");
            }

            if (receiptHeader.getGeneratedReceiptNumber() != null) {
                parameters.put("referenceNumber", receiptHeader.getGeneratedReceiptNumber());
            }
            parameters.put("firstName", receiptHeader.getCreatedBy().getFirstName());
            parameters.put("lastName", receiptHeader.getCreatedBy().getSurname());

            ContentType contentType = null;
            if (!accountsParametersService.get().getPrintPDFReceipt()) {
                contentType = ContentType.DOC;
                ByteArrayResource resource = ReportResourceUtils.getReportResource(receipt, contentType, hrisComparator.receiptItems(new ArrayList<ReceiptItem>(receiptHeader.getReceiptItems())), parameters);
                IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(), RequestCycle.get().getResponse(), null);
                resource.respond(a);
            } else {
                contentType = ContentType.PDF;
                ByteArrayResource resource = ReportResourceUtils.getReportResource(receipt, contentType, hrisComparator.receiptItems(new ArrayList<>(receiptHeader.getReceiptItems())), parameters);
                IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(), RequestCycle.get().getResponse(), null);
                resource.respond(a);
                RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
            }

        } catch (JRException ex) {
            ex.printStackTrace();
            throw new CouncilException(ex.getMessage());
        }
    }

    private final class LoadableDetachablePaymentDetailsModel extends LoadableDetachableModel<PaymentDetails> {

        private Long id;

        public LoadableDetachablePaymentDetailsModel(Long id) {
            this.id = id;
        }

        @Override
        protected PaymentDetails load() {
            return paymentDetailsService.get(id);
        }
    }
}
