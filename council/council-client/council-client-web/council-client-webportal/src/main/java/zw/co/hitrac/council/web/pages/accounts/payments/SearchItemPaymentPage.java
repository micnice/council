/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.accounts.payments;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.accounts.PaymentDetails;
import zw.co.hitrac.council.business.domain.accounts.PaymentMethod;
import zw.co.hitrac.council.business.domain.accounts.PaymentType;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.domain.accounts.ReceiptItem;
import zw.co.hitrac.council.business.domain.accounts.TransactionType;
import zw.co.hitrac.council.business.domain.reports.ProductItem;
import zw.co.hitrac.council.business.domain.reports.ProductSale;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.UserService;
import zw.co.hitrac.council.business.service.accounts.AccountsParametersService;
import zw.co.hitrac.council.business.service.accounts.CustomerService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.business.service.accounts.PaymentDetailsService;
import zw.co.hitrac.council.business.service.accounts.PaymentMethodService;
import zw.co.hitrac.council.business.service.accounts.PaymentTypeService;
import zw.co.hitrac.council.business.service.accounts.ProductService;
import zw.co.hitrac.council.business.service.accounts.ReceiptItemService;
import zw.co.hitrac.council.business.service.accounts.TransactionTypeService;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.reports.ProductItemReport;
import zw.co.hitrac.council.reports.ProductSaleNoneBankReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.ProductListModel;
import zw.co.hitrac.council.web.models.TransactionTypeListModel;
import zw.co.hitrac.council.web.pages.accounts.IAccountingPage;
import zw.co.hitrac.council.web.utility.DatePickerUtil;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 * @author tdhlakama
 */
public class SearchItemPaymentPage extends IAccountingPage {

    private Date startDate, endDate;
    private User user;
    private PaymentMethod paymentMethod;
    private PaymentType paymentType;
    private Product product;
    private TransactionType transactionType;
    private TypeOfService typeOfService;
    @SpringBean
    private UserService userService;
    @SpringBean
    private ReceiptItemService receiptItemService;
    @SpringBean
    private TransactionTypeService transactionTypeService;
    @SpringBean
    private PaymentMethodService paymentMethodService;
    @SpringBean
    private PaymentTypeService paymentTypeService;
    @SpringBean
    private ProductService productService;
    @SpringBean
    DebtComponentService debtComponentService;
    @SpringBean
    private CustomerService customerService;
    @SpringBean
    private RegistrationProcess registrationProcess;
    @SpringBean
    PaymentDetailsService paymentDetailsService;
    @SpringBean
    private AccountsParametersService accountsParametersService;


    public SearchItemPaymentPage() {
        PropertyModel<Date> startDateModel = new PropertyModel<Date>(this, "startDate");
        PropertyModel<Date> endDateModel = new PropertyModel<Date>(this, "endDate");
        PropertyModel<String> userModel = new PropertyModel<String>(this, "user");
        PropertyModel<String> productModel = new PropertyModel<String>(this, "product");
        PropertyModel<String> typeOfServiceModel = new PropertyModel<>(this, "typeOfService");
        PropertyModel<String> paymentMethodModel = new PropertyModel<String>(this, "paymentMethod");
        PropertyModel<TransactionType> transactionTypeModel = new PropertyModel<TransactionType>(this, "transactionType");

        Form<?> form = new Form("form");
        form.add(new TextField<Date>("startDate", startDateModel).setRequired(true).add(new ErrorBehavior()).add(DatePickerUtil.getDatePicker()));
        form.add(new TextField<Date>("endDate", endDateModel).setRequired(true).add(new ErrorBehavior()).add(DatePickerUtil.getDatePicker()));
        form.add(new DropDownChoice("user", userModel, userService.findAll()));
        form.add(new DropDownChoice("typeOfService", typeOfServiceModel, Arrays.asList(TypeOfService.values())).setNullValid(true));
        form.add(new DropDownChoice("transactionType", transactionTypeModel, new TransactionTypeListModel(transactionTypeService, Book.CB)));
        form.add(new DropDownChoice("product", productModel, new ProductListModel(productService)).setRequired(false).add(new ErrorBehavior()));
        form.add(new DropDownChoice("paymentMethod", paymentMethodModel, paymentMethodService.findAll()));

        add(form);

        add(new FeedbackPanel("feedback"));
        form.add(new Button("print") {
            @Override
            public void onSubmit() {
                try {
                    Set<ProductItem> productSales = new HashSet<ProductItem>();

                    if (startDate != null && endDate == null) {
                        endDate = new Date();
                    }
                    Set<TransactionType> bankAccounts = new HashSet<TransactionType>();
                    bankAccounts.addAll(transactionTypeService.findBankAccounts());
                    TransactionType manualTransactionType = accountsParametersService.get().getManualTransactionType();
                    if (manualTransactionType != null) {
                        bankAccounts.remove(manualTransactionType);
                    }

                    ProductItem productItem = new ProductItem();
                    productItem.setProduct(product);
                    productItem.setEndDate(endDate);
                    productItem.setStartDate(startDate);
                    BigDecimal totalPoints = new BigDecimal(0);
                    if (transactionType != null) {
                        totalPoints = totalPoints.add(receiptItemService.getCalcualtedReceiptTotal(product, paymentMethod, paymentType, startDate, endDate, transactionType, user));
                    } else {
                        for (TransactionType tt : bankAccounts) {
                            totalPoints = totalPoints.add(receiptItemService.getCalcualtedReceiptTotal(product, paymentMethod, paymentType, startDate, endDate, tt, user));
                        }
                    }
                    productItem.setAmountPaid(totalPoints);
                    productSales.add(productItem);

                    ProductItemReport productItemReport = new ProductItemReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    String commnet = "";
                    if (startDate != null && endDate != null) {
                        commnet = "Sales Done form From - " + DateUtil.getDate(startDate) + " to " + DateUtil.getDate(endDate);
                    }
                    parameters.put("comment", commnet);

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(productItemReport,
                            contentType, productSales, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("printBankCashReport") {
            @Override
            public void onSubmit() {
                List<ReceiptItem> receiptItems = new ArrayList<ReceiptItem>();
                List<ProductSale> productSales = new ArrayList<ProductSale>();
                Set<TransactionType> bankAccounts = new HashSet<TransactionType>();
                bankAccounts.addAll(transactionTypeService.findBankAccounts());

                TransactionType manualTransactionType = accountsParametersService.get().getManualTransactionType();
                if (manualTransactionType != null) {
                    bankAccounts.remove(manualTransactionType);
                }
                if (transactionType != null) {
                    receiptItems.addAll(receiptItemService.getReceiptItems(product, paymentMethod, paymentType, startDate, endDate, transactionType, user));
                } else {
                    for (TransactionType tt : bankAccounts) {
                        receiptItems.addAll(receiptItemService.getReceiptItems(product, paymentMethod, paymentType, startDate, endDate, tt, user));
                    }
                }
                for (ReceiptItem r : receiptItems) {
                    ProductSale productSale = new ProductSale();
                    PaymentDetails paymentDetails = paymentDetailsService.get(r.getReceiptHeader().getPaymentDetails().getId());

                    final Registrant registrant = customerService.getRegistrant(r.getDebtComponent().getAccount());
                    if (registrant != null) {
                        productSale.setRegistrant(registrant);
                    } else {
                        Institution institution = customerService.getInstitution(paymentDetails.getCustomer().getAccount());
                        productSale.setInstitution(institution);
                    }

                    productSale.setDebtComponent(r.getDebtComponent());
                    productSale.setRegistrant(registrant);
                    productSale.setProduct(r.getDebtComponent().getProduct());
                    productSale.setAmountPaid(r.getAmount());
                    productSale.setPaymentDetail(paymentDetails);

                    productSales.add(productSale);
                }

                if (!productSales.isEmpty() && typeOfService != null) {
                    productSales = productSales.stream().filter(p -> p.getProduct().getTypeOfService().equals(typeOfService)).collect(Collectors.toList());
                }


                String commnet = "";
                if (startDate != null && endDate != null) {
                    commnet = "From - " + DateUtil.getDate(startDate) + " to " + DateUtil.getDate(endDate);
                }
                printCashReport(productSales, commnet, "");
            }
        });
    }

    private void printCashReport(List<ProductSale> productSales, String comment, String product) {
        try {
            ProductSaleNoneBankReport productSaleNoneBankReport = new ProductSaleNoneBankReport();
            ContentType contentType = ContentType.PDF;
            Map parameters = new HashMap();

            parameters.put("comment", comment);
            parameters.put("item", product);

            ByteArrayResource resource = ReportResourceUtils.getReportResource(productSaleNoneBankReport,
                    contentType, HrisComparator.sortProductSaleByReceiptNumber(productSales), parameters);
            IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                    RequestCycle.get().getResponse(), null);

            resource.respond(a);

            // To make Wicket stop processing form after sending response
            RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
        } catch (JRException ex) {
            ex.printStackTrace();
        }
    }
}
