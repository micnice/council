package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author Matiashe Michael
 */
@Entity
@Table(name = "penaltyparameters")
@Audited
public class PenaltyParameters extends BaseIdEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer penaltyPeriod = 0;
    private Integer percentagePenalty = 0;
    private BigDecimal charge = new BigDecimal("0");
    private Boolean hasReregistrationProduct;
    private Integer reRegistrationPeriod;
    private Boolean penaltyIsIncremental = Boolean.FALSE;

    public Boolean getPenaltyIsIncremental() {
        if (penaltyIsIncremental == null) {
            return Boolean.FALSE;
        } else {
            return penaltyIsIncremental;
        }
    }

    public void setPenaltyIsIncremental(Boolean penaltyIsIncremental) {
        this.penaltyIsIncremental = penaltyIsIncremental;
    }

    public Boolean getHasReregistrationProduct() {
        if (hasReregistrationProduct == null) {
            return Boolean.FALSE;
        } else {
            return hasReregistrationProduct;
        }
    }

    public void setHasReregistrationProduct(Boolean hasReregistrationProduct) {
        this.hasReregistrationProduct = hasReregistrationProduct;
    }

    public Integer getReRegistrationPeriod() {
        if (reRegistrationPeriod == null) {
            return 0;
        } else {
            return reRegistrationPeriod;
        }
    }

    public void setReRegistrationPeriod(Integer reRegistrationPeriod) {
        this.reRegistrationPeriod = reRegistrationPeriod;
    }

    public Integer getPenaltyPeriod() {
        if (penaltyPeriod == null) {
            return 0;
        } else {
            return penaltyPeriod;
        }

    }

    public void setPenaltyPeriod(Integer penaltyPeriod) {
        this.penaltyPeriod = penaltyPeriod;
    }

    public Integer getPercentagePenalty() {
        if (percentagePenalty == null) {
            return 0;
        } else {
            return percentagePenalty;
        }

    }

    public void setPercentagePenalty(Integer percentagePenalty) {
        this.percentagePenalty = percentagePenalty;
    }

    public BigDecimal getCharge() {
        if (charge == null) {
            return new BigDecimal("0");
        } else {
            return charge;
        }

    }

    public void setCharge(BigDecimal charge) {
        this.charge = charge;
    }
}
