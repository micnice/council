package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author Charles Chigoriwa
 */
@Entity
@Table(name = "identificationtype")
@Audited
@XmlRootElement
public class IdentificationType extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    private Boolean expire = Boolean.FALSE;

    public Boolean getExpire() {
        if (expire == null) {
            return Boolean.FALSE;
        }
        return expire;
    }

    public void setExpire(Boolean expire) {
        this.expire = expire;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        return getName().equals(((IdentificationType)obj).getName());
    }
}
