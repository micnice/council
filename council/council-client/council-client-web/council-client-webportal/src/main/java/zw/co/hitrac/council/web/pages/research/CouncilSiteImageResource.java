package zw.co.hitrac.council.web.pages.research;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.commons.io.IOUtils;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.wicket.request.resource.DynamicImageResource;
import zw.co.hitrac.council.business.service.GeneralParametersService;

import javax.jcr.*;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author Constance Mabaso
 */
public class CouncilSiteImageResource extends DynamicImageResource {

    private GeneralParametersService generalParametersService;

    public CouncilSiteImageResource(GeneralParametersService generalParametersService) {
        this.generalParametersService = generalParametersService;
    }

    @Override
    protected byte[] getImageData(Attributes attributes) {
        InputStream inputStream = getInputStream();

        if (inputStream != null) {
            try {
                return IOUtils.toByteArray(inputStream);
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }

        return null;
    }

    private InputStream getInputStream() {
        try {
            String profileImagePath = "content/council/logos/data/images/councilPicture/profileImage";

            Repository repository = JcrUtils.getRepository(generalParametersService.get().getContent_Url());
            Session session = repository.login(new SimpleCredentials("admin", "admin".toCharArray()));

            String user = session.getUserID();
            String name = repository.getDescriptor(Repository.REP_NAME_DESC);

            try {
                Node root = session.getRootNode();
                                if (root.hasNode(profileImagePath + "/jcr:content")) {
                                       Node pictureNode = root.getNode(profileImagePath + "/jcr:content");

                    return pictureNode.getProperty("jcr:data").getBinary().getStream();
                }
            } finally {
                session.logout();
            }

        }catch (RepositoryException r){
            System.err.println(r.getMessage());
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }

        return null;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
