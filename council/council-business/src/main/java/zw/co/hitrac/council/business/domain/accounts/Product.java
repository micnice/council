package zw.co.hitrac.council.business.domain.accounts;

import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.domain.*;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author Takunda Dhlakama
 * @author Charles Chigoriwa
 * @author Michael Matiashe
 */
@Entity
@Table(name = "product")
@Audited
public class Product extends BaseEntity implements Serializable {

    private ProductPrice productPrice;
    private Course course;
    private Register register;
    private String code;
    private Account salesAccount;
    //Practice eg having your own surgery or clinic
    private EmploymentType employmentType;
    //Eg. Registration or Renewal
    private TypeOfService typeOfService;
    private InstitutionCategory institutionCategory;
    private Boolean directlyInvoicable;
    private CourseType courseType;
    private Tier tier;
    private QualificationType qualificationType;
    private ProductGroup productGroup; //Specifically for combining two or more papers
    private Boolean examProduct = Boolean.FALSE;

    public Product() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    public Account getSalesAccount() {
        return salesAccount;
    }

    public void setSalesAccount(Account salesAccount) {
        this.salesAccount = salesAccount;
    }

    @Enumerated(EnumType.STRING)
    public EmploymentType getEmploymentType() {
        return employmentType;
    }

    public void setEmploymentType(EmploymentType employmentType) {
        this.employmentType = employmentType;
    }

    @Enumerated(EnumType.STRING)
    public InstitutionCategory getInstitutionCategory() {
        return institutionCategory;
    }

    public void setInstitutionCategory(InstitutionCategory institutionCategory) {
        this.institutionCategory = institutionCategory;
    }

    @Enumerated(EnumType.STRING)
    public TypeOfService getTypeOfService() {
        return typeOfService;
    }

    public void setTypeOfService(TypeOfService typeOfService) {
        this.typeOfService = typeOfService;
    }

    @ManyToOne
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @OneToOne(mappedBy = "product")
    public ProductPrice getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(ProductPrice productPrice) {
        this.productPrice = productPrice;
    }

    @ManyToOne
    public CourseType getCourseType() {
        return courseType;
    }

    public void setCourseType(CourseType courseType) {
        this.courseType = courseType;
    }

    @ManyToOne
    public Tier getTier() {
        return tier;
    }

    public void setTier(Tier tier) {
        this.tier = tier;
    }

    @ManyToOne
    public Register getRegister() {
        return register;
    }

    public void setRegister(Register register) {
        this.register = register;
    }

    public Boolean isDirectlyInvoicable() {
        if (directlyInvoicable == null) {
            return Boolean.FALSE;
        }
        return directlyInvoicable;
    }

    public void setDirectlyInvoicable(Boolean directlyInvoicable) {
        this.directlyInvoicable = directlyInvoicable;
    }

    public QualificationType getQualificationType() {
        return qualificationType;
    }

    public void setQualificationType(QualificationType qualificationType) {
        this.qualificationType = qualificationType;
    }

    public Boolean getExamProduct() {
        if (examProduct == null) {
            return Boolean.FALSE;
        }
        return examProduct;
    }

    public void setExamProduct(Boolean examProduct) {
        this.examProduct = examProduct;
    }

    @ManyToOne
    public ProductGroup getProductGroup() {
        return productGroup;
    }

    public void setProductGroup(ProductGroup productGroup) {
        this.productGroup = productGroup;
    }
}
