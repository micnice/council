package zw.co.hitrac.council.business.dao.impl;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.*;

import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.IdentificationValidationRuleDAO;
import zw.co.hitrac.council.business.dao.RegistrantDAO;
import zw.co.hitrac.council.business.dao.repo.RegistrantRepository;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.AccountType;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.business.utils.DateUtil;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Takunda Dhlakama
 */
@Repository
public class RegistrantDAOImpl implements RegistrantDAO {

    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private RegistrantRepository registrantRepository;
    @Autowired
    private GeneralParametersService generalParametersService;
    @Autowired
    private IdentificationValidationRuleDAO identificationValidationRuleDAO;

    public List<Registrant> getRegistrantsByCitizenship(Citizenship citizenship) {

        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registrant.class);
        criteria.add(Restrictions.eq("citizenship", citizenship));
        criteria.addOrder(Order.asc("lastname"));
        criteria.addOrder(Order.asc("firstname"));
        criteria.addOrder(Order.asc("middlename"));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<Registrant> getRegistrantsByGender(String gender) {

        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registrant.class);
        criteria.add(Restrictions.eq("gender", gender));
        criteria.addOrder(Order.asc("lastname"));
        criteria.addOrder(Order.asc("firstname"));
        criteria.addOrder(Order.asc("middlename"));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public Registrant save(Registrant registrant) {
        if (registrant.getCustomerAccount() == null || registrant.getCustomerAccount().getId() == null) {
            registrant.setCustomerAccount(new Customer());
            registrant.setUid(UUID.randomUUID().toString());
            registrant.getCustomerAccount().setCode(UUID.randomUUID().toString());
            registrant.getCustomerAccount().setAccount(new Account());
            registrant.getCustomerAccount().getAccount().setBalance(BigDecimal.ZERO);
            registrant.getCustomerAccount().getAccount().setAccountType(AccountType.ACCOUNTS_RECEIVABLE);
            registrant.getCustomerAccount().getAccount().setCode(UUID.randomUUID().toString());
            registrant.getCustomerAccount().setCustomerName(registrant.getFullname());
            registrant.getCustomerAccount().getAccount().setName(registrant.getFullname());
            registrant.getCustomerAccount().getAccount().setPersonalAccount(Boolean.TRUE);
        }
        return registrantRepository.save(registrant);
    }

    public List<Registrant> findAll() {

        return registrantRepository.findAll();
    }

    public Registrant get(Long id) {

        return registrantRepository.findOne(id);
    }

    public List<Registrant> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setRegistrantRepository(RegistrantRepository registrantRepository) {

        this.registrantRepository = registrantRepository;
    }

    @Override
    public List<RegistrantData> getRegistrantsWithFirstRenewalWithoutSecondRenewal(Course course,
            CouncilDuration firstDuration,
            CouncilDuration secondDuration,
            AddressType addressType) {

        Query query;

        if (addressType == null) {

            query = entityManager.createQuery("select new zw.co.hitrac.council.business.domain.reports.RegistrantData(r.registrant.id,r.registrant.firstname,r.registrant.lastname, r.registrant.middlename,r.registrant.birthDate, r.registrant.gender,r.registrant.idNumber,r.registrant.registrationNumber,  concat( a.address1, ' ', a.address2, ' ', case when c is not null then c.name else '' end) ) "
                    + "from RegistrantActivity r inner join r.registrant.registrantQualifications q  "
                    + "inner join r.registrant.registrantAddresses a "
                    + "left join a.city c "
                    + "where q.course = :course and r.registrantActivityType='RENEWAL' "
                    + "and r.duration.councilDuration= :firstDuration and r.registrant not in "
                    + "(select r2.registrant from RegistrantActivity r2 where r2.registrantActivityType='RENEWAL' "
                    + "and r2.duration.councilDuration = :secondDuration and r2.registrant = r.registrant)", RegistrantData.class)
                    .setParameter("course", course)
                    .setParameter("firstDuration", firstDuration)
                    .setParameter("secondDuration", secondDuration);

        } else {

            query = entityManager.createQuery("select new zw.co.hitrac.council.business.domain.reports.RegistrantData("
                    + "r.registrant.id,r.registrant.firstname,r.registrant.lastname, r.registrant.middlename,"
                    + "r.registrant.birthDate, r.registrant.gender,r.registrant.idNumber,r.registrant.registrationNumber, "
                    + "concat(a.address1, ' ', a.address2, ' ', case when c is not null then c.name else '' end)) "
                    + "from RegistrantActivity r inner join r.registrant.registrantQualifications q  "
                    + "inner join r.registrant.registrantAddresses a "
                    + "inner join a.addressType t "
                    + "left join a.city c "
                    + "where q.course = :course and r.registrantActivityType='RENEWAL' "
                    + "and t = :addressType "
                    + "and r.duration.councilDuration= :firstDuration and r.registrant not in "
                    + "(select r2.registrant from RegistrantActivity r2 where r2.registrantActivityType='RENEWAL' "
                    + "and r2.duration.councilDuration = :secondDuration and r2.registrant = r.registrant)", RegistrantData.class)
                    .setParameter("course", course)
                    .setParameter("firstDuration", firstDuration)
                    .setParameter("addressType", addressType)
                    .setParameter("secondDuration", secondDuration);

        }

        return query.getResultList();
    }

    @Override
    public List<RegistrantData> getRegistrantsWithFirstRenewalWithoutSecondRenewalInRegistration(Course course,
                                                                                   CouncilDuration firstDuration,
                                                                                   CouncilDuration secondDuration,
                                                                                   AddressType addressType) {

        Query query;

        if (addressType == null) {

            query = entityManager.createQuery("select new zw.co.hitrac.council.business.domain.reports.RegistrantData(r.registrant.id,r.registrant.firstname,r.registrant.lastname, r.registrant.middlename,r.registrant.birthDate, r.registrant.gender,r.registrant.idNumber,r.registrant.registrationNumber,  concat( a.address1, ' ', a.address2, ' ', case when c is not null then c.name else '' end) ) "
                    + "from RegistrantActivity r inner join r.registrant.registrations q  "
                    + "inner join r.registrant.registrantAddresses a "
                    + "left join a.city c "
                    + "where q.course = :course and r.registrantActivityType='RENEWAL' and q.deRegistrationDate is not null "
                    + "and r.duration.councilDuration= :firstDuration and r.registrant not in "
                    + "(select r2.registrant from RegistrantActivity r2 where r2.registrantActivityType='RENEWAL' "
                    + "and r2.duration.councilDuration = :secondDuration and r2.registrant = r.registrant)", RegistrantData.class)
                    .setParameter("course", course)
                    .setParameter("firstDuration", firstDuration)
                    .setParameter("secondDuration", secondDuration);

        } else {

            query = entityManager.createQuery("select new zw.co.hitrac.council.business.domain.reports.RegistrantData("
                    + "r.registrant.id,r.registrant.firstname,r.registrant.lastname, r.registrant.middlename,"
                    + "r.registrant.birthDate, r.registrant.gender,r.registrant.idNumber,r.registrant.registrationNumber, "
                    + "concat(a.address1, ' ', a.address2, ' ', case when c is not null then c.name else '' end)) "
                    + "from RegistrantActivity r inner join r.registrant.registrations q  "
                    + "inner join r.registrant.registrantAddresses a "
                    + "inner join a.addressType t "
                    + "left join a.city c "
                    + "where q.course = :course and r.registrantActivityType='RENEWAL' and q.deRegistrationDate is not null "
                    + "and t = :addressType "
                    + "and r.duration.councilDuration= :firstDuration and r.registrant not in "
                    + "(select r2.registrant from RegistrantActivity r2 where r2.registrantActivityType='RENEWAL' "
                    + "and r2.duration.councilDuration = :secondDuration and r2.registrant = r.registrant)", RegistrantData.class)
                    .setParameter("course", course)
                    .setParameter("firstDuration", firstDuration)
                    .setParameter("addressType", addressType)
                    .setParameter("secondDuration", secondDuration);

        }

        return query.getResultList();
    }



    @Override
    public boolean hasValidIdentifications(Registrant registrant, List<IdentificationValidationRule> validationRules) {

        if (StringUtils.isBlank(registrant.getIdNumber())) {
            throw new CouncilException("Identification Number must not be empty");
        }

        boolean validIdentification = false;

        if (validationRules != null
                && !validationRules.isEmpty()) {

            for (IdentificationValidationRule validationRule : validationRules) {

                String regex = validationRule.getValidationRegularExpressionFromExample();

                if (StringUtils.isNotBlank(regex)
                        && registrant.getIdNumber().matches(regex)) {

                    validIdentification = true;
                    break;
                }
            }
        } else {

            validIdentification = true;
        }

        return validIdentification;

    }

    @Override
    public List<Registrant> findByCitizenship(Citizenship citizenship) {

        return registrantRepository.findByCitizenship(citizenship);
    }

    @Override
    public Registrant findByIdNumber(String idNumber) {

        return registrantRepository.findByIdNumber(idNumber);
    }

    public List<Registrant> getDeRegistrants() {

        return entityManager.createQuery("select r.registrant from Registration r where r.registrant not in (select r.registrant from Registration r where r.deRegistrationDate is NULL)").getResultList();//to get all who did not register for the current period
    }

    private ProjectionList createRegistrantProjectList() {
        ProjectionList projectionList = Projections.projectionList();
        projectionList.add(Projections.property("id"), "id");
        projectionList.add(Projections.property("firstname"), "firstname");
        projectionList.add(Projections.property("lastname"), "lastname");
        projectionList.add(Projections.property("middlename"), "middlename");
        projectionList.add(Projections.property("registrationNumber"), "registrationNumber");
        projectionList.add(Projections.property("idNumber"), "idNumber");
        projectionList.add(Projections.property("birthDate"),"birthDate" );
        projectionList.add(Projections.property("gender"), "gender");

        return projectionList;
    }

    public List<RegistrantData> getDeceasedRegistrants() {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registrant.class);
        criteria.add(Restrictions.eq("dead", Boolean.TRUE));
        criteria.setProjection(createRegistrantProjectList());
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        criteria.setResultTransformer(Transformers.aliasToBean(RegistrantData.class));
        return criteria.list();
    }

    public List<RegistrantData> getVoidedRegistrants() {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registrant.class);
        criteria.add(Restrictions.eq("voided", Boolean.TRUE));
        criteria.setProjection(createRegistrantProjectList());
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        criteria.setResultTransformer(Transformers.aliasToBean(RegistrantData.class));
        return criteria.list();
    }

    public List<RegistrantData> getRetiredRegistrants() {

        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registrant.class);
        criteria.add(Restrictions.eq("retired", Boolean.TRUE));
        criteria.setProjection(createRegistrantProjectList());
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        criteria.setResultTransformer(Transformers.aliasToBean(RegistrantData.class));
        return criteria.list();
    }

    public List<RegistrantData> getRegistrants() {

        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registrant.class);
        criteria.setProjection(createRegistrantProjectList());
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        criteria.setResultTransformer(Transformers.aliasToBean(RegistrantData.class));
        return criteria.list();
    }

    public void createRegistrantAccounts() {

        List<Registrant> registrants = new ArrayList<Registrant>();
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registrant.class);
        criteria.add(Restrictions.isNull("customerAccount"));
        registrants = (List<Registrant>) criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        for (Registrant registrant : registrants) {
            if (registrant.getCustomerAccount() == null || registrant.getCustomerAccount().getId() == null) {
                registrant.setCustomerAccount(new Customer());
                registrant.setUid(UUID.randomUUID().toString());
                registrant.getCustomerAccount().setCode(UUID.randomUUID().toString());
                registrant.getCustomerAccount().setAccount(new Account());
                registrant.getCustomerAccount().getAccount().setBalance(BigDecimal.ZERO);
                registrant.getCustomerAccount().getAccount().setAccountType(AccountType.ACCOUNTS_RECEIVABLE);
                registrant.getCustomerAccount().getAccount().setCode(UUID.randomUUID().toString());
                registrant.getCustomerAccount().setCustomerName(registrant.getFullname());
                registrant.getCustomerAccount().getAccount().setName(registrant.getFullname());
                registrant.getCustomerAccount().getAccount().setPersonalAccount(Boolean.TRUE);
                registrantRepository.save(registrant);
            }
        }
    }

    public List<Registrant> getApprovingRegistrants() {

        Boolean isAnApprover = Boolean.TRUE;
        return entityManager.createQuery("select r from Registrant r where r.isAnApprover=:isAnApprover").setParameter("isAnApprover", isAnApprover).getResultList();
    }

    public Registrant getRegistrant(Customer customer) {

        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registrant.class);
        criteria.add(Restrictions.eq("customerAccount", customer));
        return (Registrant) criteria.uniqueResult();
    }

    public Registrant createUids(Registrant registrant) {

        if (registrant.getUid() == null) {
            registrant.setUid(UUID.randomUUID().toString());

        }
        return registrant;

    }

    public List<RegistrantData> getRegistrantAgeByParameter(Integer lowerLimit, Integer upperLimit) {
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registrant.class);
        criteria.add(Restrictions.eq("dead", Boolean.FALSE));
        criteria.setProjection(createRegistrantProjectList());
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        criteria.setResultTransformer(Transformers.aliasToBean(RegistrantData.class));
        Set<RegistrantData> registrants = new HashSet<RegistrantData>();
        List<RegistrantData> registrantDatas = (List<RegistrantData>) criteria.list();
        for (RegistrantData r : registrantDatas) {
            if (r.getIntegerValueOfAge() >= lowerLimit && r.getIntegerValueOfAge() <= upperLimit) {

                registrants.add(r);
            }
        }
        return new ArrayList<RegistrantData>(registrants);
    }

    /**
     * @param query
     * @return
     */
    public List<Registrant> getRegistrants(String query, String gender, Citizenship citizenship, Boolean dead) {

        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registrant.class);
        if (StringUtils.isNotBlank(query)) {

            if ((query.contains(",") || query.contains(" ")) && (query.length() != 1)) {

                query = query.replace(",", " ");
                String[] names = query.split(" ");

                for (String name : names) {
                    criteria.add(getNameSearchCriteria(name));
                }

            } else {

                criteria.add(getNameSearchCriteria(query));
            }
        }
        if (gender != null) {
            criteria.add(Restrictions.eq("gender", gender));
        }
        if (citizenship != null) {
            criteria.add(Restrictions.eq("citizenship", citizenship));
        }
        if (dead != null) {
            criteria.add(Restrictions.eq("dead", dead));
        }
        criteria.addOrder(Order.asc("lastname"));
        criteria.addOrder(Order.asc("firstname"));
        criteria.addOrder(Order.asc("middlename"));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    private static Disjunction getNameSearchCriteria(String name) {

        Disjunction disjunctionCriteria = Restrictions.disjunction();
        disjunctionCriteria.add(Restrictions.like("firstname", name, MatchMode.ANYWHERE));
        disjunctionCriteria.add(Restrictions.like("lastname", name, MatchMode.ANYWHERE));
        disjunctionCriteria.add(Restrictions.like("middlename", name, MatchMode.ANYWHERE));
        disjunctionCriteria.add(Restrictions.eq("idNumber", name));
        disjunctionCriteria.add(Restrictions.eq("registrationNumber", name));

        return disjunctionCriteria;

    }

    public List<RegistrantData> getRegistrantList(String query, String gender, Citizenship citizenship, Boolean dead) {

        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registrant.class);
        if (StringUtils.isNotBlank(query)) {

            if ((query.contains(",") || query.contains(" ")) && (query.length() != 1)) {

                query = query.replace(",", " ");
                String[] names = query.split(" ");

                for (String name : names) {
                    criteria.add(getNameSearchCriteria(name));
                }

            } else {

                criteria.add(getNameSearchCriteria(query));
            }
        }

        if (gender != null) {
            criteria.add(Restrictions.eq("gender", gender));
        }
        if (citizenship != null) {
            criteria.add(Restrictions.eq("citizenship", citizenship));
        }
        if (dead != null) {
            criteria.add(Restrictions.eq("dead", dead));
        }
        criteria.addOrder(Order.asc("lastname"));
        criteria.addOrder(Order.asc("firstname"));
        criteria.addOrder(Order.asc("middlename"));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        criteria.setProjection(createRegistrantProjectList());
        criteria.setResultTransformer(Transformers.aliasToBean(RegistrantData.class));
        return criteria.list();
    }

    public List<RegistrantData> getSupervisorList(String query, Boolean dead) {

        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registrant.class);
        if (query != null) {
            criteria.add(Restrictions.or(Restrictions.like("firstname", query, MatchMode.START), Restrictions.or(Restrictions.like("lastname", query, MatchMode.START), Restrictions.or(Restrictions.like("middlename", query,
                    MatchMode.START), Restrictions.eq("idNumber", query)), Restrictions.or(Restrictions.like("placeOfBirth", query), Restrictions.like("registrationNumber", query)))));
        }

        if (dead != null) {
            criteria.add(Restrictions.eq("dead", dead));
        }
        criteria.add(Restrictions.eq("supervisor", Boolean.TRUE));
        criteria.addOrder(Order.asc("lastname"));
        criteria.addOrder(Order.asc("firstname"));
        criteria.addOrder(Order.asc("middlename"));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        criteria.setProjection(createRegistrantProjectList());
        criteria.setResultTransformer(Transformers.aliasToBean(RegistrantData.class));
        return criteria.list();
    }

    public Registrant getRegistrantByAccount(Customer customer) {

        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registrant.class);
        criteria.add(Restrictions.eq("customerAccount", customer));
        return (Registrant) criteria.uniqueResult();
    }

    public Registrant getRegistrantByAccountData(Account account) {

        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registrant.class);
        criteria.createAlias("customerAccount", "c");
        criteria.add(Restrictions.eq("c.account", account));
        return (Registrant) criteria.uniqueResult();
    }

    public Boolean getUniqueRegistrationNumber(String registrationNumber) {

        if (getRegistrantByRegistrationNumber(registrationNumber) == null) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }
    }

    public Boolean getRegistrantIdentificationNumber(String idNumber) {

        if (getRegistrantByIdentificationNumber(idNumber) == null) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }
    }

    public Registrant getRegistrantByRegistrationNumber(String registrationNumber) {

        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registrant.class);
        criteria.add(Restrictions.eq("registrationNumber", registrationNumber));
        return (Registrant) criteria.uniqueResult();

    }

    public Registrant getRegistrantByIdentificationNumber(String idNumber) {

        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registrant.class);
        criteria.add(Restrictions.eq("idNumber", idNumber));
        return (Registrant) criteria.uniqueResult();
    }

    public List<Long> getIds() {

        return entityManager.createQuery("select r.id from Registrant r").getResultList();
    }

    public List<Registrant> getRegistrantWithDebts() {

        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registrant.class);
        criteria.createAlias("customerAccount", "c");
        criteria.createAlias("c.account", "a");
        criteria.add(Restrictions.gt("a.balance", BigDecimal.ZERO));
        criteria.add(Restrictions.eq("a.personalAccount", Boolean.TRUE));
        criteria.addOrder(Order.asc("lastname"));
        criteria.addOrder(Order.asc("firstname"));
        criteria.addOrder(Order.asc("middlename"));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<Registrant> getRegistrantWithCarryForwards() {

        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registrant.class);
        criteria.createAlias("customerAccount", "c");
        criteria.createAlias("c.account", "a");
        criteria.add(Restrictions.lt("a.balance", BigDecimal.ZERO));
        criteria.add(Restrictions.eq("a.personalAccount", Boolean.TRUE));
        criteria.addOrder(Order.asc("lastname"));
        criteria.addOrder(Order.asc("firstname"));
        criteria.addOrder(Order.asc("middlename"));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<RegistrantData> getRegistrantCorrections() {

        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registrant.class);
        criteria.add(Restrictions.or(Restrictions.like("idNumber", "NOID", MatchMode.START), Restrictions.or(Restrictions.isNull("idNumber"))));
        criteria.add(Restrictions.eq("dead", Boolean.FALSE));
        criteria.addOrder(Order.asc("lastname"));
        criteria.addOrder(Order.asc("firstname"));
        criteria.addOrder(Order.asc("middlename"));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        criteria.setProjection(createRegistrantProjectList());
        criteria.setResultTransformer(Transformers.aliasToBean(RegistrantData.class));
        return criteria.list();
    }

    public List<RegistrantData> getRegistrantDateCorrections() {

        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registrant.class);
        Date date = null;
        try {
            date = DateUtil.convertStringToDate("01/01/1900");
        } catch (ParseException ex) {
            Logger.getLogger(RegistrantDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        criteria.add(Restrictions.or(Restrictions.eq("birthDate", date), Restrictions.or(Restrictions.isNull("birthDate"))));
        criteria.add(Restrictions.eq("dead", Boolean.FALSE));
        criteria.addOrder(Order.asc("lastname"));
        criteria.addOrder(Order.asc("firstname"));
        criteria.addOrder(Order.asc("middlename"));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        criteria.setProjection(createRegistrantProjectList());
        criteria.setResultTransformer(Transformers.aliasToBean(RegistrantData.class));
        return criteria.list();
    }

    @Override
    public List<Registrant> getRegistrantsWithNoOrInvalidNationalIDs(Citizenship citizenship, IdentificationType nationalIdentificationType) {

        final String missingId = "NOID";
        List<Registrant> allRegistrantsList;
        List<IdentificationValidationRule> validationRules = null;

        if (citizenship == null) {

            allRegistrantsList = findAll();

        } else {

            if (nationalIdentificationType != null) {
                validationRules = identificationValidationRuleDAO.findByCitizenshipAndIdentificationType(citizenship, nationalIdentificationType);
            }

            allRegistrantsList = findByCitizenship(citizenship);
        }

        List<Registrant> registrantsWithInvalidIdentificationNumbers = new ArrayList<Registrant>();

        for (Registrant registrant : allRegistrantsList) {

            if (registrant.getIdNumber() == null
                    || registrant.getIdNumber().equalsIgnoreCase(missingId)) {

                registrantsWithInvalidIdentificationNumbers.add(registrant);

            } else if (validationRules != null
                    && !validationRules.isEmpty()
                    && !hasValidIdentifications(registrant, validationRules)) {

                registrantsWithInvalidIdentificationNumbers.add(registrant);

            }
        }

        return registrantsWithInvalidIdentificationNumbers;

    }

    @Override
    public List<RegistrantData> getRegistrantsWithConditions(CouncilDuration councilDuration) {

        Query query = entityManager.createQuery("select DISTINCT new zw.co.hitrac.council.business.domain.reports.RegistrantData(r.registrant.id,"
                + "r.registrant.firstname,r.registrant.lastname, r.registrant.middlename,r.registrant.gender,"
                + "r.registrant.idNumber,r.registrant.registrationNumber, cond.name) "
                + "from RegistrantActivity r "
                + "inner join r.registrant.registrantQualifications q  "
                + "inner join r.registrant.registrantConditions rc  "
                + "inner join rc.condition cond "
                + "where r.registrantActivityType='RENEWAL' "
                + "and rc.status = true "
                + "and r.duration.councilDuration = :councilDuration", RegistrantData.class)
                .setParameter("councilDuration", councilDuration);

        return query.getResultList();
    }

    @Override
    public List<RegistrantData> getRegistrantsWithConditions(CouncilDuration councilDuration, Conditions condition) {

        Query query = entityManager.createQuery("select DISTINCT new zw.co.hitrac.council.business.domain.reports.RegistrantData(r.registrant.id,"
                + "r.registrant.firstname,r.registrant.lastname, r.registrant.middlename,r.registrant.gender,"
                + "r.registrant.idNumber,r.registrant.registrationNumber, cond.name) "
                + "from RegistrantActivity r "
                + "inner join r.registrant.registrantQualifications q  "
                + "inner join r.registrant.registrantConditions rc  "
                + "inner join rc.condition cond "
                + "where r.registrantActivityType='RENEWAL' "
                + "and rc.status = true "
                + "and r.duration.councilDuration = :councilDuration "
                + "and cond = :condition", RegistrantData.class)
                .setParameter("councilDuration", councilDuration)
                .setParameter("condition", condition);

        return query.getResultList();
    }

    @Override
    public List<RegistrantData> getRegistrantsWithBirthDays(Date date) {

        GeneralParameters generalParameters = generalParametersService.get();

        String emailUserName = generalParameters.getEmail();
        String emailPassword = generalParameters.getEmailPassword();
        ContactType emailContactType = generalParameters.getEmailContactType();

        if (emailContactType != null && !StringUtils.isEmpty(emailUserName) && !StringUtils.isEmpty(emailPassword)) {

            int monthParam = DateUtil.getMonthFromDate(date);
            int dayParam = DateUtil.getDayFromDate(date);

            
            Session session = entityManager.unwrap(Session.class);

            return session.createQuery("select new zw.co.hitrac.council.business.domain.reports.RegistrantData(r.id,r.firstname, r.lastname, r.middlename, rc.contactDetail, r.birthDate) "
                    + "from RegistrantContact rc join rc.registrant r "
                    + "where rc.contactType = :emailContactType  "
                    + "and month(r.birthDate) = :monthParam and day(r.birthDate) = :dayParam")
                    .setParameter("emailContactType", emailContactType)
                    .setParameter("monthParam", monthParam)
                    .setParameter("dayParam", dayParam)
                    .list();
        } else {

            System.err.println("Cannot generate list for birthday reminders because one of email address contact type, "
                    + "email user name or password was not set in General Parameters");
            return new ArrayList<RegistrantData>();
        }

    }

    @Override
    public List<RegistrantData> getRegistrantsWhoOwnInstitutions() {

        Query query = entityManager.createQuery("select distinct new zw.co.hitrac.council.business.domain.reports.RegistrantData(r.firstname, r.lastname, r.middlename, i.name) "
                + "from Institution i inner join i.registrant r", RegistrantData.class);
        List<RegistrantData> registrantDataList = query.getResultList();

        if (registrantDataList != null && !registrantDataList.isEmpty()) {

            Collections.sort(registrantDataList, new Comparator<RegistrantData>() {

                public int compare(RegistrantData o1, RegistrantData o2) {

                    return ComparisonChain.start()
                            .compare(o1.getNameOfInstitutionOwnedByRegistrant(),
                                    o2.getNameOfInstitutionOwnedByRegistrant(), Ordering.natural().nullsLast())
                            .compare(o1.getFullName(),
                                    o2.getFullName(), Ordering.natural().nullsLast())
                            .result();

                }
            });
        }

        return registrantDataList;
    }

    @Override
    public List<RegistrantData> getRegistrantsWhoSuperviseInstitutions() {

        Query query = entityManager.createQuery("select distinct new zw.co.hitrac.council.business.domain.reports.RegistrantData(r.firstname, r.lastname, r.middlename, i.name) "
                + "from Institution i inner join i.supervisor r", RegistrantData.class);
        List<RegistrantData> registrantDataList = query.getResultList();

        if (registrantDataList != null && !registrantDataList.isEmpty()) {

            Collections.sort(registrantDataList, new Comparator<RegistrantData>() {

                public int compare(RegistrantData o1, RegistrantData o2) {

                    return ComparisonChain.start()
                            .compare(o1.getNameOfInstitutionOwnedByRegistrant(),
                                    o2.getNameOfInstitutionOwnedByRegistrant(), Ordering.natural().nullsLast())
                            .compare(o1.getFullName(),
                                    o2.getFullName(), Ordering.natural().nullsLast())
                            .result();

                }
            });
        }

        return registrantDataList;
    }
}
