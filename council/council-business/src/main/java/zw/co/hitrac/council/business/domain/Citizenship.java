package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 *
 * @author Edward Zengeni
 */
@Entity
@Table(name="citizenship")
@Audited
@XmlRootElement
public class Citizenship extends  BaseEntity implements Serializable {

}
