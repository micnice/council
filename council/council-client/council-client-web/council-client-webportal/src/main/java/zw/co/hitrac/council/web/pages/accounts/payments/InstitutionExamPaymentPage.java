
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.accounts.payments;

//~--- non-JDK imports --------------------------------------------------------
import java.math.BigDecimal;
import java.util.ArrayList;
import zw.co.hitrac.council.web.pages.examinations.*;
import net.sf.jasperreports.engine.JRException;

import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.examinations.ExamSettingService;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

//~--- JDK imports ------------------------------------------------------------

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistrationDebtComponent;
import zw.co.hitrac.council.business.domain.reports.ProductSale;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.service.accounts.ReceiptItemService;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationDebtComponentService;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.reports.InstitutionExamPayment;
import zw.co.hitrac.council.web.models.ExamSettingListModel;

/**
 *
 * @author tdhlakama
 */
public class InstitutionExamPaymentPage extends IExaminationsPage {

    @SpringBean
    private ExamSettingService examSettingService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private CourseService courseService;
    private ExamSetting examSetting;
    private Course course;
    @SpringBean
    ReceiptItemService receiptItemService;
    @SpringBean
    RegistrationService registrationService;
    @SpringBean
    private ExamRegistrationDebtComponentService examRegistrationDebtComponentService;
    private HrisComparator hrisComparator = new HrisComparator();

    public InstitutionExamPaymentPage() {
        PropertyModel<ExamSetting> examSettingModel = new PropertyModel<ExamSetting>(this, "examSetting");
        PropertyModel<Course> courseModel = new PropertyModel<Course>(this, "course");
        Form<?> form = new Form("form") {
            @Override
            protected void onSubmit() {
                try {
                    InstitutionExamPayment institutionExamPayment = new InstitutionExamPayment();
                    Map parameters = new HashMap();

                    if (course.getQualification() != null) {
                        parameters.put("course", course.getQualification().getName());
                    } else {
                        parameters.put("course", course.getName());
                    }

                    if (examSetting != null) {
                        parameters.put("examSetting", examSetting.toString());
                    }

                    List<ProductSale> productSales = new ArrayList<ProductSale>();
                    for (Institution i : (List<Institution>) hrisComparator.sort((new ArrayList<Institution>(courseService.get(course.getId()).getTrainingInstitutions())))) {
                        ProductSale productSale = new ProductSale();
                        productSale.setInstitution(i);
                        BigDecimal totalAmount = new BigDecimal(0);
                        for (ExamRegistrationDebtComponent component : examRegistrationDebtComponentService.getDebtComponents(examSetting, course, i)) {
                                totalAmount = totalAmount.add(receiptItemService.getTotalPaidForDebtComponent(component.getDebtComponent()));
                        }
                        
                        productSale.setAmountPaid(totalAmount);
                        productSales.add(productSale);
                    }
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(institutionExamPayment,
                            contentType, productSales, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        };

        form.add(new DropDownChoice("examSetting", examSettingModel, new ExamSettingListModel(examSettingService)).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("course", courseModel, courseService.findAll()).setRequired(true).add(new ErrorBehavior()));
        add(form);

    }
}


//~ Formatted by Jindent --- http://www.jindent.com
