
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.reports;

//~--- non-JDK imports --------------------------------------------------------

import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.ProductIssuanceType;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.reports.CertificateTypeReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.utility.DatePickerUtil;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

//~--- JDK imports ------------------------------------------------------------

/**
 *
 * @author tdhlakama
 */
public class CertificateTypeReportPage extends IReportPage {

    @SpringBean
    private QualificationService QualificationService;
    @SpringBean
    private RegistrantQualificationService registrantQualificationService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private ProductIssuanceService productIssuanceService;
    private Date startDate, endDate;
    private ProductIssuanceType productIssuanceType;

    private ContentType contentType;
    @SpringBean
    private RegistrantService registrantService;

    public CertificateTypeReportPage() {


        PropertyModel<Date> startDateModel = new PropertyModel<Date>(this, "startDate");
        PropertyModel<Date> endDateModel = new PropertyModel<Date>(this, "endDate");
        PropertyModel<ContentType> contentTypeModel = new PropertyModel<ContentType>(this, "contentType");
        PropertyModel<ProductIssuanceType> productIssuanceTypeModel = new PropertyModel<ProductIssuanceType>(this, "productIssuanceType");
        Form<?> form = new Form("form");
        form.add(new TextField<Date>("startDate", startDateModel).add(DatePickerUtil.getDatePicker()));
        form.add(new TextField<Date>("endDate", endDateModel).add(DatePickerUtil.getDatePicker()));
        form.add(new DropDownChoice("productIssuanceType", productIssuanceTypeModel, Arrays.asList(ProductIssuanceType.values())));
        form.add(new DropDownChoice("contentType", contentTypeModel, Arrays.asList(ContentType.values())));
        add(form);

        form.add(new Button("report") {
            @Override
            public void onSubmit() {
                try {
                    Map parameters = new HashMap();
                    String query = "List of Individuals who have obtained the " + productIssuanceType + " " + "cert type ";

                    if (startDate != null && endDate != null) {
                        query.concat(" between " + DateUtil.getDate(startDate) + " to " + DateUtil.getDate(endDate));
                    } else if (startDate != null && endDate == null) {
                        query.concat(" between " + DateUtil.getDate(startDate) + " to " + DateUtil.getDate(new Date()));
                    }

                    CertificateTypeReport certificateTypeReport = new CertificateTypeReport();
                    parameters.put("comment", query);
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(certificateTypeReport, contentType, productIssuanceService.getProductIssuances(productIssuanceType, startDate, endDate),parameters);

                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);
                    resource.respond(a);
                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });
    }
}



//~ Formatted by Jindent --- http://www.jindent.com
