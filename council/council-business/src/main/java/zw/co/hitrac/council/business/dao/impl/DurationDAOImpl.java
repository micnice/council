package zw.co.hitrac.council.business.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.DurationDAO;
import zw.co.hitrac.council.business.dao.repo.CouncilDurationRepository;
import zw.co.hitrac.council.business.dao.repo.DurationRepository;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Duration;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author Michael Matiashe
 */
@Repository
public class DurationDAOImpl implements DurationDAO {

    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    private DurationRepository durationRepository;
    @Autowired
    private CouncilDurationRepository councilDurationRepository;

    public Duration save(Duration duration) {
        return durationRepository.save(duration); //Make sure every course is in one active period only!
    }

    public List<Duration> findAll() {
        return durationRepository.findAll();
    }

    public Duration get(Long id) {
        return durationRepository.findOne(id);
    }

    /**
     * A setter method that will make mocking repo object easier
     *
     * @param durationRepository
     */
    public void setDurationRepository(DurationRepository durationRepository) {
        this.durationRepository = durationRepository;
    }

    public Duration getCurrentCourseDuration(Course course) {
        Optional<Duration> durationOptional = (Optional<Duration>) entityManager.createQuery("SELECT d from Duration d join d.courses c where d.active=true and c in :course")
                .setParameter("course", course).setMaxResults(1).getResultList().stream().findFirst();
        if (durationOptional.isPresent()) {
            return durationOptional.get();
        } else {
            return null;
        }
    }

    public Duration getLastRenewalDuration(Course course, Date startDate) {
        List<Duration> durations = getCourseDurations(course);
        for (Duration duration : durations) {
            if (duration.getStartDate().equals(startDate)) {
                return duration;
            }
        }
        return null;
    }

    public List<Duration> getCourseDurations(Course course) {
        return entityManager.createQuery("SELECT distinct d from Duration d join d.courses c where c in :course")
                .setParameter("course", course).getResultList();

    }

    public Duration getDurationByCourseAndCouncilDuration(Course course, CouncilDuration councilDuration) {
        return (Duration) entityManager.createQuery("SELECT d from Duration d join d.courses c where d.councilDuration=:councilDuration and c in :course")
                .setParameter("councilDuration", councilDuration)
                .setParameter("course", course).getSingleResult();
    }

    public List<Duration> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Duration> getDurations(CouncilDuration councilDuration) {
        return entityManager.createQuery("SELECT d from Duration d where d.councilDuration=:councilDuration").setParameter("councilDuration", councilDuration).getResultList();
    }

    @Override
    public List<Duration> getDurationsAfterDuration(Duration duration, Course course) {
        return entityManager.createQuery("SELECT d from Duration d join d.courses c where d.startDate>=:startDate and c in :course ")
                .setParameter("startDate", duration.getStartDate())
                .setParameter("course", course).getResultList();
    }

    @Override
    public List<Duration> getDurationsBeforeDuration(Duration duration, Course course) {
        return entityManager.createQuery("SELECT d from Duration d join d.courses c where d.startDate<=:startDate and c in :course ")
                .setParameter("startDate", duration.getStartDate())
                .setParameter("course", course).getResultList();
    }

}
