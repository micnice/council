package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.CourseCondition;

/**
 *
 * @author tdhlakama
 */
public interface CourseConditionRepository extends CrudRepository<CourseCondition, Long> {
      public List<CourseCondition> findAll();  
       
}
