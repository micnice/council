package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.RegistrantStatusDAO;
import zw.co.hitrac.council.business.dao.repo.RegistrantStatusRepository;
import zw.co.hitrac.council.business.domain.RegistrantStatus;

/**
 *
 * @author Charles Chigoriwa
 */
@Repository
public class RegistrantStatusDAOImpl implements RegistrantStatusDAO{
   @Autowired
   private RegistrantStatusRepository registrantStatusRepository;
    
   public RegistrantStatus save(RegistrantStatus registrantStatus){
       return registrantStatusRepository.save(registrantStatus);
   }
   
   public List<RegistrantStatus> findAll(){
       return registrantStatusRepository.findAll();
   }
   
   public RegistrantStatus get(Long id){
       return registrantStatusRepository.findOne(id);
   }
 /**
     * A setter method that will make mocking repo object easier
     * @param registrantStatusRepository 
     */
   
   public void setRegistrantStatusRepository(RegistrantStatusRepository registrantStatusRepository) {
         this.registrantStatusRepository = registrantStatusRepository;
    }

    public List<RegistrantStatus> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
