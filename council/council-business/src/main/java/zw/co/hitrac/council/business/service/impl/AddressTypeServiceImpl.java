package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.AddressTypeDAO;
import zw.co.hitrac.council.business.domain.AddressType;
import zw.co.hitrac.council.business.service.AddressTypeService;

import java.util.List;

/**
 *
 * @author Charles Chigoriwa
 */
@Service
@Transactional
public class AddressTypeServiceImpl implements AddressTypeService{
    
    @Autowired
    private AddressTypeDAO addressTypeDAO;
    

    @Transactional
    public AddressType save(AddressType addressType) {
       return addressTypeDAO.save(addressType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<AddressType> findAll() {
        return addressTypeDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public AddressType get(Long id) {
       return addressTypeDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<AddressType> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setAddressTypeDAO(AddressTypeDAO addressTypeDAO) {

        this.addressTypeDAO = addressTypeDAO;
    }

	@Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public AddressType findByName(String name) {
		return addressTypeDAO.findByName(name);
	}
}
