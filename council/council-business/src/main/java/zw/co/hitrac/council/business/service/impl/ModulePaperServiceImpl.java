package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.ModulePaperDAO;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.ModulePaper;
import zw.co.hitrac.council.business.service.ModulePaperService;

import java.util.List;

/**
 *
 * @author Charles Chigoriwa
 */
@Service
@Transactional
public class ModulePaperServiceImpl implements ModulePaperService {

    @Autowired
    private ModulePaperDAO modulePaperDAO;

    @Transactional
    public ModulePaper save(ModulePaper modulePaper) {
        return modulePaperDAO.save(modulePaper);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ModulePaper> findAll() {
        return modulePaperDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public ModulePaper get(Long id) {
        return modulePaperDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ModulePaper> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setModulePaperDAO(ModulePaperDAO modulePaperDAO) {
        this.modulePaperDAO = modulePaperDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ModulePaper> findPapersInCourse(Course course) {
        return modulePaperDAO.findPapersInCourse(course);
    }
}
