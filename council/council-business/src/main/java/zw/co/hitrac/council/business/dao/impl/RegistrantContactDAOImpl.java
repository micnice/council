package zw.co.hitrac.council.business.dao.impl;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.hibernate.transform.Transformers;
import org.hibernate.type.DateType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.RegistrantContactDAO;
import zw.co.hitrac.council.business.dao.repo.RegistrantContactRepository;
import zw.co.hitrac.council.business.domain.ContactType;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantContact;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.GeneralParametersService;

/**
 *
 * @author kelvin
 * @author morris baradza
 */
@Repository
public class RegistrantContactDAOImpl implements RegistrantContactDAO {

    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    private RegistrantContactRepository registrantContactRepository;
    @Autowired
    GeneralParametersService generalParametersService;

    public RegistrantContact save(RegistrantContact registrantContact) {
        return registrantContactRepository.save(registrantContact);
    }

    public List<RegistrantContact> findAll() {
        return registrantContactRepository.findAll();
    }

    public RegistrantContact get(Long id) {
        return registrantContactRepository.findOne(id);
    }

    /**
     * A setter method that will make mocking repo object easier
     *
     * @param registrantContactRepository
     */
    public void setRegistrantContactRepository(RegistrantContactRepository registrantContactRepository) {
        this.registrantContactRepository = registrantContactRepository;
    }

    public List<RegistrantContact> getContacts(Registrant registrant, Institution institution) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantContact.class);
        if (institution == null && registrant == null) {
            return new ArrayList<RegistrantContact>();
        }
        if (registrant != null) {
            criteria.add(Restrictions.eq("registrant", registrant));
        }
        if (institution != null) {
            criteria.add(Restrictions.eq("institution", institution));
        }
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<RegistrantContact> getContactsByContactType(Registrant registrant, ContactType contactType) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantContact.class);
        criteria.add(Restrictions.eq("registrant", registrant));
        criteria.add(Restrictions.eq("contactType", contactType));
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public RegistrantContact getActiveContactbyContactType(Registrant registrant, ContactType contactType) {
        List<RegistrantContact> registrantContacts = new ArrayList<RegistrantContact>();
        registrantContacts.addAll(getContactsByContactType(registrant, contactType));
        if (registrantContacts.isEmpty()) {
            return null;
        }
        List<RegistrantContact> registrantActiveList = new ArrayList<RegistrantContact>();
        for (RegistrantContact r : registrantContacts) {
            if (r.getStatus() != null && r.getStatus()) {
                registrantActiveList.add(r);
            }
        }
        if (registrantActiveList.isEmpty()) {
            return null;
        } else {
            return registrantActiveList.get(0);
        }
    }

    public List<RegistrantContact> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<RegistrantContact> getMailContacts() {
        return entityManager.createQuery("SELECT r FROM RegistrantContact r WHERE r.contactType=:contactType and r.status=:status").setParameter("contactType", generalParametersService.get().getEmailContactType()).setParameter("status", Boolean.TRUE).getResultList();
    }

    public List<RegistrantData> getRegistrantDataAddress(ContactType contactType) {
        return entityManager.createQuery("select DISTINCT new zw.co.hitrac.council.business.domain.reports.RegistrantData(reg.registrant.id,reg.registrant.firstname,reg.registrant.lastname, reg.registrant.middlename,reg.registrant.birthDate, reg.registrant.gender,reg.registrant.idNumber,reg.registrant.registrationNumber, reg.contactDetail) from RegistrantContact reg where reg.contactType=:contactType AND reg.retired = false").setParameter("contactType", contactType).getResultList();
    }

    public List<RegistrantData> getRegistrantDataContacts(ContactType contactType, Course course) {
        StringBuilder sb = new StringBuilder();
        sb.append("select registrant.id, registrant.firstname, registrant.lastname, registrant.middlename, registrant.birthDate, registrant.gender, registrant.idNumber, registrant.registrationNumber, regC.contact_detail AS detail");
        sb.append(" FROM registrant INNER JOIN registration reg on registrant.id=reg.registrant_id INNER JOIN registrantcontact regC ON registrant.id = regC.registrant_id ");

        sb.append("WHERE regC.contactType_id=").append(contactType.getId()).append(" AND regC.retired = false ");

        if (course != null) {
            sb.append("AND reg.course_id = ").append(course.getId()).append(" ");
        }
        sb.append("ORDER BY registrant.lastname, registrant.firstname, registrant.middlename DESC");
        System.out.println(sb.toString());

        Session session = entityManager.unwrap(Session.class);
        SQLQuery query = session.createSQLQuery(sb.toString());
        query.addScalar("id", LongType.INSTANCE);
        query.addScalar("firstname", StringType.INSTANCE);
        query.addScalar("lastname", StringType.INSTANCE);
        query.addScalar("birthDate", DateType.INSTANCE);
        query.addScalar("gender", StringType.INSTANCE);
        query.addScalar("idNumber", StringType.INSTANCE);
        query.addScalar("registrationNumber", StringType.INSTANCE);
        query.addScalar("detail", StringType.INSTANCE);
        query.setResultTransformer(Transformers.aliasToBean(RegistrantData.class));
        List<RegistrantData> registrantDataList = query.list();
        return registrantDataList;
    }

    @Override
    public List<RegistrantData> getAllRegistrantDataContacts(ContactType contactType) {

        StringBuilder sb = new StringBuilder();
        sb.append("select registrant.id, registrant.firstname, registrant.lastname, registrant.middlename, registrant.birthDate, registrant.gender, registrant.idNumber, registrant.registrationNumber, regC.contact_detail AS detail");
        sb.append(" FROM registrant INNER JOIN registration reg on registrant.id=reg.registrant_id INNER JOIN registrantcontact regC ON registrant.id = regC.registrant_id ");
        sb.append("WHERE regC.contactType_id=").append(contactType.getId()).append(" AND regC.retired = false ");
        sb.append("ORDER BY registrant.lastname, registrant.firstname, registrant.middlename DESC");
        System.out.println(sb.toString());

        Session session = entityManager.unwrap(Session.class);
        SQLQuery query = session.createSQLQuery(sb.toString());
        query.addScalar("id", LongType.INSTANCE);
        query.addScalar("firstname", StringType.INSTANCE);
        query.addScalar("lastname", StringType.INSTANCE);
        query.addScalar("birthDate", DateType.INSTANCE);
        query.addScalar("gender", StringType.INSTANCE);
        query.addScalar("idNumber", StringType.INSTANCE);
        query.addScalar("registrationNumber", StringType.INSTANCE);
        query.addScalar("detail", StringType.INSTANCE);
        query.setResultTransformer(Transformers.aliasToBean(RegistrantData.class));
        List<RegistrantData> registrantDataList = query.list();
        return registrantDataList;

    }

    public List<RegistrantData> getRegistrantDataAddressCorrections(ContactType contactType) {
        return entityManager.createQuery("select DISTINCT new zw.co.hitrac.council.business.domain.reports.RegistrantData(reg.registrant.id,reg.registrant.firstname,reg.registrant.lastname, reg.registrant.middlename,reg.registrant.birthDate, reg.registrant.gender,reg.registrant.idNumber,reg.registrant.registrationNumber, reg.contactDetail) from RegistrantContact reg where reg.contactType=:contactType AND reg.retired = false and Reg.registrant.idNumber LIKE 'NOID' or reg.registrant.idNumber IS NULL").setParameter("contactType", contactType).getResultList();
    }
}
