
package zw.co.hitrac.council.business.dao;

import zw.co.hitrac.council.business.domain.Province;

/**
 *
 * @author Kelvin Goredema
 */
public interface ProvinceDAO  extends IGenericDAO<Province>{
    
}