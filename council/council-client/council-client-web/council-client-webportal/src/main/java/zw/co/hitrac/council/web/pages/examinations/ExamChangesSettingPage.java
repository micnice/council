
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------
import com.googlecode.wicket.jquery.ui.panel.JQueryFeedbackPanel;

import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationService;
import zw.co.hitrac.council.business.service.examinations.ExamSettingService;
import java.util.List;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.web.models.CourseListModel;
import zw.co.hitrac.council.web.models.ExamSettingListModel;

/**
 *
 * @author tdhlakama
 */
public class ExamChangesSettingPage extends IExaminationsPage {

    @SpringBean
    private ExamRegistrationService examRegistrationService;
    @SpringBean
    private ExamSettingService examSettingService;
    @SpringBean
    private CourseService courseService;
    private ExamSetting examSetting;
    private ExamSetting newExamSetting;
    private Course course;

    public ExamChangesSettingPage() {
        // FeedbackPanel //
        final FeedbackPanel feedback = new JQueryFeedbackPanel("feedback");
        add(feedback.setOutputMarkupId(true));
        PropertyModel<ExamSetting> examSettingModel = new PropertyModel<ExamSetting>(this, "examSetting");
        PropertyModel<ExamSetting> newExamSettingModel = new PropertyModel<ExamSetting>(this, "newExamSetting");
        PropertyModel<Course> courseModel = new PropertyModel<Course>(this, "course");
        Form<?> form = new Form("form");
        form.add(new DropDownChoice("examSetting", examSettingModel, new ExamSettingListModel(examSettingService)));
        form.add(new DropDownChoice("newExamSetting", newExamSettingModel, new ExamSettingListModel(examSettingService)));
        form.add(new DropDownChoice("course", courseModel, new CourseListModel(courseService)));

        form.add(new Button("process") {
            @Override
            public void onSubmit() {
                try {
                    if (examSetting == null) {
                        error("Select Exam Period to Change");
                        return;
                    }
                    if (newExamSetting == null) {
                        error("Select Exam Period you want to change to");
                        return;
                    }
                    if (course == null) {
                        error("Select Course");
                        return;
                    }
                    int i = 0;
                    List<ExamRegistration> examRegistrations = examRegistrationService.getAllExamCandidates(examSetting, course, null, null, null, null, null, null,null,null);
                    for (ExamRegistration ex : examRegistrations) {
                        ex.setExamSetting(newExamSetting);
                        examRegistrationService.save(ex);
                        i++;
                    }
                    info("Number of Records Changed" + i);
                } catch (Exception ex) {
                    error("Error Occured Changing Records");
                }
            }
        });
        add(form);
    }
}
//~ Formatted by Jindent --- http://www.jindent.com
