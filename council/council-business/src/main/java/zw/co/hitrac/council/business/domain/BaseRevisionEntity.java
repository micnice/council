package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.DefaultRevisionEntity;
import org.hibernate.envers.RevisionEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by scott on 09/12/2016.
 */
@Entity
@Table(name = "audit_revisions")
@RevisionEntity(CustomRevisionListener.class)
public class BaseRevisionEntity extends DefaultRevisionEntity {

    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
