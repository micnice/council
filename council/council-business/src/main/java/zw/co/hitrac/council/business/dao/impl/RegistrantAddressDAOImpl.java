/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.impl;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.RegistrantAddressDAO;
import zw.co.hitrac.council.business.dao.repo.RegistrantAddressRepository;
import zw.co.hitrac.council.business.domain.AddressType;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantAddress;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;

/**
 *
 * @author kelvin
 */
@Repository
public class RegistrantAddressDAOImpl implements RegistrantAddressDAO {

    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    private RegistrantAddressRepository registrantAddressRepository;

    public RegistrantAddress save(RegistrantAddress registrantAddress) {
        return registrantAddressRepository.save(registrantAddress);
    }

    public List<RegistrantAddress> findAll() {
        return registrantAddressRepository.findAll();
    }

    public RegistrantAddress get(Long id) {
        return registrantAddressRepository.findOne(id);
    }

    /**
     * A setter method that will make mocking repo object easier
     *
     * @param registrantAddressRepository
     */
    public void setRegistrantAddressRepository(RegistrantAddressRepository registrantAddressRepository) {
        this.registrantAddressRepository = registrantAddressRepository;
    }

    public List<RegistrantAddress> getAddresses(Registrant registrant, Institution institution) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantAddress.class);
        if (institution == null && registrant == null) {
            return new ArrayList<RegistrantAddress>();
        }
        if (registrant != null) {
            criteria.add(Restrictions.eq("registrant", registrant));
        }
        if (institution != null) {
            criteria.add(Restrictions.eq("institution", institution));
        }
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<RegistrantAddress> getAddressesbyAddressType(Registrant registrant, AddressType addressType) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantAddress.class);
        criteria.add(Restrictions.eq("registrant", registrant));
        criteria.add(Restrictions.eq("addressType", addressType));
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public RegistrantAddress getActiveAddress(Registrant registrant) {
        List<RegistrantAddress> registrantAddresses = new ArrayList<RegistrantAddress>();
        registrantAddresses.addAll(getAddresses(registrant, null));
        if (registrantAddresses.isEmpty()) {
            return null;
        }
        List<RegistrantAddress> registrantActiveList = new ArrayList<RegistrantAddress>();
        for (RegistrantAddress r : registrantAddresses) {
            if (r.getStatus() != null && r.getStatus()) {
                registrantActiveList.add(r);
            }
        }
        if (registrantActiveList.isEmpty()) {
            return null;
        } else {
            return registrantActiveList.get(0);
        }
    }

    public RegistrantAddress getActiveAddressbyAddressType(Registrant registrant, AddressType addressType) {
        List<RegistrantAddress> registrantAddresses = new ArrayList<RegistrantAddress>();
        registrantAddresses.addAll(getAddressesbyAddressType(registrant, addressType));
        if (registrantAddresses.isEmpty()) {
            return null;
        }
        List<RegistrantAddress> registrantActiveList = new ArrayList<RegistrantAddress>();
        for (RegistrantAddress r : registrantAddresses) {
            if (r.getStatus() != null && r.getStatus()) {
                registrantActiveList.add(r);
            }
        }
        if (registrantActiveList.isEmpty()) {
            return null;
        } else {
            return registrantActiveList.get(0);
        }
    }

    public List<RegistrantAddress> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<RegistrantData> getRegistrantDataAddress(AddressType addressType) {
        return entityManager.createQuery("select DISTINCT new zw.co.hitrac.council.business.domain.reports.RegistrantData(reg.registrant.id,reg.registrant.firstname,reg.registrant.lastname, reg.registrant.middlename,reg.registrant.birthDate, reg.registrant.gender,reg.registrant.idNumber,reg.registrant.registrationNumber, CONCAT(reg.address1,' ',reg.address2,' ',reg.city.name)) from RegistrantAddress reg where reg.addressType=:addressType AND reg.retired = false").setParameter("addressType", addressType).getResultList();
    }
}
