package zw.co.hitrac.council.web.pages.reports;

import net.sf.jasperreports.engine.JRException;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.PageReference;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.reports.RegistrantEmploymentDataDBReport;
import zw.co.hitrac.council.reports.RegistrantEmploymentInstitutionDataDBReport;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.CourseListModel;
import zw.co.hitrac.council.web.models.RegisterListModel;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import zw.co.hitrac.council.reports.utils.ContentType;

/**
 * @author tdhlakama
 */
public class EmploymentByCourseAndDurationPage extends TemplatePage {

    @SpringBean
    private CouncilDurationService councilDurationService;
    @SpringBean
    private RegistrantActivityService registrantActivityService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    private CouncilDuration councilDuration;
    private Course course;
    private Register register;
    @SpringBean
    private RegisterService registerService;
    @SpringBean
    private CourseService courseService;
    @SpringBean
    private DataSource dataSource;
    private String status;
    private ContentType contentType;

    public EmploymentByCourseAndDurationPage(final PageReference pageReference) {

        menuPanelManager.getSearchPanel().setTopMenuCurrent(true);
        menuPanelManager.getSearchPanel().setSearchAdvanced(true);

        Register institutionRegister = generalParametersService.get().getInstitutionRegister();
        
        PropertyModel<CouncilDuration> councilDurationModel = new PropertyModel<CouncilDuration>(this, "councilDuration");
        PropertyModel<Course> courseModel = new PropertyModel<Course>(this, "course");
        PropertyModel<String> statusModel = new PropertyModel<String>(this, "status");
        PropertyModel<ContentType> contentTypeModel = new PropertyModel<ContentType>(this, "contentType");
        final PropertyModel<Register> registerModel = new PropertyModel<Register>(this, "register");

        Form<?> form = new Form("form");
        form.add(new DropDownChoice("councilDuration", councilDurationModel, councilDurationService.findAll()).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("course", courseModel, new CourseListModel(courseService)).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("register", registerModel, new RegisterListModel(registerService)));
        form.add(new DropDownChoice("status", statusModel, Arrays.asList(new String[]{"Active", "InActive"}))
                .setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("contentType", contentTypeModel, Arrays.asList(ContentType.values())));
        add(form);

        form.add(new Link("returnLink") {

            public void onClick() {

                setResponsePage(pageReference.getPage());
            }
        });

        form.add(new Button("processList") {

            @Override
            public void onSubmit() {

                register = registerModel.getObject();

                if (StringUtils.isEmpty(status)) {

                    error("Select Status of Report-  Active or In Active");

                } else {

                    boolean isActive = status.contentEquals("Active");
                    printEmploymentReportAsUngroupedList(isActive, register);
                }

            }
        });

        form.add(new Button("processGroupedByEmployerOrInstitution") {

            @Override
            public void onSubmit() {

                register = registerModel.getObject();

                if (StringUtils.isEmpty(status)) {

                    error("Select Status of Report-  Active or In Active");

                } else {

                    boolean isActive = status.contentEquals("Active");
                    printEmploymentReportGroupedByEmployerOrInstitution(isActive, register);
                }

            }
        });

        add(new FeedbackPanel("feedback"));
    }

    public void printEmploymentReportAsUngroupedList(Boolean active, Register register) {

        Map parameters = new HashMap();
        RegistrantEmploymentDataDBReport registrantDataDBReport = new RegistrantEmploymentDataDBReport();

        try {

            List<RegistrantData> registrants = new ArrayList<RegistrantData>();
            parameters.put("comment", course.getName() + " Detailed Register. " + status + " - " + councilDuration.getName() + " Period. All Sectors.");
            Set<Register> registers = new HashSet<Register>();

            if (register == null) {
                registers.addAll(registerService.findAll());
                registers.remove(generalParametersService.get().getStudentRegister());
                registers.remove(generalParametersService.get().getInstitutionRegister());

            } else {
                registers.add(register);
            }

            List<Duration> durations = new ArrayList<Duration>(councilDurationService.get(councilDuration.getId()).getDurations());

            for (EmploymentType e : EmploymentType.values()) {

                for (Register r : registers) {

                    registrants.addAll(registrantActivityService.getListPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(course, r, durations, RegistrantActivityType.RENEWAL, active, e, null));
                }
            }

            ReportResourceUtils.processJavaBeanReport(registrantDataDBReport, registrants, parameters);

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

    }

    public void printEmploymentReportGroupedByEmployerOrInstitution(Boolean active, Register register) {

        Map parameters = new HashMap();
        RegistrantEmploymentInstitutionDataDBReport registrantEmploymentInstitutionDataDBReport = new RegistrantEmploymentInstitutionDataDBReport();
        try {

            List<RegistrantData> registrants = new ArrayList<RegistrantData>();
            parameters.put("comment", course.getName() + " Detailed Register. " + status + " - " + councilDuration.getName() + " Period. All Sectors.");
            Set<Register> registers = new HashSet<Register>();

            if (register == null) {
                registers.addAll(registerService.findAll());
                registers.remove(generalParametersService.get().getStudentRegister());
                registers.remove(generalParametersService.get().getInstitutionRegister());

            } else {
                registers.add(register);
            }

            List<Duration> durations = new ArrayList<Duration>(councilDurationService.get(councilDuration.getId()).getDurations());

            for (EmploymentType e : EmploymentType.values()) {

                for (Register r : registers) {

                    registrants.addAll(registrantActivityService.getListPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(course, r, durations, RegistrantActivityType.RENEWAL, active, e, null));
                }
            }

            parameters.put("list", ReportResourceUtils.getRegistrantIdListFromRegistrantList(registrants));
            Connection connection = dataSource.getConnection();

            try {

                ReportResourceUtils.processSQLReport(registrantEmploymentInstitutionDataDBReport,
                        connection, parameters);

            } finally {
                connection.close();
            }

        } catch (JRException ex) {
            ex.printStackTrace(System.out);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }

    }

}

//~ Formatted by Jindent --- http://www.jindent.com
