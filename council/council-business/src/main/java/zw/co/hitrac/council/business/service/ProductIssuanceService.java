package zw.co.hitrac.council.business.service;

import java.util.Date;
import java.util.List;
import zw.co.hitrac.council.business.domain.ProductIssuance;
import zw.co.hitrac.council.business.domain.ProductIssuanceType;
import zw.co.hitrac.council.business.domain.Registrant;

/**
 *
 * @author Edward Zengeni
 * @author Michael Matiashe
 */
public interface ProductIssuanceService extends IGenericService<ProductIssuance> {

    public List<ProductIssuance> getProductIssuances(Registrant registrant);

    public List<ProductIssuance> getProductIssuances(ProductIssuanceType productIssuanceType);
    
    public List<ProductIssuance> getProductIssuances(ProductIssuanceType productIssuanceType, Registrant registrant);
    
    public List<ProductIssuance> getProductAllIssuances(Registrant registrant);
    
    public List<ProductIssuance> getProductUnClosedIssuances(ProductIssuanceType productIssuanceType);

    public List<ProductIssuance> getProductIssuances(ProductIssuanceType certificateType, Date startDate, Date endDate);

    public List<ProductIssuance> getProductIssuanceList(String query, String productNumber);

    public List<ProductIssuance> getUnIssuedPractisingCertificates(Registrant registrant);




}
