
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------
import com.googlecode.wicket.jquery.ui.panel.JQueryFeedbackPanel;
import java.io.ByteArrayOutputStream;
import net.sf.jasperreports.engine.JRException;

import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationService;
import zw.co.hitrac.council.business.service.examinations.ExamSettingService;
import zw.co.hitrac.council.reports.ExaminationMarkerReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

//~--- JDK imports ------------------------------------------------------------
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import zw.co.hitrac.council.business.domain.ModulePaper;
import zw.co.hitrac.council.business.domain.examinations.Exam;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.ModulePaperService;
import zw.co.hitrac.council.business.service.examinations.ExamService;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.reports.ExaminationMarkerThreeReport;
import zw.co.hitrac.council.reports.PaperMarkerSheet;
import zw.co.hitrac.council.web.models.CourseListModel;
import zw.co.hitrac.council.web.models.ExamSettingActiveListModel;

/**
 *
 * @author tdhlakama
 */
public class ExaminationMarkerPage extends IExaminationsPage {

    @SpringBean
    private ExamRegistrationService examRegistrationService;
    @SpringBean
    private ExamSettingService examSettingService;
    @SpringBean
    private CourseService courseService;
    private Course course;
    private ExamSetting examSetting;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private ExamService examService;
    @SpringBean
    private ModulePaperService modulePaperService;

    public ExaminationMarkerPage() {
        final FeedbackPanel feedback = new JQueryFeedbackPanel("feedback");
        add(feedback.setOutputMarkupId(true));
        PropertyModel<ExamSetting> examSettingModel = new PropertyModel<ExamSetting>(this, "examSetting");
        PropertyModel<Course> courseModel = new PropertyModel<Course>(this, "course");
        Form<?> form = new Form("form");

        form.add(new DropDownChoice("examSetting", examSettingModel, new ExamSettingActiveListModel(examSettingService)).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("course", courseModel, new CourseListModel(courseService)).setRequired(true).add(new ErrorBehavior()));
        add(form);

        form.add(new Button("markerslist") {
            @Override
            public void onSubmit() {
                try {
                    List<ExamRegistration> examRegistrations = examRegistrationService.getAllExamCandidates(examSetting, course, null, null, null, null, null, null, Boolean.FALSE, null);

                    ExaminationMarkerReport examinationMarkerReport = new ExaminationMarkerReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    if (course.getQualification() != null) {
                        parameters.put("course", course.getQualification().getName());
                    } else {
                        parameters.put("course", course.getName());
                    }

                    if (examSetting != null) {
                        parameters.put("examSetting", examSetting.toString());
                    }

                    ByteArrayResource resource;

                    if (courseService.getNumberOfModulePapers(course) != 3) {
                        resource = ReportResourceUtils.getReportResource(new ExaminationMarkerReport(), contentType,
                                examRegistrations, parameters);
                    } else {
                        resource = ReportResourceUtils.getReportResource(new ExaminationMarkerThreeReport(), contentType,
                                examRegistrations, parameters);
                    }
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    Logger.getLogger(ExaminationMarkerPage.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });

        form.add(new Button("paper1") {
            @Override
            public void onSubmit() {
                try {
                    List<ExamRegistration> examRegistrations = examRegistrationService.getAllExamCandidates(examSetting, course, null, null, null, null, null, null, Boolean.FALSE, null);
                    if (examRegistrations.isEmpty()) {
                        this.info("No Data was found");
                    } else {
                        this.info("Data Found");

                        PaperMarkerSheet paperMarkersSheet = new PaperMarkerSheet();
                        ContentType contentType = ContentType.PDF;
                        Map parameters = new HashMap();

                        if (course.getQualification() != null) {
                            parameters.put("course", course.getQualification().getName());
                        } else {
                            parameters.put("course", course.getName());
                        }

                        if (examSetting != null) {
                            parameters.put("examSetting", examSetting.toString());
                        }

                        parameters.put("paper", "Paper One");

                        ByteArrayResource resource;

                        resource = ReportResourceUtils.getReportResource(paperMarkersSheet, contentType,
                                examRegistrations, parameters);

                        IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                RequestCycle.get().getResponse(), null);

                        resource.respond(a);

                        // To make Wicket stop processing form after sending response
                        RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                    }
                } catch (JRException ex) {
                    Logger.getLogger(ExaminationMarkerPage.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });

        form.add(new Button("paper2") {
            @Override
            public void onSubmit() {
                try {
                    List<ExamRegistration> examRegistrations = examRegistrationService.getAllExamCandidates(examSetting, course, null, null, null, null, null, null, Boolean.FALSE, null);
                    if (examRegistrations.isEmpty()) {
                        this.info("No Data was found");
                    } else {
                        this.info("Data Found");

                        PaperMarkerSheet paperMarkersSheet = new PaperMarkerSheet();
                        ContentType contentType = ContentType.PDF;
                        Map parameters = new HashMap();

                        if (course.getQualification() != null) {
                            parameters.put("course", course.getQualification().getName());
                        } else {
                            parameters.put("course", course.getName());
                        }

                        if (examSetting != null) {
                            parameters.put("examSetting", examSetting.toString());
                        }

                        parameters.put("paper", "Paper Two");

                        ByteArrayResource resource;

                        resource = ReportResourceUtils.getReportResource(paperMarkersSheet, contentType,
                                examRegistrations, parameters);

                        IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                RequestCycle.get().getResponse(), null);

                        resource.respond(a);

                        // To make Wicket stop processing form after sending response
                        RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                    }
                } catch (JRException ex) {
                    Logger.getLogger(ExaminationMarkerPage.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });

        form.add(new Button("paper3") {
            @Override
            public void onSubmit() {
                try {
                    List<ExamRegistration> examRegistrations = examRegistrationService.getAllExamCandidates(examSetting, course, null, null, null, null, null, null, Boolean.FALSE, null);
                    if (examRegistrations.isEmpty()) {
                        this.info("No Data was found");
                    } else {
                        this.info("Data Found");

                        PaperMarkerSheet paperMarkersSheet = new PaperMarkerSheet();
                        ContentType contentType = ContentType.PDF;
                        Map parameters = new HashMap();

                        if (course.getQualification() != null) {
                            parameters.put("course", course.getQualification().getName());
                        } else {
                            parameters.put("course", course.getName());
                        }

                        if (examSetting != null) {
                            parameters.put("examSetting", examSetting.toString());
                        }

                        parameters.put("paper", "Paper Three");

                        ByteArrayResource resource;

                        resource = ReportResourceUtils.getReportResource(paperMarkersSheet, contentType,
                                examRegistrations, parameters);

                        IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                RequestCycle.get().getResponse(), null);

                        resource.respond(a);

                        // To make Wicket stop processing form after sending response
                        RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                    }
                } catch (JRException ex) {
                    Logger.getLogger(ExaminationMarkerPage.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });

        form.add(new Button("markersListXls") {
            @Override
            public void onSubmit() {
                try {
                    List<ExamRegistration> examRegistrations = examRegistrationService.getAllExamCandidates(examSetting, course, null, null, null, null, null, null, Boolean.FALSE, null);
                    HSSFWorkbook wb = null;
                    if (courseService.getNumberOfModulePapers(course) == 3) {
                        //worksheet accommodating three papers
                        wb = createWorkBookThree(examRegistrations, course, examSetting);
                    } else {
                        wb = createWorkBook(examRegistrations, course, examSetting);
                    }
                    ByteArrayOutputStream os = new ByteArrayOutputStream();
                    wb.write(os);
                    ByteArrayResource resource = new ByteArrayResource(ContentType.EXCEL.getContentTypeString(), os.toByteArray(), "Examination Result Sheet.xls");
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                    os.close();
                } catch (Exception ex) {
                    this.error(ex.getMessage());
                }
            }
        });

    }

    public HSSFWorkbook createWorkBook(List<ExamRegistration> examRegistrations, Course course, ExamSetting examSetting) {

        String statement = "";

        Set<ModulePaper> modulePapers = new HashSet<ModulePaper>();

        //filter module papers for course and exam settings
        for (ModulePaper modulePaper : examService.findExamPapers(examSetting)) {
            if (modulePaperService.findPapersInCourse(course).contains(modulePaper)) {
                modulePapers.add(modulePaper);
            }
        }
        //filter exams needed settings
        for (Exam exam : examService.findAll(examSetting)) {
            if (modulePapers.contains(exam.getModulePaper())) {
                statement = statement.concat(exam.getExamDate().getDate() + DateUtil.getOrdinalSuffix(exam.getExamDate().getDate())) + " / ";
            }
        }
        if (statement == null || statement.length() == 0) {
            statement = " / ";
        }
        statement = statement.substring(0, statement.length() - 3);
        if (course.getQualification() != null) {
            statement = course.getQualification().getName() + " Results " + statement.concat(" " + examSetting.toString());
        } else {
            statement = course.getName() + " Results " + statement.concat(" " + examSetting.toString());
        }

        int rowIndex = 0; //rowIndex
        HSSFWorkbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet("Examination Markers Sheet");
        sheet.protectSheet("Examination Markers Sheet");
        CellStyle cs = wb.createCellStyle();
        CellStyle cs2 = wb.createCellStyle();
        cs2.setWrapText(true);
        CellStyle unlockedCellStyle = wb.createCellStyle();
        unlockedCellStyle.setLocked(false);

        Font f = wb.createFont();
        Font f2 = wb.createFont();
        //set font 1 to 12 point type
        f.setFontHeightInPoints((short) 14);
        f2.setFontHeightInPoints((short) 12);
        //arial is the default font
        f.setBoldweight(Font.BOLDWEIGHT_BOLD);
        f2.setBoldweight(Font.BOLDWEIGHT_BOLD);
        //set cell stlye
        cs.setFont(f);
        cs2.setFont(f2);

        Row row = sheet.createRow(rowIndex);
        row.createCell(0).setCellValue(generalParametersService.get().getCouncilName());
        row.getCell(rowIndex).setCellStyle(cs);
        sheet.addMergedRegion(new CellRangeAddress(
                rowIndex, //first row (0-based)
                rowIndex, //last row  (0-based)
                0, //first column (0-based)
                5 //last column  (0-based)
        ));
        rowIndex++;
        rowIndex++;
        row = sheet.createRow(rowIndex);
        row.createCell(0).setCellValue(statement);
        row.getCell(0).setCellStyle(cs2);
        sheet.addMergedRegion(new CellRangeAddress(
                rowIndex, //first row (0-based)
                rowIndex, //last row  (0-based)
                0, //first column (0-based)
                5 //last column  (0-based)
        ));
        rowIndex++;
        rowIndex++;
        row = sheet.createRow(rowIndex);
        row.createCell(0).setCellValue("Candidate Number ");
        row.getCell(0).setCellStyle(cs2);
        row.createCell(1).setCellValue("Paper 1");
        row.getCell(1).setCellStyle(cs2);
        row.createCell(2).setCellValue("Paper 2");
        row.getCell(2).setCellStyle(cs2);
        row.createCell(3).setCellValue("Total");
        row.getCell(3).setCellStyle(cs2);
        row.createCell(4).setCellValue("PASS/FAIL        ");
        row.getCell(4).setCellStyle(cs2);
        rowIndex++;
        int ii = rowIndex;
        for (ExamRegistration obj : examRegistrations) {
            if (obj.getShow()) {
                ii = ii + 1;
                row = sheet.createRow(rowIndex++);
                row.createCell(0).setCellValue(obj.getCandidateNumber());
                row.createCell(1).setCellValue(0.0);
                row.getCell(1).setCellStyle(unlockedCellStyle);
                row.createCell(2).setCellValue(0.0);
                row.getCell(2).setCellStyle(unlockedCellStyle);
                row.createCell(3).setCellFormula("B" + ii + "+C" + ii);
                row.createCell(4).setCellValue("");
                row.getCell(4).setCellStyle(unlockedCellStyle);
            }
        }
        for (int i = 0; i < 11; i++) {
            sheet.autoSizeColumn(i);
        }

        wb.writeProtectWorkbook("nczPaZweordM!", "NCZ");
        return wb;
    }

    public HSSFWorkbook createWorkBookThree(List<ExamRegistration> examRegistrations, Course course, ExamSetting examSetting) {

        String statement = "";

        Set<ModulePaper> modulePapers = new HashSet<ModulePaper>();

        //filter module papers for course and exam settings
        for (ModulePaper modulePaper : examService.findExamPapers(examSetting)) {
            if (modulePaperService.findPapersInCourse(course).contains(modulePaper)) {
                modulePapers.add(modulePaper);
            }
        }
        //filter exams needed settings
        for (Exam exam : examService.findAll(examSetting)) {
            if (modulePapers.contains(exam.getModulePaper())) {
                statement = statement.concat(exam.getExamDate().getDate() + DateUtil.getOrdinalSuffix(exam.getExamDate().getDate())) + " / ";
            }
        }
        if (statement == null || statement.length() == 0) {
            statement = " / ";
        }
        statement = statement.substring(0, statement.length() - 3);
        if (course.getQualification() != null) {
            statement = course.getQualification().getName() + " Results " + statement.concat(" " + examSetting.toString());
        } else {
            statement = course.getName() + " Results " + statement.concat(" " + examSetting.toString());
        }

        int rowIndex = 0; //rowIndex
        HSSFWorkbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet("Examination Markers Sheet");
        sheet.protectSheet("Examination Markers Sheet");
        CellStyle cs = wb.createCellStyle();
        CellStyle cs2 = wb.createCellStyle();
        cs2.setWrapText(true);
        CellStyle unlockedCellStyle = wb.createCellStyle();
        unlockedCellStyle.setLocked(false);

        Font f = wb.createFont();
        Font f2 = wb.createFont();
        //set font 1 to 12 point type
        f.setFontHeightInPoints((short) 14);
        f2.setFontHeightInPoints((short) 12);
        //arial is the default font
        f.setBoldweight(Font.BOLDWEIGHT_BOLD);
        f2.setBoldweight(Font.BOLDWEIGHT_BOLD);
        //set cell stlye
        cs.setFont(f);
        cs2.setFont(f2);

        Row row = sheet.createRow(rowIndex);
        row.createCell(0).setCellValue(generalParametersService.get().getCouncilName());
        row.getCell(rowIndex).setCellStyle(cs);
        sheet.addMergedRegion(new CellRangeAddress(
                rowIndex, //first row (0-based)
                rowIndex, //last row  (0-based)
                0, //first column (0-based)
                6 //last column  (0-based)
        ));
        rowIndex++;
        rowIndex++;
        row = sheet.createRow(rowIndex);
        row.createCell(0).setCellValue(statement);
        row.getCell(0).setCellStyle(cs2);
        sheet.addMergedRegion(new CellRangeAddress(
                rowIndex, //first row (0-based)
                rowIndex, //last row  (0-based)
                0, //first column (0-based)
                6 //last column  (0-based)
        ));
        rowIndex++;
        rowIndex++;
        row = sheet.createRow(rowIndex);
        row.createCell(0).setCellValue("Candidate Number ");
        row.getCell(0).setCellStyle(cs2);
        row.createCell(1).setCellValue("Paper 1");
        row.getCell(1).setCellStyle(cs2);
        row.createCell(2).setCellValue("Paper 2");
        row.getCell(2).setCellStyle(cs2);
        row.createCell(3).setCellValue("Paper 3");
        row.getCell(3).setCellStyle(cs2);
        row.createCell(4).setCellValue("Total");
        row.getCell(4).setCellStyle(cs2);
        row.createCell(5).setCellValue("PASS/FAIL        ");
        row.getCell(5).setCellStyle(cs2);
        rowIndex++;
        int ii = rowIndex;
        for (ExamRegistration obj : examRegistrations) {
            if (obj.getShow()) {
                ii = ii + 1;
                row = sheet.createRow(rowIndex++);
                row.createCell(0).setCellValue(obj.getCandidateNumber());
                row.createCell(1).setCellValue(0.0);
                row.getCell(1).setCellStyle(unlockedCellStyle);
                row.createCell(2).setCellValue(0.0);
                row.getCell(2).setCellStyle(unlockedCellStyle);
                row.createCell(3).setCellValue(0.0);
                row.getCell(3).setCellStyle(unlockedCellStyle);
                row.createCell(3).setCellFormula("B" + ii + "+C" + ii+ "+D" + ii);
                row.createCell(5).setCellValue("");
                row.getCell(5).setCellStyle(unlockedCellStyle);
            }
        }
        for (int i = 0; i < 11; i++) {
            sheet.autoSizeColumn(i);
        }

        wb.writeProtectWorkbook("nczPaZweordM!", "NCZ");
        return wb;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
