
package zw.co.hitrac.council.business.dao;

import zw.co.hitrac.council.business.domain.Citizenship;

/**
 *
 * @author Edward Zengeni
 */
public interface CitizenshipDAO extends IGenericDAO<Citizenship>{
    
}
