package zw.co.hitrac.council.web.pages.security;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.User;
import zw.co.hitrac.council.business.service.UserService;
import zw.co.hitrac.council.web.pages.accounts.payments.ItemUserPaymentPage;
import zw.co.hitrac.council.web.pages.configure.IAdministerDatabaseBasePage;

/**
 *
 * @author Clive Gurure
 */
public class UserViewPage extends IAdministerDatabaseBasePage {
    @SpringBean
    UserService userService;

    public UserViewPage(Long id) {
        CompoundPropertyModel<User> model =
            new CompoundPropertyModel<User>(new UserViewPage.LoadableDetachableUserModel(id));

        setDefaultModel(model);
        add(new Link<User>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new UserEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<User>("returnLink", UserListPage.class));
        add(new Label("username"));
        add(new Label("firstName"));
        add(new Label("surname"));
        add(new Label("printerName"));        
        add(new Label("email"));
        add(new Label("roles"));
    }

    private final class LoadableDetachableUserModel extends LoadableDetachableModel<User> {
        private long id;

        public LoadableDetachableUserModel(long id) {
            this.id = id;
        }

        @Override
        protected User load() {
            return userService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
