/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.examinations;

import zw.co.hitrac.council.business.domain.examinations.ExamPeriod;
import zw.co.hitrac.council.business.service.IGenericService;

/**
 *
 * @author tidza
 */
public interface ExamPeriodService extends IGenericService<ExamPeriod> {
    
}
