package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.ModulePaper;
import zw.co.hitrac.council.business.service.ModulePaperService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;

/**
 *
 * @author Matiashe Michael
 */
public class ModulePaperListPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private ModulePaperService modulePaperService;

    public ModulePaperListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new ModulePaperEditPage(null));
            }
        });

        IModel<List<ModulePaper>> model = new LoadableDetachableModel<List<ModulePaper>>() {
            @Override
            protected List<ModulePaper> load() {
                return modulePaperService.findAll();
            }
        };
        PropertyListView<ModulePaper> eachItem = new PropertyListView<ModulePaper>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<ModulePaper> item) {
                Link<ModulePaper> viewLink = new Link<ModulePaper>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new ModulePaperViewPage(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(viewLink);
                viewLink.add(new Label("name"));
                item.add(new Label("position")); 
                item.add(new Label("course")); 
            }
        };

        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
