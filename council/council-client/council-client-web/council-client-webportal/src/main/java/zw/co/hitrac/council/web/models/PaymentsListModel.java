package zw.co.hitrac.council.web.models;

import java.util.List;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.accounts.PaymentDetails;
import zw.co.hitrac.council.business.service.accounts.PaymentDetailsService;

/**
 *
 * @author Michael Matiashe
 */
public class PaymentsListModel extends LoadableDetachableModel<List<PaymentDetails>> {
    
    private final PaymentDetailsService paymentDetailsService;

    public PaymentsListModel(PaymentDetailsService paymentDetailsService) {
        this.paymentDetailsService = paymentDetailsService;
    }
    
    @Override
    protected List<PaymentDetails> load() {
      return this.paymentDetailsService.findAll();
    }
    
}
