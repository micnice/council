package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.SMSMessageDAO;
import zw.co.hitrac.council.business.dao.repo.SMSMessageRepository;
import zw.co.hitrac.council.business.domain.SMSMessage;

/**
 *
 * @author Michael Matiashe
 */
@Repository
public class SMSMessageDAOImpl implements SMSMessageDAO{
   @Autowired
   private SMSMessageRepository sMSMessageRepository;
    
   public SMSMessage save(SMSMessage sMSMessage){
       return sMSMessageRepository.save(sMSMessage);
   }
   
   public List<SMSMessage> findAll(){
       return sMSMessageRepository.findAll();
   }
   
   public SMSMessage get(Long id){
       return sMSMessageRepository.findOne(id);
   }
 /**
     * A setter method that will make mocking repo object easier
     * @param sMSMessageRepository 
     */
   
   public void setSMSMessageRepository(SMSMessageRepository sMSMessageRepository) {
         this.sMSMessageRepository = sMSMessageRepository;
    }

    public List<SMSMessage> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
