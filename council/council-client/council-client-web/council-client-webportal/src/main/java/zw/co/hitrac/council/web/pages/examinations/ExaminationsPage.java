
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.link.BookmarkablePageLink;

/**
 *
 * @author tdhlakama
 */
public class ExaminationsPage extends IExaminationsPage {

    public ExaminationsPage() {
        add(new BookmarkablePageLink<Void>("examRegistrationSearchPage", ExamRegistrationSearchPage.class));
        add(new BookmarkablePageLink<Void>("examinationMarkerPage", ExaminationMarkerPage.class));
        add(new BookmarkablePageLink<Void>("examinationCardPage", ExaminationCardPage.class));
        add(new BookmarkablePageLink<Void>("examinationConfirmationPage", ExaminationConfirmationPage.class));
        add(new BookmarkablePageLink<Void>("examinationMasterPage", ExaminationMasterPage.class));

    }
}


//~ Formatted by Jindent --- http://www.jindent.com
