package zw.co.hitrac.council.business.domain.reports;

import java.util.Comparator;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;

/**
 *
 * @author tdhlakama
 */
public class NameComparator implements Comparator<ExamRegistration> {

    @Override
    public int compare(ExamRegistration o1, ExamRegistration o2) {
        return o1.getRegistration().getRegistrant().getFullname().compareTo(o2.getRegistration().getRegistrant().getFullname());
    }
}
