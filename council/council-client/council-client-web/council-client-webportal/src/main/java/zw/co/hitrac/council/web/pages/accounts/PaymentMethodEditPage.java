package zw.co.hitrac.council.web.pages.accounts;

import org.apache.wicket.markup.html.form.CheckBox;
import zw.co.hitrac.council.web.pages.configure.*;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.PaymentMethod;
import zw.co.hitrac.council.business.service.accounts.PaymentMethodService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Charles Chigoriwa
 */
public class PaymentMethodEditPage extends IBankBookPage{
    
    @SpringBean
    private PaymentMethodService paymentMethodService;

    public PaymentMethodEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<PaymentMethod>(new LoadableDetachablePaymentMethodModel(id)));
        Form<PaymentMethod> form=new Form<PaymentMethod>("form",(IModel<PaymentMethod>)getDefaultModel()) {
            @Override
            public void onSubmit(){
                paymentMethodService.save(getModelObject());
                setResponsePage(new PaymentMethodViewPage(getModelObject().getId()));
            }
        };
        
        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior())); 
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new CheckBox("receiptNumberRequired"));
        form.add(new CheckBox("depositDateRequired"));
        form.add(new BookmarkablePageLink<Void>("returnLink",PaymentMethodListPage.class));
        add(form);
    }
    
    
    private final class LoadableDetachablePaymentMethodModel extends LoadableDetachableModel<PaymentMethod> {

        private Long id;

        public LoadableDetachablePaymentMethodModel(Long id) {
            this.id = id;
        }

        @Override
        protected PaymentMethod load() {
            if(id==null){
                return new PaymentMethod();
            }
            return paymentMethodService.get(id);
        }
    }
    
    
}
