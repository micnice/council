package zw.co.hitrac.council.business.domain.accounts;

import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.domain.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;


/**
 *
 * @author Michael Matiashe
 */
@Entity
@Table(name="paymenttype")
@Audited
public class PaymentType extends BaseEntity {
    
}
