package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.PaymentConfiguration;
import zw.co.hitrac.council.business.service.PaymentConfigurationService;
import zw.co.hitrac.council.web.utility.CustomDateLabel;

/**
 *
 * @author charlesc
 */
public class PaymentConfigurationViewPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private PaymentConfigurationService paymentConfigurationService;

    public PaymentConfigurationViewPage(Long id) {
        CompoundPropertyModel<PaymentConfiguration> model =
            new CompoundPropertyModel<PaymentConfiguration>(
                new PaymentConfigurationViewPage.LoadableDetachablePaymentConfigurationModel(id));

        setDefaultModel(model);
        add(new Link<PaymentConfiguration>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new PaymentConfigurationEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", PaymentConfigurationListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("course"));
        add(new Label("typeOfService"));
        add(new  CustomDateLabel("dueDate"));
        add(new Label("paymentConfiguration", model.bind("name")));
    }

    private final class LoadableDetachablePaymentConfigurationModel
            extends LoadableDetachableModel<PaymentConfiguration> {
        private Long id;

        public LoadableDetachablePaymentConfigurationModel(Long id) {
            this.id = id;
        }

        @Override
        protected PaymentConfiguration load() {
            return paymentConfigurationService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
