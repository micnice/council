/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.examinations.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.examinations.ExamPeriodDao;
import zw.co.hitrac.council.business.domain.examinations.ExamPeriod;
import zw.co.hitrac.council.business.service.examinations.ExamPeriodService;

import java.util.List;

/**
 *
 * @author tidza
 */
@Service
@Transactional
public class ExamPeriodServiceImpl implements ExamPeriodService {

    @Autowired
    private ExamPeriodDao examPeriodDao;

    @Transactional
    public ExamPeriod save(ExamPeriod t) {
        return examPeriodDao.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ExamPeriod> findAll() {
        return examPeriodDao.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public ExamPeriod get(Long id) {
        return examPeriodDao.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ExamPeriod> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setExamPeriodDao(ExamPeriodDao examPeriodDao) {

        this.examPeriodDao = examPeriodDao;
    }
    
}
