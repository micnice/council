package zw.co.hitrac.council.web.pages.application;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.service.ApplicationService;
import zw.co.hitrac.council.business.service.RegistrantService;

import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.configure.QualificationViewPage;
import zw.co.hitrac.council.web.pages.registration.RegistrantDocumentPage;
import zw.co.hitrac.council.web.pages.registration.RegistrantSupportingDocumentPanel;
import zw.co.hitrac.council.web.pages.registration.RegistrantViewPage;
import zw.co.hitrac.council.web.utility.CustomDateLabel;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;

/**
 *
 * @author Michael Matiashe
 */
public class QualificationApplicationViewPage extends TemplatePage {

    @SpringBean
    private ApplicationService applicationService;
    @SpringBean
    private RegistrantService registrantService;

    public QualificationApplicationViewPage(long id) {

        menuPanelManager.getApplicationPanel().setTopMenuCurrent(true);
        final CompoundPropertyModel<Application> model = new CompoundPropertyModel<Application>(applicationService.get(id));
        final CompoundPropertyModel<Registrant> registrantModel = new CompoundPropertyModel<Registrant>(new DetachableRegistrantModel(model.getObject().getRegistrant().getId(), registrantService));

        setDefaultModel(model);

        add(new Label("approvedBy"));
        add(new CustomDateLabel("dateApproved"));
        add(new Label("applicationStatus"));
        add(new Label("qualification"));
        add(new CustomDateLabel("qualificationEndDate"));
        add(new CustomDateLabel("qualificationStartDate"));
        add(new Label("institution"));
        add(new Label("trainingInstitution"));
        add(new CustomDateLabel("dateAwarded"));
        add(new Label("applicationPurpose"));
        add(new CustomDateLabel("applicationDate"));
        add(new Label("comment"));
        add(new Link<Void>("registrationOptionPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });
        add(new Link<Application>("applicationApprovalPage", model) {
            @Override
            public void onClick() {
                setResponsePage(new QualificationApplicationPage(getModelObject().getId()));
            }

            @Override
            protected void onConfigure() {
                if (getModel().getObject().getApplicationStatus().equals(Application.APPLICATIONAPPROVED) || getModel().getObject().getApplicationStatus().equals(Application.APPLICATIONDECLINED)) {
                    setVisible(Boolean.FALSE);
                }
            }
        });
        add(new Label("currentStatus", model.getObject().getApplicationTextStatus()));

        add(new Link<Application>("registrantDocumentPage", model) {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantDocumentPage(getModelObject(), registrantModel));
            }
        });


        add(new Label("registrant.fullname"));
        add(new FeedbackPanel("feedback"));

        //supporting documents
        add(new RegistrantSupportingDocumentPanel("supportingDocumentPanel", null, model.getObject(), null, null, null, null, null));

    }
}


//~ Formatted by Jindent --- http://www.jindent.com
