package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.CourseCertificateNumber;
import zw.co.hitrac.council.business.domain.Register;

/**
 *
 * @author Charles Chigoriwa
 */
public interface CourseCertificateNumberService extends IGenericService<CourseCertificateNumber> {

    public CourseCertificateNumber get(Course course);

    public void resetCertificateNumbers();
    
    public CourseCertificateNumber get(Course course, Register register);
}
