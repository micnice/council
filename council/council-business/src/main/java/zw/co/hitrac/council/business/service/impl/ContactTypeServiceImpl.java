package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.ContactTypeDAO;
import zw.co.hitrac.council.business.domain.ContactType;
import zw.co.hitrac.council.business.service.ContactTypeService;

import java.util.List;

/**
 *
 * @author Charles Chigoriwa
 */
@Service
@Transactional
public class ContactTypeServiceImpl implements ContactTypeService{
    
    @Autowired
    private ContactTypeDAO contactTypeDAO;
    

    @Transactional
    public ContactType save(ContactType contactType) {
       return contactTypeDAO.save(contactType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ContactType> findAll() {
        return contactTypeDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public ContactType get(Long id) {
       return contactTypeDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ContactType> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setContactTypeDAO(ContactTypeDAO contactTypeDAO) {

        this.contactTypeDAO = contactTypeDAO;
    }
    
}
