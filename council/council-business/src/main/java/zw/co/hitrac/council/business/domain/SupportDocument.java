package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

/**
 *
 * @author tdhlakama
 */
@Entity
@Table(name = "supportdocument")
@Audited
public class SupportDocument extends BaseEntity implements Serializable {

    private String pathName;
    private String documentType;
    private Requirement requirement;
    private RegistrantDisciplinary registrantDisciplinary;
    private Application application;
    private Registrant registrant;
    private Institution institution;
    private ExamRegistration examRegistration;
    private Boolean submitted = Boolean.FALSE;

    @ManyToOne
    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }
    private RegistrantQualification registrantQualification;

    @ManyToOne
    public Registrant getRegistrant() {
        return registrant;
    }

    public void setRegistrant(Registrant registrant) {
        this.registrant = registrant;
    }

    @ManyToOne
    public Requirement getRequirement() {
        return requirement;
    }

    public void setRequirement(Requirement requirement) {
        this.requirement = requirement;
    }

    @ManyToOne
    public RegistrantDisciplinary getRegistrantDisciplinary() {
        return registrantDisciplinary;
    }

    public void setRegistrantDisciplinary(RegistrantDisciplinary registrantDisciplinary) {
        this.registrantDisciplinary = registrantDisciplinary;
    }

    @ManyToOne
    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public String getPathName() {
        return pathName;
    }

    public void setPathName(String pathName) {
        this.pathName = pathName;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    @ManyToOne
    public RegistrantQualification getRegistrantQualification() {
        return registrantQualification;
    }

    public void setRegistrantQualification(RegistrantQualification registrantQualification) {
        this.registrantQualification = registrantQualification;
    }

    @ManyToOne
    public ExamRegistration getExamRegistration() {
        return examRegistration;
    }

    public void setExamRegistration(ExamRegistration examRegistration) {
        this.examRegistration = examRegistration;
    }

    public Boolean getSubmitted() {
        return submitted;
    }

    public void setSubmitted(Boolean submitted) {
        this.submitted = submitted;
    }

}
