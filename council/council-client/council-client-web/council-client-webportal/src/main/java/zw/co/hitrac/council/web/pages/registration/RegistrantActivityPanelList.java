/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.registration;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantActivity;
import zw.co.hitrac.council.business.domain.RegistrantActivityType;
import zw.co.hitrac.council.business.service.RegistrantActivityService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.utility.CustomDateTimeLabel;

/**
 * @author kelvin
 */
public class RegistrantActivityPanelList extends Panel {

    @SpringBean
    private RegistrantActivityService registrantActivityService;

    public RegistrantActivityPanelList(String id, final IModel<Registrant> registrantModel) {
        super(id);

        add(new PropertyListView<RegistrantActivity>("registrantActivityList", registrantActivityService
                .getRegistrantActivities(registrantModel.getObject())) {
            @Override
            protected void populateItem(final ListItem<RegistrantActivity> item) {
                Link<RegistrantActivity> viewLink = new Link<RegistrantActivity>("registrantActivityLink", item
                        .getModel()) {
                    @Override
                    public void onClick() {
                        registrantActivityService.remove(item.getModelObject());
                        setResponsePage(new RegistrantActivityListPage(registrantModel.getObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                item.add(new Label("registrantActivityType"));
                item.add(new Label("duration"));
                item.add(new Label("startDate"));
                item.add(new Label("endDate"));
                item.add(new Label("createdBy"));
                item.add(new Label("modifiedBy"));
                item.add(CustomDateTimeLabel.forDate("dateCreated",
                        new PropertyModel<>(item.getModel(), "dateCreated")));
                item.add(CustomDateTimeLabel.forDate("dateModified",
                        new PropertyModel<>(item.getModel(), "dateModified")));
            }
        });

    }

    public RegistrantActivityPanelList(String id, final IModel<Registrant> registrantModel, RegistrantActivityType
            registrantActivityType) {
        super(id);

        add(new PropertyListView<RegistrantActivity>("registrantActivityList", registrantActivityService
                .getRegistrantActivities(registrantModel.getObject(), registrantActivityType)) {
            @Override
            protected void populateItem(final ListItem<RegistrantActivity> item) {
                Link<RegistrantActivity> viewLink = new Link<RegistrantActivity>("registrantActivityLink", item
                        .getModel()) {
                    @Override
                    public void onClick() {
                        registrantActivityService.remove(item.getModelObject());
                        setResponsePage(new RegistrantActivityListPage(registrantModel.getObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                item.add(new Label("registrantActivityType"));
                item.add(new Label("duration"));
                item.add(new Label("startDate"));
                item.add(new Label("endDate"));
                item.add(new Label("createdBy"));
                item.add(new Label("modifiedBy"));
                item.add(CustomDateTimeLabel.forDate("dateCreated",
                        new PropertyModel<>(item.getModel(), "dateCreated")));
                item.add(CustomDateTimeLabel.forDate("dateModified",
                        new PropertyModel<>(item.getModel(), "dateModified")));
            }
        });

    }
}
