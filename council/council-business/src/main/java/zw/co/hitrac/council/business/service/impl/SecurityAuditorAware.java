package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import zw.co.hitrac.council.business.domain.User;
import zw.co.hitrac.council.business.service.UserService;


/**
 * Created by scott on 19/02/16.
 */

@Service(value = "securityAuditorAware")
@Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
public class SecurityAuditorAware implements AuditorAware<User> {

    @Autowired
    private UserService userService;

    @Override
    public User getCurrentAuditor() {

        return userService.getCurrentUser().orElse(null);
    }
}
