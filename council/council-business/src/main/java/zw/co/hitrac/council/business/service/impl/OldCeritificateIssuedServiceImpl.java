/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.OldCertificateIssuedDao;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.OldCertificateIssued;
import zw.co.hitrac.council.business.service.OldCertificateIssuedService;

/**
 *
 * @author kelvin
 */
@Service
@Transactional
public class OldCeritificateIssuedServiceImpl implements OldCertificateIssuedService {

    @Autowired
    private OldCertificateIssuedDao oldCertificateIssuedDAO;

    @Transactional
    public OldCertificateIssued save(OldCertificateIssued oldCertificateIssued) {
        return oldCertificateIssuedDAO.save(oldCertificateIssued);
    }

    public List<OldCertificateIssued> findAll() {
        return oldCertificateIssuedDAO.findAll();
    }

    public OldCertificateIssued get(Long id) {
        return oldCertificateIssuedDAO.get(id);
    }

    public void setOldCertificateIssuedDAO(OldCertificateIssuedDao oldCertificateIssuedDAO) {
        this.oldCertificateIssuedDAO = oldCertificateIssuedDAO;
    }

    public List<OldCertificateIssued> getCertificates(Registrant registrant) {
        return oldCertificateIssuedDAO.getCertificates(registrant);
    }

    public OldCertificateIssued getCertificate(Long certificateProcessRecordID) {
        return oldCertificateIssuedDAO.getCertificate(certificateProcessRecordID);
    }

    public List<OldCertificateIssued> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
