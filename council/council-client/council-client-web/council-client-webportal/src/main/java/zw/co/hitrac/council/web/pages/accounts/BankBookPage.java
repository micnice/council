/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.accounts;

import org.apache.wicket.markup.html.link.BookmarkablePageLink;

/**
 *
 * @author tdhlakama
 */
public class BankBookPage extends IBankBookPage {

    public BankBookPage() {
        add(new BookmarkablePageLink<Void>("bankAccountLink", BankAccountListPage.class));
        add(new BookmarkablePageLink<Void>("paymentTypeLink", PaymentTypeListPage.class));
         add(new BookmarkablePageLink<Void>("paymentMethodLink", PaymentMethodListPage.class));
    }
}