package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author Charles Chigoriwa
 */
public class ConfigureSystemPage extends TemplatePage {

    public ConfigureSystemPage() {
        menuPanelManager.getConfigureMenuPanel().setTopMenuCurrent(true);
        add(new BookmarkablePageLink<Void>("administerDatabaseLink",AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("reportViewsLink",ReportViewsPage.class));
    }
    
    
    
}
