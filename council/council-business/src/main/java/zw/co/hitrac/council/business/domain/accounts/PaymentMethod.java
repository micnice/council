package zw.co.hitrac.council.business.domain.accounts;

import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.domain.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 *
 * @author Matiashe Michael
 */
@Entity
@Table(name = "paymentmethod")
@Audited
public class PaymentMethod extends BaseEntity {

    private Boolean receiptNumberRequired;
    private Boolean depositDateRequired;

    public Boolean getReceiptNumberRequired() {
        if (receiptNumberRequired == null) {
            return Boolean.FALSE;
        }
        return receiptNumberRequired;
    }

    public void setReceiptNumberRequired(Boolean receiptNumberRequired) {
        this.receiptNumberRequired = receiptNumberRequired;
    }

    public Boolean getDepositDateRequired() {
        return depositDateRequired;
    }

    public void setDepositDateRequired(Boolean depositDateRequired) {
        this.depositDateRequired = depositDateRequired;
    }
}
