
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.accounts.payments;

//~--- non-JDK imports --------------------------------------------------------
import java.util.ArrayList;
import zw.co.hitrac.council.web.pages.examinations.*;
import net.sf.jasperreports.engine.JRException;

import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationService;
import zw.co.hitrac.council.business.service.examinations.ExamSettingService;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

//~--- JDK imports ------------------------------------------------------------
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.accounts.ProductGroup;
import zw.co.hitrac.council.business.domain.accounts.ReceiptItem;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistrationDebtComponent;
import zw.co.hitrac.council.business.domain.reports.ProductSale;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.service.accounts.CustomerService;
import zw.co.hitrac.council.business.service.accounts.ProductService;
import zw.co.hitrac.council.business.service.accounts.ReceiptItemService;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationDebtComponentService;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.reports.ExaminationPaymentReport;
import zw.co.hitrac.council.web.models.CourseListModel;
import zw.co.hitrac.council.web.models.ExamSettingActiveListModel;
import zw.co.hitrac.council.web.models.ExamSettingListModel;

/**
 *
 * @author tdhlakama
 */
public class ExaminationPaymentPage extends IExaminationsPage {
    
    @SpringBean
    private ExamRegistrationService examRegistrationService;
    @SpringBean
    private ExamSettingService examSettingService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private CourseService courseService;
    private Institution institution;
    private ExamSetting examSetting;
    private String idNumber;
    private Course course;
    private ProductGroup productGroup;
    @SpringBean
    private CustomerService customerService;
    @SpringBean
    ReceiptItemService receiptItemService;
    @SpringBean
    RegistrationService registrationService;
    @SpringBean
    private ProductService productService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private RegistrationProcess registrationProcess;
    @SpringBean
    private ExamRegistrationDebtComponentService examRegistrationDebtComponentService;
    private HrisComparator hrisComparator = new HrisComparator();
    
    public ExaminationPaymentPage() {
        PropertyModel<Institution> institutionModel = new PropertyModel<Institution>(this, "institution");
        PropertyModel<ExamSetting> examSettingModel = new PropertyModel<ExamSetting>(this, "examSetting");
        PropertyModel<Course> courseModel = new PropertyModel<Course>(this, "course");
        Form<?> form = new Form("form") {
            @Override
            protected void onSubmit() {
                try {
                    List<ProductSale> productSales = new ArrayList<ProductSale>();
                    for (ExamRegistrationDebtComponent component : examRegistrationDebtComponentService.getDebtComponents(examSetting, course, institution)) {
                        for (ReceiptItem r : receiptItemService.get(component.getDebtComponent())) {
                            ProductSale productSale = new ProductSale();
                            productSale.setDebtComponent(r.getDebtComponent());
                            productSale.setPaymentDetail(r.getReceiptHeader().getPaymentDetails());
                            productSale.setRegistrant(component.getExamRegistration().getRegistration().getRegistrant());
                            productSale.setInstitution(component.getExamRegistration().getInstitution());
                            productSale.setAmountPaid(r.getAmount());
                            productSales.add(productSale);
                        }
                    }
                    
                    ExaminationPaymentReport examinationPaymentReport = new ExaminationPaymentReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();
                    
                    if (course != null) {
                        parameters.put("course", course.getName());
                    }
                    
                    if (examSetting != null) {
                        parameters.put("examSetting", examSetting.toString());
                    }
                    
                    if (institution != null) {
                        parameters.put("institution", institution.toString());
                    }
                    
                    ByteArrayResource resource;
                    
                    resource = ReportResourceUtils.getReportResource(examinationPaymentReport, contentType, productSales, parameters);
                    
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);
                    
                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    error(ex.getMessage());
                }
            }
        };
        
        form.add(new DropDownChoice("institution", institutionModel, new InstitutionsModel()).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("examSetting", examSettingModel, new ExamSettingListModel(examSettingService)).setRequired(true).add(new ErrorBehavior()));
        
        form.add(new DropDownChoice("course", courseModel, new CourseListModel(courseService), new ChoiceRenderer<Course>()) {
            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }
            
            @Override
            public boolean isRequired() {
                return true;
            }
        }.add(new ErrorBehavior()));
        add(form);        
    }
    
    private class InstitutionsModel extends LoadableDetachableModel<List<? extends Institution>> {
        
        protected List<? extends Institution> load() {
            if (course != null) {
                return (List<Institution>) hrisComparator.sort((new ArrayList<Institution>(courseService.get(course.getId()).getTrainingInstitutions())));
            } else {
                return new ArrayList<Institution>();
            }
        }
    }
}



//~ Formatted by Jindent --- http://www.jindent.com
