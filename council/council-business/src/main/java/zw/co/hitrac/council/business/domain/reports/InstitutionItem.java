package zw.co.hitrac.council.business.domain.reports;

import zw.co.hitrac.council.business.domain.City;
import zw.co.hitrac.council.business.domain.District;
import zw.co.hitrac.council.business.domain.InstitutionCategory;
import zw.co.hitrac.council.business.domain.InstitutionType;
import zw.co.hitrac.council.business.domain.Province;

/**
 *
 * @author tdhlakama
 */
public class InstitutionItem {

    private InstitutionCategory institutionCategory;
    private InstitutionType institutionType;
    private Province province;
    private District district;
    private City city;
    private Long total = 0L;
    private String name;

    public InstitutionCategory getInstitutionCategory() {
        return institutionCategory;
    }

    public void setInstitutionCategory(InstitutionCategory institutionCategory) {
        this.institutionCategory = institutionCategory;
    }

    public InstitutionType getInstitutionType() {
        return institutionType;
    }

    public void setInstitutionType(InstitutionType institutionType) {
        this.institutionType = institutionType;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public String getName() {
        if (institutionCategory != null) {
            return institutionCategory.getName();
        }
        if (institutionType != null) {
            return institutionType.getName();
        }
        if (province != null) {
            return province.getName();
        }
        if (district != null) {
            return district.getName();
        }
        if (city != null) {
            return city.getName();
        }
        return "N/A";
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + (this.name != null ? this.name.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final InstitutionItem other = (InstitutionItem) obj;
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        return true;
    }

}
