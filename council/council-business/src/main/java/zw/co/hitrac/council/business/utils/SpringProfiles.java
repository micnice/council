package zw.co.hitrac.council.business.utils;

/**
 * Created by scott on 07/10/2016.
 */
public enum SpringProfiles {
  PROD("prod"),
  DEV("dev"),
  NCZ("ncz"),
  PCZ("pcz"),
  MEDLABS("medlabs"),
  AHPCZ("ahpcz"),
  EHPCZ("ehpcz"),
  REHAB("rehab");

  private String name;

  public String getName() {
    return this.name;
  }

  SpringProfiles(String name) {
    this.name = name;
  }
}
