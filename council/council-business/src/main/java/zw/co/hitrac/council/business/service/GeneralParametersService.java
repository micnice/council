package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.GeneralParameters;
import zw.co.hitrac.council.business.domain.RequiredParameter;

/**
 *
 * @author Edward Zengeni
 */

public interface GeneralParametersService extends IGenericService<GeneralParameters> {
    public GeneralParameters get();

}
