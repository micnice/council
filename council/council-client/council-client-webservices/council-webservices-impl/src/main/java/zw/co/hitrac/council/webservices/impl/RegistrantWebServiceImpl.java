package zw.co.hitrac.council.webservices.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.service.GenericService;
import zw.co.hitrac.council.business.service.RegistrantActivityService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.webservices.api.RegistrantWebService;
import zw.co.hitrac.council.webservices.api.domain.LeanRegistrant;

/**
 *
 * @author Daniel Nkhoma
 */
@Component
public class RegistrantWebServiceImpl implements RegistrantWebService {

    @Autowired
    private RegistrantService registrantService;
    @Autowired
    private GenericService genericService;
    @Autowired
    private RegistrantActivityService registrantActivityService;

    public List<Registrant> getRegistrants() {
        return registrantService.findAll();
    }

    public List<LeanRegistrant> getAllLean() {
        List<LeanRegistrant> leanRegistrants = new ArrayList<LeanRegistrant>();
        for (Long registrantId : genericService.getAllLean("Registrant")) {
            LeanRegistrant leanRegistrant = new LeanRegistrant();
            leanRegistrant.setRegistrantId(registrantId);
            leanRegistrants.add(leanRegistrant);
        }
        return leanRegistrants;
    }

    @Override
    public Registrant getRegistrant(Long id) {
        return registrantService.get(id);
    }

    @Override
    public Registrant getRegistrant(String registrationNumber) {
        return registrantService.getRegistrantByRegistrationNumber(registrationNumber);
    }

    @Override
    public List<CouncilDuration> getRegisteredRenewalPeriods(String registrationNumber) {
        return registrantActivityService.getRegisteredRenewalPeriods(registrationNumber);
    }

    @Override
    public CouncilDuration getLastCouncilDurationRenewal(String registrationNumber) {
        Registrant registrant = registrantService.getRegistrantByRegistrationNumber(registrationNumber);
        return registrantActivityService.getLastCouncilDurationRenewal(registrant);
    }
  
}
