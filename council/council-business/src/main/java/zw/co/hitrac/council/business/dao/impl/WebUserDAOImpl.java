/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.impl;

import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.jasypt.springsecurity3.authentication.encoding.PasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import zw.co.hitrac.council.business.dao.WebUserDAO;
import zw.co.hitrac.council.business.dao.repo.WebUserRepository;
import zw.co.hitrac.council.business.domain.WebUser;

/**
 * @author tdhlakama
 */
@Repository
public class WebUserDAOImpl implements WebUserDAO {

    @Autowired
    private PasswordEncoder encoder;
    @Autowired
    private WebUserRepository userRepository;
    @PersistenceContext
    private EntityManager entityManager;

    public WebUser save(WebUser user) {
        user.setPassword(encoder.encodePassword(user.getPassword(), null));
        return userRepository.save(user);
    }

    public List<WebUser> findAll() {
        return userRepository.findAll();
    }

    public WebUser get(Long id) {
        return userRepository.findOne(id);
    }

    @Override
    public WebUser get(String username, String password) {
        List<WebUser> users = entityManager.createQuery("select u from WebUser u where u.username=:username and u.retired=:retired")
                .setParameter("retired", Boolean.FALSE)
                .setParameter("username", username).getResultList();

        for (WebUser user : users) {
            try {
                if (encoder.isPasswordValid(user.getPassword(), password, null)) {
                    return user;
                }
            } catch (EncryptionOperationNotPossibleException ex) {
                System.out.println("An error occured during password hash retrieval.has the password been altered? " + ex.getMessage());
            }
        }

        return null;

    }

    @Override
    public WebUser getByRegistrationNumber(String registrationNumber) {
        List<WebUser> webUsers = entityManager.createQuery("select u from WebUser u where u.registrationNumber=:registrationNumber and u.retired=:retired")
                .setParameter("retired", Boolean.FALSE)
                .setParameter("registrationNumber", registrationNumber).getResultList();

        if (webUsers.isEmpty()) {
            return null;
        } else {
            return webUsers.get(0);
        }

    }

    @Override
    public WebUser getWebUserByUsername(String username) {

        List<WebUser> webUsers = entityManager.createQuery("select u from WebUser u where u.username=:username and u.retired=:retired")
                .setParameter("retired", Boolean.FALSE)
                .setParameter("username", username).getResultList();
        if (webUsers.isEmpty()) {
            return null;
        } else {
            return webUsers.get(0);
        }
    }

    @Override
    public String loginWebUser(String username, String password) {
        List<WebUser> users = entityManager.createQuery("select u from WebUser u where u.username=:username and u.retired=:retired")
                .setParameter("retired", Boolean.FALSE)
                .setParameter("username", username).getResultList();

        for (WebUser user : users) {
            try {
                if (encoder.isPasswordValid(user.getPassword(), password, null)) {
                    return user.getRegistrantNumber();
                }
            } catch (EncryptionOperationNotPossibleException ex) {
                return "fail";
            }
        }

        return "fail";

    }

    @Override
    public String createWebUser(String username, String password, String registrationNumber) {
        Optional<WebUser> webUserOptional = Optional.ofNullable(getWebUserByUsername(username));
        WebUser webUser = null;
        if (webUserOptional.isPresent()) {
            webUser = webUserOptional.get();
        } else {
            webUser = new WebUser();
            webUser.setUsername(username);
            webUser.setRegistrantNumber(registrationNumber);
        }
        webUser.setPassword(password);
        save(webUser);
        return "success";
    }

    @Override
    public void changeWebUserPassword(WebUser webUser, String password) {
        webUser.setPassword(password);
        save(webUser);
    }

    public void setWebUserRepository(WebUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<WebUser> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
