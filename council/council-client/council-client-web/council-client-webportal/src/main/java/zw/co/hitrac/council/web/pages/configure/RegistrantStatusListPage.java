package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.RegistrantStatus;
import zw.co.hitrac.council.business.service.RegistrantStatusService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;

/**
 *
 * @author Michael Matiashe
 */
public class RegistrantStatusListPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private RegistrantStatusService registrantStatusService;

    public RegistrantStatusListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantStatusEditPage(null));
            }
        });

        IModel<List<RegistrantStatus>> model = new LoadableDetachableModel<List<RegistrantStatus>>() {
            @Override
            protected List<RegistrantStatus> load() {
                return registrantStatusService.findAll();
            }
        };
        PropertyListView<RegistrantStatus> eachItem = new PropertyListView<RegistrantStatus>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<RegistrantStatus> item) {
                Link<RegistrantStatus> viewLink = new Link<RegistrantStatus>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new RegistrantStatusViewPage(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };

        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
