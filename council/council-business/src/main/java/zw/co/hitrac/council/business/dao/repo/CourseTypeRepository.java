package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.CourseType;

/**
 *
 * @author Charles Chigoriwa
 */
public interface CourseTypeRepository extends CrudRepository<CourseType, Long> {

    public List<CourseType> findAll();
}
