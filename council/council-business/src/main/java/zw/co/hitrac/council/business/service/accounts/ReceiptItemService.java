package zw.co.hitrac.council.business.service.accounts;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.domain.TypeOfService;
import zw.co.hitrac.council.business.domain.User;
import zw.co.hitrac.council.business.domain.accounts.*;
import zw.co.hitrac.council.business.service.IGenericService;

/**
 * @author Michael Matiashe
 */
public interface ReceiptItemService extends IGenericService<ReceiptItem> {

    List<ReceiptItem> getReceiptItems(Product product, Date startDate, Date endDate, TransactionType transactionType, User user);

    List<ReceiptItem> getReceiptItemsDeposit(Product product, Date startDate, Date endDate, TransactionType transactionType, User user);

    List<ReceiptItem> get(DebtComponent debtComponent);

    List<ReceiptItem> getReceiptItemsWithNoReceiptHeader(Account account);

    List<Account> getReceiptItemsWithNoReceiptHeaders();

    void correctReceiptItems();

    List<ReceiptItem> getIncomeGenerated(Date startDate, Date endDate);

    List<ReceiptItem> getReceiptItems(Product product, PaymentMethod paymentMethod, PaymentType paymentType, Date startDate, Date endDate, TransactionType transactionType, User user);

    BigDecimal getTotalPaidForDebtComponent(DebtComponent debtComponent);

    BigDecimal getReceiptItemsDepositTotal(Product product, Date startDate, Date endDate, TransactionType transactionType, User user);

    BigDecimal getCalcualtedReceiptTotal(Product product, Date startDate, Date endDate, TransactionType transactionType, User user);

    BigDecimal getCalcualtedReceiptTotal(Product product, PaymentMethod paymentMethod, PaymentType paymentType, Date startDate, Date endDate, TransactionType transactionType, User user);

    List<Product> getDistinctProductsInSales(Date startDate, Date endDate);

    List<Product> getDistinctProductsInSales();

    BigDecimal getTotalPaidForDebtComponent(DebtComponent debtComponent, TransactionType transactionType);

    BigDecimal getReceiptItemsDepositTotal(Product product, Date startDate, Date endDate, Date depositStartDate, Date depositEndDate, TransactionType transactionType, User user);

    List<CouncilDuration> getCouncilDurationsPaidForByAccountAndTypeOfService(Account account, TypeOfService typeOfService);

    boolean hasPaidAnnualFeeForCouncilDuration(Account account, CouncilDuration duration);

    List<Long> ListOfPaidAnnualFeeForCouncilDuration(Account account, CouncilDuration duration);

    BigDecimal getTotalAmountPaidForByAccountAndTypeOfServiceAndCouncilDuration(Account account, TypeOfService typeOfService, CouncilDuration duration);

}
