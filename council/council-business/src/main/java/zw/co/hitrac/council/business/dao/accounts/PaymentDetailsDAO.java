
package zw.co.hitrac.council.business.dao.accounts;

import java.util.List;
import zw.co.hitrac.council.business.dao.IGenericDAO;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.accounts.PaymentDetails;

/**
 *
 * @author Tatenda Chiwandire
 * @author Michael Matiashe
 */
public interface PaymentDetailsDAO extends IGenericDAO<PaymentDetails> {
   public  List<PaymentDetails> transactionHistory(Customer customer);
   public List<PaymentDetails> getUnprintedPaymentDetails(Customer customer);
    public List<PaymentDetails> findByUid(String uid);
}
