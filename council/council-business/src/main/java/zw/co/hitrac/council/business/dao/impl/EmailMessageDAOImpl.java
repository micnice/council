package zw.co.hitrac.council.business.dao.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.EmailMessageDAO;
import zw.co.hitrac.council.business.dao.repo.EmailMessageRepository;
import zw.co.hitrac.council.business.domain.EmailMessage;
import zw.co.hitrac.council.business.domain.GeneralParameters;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.business.utils.Util;

import javax.mail.internet.MimeMessage;
import java.util.Collection;
import java.util.List;

/**
 * @author Charles Chigoriwa
 */
@Repository
public class EmailMessageDAOImpl implements EmailMessageDAO {

    @Autowired
    private EmailMessageRepository emailMessageRepository;
    @Autowired
    private JavaMailSenderImpl mailSender;
    @Autowired
    private GeneralParametersService generalParametersService;

    public EmailMessage save(EmailMessage emailMessage) {

        return emailMessageRepository.save(emailMessage);
    }

    public List<EmailMessage> findAll() {

        return emailMessageRepository.findAll();
    }

    public EmailMessage get(Long id) {

        return emailMessageRepository.findOne(id);
    }

    public List<EmailMessage> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * A setter method that will make mocking repo object easier
     *
     * @param emailMessageRepository
     */
    public void setEmailMessageRepository(EmailMessageRepository emailMessageRepository) {

        this.emailMessageRepository = emailMessageRepository;
    }

    @Override
    public void send(RegistrantData registrantData, final String subject, final String message) {

        GeneralParameters generalParameters = generalParametersService.get();

        if (StringUtils.isEmpty(generalParameters.getEmail()) || StringUtils.isEmpty(generalParameters.getEmailPassword())) {

            System.err.println("Cannot send email to registrant(s). Either user name or password was not set in General Parameters");

        } else {
            sendMail(generalParameters.getEmail(), generalParameters.getEmailPassword(),
                    registrantData.getEmailAddress(), subject, message);
        }
    }

    public void send(String email, final String subject, final String message) {

        GeneralParameters generalParameters = generalParametersService.get();

        sendMail(generalParameters.getEmail(), generalParameters.getEmailPassword(),
                email, subject, message);

    }

    @Override
    public void send(Collection<RegistrantData> registrantDataList, final String subject, final String message) {

        GeneralParameters generalParameters = generalParametersService.get();

        if (StringUtils.isEmpty(generalParameters.getEmail()) || StringUtils.isEmpty(generalParameters.getEmailPassword())) {

            throw new CouncilException("Cannot send email to registrant(s). Either user name or password was not set in General Parameters");

        } else {

            for (RegistrantData registrant : registrantDataList) {

                sendMail(generalParameters.getEmail(), generalParameters.getEmailPassword(),
                        registrant.getEmailAddress(), subject, message);
            }
        }
    }

    /**
     * Send plain email
     *
     * @param sender
     * @param senderPassword
     * @param recipient
     * @param subject
     * @param msg
     */
    private void sendMail(String sender, String senderPassword, String recipient, String subject, String msg) {


        if (Util.isValidEmailAddress(recipient)) {
            try {

                SimpleMailMessage message = new SimpleMailMessage();

                mailSender.setUsername(sender);
                mailSender.setPassword(senderPassword);

                message.setFrom(sender);
                message.setTo(recipient);
                message.setSubject(subject);
                message.setText(msg);
                mailSender.send(message);
            } catch (CouncilException e) {
                throw new CouncilException(e.getMessage());
            }
        } else {
            throw new CouncilException("Email Address Not Valid -> " + recipient);

        }

    }

    /**
     * Send an email with an attachment
     *
     * @param sender
     * @param senderPassword
     * @param recipient
     * @param subject
     * @param msg
     * @param attachmentFileName
     */
    private void sendMail(String sender, String senderPassword, String recipient, String subject, String msg,
                          String attachmentFileName) {

        if (Util.isValidEmailAddress(recipient)) {

            mailSender.setUsername(sender);
            mailSender.setPassword(senderPassword);

            MimeMessage message = mailSender.createMimeMessage();

            try {

                MimeMessageHelper helper = new MimeMessageHelper(message, true);

                helper.setFrom(sender);
                helper.setTo(recipient);
                //helper.setCc();
                helper.setSubject(subject);
                helper.setText(msg);

                FileSystemResource fileSystemResource = new FileSystemResource(attachmentFileName);
                helper.addAttachment(fileSystemResource.getFilename(), fileSystemResource);

                mailSender.send(message);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
