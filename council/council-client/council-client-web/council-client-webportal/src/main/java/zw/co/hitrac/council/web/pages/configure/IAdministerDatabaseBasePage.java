package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author Charles Chigoriwa
 */
public abstract class IAdministerDatabaseBasePage extends TemplatePage {
    public IAdministerDatabaseBasePage() {
        menuPanelManager.getConfigureMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getConfigureMenuPanel().setAdministerDatabaseMenuCurrent(true);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
