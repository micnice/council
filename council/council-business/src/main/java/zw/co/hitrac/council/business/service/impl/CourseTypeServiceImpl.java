package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.CourseTypeDAO;
import zw.co.hitrac.council.business.domain.CourseType;
import zw.co.hitrac.council.business.service.CourseTypeService;

import java.util.List;

/**
 *
 * @author Charles Chigoriwa
 */
@Service
@Transactional
public class CourseTypeServiceImpl implements CourseTypeService {

    @Autowired
    private CourseTypeDAO courseTypeDAO;

    @Transactional
    public CourseType save(CourseType courseType) {
        return courseTypeDAO.save(courseType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<CourseType> findAll() {
        return courseTypeDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public CourseType get(Long id) {
        return courseTypeDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<CourseType> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setCourseTypeDAO(CourseTypeDAO courseTypeDAO) {

        this.courseTypeDAO = courseTypeDAO;
    }

}
