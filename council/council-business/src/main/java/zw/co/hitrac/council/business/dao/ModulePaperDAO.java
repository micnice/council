/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao;

import java.util.List;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.ModulePaper;

/**
 *
 * @author tdhlakama
 */
public interface ModulePaperDAO extends IGenericDAO<ModulePaper> {

    public List<ModulePaper> findPapersInCourse(Course course);
}
