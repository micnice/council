package zw.co.hitrac.council.business.process;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.domain.accounts.*;
import zw.co.hitrac.council.business.service.accounts.*;

import java.math.BigDecimal;
import java.util.Set;

/**
 * @author Matiashe Michael
 */
@Service
public class ReversePaymentProcess {

    @Autowired
    private ReceiptHeaderService receiptHeaderService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private DebtComponentService debtComponentService;
    @Autowired
    private AccountsParametersService accountsParametersService;
    @Autowired
    private PrepaymentDetailsService prepaymentDetailsService;
    @Autowired
    private PrepaymentService prepaymentService;

    @Transactional()
    public ReceiptHeader reversePayment(ReceiptHeader receiptHeader) {

        PrepaymentDetails prepaymentDetails = prepaymentDetailsService.findByPaymentDetails(receiptHeader.getPaymentDetails());

        if (prepaymentDetails != null) {
            prepaymentDetails.setCancelled(Boolean.TRUE);
            Prepayment prepayment = prepaymentDetails.getPrepayment();
            BigDecimal balance = prepayment.getBalance();
            balance = balance.subtract(receiptHeader.getTotalAmountPaid());
            prepayment.setBalance(balance);
            prepaymentService.save(prepayment);
        }
        TransactionType receiptTranType = receiptHeader.getPaymentDetails().getTransactionHeader().getTransactionType();

        Set<ReceiptItem> receiptItems = receiptHeader.getReceiptItems();
        for (ReceiptItem receiptItem : receiptItems) {
            DebtComponent debtComponent = receiptItem.getDebtComponent();
            debtComponent.setRemainingBalance(debtComponent.getRemainingBalance().add(receiptItem.getAmount()));
            debtComponentService.save(debtComponent);
        }

        Account customerAccount = receiptHeader.getPaymentDetails().getCustomer().getAccount();
        // Update personal account case over payment by bank
        if (prepaymentDetails == null) {
            if (receiptHeader.getTotalAmount().compareTo(receiptHeader.getTotalAmountPaid()) == 1) {
                customerAccount.debit(receiptHeader.getTotalAmount().subtract(receiptHeader.getTotalAmountPaid()));
            }
        }
        receiptHeader.setReversed(Boolean.TRUE);
        receiptHeader = receiptHeaderService.save(receiptHeader);

        /*No need to update personal account, balance due is calculated from debt componets*/
        for (ReceiptItem receiptItem : receiptItems) {
            //  Update personal acount case Carryforward
            if (prepaymentDetails == null) {
                if (receiptTranType.equals(accountsParametersService.get().getCarryForwardTransactionType())) {
                    customerAccount.credit(receiptItem.getAmount());
                    accountService.save(customerAccount);
                }
            }
            //Update bank acc
            Account bankAccount = receiptTranType.getDrLedger();
            bankAccount.credit(receiptItem.getAmount());
            accountService.save(bankAccount);
            //Update Debtos acc
            Account creditAccount = receiptTranType.getCrLedger();
            creditAccount.debit(receiptItem.getAmount());
            accountService.save(creditAccount);
        }

        return receiptHeader;

    }
}
