
package zw.co.hitrac.council.business.dao.accounts;

import zw.co.hitrac.council.business.dao.IGenericDAO;
import zw.co.hitrac.council.business.domain.accounts.PaymentData;

/**
 *
 * @author Michael Matiashe
 */
public interface PaymentDataDAO extends IGenericDAO<PaymentData> {
    
}
