package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Tier;
import zw.co.hitrac.council.business.service.TierService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Edward Zengeni
 */
public class TierEditPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private TierService tierService;

    public TierEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<Tier>(new LoadableDetachableTierModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        Form<Tier> form = new Form<Tier>("form", (IModel<Tier>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                tierService.save(getModelObject());
                setResponsePage(new TierViewPage(getModelObject().getId()));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new TextField<String>("code").add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink", TierListPage.class));
        add(form);
    }

    private final class LoadableDetachableTierModel extends LoadableDetachableModel<Tier> {

        private Long id;

        public LoadableDetachableTierModel(Long id) {
            this.id = id;
        }

        @Override
        protected Tier load() {
            if (id == null) {
                return new Tier();
            }
            return tierService.get(id);
        }
    }
}
