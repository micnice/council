/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.models;

import java.util.List;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.accounts.PaymentType;
import zw.co.hitrac.council.business.service.accounts.PaymentTypeService;

/**
 *
 * @author Michael Matiashe
 */
public class PaymentTypeListmodel extends LoadableDetachableModel<List<PaymentType>> {
    private final PaymentTypeService paymentTypeService;

    public PaymentTypeListmodel(PaymentTypeService paymentTypeService) {
        this.paymentTypeService = paymentTypeService;
    }

    @Override
    protected List<PaymentType> load() {
      return paymentTypeService.findAll();
    }
    
}
