package zw.co.hitrac.council.web.pages.accounts;

import zw.co.hitrac.council.web.pages.configure.*;
import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.PaymentType;
import zw.co.hitrac.council.business.service.accounts.PaymentTypeService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

/**
 *
 * @author Charles Chigoriwa
 */
public class PaymentTypeListPage extends IBankBookPage{
    
    @SpringBean
    private PaymentTypeService paymentTypeService;

    public PaymentTypeListPage() {
        add(new BookmarkablePageLink<Void>("returnLink",BankBookPage.class));
        add(new Link<Void>("addNewLink") {

            @Override
            public void onClick() {
               setResponsePage(new PaymentTypeEditPage(null));
            }
        });
        
        IModel<List<PaymentType>> model=new LoadableDetachableModel<List<PaymentType>>() {

            @Override
            protected List<PaymentType> load() {
               return paymentTypeService.findAll();
            }
        };
        
        PropertyListView<PaymentType> eachItem=new PropertyListView<PaymentType>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<PaymentType> item) {
                Link<PaymentType> viewLink=new Link<PaymentType>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                       setResponsePage(new PaymentTypeViewPage(getModelObject().getId()));
                    }
                };
                if(item.getIndex()%2==0){
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };
        
        add(eachItem);
    }
    
    
    
}
