package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.ApplicationDAO;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.ApplicationPurpose;
import zw.co.hitrac.council.business.domain.QualificationType;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.domain.accounts.Document;
import zw.co.hitrac.council.business.process.CreditNoteProcess;
import zw.co.hitrac.council.business.service.ApplicationService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Michael Matiashe
 */
@Service
@Transactional
public class ApplicationServiceImpl implements ApplicationService {

    @Autowired
    private ApplicationDAO applicationDAO;
    @Autowired
    private DebtComponentService debtComponentService;
    @Autowired
    private CreditNoteProcess creditNoteProcess;


    @Transactional
    public Application save(Application application) {

        return applicationDAO.save(application);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Application> findAll() {

        return applicationDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Application get(Long id) {

        return applicationDAO.get(id);
    }

    public List<Application> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setApplicationDAO(ApplicationDAO applicationDAO) {

        this.applicationDAO = applicationDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Application> getApplications(String query, String applicationStatus, Date startDate, Date endDate, ApplicationPurpose applicationPurpose, Boolean used, Boolean expired) {

        return applicationDAO.getApplications(query, applicationStatus, startDate, endDate, applicationPurpose, used, expired);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Application> getRegistrantApplicationStatus(String applicationStatus) {

        return applicationDAO.getRegistrantApplicationStatus(applicationStatus);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Integer getTotalApplicationsByStatus(String status) {

        return applicationDAO.getTotalApplicationsByStatus(status);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Application> getExpiredApplications() {

        return applicationDAO.getExpiredApplications();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Integer getNumberOfPendingApplications() {

        return applicationDAO.getNumberOfPendingApplications();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Application> getFilteredPendingApplications() {

        return applicationDAO.getFilteredPendingApplications();
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Application> getApplicationsByYearApproved(int year) {

        return applicationDAO.getApplicationsByYearApproved(year);
    }

    public List<Application> getApplicationsWithoutPayment() {
        return applicationDAO.getApplicationsWithoutPayment();
    }

    public void removeDebtApplicationsWithoutPayment() {
        for (Application application : getApplicationsWithoutPayment()) {
            Document document = new Document();
            DebtComponent debtComponent = application.getDebtComponent();
            List<DebtComponent> selectedDebtComponents = new ArrayList<>();
            selectedDebtComponents.add(debtComponent);
            creditNoteProcess.processCreditNote(application.getRegistrant().getCustomerAccount(), document, selectedDebtComponents);
        }
    }

    public Boolean hasPendingApplication(Registrant registrant, ApplicationPurpose applicationPurpose) {
        return applicationDAO.hasPendingApplication(registrant, applicationPurpose);
    }
}
