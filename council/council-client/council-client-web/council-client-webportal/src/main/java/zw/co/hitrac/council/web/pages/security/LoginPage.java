package zw.co.hitrac.council.web.pages.security;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.devutils.stateless.StatelessComponent;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.StatelessForm;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import zw.co.hitrac.council.business.domain.GeneralParameters;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.UserService;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.models.VersionReadOnlyModel;
import zw.co.hitrac.council.web.pages.WebUserPortalLoginPage;
import zw.co.hitrac.council.web.pages.research.CouncilSiteImageResource;
import zw.co.hitrac.council.web.pages.research.MyDynamicImageResource;

/**
 * @author Clive Gurure
 *         <p>
 *         Please ensure that this page remains stateless for Spring security intergration to keep on working
 */
@StatelessComponent
public class LoginPage extends WebPage {

    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private UserService userService;
    private static final Logger logger = LoggerFactory.getLogger(LoginPage.class);

    public LoginPage() {
        add(new SignInForm("signInForm"));

        GeneralParameters generalParameters = generalParametersService.get();
        final String councilName = StringUtils.defaultIfBlank(generalParameters.getCouncilName(), "Council");

        add(new BookmarkablePageLink<Void>("webUserLogin", WebUserPortalLoginPage.class));

        add(new Label("displayCouncilName", councilName));
        add(new Label("councilName", councilName));
        final Image siteLogoImage = new Image("siteLogoImage", new MyDynamicImageResource(generalParametersService)) {
            @Override
            protected boolean getStatelessHint() {
                //Ensure stateless to avoid making page part of session
                return true;
            }
        };

        final Image councilPicture = new Image("councilPicture", new CouncilSiteImageResource(generalParametersService)) {

            @Override
            protected boolean getStatelessHint() {
                //Ensure stateless to avoid making page part of session
                return true;
            }
        };

        add(councilPicture);
        add(siteLogoImage);
        add(new Label("version", new VersionReadOnlyModel()));
        add(new BookmarkablePageLink<Void>("syslogout", SignOutPage.class));
    }

    /**
     * Sign in form
     */
    public final class SignInForm extends StatelessForm<Void> {

        private transient String username = "";
        private transient String password = "";

        /**
         * Constructor
         *
         * @param id id of the form component
         */
        public SignInForm(final String id) {
            super(id);
            add(new FeedbackPanel("errorMessage"));

            add(new TextField<String>("username", new PropertyModel<>(this, "username")));
            add(new PasswordTextField("password", new PropertyModel<>(this, "password")));
        }

        /**
         * @see org.apache.wicket.markup.html.form.Form#onSubmit()
         */
        @Override
        public final void onSubmit() {

            // Get session info
            CouncilSession session = getMySession();

            // Sign the user in
            if (session.signIn(username, password)) {
                continueToOriginalDestination();
                setResponsePage(getApplication().getHomePage());
            } else {

                // Get the error message from the properties file associated with the Component
                String errmsg = getString("loginError", null, "Unable to sign you in");

                // Register the error message with the feedback panel
                error(errmsg);
            }
        }

        private CouncilSession getMySession() {
            return CouncilSession.get();
        }
    }
}
