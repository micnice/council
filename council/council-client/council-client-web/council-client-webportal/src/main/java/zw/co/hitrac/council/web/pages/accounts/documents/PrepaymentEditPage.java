package zw.co.hitrac.council.web.pages.accounts.documents;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.accounts.Prepayment;
import zw.co.hitrac.council.business.process.PaymentProcess;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.accounts.PrepaymentService;
import zw.co.hitrac.council.business.service.accounts.TransactionTypeService;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.CustomDateTextField;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Michael Matiashe
 */
public class PrepaymentEditPage extends TemplatePage {

    @SpringBean
    private PrepaymentService prepaymentService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private TransactionTypeService transactionTypeService;
    @SpringBean
    private PaymentProcess paymentProcess;

    public PrepaymentEditPage(Long id) {
        CompoundPropertyModel<Prepayment> prepaymentModel = new CompoundPropertyModel<Prepayment>(new LoadableDetachablePrepaymentModel());
        final Institution institution = institutionService.get(id);
        prepaymentModel.getObject().setInstitution(institution);
        setDefaultModel(prepaymentModel);
        Form<Prepayment> form = new Form<Prepayment>("form", (IModel<Prepayment>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    paymentProcess.validateDateOfDeposit(getModelObject().getDateOfDeposit());
                    getModelObject().setDateOfPayment(new Date());
                    BigDecimal amount = getModelObject().getAmount();
                    getModelObject().setBalance(amount.multiply(new BigDecimal("-1")));
                    getModelObject().setInstitution(institution);
                    getModelObject().setCreatedBy(CouncilSession.get().getUser());
                    Prepayment prepayment = prepaymentService.save(getModelObject());
                    setResponsePage(new PrepaymentViewPage(prepayment.getId()));
                } catch (Exception e) {
                    e.printStackTrace();
                    error(e.getMessage());
                }
            }
        };
        Label dateOfpaymentLabel = new Label("dateOfPayment", new LoadableDetachableModel<String>() {

            @Override
            protected String load() {
                return DateUtil.getDate(new Date());
            }
        });

        form.add(new TextField<String>("amount").setRequired(true).add(new ErrorBehavior()));
        form.add(dateOfpaymentLabel);
        form.add(new CustomDateTextField("dateOfDeposit").add(new ErrorBehavior()).add(DatePickerUtil.getDatePicker()));
        form.add(new DropDownChoice("transactionType", transactionTypeService.findBankAccounts()).setRequired(true));
        add(new FeedbackPanel("feedback"));
        add(new Label("institution.name"));
        add(form);
    }

    private final class LoadableDetachablePrepaymentModel extends LoadableDetachableModel<Prepayment> {

        public LoadableDetachablePrepaymentModel() {
        }

        @Override
        protected Prepayment load() {
            return new Prepayment();

        }
    }

}
