package zw.co.hitrac.council.reports.utils;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.*;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import zw.co.hitrac.council.reports.Report;

import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.HashPrintServiceAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.PrintServiceAttributeSet;
import javax.print.attribute.standard.PrinterName;
import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.util.Collection;
import java.util.Map;

/**
 * @author Charles Chigoriwa Consider "Spring" this class in future
 */
public class ReportManager implements IReportManager {

    public JasperReport getJasperReport(String reportPath) throws JRException {

        if (reportPath.endsWith(Report.JRXML)) {

            return getJasperReportAfterCompile(reportPath);

        } else if (reportPath.endsWith(Report.JASPER)) {

            return getJasperReportPreCompiled(reportPath);

        } else {
            throw new IllegalArgumentException("Uknown extension of report path. Must specify either JRXML or JASPER extension");
        }
    }

    private JasperReport getJasperReportAfterCompile(String reportPath)
            throws JRException {

        JasperDesign design;
        JasperReport report;

        design = JRXmlLoader.load(CouncilReportApplication.class.getResourceAsStream(reportPath));
        report = JasperCompileManager.compileReport(design);

        return report;
    }

    private JasperReport getJasperReportPreCompiled(String reportPath)
            throws JRException {

        JasperReport report;

        report = (JasperReport) JRLoader.loadObjectFromFile(reportPath);

        return report;
    }

    // Make it more intelligent to generate other file formats
    public byte[] getPdfReportBytes(Map<String, Object> parameters, String reportPath, Collection<?> beanCollection)
            throws JRException {

        JasperPrint jasperPrint;

        if (reportPath.endsWith(Report.JASPER)) {
            jasperPrint = getJasperPrint(parameters, reportPath, beanCollection);
        } else {
            jasperPrint = getJasperPrintAfterCompile(parameters, reportPath, beanCollection);
        }

        JRPdfExporter exporter = new JRPdfExporter();
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, os);
        exporter.exportReport();

        return os.toByteArray();
    }

    public byte[] getPdfReportBytes(Map<String, Object> parameters, String reportPath, Connection connection) throws JRException {

        JasperPrint jasperPrint;

        if (reportPath.endsWith(Report.JASPER)) {
            jasperPrint = getJasperPrint(parameters, reportPath, connection);
        } else {
            jasperPrint = getJasperPrintAfterCompile(parameters, reportPath, connection);
        }

        JRPdfExporter exporter = new JRPdfExporter();
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, os);
        exporter.exportReport();

        return os.toByteArray();
    }

    @Override
    public byte[] getReportHTML(Map<String, Object> parameters, String reportPath, Collection<?> beanCollection) throws JRException {

        JasperPrint jasperPrint = getJasperPrintAfterCompile(parameters, reportPath, beanCollection);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        if (jasperPrint == null) {
            throw new IllegalArgumentException("jasperPrint null, can't convert to HTML report");
        }

        try {

            jasperPrint.setProperty("net.sf.jasperreports.export.html.exclude.origin.keep.first.band.2", "columnHeader");
            jasperPrint.setProperty("net.sf.jasperreports.export.html.exclude.origin.band.2", "pageFooter");
            JRHtmlExporter jrHtmlExporter = new JRHtmlExporter();
            jrHtmlExporter.setParameter(JRHtmlExporterParameter.JASPER_PRINT, jasperPrint);
            jrHtmlExporter.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, true);
//            jrHtmlExporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, "../servlets/image?image=");
            jrHtmlExporter.setParameter(JRHtmlExporterParameter.BETWEEN_PAGES_HTML, "");
            jrHtmlExporter.setParameter(JRHtmlExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            jrHtmlExporter.setParameter(JRHtmlExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
            jrHtmlExporter.setParameter(JRHtmlExporterParameter.OUTPUT_STREAM, outputStream);
            jrHtmlExporter.exportReport();

            return outputStream.toByteArray();

        } catch (JRException e) {
            throw new JRException("Error occurred exporting HTML report ", e);
        }
    }

    @Override
    public byte[] getReportHTML(Map<String, Object> parameters, String reportPath, Connection connection) throws JRException {

        JasperPrint jasperPrint = getJasperPrintAfterCompile(parameters, reportPath, connection);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        if (jasperPrint == null) {
            throw new IllegalArgumentException("jasperPrint null, can't convert to HTML report");
        }

        try {

            jasperPrint.setProperty("net.sf.jasperreports.export.html.exclude.origin.keep.first.band.2", "columnHeader");
            jasperPrint.setProperty("net.sf.jasperreports.export.html.exclude.origin.band.2", "pageFooter");
            JRHtmlExporter jrHtmlExporter = new JRHtmlExporter();
            jrHtmlExporter.setParameter(JRHtmlExporterParameter.JASPER_PRINT, jasperPrint);
            jrHtmlExporter.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, true);
//            jrHtmlExporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, "../servlets/image?image=");
            jrHtmlExporter.setParameter(JRHtmlExporterParameter.BETWEEN_PAGES_HTML, "");
            jrHtmlExporter.setParameter(JRHtmlExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
            jrHtmlExporter.setParameter(JRHtmlExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
            jrHtmlExporter.setParameter(JRHtmlExporterParameter.OUTPUT_STREAM, outputStream);
            jrHtmlExporter.exportReport();

            return outputStream.toByteArray();

        } catch (JRException e) {
            throw new JRException("Error occurred exporting HTML report ", e);
        }
    }

    // Make it more intelligent to generate other file formats
    public byte[] getReportXls(Map<String, Object> parameters, String reportPath, Collection<?> beanCollection)
            throws JRException {

        JasperPrint jasperPrint;

        if (reportPath.endsWith(Report.JASPER)) {
            jasperPrint = getJasperPrint(parameters, reportPath, beanCollection);
        } else {
            jasperPrint = getJasperPrintAfterCompile(parameters, reportPath, beanCollection);
        }

        JRXlsxExporter docxExporter = new JRXlsxExporter();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        docxExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        docxExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, os);
        docxExporter.exportReport();
        return os.toByteArray();
    }

    public byte[] getReportXls(Map<String, Object> parameters, String reportPath, Connection connection) throws JRException {

        JasperPrint jasperPrint;

        if (reportPath.endsWith(Report.JASPER)) {
            jasperPrint = getJasperPrint(parameters, reportPath, connection);
        } else {
            jasperPrint = getJasperPrintAfterCompile(parameters, reportPath, connection);
        }
        JRXlsxExporter docxExporter = new JRXlsxExporter();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        docxExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        docxExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, os);
        docxExporter.exportReport();
        return os.toByteArray();
    }

    public byte[] getReportDoc(Map<String, Object> parameters, String reportPath, Collection<?> beanCollection)
            throws JRException {

        JasperPrint jasperPrint;

        if (reportPath.endsWith(Report.JASPER)) {
            jasperPrint = getJasperPrint(parameters, reportPath, beanCollection);
        } else {
            jasperPrint = getJasperPrintAfterCompile(parameters, reportPath, beanCollection);
        }

        JRDocxExporter docxExporter = new JRDocxExporter();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        docxExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        docxExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, os);
        docxExporter.exportReport();
        return os.toByteArray();
    }

    @Override
    public byte[] getReportDoc(Map<String, Object> parameters, String reportPath, Connection connection) throws JRException {

        JasperPrint jasperPrint;

        if (reportPath.endsWith(Report.JASPER)) {
            jasperPrint = getJasperPrint(parameters, reportPath, connection);
        } else {
            jasperPrint = getJasperPrintAfterCompile(parameters, reportPath, connection);
        }

        JRDocxExporter docxExporter = new JRDocxExporter();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        docxExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        docxExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, os);
        docxExporter.exportReport();
        return os.toByteArray();
    }

    public void printJasperPrint(String printerName, Map<String, Object> parameters, String reportPath, Collection<?> beanCollection)
            throws JRException {

        JasperPrint jasperPrint;

        if (reportPath.endsWith(Report.JASPER)) {
            jasperPrint = getJasperPrint(parameters, reportPath, beanCollection);
        } else {
            jasperPrint = getJasperPrintAfterCompile(parameters, reportPath, beanCollection);
        }
        // - source http://jasperreports.sourceforge.net/sample.reference/printservice/index.html
        long start = System.currentTimeMillis();

        JRDocxExporter docxExporter = new JRDocxExporter();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        docxExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        docxExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, os);

        JRPrintServiceExporter exporter = new JRPrintServiceExporter();
        PrintServiceAttributeSet printServiceAttributeSet = new HashPrintServiceAttributeSet();
        printServiceAttributeSet.add(new PrinterName(printerName, null));

        exporter.setParameter(JRPrintServiceExporterParameter.PRINT_SERVICE_ATTRIBUTE_SET, printServiceAttributeSet);
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PAGE_DIALOG, Boolean.FALSE);
        exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PRINT_DIALOG, Boolean.FALSE);
        exporter.exportReport();
     
        System.err.println("Printing time : " + (System.currentTimeMillis() - start));

    }

    @Override
    public void printJasperPrint(String printerName, Map<String, Object> parameters, String reportPath, Connection connection) throws JRException {

        JasperPrint jasperPrint;

        if (reportPath.endsWith(Report.JASPER)) {
            jasperPrint = getJasperPrint(parameters, reportPath, connection);
        } else {
            jasperPrint = getJasperPrintAfterCompile(parameters, reportPath, connection);
        }

        // - source http://jasperreports.sourceforge.net/sample.reference/printservice/index.html
        //       JasperPrintManager.printReport(jasperPrint, false);
        long start = System.currentTimeMillis();
        JRPrintServiceExporter exporter = new JRPrintServiceExporter();
        PrintRequestAttributeSet printRequestAttributeSet = new HashPrintRequestAttributeSet();
        exporter.setParameter(JRPrintServiceExporterParameter.PRINT_REQUEST_ATTRIBUTE_SET, printRequestAttributeSet);

        PrintServiceAttributeSet printServiceAttributeSet = new HashPrintServiceAttributeSet();
        //printServiceAttributeSet.add(new PrinterName("EPSON LX-300+ /II", null));
        printServiceAttributeSet.add(new PrinterName(printerName, null));
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        exporter.setParameter(JRPrintServiceExporterParameter.PRINT_SERVICE_ATTRIBUTE_SET, printServiceAttributeSet);
        exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PAGE_DIALOG, Boolean.FALSE);
        exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PRINT_DIALOG, Boolean.FALSE);
        exporter.exportReport();

        System.err.println("Printing time : " + (System.currentTimeMillis() - start));

    }

    public JasperPrint getJasperPrint(Map<String, Object> parameters, String reportPath, Collection<?> beanCollection)
            throws JRException {

        JasperPrint jasperPrint;
        JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(beanCollection);

        jasperPrint = JasperFillManager.fillReport(CouncilReportApplication.class.getResourceAsStream(reportPath),
                parameters, beanCollectionDataSource);

        return jasperPrint;
    }

    public JasperPrint getJasperPrint(Map<String, Object> parameters, String reportPath, Connection connection)
            throws JRException {

        JasperPrint jasperPrint;

        jasperPrint = JasperFillManager.fillReport(CouncilReportApplication.class.getResourceAsStream(reportPath),
                parameters, connection);

        return jasperPrint;
    }

    public JasperPrint getJasperPrintAfterCompile(Map<String, Object> parameters, String reportPath,
            Collection<?> beanCollection)
            throws JRException {

        JasperDesign design;
        JasperReport report;
        JasperPrint jasperPrint;
        JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(beanCollection);

        design = JRXmlLoader.load(CouncilReportApplication.class.getResourceAsStream(reportPath));
        report = JasperCompileManager.compileReport(design);
        jasperPrint = JasperFillManager.fillReport(report, parameters, beanCollectionDataSource);

        return jasperPrint;
    }

    public JasperPrint getJasperPrintAfterCompile(Map<String, Object> parameters, String reportPath,
            Connection connection)
            throws JRException {

        JasperDesign design;
        JasperReport report;
        JasperPrint jasperPrint;

        design = JRXmlLoader.load(CouncilReportApplication.class.getResourceAsStream(reportPath));
        report = JasperCompileManager.compileReport(design);
        jasperPrint = JasperFillManager.fillReport(report, parameters, connection);

        return jasperPrint;
    }
}
