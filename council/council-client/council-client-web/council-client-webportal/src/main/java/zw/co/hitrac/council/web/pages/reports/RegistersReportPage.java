package zw.co.hitrac.council.web.pages.reports;

//~--- non-JDK imports --------------------------------------------------------
import net.sf.jasperreports.engine.JRException;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.reports.utils.ContentType;

//~--- JDK imports ------------------------------------------------------------

import java.util.HashMap;
import zw.co.hitrac.council.reports.RegistrantAgeReport;

/**
 *
 * @author Edward Zengeni
 */
public class RegistersReportPage extends IReportPage {

    @SpringBean
    private RegistrantService registrantService;

    public RegistersReportPage() {
        Form<Void> form = new Form<Void>("form2") {
            @Override
            protected void onSubmit() {
                try {
                    RegistrantAgeReport registrantReport = new RegistrantAgeReport();
                    ContentType contentType = ContentType.PDF;

                    processReport(registrantReport, contentType, registrantService.findAll(),
                            new HashMap<String, Object>());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        };

        add(form);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
