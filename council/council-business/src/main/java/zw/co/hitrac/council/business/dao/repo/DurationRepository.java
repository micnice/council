package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.Duration;

/**
 *
 * @author Michael Matiashe
 */
public interface DurationRepository extends CrudRepository<Duration, Long> {
    public List<Duration> findAll();
}
