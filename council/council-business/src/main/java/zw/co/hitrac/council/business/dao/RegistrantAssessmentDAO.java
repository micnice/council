/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao;

import java.util.List;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantAssessment;

/**
 *
 * @author kelvin
 */
public interface RegistrantAssessmentDAO extends IGenericDAO<RegistrantAssessment> {

    public List<RegistrantAssessment> getAssessments(Registrant registrant);

    public List<RegistrantAssessment> getAssessments(Institution institution);
    
    public List<RegistrantAssessment> getAssessments(Application application);
}
