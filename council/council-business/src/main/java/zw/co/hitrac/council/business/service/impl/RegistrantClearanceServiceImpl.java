/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.RegistrantClearanceDAO;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantClearance;
import zw.co.hitrac.council.business.service.RegistrantClearanceService;

import java.util.Date;
import java.util.List;

/**
 *
 * @author kelvin
 */
@Service
@Transactional
public class RegistrantClearanceServiceImpl implements RegistrantClearanceService {

    @Autowired
    private RegistrantClearanceDAO registrantClearanceDAO;

    @Transactional
    public RegistrantClearance save(RegistrantClearance registrantClearance) {
        return registrantClearanceDAO.save(registrantClearance);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantClearance> findAll() {
        return registrantClearanceDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantClearance get(Long id) {
        return registrantClearanceDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantClearance> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setRegistrantClearanceDAO(RegistrantClearanceDAO registrantClearanceDAO) {
        this.registrantClearanceDAO = registrantClearanceDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantClearance> getClearances(Registrant registrant) {
        return registrantClearanceDAO.getClearances(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Date getRegistrantClearanceDate(Registrant registrant, Course course) {
        return registrantClearanceDAO.getRegistrantClearanceDate(registrant, course);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantClearance getRegistrantClearance(Registrant registrant, Course course) {
        return registrantClearanceDAO.getRegistrantClearance(registrant, course);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Boolean getHasPBQExamClearance(Registrant registrant) {
        return registrantClearanceDAO.getHasPBQExamClearance(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Boolean getHasExamClearance(Registrant registrant) {
        return registrantClearanceDAO.getHasExamClearance(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Date getLastClearanceDate(Registrant registrant) {
        return registrantClearanceDAO.getLastClearanceDate(registrant);
    }
}
