/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.reports;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.service.ApplicationService;
import zw.co.hitrac.council.business.service.RegistrantQualificationService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.reports.ApplicationStatusReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 * @author Artwell Mamvura
 */
public final class ApplicationStatusReportPage extends IReportPage {

    @SpringBean
    private ApplicationService applicationService;
    @SpringBean
    private RegistrantQualificationService registrantQualificationService;
    private String applicationStatus;
    private ContentType contentType;

    public ApplicationStatusReportPage() {
        PropertyModel<String> applicationStatusModel = new PropertyModel<String>(this, "applicationStatus");
        PropertyModel<ContentType> contentTypeModel = new PropertyModel<ContentType>(this, "contentType");
        final List applicationStates = Arrays.asList(new String[]{Application.APPLICATIONPENDING, Application.APPLICATIONAPPROVED,
            Application.APPLICATIONDECLINED, Application.APPLICATION_AWAITING_COUNCIL_APPROVAL, Application.APPLICATIONUNDERREVIEW});

      
        Form<?> form = new Form("form") {
            @Override
            protected void onSubmit() {
                try {
                    ApplicationStatusReport applicationStatusReport = new ApplicationStatusReport();
                    
                    Map parameters = new HashMap();

                    parameters.put("comment", "List of Individuals with " + applicationStatus);

                    ByteArrayResource resource;

                    try {

                        resource = ReportResourceUtils.getReportResource(applicationStatusReport, contentType,
                                applicationService.getRegistrantApplicationStatus(applicationStatus),
                                parameters);

                        IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                RequestCycle.get().getResponse(), null);

                        resource.respond(a);
                    } catch (JRException ex) {
                        Logger.getLogger(ApplicationStatusReport.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };

          form.add(new DropDownChoice("applicationStatus", applicationStatusModel, applicationStates));
          form.add(new DropDownChoice("contentType", contentTypeModel, Arrays.asList(ContentType.values())));
          
        add(form);
    }
}
