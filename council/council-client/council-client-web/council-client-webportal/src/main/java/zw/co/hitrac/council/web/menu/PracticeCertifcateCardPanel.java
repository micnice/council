/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.menu;

import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.image.NonCachingImage;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantCondition;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.web.pages.research.MyDynamicImageResource;
import zw.co.hitrac.council.web.pages.research.RegistrantImageResource;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;

/**
 * @author tdhlakama
 */
public class PracticeCertifcateCardPanel extends Panel {

    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private RegistrationService registrationService;
    @SpringBean
    private RegistrationProcess registrationProcess;
    @SpringBean
    private RegistrantActivityService registrantActivityService;
    @SpringBean
    private RegistrantConditionService registrantConditionService;

    public PracticeCertifcateCardPanel(String id, final Long registrantId) {
        super(id);

        final CompoundPropertyModel<Registrant> model = new CompoundPropertyModel<Registrant>(new DetachableRegistrantModel(registrantId, registrantService));

        setDefaultModel(model);
        add(new Label("councilName", generalParametersService.get().getCouncilName()));
        add(new Label("fullname"));
        add(new Label("idNumber"));
        add(new Label("registrationNumber"));

        CouncilDuration lastCouncilDuration = registrantActivityService.getLastCouncilDurationRenewal(model.getObject());

        if (lastCouncilDuration != null) {
            add(new Label("lastRenewal", lastCouncilDuration.getName()));
        } else {
            add(new Label("lastRenewal", ""));
        }

        String courseName = "";
        String conditions = "";
        for (Registration reg : registrationService.getActiveRegistrations(model.getObject())) {
            if (reg.getRegister() != null && !reg.getRegister().equals(generalParametersService.get().getStudentRegister())) {
                courseName = reg.getCourse().getName();
                break;
            }

        }

        for (RegistrantCondition registrantCondition : registrantConditionService.getRegistrantActiveConditions(model.getObject())) {
            conditions = conditions.concat(registrantCondition.toString()).concat(".");
        }


        add(new Label("course", courseName));
        add(new Label("conditions", conditions));
        

        add(new Image("siteLogoImage", new MyDynamicImageResource(generalParametersService)));
        add(new NonCachingImage("registrantImage", new RegistrantImageResource(model.getObject(), generalParametersService)));
    }

}
