/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.RegistrantDisciplinaryDAO;
import zw.co.hitrac.council.business.domain.CaseOutcome;
import zw.co.hitrac.council.business.domain.MisconductType;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantDisciplinary;
import zw.co.hitrac.council.business.service.RegistrantDisciplinaryService;

import java.util.Date;
import java.util.List;

/**
 *
 * @author hitrac
 */
@Service
@Transactional
public class RegistrantDisciplinaryServiceImpl implements RegistrantDisciplinaryService {

    @Autowired
    private RegistrantDisciplinaryDAO registrantDisciplinaryDAO;

    @Transactional
    public RegistrantDisciplinary save(RegistrantDisciplinary registrantDisciplinary) {
        return registrantDisciplinaryDAO.save(registrantDisciplinary);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantDisciplinary> findAll() {
        return registrantDisciplinaryDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantDisciplinary get(Long id) {
        return registrantDisciplinaryDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantDisciplinary> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setRegistrantDisciplinaryDAO(RegistrantDisciplinaryDAO registrantDisciplinaryDAO) {
        this.registrantDisciplinaryDAO = registrantDisciplinaryDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantDisciplinary> getCurrentUnResolved(Registrant registrant) {
        return registrantDisciplinaryDAO.getCurrentUnResolved(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public String getPendingCases(Registrant registrant) {

        return registrantDisciplinaryDAO.getPendingCases(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantDisciplinary> getGuiltyResolvedCases(Registrant registrant) {
        return registrantDisciplinaryDAO.getGuiltyResolvedCases(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantDisciplinary> getRegistrantDisciplinaries(Registrant registrant) {
        return registrantDisciplinaryDAO.getRegistrantDisciplinaries(registrant);

    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantDisciplinary> getDisciplinariesByDates(Date startDate, Date endDate, CaseOutcome caseOutcome, MisconductType misconductType, String caseStatus) {
        return registrantDisciplinaryDAO.getDisciplinariesByDates(startDate, endDate, caseOutcome, misconductType, caseStatus);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Boolean registrantSuspendedStatus(Registrant registrant) {
        return registrantDisciplinaryDAO.registrantSuspendedStatus(registrant);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantDisciplinary> find(MisconductType misconductType, int startYear, int endYear) {

        return registrantDisciplinaryDAO.find(misconductType, startYear, endYear);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantDisciplinary> find(MisconductType misconductType, int yearReported) {

        return registrantDisciplinaryDAO.find(misconductType, yearReported);
    }
}
