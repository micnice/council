package zw.co.hitrac.council.web.pages.accounts;

/**
 *
 * @author Edward Zengeni
 */
public abstract class IInventoryModulePage extends IAccountingPage {

    public IInventoryModulePage() {
        menuPanelManager.getAccountingPanel().setInventoryModule(true);
    }
}
