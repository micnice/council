package zw.co.hitrac.council.business.dao;

import java.util.List;
import zw.co.hitrac.council.business.domain.Conditions;

/**
 *
 * @author Michael Matiashe
 */
public interface ConditionDAO extends IGenericDAO<Conditions> {
    public List<Conditions> findAll(Boolean examCondition);
    public List<Conditions> getRegisterConditions();
}