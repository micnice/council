package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Qualification;
import zw.co.hitrac.council.business.service.QualificationService;

/**
 *
 * @author charlesc
 */
public class QualificationViewPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private QualificationService qualificationService;

    public QualificationViewPage(Long id) {
        CompoundPropertyModel<Qualification> model =
                new CompoundPropertyModel<Qualification>(new LoadableDetachableQualificationModel(id));

        setDefaultModel(model);
        add(new Link<Qualification>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new QualificationEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", QualificationListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("qualificationType"));
        add(new Label("qualification", model.bind("name")));
        add(new Label("prefixName"));
        add(new Label("registerType"));
        add(new Label("course"));
        add(new Label("isoCode"));
        add(new Label("preReq"));
    }

    private final class LoadableDetachableQualificationModel extends LoadableDetachableModel<Qualification> {

        private Long id;

        public LoadableDetachableQualificationModel(Long id) {
            this.id = id;
        }

        @Override
        protected Qualification load() {
            return qualificationService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
