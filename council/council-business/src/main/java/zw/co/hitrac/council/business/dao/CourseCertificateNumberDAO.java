package zw.co.hitrac.council.business.dao;

import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.CourseCertificateNumber;
import zw.co.hitrac.council.business.domain.Register;

/**
 *
 * @author Charles Chigoriwa
 */
public interface CourseCertificateNumberDAO extends IGenericDAO<CourseCertificateNumber> {

    public CourseCertificateNumber get(Course course);

    public void resetCertificateNumbers();

    public CourseCertificateNumber get(Course course, Register register);
}
