
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------
import java.text.ParseException;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.examinations.Exam;
import zw.co.hitrac.council.business.service.ModulePaperService;
import zw.co.hitrac.council.business.service.examinations.ExamService;
import zw.co.hitrac.council.business.service.examinations.ExamSettingService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.configure.AdministerDatabasePage;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

//~--- JDK imports ------------------------------------------------------------

import java.util.Date;
import org.apache.wicket.extensions.yui.calendar.TimeField;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.web.models.ExamSettingActiveListModel;
import zw.co.hitrac.council.web.utility.CustomDateTextField;

/**
 *
 * @author tdhlakama
 */
public class ExamEditPage extends IExaminationsPage {

    @SpringBean
    private ExamService examService;
    @SpringBean
    private ExamSettingService examSettingService;
    @SpringBean
    private ModulePaperService modulePaperService;

    public ExamEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<Exam>(new ExamEditPage.LoadableDetachableExamModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        Form<Exam> form = new Form<Exam>("form", (IModel<Exam>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    Date date = getModel().getObject().getExamDate();
                    getModel().getObject().setExamDate(DateUtil.getInitialDateTime(date));
                    examService.save(getModelObject());
                    setResponsePage(new ExamViewPage(getModelObject().getId()));
                } catch (ParseException e) {
                    throw new CouncilException("Failed to create Exam - Date");
                }
            }
        };

        form.add(new BookmarkablePageLink<Void>("returnLink", ExamListPage.class));
        form.add(new TextField<Double>("passMark").setRequired(true).add(new ErrorBehavior()));
        form.add(new TimeField("examTime").setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("modulePaper", modulePaperService.findAll()).add(new ErrorBehavior()));
        form.add(new CustomDateTextField("examDate").setRequired(true).add(new ErrorBehavior()).add(DatePickerUtil.getDatePicker()));
        form.add(new DropDownChoice("examSetting", new ExamSettingActiveListModel(examSettingService)).setRequired(true).add(new ErrorBehavior()));
        add(form);
    }

    private final class LoadableDetachableExamModel extends LoadableDetachableModel<Exam> {

        private Long id;

        public LoadableDetachableExamModel(Long id) {
            this.id = id;
        }

        @Override
        protected Exam load() {
            if (id == null) {
                return new Exam();
            }

            return examService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
