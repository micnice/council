package zw.co.hitrac.council.web.pages.registration;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.process.BillingProcess;
import zw.co.hitrac.council.business.process.ProductIssuer;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.process.RenewalProcess;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.models.*;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.accounts.documents.DebtComponentsPage;
import zw.co.hitrac.council.web.pages.institution.InstitutionEditPage;
import zw.co.hitrac.council.web.utility.CustomDateTextField;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

import java.util.Arrays;

import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;

/**
 * @author Tatenda Chiwandire
 */
public class RegistrantQualificationPage extends TemplatePage {

    @SpringBean
    private QualificationService qualificationService;
    @SpringBean
    private RegistrantQualificationService registrantQualificationService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private BillingProcess billingProcess;
    @SpringBean
    private CourseService courseService;
    @SpringBean
    private ProductIssuer productIssuer;
    @SpringBean
    private RegistrationProcess registrationProcess;
    @SpringBean
    private RenewalProcess renewalProcess;
    @SpringBean
    private DurationService durationService;

    public RegistrantQualificationPage(final IModel<Registrant> registrantModel, final Boolean foreign) {
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());

        CompoundPropertyModel<RegistrantQualification> model = new CompoundPropertyModel<RegistrantQualification>(
                new RegistrantQualificationPage.LoadableDetachableRegistrantQualificationModel(
                        registrantModel.getObject()));
        if (foreign) {
            model.getObject().setQualificationType(QualificationType.FOREIGN);
        } else {
            model.getObject().setQualificationType(QualificationType.LOCAL);
        }
        setDefaultModel(model);
        Form<RegistrantQualification> form = new Form<RegistrantQualification>("form",
                (IModel<RegistrantQualification>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    //course implement discipline from qualification
                    if (getModelObject().getCourse() == null) {
                        if (getModelObject().getQualification().getCourse() != null) {
                            getModelObject().setCourse(getModelObject().getQualification().getCourse());
                        }
                    }
                    registrantQualificationService.save(getModelObject());
                    setResponsePage(new RegistrantQualificationEmploymentPage(getModelObject().getRegistrant().getId()));
                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };

        form.add(new DropDownChoice("qualification", new QualificationListModel(qualificationService)));

        form.add(new DropDownChoice("awardingInstitution", new AcademicInstitutionstModel(institutionService)));
        form.add(new DropDownChoice("institution", new TrainingInstitutionstModel(institutionService)));
        form.add(new CustomDateTextField("dateAwarded").add(DatePickerUtil.getDatePicker()));
        form.add(new CustomDateTextField("startDate").add(DatePickerUtil.getDatePicker()));
        form.add(new CustomDateTextField("endDate").add(DatePickerUtil.getDatePicker()));
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });
        form.add(new Label("courseLabel", "Course") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new DropDownChoice("course", new CourseListModel(courseService)) {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }

            @Override
            public boolean isNullValid() {
                return Boolean.TRUE;
            }
        });
        form.add(new DropDownChoice("qualificationType", Arrays.asList(QualificationType.values())));
        add(form);
        add(new FeedbackPanel("feedback"));
        add(new Label("registrant.fullname"));

        add(new Link<Void>("addInstitutionPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionEditPage(null, registrantModel, foreign));
            }
        });
    }

    public RegistrantQualificationPage(Long id) {
        CompoundPropertyModel<RegistrantQualification> model = new CompoundPropertyModel<RegistrantQualification>(
                new RegistrantQualificationPage.LoadableDetachableRegistrantQualificationModel(id));

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);

        final Long registrantId = model.getObject().getRegistrant().getId();

        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantId);
        setDefaultModel(model);

        Form<RegistrantQualification> form = new Form<RegistrantQualification>("form",
                (IModel<RegistrantQualification>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                registrantQualificationService.save(getModelObject());
                setResponsePage(new RegistrantQualificationEmploymentPage(getModelObject().getRegistrant().getId()));
            }
        };

        form.add(new DropDownChoice("qualification", new QualificationListModel(qualificationService)));
        form.add(new DropDownChoice("awardingInstitution", new AcademicInstitutionstModel(institutionService)));
        form.add(new DropDownChoice("institution", new TrainingInstitutionstModel(institutionService)));
        form.add(
                new CustomDateTextField("dateAwarded").add(
                        DatePickerUtil.getDatePicker()));
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantId));
            }
        });
        form.add(new CustomDateTextField("startDate").add(DatePickerUtil.getDatePicker()));
        form.add(new CustomDateTextField("endDate").add(DatePickerUtil.getDatePicker()));
        form.add(new Label("courseLabel", "Course") {
            @Override
            protected void onConfigure() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.IT_OFFICER)));
                setVisible(enabled);
            }
        });
        form.add(new DropDownChoice("course", new CourseListModel(courseService)) {
            @Override
            protected void onConfigure() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.IT_OFFICER)));
                setVisible(enabled);
            }

            @Override
            public boolean isNullValid() {
                return Boolean.TRUE;
            }
        });
        form.add(new DropDownChoice("qualificationType", Arrays.asList(QualificationType.values())));
        add(form);
        add(new FeedbackPanel("feedback"));
        add(new Label("registrant.fullname"));

        add(new Link<Void>("addInstitutionPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionEditPage(null, null, null));
            }

            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
    }

    public RegistrantQualificationPage(final IModel<Registrant> registrantModel, final Application application) {
        CompoundPropertyModel<RegistrantQualification> model = new CompoundPropertyModel<RegistrantQualification>(
                new RegistrantQualificationPage.LoadableDetachableRegistrantQualificationModel(
                        registrantModel.getObject()));

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);

        final Long registrantId = model.getObject().getRegistrant().getId();

        model.getObject().setQualification(application.getQualification());
        model.getObject().setInstitution(application.getTrainingInstitution());
        model.getObject().setAwardingInstitution(application.getInstitution());
        model.getObject().setDateAwarded(application.getDateAwarded());
        model.getObject().setStartDate(application.getQualificationStartDate());
        model.getObject().setEndDate(application.getQualificationEndDate());

        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantId);
        setDefaultModel(model);

        Form<RegistrantQualification> form = new Form<RegistrantQualification>("form",
                (IModel<RegistrantQualification>) getDefaultModel()) {
            @Override
            public void onSubmit() {

                RegistrantQualification registrantQualification = registrantQualificationService.get(registrantModel.getObject(), getModelObject().getQualification());
                if (registrantQualification != null) {
                    registrantQualification.setInstitution(getModelObject().getInstitution());
                    registrantQualification.setAwardingInstitution(getModelObject().getAwardingInstitution());
                    registrantQualification.setDateAwarded(getModelObject().getDateAwarded());
                    registrantQualification.setStartDate(getModelObject().getStartDate());
                    registrantQualification.setEndDate(getModelObject().getEndDate());
                    registrantQualificationService.save(registrantQualification);

                    //course implement discipline from qualification
                    if (registrantQualification.getCourse() == null) {
                        if (registrantQualification.getQualification().getCourse() != null) {
                            registrantQualification.setCourse(getModelObject().getQualification().getCourse());
                        }
                    }
                    registrantQualificationService.save(registrantQualification);
                } else {
                    //course implement discipline from qualification
                    if (getModelObject().getCourse() == null) {
                        if (getModelObject().getCourse().getQualification().getCourse() != null) {
                            getModelObject().setCourse(getModelObject().getQualification().getCourse());
                        }
                    }
                    registrantQualificationService.save(getModelObject());
                }

                Registration newRegistration = new Registration();
                newRegistration.setRegistrant(getModelObject().getRegistrant());
                newRegistration.setRegister(generalParametersService.get().getProvisionalRegister());
                newRegistration.setCourse(getModelObject().getCourse());
                //create additional or registration certificate of course if any
                productIssuer.issueRegistrationOrAdditionalQualficationCertificate(newRegistration, null);

                setResponsePage(new RegistrantQualificationEmploymentPage(getModelObject().getRegistrant().getId()));
            }
        };

        form.add(new DropDownChoice("qualification", new QualificationListModel(qualificationService)));
        form.add(new DropDownChoice("awardingInstitution", new AcademicInstitutionstModel(institutionService)));
        form.add(new DropDownChoice("institution", new TrainingInstitutionstModel(institutionService)));
        form.add(
                new CustomDateTextField("dateAwarded").add(
                        DatePickerUtil.getDatePicker()));
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantId));
            }
        });
        form.add(new CustomDateTextField("startDate").add(DatePickerUtil.getDatePicker()));
        form.add(new CustomDateTextField("endDate").add(DatePickerUtil.getDatePicker()));

        form.add(new Label("courseLabel", "Qualification Course") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.TRUE);
            }
        });
        form.add(new DropDownChoice("course", new CourseListModel(courseService)) {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.TRUE);
            }

            @Override
            public boolean isNullValid() {
                return Boolean.TRUE;
            }
        });
        form.add(new DropDownChoice("qualificationType", Arrays.asList(QualificationType.values())));
        add(form);
        add(new Label("registrant.fullname"));
        add(new FeedbackPanel("feedback"));
        add(new Link<Void>("addInstitutionPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionEditPage(null, null, null));
            }

            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
    }

    public RegistrantQualificationPage(final Registration registration) {

        CompoundPropertyModel<RegistrantQualification> model = new CompoundPropertyModel<RegistrantQualification>(
                new RegistrantQualificationPage.LoadableDetachableRegistrantQualificationModel(
                        registration.getRegistrant()));

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);

        final Long registrantId = model.getObject().getRegistrant().getId();

        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantId);
        setDefaultModel(model);

        Form<RegistrantQualification> form = new Form<RegistrantQualification>("form",
                (IModel<RegistrantQualification>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                DebtComponent debtComponent = null;
                Registration newRegistration = registration;
                newRegistration.setCourse(getModelObject().getCourse());

                RegistrantQualification registrantQualification = registrantQualificationService.get(registration.getRegistrant(), getModelObject().getQualification());
                if (registrantQualification != null) {
                    registrantQualification.setInstitution(getModelObject().getInstitution());
                    registrantQualification.setAwardingInstitution(getModelObject().getAwardingInstitution());
                    registrantQualification.setDateAwarded(getModelObject().getDateAwarded());
                    registrantQualification.setStartDate(getModelObject().getStartDate());
                    registrantQualification.setEndDate(getModelObject().getEndDate());
                    //course implement discipline from qualification
                    if (registrantQualification.getCourse() == null) {
                        if (registrantQualification.getQualification().getCourse() != null) {
                            registrantQualification.setCourse(getModelObject().getQualification().getCourse());
                        }
                    }
                    registrantQualificationService.save(registrantQualification);
                } else {
                    if (getModelObject().getCourse() == null) {
                        if (getModelObject().getCourse().getQualification().getCourse() != null) {
                            getModelObject().setCourse(getModelObject().getQualification().getCourse());
                        }
                    }
                    registrantQualificationService.save(getModelObject());
                }
                debtComponent = billingProcess.billRegistrationOfNewQualificationProduct(registration, getModelObject().getCourse());
                productIssuer.issueRegistrationOrAdditionalQualficationCertificate(newRegistration, debtComponent);
                Registration currentRegistration = registrationProcess.activeRegisterNotStudentRegister(registration.getRegistrant());
                Duration duration = durationService.getCurrentCourseDuration(currentRegistration.getCourse());
                if (duration != null) {
                    if (renewalProcess.hasCurrentRenewalActivity(currentRegistration.getRegistrant(), duration)) ;
                    {
                        productIssuer.issueProductPracticeCertificate(currentRegistration, debtComponent);
                    }
                }

                setResponsePage(new DebtComponentsPage(registrantId));
            }
        };

        form.add(new DropDownChoice("qualification", new QualificationListModel(qualificationService)));
        form.add(new DropDownChoice("awardingInstitution", new AcademicInstitutionstModel(institutionService)));
        form.add(new DropDownChoice("institution", new TrainingInstitutionstModel(institutionService)));
        form.add(
                new CustomDateTextField("dateAwarded").add(
                        DatePickerUtil.getDatePicker()));
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantId));
            }
        });
        form.add(new CustomDateTextField("startDate").add(DatePickerUtil.getDatePicker()));
        form.add(new CustomDateTextField("endDate").add(DatePickerUtil.getDatePicker()));
        form.add(new Label("courseLabel", "Course"));
        form.add(new DropDownChoice("course", new CourseListModel(courseService)) {

            @Override
            public boolean isNullValid() {
                return Boolean.TRUE;
            }

        });
        form.add(new DropDownChoice("qualificationType", Arrays.asList(QualificationType.values())));
        add(form);
        add(new Label("registrant.fullname"));
        add(new FeedbackPanel("feedback"));
        add(new Link<Void>("addInstitutionPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionEditPage(null, null, null));
            }

            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });

    }

    private final class LoadableDetachableRegistrantQualificationModel
            extends LoadableDetachableModel<RegistrantQualification> {

        private Long id;
        private Registrant registrant;

        public LoadableDetachableRegistrantQualificationModel(Long id) {
            this.id = id;
        }

        public LoadableDetachableRegistrantQualificationModel(Registrant registrant) {
            this.registrant = registrant;
        }

        @Override
        protected RegistrantQualification load() {
            RegistrantQualification registrantQualification = null;

            if (id == null) {
                registrantQualification = new RegistrantQualification();
                registrantQualification.setRegistrant(registrant);
            } else {
                registrantQualification = registrantQualificationService.get(id);
            }

            return registrantQualification;
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
