package zw.co.hitrac.council.business.service;

import java.util.List;
import zw.co.hitrac.council.business.domain.RegistrantCPDCategory;
import zw.co.hitrac.council.business.domain.RegistrantCPDItem;

/**
 *
 * @author Constance Mabaso
 */
public interface RegistrantCPDItemService extends IGenericService<RegistrantCPDItem> {
    
    public List<RegistrantCPDItem> getRegistrantCPDItems(RegistrantCPDCategory registrantCPDCategory);
}
