package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.GeneralParametersDAO;
import zw.co.hitrac.council.business.domain.GeneralParameters;
import zw.co.hitrac.council.business.domain.RequiredParameter;
import zw.co.hitrac.council.business.service.GeneralParametersService;

import java.util.List;

/**
 *
 * @author Edward Zengeni
 */
@Service
@Transactional
public class GeneralParametersServiceImpl implements GeneralParametersService {

    @Autowired
    private GeneralParametersDAO generalParameterDAO;

    @Transactional
    public GeneralParameters save(GeneralParameters generalParameters) {
        List<GeneralParameters> list = findAll();
        if (generalParameters.getId() == null && !list.isEmpty()) {
            throw new IllegalStateException("Only one row is allowed for parameters!!!!!");
        } else if (generalParameters.getId() != null && get(generalParameters.getId()) == null) {
            throw new IllegalStateException("Row does not exist");
        } else if (list.size() >= 2) {
            throw new IllegalStateException("Only one row is allowed for parameters!!!!!");
        }
        return generalParameterDAO.save(generalParameters);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<GeneralParameters> findAll() {
        return generalParameterDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public GeneralParameters get(Long id) {
        return generalParameterDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<GeneralParameters> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public GeneralParameters get() {
        List<GeneralParameters> parameters = generalParameterDAO.findAll();
        if (parameters.isEmpty()) {
            return new GeneralParameters();
        } else if (parameters.size()== 1) {
            return parameters.get(0);
        } else {
            throw new IllegalStateException("Only one row is allowed for parameters!!!!!");
        }
    }


    /**
     * A DAO setter method to facilitate mocking
     *
     * @param generalParameterDAO
     */
    public void setGeneralParameterDAO(GeneralParametersDAO generalParameterDAO) {
        this.generalParameterDAO = generalParameterDAO;
    }
}
