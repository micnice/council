/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.examinations.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.examinations.ExamDao;
import zw.co.hitrac.council.business.domain.ModulePaper;
import zw.co.hitrac.council.business.domain.examinations.Exam;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.service.examinations.ExamService;

import java.util.List;

/**
 *
 * @author tidza
 */
@Service
@Transactional
public class ExamServiceImpl implements ExamService {

    @Autowired
    private ExamDao examDao;

    @Transactional
    public Exam save(Exam t) {
        return examDao.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Exam> findAll() {
        return examDao.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Exam get(Long id) {
        return examDao.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Exam> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setExamDao(ExamDao examDao) {
        this.examDao = examDao;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Exam> findAll(ExamSetting examSetting) {
        return examDao.findAll(examSetting);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ModulePaper> findExamPapers(ExamSetting examSetting) {
        return examDao.findExamPapers(examSetting);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Exam getExam(ExamSetting examSetting, ModulePaper modulePaper) {
        return examDao.getExam(examSetting, modulePaper);
    }
}
