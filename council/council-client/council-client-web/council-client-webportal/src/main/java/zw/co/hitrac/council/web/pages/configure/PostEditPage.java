package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Post;
import zw.co.hitrac.council.business.service.PostService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Charles Chigoriwa
 */
public class PostEditPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private PostService postService;

    public PostEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<Post>(new LoadableDetachablePostModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));

        Form<Post> form = new Form<Post>("form", (IModel<Post>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                postService.save(getModelObject());
                setResponsePage(new PostViewPage(getModelObject().getId()));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink", PostListPage.class));
        add(form);
    }

    private final class LoadableDetachablePostModel extends LoadableDetachableModel<Post> {
        private Long id;

        public LoadableDetachablePostModel(Long id) {
            this.id = id;
        }

        @Override
        protected Post load() {
            if (id == null) {
                return new Post();
            }

            return postService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
