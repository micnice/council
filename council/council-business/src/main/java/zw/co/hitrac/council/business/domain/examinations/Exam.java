/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain.examinations;

import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.domain.BaseIdEntity;
import zw.co.hitrac.council.business.domain.ModulePaper;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author tdhlakama
 */
@Entity
@Table(name="exam")
@Audited
public class Exam extends BaseIdEntity implements Serializable {

    private ModulePaper modulePaper;
    private Date examDate;
    private ExamSetting examSetting;
    private Double passMark = 50.0; //pass mark default to 50
    private Date examTime = new Date();
    
    @ManyToOne
    public ModulePaper getModulePaper() {
        return modulePaper;
    }

    public void setModulePaper(ModulePaper modulePaper) {
        this.modulePaper = modulePaper;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getExamDate() {
        return examDate;
    }

    public void setExamDate(Date examDate) {
        this.examDate = examDate;
    }
    
    @Temporal(javax.persistence.TemporalType.TIME)
    public Date getExamTime() {
        return examTime;
    }

    public void setExamTime(Date examTime) {
        this.examTime = examTime;
    }
    
    @ManyToOne
    public ExamSetting getExamSetting() {
        return examSetting;
    }

    public void setExamSetting(ExamSetting examSetting) {
        this.examSetting = examSetting;
    }

    @Override
    public String toString() {
        return modulePaper.getName();
    }

    public Double getPassMark() {
        return passMark;
    }

    public void setPassMark(Double passMark) {
        this.passMark = passMark;
    }  

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + (this.modulePaper != null ? this.modulePaper.hashCode() : 0);
        hash = 97 * hash + (this.getId() != null ? this.getId().hashCode() : 0);
        return hash;
}

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Exam other = (Exam) obj;
        if (this.modulePaper != other.modulePaper && (this.modulePaper == null || !this.modulePaper.equals(other.modulePaper))) {
            return false;
        }
        if (this.getId() != other.getId() && (this.getId() == null || !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }
       
}
