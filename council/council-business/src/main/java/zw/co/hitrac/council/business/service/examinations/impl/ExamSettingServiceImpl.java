/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.examinations.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.examinations.ExamSettingDao;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.service.examinations.ExamSettingService;

import java.util.List;
import zw.co.hitrac.council.business.domain.examinations.ExamYear;

/**
 *
 * @author tidza
 */
@Service
@Transactional
public class ExamSettingServiceImpl implements ExamSettingService {

    @Autowired
    private ExamSettingDao examSettingDao;

    @Transactional
    public ExamSetting save(ExamSetting t) {
        return examSettingDao.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ExamSetting> findAll() {
        return examSettingDao.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public ExamSetting get(Long id) {
        return examSettingDao.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ExamSetting> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public void setExamSettingDao(ExamSettingDao examSettingDao) {
        this.examSettingDao = examSettingDao;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ExamSetting> getExamSettings(Boolean active){
    return examSettingDao.getExamSettings(active);
    }
    
    public List<ExamSetting> findByExamYear(ExamYear examYear){
    return examSettingDao.findByExamYear(examYear);
    }
}
