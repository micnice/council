/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.reports;

import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.TypeOfService;
import zw.co.hitrac.council.business.domain.reports.CustomerProductSale;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.CouncilDurationService;
import zw.co.hitrac.council.business.service.RegistrantCpdService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.accounts.ReceiptItemService;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.reports.CustomerSalesReport;
import zw.co.hitrac.council.reports.RegistrantDataDBReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.models.CouncilDurationListModel;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author hitrac
 */
public class CpdPointsReportPage extends IReportPage {

    private CouncilDuration councilDuration;
    private CouncilDurationListModel councilDurationListModel;
    @SpringBean
    private CouncilDurationService councilDurationService;
    @SpringBean
    private RegistrantCpdService registrantCpdService;
    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private DataSource dataSource;
    @SpringBean
    private ReceiptItemService receiptItemService;
    private HrisComparator hrisComparator;

    public CpdPointsReportPage() {

        add(new FeedbackPanel("feedback"));
        PropertyModel<CouncilDuration> councilDurationModel = new PropertyModel<CouncilDuration>(this, "councilDuration");
        Form<?> form = new Form("form");
        form.add(new DropDownChoice("councilDuration", councilDurationModel, new CouncilDurationListModel(councilDurationService)).setRequired(true));

        form.add(new Button("NoAdequateCpdPointsButPaid") {

            @Override
            public void onSubmit() {
                Map parameters = new HashMap();
                RegistrantDataDBReport registrantDataDBReport = new RegistrantDataDBReport();
                try {

                    parameters.put("comment", "DETAILED REGISTER FOR - " + councilDuration.getName() + " PERIOD WITH INADEQUATE/NO CPD POINTS BUT PAYMENT DONE");

                    String idList = ReportResourceUtils.getRegistrantIdListFromRegistrantList(registrantCpdService.getPaidAnnualFeeAndInadequateCPDS(councilDuration));

                    parameters.put("list", idList);
                    final Connection connection = dataSource.getConnection(); //DBConnect.getConnection();
                    ByteArrayResource resource = null;
                    try {
                        resource = ReportResourceUtils.getReportResource(registrantDataDBReport, ContentType.PDF, connection, parameters);
                    } catch (JRException ex) {
                        Logger.getLogger(CpdPointsReportPage.class.getName()).log(Level.SEVERE, null, ex);
                    } finally {
                        connection.close();
                    }
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (SQLException ex) {
                    ex.printStackTrace(System.out);
                }
            }

        });
        form.add(new Button("process") {
            @Override
            public void onSubmit() {
                try {

                    List<CustomerProductSale> productSales = new ArrayList<>();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    for (RegistrantData r : registrantCpdService.getPaidAnnualFeeAndInadequateCPDS(councilDuration)) {

                        CustomerProductSale productSale = new CustomerProductSale();

                        final Registrant registrant = registrantService.get(r.getId());

                        productSale.setAmountPaid(BigDecimal.ZERO);

                        productSale.setAmountPaid(receiptItemService.getTotalAmountPaidForByAccountAndTypeOfServiceAndCouncilDuration(registrant.getCustomerAccount().getAccount(),
                                TypeOfService.ANNUAL_FEES, councilDuration));

                        productSale.setRegistrant(registrant);

                        productSales.add(productSale);
                    }

                    productSales = (List<CustomerProductSale>) hrisComparator.sortProductSaleByCustomerName(productSales);

                    CustomerSalesReport cashReceiptHeader = new CustomerSalesReport();
                    parameters.put("comment", "DETAILED REGISTER FOR - " + councilDuration.getName() + " PERIOD WITH INADEQUATE/NO CPD POINTS BUT PAYMENT DONE");
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(cashReceiptHeader,
                            contentType, productSales, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);
                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        add(form);

    }

}
