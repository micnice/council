package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.RegistrantCPDCategory;
import zw.co.hitrac.council.business.service.RegistrantCPDCategoryService;

/**
 *
 * @author Constance Mabaso
 */
public class RegistrantCPDCategoryViewPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private RegistrantCPDCategoryService registrantCPDCategoryService;

    public RegistrantCPDCategoryViewPage(Long id) {
        CompoundPropertyModel<RegistrantCPDCategory> model =
            new CompoundPropertyModel<RegistrantCPDCategory>(new LoadableDetachableRegistrantCPDCategoryModel(id));

        setDefaultModel(model);
        add(new Link<RegistrantCPDCategory>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantCPDCategoryEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", RegistrantCPDCategoryListPage.class));
        add(new Label("name"));
        add(new Label("maximumPoints"));
        add(new Label("registrantCPDCategory", model.bind("name")));
    }

    private final class LoadableDetachableRegistrantCPDCategoryModel
            extends LoadableDetachableModel<RegistrantCPDCategory> {
        private Long id;

        public LoadableDetachableRegistrantCPDCategoryModel(Long id) {
            this.id = id;
        }

        @Override
        protected RegistrantCPDCategory load() {
            return registrantCPDCategoryService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
