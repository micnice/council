package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.domain.accounts.Product;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author Matiashe Michael
 * @author Charles Chigoriwa
 */
@Entity
@Table(name = "modulepaper")
@Audited
public class ModulePaper extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    private Course course;

    private Product product;

    private Long position;

    @ManyToOne
    @JoinColumn
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @OneToOne
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Long getPosition() {
        return position;
    }

    public void setPosition(Long position) {
        this.position = position;
    }

}
