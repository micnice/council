package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.RegistrantDAO;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.RegistrantService;

import java.util.Date;
import java.util.List;

/**
 * @author Takunda Dhlakama
 * @author Charles Chigoriwa
 * @author Michael Matiashe
 */
@Service
@Transactional
public class RegistrantServiceImpl implements RegistrantService {

    @Autowired
    private RegistrantDAO registrantDAO;
    @Autowired
    private RegistrationProcess registrationProcess;

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public void setRegistrantDAO(RegistrantDAO registrantDAO) {

        this.registrantDAO = registrantDAO;
    }

    @Override
    public List<RegistrantData> getRegistrantsWithBirthDays() {

        return registrantDAO.getRegistrantsWithBirthDays(new Date());
    }

    @Override
    public List<RegistrantData> getRegistrantsWithBirthDays(Date date) {

        return registrantDAO.getRegistrantsWithBirthDays(date);
    }

    public List<Registrant> getApprovingRegistrants() {

        return registrantDAO.getApprovingRegistrants();
    }

    public Registrant createUids(Registrant registrant) {

        return registrantDAO.createUids(registrant);
    }

    public void createRegistrantAccounts() {

        registrantDAO.createRegistrantAccounts();
    }

    public void updateCustomerAndAccountDetails() {

        for(Registrant r :findAll()){
            registrationProcess.updateCustomerAndAccountDetails(r);
        }
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Registrant> getRegistrants(String query, String gender, Citizenship citizenship, Boolean dead) {

        return registrantDAO.getRegistrants(query, gender, citizenship, dead);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getRegistrantList(String query, String gender, Citizenship citizenship, Boolean dead) {

        return registrantDAO.getRegistrantList(query, gender, citizenship, dead);
    }

    public List<RegistrantData> getSupervisorlist(String query, Boolean dead) {

        return registrantDAO.getSupervisorList(query, dead);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Registrant> getDeRegistered() {

        return registrantDAO.getDeRegistrants();
    }

    public List<RegistrantData> getVoidedRegistrants() {
        return registrantDAO.getVoidedRegistrants();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getRegistrants() {

        return registrantDAO.getRegistrants();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getRegistrantAgeByParameter(Integer lowerLimit, Integer upperLimit) {

        return registrantDAO.getRegistrantAgeByParameter(lowerLimit, upperLimit);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Registrant getRegistrant(Customer customer) {

        return registrantDAO.getRegistrant(customer);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Registrant getRegistrantByAccount(Customer customer) {

        return registrantDAO.getRegistrantByAccount(customer);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Registrant getRegistrantByAccountData(Account account) {

        return registrantDAO.getRegistrantByAccountData(account);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Boolean getUniqueRegistrationNumber(String registrationNumber) {

        return registrantDAO.getUniqueRegistrationNumber(registrationNumber);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Boolean getRegistrantIdentificationNumber(String idNumber) {

        return registrantDAO.getRegistrantIdentificationNumber(idNumber);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Registrant getRegistrantByRegistrationNumber(String registrationNumber) {

        return registrantDAO.getRegistrantByRegistrationNumber(registrationNumber);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Registrant getRegistrantByIdentificationNumber(String idNumber) {

        return registrantDAO.getRegistrantByIdentificationNumber(idNumber);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Long> getIds() {

        return registrantDAO.getIds();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Registrant> getRegistrantWithDebts() {

        return registrantDAO.getRegistrantWithDebts();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Registrant> getRegistrantWithCarryForwards() {

        return registrantDAO.getRegistrantWithCarryForwards();
    }

    @Transactional
    public Registrant activateRegistrantManually(Registrant registrant) {

        registrant.setDead(Boolean.FALSE);
        registrant.setRetired(Boolean.FALSE);
        registrant.setVoided(Boolean.FALSE);
        return this.save(registrant);
    }

    @Transactional
    public Registrant save(Registrant registrant) {

        return registrantDAO.save(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Registrant> findAll() {

        return registrantDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Registrant get(Long id) {

        return registrantDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Registrant> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getRegistrantCorrections() {

        return registrantDAO.getRegistrantCorrections();
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Registrant> getRegistrantsWithNoOrInvalidNationalIDs(Citizenship citizenship, IdentificationType nationalIdentificationType) {

        return registrantDAO.getRegistrantsWithNoOrInvalidNationalIDs(citizenship, nationalIdentificationType);

    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public boolean hasValidIdentifications(Registrant registrant, List<IdentificationValidationRule> validationRules) {

        return registrantDAO.hasValidIdentifications(registrant, validationRules);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getRegistrantDateCorrections() {

        return registrantDAO.getRegistrantDateCorrections();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getDeceasedRegistrants() {

        return registrantDAO.getDeceasedRegistrants();
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getRegistrantsWithFirstRenewalWithoutSecondRenewal(Course course,
                                                                                   CouncilDuration firstDuration,
                                                                                   CouncilDuration secondDuration,
                                                                                   AddressType addressType) {

        return registrantDAO.getRegistrantsWithFirstRenewalWithoutSecondRenewal(course, firstDuration, secondDuration, addressType);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getRegistrantsWithFirstRenewalWithoutSecondRenewalInRegistration(Course course,
                                                                                                 CouncilDuration firstDuration,
                                                                                                 CouncilDuration secondDuration,
                                                                                                 AddressType addressType){
    return registrantDAO.getRegistrantsWithFirstRenewalWithoutSecondRenewalInRegistration(course,firstDuration,secondDuration,addressType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getRetiredRegistrants() {

        return registrantDAO.getRetiredRegistrants();
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Registrant> findByCitizenship(Citizenship citizenship) {

        return registrantDAO.findByCitizenship(citizenship);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Registrant findByIdNumber(String idNumber) {

        return registrantDAO.findByIdNumber(idNumber);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getRegistrantsWithConditions(CouncilDuration councilDuration) {

        return registrantDAO.getRegistrantsWithConditions(councilDuration);
    }
     @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getRegistrantsWithConditions(CouncilDuration councilDuration, Conditions condition) {
                return registrantDAO.getRegistrantsWithConditions(councilDuration, condition);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getRegistrantsWhoOwnInstitutions() {

        return registrantDAO.getRegistrantsWhoOwnInstitutions();
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getRegistrantsWhoSuperviseInstitutions() {

        return registrantDAO.getRegistrantsWhoSuperviseInstitutions();
    }
}
