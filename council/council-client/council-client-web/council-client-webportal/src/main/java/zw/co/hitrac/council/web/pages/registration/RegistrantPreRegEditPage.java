package zw.co.hitrac.council.web.pages.registration;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.accounts.documents.InvoicePage;

/**
 *
 * @author Takunda Dhlakama
 * @author Matiashe Michael
 */
public class RegistrantPreRegEditPage extends TemplatePage {

    @SpringBean
    private RegistrantService registrantService;
    
    @SpringBean
    private RegistrationProcess registrationProcess;
  
     public RegistrantPreRegEditPage(Long id) {
        menuPanelManager.getRegistrantionPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantionPanel().setRegistrantEditPageCurrent(true);
        setDefaultModel(new CompoundPropertyModel<Registrant>(new LoadableDetachableRegistrantModel(id)));

        Form<Registrant> form = new Form<Registrant>("form", (IModel<Registrant>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    //pre-registrant select payment
                    Registrant registrant = registrationProcess.preRegistrant(getModelObject());
                    setResponsePage(new InvoicePage(registrant.getCustomerAccount(),Boolean.FALSE));
                } catch (CouncilException e) {
                    error(e.getMessage());
                }
            }
        };

        form.add(new TextField<String>("firstname").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("lastname").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("middlename"));
        form.add(new TextField<String>("registrationNumber"));
        form.add(new BookmarkablePageLink<Void>("registrantListPage", RegistrantListPage.class));
        add(form);
        add(new FeedbackPanel("feedback"));
    }

    private final class LoadableDetachableRegistrantModel extends LoadableDetachableModel<Registrant> {

        private Long id;

        public LoadableDetachableRegistrantModel(Long id) {
            this.id = id;
        }

        @Override
        protected Registrant load() {
            Registrant registrant = null;

            if (id == null) {
                registrant = new Registrant();
            } else {
                registrant = registrantService.get(id);
            }

            return registrant;
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
