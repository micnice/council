
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.ProvinceDAO;
import zw.co.hitrac.council.business.domain.Province;
import zw.co.hitrac.council.business.service.ProvinceService;

import java.util.List;

/**
 *
 * @author Kelvin Goredema
 */

@Service
@Transactional
public class ProvinceServiceImpl implements ProvinceService {
    
    
     @Autowired
    private ProvinceDAO provinceDAO;
    

    @Transactional
    public Province save(Province province) {
       return provinceDAO.save(province);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Province> findAll() {
        return provinceDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Province get(Long id) {
       return provinceDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Province> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setProvinceDAO(ProvinceDAO provinceDAO) {

        this.provinceDAO = provinceDAO;
    }
    
    
}
