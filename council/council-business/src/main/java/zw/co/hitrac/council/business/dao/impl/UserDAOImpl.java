/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.impl;

import org.jasypt.springsecurity3.authentication.encoding.PasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.UserDAO;
import zw.co.hitrac.council.business.dao.repo.UserRepository;
import zw.co.hitrac.council.business.domain.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Clive Gurure
 * @author Michael Matiashe
 */
@Repository
public class UserDAOImpl implements UserDAO {

    @Autowired
    private PasswordEncoder encoder;
    @Autowired
    private UserRepository userRepository;
    @PersistenceContext
    private EntityManager entityManager;

    public User save(User user) {
        //If user has been saved before -- do not re-encrypt password
        user.setPassword(getEncodedPassword(user.getPassword()));
        user.setIsEncrypted(Boolean.TRUE);
        return userRepository.save(user);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User get(Long id) {
        return userRepository.findOne(id);
    }
//
//    public User get(String username, String password) {
//        List<User> users = entityManager.createQuery("select u from User u where (u.username=:username and u
// .password=:password) and u.retired=:retired")
//                .setParameter("retired", Boolean.FALSE)
//                .setParameter("username", username)
//                .setParameter("password", configurablePasswordEncryptor.encryptPassword(password)).getResultList();
//        if (users.isEmpty()) {
//            return null;
//        } else {
//            return users.get(0);
//        }
//    }

    public List<User> getAuthorisers() {
        Boolean authoriser = Boolean.TRUE;
        return entityManager.createQuery("SELECT u FROM User u WHERE u.authoriser=:authoriser and u" +
                ".retired=:retired").setParameter("authoriser", authoriser).setParameter("retired", Boolean.FALSE)
                .getResultList();
    }

    public List<User> getApplicationpProcessors() {
        return entityManager.createQuery("select u from User u where u.applicationProcessor=:applicationProcessor and" +
                " u.retired=:retired").setParameter("applicationProcessor", Boolean.TRUE).setParameter("retired",
                Boolean.FALSE).getResultList();
    }

    public List<User> getCouncilProcessors() {
        return entityManager.createQuery("select u from User u where u.councilProcessor=:councilProcessor and u" +
                ".retired=:retired").setParameter("councilProcessor", Boolean.TRUE).setParameter("retired", Boolean
                .FALSE).getResultList();
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose
      // Tools | Templates.
    }

    //make this thread safe
    public synchronized void encryptAllPasswords() {

        for (User user : findUsersWithPlainTextPasswords()) {

            user.setPassword(getEncodedPassword(user.getPassword()));
            user.setIsEncrypted(Boolean.TRUE);
            userRepository.save(user);

        }
    }

    private List<User> findUsersWithPlainTextPasswords() {

        List<User> users = userRepository.findUsersWithPlainTextPasswords();
        return users == null ? Collections.emptyList() : users;
    }

    private String getEncodedPassword(String plainTextPassword) {

        //According to the javadoc method ignores salt as it generates one, safe to pass null
        return encoder.encodePassword(plainTextPassword, null);
    }

    @Override
    public Optional<User> findByNameIgnoreCase(String userName) {

        Objects.requireNonNull(userName, "User name is required");
        return Optional.ofNullable(userRepository.findByUsernameIgnoreCase(userName));
    }

}
