/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.RegistrantDisciplineDAO;
import zw.co.hitrac.council.business.dao.repo.RegistrantDisciplineRepository;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantDiscipline;

/**
 *
 * @author kelvin
 * @author tdhlakama
 */
@Repository
public class RegistrantDisciplineDAOImpl implements RegistrantDisciplineDAO {

    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    private RegistrantDisciplineRepository registrantDisciplineRepository;

    public RegistrantDiscipline save(RegistrantDiscipline registrantDiscipline) {
        return registrantDisciplineRepository.save(registrantDiscipline);
    }

    public List<RegistrantDiscipline> findAll() {
        return registrantDisciplineRepository.findAll();
    }

    public RegistrantDiscipline get(Long id) {
        return registrantDisciplineRepository.findOne(id);
    }

    /**
     * A setter method that will make mocking repo object easier
     *
     * @param registrantDisciplineRepository
     */
    public void setRegistrantDisciplineRepository(RegistrantDisciplineRepository registrantDisciplineRepository) {
        this.registrantDisciplineRepository = registrantDisciplineRepository;
    }

    public List<RegistrantDiscipline> getDisciplines(Registrant registrant) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantDiscipline.class);
        if (registrant == null) {
            return new ArrayList<RegistrantDiscipline>();
        } else {
            criteria.add(Restrictions.eq("registrant", registrant));
        }
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public Date getRegistrantDisciplineDate(Registrant registrant, Course course) {
        if (course == null) {
            return new Date();
        }
        if (registrant == null) {
            return new Date();
        }
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantDiscipline.class);
        criteria.add(Restrictions.eq("registrant", registrant));
        criteria.add(Restrictions.eq("course", course));
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        List<RegistrantDiscipline> list = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        if (list.isEmpty()) {
            return new Date();
        }
        return list.get(0).getRegistrationDate();
    }

    public RegistrantDiscipline getRegistrantDiscipline(Registrant registrant, Course course) {
        if (course == null) {
            return null;
        }
        if (registrant == null) {
            return null;
        }
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantDiscipline.class);
        criteria.add(Restrictions.eq("registrant", registrant));
        criteria.add(Restrictions.eq("course", course));
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        List<RegistrantDiscipline> list = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    public List<RegistrantDiscipline> findAll(Boolean retired) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantDiscipline.class);
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }
}
