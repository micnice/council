package zw.co.hitrac.council.web.pages.accounts.documents;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Check;
import org.apache.wicket.markup.html.form.CheckGroup;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.service.accounts.AccountService;
import zw.co.hitrac.council.business.service.accounts.CustomerService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.accounts.payments.PrepaymentPage;
import zw.co.hitrac.council.web.pages.registration.RegistrantViewPage;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Takunda Dhlakama
 * @author Charles Chigoriwa
 */
public class PayThroughInstitutionPage extends TemplatePage {

    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    AccountService accountService;
    @SpringBean
    CustomerService customerAccountService;
    @SpringBean
    RegistrationService registrationService;
    @SpringBean
    DebtComponentService debtComponentService;
    @SpringBean
    GeneralParametersService generalParametersService;
    private List<DebtComponent> selectedDebtComponents = new ArrayList<DebtComponent>();
    private Long registrantId;

    public PayThroughInstitutionPage(PageParameters parameters) {
        this(parameters.get("registrantId").toLong());
    }

    public PayThroughInstitutionPage(Long id) {
        registrantId = id;
        final CompoundPropertyModel<Registrant> registrantModel = new CompoundPropertyModel<Registrant>(new DetachableRegistrantModel(id, registrantService));
        setDefaultModel(registrantModel);
        add(new Label("fullname"));

        IModel<List<DebtComponent>> debtComponentsModel = new LoadableDetachableModel<List<DebtComponent>>() {
            @Override
            protected List<DebtComponent> load() {
                return debtComponentService.getDebtComponents(registrantModel.getObject().getCustomerAccount());
            }
        };

        Form<Void> form = new Form<Void>("form") {
            @Override
            protected void onSubmit() {
                if (!selectedDebtComponents.isEmpty()) {
                    setResponsePage(new PrepaymentPage(registrantModel.getObject().getCustomerAccount(), selectedDebtComponents));
                }
            }
        };

        CheckGroup<DebtComponent> selectedDebtComponentsGroup = new CheckGroup<DebtComponent>("selectedDebtComponents", this.selectedDebtComponents);
        form.add(selectedDebtComponentsGroup);
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });

        add(form);
        PropertyListView<DebtComponent> eachItem = new PropertyListView<DebtComponent>("eachItem", debtComponentsModel) {
            @Override
            protected void populateItem(final ListItem<DebtComponent> item) {

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(new Check("selected", item.getModel()));
                item.add(new Label("transactionComponent.account.name"));
                item.add(new Label("remainingBalance"));
            }
        };

        selectedDebtComponentsGroup.add(eachItem);

    }

    @Override
    protected void onConfigure() {
        //if billing module disabled - direct to dashboard
        if (!generalParametersService.get().isBillingModule()) {
            if (registrantId != null) {
                setResponsePage(new RegistrantViewPage(registrantId));
            }
        }

    }
}
