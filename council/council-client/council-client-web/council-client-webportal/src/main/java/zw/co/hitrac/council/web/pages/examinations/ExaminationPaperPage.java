
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------
import java.util.ArrayList;
import java.util.List;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.examinations.Exam;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.process.ExamRegistrationProcess;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationService;
import zw.co.hitrac.council.business.service.examinations.ExamResultService;
import zw.co.hitrac.council.business.service.examinations.ExamSettingService;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.web.models.ExamSettingListModel;

/**
 *
 * @author tdhlakama
 */
public final class ExaminationPaperPage extends IExaminationsPage {

    @SpringBean
    private ExamRegistrationService examRegistrationService;
    @SpringBean
    private ExamSettingService examSettingService;
    @SpringBean
    private ExamResultService examResultService;
    @SpringBean
    private ExamRegistrationProcess examRegistrationProcess;
    private Course course;
    private ExamSetting examSetting;
    private HrisComparator hrisComparator = new HrisComparator();

    public ExaminationPaperPage() {
        PropertyModel<ExamSetting> examSettingModel = new PropertyModel<ExamSetting>(this, "examSetting");

        Form<?> form = new Form("form");
        form.add(new DropDownChoice("examSetting", examSettingModel, new ExamSettingListModel(examSettingService), new ChoiceRenderer<ExamSetting>()) {
            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }
        });
        add(form);

        IModel<List<Exam>> modelExams = new LoadableDetachableModel<List<Exam>>() {
            @Override
            protected List<Exam> load() {
                if (examSetting != null) {
                    return (List<Exam>) hrisComparator.sortExams(examSettingService.get(examSetting.getId()).getExamList());
                } else {
                    return new ArrayList<Exam>();
                }
            }
        };
        add(new ExamPanelReportList("examPanelReportList", modelExams));


    }
}
//~ Formatted by Jindent --- http://www.jindent.com
