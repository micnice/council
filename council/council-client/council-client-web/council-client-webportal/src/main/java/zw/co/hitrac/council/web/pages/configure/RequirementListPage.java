package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Requirement;
import zw.co.hitrac.council.business.service.RequirementService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;

/**
 *
 * @author Michael Matiashe
 */
public class RequirementListPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private RequirementService requirementService;

    public RequirementListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new RequirementEditPage(null));
            }
        });

        IModel<List<Requirement>> model = new LoadableDetachableModel<List<Requirement>>() {
            @Override
            protected List<Requirement> load() {
                return requirementService.findAll();
            }
        };
        PropertyListView<Requirement> eachItem = new PropertyListView<Requirement>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<Requirement> item) {
                Link<Requirement> viewLink = new Link<Requirement>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new RequirementViewPage(getModelObject().getId()));
                    }
                };

                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };

        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
