package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------
import java.util.Arrays;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxCheckBox;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.CheckBoxMultipleChoice;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.ProductIssuanceType;

import zw.co.hitrac.council.business.domain.Qualification;
import zw.co.hitrac.council.business.domain.QualificationType;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.QualificationService;
import zw.co.hitrac.council.business.service.RegisterTypeService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.CourseListModel;
import zw.co.hitrac.council.web.models.QualificationListModel;
import zw.co.hitrac.council.web.models.RegisterTypeListModel;

/**
 *
 * @author Charles Chigoriwa
 */
public class QualificationEditPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private QualificationService qualificationService;
    @SpringBean
    private RegisterTypeService registerTypeService;
    @SpringBean
    private CourseService courseService;

    public QualificationEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<Qualification>(new LoadableDetachableQualificationModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));

        Form<Qualification> form = new Form<Qualification>("form", (IModel<Qualification>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                qualificationService.save(getModelObject());
                setResponsePage(new QualificationViewPage(getModelObject().getId()));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new DropDownChoice("productIssuanceType", Arrays.asList(ProductIssuanceType.values())));
        form.add(new TextField<String>("isoCode"));
        form.add(new TextField<String>("prefixName").setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("qualificationType", Arrays.asList(QualificationType.values())));
        form.add(new DropDownChoice("registerType", new RegisterTypeListModel(registerTypeService)));
        form.add(new DropDownChoice("course", new CourseListModel(courseService)));
        form.add(new BookmarkablePageLink<Void>("returnLink", QualificationListPage.class));
        form.add(new CheckBox("retired"));
        add(form);

        final WebMarkupContainer wmc = new WebMarkupContainer("wmc");
        wmc.setVisible(false);
        wmc.setOutputMarkupPlaceholderTag(true);
        form.add(wmc);
        form.add(new AjaxCheckBox("pbq", new PropertyModel(wmc, "visible")) {
            @Override
            protected void onUpdate(AjaxRequestTarget art) {
                art.add(wmc);
            }
        });

        wmc.add(new CheckBoxMultipleChoice("preReq", new QualificationListModel(qualificationService)));
    }

    private final class LoadableDetachableQualificationModel extends LoadableDetachableModel<Qualification> {

        private Long id;

        public LoadableDetachableQualificationModel(Long id) {
            this.id = id;
        }

        @Override
        protected Qualification load() {
            if (id == null) {
                return new Qualification();
            }

            return qualificationService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
