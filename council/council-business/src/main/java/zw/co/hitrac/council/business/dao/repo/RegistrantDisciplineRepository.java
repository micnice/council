/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.RegistrantDiscipline;

/**
 *
 * @author kelvin
 */
public interface RegistrantDisciplineRepository extends CrudRepository<RegistrantDiscipline, Long>{
     public List<RegistrantDiscipline> findAll();   
}
