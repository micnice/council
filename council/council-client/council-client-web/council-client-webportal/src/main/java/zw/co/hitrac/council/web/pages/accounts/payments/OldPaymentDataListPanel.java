/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.accounts.payments;

import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PageableListView;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigator;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.OldReceipt;
import zw.co.hitrac.council.business.process.ReversePaymentProcess;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.accounts.PaymentDetailsService;
import zw.co.hitrac.council.business.service.accounts.OldReceiptService;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.utility.CustomDateLabel;

/**
 *
 * @author tdhlakama
 */
public class OldPaymentDataListPanel extends Panel {
    
    @SpringBean
    OldReceiptService receiptHeaderService;
    @SpringBean
    PaymentDetailsService paymentDetailsService;
    private HrisComparator hrisComparator = new HrisComparator();
    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    ReversePaymentProcess reversePaymentProcess;
    
    public OldPaymentDataListPanel(String id, IModel<List<OldReceipt>> model) {
        super(id);
        PageableListView<OldReceipt> eachItem = new PageableListView<OldReceipt>("eachItem", model, 20) {
            @Override
            protected void populateItem(ListItem<OldReceipt> item) {
                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                item.setModel(new CompoundPropertyModel<OldReceipt>(item.getModel()));
                item.add(new Label("receiptRecordID"));
                item.add(new CustomDateLabel("dateOfReceipt"));
                item.add(new Label("lineTotal"));                
                item.add(new Label("product"));
                item.add(new Label("paymentPurpose"));
                final OldReceipt header = item.getModelObject();                         
            }
        };
        add(new PagingNavigator("navigator", eachItem));
        add(eachItem);
    }

}
