package zw.co.hitrac.council.webservices.api.domain;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Daniel Nkhoma
 */
@XmlRootElement
public class LeanRegistrantAddress {
    
    private Long registrantAddressId;

    public LeanRegistrantAddress() {
    }

    public LeanRegistrantAddress(Long registrantAddressId) {
        this.registrantAddressId = registrantAddressId;
    }

    public Long getRegistrantAddressId() {
        return registrantAddressId;
    }

    public void setRegistrantAddressId(Long registrantAddressId) {
        this.registrantAddressId = registrantAddressId;
    }
}
