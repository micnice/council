/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.RegistrantCondition;

/**
 *
 * @author kelvin
 */
public interface RegistrantConditionRepository extends CrudRepository<RegistrantCondition, Long>{
     public List<RegistrantCondition> findAll();   
}
