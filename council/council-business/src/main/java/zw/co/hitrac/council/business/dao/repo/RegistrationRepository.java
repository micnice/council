
package zw.co.hitrac.council.business.dao.repo;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.business.domain.Registration;


/**
 *
 * @author Kelvin Goredema
 */
public  interface RegistrationRepository extends CrudRepository<Registration, Long>{
    
    public List<Registration> findAll();
//    
//    @Query("select r from Registration r where r.course=:course and r.register=:register and deRegistrationDate is null")
//    public List<Registration> getActiveRegistrations(@Param(value="course") Course course, @Param(value="register") Register register);
    
    public  List<Registration>  findByCourseAndRegisterAndDeRegistrationDateIsNull(Course course,Register register);
}