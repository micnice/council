/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.TitleDAO;
import zw.co.hitrac.council.business.domain.Title;
import zw.co.hitrac.council.business.service.TitleService;

import java.util.List;

/**
 *
 * @author tidza
 */
@Service
@Transactional
public class TitleServiceImpl implements TitleService {

    @Autowired
    private TitleDAO titleDao;

    @Transactional
    public Title save(Title t) {
        return titleDao.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Title> findAll() {
        return titleDao.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Title get(Long id) {
        return titleDao.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Title> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setTitleDAO(TitleDAO titleDAO) {

        this.titleDao = titleDAO;
    }

	@Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Title findByName(String name) {
		return titleDao.findByName(name);
	}
}
