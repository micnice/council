package zw.co.hitrac.council.web.pages.institution;

import zw.co.hitrac.council.web.pages.registration.*;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.DetachableInstitutionModel;

/**
 *
 * @author Michael Matiashe
 */
public class InstitutionContactAddressPage extends TemplatePage {

    @SpringBean
    private InstitutionService institutionService;

    public InstitutionContactAddressPage(Long id) {

        final CompoundPropertyModel<Institution> model = new CompoundPropertyModel<Institution>(new DetachableInstitutionModel(id, institutionService));

        setDefaultModel(model);
        add(new Label("registrant.fullname"));
        add(new RegistrantContactPanelList("registrantContactPanelList", model.getObject()));

        add(new RegistrantAddressPanelList("registrantAddressPanelList", model.getObject()));
        
        add(new Link<Void>("applicationLink") {
            @Override
            public void onClick() {
                if (model.getObject().getRegistered()) {
                    setResponsePage(new InstitutionViewPage(model.getObject().getId()));
                } else {
                    setResponsePage(new RequirementConfirmationPage(model.getObject()));
                }
            }

            @Override
            protected void onConfigure() {
                setVisible(model.getObject().getRegistrant() != null);
            }
        });
        add(new Link<Void>("contactsPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionViewPage(model.getObject().getId()));
            }
        });
    }
}
