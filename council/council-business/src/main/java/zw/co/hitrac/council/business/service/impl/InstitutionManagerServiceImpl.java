package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.InstitutionManagerDAO;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.InstitutionManager;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.service.InstitutionManagerService;

import java.util.List;

/**
 * @author Charles Chigoriwa
 */
@Service
@Transactional
public class InstitutionManagerServiceImpl implements InstitutionManagerService {

    @Autowired
    private InstitutionManagerDAO institutionManagerDAO;


    @Transactional
    public InstitutionManager save(InstitutionManager institutionManager) {
        return institutionManagerDAO.save(institutionManager);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<InstitutionManager> findAll() {
        return institutionManagerDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public InstitutionManager get(Long id) {
        return institutionManagerDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<InstitutionManager> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setInstitutionManagerDAO(InstitutionManagerDAO institutionManagerDAO) {

        this.institutionManagerDAO = institutionManagerDAO;
    }

    @Override
    public List<InstitutionManager> findInstitutionManagers(Registrant registrant, Institution institution) {
        return institutionManagerDAO.findInstitutionManagers(registrant, institution);
    }

    @Override
    public boolean checkDuplicate(Institution institution, Registrant registrant, Boolean supervisor, Boolean practitionerInCharge) {
        return institutionManagerDAO.checkDuplicate(institution, registrant, supervisor, practitionerInCharge);
    }
}
