
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.registration;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PageableListView;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigator;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.service.GeneralParametersService;

/**
 *
 * @author tdhlakama
 */
public class RegistrantDataViewPanel extends Panel {
    
    @SpringBean
    private GeneralParametersService generalParametersService;

    public RegistrantDataViewPanel(String id, IModel<List<RegistrantData>> model) {
        super(id);

        PageableListView<RegistrantData> eachItem = new PageableListView<RegistrantData>("eachItem",model, 20) {
            @Override
            protected void populateItem(final ListItem<RegistrantData> item) {

                PageParameters params = new PageParameters();
                params.add("registrantId", item.getModelObject().getId());
                item.add(new BookmarkablePageLink("registrationOptionPage", RegistrantViewPage.class, params).add(new Label("lastname", item.getModelObject().getLastname())));

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(new Label("firstname", item.getModelObject().getFirstname()));
                item.add(new Label("middlename", item.getModelObject().getMiddlename()));
                item.add(new Label("idNumber", item.getModelObject().getIdNumber()));
                item.add(new Label("registrationNumber", item.getModelObject().getRegistrationNumber()));
                item.add(new Label("gender", item.getModelObject().getGender()));
                item.add(new BookmarkablePageLink("registrantViewPage", RegistrantViewPage.class, params));
                item.add(new Link("takeOnBalance") {
                    @Override
                    public void onClick() {
                        setResponsePage(new TakeOnBalanceEditPage(item.getModelObject().getId()));
                    }

                    @Override
                    protected void onConfigure() {
                        setVisible(generalParametersService.get().getRegistrationByApplication());
                    }
                    
                });

            }
        };

        add(new PagingNavigator("navigator", eachItem));
        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
