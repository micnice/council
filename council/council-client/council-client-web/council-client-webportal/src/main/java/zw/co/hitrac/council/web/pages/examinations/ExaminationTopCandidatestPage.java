
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------

import com.googlecode.wicket.jquery.ui.panel.JQueryFeedbackPanel;
import net.sf.jasperreports.engine.JRException;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationService;
import zw.co.hitrac.council.business.service.examinations.ExamSettingService;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.reports.CountryTopExamResultReport;
import zw.co.hitrac.council.reports.InstitutionTopExamResultReport;
import zw.co.hitrac.council.web.models.ExamSettingListModel;

/**
 * @author tdhlakama
 */
public class ExaminationTopCandidatestPage extends IExaminationsPage {

    @SpringBean
    private ExamRegistrationService examRegistrationService;
    @SpringBean
    private ExamSettingService examSettingService;
    @SpringBean
    private InstitutionService institutionService;
    private Institution institution;
    private ExamSetting examSetting;
    private String candidateNumber;
    private String idNumber;
    private Course course;
    private Integer range;
    @SpringBean
    private CourseService courseService;
    private HrisComparator hrisComparator = new HrisComparator();

    public ExaminationTopCandidatestPage() {
        final FeedbackPanel feedback = new JQueryFeedbackPanel("feedback");
        add(feedback.setOutputMarkupId(true));
        PropertyModel<Institution> institutionModel = new PropertyModel<Institution>(this, "institution");
        PropertyModel<ExamSetting> examSettingModel = new PropertyModel<ExamSetting>(this, "examSetting");
        PropertyModel<Course> courseModel = new PropertyModel<Course>(this, "course");
        PropertyModel<Integer> rangeModel = new PropertyModel<Integer>(this, "range");
        Form<?> form = new Form("form");

        form.add(new DropDownChoice("course", courseModel, courseService.findAll(), new ChoiceRenderer<Course>()) {
            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }

            @Override
            public boolean isRequired() {
                return true;
            }
        }.add(new ErrorBehavior()));
        form.add(new DropDownChoice("institution", institutionModel, new InstitutionsModel()));
        form.add(new DropDownChoice("examSetting", examSettingModel, new ExamSettingListModel(examSettingService)).setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField("range", rangeModel).setRequired(true).add(new ErrorBehavior()));
        add(form);

        form.add(new Button("process") {
            @Override
            public void onSubmit() {
                if (institution != null) {

                    try {
                        List<ExamRegistration> instituionPositions = new ArrayList<ExamRegistration>();
                        List<ExamRegistration> institutionsExamResults = examRegistrationService.getAllExamCandidates(examSetting, course, institution, null, null, null, Boolean.FALSE, null, Boolean.FALSE, false);

                        ///////////////////Successful Candidates//////////////////                    
                        List<ExamRegistration> passed = new ArrayList<ExamRegistration>();
                        for (ExamRegistration e : institutionsExamResults) {
                            if (e.getPassStatus() && e.getCompletedStatus()) {
                                passed.add(e);
                            }
                        }
                        passed = ExamRegistration.sortInstitutionExamRegistrations(passed);
                        ///////////////////institution Top Candidates//////////////////
                        if (examRegistrationService.getTotalNumberOfCandidates(examSetting, course, institution, null, null, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE) == 0) {
                            //sort candidates marks in acsending order
                            for (ExamRegistration ex : passed) {
                                if (ex.getInstitutionPosition() <= range) {
                                    instituionPositions.add(ex);
                                }
                            }
                            instituionPositions = (List<ExamRegistration>) hrisComparator.sortExamCandidateMarks(instituionPositions);
                        }
                        InstitutionTopExamResultReport institutionTopExamResultReport = new InstitutionTopExamResultReport();
                        ContentType contentType = ContentType.PDF;
                        Map parameters = new HashMap();

                        if (course != null) {
                            parameters.put("course", course.getQualification().getName());
                        }

                        if (examSetting != null) {
                            parameters.put("examSetting", examSetting.toString());
                        }

                        parameters.put("comment", "Top " + range + " Candidates at " + institution.getName());

                        ByteArrayResource resource = ReportResourceUtils.getReportResource(institutionTopExamResultReport,
                                contentType, instituionPositions, parameters);
                        IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                RequestCycle.get().getResponse(), null);

                        resource.respond(a);

                        // To make Wicket stop processing form after sending response
                        RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                    } catch (JRException ex) {
                        ex.printStackTrace();
                    }
                } else {
                    try {
                        List<ExamRegistration> countryFinalPositions = new ArrayList<ExamRegistration>();

                        //sort candidates marks in acsending order
                        ///////////////////Country Top Candidates//////////////////
                        if (examRegistrationService.getTotalNumberOfCandidates(examSetting, course, null, null, null, Boolean.FALSE, Boolean.FALSE, false, Boolean.FALSE, Boolean.FALSE) == 0) {

                            List<ExamRegistration> countryExamResults = examRegistrationService.getAllExamCandidates(examSetting, course, null, null, null, null, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE, false);

                            countryExamResults = ExamRegistration.sortCountryExamRegistrations(countryExamResults);

                            for (ExamRegistration ex : countryExamResults) {
                                if (ex.getCountryPosition() <= range) {
                                    countryFinalPositions.add(ex);
                                }
                            }
                            countryFinalPositions = (List<ExamRegistration>) hrisComparator.sortExamCandidateMarks(countryFinalPositions);
                        }
                        CountryTopExamResultReport countryTopExamResultReport = new CountryTopExamResultReport();
                        ContentType contentType = ContentType.PDF;
                        Map parameters = new HashMap();

                        if (course != null) {
                            parameters.put("course", course.getQualification().getName());
                        }

                        if (examSetting != null) {
                            parameters.put("examSetting", examSetting.toString());
                        }

                        parameters.put("comment", "Top " + range + " Candidates in the Country ");
                        ByteArrayResource resource = ReportResourceUtils.getReportResource(countryTopExamResultReport,
                                contentType, countryFinalPositions, parameters);
                        IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                RequestCycle.get().getResponse(), null);

                        resource.respond(a);

                        // To make Wicket stop processing form after sending response
                        RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                    } catch (JRException ex) {
                        ex.printStackTrace();
                    }

                }
            }
        });

    }

    private class InstitutionsModel extends LoadableDetachableModel<List<? extends Institution>> {

        protected List<? extends Institution> load() {
            if (course != null) {
                return (List<Institution>) hrisComparator.sort((new ArrayList<Institution>(courseService.get(course.getId()).getTrainingInstitutions())));
            } else {
                return new ArrayList<Institution>();
            }
        }
    }
}
