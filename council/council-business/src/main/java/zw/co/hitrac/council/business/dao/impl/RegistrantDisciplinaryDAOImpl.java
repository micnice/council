/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.RegistrantDisciplinaryDAO;
import zw.co.hitrac.council.business.dao.repo.RegistrantDisciplinaryRepository;
import zw.co.hitrac.council.business.domain.CaseOutcome;
import zw.co.hitrac.council.business.domain.MisconductType;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantDisciplinary;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;

/**
 * @author hitrac
 */
@Repository
public class RegistrantDisciplinaryDAOImpl implements RegistrantDisciplinaryDAO {

    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    private RegistrantDisciplinaryRepository registrantDisciplinaryRepository;

    public RegistrantDisciplinary save(RegistrantDisciplinary registrantDisciplinary) {
        return registrantDisciplinaryRepository.save(registrantDisciplinary);
    }

    public List<RegistrantDisciplinary> findAll() {
        return registrantDisciplinaryRepository.findAll();
    }

    public RegistrantDisciplinary get(Long id) {
        return registrantDisciplinaryRepository.findOne(id);
    }

    /**
     * A setter method that will make mocking repo object easier
     *
     * @param addressTypeRepository
     */
    public void setRegistrantDisciplinaryRepository(RegistrantDisciplinaryRepository addressTypeRepository) {
        this.registrantDisciplinaryRepository = addressTypeRepository;
    }

    public List<RegistrantDisciplinary> getCurrentUnResolved(Registrant registrant) {
        return entityManager.createQuery("SELECT r FROM RegistrantDisciplinary r WHERE r.registrant=:registrant AND r.dateResolved is null").setParameter("registrant", registrant).getResultList();
    }

    public String getPendingCases(Registrant registrant) {
        if (getCurrentUnResolved(registrant).isEmpty()) {
            return "NO";
        } else {
            return "YES";
        }
    }

    public List<RegistrantDisciplinary> getGuiltyResolvedCases(Registrant registrant) {
        return entityManager.createQuery("SELECT r FROM RegistrantDisciplinary r WHERE r.registrant=:registrant AND r.dateResolved is not null AND r.caseOutcome.guilty= true ").setParameter("registrant", registrant).getResultList();
    }

    public List<RegistrantDisciplinary> getRegistrantDisciplinaries(Registrant registrant) {
        return entityManager.createQuery("SELECT r FROM RegistrantDisciplinary r WHERE r.registrant=:registrant").setParameter("registrant", registrant).getResultList();
    }

    public List<RegistrantDisciplinary> getDisciplinariesByDates(Date startDate, Date endDate, CaseOutcome caseOutcome, MisconductType misconductType, String caseStatus) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantDisciplinary.class);
        if (startDate != null) {
            if (endDate == null) {
                endDate = new Date();
            }
            criteria.add(Restrictions.between("caseDate", startDate, endDate));
        }
        if (caseOutcome != null) {
            criteria.add(Restrictions.eq("caseOutcome", caseOutcome));
        }
        if (misconductType != null) {
            criteria.add(Restrictions.eq("misconductType", misconductType));
        }


        if(caseStatus!=null){
            criteria.add(Restrictions.eq("caseStatus", caseStatus));
        }

        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<RegistrantDisciplinary> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Boolean registrantSuspendedStatus(Registrant registrant) {
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantDisciplinary.class);
        criteria.add(org.hibernate.criterion.Restrictions.eq("registrant", registrant));
        criteria.add(Restrictions.gt("penaltyEndDate", new Date()));
        Criteria r = criteria.createCriteria("caseOutcome");
        r.add(Restrictions.eq("guilty", Boolean.TRUE));
        return !criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list().isEmpty();
    }

    @Override
    public List<RegistrantDisciplinary> find(MisconductType misconductType, int startYear, int endYear) {

        //Standard JPQL has no year function

        Session session = entityManager.unwrap(Session.class);
        org.hibernate.Query query;

        if (misconductType == null) {

            query = session.createQuery("from RegistrantDisciplinary r where YEAR(r.caseDate) between :startYear and :endYear ")
                    .setParameter("startYear", startYear)
                    .setParameter("endYear", endYear);
        } else {

            query = session.createQuery("from RegistrantDisciplinary r where misconductType = :misconductType " +
                    "AND YEAR(r.caseDate) between :startYear and :endYear ")
                    .setParameter("misconductType", misconductType)
                    .setParameter("startYear", startYear)
                    .setParameter("endYear", endYear);
        }

        return query.list();
    }

    @Override
    public List<RegistrantDisciplinary> find(MisconductType misconductType, int yearReported) {

        return find(misconductType, yearReported, yearReported);
    }
}
