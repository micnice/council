package zw.co.hitrac.council.business.domain.accounts;

import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.domain.BaseIdEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Michael Matiashe
 */
@Entity
@Table(name = "carryforwardlog")
@Audited
public class CarryForwardLog extends BaseIdEntity implements Serializable{

    
    private Date datePaid;
    private BigDecimal amount; //total amount paid
    private ReceiptHeader receiptHeader;
    private Boolean canceled;
    private Boolean used;
    private Set<ReceiptHeader> receiptHeaders = new HashSet<ReceiptHeader>();

    @OneToMany
    public Set<ReceiptHeader> getReceiptHeaders() {
        return receiptHeaders;
    }

    public void setReceiptHeaders(Set<ReceiptHeader> receiptHeaders) {
        this.receiptHeaders = receiptHeaders;
    }

    public Boolean getUsed() {
        if(used==null){
            return Boolean.FALSE;
        }else{
            return used;
        }
    }

    public void setUsed(Boolean used) {
        this.used = used;
    }

    
    public Boolean getCanceled() {
        if(canceled==null){
            return Boolean.FALSE;
        }
        return canceled;
    }

    public void setCanceled(Boolean canceled) {
        this.canceled = canceled;
    }
    

    
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDatePaid() {
        return datePaid;
    }

    public void setDatePaid(Date datePaid) {
        this.datePaid = datePaid;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    
    @OneToOne
    public ReceiptHeader getReceiptHeader() {
        return receiptHeader;
    }

    public void setReceiptHeader(ReceiptHeader receiptHeader) {
        this.receiptHeader = receiptHeader;
    }
    
}
