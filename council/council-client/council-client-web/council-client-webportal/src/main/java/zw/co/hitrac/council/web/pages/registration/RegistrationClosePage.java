package zw.co.hitrac.council.web.pages.registration;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.web.pages.TemplatePage;

//~--- JDK imports ------------------------------------------------------------

import java.util.Date;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.web.utility.CustomDateLabel;

/**
 *
 * @author Takunda Dhlakama
 * @author Michael Matiashe
 */
public class RegistrationClosePage extends TemplatePage {

    @SpringBean
    private RegistrationService registrationService;
 
    public RegistrationClosePage(Long id) {
        CompoundPropertyModel<Registration> model = new CompoundPropertyModel<Registration>(new RegistrationClosePage.LoadableDetachableRegistrationModel(id));
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        final Long registrantId = model.getObject().getRegistrant().getId();
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantId);
        setDefaultModel(model);
        Form<Registration> form = new Form<Registration>("form", (IModel<Registration>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                getModel().getObject().setDeRegistrationDate(new Date());
                registrationService.save(getModelObject());
                setResponsePage(new RegistrantViewPage(getModelObject().getRegistrant().getId()));
            }
        };
        form.add(new Label("course"));
        form.add(new Label("register"));
        form.add(new CustomDateLabel("registrationDate"));
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantId));
            }
        });
        add(form);
        add(new Label("registrant.fullname"));
        add(new FeedbackPanel("feedback"));
    }

    private final class LoadableDetachableRegistrationModel extends LoadableDetachableModel<Registration> {

        private Long id;
        private Registrant registrant;

        public LoadableDetachableRegistrationModel(Long id) {
            this.id = id;
        }

        public LoadableDetachableRegistrationModel(Registrant registrant) {
            this.registrant = registrant;
        }

        @Override
        protected Registration load() {
            Registration registration = null;
            if (id == null) {
                registration = new Registration();
                registration.setRegistrant(registrant);
            } else {
                registration = registrationService.get(id);
            }
            return registration;
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
