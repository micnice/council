package zw.co.hitrac.council.business.service.accounts.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.accounts.OldReceiptDAO;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.User;
import zw.co.hitrac.council.business.domain.accounts.OldReceipt;
import zw.co.hitrac.council.business.domain.accounts.PaymentMethod;
import zw.co.hitrac.council.business.domain.accounts.PaymentType;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.service.accounts.OldReceiptService;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Tatenda Chiwandire
 */
@Service
@Transactional
public class OldReceiptServiceImpl implements OldReceiptService {

    @Autowired
    private OldReceiptDAO oldReceiptDAO;

    @Transactional
    public OldReceipt save(OldReceipt t) {
        return oldReceiptDAO.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<OldReceipt> findAll() {
        return oldReceiptDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public OldReceipt get(Long id) {
        return oldReceiptDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<OldReceipt> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setOldReceiptDao(OldReceiptDAO oldReceiptDAO) {
        this.oldReceiptDAO = oldReceiptDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<OldReceipt> getOldRenewalReceipts() {

        return oldReceiptDAO.getOldRenewalReceipts();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<OldReceipt> getReceipts(Long receiptNumber, Product product, PaymentMethod paymentMethod, PaymentType paymentType, Date startDate, Date endDate, User user, Boolean cancel, Boolean bank, String sourceReference) {
        return oldReceiptDAO.getReceipts(receiptNumber, product, paymentMethod, paymentType, startDate, endDate, user, cancel, bank, sourceReference);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<OldReceipt> getReceiptErrors(Long receiptNumber, Product product, PaymentMethod paymentMethod, PaymentType paymentType, Date startDate, Date endDate, User user, Boolean cancel, Boolean bank, String sourceReference) {
        return oldReceiptDAO.getReceiptErrors(receiptNumber, product, paymentMethod, paymentType, startDate, endDate, user, cancel, bank, sourceReference);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<OldReceipt> getReceipts(Product product, Date startDate, Date endDate, Boolean canceled,String sourceReference) {
        return oldReceiptDAO.getReceipts(product, startDate, endDate, canceled, sourceReference);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<OldReceipt> getReceipts(Product product, Registrant registrant) {
        return oldReceiptDAO.getReceipts(product, registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Double getReceiptTotal(Long receiptNumber, Product product, PaymentMethod paymentMethod, PaymentType paymentType, Date startDate, Date endDate, User user, Boolean cancel, Boolean bank) {
        return oldReceiptDAO.getReceiptTotal(receiptNumber, product, paymentMethod, paymentType, startDate, endDate, user, cancel, bank);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Double getReceiptTotal(Product product, Date startDate, Date endDate, Boolean cancel) {
        return oldReceiptDAO.getReceiptTotal(product, startDate, endDate, cancel);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<String> getAccountSourceReferences() {
        return oldReceiptDAO.getAccountSourceReferences();
    }
}
