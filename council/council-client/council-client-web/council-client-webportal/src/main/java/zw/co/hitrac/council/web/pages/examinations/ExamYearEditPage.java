package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.examinations.ExamYear;
import zw.co.hitrac.council.business.service.examinations.ExamYearService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.configure.AdministerDatabasePage;

/**
 *
 * @author Constance Mabaso
 */
public class ExamYearEditPage extends IExaminationsPage {
    @SpringBean
    private ExamYearService examYearService;

    public ExamYearEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<ExamYear>(new LoadableDetachableExamYearModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));

        Form<ExamYear> form = new Form<ExamYear>("form", (IModel<ExamYear>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                examYearService.save(getModelObject());
                setResponsePage(new ExamYearViewPage(getModelObject().getId()));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink", ExamYearListPage.class));
        add(form);
    }

    private final class LoadableDetachableExamYearModel extends LoadableDetachableModel<ExamYear> {
        private Long id;

        public LoadableDetachableExamYearModel(Long id) {
            this.id = id;
        }

        @Override
        protected ExamYear load() {
            if (id == null) {
                return new ExamYear();
            }

            return examYearService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
