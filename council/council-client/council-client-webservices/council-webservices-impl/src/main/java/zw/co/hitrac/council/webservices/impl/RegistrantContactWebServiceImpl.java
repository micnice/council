package zw.co.hitrac.council.webservices.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantContact;
import zw.co.hitrac.council.business.service.GenericService;
import zw.co.hitrac.council.business.service.RegistrantContactService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.webservices.api.RegistrantContactWebService;
import zw.co.hitrac.council.webservices.api.domain.LeanRegistrantContact;

/**
 *
 * @author Daniel Nkhoma
 */
@Component
public class RegistrantContactWebServiceImpl implements RegistrantContactWebService {

    @Autowired
    private RegistrantContactService registrantContactService;
    @Autowired
    private GenericService genericService;
    @Autowired
    private RegistrantService registrantService;

    @Override
    public List<RegistrantContact> registrantContacts(Long registrantId) {
        return registrantContactService.getContacts((Registrant) genericService.getLean("Registrant", registrantId), null);
    }

    @Override
    public List<RegistrantContact> registrantContacts(String registrationNumber) {
        Registrant registrant = registrantService.getRegistrantByRegistrationNumber(registrationNumber);
        return registrant != null ? registrantContactService.getContacts(registrant, null) : Collections.EMPTY_LIST;
    }

    @Override
    public List<RegistrantContact> registrantContacts() {
        return registrantContactService.findAll();
    }

    public List<LeanRegistrantContact> leanRegistrantContacts() {
        List<LeanRegistrantContact> leanRegistrantContacts = new ArrayList<LeanRegistrantContact>();
        for (Long registrantContactId : genericService.getAllLean("RegistrantContact")) {
            LeanRegistrantContact leanRegistrantContact = new LeanRegistrantContact();
            leanRegistrantContact.setRegistrantContactId(registrantContactId);
            leanRegistrantContacts.add(leanRegistrantContact);
        }
        return leanRegistrantContacts;
    }

    @Override
    public RegistrantContact registrantContact(Long id) {
        return registrantContactService.get(id);
    }
}
