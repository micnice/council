package zw.co.hitrac.council.business.dao.accounts;

import zw.co.hitrac.council.business.dao.IGenericDAO;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.Customer;

/**
 *
 * @author tdhlakama
 */
public interface CustomerDAO extends IGenericDAO<Customer> {

    public Customer get(Account account);
    
    public Registrant getRegistrant(Account account);

    public Institution getInstitution(Account account);
}
