package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.EmailMessage;

/**
 *
 * @author Charles Chigoriwa
 */
public interface EmailMessageRepository extends CrudRepository<EmailMessage, Long> {
    public List<EmailMessage> findAll();
}
