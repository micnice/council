package zw.co.hitrac.council.business.domain;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Kelvin Goredema
 */
@Entity
@Table(name = "register")
@Audited
public class Register extends BaseEntity implements Serializable,Comparable<Register> {

    private Set<Requirement> requirements = new HashSet<Requirement>();
    private Set<Conditions> conditions = new HashSet<Conditions>();
    private Integer numberOfMonthsBeforeRenewal = 12;
    private Boolean quarterlyRenewal = Boolean.FALSE;
    private Boolean discipline = Boolean.TRUE;

    public Integer getNumberOfMonthsBeforeRenewal() {
        return numberOfMonthsBeforeRenewal;
    }

    public Boolean isQuarterlyRenewal() {
        if (quarterlyRenewal == null) {
            return Boolean.FALSE;
        }
        return quarterlyRenewal;
    }

    public void setQuarterlyRenewal(Boolean quarterlyRenewal) {
        this.quarterlyRenewal = quarterlyRenewal;
    }

    public void setNumberOfMonthsBeforeRenewal(Integer numberOfMonthsBeforeRenewal) {
        this.numberOfMonthsBeforeRenewal = numberOfMonthsBeforeRenewal;
    }

    @ManyToMany
    public Set<Requirement> getRequirements() {
        return requirements;
    }

    public void setRequirements(Set<Requirement> requirements) {
        this.requirements = requirements;
    }

    @ManyToMany
    public Set<Conditions> getConditions() {
        return conditions;
    }

    public void setConditions(Set<Conditions> conditions) {
        this.conditions = conditions;
    }

    @Transient
    public String getQuarterlyRenewalStatus() {
        if (quarterlyRenewal == null) {
            return "";
        } else {
            return quarterlyRenewal ? "YES" : "NO";
        }
    }

    public Boolean getDiscipline() {
        if (discipline == null) {
            return Boolean.TRUE;
        }
        return discipline;
    }

    public void setDiscipline(Boolean discipline) {
        this.discipline = discipline;
    }

    @Override
    @Transient
    public String toString() {

        return super.toString();
    }

    @Override
    @Transient
    //For use in jasper reports and other uses
    public int compareTo(Register o) {

        return ComparisonChain.start()
                .compare(this.getName(), o.getName(), Ordering.natural().nullsLast())
                .result();
    }
}
