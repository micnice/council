package zw.co.hitrac.council.web.pages.registrar;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.SMSMessage;

import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.business.domain.RegistrantActivityType;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.process.RenewalProcess;
import zw.co.hitrac.council.business.service.CouncilDurationService;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegisterService;
import zw.co.hitrac.council.business.service.RegistrantActivityService;
import zw.co.hitrac.council.business.service.RegistrantContactService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.utils.SendSMS;
import zw.co.hitrac.council.web.models.CourseListModel;
import zw.co.hitrac.council.web.models.RegisterListModel;
import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author Takunda Dhlakama
 */
public class SendSMSPage extends TemplatePage {

    @SpringBean
    private CouncilDurationService councilDurationService;
    @SpringBean
    private RegistrantActivityService registrantActivityService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    private RegistrantActivityType registrantActivityType;
    private CouncilDuration councilDuration;
    private Course course;
    private Register register;
    @SpringBean
    private RegistrationService registrationService;
    @SpringBean
    private RegisterService registerService;
    @SpringBean
    private CourseService courseService;
    @SpringBean
    private RegistrantContactService registrantContactService;
    @SpringBean
    private RenewalProcess renewalProcess;

    public SendSMSPage(final SMSMessage sMSMessage) {
        PropertyModel<Course> courseModel = new PropertyModel<Course>(this, "course");
        PropertyModel<Register> registerModel = new PropertyModel<Register>(this, "register");
        Form<?> form = new Form("form");
        form.add(new DropDownChoice("course", courseModel, new CourseListModel(courseService)));
        form.add(new DropDownChoice("register", registerModel, new RegisterListModel(registerService)));
        add(form);

        form.add(new Button("send") {
            @Override
            public void onSubmit() {
                System.out.println("++++++++Start");
                SendSMS sendSMS = new SendSMS(generalParametersService);
                List<RegistrantData> recepients = new ArrayList<RegistrantData>();
                RegistrantData data1 = new RegistrantData();
                data1.setMobileNumber("+263738513772");
                recepients.add(data1);
                RegistrantData data2 = new RegistrantData();
                data2.setMobileNumber("+263776183301");
                recepients.add(data2);
                RegistrantData data3 = new RegistrantData();
                data3.setMobileNumber("+263736650916");
                recepients.add(data3);
                try {
                    sendSMS.send(null, recepients, sMSMessage.getDetail());
                } catch (IOException ex) {
                    Logger.getLogger(SendSMSPage.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        add(new FeedbackPanel("feedback"));
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
