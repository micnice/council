package zw.co.hitrac.council.web.pages.registration;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Employment;
import zw.co.hitrac.council.business.domain.EmploymentType;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantAddress;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.web.models.InstitutionListModel;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.CustomDateTextField;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

import java.util.Arrays;

import zw.co.hitrac.council.web.models.CityListModel;

/**
 * @author Tatenda Chiwandire
 */
public class EmploymentEditPage extends TemplatePage {

    @SpringBean
    private EmploymentService employmentService;
    @SpringBean
    private PostService postService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private EmploymentStatusService employmentStatusService;
    @SpringBean
    private EmploymentAreaService employmentAreaService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private CityService cityService;
    @SpringBean
    private RegistrantAddressService registrantAddressService;

    public EmploymentEditPage(Long id, IModel<Registrant> registrantModel) {
        CompoundPropertyModel<Employment> model
                = new CompoundPropertyModel<Employment>(new EmploymentEditPage.LoadableDetachableEmploymentModel(id, registrantModel));

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        final Long registrantId = model.getObject().getRegistrant().getId();
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantId);
        setDefaultModel(model);

        Form<Employment> form = new Form<Employment>("form", (IModel<Employment>) getDefaultModel()) {
            @Override
            public void onSubmit() {

                Employment employment = getModelObject();
                employmentService.save(employment);
                setResponsePage(new RegistrantViewPage(employment.getRegistrant().getId()));
            }
        };

        form.add(new TextField<String>("employer"));
        form.add(new Label("postLabel", "Post") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else if (!generalParametersService.get().getRegistrationByApplication()) {
                    setVisible(Boolean.FALSE);
                } else {
                    setVisible(Boolean.TRUE);
                }
            }
        });
        form.add(new DropDownChoice("post", postService.findAll()) {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else if (!generalParametersService.get().getRegistrationByApplication()) {
                    setVisible(Boolean.FALSE);
                } else {
                    setVisible(Boolean.TRUE);
                }
            }
        });

        form.add(new Label("employmentAreaLabel", "EmploymentArea") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else if (!generalParametersService.get().getRegistrationByApplication()) {
                    setVisible(Boolean.FALSE);
                } else {
                    setVisible(Boolean.TRUE);
                }
            }
        });
        form.add(new DropDownChoice("employmentArea", employmentAreaService.findAll()) {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else if (!generalParametersService.get().getRegistrationByApplication()) {
                    setVisible(Boolean.FALSE);
                } else {
                    setVisible(Boolean.TRUE);
                }
            }
        });

        form.add(new Label("employmentStatusLabel", "Employment Status") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else if (!generalParametersService.get().getRegistrationByApplication()) {
                    setVisible(Boolean.FALSE);
                } else {
                    setVisible(Boolean.TRUE);
                }
            }
        });
        form.add(new DropDownChoice("employmentStatus", employmentStatusService.findAll()) {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else if (!generalParametersService.get().getRegistrationByApplication()) {
                    setVisible(Boolean.FALSE);
                } else {
                    setVisible(Boolean.TRUE);
                }
            }
        });
        form.add(new DropDownChoice("employmentType", Arrays.asList(EmploymentType.values())));
        form.add(new Label("institutionLabel", "Institution") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.FALSE);
                }
            }
        });
        form.add(new DropDownChoice("institution", new InstitutionListModel(institutionService)) {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.FALSE);
                }
            }

        });
        form.add(new DropDownChoice("city", new CityListModel(cityService)));
        form.add(new CustomDateTextField("startDate").add(DatePickerUtil.getDatePicker()));
        form.add(new CustomDateTextField("endDate").add(DatePickerUtil.getDatePicker()));
        form.add(new CheckBox("active"));
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantId));
            }
        });
        add(form);
        add(new Label("registrant.fullname"));
    }

    private final class LoadableDetachableEmploymentModel extends LoadableDetachableModel<Employment> {

        private Long id;
        private IModel<Registrant> registrantModel;

        public LoadableDetachableEmploymentModel(Long id, IModel<Registrant> registrantModel) {
            this.id = id;
            this.registrantModel = registrantModel;
        }

        @Override
        protected Employment load() {
            Employment employment = null;
            if (id == null) {
                employment = new Employment();
                final Registrant registrant = registrantModel.getObject();
                employment.setRegistrant(registrant);
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    //Get Bussiness Address -
                    RegistrantAddress address = registrantAddressService.getActiveAddressbyAddressType(registrantModel.getObject(), null);
                    if (address != null) {
                        employment.setEmployer(address.getAddress1());
                    }
                }

            } else {
                employment = employmentService.get(id);
            }

            return employment;
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
