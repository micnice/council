/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.models;

import java.util.List;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.Province;
import zw.co.hitrac.council.business.service.ProvinceService;
import zw.co.hitrac.council.business.utils.HrisComparator;

/**
 *
 * @author kelvin
 */
public class ProvinceListModel extends LoadableDetachableModel<List<Province>> {

    private final ProvinceService provinceService;
    private HrisComparator hrisComparator = new HrisComparator();

    public ProvinceListModel(ProvinceService provinceService) {
        this.provinceService = provinceService;
    }

    @Override
    protected List<Province> load() {
        return (List<Province>) hrisComparator.sort(provinceService.findAll());
    }
}
