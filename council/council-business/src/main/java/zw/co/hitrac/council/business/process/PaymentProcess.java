package zw.co.hitrac.council.business.process;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.domain.Book;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.User;
import zw.co.hitrac.council.business.domain.accounts.*;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.accounts.*;
import zw.co.hitrac.council.business.utils.Context;
import zw.co.hitrac.council.business.utils.CouncilException;

import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author Charles Chigoriwa
 * @author Michael Matiashe
 */
@Service
public class PaymentProcess {

    @Autowired
    private ReceiptHeaderService receiptHeaderService;
    @Autowired
    private ReceiptItemService receiptItemService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private DebtComponentService debtComponentService;
    @Autowired
    private AllocationService allocationService;
    @Autowired
    private TransactionHeaderService transactionHeaderService;
    @Autowired
    private TransactionComponentService transactionComponentService;
    @Autowired
    private PaymentDetailsService paymentDetailsService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private CarryForwardLogService carryForwardLogService;
    @Autowired
    private InvoiceProcess invoiceProcess;
    @Autowired
    private PrepaymentService prepaymentService;
    @Autowired
    private AccountsParametersService accountsParametersService;
    @Autowired
    private TransactionTypeService transactionTypeService;
    @Autowired
    private GeneralParametersService generalParametersService;
    @Autowired
    private RenewalProcess renewalProcess;

    @Transactional
    public PaymentDetails processPayment(PaymentDetails paymentDetails, List<ReceiptItem> receiptItems) {

        List<PaymentDetails> detailsList = paymentDetailsService.findByUid(paymentDetails.getUid());
        if (!detailsList.isEmpty()) {
            throw new CouncilException("Payment Details already saved");
        }

        for (ReceiptItem item : receiptItems) {
            if (item.getPaymentPeriod() == null) {
                item.setPaymentPeriod(paymentDetails.getCouncilDuration());
            }
        }

        if (paymentDetails.getCreatedBy() == null) {
            throw new CouncilException("Please Logout and Login - Session Timed Out");
        }

        if (paymentDetails.getCouncilDuration() == null) {
            throw new CouncilException("Please Select Payment Period");
        }

        if (paymentDetails.getPaymentMethod().getReceiptNumberRequired()) {
            validateDateOfPayment(paymentDetails.getDateOfPayment());
        }

        if (paymentDetails.getPaymentMethod().getDepositDateRequired()) {
            validateDateOfDeposit(paymentDetails.getDateOfDeposit());
        }

        if (!paymentDetails.getPaymentMethod().getDepositDateRequired()) {
            validateAmountTendered(paymentDetails, receiptItems);
        }

        ReceiptHeader receiptHeader = paymentDetails.getReceiptHeader();

        int zeroOrNegativeCompare = receiptHeader.getTotalAmount().compareTo(new BigDecimal("0"));

        if (zeroOrNegativeCompare == 0 || zeroOrNegativeCompare == -1) {
            throw new CouncilException("Amount paid must be greater than zero");
        }

        if (!paymentDetails.getPaymentMethod().getReceiptNumberRequired()) {
            receiptHeader.setGeneratedReceiptNumber(null);
        }

        TransactionType receiptTranType = paymentDetails.getTransactionHeader().getTransactionType();

        if (receiptTranType == null) {
            throw new CouncilException("Receipt transaction type must not be null");
        }

        BigDecimal totalReceiptItemAmount = new BigDecimal("0");
        BigDecimal totalReceiptItemRemainingBalance = new BigDecimal("0");
        for (ReceiptItem receiptItem : receiptItems) {
            if (receiptItem.getDebtComponent().getRemainingBalance().compareTo(receiptItem.getAmount()) == -1) {
                throw new CouncilException("One of the receipt items has an amount greater than remaining balance");
            }
            zeroOrNegativeCompare = receiptItem.getAmount().compareTo(new BigDecimal("0"));
            if (zeroOrNegativeCompare == 0 || zeroOrNegativeCompare == -1) {
                throw new CouncilException("Receipt item amount must be greater than zero");
            }
            receiptItem.setReceiptHeader(receiptHeader);
            totalReceiptItemAmount = totalReceiptItemAmount.add(receiptItem.getAmount());
            totalReceiptItemRemainingBalance = totalReceiptItemRemainingBalance.add(receiptItem.getDebtComponent().getRemainingBalance());
        }

        if (receiptHeader.getTotalAmount().compareTo(totalReceiptItemAmount) == -1) {
            throw new CouncilException("Total amount paid can not be less than total receipt item amount");
        }

        List<DebtComponent> debtComponents = debtComponentService.getDebtComponents(paymentDetails.getCustomer().getAccount());
        if (!debtComponents.isEmpty()) {
            if (debtComponents.size() > receiptItems.size() && receiptHeader.getTotalAmount().compareTo(totalReceiptItemAmount) == 1) {
                throw new CouncilException("Select other items to pay");

            }
        }

        int compare = receiptHeader.getTotalAmount().compareTo(totalReceiptItemRemainingBalance);
        for (ReceiptItem receiptItem : receiptItems) {
            if (compare == 0 || compare == 1) {
                if (receiptItem.getAmount().compareTo(receiptItem.getDebtComponent().getRemainingBalance()) != 0) {
                    throw new CouncilException("Receipt amount must be fully paid");
                }
            }
        }

        Customer customer = customerService.get(paymentDetails.getCustomer().getAccount());

        Account customerAccount = customer.getAccount();

        //in case user is overpaying but with some remaining debts not picked
        if (compare == 1) {
            if (receiptHeader.getTotalAmount().compareTo(customerAccount.getBalance()) == -1) {
                throw new CouncilException("Overpayment while still having other unpaid debt components is not allowed. Please add other debt components.");
            }
        }

        receiptHeader.setDate(new Date());
        receiptHeader.setTransactionType(receiptTranType);
        receiptHeader.setPaymentDetails(paymentDetails);
        receiptHeader.setCreatedBy(paymentDetails.getCreatedBy());
        receiptHeader = receiptHeaderService.save(receiptHeader);
        if (receiptHeader.getTotalAmount().compareTo(totalReceiptItemAmount) == 1) {
            CarryForwardLog carryForwardLog = new CarryForwardLog();
            carryForwardLog.setAmount(receiptHeader.getTotalAmount().subtract(totalReceiptItemAmount));
            carryForwardLog.setDateCreated(new Date());
            carryForwardLog.setUsed(Boolean.FALSE);
            carryForwardLog.setCanceled(Boolean.FALSE);
            carryForwardLog.setReceiptHeader(receiptHeader);
            carryForwardLog.setCreatedBy(receiptHeader.getCanceledBy());
            carryForwardLogService.save(carryForwardLog);
        }
        if (receiptHeader.getCreatedBy() == null) {
            throw new CouncilException("Please Logout and Login - Session Timed Out");
        }

        BigDecimal totalAmount = receiptHeader.getTotalAmount();

        for (ReceiptItem receiptItem : receiptItems) {
            customerAccount.debit(receiptItem.getAmount());
        }

        customerAccount.credit(totalAmount);
        accountService.save(customerAccount);
        // account is refreshed
        Account bankAccount = receiptTranType.getDrLedger();
        bankAccount = accountService.get(bankAccount.getId());
        bankAccount.debit(totalAmount);
        accountService.save(bankAccount);
        // account is refreshed
        Account creditAccount = receiptTranType.getCrLedger();
        creditAccount = accountService.get(creditAccount.getId());
        creditAccount.credit(totalAmount);
        accountService.save(creditAccount);

        TransactionHeader transactionHeader = paymentDetails.getTransactionHeader();
        transactionHeader.setTransactionType(receiptTranType);
        transactionHeader.setDueDate(new Date());
        transactionHeader.setDescription(receiptTranType.getDescription());
        transactionHeaderService.save(transactionHeader);

        TransactionComponent transactionComponent = new TransactionComponent();
        transactionComponent.setAmount(totalAmount);
        transactionComponent.setTransactionHeader(transactionHeader);
        transactionComponent.setAccount(bankAccount);
        transactionComponent.setDueDate(new Date());
        transactionComponentService.save(transactionComponent);

        transactionComponent = new TransactionComponent();
        transactionComponent.setAccount(customerAccount);
        transactionComponent.setAmount(totalAmount.multiply(new BigDecimal("-1")));
        transactionComponent.setDueDate(new Date());
        transactionComponent.setTransactionHeader(transactionHeader);
        transactionComponentService.save(transactionComponent);

        transactionComponent = new TransactionComponent();
        transactionComponent.setAccount(creditAccount);
        transactionComponent.setAmount(totalAmount.multiply(new BigDecimal("-1")));
        transactionComponent.setDueDate(new Date());
        transactionComponent.setTransactionHeader(transactionHeader);
        transactionComponentService.save(transactionComponent);

        for (ReceiptItem receiptItem : receiptItems) {

            receiptItem.setReceiptHeader(receiptHeader);
            if (receiptItem.getPaymentPeriod() == null) {
                receiptItem.setPaymentPeriod(receiptHeader.getPaymentDetails().getCouncilDuration());
            }
            ReceiptItem savedReceiptItem = receiptItemService.save(receiptItem);
            receiptHeader.getReceiptItems().add(savedReceiptItem);
            DebtComponent debtComponent = savedReceiptItem.getDebtComponent();
            BigDecimal amount = savedReceiptItem.getAmount();
            debtComponent.reduceRemainingBalance(amount);
            debtComponentService.save(debtComponent);

            Allocation allocation = new Allocation();
            allocation.setDebtTransactionComponent(debtComponent.getTransactionComponent());
            allocation.setPaymentTransactionComponent(transactionComponent);
            allocation.setAmount(amount);
            allocationService.save(allocation);
        }
        paymentDetails.setReceiptHeader(receiptHeader);

        PaymentDetails details = paymentDetailsService.save(paymentDetails);

        details.setBankAccount(details.getTransactionHeader().getTransactionType().getDrLedger());
        receiptHeader.setPaymentDetails(details);
        receiptHeaderService.save(receiptHeader);

        if (generalParametersService.get().getAllowRenewalBeforeCPDs()) {
            final Registrant registrant = customerService.getRegistrant(customerAccount);
            renewalProcess.createRenewalPaidForCurrentPeriod(registrant);
        }

        return details;

    }

    public void validateDateOfPayment(Date date) {
        if (date.after(new Date())) {
            throw new CouncilException("Receipt cannot be in the future");
        }
    }

    public void validateDateOfDeposit(Date date) {

        if (date.after(new Date())) {

            throw new CouncilException("Deposit date must not be a future date");
        }
    }

    private void validateAmountTendered(PaymentDetails paymentDetails, List<ReceiptItem> receiptItems) {
        BigDecimal amount = BigDecimal.ZERO;
        for (ReceiptItem receiptItem : receiptItems) {
            amount = amount.add(receiptItem.getAmount());

        }

        if (paymentDetails.getReceiptHeader().getTotalAmount().compareTo(amount) > 0) {
            if (!accountsParametersService.get().isAllowCashOverpayment()) {
                throw new CouncilException("Total amount paid can not be more than total receipt item amount");
            }
        }

    }

    @Transactional
    public ReceiptHeader getChangeTransactionType(TransactionType transactionType, ReceiptHeader receiptHeader) {
        Account crAccount = accountService.get(transactionType.getDrLedger().getId());
        Account drAccount = accountService.get(receiptHeader.getTransactionType().getDrLedger().getId());
        if (!transactionType.equals(receiptHeader.getTransactionType())) {
            crAccount.credit(receiptHeader.getTotalAmount());
            accountService.save(crAccount);
            drAccount.debit(receiptHeader.getTotalAmount());
            accountService.save(drAccount);
            receiptHeader.setDateModified(new Date());
            receiptHeader.setTransactionTypeChange(transactionType);
            receiptHeaderService.save(receiptHeader);
        }
        return receiptHeader;
    }

    public void payThroughCarryForward(Account account, Customer customer, TransactionType transactionType, List<DebtComponent> selectedDebtComponents) {

        for (DebtComponent debtComponent : selectedDebtComponents) {
            account = accountService.get(account.getId());
            BigDecimal initialBalance = account.getBalance().multiply(new BigDecimal("-1"));
            final int compareResult = initialBalance.compareTo(debtComponent.getRemainingBalance());

            if (compareResult >= 0) {
                BigDecimal price = debtComponent.getProduct().getProductPrice().getPrice();
                invoiceProcess.saveCustomerNewBalance(customer, transactionType, price);
                invoiceProcess.saveNewBalances(transactionType, account, customer, price);
                invoiceProcess.alterDebtComponentRemainingBalance(debtComponent, account.getBalance(), Boolean.FALSE);

            } else if (compareResult == -1) {

                BigDecimal pricePaid = initialBalance;
                invoiceProcess.saveCustomerNewBalance(customer, transactionType, pricePaid);
                invoiceProcess.saveNewBalances(transactionType, account, customer, pricePaid);
                invoiceProcess.alterDebtComponentRemainingBalance(debtComponent, account.getBalance(), Boolean.FALSE);
            }

        }

        if (generalParametersService.get().getAllowRenewalBeforeCPDs()) {
            final Registrant registrant = customerService.getRegistrant(account);
            renewalProcess.createRenewalPaidForCurrentPeriod(registrant);
        }
    }

    public void payThroughInstitution(Prepayment prepayment, TransactionType transactionType, List<DebtComponent> selectedDebtComponents) {

        for (DebtComponent debtComponent : selectedDebtComponents) {
            BigDecimal initialBalance = prepaymentService.get(prepayment.getId()).getBalance();
            if (initialBalance.multiply(new BigDecimal("-1")).compareTo(new BigDecimal("0")) == 0) { /// if prepayment is used up or being over drawn
                throw new CouncilException("Prepayment has not enough money left for this payment");
            }
            final int compareResult = initialBalance.multiply(new BigDecimal("-1")).compareTo(debtComponent.getRemainingBalance());

            if (compareResult >= 0) { //update prepayment balance and alter debtcomponent
                BigDecimal balance = initialBalance;
                balance = balance.add(debtComponent.getRemainingBalance());
                prepayment.setBalance(balance);
                prepaymentService.save(prepayment);
                invoiceProcess.alterDebtComponentRemainingBalance(debtComponent, initialBalance, Boolean.TRUE);
            } else {
                BigDecimal balance = prepayment.getBalance();
                balance = balance.add(initialBalance.multiply(new BigDecimal("-1")));
                prepayment.setBalance(balance);
                prepaymentService.save(prepayment);
                invoiceProcess.alterDebtComponentRemainingBalance(debtComponent, initialBalance, Boolean.TRUE);
            }

        }

    }

    public void exportToPastel(List<ReceiptItem> receiptItems, Date startDate, Date endDate) {
        String fName = "/home/micnice/Desktop/" + startDate.toString().replace("/", "-") + " - " + endDate.toString().replace("/", "-") + ".csv";

        try {

            FileWriter fw = new FileWriter(fName);
            for (ReceiptItem ra : receiptItems) {
                String code;
                String description;
                description = ra.getDebtComponent().getProduct().getName();
                fw.append(ra.getDateCreated().getMonth() + 1 + "");
                fw.append(',');
                fw.append(Context.getDateFormat().format(ra.getDateCreated()));
                fw.append(',');
                fw.append('G');
                fw.append(',');
                fw.append(ra.getDebtComponent().getProduct().getCode().replace("/", ""));
                fw.append(',');
                fw.append("RC" + ra.getReceiptHeader().getId().toString());
                fw.append(',');
                fw.append(description);
                fw.append(',');
                fw.append("" + ra.getAmount().multiply(new BigDecimal("-1")));
                fw.append(',');
                fw.append("" + 0);
                fw.append(',');
                fw.append("" + 0);
                fw.append(',');
                fw.append("");
                fw.append(',');
                fw.append("");
                fw.append(',');
                fw.append("" + ra.getReceiptHeader().getPaymentDetails().getTransactionHeader().getTransactionType().getCrLedger().getCode().replace("/", ""));
                fw.append(',');
                fw.append("" + 1);
                fw.append(',');
                fw.append("" + 1);
                fw.append(',');
                fw.append("" + 0);
                fw.append(',');
                fw.append("" + 0);
                fw.append(',');
                fw.append("" + 0);
                fw.append(',');
                fw.append("" + ra.getAmount().multiply(new BigDecimal("-1")));
                fw.append(',');
                fw.append('\n');

            }
            fw.flush();
            fw.close();

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

    @Transactional
    public PaymentDetails accountBallancePaysAll(Set<ReceiptItem> receiptItems, User user, Customer customer, CouncilDuration councilDuration) {
        BigDecimal amountUsed = BigDecimal.ZERO;
        for (ReceiptItem receiptItem : receiptItems) {
            receiptItem.setDateCreated(new Date());
            amountUsed.add(receiptItem.getAmount());
            receiptItem.setCreatedBy(user);
            receiptItemService.save(receiptItem);
        }
        PaymentDetails paymentDetails = new PaymentDetails();
        paymentDetails.setTransactionHeader(new TransactionHeader());
        paymentDetails.setReceiptHeader(new ReceiptHeader());
        paymentDetails.getReceiptHeader().setCreatedBy(user);
        paymentDetails.getReceiptHeader().setReceiptItems(receiptItems);
        paymentDetails.getReceiptHeader().setTotalAmount(amountUsed);
        paymentDetails.getReceiptHeader().setDate(new Date());
        paymentDetails.getReceiptHeader().setCreatedBy(user);
        paymentDetails.getReceiptHeader().setPrinted(Boolean.FALSE);
        paymentDetails.setCustomer(customer);
        paymentDetails.setCreatedBy(user);
        paymentDetails.setDateCreated(new Date());
        paymentDetails.getReceiptHeader().setDateCreated(new Date());
        paymentDetails.setDateOfPayment(new Date());
        //TODO: to set council duration if null
        paymentDetails.setCouncilDuration(councilDuration);
        TransactionHeader transactionHeader = paymentDetails.getTransactionHeader();
        TransactionType receiptTransactionType = transactionTypeService.find(Book.CB, accountsParametersService.get().getDefaultPaymentMethod()).get(0);
        transactionHeader.setTransactionType(receiptTransactionType);
        transactionHeader.setDueDate(new Date());
        transactionHeader.setDescription(receiptTransactionType.getDescription());
        transactionHeaderService.save(transactionHeader);

        TransactionComponent transactionComponent = new TransactionComponent();
        transactionComponent.setAmount(amountUsed);
        transactionComponent.setTransactionHeader(transactionHeader);
        transactionComponent.setAccount(receiptTransactionType.getDrLedger());
        transactionComponent.setDueDate(new Date());
        transactionComponentService.save(transactionComponent);

        transactionComponent = new TransactionComponent();
        transactionComponent.setAccount(receiptTransactionType.getCrLedger());
        transactionComponent.setAmount(amountUsed.multiply(new BigDecimal("-1")));
        transactionComponent.setDueDate(new Date());
        transactionComponent.setTransactionHeader(transactionHeader);
        transactionComponentService.save(transactionComponent);

        transactionComponent = new TransactionComponent();
        transactionComponent.setAccount(receiptTransactionType.getCrLedger());
        transactionComponent.setAmount(amountUsed.multiply(new BigDecimal("-1")));
        transactionComponent.setDueDate(new Date());
        transactionComponent.setTransactionHeader(transactionHeader);
        transactionComponentService.save(transactionComponent);
        paymentDetails.setTransactionHeader(transactionHeader);
        paymentDetails.setPaymentMethod(accountsParametersService.get().getDefaultPaymentMethod());
        Account bankAccount = transactionTypeService.find(Book.CB, accountsParametersService.get().getDefaultPaymentMethod()).get(0).getDrLedger();
        paymentDetails.setBankAccount(bankAccount);
        paymentDetails.getReceiptHeader().setTransactionType(receiptTransactionType);
        receiptHeaderService.save(paymentDetails.getReceiptHeader());
        for (ReceiptItem receiptItem : receiptItems) {
            receiptItem.setReceiptHeader(paymentDetails.getReceiptHeader());
            if (receiptItem.getPaymentPeriod() == null) {
                receiptItem.setPaymentPeriod(paymentDetails.getReceiptHeader().getPaymentDetails().getCouncilDuration());
            }
            receiptItemService.save(receiptItem);

        }
        transactionHeaderService.save(paymentDetails.getTransactionHeader());
        paymentDetails = paymentDetailsService.save(paymentDetails);
        paymentDetails.getReceiptHeader().setPaymentDetails(paymentDetails);
        paymentDetails.getReceiptHeader().setTransactionType(receiptTransactionType);
        receiptHeaderService.save(paymentDetails.getReceiptHeader());
        List<CarryForwardLog> carryForwardLogs = carryForwardLogService.getRegistrantCarryForwardLogs(customer);
        if (!carryForwardLogs.isEmpty()) {

            CarryForwardLog carryForwardLog = carryForwardLogs.get(0);
            BigDecimal amount = new BigDecimal(0);
            BigDecimal cfBalance = new BigDecimal(0);

            if (carryForwardLog.getAmount().compareTo(amountUsed) == -1) {
                carryForwardLog.getReceiptHeaders().add(paymentDetails.getReceiptHeader());
                carryForwardLog.getAmount().subtract(amountUsed);
            } else if (carryForwardLog.getAmount().compareTo(amountUsed) == 0) {
                carryForwardLog.getReceiptHeaders().add(paymentDetails.getReceiptHeader());
                carryForwardLog.getAmount().subtract(amountUsed);
                carryForwardLog.setUsed(Boolean.TRUE);
            } else if (carryForwardLog.getAmount().compareTo(amountUsed) == 1) {
                carryForwardLog.getReceiptHeaders().add(paymentDetails.getReceiptHeader());
                amountUsed = amountUsed.subtract(carryForwardLog.getAmount());
                carryForwardLog.setAmount(new BigDecimal("0"));
                carryForwardLogService.save(carryForwardLog);
                if (carryForwardLogs.size() > 1) {
                    carryForwardLog = carryForwardLogs.get(1);
                    if (carryForwardLog.getAmount().compareTo(amountUsed) == -1) {
                        carryForwardLog.getReceiptHeaders().add(paymentDetails.getReceiptHeader());
                        carryForwardLog.getAmount().subtract(amountUsed);
                    } else if (carryForwardLog.getAmount().compareTo(amountUsed) == 0) {
                        carryForwardLog.getReceiptHeaders().add(paymentDetails.getReceiptHeader());
                        carryForwardLog.getAmount().subtract(amountUsed);
                        carryForwardLog.setUsed(Boolean.TRUE);
                    }
                }

            }

            carryForwardLogService.save(carryForwardLog);
        }

        if (generalParametersService.get().getAllowRenewalBeforeCPDs()) {
            final Registrant registrant = customerService.getRegistrant(customer.getAccount());
            renewalProcess.createRenewalPaidForCurrentPeriod(registrant);
        }

        return paymentDetails;
    }

    @Transactional
    public PaymentDetails accountBalancePaysLess(Set<ReceiptItem> receiptItems, User user, Customer customer, CouncilDuration councilDuration) {
        BigDecimal amountUsed = BigDecimal.ZERO;
        for (ReceiptItem receiptItem : receiptItems) {
            receiptItem.setDateCreated(new Date());
            amountUsed.add(receiptItem.getAmount());
            receiptItem.setCreatedBy(user);
            receiptItemService.save(receiptItem);
        }
        PaymentDetails paymentDetails = new PaymentDetails();
        paymentDetails.setTransactionHeader(new TransactionHeader());
        ReceiptHeader receiptHeader = new ReceiptHeader();
        receiptHeader.setCreatedBy(user);
        receiptHeader.setReceiptItems(receiptItems);
        receiptHeader.setTotalAmount(amountUsed);
        receiptHeader.setDate(new Date());
        receiptHeader.setCreatedBy(user);
        receiptHeaderService.save(receiptHeader);
        paymentDetails.setReceiptHeader(receiptHeader);
        paymentDetails.setCustomer(customer);
        paymentDetails.setCreatedBy(user);
        paymentDetails.setDateCreated(new Date());
        paymentDetails.setDateOfPayment(new Date());
        //TODO: to set council duration if null
        paymentDetails.setCouncilDuration(councilDuration);
        paymentDetails.getReceiptHeader().setDateCreated(new Date());
        TransactionHeader transactionHeader = paymentDetails.getTransactionHeader();
        TransactionType receiptTransactionType = transactionTypeService.find(Book.CB, accountsParametersService.get().getDefaultPaymentMethod()).get(0);
        transactionHeader.setTransactionType(receiptTransactionType);
        transactionHeader.setDueDate(new Date());
        transactionHeader.setDescription(receiptTransactionType.getDescription());
        transactionHeaderService.save(transactionHeader);

        TransactionComponent transactionComponent = new TransactionComponent();
        transactionComponent.setAmount(amountUsed);
        transactionComponent.setTransactionHeader(transactionHeader);
        transactionComponent.setAccount(receiptTransactionType.getDrLedger());
        transactionComponent.setDueDate(new Date());
        transactionComponentService.save(transactionComponent);

        transactionComponent = new TransactionComponent();
        transactionComponent.setAccount(receiptTransactionType.getCrLedger());
        transactionComponent.setAmount(amountUsed.multiply(new BigDecimal("-1")));
        transactionComponent.setDueDate(new Date());
        transactionComponent.setTransactionHeader(transactionHeader);
        transactionComponentService.save(transactionComponent);

        transactionComponent = new TransactionComponent();
        transactionComponent.setAccount(receiptTransactionType.getCrLedger());
        transactionComponent.setAmount(amountUsed.multiply(new BigDecimal("-1")));
        transactionComponent.setDueDate(new Date());
        transactionComponent.setTransactionHeader(transactionHeader);
        transactionComponentService.save(transactionComponent);
        paymentDetails.setTransactionHeader(transactionHeader);
        paymentDetails.setPaymentMethod(accountsParametersService.get().getDefaultPaymentMethod());
        Account bankAccount = transactionTypeService.find(Book.CB, accountsParametersService.get().getDefaultPaymentMethod()).get(0).getDrLedger();
        paymentDetails.setBankAccount(bankAccount);
        paymentDetails.getReceiptHeader().setTransactionType(receiptTransactionType);
        receiptHeaderService.save(receiptHeader);
        transactionHeaderService.save(paymentDetails.getTransactionHeader());
        paymentDetailsService.save(paymentDetails);
        receiptHeader.setPaymentDetails(paymentDetails);
        receiptHeader.setTransactionType(receiptTransactionType);
        for (ReceiptItem receiptItem : receiptItems) {
            receiptItem.setReceiptHeader(receiptHeader);
            if (receiptItem.getPaymentPeriod() == null) {
                receiptItem.setPaymentPeriod(paymentDetails.getReceiptHeader().getPaymentDetails().getCouncilDuration());
            }
            receiptItemService.save(receiptItem);
        }
        receiptHeaderService.save(receiptHeader);
        List<CarryForwardLog> carryForwardLogs = carryForwardLogService.getRegistrantCarryForwardLogs(customer);
        if (!carryForwardLogs.isEmpty()) {

            CarryForwardLog carryForwardLog = carryForwardLogs.get(0);
            BigDecimal amount = new BigDecimal(0);
            BigDecimal cfBalance = new BigDecimal(0);

            if (carryForwardLog.getAmount().compareTo(amountUsed) == -1) {
                carryForwardLog.getReceiptHeaders().add(paymentDetails.getReceiptHeader());
                carryForwardLog.getAmount().subtract(amountUsed);
            } else if (carryForwardLog.getAmount().compareTo(amountUsed) == 0) {
                carryForwardLog.getReceiptHeaders().add(paymentDetails.getReceiptHeader());
                carryForwardLog.getAmount().subtract(amountUsed);
                carryForwardLog.setUsed(Boolean.TRUE);
            } else if (carryForwardLog.getAmount().compareTo(amountUsed) == 1) {
                carryForwardLog.getReceiptHeaders().add(paymentDetails.getReceiptHeader());
                amountUsed = amountUsed.subtract(carryForwardLog.getAmount());
                carryForwardLog.setAmount(new BigDecimal("0"));
                carryForwardLogService.save(carryForwardLog);
                if (carryForwardLogs.size() > 1) {
                    carryForwardLog = carryForwardLogs.get(1);
                    if (carryForwardLog.getAmount().compareTo(amountUsed) == -1) {
                        carryForwardLog.getReceiptHeaders().add(paymentDetails.getReceiptHeader());
                        carryForwardLog.getAmount().subtract(amountUsed);
                    } else if (carryForwardLog.getAmount().compareTo(amountUsed) == 0) {
                        carryForwardLog.getReceiptHeaders().add(paymentDetails.getReceiptHeader());
                        carryForwardLog.getAmount().subtract(amountUsed);
                        carryForwardLog.setUsed(Boolean.TRUE);
                    }
                }

            }
            carryForwardLogService.save(carryForwardLog);
        }

        if (generalParametersService.get().getAllowRenewalBeforeCPDs()) {
            final Registrant registrant = customerService.getRegistrant(customer.getAccount());
            renewalProcess.createRenewalPaidForCurrentPeriod(registrant);
        }

        return paymentDetails;
    }
}
