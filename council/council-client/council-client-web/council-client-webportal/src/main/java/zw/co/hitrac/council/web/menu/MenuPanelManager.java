package zw.co.hitrac.council.web.menu;

import java.io.Serializable;

/**
 *
 * @author Charles Chigoriwa
 * @author Michael Matiashe
 */
public class MenuPanelManager implements Serializable {

    private final ConfigureMenuPanel configureMenuPanel;
    private final ChangePasswordMenuPanel changePasswordMenuPanel;
    private final RegistrantionPanel registrantionPanel;
    private final ApplicationPanel applicationPanel;
    private final RegistrantMenuPanel registrantMenuPanel;
    private final RegistrantBackMenuPanel registrantBackMenuPanel;
    private final SearchPanel searchPanel;
    private final AccountingMenuPanel accountingPanel;
    private final ViewReportsMenuPanel viewReportsMenuPanel;
    private final ExamMenuPanel examPanel;
    private final InstitutionPanel institutionPanel;
    private final FinanceMenuPanel financeMenuPanel;
  
    public MenuPanelManager() {
        configureMenuPanel = new ConfigureMenuPanel("configureMenu");
        changePasswordMenuPanel = new ChangePasswordMenuPanel("passwordMenu");
        registrantionPanel = new RegistrantionPanel("registrationMenu");
        registrantMenuPanel = new RegistrantMenuPanel("registrantMenu");
        registrantBackMenuPanel = new RegistrantBackMenuPanel("registrantBackMenu");
        searchPanel = new SearchPanel("searchMenu");
        accountingPanel = new AccountingMenuPanel("accountingMenu");
        financeMenuPanel = new FinanceMenuPanel("financeMenu");
        viewReportsMenuPanel=new ViewReportsMenuPanel("reportsMenu");
        examPanel = new ExamMenuPanel("examMenu");
        applicationPanel=new ApplicationPanel("applicationMenu");
        institutionPanel = new InstitutionPanel("institutionMenu");
        
    }

    public ConfigureMenuPanel getConfigureMenuPanel() {
        return configureMenuPanel;
    }

    public ChangePasswordMenuPanel getChangePasswordMenuPanel() {
        return changePasswordMenuPanel;
    }

    public RegistrantionPanel getRegistrantionPanel() {
        return registrantionPanel;
    }

    public RegistrantMenuPanel getRegistrantMenuPanel() {
        return registrantMenuPanel;
    }

    public RegistrantBackMenuPanel getRegistrantBackMenuPanel() {
        return registrantBackMenuPanel;
    }
    public ApplicationPanel getApplicationPanel(){
        return applicationPanel;
    }

    public SearchPanel getSearchPanel() {
        return searchPanel;
    }

    public AccountingMenuPanel getAccountingPanel() {
        return accountingPanel;
    }  
    
    public ViewReportsMenuPanel getViewReportsPanel() {
        return viewReportsMenuPanel;
    }  

    public ExamMenuPanel getExamPanel() {
        return examPanel;
    }

    public InstitutionPanel getInstitutionPanel() {
        return institutionPanel;
    }
    
    public FinanceMenuPanel getFinanceMenuPanel(){
        return financeMenuPanel;
    }
    
}
