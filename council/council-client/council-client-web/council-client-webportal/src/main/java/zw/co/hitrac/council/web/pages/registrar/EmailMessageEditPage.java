package zw.co.hitrac.council.web.pages.registrar;

import zw.co.hitrac.council.web.pages.configure.*;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.EmailMessage;
import zw.co.hitrac.council.business.service.EmailMessageService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author Takunda Dhlakama
 */
public class EmailMessageEditPage extends TemplatePage {

    @SpringBean
    private EmailMessageService emailMessageService;

    public EmailMessageEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<EmailMessage>(new LoadableDetachableEmailMessageModel(id)));
        Form<EmailMessage> form = new Form<EmailMessage>("form", (IModel<EmailMessage>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                emailMessageService.save(getModelObject());
                setResponsePage(EmailMessageListPage.class);
            }
        };

        form.add(new TextField<String>("subject").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextArea<String>("paragraphOne").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextArea<String>("paragraphTwo").setRequired(true).add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink", EmailMessageListPage.class));
        add(form);
    }

    private final class LoadableDetachableEmailMessageModel extends LoadableDetachableModel<EmailMessage> {

        private Long id;

        public LoadableDetachableEmailMessageModel(Long id) {
            this.id = id;
        }

        @Override
        protected EmailMessage load() {
            if (id == null) {
                return new EmailMessage();
            }
            return emailMessageService.get(id);
        }
    }

}
