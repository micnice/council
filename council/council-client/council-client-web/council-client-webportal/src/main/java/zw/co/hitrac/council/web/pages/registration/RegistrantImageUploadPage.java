package zw.co.hitrac.council.web.pages.registration;

import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.jackrabbit.value.BinaryImpl;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.lang.Bytes;
import sun.net.www.MimeTable;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;

import javax.jcr.Node;
import javax.jcr.Repository;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Charles Chigoriwa
 */
public class RegistrantImageUploadPage extends TemplatePage {

    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private RegistrantService registrantService;

    public RegistrantImageUploadPage(final Model<Registrant> registrantModel) {
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());
        add(new Label("fullname", registrantModel.getObject().getFullname()));
        // Create feedback panels
        final FeedbackPanel uploadFeedback = new FeedbackPanel("uploadFeedback");

        // Add uploadFeedback to the page itself
        add(uploadFeedback);

        final FileUploadForm simpleUploadForm = new FileUploadForm("simpleUpload", registrantModel);
        add(simpleUploadForm);

        add(new Link<Registrant>("registrationOptionPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });
        add(simpleUploadForm);
    }

    public RegistrantImageUploadPage(long id) {
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        final CompoundPropertyModel<Registrant> registrantModel = new CompoundPropertyModel<Registrant>(new DetachableRegistrantModel(id, registrantService));

        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());
        add(new Label("fullname", registrantModel.getObject().getFullname()));
        // Create feedback panels
        final FeedbackPanel uploadFeedback = new FeedbackPanel("uploadFeedback");

        // Add uploadFeedback to the page itself
        add(uploadFeedback);

        final FileUploadForm simpleUploadForm = new FileUploadForm("simpleUpload", Model.of(registrantModel.getObject()));
        add(simpleUploadForm);

        add(new Link<Registrant>("registrationOptionPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantContactAddressPage(registrantModel.getObject().getId()));
            }
        });


        //final FileUploadForm simpleUploadForm = new FileUploadForm("simpleUpload", registrantModel);

        add(simpleUploadForm);
    }

    private class FileUploadForm extends Form<Registrant> {

        FileUploadField fileUploadField;

        /**
         * Construct.
         *
         * @param name Component name
         */
        public FileUploadForm(String name, final Model<Registrant> registrantModel) {
            super(name);

            // set this form to multipart mode (allways needed for uploads!)
            setMultiPart(true);

            // Add one file input field
            add(fileUploadField = new FileUploadField("fileInput"));
            setDefaultModel(registrantModel);
            // Set maximum size to 100K for demo purposes
            setMaxSize(Bytes.kilobytes(10000));
        }

        /**
         * @see org.apache.wicket.markup.html.form.Form#onSubmit()
         */
        @Override
        protected void onSubmit() {
            final List<FileUpload> uploads = fileUploadField.getFileUploads();

            if (uploads != null) {
                for (FileUpload upload : uploads) {

                    // UploadPage.this.info("saved file: " + upload.getClientFileName());
                    try {

                        Repository repository = JcrUtils.getRepository(generalParametersService.get().getContent_Url());
                        Session session = repository.login(new SimpleCredentials("admin", "admin".toCharArray()));

                        String user = session.getUserID();
                        String name = repository.getDescriptor(Repository.REP_NAME_DESC);

                        System.out.println("Logged in as " + user + " to a " + name + " repository.");

                        try {
                            Node root = session.getRootNode();
                            String fileName = upload.getClientFileName();
                            MimeTable mt = MimeTable.getDefaultTable();
                            String mimeType = mt.getContentTypeFor(fileName);




                            if (mimeType == null || !mimeType.toLowerCase().startsWith("image/")) {
                                error("File selected cannot be uploaded because it is not an image");
                                return;

                            }


                            String id = String.valueOf(this.getModelObject().getId());

                            String profileImagePath = "content/council/registrants/" + id + "/images/profileImages/profileImage";



                            if (root.hasNode(profileImagePath)) {
                                root.getNode(profileImagePath).remove();
                                session.save();
                            }

                            Node contentNode;

                            if (root.hasNode("content")) {
                                contentNode = root.getNode("content");
                            } else {
                                contentNode = root.addNode("content");
                            }

                            Node councilNode;

                            if (contentNode.hasNode("council")) {
                                councilNode = contentNode.getNode("council");
                            } else {
                                councilNode = contentNode.addNode("council");
                            }

                            Node registrantsNode;

                            if (councilNode.hasNode("registrants")) {
                                registrantsNode = councilNode.getNode("registrants");
                            } else {
                                registrantsNode = councilNode.addNode("registrants");
                            }

                            Node idNode;

                            if (registrantsNode.hasNode(id)) {
                                idNode = registrantsNode.getNode(id);
                            } else {
                                idNode = registrantsNode.addNode(id);
                            }

                            Node imagesNode;

                            if (idNode.hasNode("images")) {
                                imagesNode = idNode.getNode("images");
                            } else {
                                imagesNode = idNode.addNode("images");
                            }

                            Node profileImagesNode;

                            if (imagesNode.hasNode("profileImages")) {
                                profileImagesNode = imagesNode.getNode("profileImages");
                            } else {
                                profileImagesNode = imagesNode.addNode("profileImages");
                            }

                            Node profileImageNode = profileImagesNode.addNode("profileImage", "nt:file");
                            Node profileImageContentNode = profileImageNode.addNode("jcr:content", "nt:resource");

                            BinaryImpl binaryImpl = new BinaryImpl(upload.getInputStream());
                            profileImageContentNode.setProperty("jcr:data", binaryImpl);
                            profileImageContentNode.setProperty("jcr:lastModified", Calendar.getInstance());
                            profileImageContentNode.setProperty("jcr:mimeType", mimeType);
                            session.save();
                            binaryImpl.dispose();
                            setResponsePage(new RegistrantContactAddressPage(this.getModelObject().getId()));
                        } finally {
                            session.logout();
                            
                        }
                    } catch (Exception ex) {
                        error("error::" + ex.toString());
                        ex.printStackTrace(System.out);
                    }
                }
            }
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
