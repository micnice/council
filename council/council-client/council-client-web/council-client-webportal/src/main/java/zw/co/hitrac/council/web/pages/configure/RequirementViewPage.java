package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Requirement;
import zw.co.hitrac.council.business.service.RequirementService;

/**
 *
 * @author Michael Matiashe
 */
public class RequirementViewPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private RequirementService requirementService;

    public RequirementViewPage(Long id) {
        CompoundPropertyModel<Requirement> model =
                new CompoundPropertyModel<Requirement>(new LoadableDetachableRequirementModel(id));

        setDefaultModel(model);
        add(new Link<Requirement>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new RequirementEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", RequirementListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("generalRequirement"));
        add(new Label("examRequirement"));
        add(new Label("requirement", model.bind("name")));
        add(new Label("qualificationType"));
    }

    private final class LoadableDetachableRequirementModel extends LoadableDetachableModel<Requirement> {

        private Long id;

        public LoadableDetachableRequirementModel(Long id) {
            this.id = id;
        }

        @Override
        protected Requirement load() {
            return requirementService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
