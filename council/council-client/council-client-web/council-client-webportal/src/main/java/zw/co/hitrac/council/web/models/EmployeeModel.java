package zw.co.hitrac.council.web.models;

import com.google.gson.Gson;
import org.apache.wicket.injection.Injector;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.integration.Employee;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * @author tdhlakama
 */
public class EmployeeModel extends LoadableDetachableModel<Employee> {

    @SpringBean
    private GeneralParametersService generalParametersService;
    private final DetachableRegistrantModel registrantModel;

    public EmployeeModel(DetachableRegistrantModel registrantModel) {
        this.registrantModel = registrantModel;
        Injector.get().inject(this);
    }

    @Override
    protected Employee load() {

        Employee employee;
        Registrant registrant = registrantModel.getObject();
        final String id = registrant.getFormattedIDNumber();

        System.out.println("One ----------------------------------->" + generalParametersService.get().getContent_Url());
        try {

            if (!generalParametersService.get().isIntegrationAllowed()) {
                employee = new Employee();
                employee.setStatus("Integration Service Disabled");
                employee.setValid(false);
                return employee;
            }

            if (id == null || id.length() == 0) {
                employee = new Employee();
                employee.setStatus("Error Check Practitoner ID");
                employee.setValid(false);
                return employee;
            }

            URL url = new URL(generalParametersService.get().getIntegrationUrl() + id);
            System.out.println("----------------------------------->" + url.getPath());

            System.out.println("----------------------------------->" + url.getPath());
            // read from the URL
            Scanner scan = new Scanner(url.openStream());
            String str = new String();
            while (scan.hasNext())
                str += scan.nextLine();
            scan.close();

            Gson gson = new Gson();
            employee = gson.fromJson(str, Employee.class);
            employee.setValid(true);
            employee.setStatus("Record Found");

            return employee;
        } catch (MalformedURLException e) {
            employee = new Employee();
            employee.setStatus("Error Occured, Cannot Retrieve file ");
            employee.setValid(false);
            return employee;
        } catch (IOException e) {
            employee = new Employee();
            employee.setStatus("Error Occured, Cannot Retrieve file ");
            employee.setValid(false);
            return employee;
        }

    }

}
