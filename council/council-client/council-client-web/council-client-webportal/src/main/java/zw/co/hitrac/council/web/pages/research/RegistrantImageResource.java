package zw.co.hitrac.council.web.pages.research;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.commons.io.IOUtils;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.wicket.request.resource.DynamicImageResource;

import zw.co.hitrac.council.business.domain.Registrant;

//~--- JDK imports ------------------------------------------------------------

import java.io.IOException;
import java.io.InputStream;

import javax.jcr.Node;
import javax.jcr.Repository;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;
import zw.co.hitrac.council.business.service.GeneralParametersService;

/**
 *
 * @author Charles Chigoriwa
 */
public class RegistrantImageResource extends DynamicImageResource {

    final private Registrant registrant;
    //@SpringBean
    final private GeneralParametersService generalParametersService;

    public RegistrantImageResource(Registrant registrant, GeneralParametersService generalParametersService) {
        this.registrant = registrant;
        this.generalParametersService = generalParametersService;
    }

    @Override
    protected byte[] getImageData(Attributes attributes) {
        InputStream inputStream = getInputStream();

        if (inputStream != null) {
            try {
                return IOUtils.toByteArray(inputStream);
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }

        return null;
    }

    private InputStream getInputStream() {
        String profileImagePath = "content/council/registrants/" + this.registrant.getId()
                + "/images/profileImages/profileImage";

        try {

            Repository repository = JcrUtils.getRepository(generalParametersService.get().getContent_Url());
            Session session = repository.login(new SimpleCredentials("admin", "admin".toCharArray()));
            String user = session.getUserID();
            String name = repository.getDescriptor(Repository.REP_NAME_DESC);

            System.out.println("Logged in as " + user + " to a " + name + " repository.");

            try {
                Node root = session.getRootNode();

                if (root.hasNode(profileImagePath + "/jcr:content")) {
                    Node pictureNode = root.getNode(profileImagePath + "/jcr:content");

                    return pictureNode.getProperty("jcr:data").getBinary().getStream();
                }
            } finally {
                
                session.logout();
                
            }
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
        }

        return null;
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
