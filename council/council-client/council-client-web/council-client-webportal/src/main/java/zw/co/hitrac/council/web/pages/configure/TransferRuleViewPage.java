package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.TransferRule;
import zw.co.hitrac.council.business.service.TransferRuleService;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;

/**
 *
 * @author Michael Matiashe
 * @author Daniel Nkhoma
 */
public class TransferRuleViewPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private TransferRuleService transferRuleService;

    public TransferRuleViewPage(Long id) {
        CompoundPropertyModel<TransferRule> model =
            new CompoundPropertyModel<TransferRule>(new LoadableDetachableTransferRuleModel(id));

        setDefaultModel(model);
        add(new Link<TransferRule>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new TransferRuleEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Label("name"));
        add(new Label("fromRegister.name"));
        add(new Label("toRegister.name"));
        add(new Label("fromCourse.name"));
        add(new Label("toCourse.name"));
        add(new Label("product"));
    }
    
    private final class LoadableDetachableTransferRuleModel extends LoadableDetachableModel<TransferRule> {
        private Long id;

        public LoadableDetachableTransferRuleModel(Long id) {
            this.id = id;
        }

        @Override
        protected TransferRule load() {
            return transferRuleService.get(id);
        }
    }
    
}


//~ Formatted by Jindent --- http://www.jindent.com
