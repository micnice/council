/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service;

import java.util.List;
import zw.co.hitrac.council.business.domain.AddressType;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantAddress;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;

/**
 *
 * @author kelvin
 */
public interface RegistrantAddressService extends IGenericService<RegistrantAddress> {
    public List<RegistrantAddress> getAddresses(Registrant registrant, Institution institution);
    
    public RegistrantAddress getActiveAddress(Registrant registrant);
    
    public RegistrantAddress getActiveAddressbyAddressType(Registrant registrant, AddressType addressType);
    
    public List<RegistrantData> getRegistrantDataAddress(AddressType addressType);
}
