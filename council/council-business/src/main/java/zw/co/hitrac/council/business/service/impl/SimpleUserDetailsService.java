package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Optional;

import zw.co.hitrac.council.business.domain.User;
import zw.co.hitrac.council.business.service.UserService;
import zw.co.hitrac.council.business.utils.CouncilException;

/**
 * Created by scott on 19/02/16.
 */

@Service
public class SimpleUserDetailsService implements UserDetailsService {

  @Autowired
  private UserService userService;

  @Override
  public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

    if (StringUtils.isEmpty(userName)) {
      throw new CouncilException("User name cannot be empty");
    }

    Optional<User> user = userService.findByNameIgnoreCase(userName);

    if (user.isPresent()) {
      return user.get();
    } else {
      throw new UsernameNotFoundException("No such user with user name: " + userName);
    }

  }
}
