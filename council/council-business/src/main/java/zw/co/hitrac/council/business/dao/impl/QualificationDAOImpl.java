package zw.co.hitrac.council.business.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.QualificationDAO;
import zw.co.hitrac.council.business.dao.repo.QualificationRepository;
import zw.co.hitrac.council.business.domain.ProductIssuanceType;
import zw.co.hitrac.council.business.domain.Qualification;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.Registration;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author Michael Matiashe
 */
@Repository
public class QualificationDAOImpl implements QualificationDAO {

	@Autowired
	private QualificationRepository qualificationRepository;
	@PersistenceContext
	EntityManager entityManager;

	public Qualification save(Qualification qualification) {
		return qualificationRepository.save(qualification);
	}

	public List<Qualification> findAll() {
		
		Session session = entityManager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(Qualification.class);
		criteria.add(Restrictions.eq("retired", Boolean.FALSE));
		criteria.addOrder(Order.asc("name"));
		return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
	}

	public Qualification get(Long id) {
		return qualificationRepository.findOne(id);
	}

	public void setQualificationRepository(QualificationRepository qualificationRepository) {
		this.qualificationRepository = qualificationRepository;

	}

	public List<Qualification> getQualificationCertificateType(ProductIssuanceType productIssuanceType) {
		
		Session session = entityManager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(Qualification.class);
		criteria.add(Restrictions.eq("productIssuanceType", productIssuanceType));
		criteria.addOrder(Order.asc("name"));
		return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
	}

	public List<Qualification> findAll(Boolean retired) {
		
		Session session = entityManager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(Qualification.class);
		criteria.add(Restrictions.eq("retired", retired));
		criteria.addOrder(Order.asc("name"));
		return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
	}


	@Override
	public Qualification findByName(String name) {
		return qualificationRepository.findByName(name);
	}
}
