
package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.CityDAO;
import zw.co.hitrac.council.business.dao.repo.CityRepository;
import zw.co.hitrac.council.business.domain.City;
import zw.co.hitrac.council.business.domain.Country;

/**
 *
 * @author Matiashe Michael
 */
@Repository
public class CityDAOImpl implements CityDAO {

    @Autowired
    private CityRepository cityRepository;
    
    @PersistenceContext
    private EntityManager entityManager;

    public City save(City city) {
        return cityRepository.save(city);
    }

    public List<City> findAll() {
        return cityRepository.findAll();
    }

    public City get(Long id) {
        return cityRepository.findOne(id);
    }
    
    
    /**
     * A setter method that will make mocking repo object easier
     * @param CityRepository 
     */
    public void setCityRepository(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    public List<City> getCities(Country country) {
        String sortOrder ="ASC";
       return entityManager.createQuery("select c from City c where c.country=:country ORDER BY c.name " + sortOrder).setParameter("country", country).getResultList();
    }

    public List<City> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
