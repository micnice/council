/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.configure;

import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.EmploymentArea;
import zw.co.hitrac.council.business.service.EmploymentAreaService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

/**
 *
 * @author kelvin
 */
public class EmploymentAreaListPage extends IAdministerDatabaseBasePage{
    
    @SpringBean
    private EmploymentAreaService employmentAreaService;

    public EmploymentAreaListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink",AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink",AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {

            @Override
            public void onClick() {
               setResponsePage(new EmploymentAreaEditPage(null));
            }
        });
        
        IModel<List<EmploymentArea>> model=new LoadableDetachableModel<List<EmploymentArea>>() {

            @Override
            protected List<EmploymentArea> load() {
               return employmentAreaService.findAll();
            }
        };
        
        PropertyListView<EmploymentArea> eachItem=new PropertyListView<EmploymentArea>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<EmploymentArea> item) {
                Link<EmploymentArea> viewLink=new Link<EmploymentArea>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                       setResponsePage(new EmploymentAreaViewPage(getModelObject().getId()));
                    }
                };
                if(item.getIndex()%2==0){
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };
        
        add(eachItem);
    }
    
    
    
}
