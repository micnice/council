package zw.co.hitrac.council.web.models;

import java.util.List;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.service.examinations.ExamSettingService;

/**
 *
 * @author Charles Chigoriwa
 */
public class ExamSettingListModel extends LoadableDetachableModel<List<ExamSetting>> {

    private final ExamSettingService examSettingService;

    public ExamSettingListModel(ExamSettingService examSettingService) {
        this.examSettingService = examSettingService;
    }

    @Override
    protected List<ExamSetting> load() {
        return examSettingService.getExamSettings(null);
    }
}
