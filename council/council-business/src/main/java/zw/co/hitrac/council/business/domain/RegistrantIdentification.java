/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.utils.CaseUtil;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author tidza
 */
@Entity
@Table(name="registrantidentification")
@Audited
public class RegistrantIdentification extends BaseIdEntity implements Serializable {

    private IdentificationType identificationType;
    private Registrant registrant;
    private String idNumber;
    private Date expiry;

    @Column(name = "id_number", nullable = false, length = 36, unique = true)
    public String getIdNumber() {
          return CaseUtil.upperCase(idNumber);
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getExpiry() {
        return expiry;
    }

    public void setExpiry(Date expiry) {
        this.expiry = expiry;
    }

    @ManyToOne
    public Registrant getRegistrant() {
        return registrant;
    }

    public void setRegistrant(Registrant registrant) {
        this.registrant = registrant;
    }

    @ManyToOne
    public IdentificationType getIdentificationType() {
        return identificationType;
    }

    public void setIdentificationType(IdentificationType identificationType) {
        this.identificationType = identificationType;
    }

    @Transient
    public String getExpiryStatus() {
        if (expiry == null) {
            return "";
        } else {
            if (expiry.after(new Date())) {
                return "EXPIRED";
            } else {
                return "VALID";
            }
        }
    }
}
