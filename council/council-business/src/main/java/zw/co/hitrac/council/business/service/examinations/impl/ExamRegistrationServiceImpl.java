/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.examinations.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.examinations.ExamRegistrationDao;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.business.domain.examinations.ExamResult;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationService;

import java.util.List;

/**
 * @author tidza
 */
@Service
@Transactional
public class ExamRegistrationServiceImpl implements ExamRegistrationService {

    @Autowired
    private ExamRegistrationDao examRegistrationDao;

    @Transactional
    @Override
    public ExamRegistration save(ExamRegistration t) {
        return examRegistrationDao.save(t);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ExamRegistration> findAll() {
        return examRegistrationDao.findAll();
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public ExamRegistration get(Long id) {
        return examRegistrationDao.get(id);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ExamRegistration> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setExamRegistrationDao(ExamRegistrationDao examRegistrationDao) {
        this.examRegistrationDao = examRegistrationDao;
    }

    /*
    * Get Total Number of Parameters
    * exam, Course, institution,candidate number, id number, supplementary exam, suppressed, closed, reversed, disqualified
    *
    */
    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ExamRegistration> getAllExamCandidates(ExamSetting examSetting, Course course, Institution institution, String candidateNumber, String idNumber, Boolean supplementaryExam, Boolean suppressed, Boolean closed, Boolean reversedResult, Boolean disqualified) {
        return examRegistrationDao.getAllExamCandidates(examSetting, course, institution, candidateNumber, idNumber, supplementaryExam, suppressed, closed, reversedResult, disqualified);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ExamRegistration> getExamRegistrations(Registrant registrant) {
        return examRegistrationDao.getExamRegistrations(registrant);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ExamRegistration> getExamRegistrations(Registration registration) {
        return examRegistrationDao.getExamRegistrations(registration);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ExamResult> getExamResults(ExamSetting examSetting, Course course, Institution institution, String candidateNumber, String idNumber, Boolean supplementaryExam) {
        return examRegistrationDao.getExamResults(examSetting, course, institution, candidateNumber, idNumber, supplementaryExam);

    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public ExamRegistration get(ExamSetting examSetting, Course course, Registrant registrant) {
        return examRegistrationDao.get(examSetting, course, registrant);
    }

    /*
    * Get Total Number of Parameters
    * exam, Course, institution,candidate number, id number, supplementary exam, suppressed, closed, reversed, disqualified
    *
     */
    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Integer getTotalNumberOfCandidates(ExamSetting examSetting, Course course, Institution institution, String candidateNumber, String idNumber, Boolean supplementaryExam, Boolean suppressed, Boolean closed, Boolean reversedResult, Boolean disqualified) {
        return examRegistrationDao.getTotalNumberOfCandidates(examSetting, course, institution, candidateNumber, idNumber, supplementaryExam, suppressed, closed, reversedResult, disqualified);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Integer getNumberofRewrites(Registration registration) {
        return examRegistrationDao.getNumberofRewrites(registration);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Boolean getExamRegistration(String candidateNumber) {
        return examRegistrationDao.getExamRegistration(candidateNumber);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Boolean getCheckExamRegistration(ExamSetting examSetting, Registration registration) {
        return examRegistrationDao.getCheckExamRegistration(examSetting, registration);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public ExamRegistration getExamRegistration(ExamSetting examSetting, Registrant registrant) {
        return examRegistrationDao.getExamRegistration(examSetting, registrant);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public ExamRegistration getExaminationInformation(ExamSetting examSetting, Course course) {
        return examRegistrationDao.getExaminationInformation(examSetting, course);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Institution> getInstitutiuons(ExamSetting examSetting, Course course) {
        return examRegistrationDao.getInstitutiuons(examSetting, course);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Integer getTotalNumberSchool(ExamSetting examSetting, Course course) {
        return examRegistrationDao.getTotalNumberSchool(examSetting, course);
    }
}
