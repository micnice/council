package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxCheckBox;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.CheckBoxMultipleChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.business.service.ConditionService;
import zw.co.hitrac.council.business.service.RegisterService;
import zw.co.hitrac.council.business.service.RequirementService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.ConditionsListModel;
import zw.co.hitrac.council.web.models.RequirementListModel;

/**
 *
 * @author Kelvin Goredema
 * @author  Matiashe Michael
 */
public class RegisterEditPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private RegisterService registerService;
    @SpringBean
    private RequirementService requirementService;
    @SpringBean
    private ConditionService conditionService;

    public RegisterEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<Register>(new LoadableDetachableRegisterModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));

        Form<Register> form = new Form<Register>("form", (IModel<Register>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                registerService.save(getModelObject());
                setResponsePage(new RegisterViewPage(getModelObject().getId()));
            }
        };
        form.add(new CheckBox("quarterlyRenewal"));
        form.add(new CheckBox("discipline"));
        form.add(new TextField<Integer>("numberOfMonthsBeforeRenewal"));
        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink", RegisterListPage.class));
        add(form);
        
        final WebMarkupContainer wmc = new WebMarkupContainer("wmc");
        wmc.setVisible(false);
        wmc.setOutputMarkupPlaceholderTag(true);
        form.add(wmc);
 
        form.add(new AjaxCheckBox("advanced1", new PropertyModel(wmc, "visible")) {
            @Override
            protected void onUpdate(AjaxRequestTarget art) {
                art.add(wmc);
            }
        });
        wmc.add(new CheckBoxMultipleChoice("requirements", new RequirementListModel(requirementService)));
        
        final WebMarkupContainer wmc1 = new WebMarkupContainer("wmc1");
        wmc1.setVisible(false);
        wmc1.setOutputMarkupPlaceholderTag(true);
        form.add(wmc1);
 
        form.add(new AjaxCheckBox("advanced2", new PropertyModel(wmc1, "visible")) {
            @Override
            protected void onUpdate(AjaxRequestTarget art) {
                art.add(wmc1);
            }
        });
        wmc1.add(new CheckBoxMultipleChoice("conditions", new ConditionsListModel(conditionService)));

    }

    private final class LoadableDetachableRegisterModel extends LoadableDetachableModel<Register> {
        private Long id;

        public LoadableDetachableRegisterModel(Long id) {
            this.id = id;
        }

        @Override
        protected Register load() {
            if (id == null) {
                return new Register();
            }

            return registerService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
