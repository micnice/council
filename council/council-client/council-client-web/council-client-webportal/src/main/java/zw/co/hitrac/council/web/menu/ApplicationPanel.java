package zw.co.hitrac.council.web.menu;

import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.pages.application.ApplicationPage;
import zw.co.hitrac.council.web.pages.application.ApplicationSearchPage;
import zw.co.hitrac.council.web.pages.application.ApplicationListPage;
import zw.co.hitrac.council.web.pages.application.ApplicationPendingPage;
import zw.co.hitrac.council.web.pages.application.SearchApprovedApplicationPage;
import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;

/**
 *
 * @author Michael Matiashe
 */
public class ApplicationPanel extends MenuPanel {

    private boolean applyPageCurrent;
    private boolean applicationSimpleSearch;
    private boolean applicationPending;
    private boolean searchApprovedApplication;
    private Link<Void> applicationLink;
    private Link<Void> applicationSimpleSearchPage;
    private Link<Void> applicationPendingPage;
    private Link<Void> searchApprovedApplicationPage;
    private WebMarkupContainer childMenuLinks;

    public ApplicationPanel(String id) {
        super(id);
        add(applicationLink = new BookmarkablePageLink<Void>("applicationViewLink", ApplicationListPage.class) {
            @Override
            protected void onConfigure() {
                setVisibilityAllowed(CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.EDUCATION_OFFICER, Role.REGISTRY_CLERK))));
            }
        });
        add(childMenuLinks = new WebMarkupContainer("childMenuLinks"));
        childMenuLinks.add(applicationSimpleSearchPage = new BookmarkablePageLink<Void>("applicationSimpleSearchPage", ApplicationSearchPage.class) {
            @Override
            protected void onConfigure() {
                setVisibilityAllowed(CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.EDUCATION_OFFICER))));
            }
        });
        childMenuLinks.add(applicationPendingPage = new BookmarkablePageLink<Void>("applicationPendingPage", ApplicationPendingPage.class) {
            @Override
            protected void onConfigure() {
                setVisibilityAllowed(CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.EDUCATION_OFFICER))));
            }
        });
        childMenuLinks.add(searchApprovedApplicationPage = new BookmarkablePageLink<Void>("approvedApplicationSearchPage", SearchApprovedApplicationPage.class) {
            @Override
            protected void onConfigure() {
                setVisibilityAllowed(CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.EDUCATION_OFFICER, Role.REGISTRY_CLERK))));
            }
        });
    }

    @Override
    protected void onConfigure() {
        childMenuLinks.setVisibilityAllowed(topMenuCurrent);
        addCurrentBehavior(applicationLink, topMenuCurrent);
        addCurrentBehavior(applicationSimpleSearchPage, applicationSimpleSearch);
        addCurrentBehavior(applicationPendingPage, applicationPending);
        addCurrentBehavior(searchApprovedApplicationPage, searchApprovedApplication);
    }

    public boolean isApplicationSimpleSearch() {
        return applicationSimpleSearch;
    }

    public void setApplicationSimpleSearch(boolean applicationSimpleSearch) {
        this.applicationSimpleSearch = applicationSimpleSearch;
    }

    public boolean isApplicationPending() {
        return applicationPending;
    }

    public void setApplicationPending(boolean applicationPending) {
        this.applicationPending = applicationPending;
    }

    public boolean isSearchApprovedApplication() {
        return searchApprovedApplication;
    }

    public void setSearchApprovedApplication(boolean searchApprovedApplication) {
        this.searchApprovedApplication = searchApprovedApplication;
    }
}
