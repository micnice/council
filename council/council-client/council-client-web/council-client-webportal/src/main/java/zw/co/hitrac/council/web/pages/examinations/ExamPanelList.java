
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;

import zw.co.hitrac.council.business.domain.examinations.Exam;

//~--- JDK imports ------------------------------------------------------------
import java.util.List;
import zw.co.hitrac.council.web.utility.CustomDateLabel;

/**
 *
 * @author tdhlakama
 */
public class ExamPanelList extends Panel {

    /**
     * Constructor
     *
     * @param id
     */
    public ExamPanelList(String id) {
        super(id);

        // - Used to Display a to list of Exam Papers in Exam Registration
        add(new PropertyListView<Exam>("examList") {
            @Override
            protected void populateItem(final ListItem<Exam> item) {
                item.add(new Link<Void>("viewLink") {
                    @Override
                    public void onClick() {
                        setResponsePage(new ExamViewPage(item.getModelObject().getId()));
                    }
                });
                item.add(new Label("modulePaper"));
                item.add(new CustomDateLabel("examDate"));
                item.add(new Label("examSetting"));
                item.setModel(new CompoundPropertyModel<Exam>(item.getModel()));
            }
        });
    }

    /**
     * Constructor
     *
     * @param id
     * @param ExamList
     */
    public ExamPanelList(String id, IModel<List<Exam>> model) {
        super(id);

        PropertyListView<Exam> eachItem = new PropertyListView<Exam>("examList", model) {
            @Override
            protected void populateItem(final ListItem<Exam> item) {
                item.add(new Link<Void>("viewLink") {
                    @Override
                    public void onClick() {
                        setResponsePage(new ExamViewPage(item.getModelObject().getId()));
                    }
                });
                item.add(new Label("modulePaper"));
                item.add(new CustomDateLabel("examDate"));
                item.add(new Label("examSetting"));
                item.setModel(new CompoundPropertyModel<Exam>(item.getModel()));
            }
        };

        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
