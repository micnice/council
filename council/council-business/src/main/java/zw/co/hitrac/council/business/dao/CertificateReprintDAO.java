package zw.co.hitrac.council.business.dao;

import zw.co.hitrac.council.business.domain.CertificateReprint;

/**
 *
 * @author Michael Matiashe
 */
public interface CertificateReprintDAO extends IGenericDAO<CertificateReprint> {
    
}
