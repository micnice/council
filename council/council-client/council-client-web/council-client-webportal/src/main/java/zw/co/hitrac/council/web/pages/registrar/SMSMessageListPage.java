package zw.co.hitrac.council.web.pages.registrar;

import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.SMSMessage;
import zw.co.hitrac.council.business.process.RenewalProcess;
import zw.co.hitrac.council.business.service.SMSMessageService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrantContactService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author Michael Matiashe
 */
public class SMSMessageListPage extends TemplatePage {

    @SpringBean
    private SMSMessageService sMSMessageService;
    @SpringBean
    private RegistrantContactService registrantContactService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private RenewalProcess renewalProcess;

    public SMSMessageListPage() {

        IModel<List<SMSMessage>> model = new LoadableDetachableModel<List<SMSMessage>>() {

            @Override
            protected List<SMSMessage> load() {
                return sMSMessageService.findAll();
            }
        };

        PropertyListView<SMSMessage> eachItem = new PropertyListView<SMSMessage>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<SMSMessage> item) {
                Link<SMSMessage> viewLink = new Link<SMSMessage>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new SMSMessageEditPage(getModelObject().getId()));
                    }
                };
                Link<SMSMessage> link = new Link<SMSMessage>("sendSMSLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new SendSMSPage(getModelObject()));
                    }
                };
                Link<SMSMessage> sendAll = new Link<SMSMessage>("sendToAll", item.getModel()) {
                    @Override
                    public void onClick() {
                        try {
                            System.out.println("---------------------here");
//                            SendMail sendMail = new SendMail(generalParametersService);
//                            sendMail.sendToAll(getModelObject(), registrantContactService.getRegistrantDataAddress(generalParametersService.get().getEmailContactType()));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                };
                item.add(viewLink);
                item.add(sendAll);
                item.add(link);
                viewLink.add(new Label("detail"));
                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
            }
        };

        add(eachItem);
    }

}
