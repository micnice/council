package zw.co.hitrac.council.business.dao.accounts;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import zw.co.hitrac.council.business.dao.IGenericDAO;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.TypeOfService;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.domain.accounts.Product;

/**
 *
 * @author Tatenda Chiwandire
 * @author Michael Matiashe
 */
public interface DebtComponentDAO extends IGenericDAO<DebtComponent> {

    List<DebtComponent> getDebtComponents(Account account);

    List<DebtComponent> getDebtComponents(Customer customerAccount);

    public Boolean hasAccountDebtComponents(Customer customerAccount, TypeOfService typeOfService);

    public List<DebtComponent> getPenaltyDeptComponets();

    List<DebtComponent> getDebtComponents(Product product, Date startDate, Date endDate);

    public List<DebtComponent> getViewRemovedDeptComponets(Customer customerAccount);

    public List<DebtComponent> getViewDebtComponentsRemoved(Account account);

    public BigDecimal getCustomerBalanceDue(Customer customerAccount);

    public List<DebtComponent> getUnPaidDeptComponets();

    public List<DebtComponent> getRemovedDeptComponents();

}
