package zw.co.hitrac.council.business.process;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.service.EmploymentService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegisterService;
import zw.co.hitrac.council.business.service.accounts.ProductService;
import zw.co.hitrac.council.business.utils.CouncilException;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author Michael Matiashe
 */
@Service
public class ProductFinder {

    @Autowired
    private ProductGoogler productGoogler;
    @Autowired
    private ProductService productService;
    @Autowired
    private GeneralParametersService generalParametersService;
    @Autowired
    private RegisterService registerService;
    @Autowired
    private EmploymentService employmentService;

    public Product searchReRegistrationProduct(Registration registration) {
        Product searchProduct = new Product();
        searchProduct.setCourse(registration.getCourse());
        searchProduct.setTypeOfService(TypeOfService.RE_REGISTRATION_FEES);
        searchProduct.setRegister(registration.getRegister());
        EmploymentType employmentType = employmentService.getRegistrantEmployment(registration.getRegistrant());
        if (employmentType != null) {
            searchProduct.setEmploymentType(employmentType);
        }
        if (registration.getCourse().getCourseType() != null) {
            searchProduct.setCourseType(registration.getCourse().getCourseType());
        }
        Product productFound = productGoogler.searchProduct(searchProduct);
        if (productFound != null && productFound.getCourse() != null && !productFound.getCourse().equals(registration.getCourse())) {
            throw new CouncilException("Re-Registration Product for " + registration.getCourse().getName() + " re-registration not set ");
        }
        if (productFound == null) {
            throw new CouncilException("Product for " + registration.getCourse().getName() + " re-registration in not set ");
        }
        return productFound;
    }

    public Product searchLateRegistrationProduct(Registration registration) {
        Product searchProduct = new Product();
        searchProduct.setCourse(registration.getCourse());
        searchProduct.setTypeOfService(TypeOfService.LATE_REGISTRATION_FEES);
        searchProduct.setRegister(registration.getRegister());
        Product productFound = productGoogler.searchProduct(searchProduct);
        if (productFound != null && productFound.getCourse() != null && !productFound.getCourse().equals(registration.getCourse())) {
            throw new CouncilException("Late Registration Product for " + registration.getCourse().getName() + " re-registration not set ");
        }
        if (productFound == null) {
            throw new CouncilException("Product for " + registration.getCourse().getName() + " late registration not set ");
        }
        return productFound;
    }

    public Product searchRegistrationProduct(Registration registration) {
        Product searchProduct = new Product();
        searchProduct.setCourse(registration.getCourse());
        searchProduct.setTypeOfService(TypeOfService.REGISTRATION_FEES);
        searchProduct.setRegister(registration.getRegister());
        EmploymentType employmentType = employmentService.getRegistrantEmployment(registration.getRegistrant());
        if (employmentType != null) {
            searchProduct.setEmploymentType(employmentType);
        }
        if (registration.getCourse().getCourseType() != null) {
            searchProduct.setCourseType(registration.getCourse().getCourseType());
        }
        Product productFound = productGoogler.searchProduct(searchProduct);
        if (productFound != null && productFound.getCourse() != null && !productFound.getCourse().equals(registration.getCourse())) {
            throw new CouncilException("Registration Product for " + registration.getCourse().getName() + " re-registration not set ");
        }
        if (productFound == null) {
            throw new CouncilException("Product for " + registration.getCourse().getName() + " registration in " + registration.getRegister().getName() + " not set ");
        }
        return productFound;
    }

    public Product searchRegistrationOfNewQualificationProduct(Registration registration, Course course) {
        Product searchProduct = new Product();
        searchProduct.setCourse(course);
        searchProduct.setTypeOfService(TypeOfService.REGISTRATION_FEES);
        searchProduct.setRegister(registration.getRegister());
        EmploymentType employmentType = employmentService.getRegistrantEmployment(registration.getRegistrant());
        if (employmentType != null) {
            searchProduct.setEmploymentType(employmentType);
        }
        if (registration.getCourse().getCourseType() != null) {
            searchProduct.setCourseType(registration.getCourse().getCourseType());
        }
        Product productFound = productGoogler.searchProduct(searchProduct);
        if (productFound == null) {
            throw new CouncilException("Product for " + registration.getCourse().getName() + " registration in " + registration.getRegister().getName() + " not set ");
        }
        return productFound;
    }

    public Product searchInstitutionRegistrationProduct(Registration registration) {
        Product searchProduct = new Product();
        searchProduct.setTypeOfService(TypeOfService.INSTITUTION__REGISTRATION_FEES);
        searchProduct.setRegister(generalParametersService.get().getInstitutionRegister());
        Product productFound = productGoogler.searchProduct(searchProduct);
        if (productFound == null) {
            throw new CouncilException("Product for Institution registration  not set ");
        }
        return productFound;
    }

    public Product searchQualifyingFeeProduct(Registration registration) {
        Product searchProduct = new Product();
        searchProduct.setCourse(registration.getCourse());
        searchProduct.setTypeOfService(TypeOfService.QUALIFYING_FEES);
        searchProduct.setRegister(registration.getRegister());
        EmploymentType employmentType = employmentService.getRegistrantEmployment(registration.getRegistrant());
        if (employmentType != null) {
            searchProduct.setEmploymentType(employmentType);
        }
        if (registration.getCourse().getCourseType() != null) {
            searchProduct.setCourseType(registration.getCourse().getCourseType());
        }
        Product productFound = productGoogler.searchProduct(searchProduct);
        if (productFound == null) {
            throw new CouncilException("Product for " + registration.getCourse().getName() + " registration in " + registration.getRegister().getName() + " not set ");
        }
        return productFound;
    }

    public Product searchAnnualProduct(Registration registration, QualificationType... qualificationTypes) {
        Product searchProduct = new Product();
        searchProduct.setCourse(registration.getCourse());
        //code to search employment to find current employement through employment service
        EmploymentType employmentType = employmentService.getRegistrantEmployment(registration.getRegistrant());
        if (employmentType != null) {
            searchProduct.setEmploymentType(employmentType);
        }
        if (registration.getCourse() != null) {
            if (registration.getCourse().getTier() != null) {
                searchProduct.setTier(registration.getCourse().getTier());
            }
            if (registration.getCourse().getCourseType() != null) {
                searchProduct.setCourseType(registration.getCourse().getCourseType());
            }
        }
        searchProduct.setTypeOfService(TypeOfService.ANNUAL_FEES);
        searchProduct.setRegister(registration.getRegister());
        if (qualificationTypes != null && qualificationTypes.length >= 1) {
            searchProduct.setQualificationType(qualificationTypes[0]);
        }
        Product productFound;
        productFound = productGoogler.searchProduct(searchProduct);

        if (productFound == null) {
            throw new CouncilException("Product for " + registration.getCourse().getName() + " Annual Fee in " + registration.getRegister().getName() + " not set ");
        } else {
            return productFound;
        }
    }

    public Product searchAnnualProduct(Course course, Register register, EmploymentType employmentType, TypeOfService typeOfService) {
        Product searchProduct = new Product();
        searchProduct.setCourse(course);
        //code to search employment to find current employement through employment service
        if (employmentType != null) {
            searchProduct.setEmploymentType(employmentType);
        }
        if (course != null) {
            if (course.getTier() != null) {
                searchProduct.setTier(course.getTier());
            }
            if (course.getCourseType() != null) {
                searchProduct.setCourseType(course.getCourseType());
            }
        }
        searchProduct.setTypeOfService(TypeOfService.ANNUAL_FEES);
        searchProduct.setRegister(register);
        Product productFound;
        productFound = productGoogler.searchProduct(searchProduct);

        if (productFound == null) {
            throw new CouncilException("Product for " + course.getName() + " Annual Fee in " + course.getName() + " not set ");
        } else {
            return productFound;
        }
    }

    public Product searchRegistrationTransferProduct(Application application) {
        Product productFound = application.getTransferRule().getProduct();
        if (productFound == null) {
            throw new CouncilException("Product for " + application.getApplicationPurpose().getName() + " application not set");
        } else {
            return productService.get(productFound.getId());
        }

    }

    public Product searchApplicationProduct(Application application, Register register) {
        Product searchProduct = new Product();
        searchProduct.setCourse(application.getCourse());
        if (application.getEmploymentType() != null) {
            searchProduct.setEmploymentType(application.getEmploymentType());
        }
        if (application.getCourse().getCourseType() != null) {
            searchProduct.setCourseType(application.getCourse().getCourseType());
        }
        if (register != null) {
            searchProduct.setRegister(registerService.get(register.getId()));
        }
        searchProduct.setTypeOfService(TypeOfService.APPLICATION_FEES);
        if (application.getQualificationType() != null) {
            searchProduct.setQualificationType(application.getQualificationType());
        }
        if (application.getCourse().getTier() != null) {
            searchProduct.setTier(application.getCourse().getTier());
        }
        Product productFound = productGoogler.searchProduct(searchProduct);
        if (productFound == null) {
            throw new CouncilException("Product for " + application.getApplicationPurpose().getName() + " application not set");
        } else {
            return productFound;
        }

    }

    public Product searchInstitutionApplicationProduct(Application application, Register register) {
        Product searchProduct = new Product();
        searchProduct.setTypeOfService(TypeOfService.APPLICATION_FOR_INSTITUTION_REGISTRATION);
        searchProduct.setRegister(register);
        Product productFound = productGoogler.searchProduct(searchProduct);
        if (productFound == null) {
            throw new CouncilException("Product for " + application.getApplicationPurpose().getName() + " application not set");
        } else {
            return productFound;
        }

    }

    public Product searchCGSApplicationProduct(Application application, Register register) {
        Product searchProduct = new Product();
        searchProduct.setCourse(application.getCourse());
        searchProduct.setTypeOfService(TypeOfService.APPLICATION_FOR_CGS_FEES);
        searchProduct.setRegister(register);
        if (application.getEmploymentType() != null) {
            searchProduct.setEmploymentType(application.getEmploymentType());
        } else {
            //code to search employment to find current employement through employment service
            EmploymentType employmentType = employmentService.getRegistrantEmployment(application.getRegistrant());
            if (employmentType != null) {
                searchProduct.setEmploymentType(employmentType);
            }
        }
        if (application.getQualificationType() != null) {
            searchProduct.setQualificationType(application.getQualificationType());
        }
        if (application.getCourse().getTier() != null) {
            searchProduct.setTier(application.getCourse().getTier());
        }
        Product productFound = productGoogler.searchProduct(searchProduct);
        if (productFound == null) {
            throw new CouncilException("Product for " + application.getApplicationPurpose().getName() + " application not set");
        } else {
            return productFound;
        }
    }

    public Product searchPenaltyProduct(Registration registration) {
        Product searchProduct = new Product();

        searchProduct.setTypeOfService(TypeOfService.PENALTY_FEES);
        EmploymentType employmentType = employmentService.getRegistrantEmployment(registration.getRegistrant());
        if (employmentType != null) {
            searchProduct.setEmploymentType(employmentType);
        }
        if (generalParametersService.get().getRegistrationByApplication()) {
            searchProduct.setCourse(registration.getCourse());
            searchProduct.setRegister(registration.getRegister());
            if (registration.getApplication() != null) {
                searchProduct.setQualificationType(registration.getApplication().getQualificationType());
            }
        }
        Product productFound = productGoogler.searchProduct(searchProduct);
        if (productFound == null) {
            throw new CouncilException("Penalty product not set");
        } else {
            return productFound;
        }
    }

    public Product searchChangeOfNameProduct(Application application, Register register) {
        Product searchProduct = new Product();
        searchProduct.setTypeOfService(TypeOfService.CHANGE_OF_NAME_FEES);
        //searchProduct.setRegister(register);
        Product productFound = productGoogler.searchProduct(searchProduct);
        if (productFound == null) {
            throw new CouncilException("Product for " + application.getApplicationPurpose().getName() + " application not set");
        } else {
            return productFound;
        }

    }

    public Product searchUnrestrictedPracticingProduct(Application application, Register register) {
        Product searchProduct = new Product();
        searchProduct.setCourse(application.getCourse());
        searchProduct.setTypeOfService(TypeOfService.APPLICATION_FEES);
        searchProduct.setRegister(register);
        if (application.getEmploymentType() != null) {
            searchProduct.setEmploymentType(application.getEmploymentType());
        }
        if (application.getQualificationType() != null) {
            searchProduct.setQualificationType(application.getQualificationType());
        }
        if (application.getCourse().getTier() != null) {
            searchProduct.setTier(application.getCourse().getTier());
        }
        Product productFound = productGoogler.searchProduct(searchProduct);
        if (productFound == null) {
            throw new CouncilException("Product for " + application.getApplicationPurpose().getName() + " application not set");
        } else {
            return productFound;
        }
    }

    public Product searchQualificationProduct(Application application) {
        Product searchProduct = new Product();
        searchProduct.setTypeOfService(TypeOfService.PROVISIONAL_QUALIFICATION_FEES);
        Product productFound = productGoogler.searchProduct(searchProduct);
        if (productFound == null) {
            throw new CouncilException("Product for " + application.getApplicationPurpose().getName() + " application not set");
        } else {
            return productFound;
        }
    }

    public Product searchAdditionalQualificationProduct(Application application) {
        Product searchProduct = new Product();
        searchProduct.setTypeOfService(TypeOfService.ADDITIONAL_QUALIFICATION_APPLICATION_FEES);
        //searchProduct.setCourse(application.getCourse());
        Product productFound = productGoogler.searchProduct(searchProduct);
        if (productFound == null) {
            throw new CouncilException("Product for " + application.getApplicationPurpose().getName() + " application not set");
        } else {
            return productFound;
        }
    }

    public Product searchUnrestrictedPracticingCertificateProduct(Registration registration, QualificationType... qualificationTypes) {
        Product searchProduct = new Product();
        searchProduct.setCourse(registration.getCourse());
        EmploymentType employmentType = employmentService.getRegistrantEmployment(registration.getRegistrant());
        if (employmentType != null) {
            searchProduct.setEmploymentType(employmentType);
        }
        if (registration.getCourse() != null) {
            if (registration.getCourse().getTier() != null) {
                searchProduct.setTier(registration.getCourse().getTier());
            }
            if (registration.getCourse().getCourseType() != null) {
                searchProduct.setCourseType(registration.getCourse().getCourseType());
            }
        }
        searchProduct.setTypeOfService(TypeOfService.APPLICATION_FOR_UNRESTRICTED_PRACTICE);
        searchProduct.setRegister(registration.getRegister());
        if (qualificationTypes != null && qualificationTypes.length >= 1) {
            searchProduct.setQualificationType(qualificationTypes[0]);
        }
        Product productFound;
        productFound = productGoogler.searchProduct(searchProduct);

        if (productFound == null) {
            throw new CouncilException("Product for " + registration.getCourse().getName() + " UnRestricted Practice Certificate Fee in " + registration.getRegister().getName() + " not set ");
        } else {
            return productFound;
        }
    }

    public Product searchTranscriptOfTrainingProduct(Application application) {
        Product searchProduct = new Product();
        searchProduct.setTypeOfService(TypeOfService.TRANSCRIPT_OF_TRAINING_FEES);
        //searchProduct.setRegister(register);
        Product productFound = productGoogler.searchProduct(searchProduct);
        if (productFound == null) {
            throw new CouncilException("Product for " + application.getApplicationPurpose().getName() + " application not set");
        } else {
            return productFound;
        }
    }

    public Product searchPractisingCertificateProduct(Registration registration) {
        Product searchProduct = new Product();
        searchProduct.setCourse(registration.getCourse());
        searchProduct.setTypeOfService(TypeOfService.PRACTISING_CERTIFICATE_FEES);
        searchProduct.setRegister(registration.getRegister());
        //code to search employment to find current employement through employment service
        EmploymentType employmentType = employmentService.getRegistrantEmployment(registration.getRegistrant());
        if (employmentType != null) {
            searchProduct.setEmploymentType(employmentType);
        }
        if (registration.getCourse().getCourseType() != null) {
            searchProduct.setCourseType(registration.getCourse().getCourseType());
        }
        if (registration.getApplication() != null) {
            if (registration.getApplication().getQualificationType() != null) {
                searchProduct.setQualificationType(registration.getApplication().getQualificationType());
            }
        }
        Product productFound = productGoogler.searchProduct(searchProduct);
        if (productFound == null) {
            throw new CouncilException("Product for " + registration.getCourse().getName() + " Practice Certificate Fee in " + registration.getRegister().getName() + " not set ");
        }
        return productFound;

    }

    public Product searchSupervisionProduct(Application application) {
        Product searchProduct = new Product();
        searchProduct.setTypeOfService(TypeOfService.APPLICATION_FOR_SUPERVISION);
        Product productFound = productGoogler.searchProduct(searchProduct);
        if (productFound == null) {
            throw new CouncilException("Product for " + application.getApplicationPurpose().getName() + " application not set");
        } else {
            return productService.get(productFound.getId());
        }

    }


    public BigDecimal searchAnnualProductPrice(Course course, Register register, EmploymentType employmentType,
                                               TypeOfService typeOfService) {

        try {

            Product product = searchAnnualProduct(course, register, employmentType, typeOfService);

            if (product != null && product.getProductPrice() != null) {
                return product.getProductPrice().getPrice();
            } else {
                return BigDecimal.ZERO;
            }
        } catch (CouncilException ex) {
            return BigDecimal.ZERO;
        }
    }

    public Product searchStudentApplicationProduct(Application application, Register register) {
        Product searchProduct = new Product();
        searchProduct.setCourse(application.getCourse());
        if (application.getEmploymentType() != null) {
            searchProduct.setEmploymentType(application.getEmploymentType());
        }
        if (application.getCourse().getCourseType() != null) {
            searchProduct.setCourseType(application.getCourse().getCourseType());
        }
        if (register != null) {
            searchProduct.setRegister(registerService.get(register.getId()));
        }
        if (register != null) {
            searchProduct.setRegister(registerService.get(register.getId()));
        }
        searchProduct.setTypeOfService(TypeOfService.STUDENT_APPLICATION_FEES);
        if (application.getQualificationType() != null) {
            searchProduct.setQualificationType(application.getQualificationType());
        }
        if (application.getCourse().getTier() != null) {
            searchProduct.setTier(application.getCourse().getTier());
        }
        Product productFound = productGoogler.searchProduct(searchProduct);
        if (productFound == null) {
            throw new CouncilException("Product for " + application.getApplicationPurpose().getName() + " student application not set");
        } else {
            return productFound;
        }

    }

    public Product searchByTypeOfService(TypeOfService typeOfService) {
        Product productFound = productGoogler.searchByTypeOfService(typeOfService);
        if (productFound == null) {
            throw new CouncilException("Product for " + typeOfService.getName() + " not set");
        } else {
            return productFound;
        }

    }

}