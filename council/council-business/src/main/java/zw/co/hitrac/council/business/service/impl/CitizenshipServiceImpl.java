
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.CitizenshipDAO;
import zw.co.hitrac.council.business.domain.Citizenship;
import zw.co.hitrac.council.business.service.CitizenshipService;

import java.util.List;


@Service
@Transactional
public class CitizenshipServiceImpl implements CitizenshipService {

    @Autowired
    private CitizenshipDAO citizenshipDAO;

    @Transactional
    public Citizenship save(Citizenship citizenship) {
        return citizenshipDAO.save(citizenship);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Citizenship> findAll() {
        return citizenshipDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Citizenship get(Long id) {
        return citizenshipDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Citizenship> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * A DAO setter method to facilitate mocking
     *
     * @param citizenshipDAO
     */
    public void setCitizenshipDAO(CitizenshipDAO citizenshipDAO) {
        this.citizenshipDAO = citizenshipDAO;
    }
}
