package zw.co.hitrac.council.web.pages.reports;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.wicket.injection.Injector;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.image.NonCachingImage;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.resource.DynamicImageResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.utils.Gender;
import zw.co.hitrac.council.web.models.CourseListModel;

/**
 *
 * @author tndangana
 */
public class GenderAgeDistributionReport extends NonCachingImage {

    private final int width;
    private final int height;

    private final String gen = "Gender";
    private final String test = "test";
    private Course course;
    @SpringBean
    private CourseService courseService;
    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private RegistrationService registrationService;
    private Gender gender;
    private Date birthDate;
    private final Integer count = 1;

    private final Integer ageA = 20 + count;
    private Integer ageB = 21 - 25;
    private Integer ageC = 26 - 30;
    private Integer ageD = 31 - 35;
    private Integer ageE = 36 - 40;
    private Integer ageF = 41 - 45;
    private Integer ageG = 46 - 50;
    private Integer ageH = 51 - 55;
    private Integer ageI = 56 - 60;
    private Integer ageJ = 61 - 65;
    private Integer ageK = 65 + count;

    List<Integer> totalAges = new ArrayList<Integer>();

    public GenderAgeDistributionReport(String id, int width, int height) {
        super(id);

        this.width = width;
        this.height = height;

        Injector.get().inject(this);

      
    }

    @Override
    protected IResource getImageResource() {
        return new DynamicImageResource() {

            @Override
            protected byte[] getImageData(IResource.Attributes atrbts) {
                JFreeChart chart = createData();
                return toImageData(chart.createBufferedImage(width, height));
            }
        };
    }

    public JFreeChart createData() {
        List<Registrant> registrants = registrantService.findAll();

        final DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        Registrant r = new Registrant();

        for (Registrant registrant : registrants) {
            if ((registrant.getIntegerValueOfAge() >= 21 && registrant.getIntegerValueOfAge() <= 25)) {
                ageB++;
                totalAges.add(ageB);

            } else if (registrant.getIntegerValueOfAge() >= 26 && registrant.getIntegerValueOfAge() <= 30) {

                ageC++;
                totalAges.add(ageC);
            } else if (registrant.getIntegerValueOfAge() >= 31 && registrant.getIntegerValueOfAge() <= 35) {
                totalAges.add(ageD);
                ageD++;

            } else if (registrant.getIntegerValueOfAge() >= 36 && registrant.getIntegerValueOfAge() <= 40) {

                ageE++;
                totalAges.add(ageE);

            } else if (registrant.getIntegerValueOfAge() >= 41 && registrant.getIntegerValueOfAge() <= 45) {
                totalAges.add(ageF);
                ageF++;
            } else if (registrant.getIntegerValueOfAge() >= 46 && registrant.getIntegerValueOfAge() <= 50) {
                totalAges.add(ageG);
                ageG++;
            } else if (registrant.getIntegerValueOfAge() >= 51 && registrant.getIntegerValueOfAge() <= 55) {
                totalAges.add(ageH);
                ageH++;
            } else if (registrant.getIntegerValueOfAge() >= 56 && registrant.getIntegerValueOfAge() <= 60) {
                totalAges.add(ageI);
                ageI++;
            } else if (registrant.getIntegerValueOfAge() >= 61 && registrant.getIntegerValueOfAge() <= 65) {
                totalAges.add(ageJ);
                ageJ++;
            } else if (registrant.getIntegerValueOfAge() >= 61 && registrant.getIntegerValueOfAge() <= 65) {
                totalAges.add(ageK);
                ageK++;
            }

        }

        for (Integer total : totalAges) {
            dataset.addValue(total, "ff", "d");
        }

        final JFreeChart chart = ChartFactory.createBarChart(
                "Bar Chart For :" + course.getName(), // chart title
                "Gender", // domain axis label
                "Age", // range axis label
                dataset, // data
                PlotOrientation.VERTICAL, // orientation
                true, // include legend
                true, // tooltips
                false // urls
        );

        chart.setBackgroundPaint(Color.white);

        final CategoryPlot plot = (CategoryPlot) chart.getPlot();
        plot.setBackgroundPaint(Color.lightGray);
        plot.setRangeGridlinePaint(Color.white);

        // customise the range axis...
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        rangeAxis.setAutoRangeIncludesZero(true);

        final LineAndShapeRenderer renderer = (LineAndShapeRenderer) plot.getRenderer();

        return chart;

    }

}
