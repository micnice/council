
package zw.co.hitrac.council.business.dao.accounts;
import zw.co.hitrac.council.business.dao.IGenericDAO;
import zw.co.hitrac.council.business.domain.accounts.PaymentType;

/**
 *
 * @author Michael Matiashe
 */
public interface PaymentTypeDAO extends IGenericDAO<PaymentType> {
}
