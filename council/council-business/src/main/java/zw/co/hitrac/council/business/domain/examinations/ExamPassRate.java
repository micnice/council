/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain.examinations;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Institution;

/**
 *
 * @author tdhlakama
 */
public class ExamPassRate implements Serializable {

    private Institution institution;
    private ExamSetting examSetting;
    private Course course;
    private Integer numberOfCandidatesPassed = 0;
    private Integer numberOfCandidatesFailed = 0;
    private Double percentageFailed = 0.0;
    private Double percentagePassed = 0.0;
    private Integer totalNumberOfCandidates = 0;
    private Integer paperOneTotalNumberOfCandidates = 0;
    private Integer paperTwoTotalNumberOfCandidates = 0;
    private Integer numberOfCandidatesDisqualified =0;

    private Integer passedFirstTime = 0;
    private Integer failedFirstTime = 0;

    private Integer totalNumberFirstTime = 0;
    private Integer totalNumberOfRewrites = 0;

    private Integer passedRewrites = 0;
    private Integer failedRewrites = 0;

    private Integer passedPaperOneRewrites = 0;
    private Integer failedPaperOneRewrites = 0;

    private Integer passedPaperTwoRewrites = 0;
    private Integer failedPaperTwoRewrites = 0;

    private Integer totalFailedBothPapers = 0;
    private Integer totalFailedRewriteBothPapers = 0;

    private Double percentagePassedPaperOneRewrites = 0.0;
    private Double percentagePassedPaperTwoRewrites = 0.0;

    private Double percentagePassedRewrites = 0.0;
    private Double percentageFailedRewrites = 0.0;

    private Exam exam;

    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    public Double getPercentageFailed() {
        if (totalNumberOfCandidates == 0) {
            return 0.0;
        }
        percentageFailed = (numberOfCandidatesFailed.doubleValue() / totalNumberOfCandidates.doubleValue()) * 100;
        return (double) Math.round(percentageFailed * 100) / 100;
    }

    public Double getPercentagePassed() {
        if (totalNumberOfCandidates == 0) {
            return 0.0;
        }
        percentagePassed = (numberOfCandidatesPassed.doubleValue() / totalNumberOfCandidates.doubleValue()) * 100;
        return (double) Math.round(percentagePassed * 100) / 100;
    }

    public ExamSetting getExamSetting() {
        return examSetting;
    }

    public void setExamSetting(ExamSetting examSetting) {
        this.examSetting = examSetting;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Integer getNumberOfCandidatesPassed() {
        return numberOfCandidatesPassed;
    }

    public void setNumberOfCandidatesPassed(Integer numberOfCandidatesPassed) {
        this.numberOfCandidatesPassed = numberOfCandidatesPassed;
    }

    public Integer getNumberOfCandidatesFailed() {
        return numberOfCandidatesFailed;
    }

    public void setNumberOfCandidatesFailed(Integer numberOfCandidatesFailed) {
        this.numberOfCandidatesFailed = numberOfCandidatesFailed;
    }

    public Integer getTotalNumberOfCandidates() {
        return totalNumberOfCandidates;
    }

    public void setTotalNumberOfCandidates(Integer totalNumberOfCandidates) {
        this.totalNumberOfCandidates = totalNumberOfCandidates;
    }

    public Boolean getShow() {
        if (totalNumberOfCandidates == 0) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }
    }

    public Integer getPaperOneTotalNumberOfCandidates() {
        return paperOneTotalNumberOfCandidates;
    }

    public void setPaperOneTotalNumberOfCandidates(Integer paperOneTotalNumberOfCandidates) {
        this.paperOneTotalNumberOfCandidates = paperOneTotalNumberOfCandidates;
    }

    public Integer getPaperTwoTotalNumberOfCandidates() {
        return paperTwoTotalNumberOfCandidates;
    }

    public void setPaperTwoTotalNumberOfCandidates(Integer paperTwoTotalNumberOfCandidates) {
        this.paperTwoTotalNumberOfCandidates = paperTwoTotalNumberOfCandidates;
    }

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    public Integer getTotalNumberOfRewrites() {
        return totalNumberOfRewrites;
    }

    public void setTotalNumberOfRewrites(Integer totalNumberOfRewrites) {
        this.totalNumberOfRewrites = totalNumberOfRewrites;
    }

    public Integer getPassedRewrites() {
        return passedRewrites;
    }

    public void setPassedRewrites(Integer passedRewrites) {
        this.passedRewrites = passedRewrites;
    }

    public Integer getFailedRewrites() {
        return failedRewrites;
    }

    public void setFailedRewrites(Integer failedRewrites) {
        this.failedRewrites = failedRewrites;
    }

    public Integer getPassedPaperOneRewrites() {
        return passedPaperOneRewrites;
    }

    public void setPassedPaperOneRewrites(Integer passedPaperOneRewrites) {
        this.passedPaperOneRewrites = passedPaperOneRewrites;
    }

    public Integer getFailedPaperOneRewrites() {
        return failedPaperOneRewrites;
    }

    public void setFailedPaperOneRewrites(Integer failedPaperOneRewrites) {
        this.failedPaperOneRewrites = failedPaperOneRewrites;
    }

    public Integer getPassedPaperTwoRewrites() {
        return passedPaperTwoRewrites;
    }

    public void setPassedPaperTwoRewrites(Integer passedPaperTwoRewrites) {
        this.passedPaperTwoRewrites = passedPaperTwoRewrites;
    }

    public Integer getFailedPaperTwoRewrites() {
        return failedPaperTwoRewrites;
    }

    public void setFailedPaperTwoRewrites(Integer failedPaperTwoRewrites) {
        this.failedPaperTwoRewrites = failedPaperTwoRewrites;
    }

    public Integer getTotalFailedBothPapers() {
        return totalFailedBothPapers;
    }

    public void setTotalFailedBothPapers(Integer totalFailedBothPapers) {
        this.totalFailedBothPapers = totalFailedBothPapers;
    }

    public Integer getTotalFailedRewriteBothPapers() {
        return totalFailedRewriteBothPapers;
    }

    public void setTotalFailedRewriteBothPapers(Integer totalFailedRewriteBothPapers) {
        this.totalFailedRewriteBothPapers = totalFailedRewriteBothPapers;
    }

    public Integer getTotalNumberFirstTime() {
        return totalNumberFirstTime;
    }

    public void setTotalNumberFirstTime(Integer totalNumberFirstTime) {
        this.totalNumberFirstTime = totalNumberFirstTime;
    }

    public Double getPercentageFirstTimePassed() {
        if (passedFirstTime == 0 || totalNumberFirstTime == 0) {
            return 0.0;
        }
        Double value = (passedFirstTime.doubleValue() / totalNumberFirstTime.doubleValue()) * 100;
        return (double) Math.round(value * 100) / 100;
    }

    public Double getPercentageFirstTimeFailed() {
        if (failedFirstTime == 0 || totalNumberFirstTime == 0) {
            return 0.0;
        }
         Double value = (failedFirstTime.doubleValue() / totalNumberFirstTime.doubleValue()) * 100;
        return (double) Math.round(value * 100) / 100;
    }

    public Double getPercentagePassedRewrites() {
        if (passedRewrites == 0 || totalNumberOfRewrites == 0) {
            return 0.0;
        }
        percentagePassedRewrites = (passedRewrites.doubleValue() / totalNumberOfRewrites.doubleValue()) * 100;
        return (double) Math.round(percentagePassedRewrites * 100) / 100;
    }

    public Double getPercentageFailedRewrites() {
        if (failedRewrites == 0 || totalNumberOfRewrites == 0) {
            return 0.0;
        }
        percentageFailedRewrites = (failedRewrites.doubleValue() / totalNumberOfRewrites.doubleValue()) * 100;
        return (double) Math.round(percentageFailedRewrites * 100) / 100;
    }

    public Double getPercentagePassedPaperOneRewrites() {
        return percentagePassedPaperOneRewrites;
    }

    public Double getPercentagePassedPaperTwoRewrites() {
        return percentagePassedPaperTwoRewrites;
    }

    public Integer getPassedFirstTime() {
        return passedFirstTime;
    }

    public void setPassedFirstTime(Integer passedFirstTime) {
        this.passedFirstTime = passedFirstTime;
    }

    public Integer getFailedFirstTime() {
        return failedFirstTime;
    }

    public void setFailedFirstTime(Integer failedFirstTime) {
        this.failedFirstTime = failedFirstTime;
    }

    public Integer getNumberOfCandidatesDisqualified() {
        return numberOfCandidatesDisqualified;
    }

    public void setNumberOfCandidatesDisqualified(Integer numberOfCandidatesDisqualified) {
        this.numberOfCandidatesDisqualified = numberOfCandidatesDisqualified;
    }
}
