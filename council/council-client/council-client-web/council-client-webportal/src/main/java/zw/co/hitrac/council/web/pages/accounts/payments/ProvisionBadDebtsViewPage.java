/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.accounts.payments;

import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.PageReference;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.domain.reports.PaymentDue;
import zw.co.hitrac.council.business.process.ProductFinder;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.CouncilDurationService;
import zw.co.hitrac.council.business.service.RegistrantActivityService;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.reports.PaymentDueReport;
import zw.co.hitrac.council.reports.RegistrantDataDBReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.accounts.IAccountingPage;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author tdhlakama
 */
public class ProvisionBadDebtsViewPage extends IAccountingPage {

    @SpringBean
    private CouncilDurationService councilDurationService;
    @SpringBean
    private RegistrantActivityService registrantActivityService;
    @SpringBean
    private RegistrationProcess registrationProcess;
    private CouncilDuration previousCouncilDuration;
    @SpringBean
    private ProductFinder productFinder;

    @SpringBean
    private DataSource dataSource;

    public ProvisionBadDebtsViewPage(final PageReference pageReference) {

        menuPanelManager.getSearchPanel().setTopMenuCurrent(true);
        menuPanelManager.getSearchPanel().setSearchAdvanced(true);

        PropertyModel<CouncilDuration> previousCouncilDurationModel = new PropertyModel<CouncilDuration>(this, "previousCouncilDuration");
        Form<?> form = new Form("form");
        form.add(new DropDownChoice("previousCouncilDuration", previousCouncilDurationModel, councilDurationService.findAll()).setRequired(true).add(new ErrorBehavior()));
        add(form);

        form.add(new Link("returnLink") {
            public void onClick() {
                setResponsePage(pageReference.getPage());
            }
        });

        form.add(new Button("process") {

            @Override
            public void onSubmit() {

                Thread t = new Thread() {
                    @Override
                    public synchronized void start()

                    {
                        printDetailReport();
                    }

                };
                t.start();

            }
        });

        form.add(new Button("processDue") {

            @Override
            public void onSubmit() {

                Thread t = new Thread() {
                    @Override
                    public synchronized void start()

                    {
                        printReport();
                    }

                };
                t.start();


            }
        });

        add(new FeedbackPanel("feedback"));

    }

    private void printDetailReport() {

        Map parameters = new HashMap();
        RegistrantDataDBReport registrantDataDBReport = new RegistrantDataDBReport();
        try {

            final Integer year = Integer.valueOf(previousCouncilDuration.getName());
            List<CouncilDuration> councilDurations = councilDurationService.findAll();
            councilDurations = councilDurations.stream()
                    .filter(councilDuration -> Integer.valueOf(councilDuration.getName()) > year)
                    .collect(Collectors.toList());

            List<Registrant> registrants = new ArrayList<>();
            if (!councilDurations.isEmpty()) {
                registrants = registrantActivityService.getRegistrantsRenewedInCouncilDurationNotInListOfCouncilDurations(previousCouncilDuration, councilDurations);
                registrants = registrants.stream().filter(registrant -> !registrant.getVoided() || !registrant.getDead()).collect(Collectors.toList());
            }else{
                error("List is Empty - No Periods Available");
                return;
            }

            parameters.put("comment", "Provision for bad debts: Individuals who renewed in " + previousCouncilDuration.getName() + " who have not renewed in period " + councilDurations);
            String idList = ReportResourceUtils.getRegistrantIdListFromRegistrantsList(registrants);
            parameters.put("list", idList);

            final Connection connection = dataSource.getConnection(); //DBConnect.getConnection();
            ByteArrayResource resource = null;

            try {
                resource = ReportResourceUtils.getReportResource(registrantDataDBReport, ContentType.PDF, connection, parameters);
            } finally {
                connection.close();
            }
            IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                    RequestCycle.get().getResponse(), null);

            resource.respond(a);

            // To make Wicket stop processing form after sending response
            RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
        } catch (JRException ex) {
            ex.printStackTrace();
            error(ex.getMessage());
        } catch (SQLException ex) {
            ex.printStackTrace();
            error(ex.getMessage());
        }

    }

    private void printReport() {

        final Integer year = Integer.valueOf(previousCouncilDuration.getName());
        List<CouncilDuration> councilDurations = councilDurationService.findAll();
        councilDurations = councilDurations.stream()
                .filter(councilDuration -> Integer.valueOf(councilDuration.getName()) > year)
                .collect(Collectors.toList());

        List<Registrant> registrants = new ArrayList<>();
        if (!councilDurations.isEmpty()) {
            registrants = registrantActivityService.getRegistrantsRenewedInCouncilDurationNotInListOfCouncilDurations(previousCouncilDuration, councilDurations);
            registrants = registrants.stream().filter(registrant -> !registrant.getVoided() || !registrant.getDead()).collect(Collectors.toList());
        }else{
            error("List is Empty - No Periods Available");
            return;
        }

        List<PaymentDue> paymentDues = new ArrayList<>();

        int countPeriodsToPay = councilDurations.size();
        for (Registrant registrant : registrants) {
            PaymentDue paymentDue = new PaymentDue();
            paymentDue.setRegistration(registrationProcess.activeRegisterNotStudentRegister(registrant));
            paymentDue.setLastRenewal(registrantActivityService.getLastCouncilDurationRenewal(registrant));
            paymentDue.setPeriodsDue(countPeriodsToPay);
            if (paymentDue.getRegistration() != null && paymentDue.getRegistration().getCourse() != null) {
                try {
                    Product renewalProduct = productFinder.searchAnnualProduct(paymentDue.getRegistration());
                    paymentDue.setTotalAmountForRenewal(calculateCost(countPeriodsToPay, renewalProduct.getProductPrice().getPrice()));

                    Product penaltyProduct = productFinder.searchPenaltyProduct(paymentDue.getRegistration());

                    paymentDue.setTotalAmountForPenalty(calculateCost(countPeriodsToPay, penaltyProduct.getProductPrice().getPrice()));

                    paymentDues.add(paymentDue);

                } catch (Exception e) {
                    System.out.print("-----------------------" + registrant.getFullname());
                    e.printStackTrace();
                }
            }
        }

        showPaymentDues((List<PaymentDue>) HrisComparator.sortPaymentsDueByCourseAndNameSurname(paymentDues), councilDurations, year);
    }

    private void showPaymentDues(List<PaymentDue> paymentDueList, List<CouncilDuration> yearsToPay, int year) {
        Map parameters = new HashMap();
        parameters.put("comment", "Provision for bad debts: Payments Due for" + yearsToPay);
        PaymentDueReport paymentDueReport = new PaymentDueReport();
        try {

            ReportResourceUtils.processJavaBeanReport(paymentDueReport, paymentDueList, parameters);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public BigDecimal calculateCost(int itemQuantity, BigDecimal itemPrice) {
        BigDecimal itemCost = BigDecimal.ZERO;
        BigDecimal totalCost = BigDecimal.ZERO;

        itemCost = itemPrice.multiply(new BigDecimal(itemQuantity));
        totalCost = totalCost.add(itemCost);
        return totalCost;
    }

}