package zw.co.hitrac.council.business.dao;

import zw.co.hitrac.council.business.domain.Citizenship;
import zw.co.hitrac.council.business.domain.IdentificationType;
import zw.co.hitrac.council.business.domain.IdentificationValidationRule;

import java.util.List;

/**
 * Created by clive on 6/16/15.
 */
public interface IdentificationValidationRuleDAO extends IGenericDAO<IdentificationValidationRule> {

    public List<IdentificationValidationRule> findByCitizenshipAndIdentificationType(Citizenship citizenship,
                                                                               IdentificationType identificationType);
}
