package zw.co.hitrac.council.web.pages.registration;

//~--- non-JDK imports ---------------------------------------

import com.googlecode.wicket.jquery.ui.panel.JQueryFeedbackPanel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantCPDCategory;
import zw.co.hitrac.council.business.domain.RegistrantCPDItem;
import zw.co.hitrac.council.business.domain.RegistrantCpd;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.process.RenewalProcess;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.certification.CertificationPage;

/**
 * @author Constance Mabaso
 * @author Matiashe Michael
 */
public class RegistrantCpdPage extends TemplatePage {

    @SpringBean
    private RegistrantCpdService registrantCpdService;
    private RegistrantData registrant;
    @SpringBean
    private DurationService durationService;
    @SpringBean
    private RegistrantCpdItemConfigService registrantCpdItemConfigService;
    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private RegistrantCPDItemService registrantCPDItemService;
    @SpringBean
    private RegistrantCPDCategoryService CPDCategoryService;
    private RegistrantCPDCategory registrantCPDCategory;
    private Course course;
    @SpringBean
    private RenewalProcess renewalProcess;
    @SpringBean
    RegistrantActivityService registrantActivityService;
    @SpringBean
    private CouncilDurationService councilDurationService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    private List<RegistrantCpd> items = new ArrayList<RegistrantCpd>();
    private BigDecimal total = BigDecimal.ZERO;
    private CouncilDuration councilDuration;

    public RegistrantCpdPage(final IModel<Registrant> registrantModel) {
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());

        PropertyModel<CouncilDuration> councilDurationModel = new PropertyModel<CouncilDuration>(this, "councilDuration");

        CompoundPropertyModel<RegistrantCpd> model = new CompoundPropertyModel<RegistrantCpd>(
                new RegistrantCpdPage.LoadableDetachableRegistrantCpdModel(
                        registrantModel.getObject()));

        setDefaultModel(model);

        PropertyModel<BigDecimal> totalModel = new PropertyModel<BigDecimal>(this, "total");

        Form<RegistrantCpd> form = new Form<RegistrantCpd>("form", (IModel<RegistrantCpd>) getDefaultModel()) {
            @Override
            public void onSubmit() {

                try {
                    items.add(getModel().getObject());
                    total = total.add(getModel().getObject().getRegistrantCPDItem().getPoints());

                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };
        if(generalParametersService.get().getShowActiveDurationsOnly()){
            form.add(new DropDownChoice("councilDuration", councilDurationModel, councilDurationService.getActiveCouncilDurations()).setRequired(true).add(new ErrorBehavior()));
        }
        else
        {form.add(new DropDownChoice("councilDuration", councilDurationModel, councilDurationService.findAll()).setRequired(true).add(new ErrorBehavior()));}
        DropDownChoice<RegistrantCPDCategory> registrantCPDCategorys
                = new DropDownChoice<RegistrantCPDCategory>("registrantCPDCategory",
                new PropertyModel<RegistrantCPDCategory>(this, "registrantCPDCategory"),
                new RegistrantCPDCategorysModel()) {
            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }

            @Override
            protected void onSelectionChanged(RegistrantCPDCategory newSelection) {
                RegistrantCpd registrantCpd = (RegistrantCpd) RegistrantCpdPage.this.getDefaultModelObject();

                registrantCpd.setRegistrantCPDItem(null);
            }
        };

        form.add(registrantCPDCategorys.setRequired(true).add(new ErrorBehavior()));
        DropDownChoice<RegistrantCPDItem> registrantCPDItems = new DropDownChoice<RegistrantCPDItem>("registrantCPDItem", new RegistrantCPDItemsModel());
        form.add(registrantCPDItems.setRequired(true).add(new ErrorBehavior()));
        // FeedbackPanel //
        final FeedbackPanel feedback = new JQueryFeedbackPanel("feedback");
        add(feedback.setOutputMarkupId(true));

        form.add(new Link<Void>("saveItems") {
            @Override
            public void onClick() {

                if (items.isEmpty()) {
                    setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
                }
                renewalProcess.addCPDs(registrantModel.getObject(), councilDuration, items);
                boolean adequatePoints = registrantCpdService.getCurrentCPDs(registrantService.get(registrantModel.getObject().getId()), councilDuration).compareTo(registrantCpdItemConfigService.get().getTotalCpdPointsPermissable()) >= 0;
                CouncilDuration lastCouncilDuration = registrantActivityService.getLastCouncilDurationRenewal(registrantModel.getObject());

                if (generalParametersService.get().getCaptureCPDsDirectly() || generalParametersService.get().getCaptureCPDsDirectlyAndInDirectly()) {
                    if (adequatePoints && lastCouncilDuration != null && lastCouncilDuration.equals(councilDuration)) {
                        setResponsePage(new CertificationPage(registrantModel.getObject().getId()));
                    } else {
                        setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
                    }
                } else {
                    setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
                }
            }
        });
        add(new Label("totalPoints", totalModel));

        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });
        add(form);
        add(new Label("registrant.fullname"));

        add(new PropertyListView<RegistrantCpd>("items", items) {
            @Override
            protected void populateItem(ListItem<RegistrantCpd> item) {
                item.add(new Label("registrantCPDItem.name"));
                item.add(new Label("registrantCPDItem.points"));
            }
        });

    }

    private final class RegistrantCPDCategorysModel extends LoadableDetachableModel<List<? extends RegistrantCPDCategory>> {

        protected List<? extends RegistrantCPDCategory> load() {
            return CPDCategoryService.findAll();
        }
    }

    private class RegistrantCPDItemsModel extends LoadableDetachableModel<List<? extends RegistrantCPDItem>> {

        protected List<? extends RegistrantCPDItem> load() {
            if (registrantCPDCategory != null) {
                return registrantCPDItemService.getRegistrantCPDItems(registrantCPDCategory);
            } else {
                return new ArrayList<RegistrantCPDItem>();
            }
        }
    }

    private final class LoadableDetachableRegistrantCpdModel extends LoadableDetachableModel<RegistrantCpd> {

        private Long id;
        private Registrant registrant;

        public LoadableDetachableRegistrantCpdModel(Long id) {
            this.id = id;
        }

        public LoadableDetachableRegistrantCpdModel(Registrant registrant) {
            this.registrant = registrant;
        }

        @Override
        protected RegistrantCpd load() {
            RegistrantCpd registrantCpd = null;

            if (id == null) {
                registrantCpd = new RegistrantCpd();
                registrantCpd.setRegistrant(registrant);
            } else {
                registrantCpd = registrantCpdService.get(id);
            }

            return registrantCpd;
        }
    }
}
