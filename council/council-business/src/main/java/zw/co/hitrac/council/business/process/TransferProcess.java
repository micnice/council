package zw.co.hitrac.council.business.process;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.domain.RegistrantTransfer;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.business.utils.CouncilException;

import java.util.Date;

/**
 * @author Michael Matiashe
 */
@Service
public class TransferProcess {

    @Autowired
    private GeneralParametersService generalParametersService;
    @Autowired
    private RegistrationService registrationService;
    @Autowired
    private RegistrantActivityService registrantActivityService;
    @Autowired
    private ProductIssuer productIssuer;
    @Autowired
    private RegistrationProcess registrationProcess;
    @Autowired
    private RegistrantTransferService registrantTransferService;
    @Autowired
    private BillingProcess billingProcess;
    @Autowired
    private RegistrantActivityProcess registrantActivityProcess;
    @Autowired
    private DurationService durationService;
    @Autowired
    private DebtComponentService debtComponentService;

    @Transactional
    public RegistrantTransfer transfer(RegistrantTransfer registrantTransfer) {

        DebtComponent debtComponent = null;
        DebtComponent debtComponent1 = null;

        Registration registration = registrationProcess.activeRegisterNotStudentRegister(registrantTransfer.getRegistrant());
        Registration studentRegistration = registrationProcess.activeStudentRegister(registrantTransfer.getRegistrant());

        if (registration == null && studentRegistration == null) {
            throw new CouncilException("No Active Registration");
        } else {
            if (registration == null) {
                registration = studentRegistration;
            }
        }

        registration.setDeRegistrationDate(new Date());
        registrantTransferService.save(registrantTransfer);
        registration = registrationService.save(registration);
        Duration duration = null;
        Registration newRegistration = new Registration();
        newRegistration.setRegistrant(registration.getRegistrant());
        duration = durationService.getCurrentCourseDuration(registrantTransfer.getCourse());
        newRegistration.setCourse(registrantTransfer.getCourse());
        newRegistration.setDuration(duration);
        newRegistration.setRegistrationDate(new Date());
        newRegistration.setInstitution(registration.getInstitution());
        newRegistration.setRegister(registrantTransfer.getTransferRule().getToRegister());
        newRegistration = registrationService.save(newRegistration);

        if (generalParametersService.get().getHasProcessedByBoard()) {
            debtComponent = billingProcess.billAnnualProduct(newRegistration, debtComponent);
            debtComponent1 = billingProcess.billRegistrationProduct(newRegistration, debtComponent1);
        }

        Course course = newRegistration.getCourse();
        registrationProcess.createRegistrationNumberByCourse(registrantTransfer.getRegistrant(), registrantTransfer.getCourse());

        //Issue Registration and Practising certificate
        if (course.getProductIssuanceType() == null) {
            throw new CouncilException("Registration Certificate Type not Set for the Course");
        } else {
            productIssuer.issueRegistrationOrAdditionalQualficationCertificate(newRegistration, debtComponent1);
            productIssuer.issueProductPracticeCertificate(newRegistration, debtComponent);
        }

        registrantActivityProcess.registrationAndRenewalActivity(registration, debtComponent, duration);
        registrantActivityProcess.transferRegistrantActivity(registration, debtComponent, duration);
        return registrantTransfer;
    }
}
