package zw.co.hitrac.council.business.service;

import java.util.List;
import zw.co.hitrac.council.business.domain.Conditions;
import zw.co.hitrac.council.business.domain.Register;

/**
 *
 * @author Michael Matiashe
 */
public interface ConditionService extends IGenericService<Conditions> {
    List<Conditions> findAll(Boolean examCondition);
    List<Conditions> getRegisterConditions();
}
