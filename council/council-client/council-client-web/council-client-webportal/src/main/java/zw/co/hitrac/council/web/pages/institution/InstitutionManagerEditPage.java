package zw.co.hitrac.council.web.pages.institution;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.InstitutionManager;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.web.utility.CustomDateTextField;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

import java.util.List;

//~--- JDK imports ------------------------------------------------------------

/**
 * @author Tatenda Chiwandire
 * @author Michael Matiashe
 * @author tdhlakama
 */
public class InstitutionManagerEditPage extends IInstitutionView {

    @SpringBean
    private InstitutionManagerService institutionManagerService;
    @SpringBean
    private InstitutionTypeService institutionTypeService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private DistrictService districtService;
    @SpringBean
    private CityService cityService;

    public InstitutionManagerEditPage(Long id, CompoundPropertyModel<Institution> institutionIModel, CompoundPropertyModel<Registrant> registrantIModel) {

        final CompoundPropertyModel<InstitutionManager> model = new CompoundPropertyModel<InstitutionManager>(
                new InstitutionManagerEditPage.LoadableDetachableInstitutionManagerModel(id, institutionIModel, registrantIModel));

        setDefaultModel(model);

        Form<InstitutionManager> form = new Form<InstitutionManager>("form", (IModel<InstitutionManager>) getDefaultModel()) {
            @Override
            public void onSubmit() {

                try {
                    InstitutionManager institutionManager = getModelObject();

                    if (institutionManager.isInstitutionSupervisor() && institutionManager.getInstitutionSupervisorStartDate() == null) {

                        error("Enter Supervisor Start Date");
                        return;
                    }

                    if (institutionManager.isPractitionerInCharge() && institutionManager.getPractitionerInChargeStartDate() == null) {

                        error("Enter Practitioner in Charge Start Date");
                        return;
                    }

                    institutionManager = institutionManagerService.save(institutionManager);

                    setResponsePage(new InstitutionViewPage(institutionManager.getInstitution().getId()));

                } catch (Exception ex) {
                    error("Error Saving Institution Manager");
                    return;
                }

            }
        };

        form.add(new CustomDateTextField("practitionerInChargeStartDate").add(DatePickerUtil.getDatePicker()));
        form.add(new CustomDateTextField("practitionerInChargeEndDate").add(DatePickerUtil.getDatePicker()));
        form.add(new CustomDateTextField("institutionSupervisorStartDate").add(DatePickerUtil.getDatePicker()));
        form.add(new CustomDateTextField("institutionSupervisorEndDate").add(DatePickerUtil.getDatePicker()));

        form.add(new CheckBox("practitionerInCharge"));
        form.add(new CheckBox("institutionSupervisor"));

        form.add(new Label("institution"));
        form.add(new Label("registrant.fullname"));
        add(new FeedbackPanel("feedback"));

        IModel<List<InstitutionManager>> institutionManagerModelList = new LoadableDetachableModel<List<InstitutionManager>>() {

            @Override
            protected List<InstitutionManager> load() {

                return institutionManagerService.findInstitutionManagers(model.getObject().getRegistrant(), null);
            }
        };

        IModel<List<InstitutionManager>> institutionManagerList = new LoadableDetachableModel<List<InstitutionManager>>() {

            @Override
            protected List<InstitutionManager> load() {

                return institutionManagerService.findInstitutionManagers(null, model.getObject().getInstitution());
            }
        };

        add(new Label("institutionsSupervised", new LoadableDetachableModel<String>() {

            @Override
            protected String load() {
                String names = "";
                for (InstitutionManager institutionManager : institutionManagerModelList.getObject()) {
                    if (institutionManager.isInstitutionSupervisor() && institutionManager.getInstitutionSupervisorEndDate() == null) {
                        names = names + institutionManager.getInstitution().getName() + " - ";
                    }
                }
                if (names.isEmpty()) {
                    return "N/A";
                }
                names = names.substring(0, names.length() - 2);
                return names;
            }
        }));

        add(new Label("currentlySupervisedBy", new LoadableDetachableModel<String>() {

            @Override
            protected String load() {
                String names = "";
                for (InstitutionManager institutionManager : institutionManagerList.getObject()) {
                    if (institutionManager.isInstitutionSupervisor() && institutionManager.getInstitutionSupervisorEndDate() == null) {
                        names = names + institutionManager.getRegistrant().getFullname() + " - ";
                    }
                }
                if (names.isEmpty()) {
                    return "N/A";
                }
                names = names.substring(0, names.length() - 2);
                return names;
            }
        }));

        add(form);
    }

    private final class LoadableDetachableInstitutionManagerModel extends LoadableDetachableModel<InstitutionManager> {

        private Long id;
        private IModel<Institution> institutionIModel;
        private IModel<Registrant> registrantIModel;

        public LoadableDetachableInstitutionManagerModel(Long id, CompoundPropertyModel<Institution> institutionIModel, CompoundPropertyModel<Registrant> registrantIModel) {
            this.id = id;
            this.institutionIModel = institutionIModel;
            this.registrantIModel = registrantIModel;
        }

        @Override
        protected InstitutionManager load() {
            InstitutionManager institutionManager;
            if (id == null) {
                institutionManager = new InstitutionManager();
                Institution institution = institutionIModel.getObject();
                Registrant registrant = registrantIModel.getObject();
                institutionManager.setRegistrant(registrant);
                institutionManager.setInstitution(institution);
            } else {
                institutionManager = institutionManagerService.get(id);
            }
            return institutionManager;
        }
    }
}
