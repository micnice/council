package zw.co.hitrac.council.web.menu;

import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.pages.reports.ViewReportsPage;

import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;

/**
 *
 * @author Edward Zengeni
 */
public class ViewReportsMenuPanel extends MenuPanel {

    private Link<Void> viewReportsLink;

    public ViewReportsMenuPanel(String id) {
        super(id);
        add(viewReportsLink = new BookmarkablePageLink<Void>("viewReportsLink", ViewReportsPage.class) {
            @Override
            protected void onConfigure() {
                setVisibilityAllowed(CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.EDUCATION_OFFICER))));
            }
        });
    }

    @Override
    protected void onConfigure() {
        addCurrentBehavior(viewReportsLink, topMenuCurrent);
    }
}
