package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Conditions;
import zw.co.hitrac.council.business.service.ConditionService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;

/**
 *
 * @author Michael Matiashe
 */
public class ConditionListPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private ConditionService conditionService;

    public ConditionListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new ConditionEditPage(null));
            }
        });

        IModel<List<Conditions>> model = new LoadableDetachableModel<List<Conditions>>() {
            @Override
            protected List<Conditions> load() {
                return conditionService.findAll();
            }
        };
        PropertyListView<Conditions> eachItem = new PropertyListView<Conditions>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<Conditions> item) {
                Link<Conditions> viewLink = new Link<Conditions>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new ConditionViewPage(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(viewLink);
                viewLink.add(new Label("name"));
                item.add(new Label("productIssuanceType"));
            }
        };

        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
