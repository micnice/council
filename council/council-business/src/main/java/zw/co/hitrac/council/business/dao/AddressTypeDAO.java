package zw.co.hitrac.council.business.dao;

import zw.co.hitrac.council.business.domain.AddressType;

/**
 *
 * @author Charles Chigoriwa
 */
public interface AddressTypeDAO extends IGenericDAO<AddressType> {
	public AddressType findByName(String name);
}
