
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.MisconductType;
import zw.co.hitrac.council.business.service.MisconductTypeService;

/**
 *
 * @author hitrac
 */
public class MisconductTypeViewPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private MisconductTypeService misconductTypeService;

    public MisconductTypeViewPage(Long id) {
        CompoundPropertyModel<MisconductType> model =
            new CompoundPropertyModel<MisconductType>(
                new MisconductTypeViewPage.LoadableDetachableMisconductTypeModel(id));

        setDefaultModel(model);
        add(new Link<MisconductType>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new MisconductTypeEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", MisconductTypeListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("misconductType", model.bind("name")));
    }

    private final class LoadableDetachableMisconductTypeModel extends LoadableDetachableModel<MisconductType> {
        private Long id;

        public LoadableDetachableMisconductTypeModel(Long id) {
            this.id = id;
        }

        @Override
        protected MisconductType load() {
            return misconductTypeService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
