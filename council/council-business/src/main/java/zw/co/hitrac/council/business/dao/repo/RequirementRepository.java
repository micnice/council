package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.Requirement;

/**
 *
 * @author Michael Matiashe
 */
public interface RequirementRepository extends CrudRepository<Requirement, Long> {
    public List<Requirement> findAll();
}
