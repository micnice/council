package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.CourseDAO;
import zw.co.hitrac.council.business.dao.repo.CourseRepository;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.CourseGroup;
import zw.co.hitrac.council.business.domain.CourseType;
import zw.co.hitrac.council.business.domain.Qualification;
import zw.co.hitrac.council.business.domain.Tier;

/**
 *
 * @author Charles Chigoriwa
 */
@Repository
public class CourseDAOImpl implements CourseDAO {

    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    private CourseRepository courseRepository;

    public Course save(Course course) {
        return courseRepository.save(course);
    }

    public List<Course> findAll() {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Course.class);
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        criteria.addOrder(Order.asc("name"));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public Course get(Long id) {
        return courseRepository.findOne(id);
    }

    public void setCourseRepository(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    public List<Course> getCourses(Boolean basic, CourseGroup courseGroup, Qualification qualification, CourseType courseType, Tier tier) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Course.class);
        if (basic != null) {
            criteria.add(Restrictions.eq("basic", basic));
        }
        if (courseGroup != null) {
            criteria.add(Restrictions.eq("courseGroup", courseGroup));
        }
        if (qualification != null) {
            criteria.add(Restrictions.eq("qualification", qualification));
        }
        if (courseType != null) {
            criteria.add(Restrictions.eq("courseType", courseType));
        }
        if (qualification != null) {
            criteria.add(Restrictions.eq("qualification", qualification));
        }
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        criteria.addOrder(Order.asc("name"));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<Course> findAll(Boolean retired) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Course.class);
        criteria.add(Restrictions.eq("retired", retired));
        criteria.addOrder(Order.asc("name"));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public Long getNumberOfModulePapers(Course course) {
        return (Long)entityManager.createQuery("SELECT DISTINCT count(m) from ModulePaper m where m.course=:course")
                .setParameter("course", course)
                .getSingleResult();
    }

	@Override
	public Course findByPrefixName(String prefixName) {
		return courseRepository.findByPrefixName(prefixName);
	}
}
