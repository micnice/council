
package zw.co.hitrac.council.business.dao.repo.accounts;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.accounts.PaymentDetails;
import zw.co.hitrac.council.business.domain.accounts.Prepayment;
import zw.co.hitrac.council.business.domain.accounts.PrepaymentDetails;

/**
 *
 * @author Michael Matiashe
 */
public interface PrepaymentDetailsRepository extends CrudRepository<PrepaymentDetails, Long> {

    public List<PrepaymentDetails> findAll();
    public List<PrepaymentDetails> findByUid(String uid);
    public List<PrepaymentDetails> findByPrepayment(Prepayment prepayment);
    public PrepaymentDetails findByPaymentDetails(PaymentDetails paymentDetails);
    public List<PrepaymentDetails> findByCustomerAndCancelled(Customer customer, Boolean cancelled);
    
}
