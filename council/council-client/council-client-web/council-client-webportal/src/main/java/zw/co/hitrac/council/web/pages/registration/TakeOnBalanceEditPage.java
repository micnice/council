package zw.co.hitrac.council.web.pages.registration;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.accounts.AccountService;
import zw.co.hitrac.council.business.utils.TakeOnBalanceDMO;

/**
 *
 * @author Takunda Dhlakama
 * @author Matiashe Michael
 */
public class TakeOnBalanceEditPage extends TemplatePage {

    @SpringBean
    private AccountService accountService;
    @SpringBean
    private GeneralParametersService generalParametersService;

    public TakeOnBalanceEditPage(final Long id) {
        menuPanelManager.getRegistrantionPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantionPanel().setRegistrantEditPageCurrent(true);
        setDefaultModel(new CompoundPropertyModel<TakeOnBalanceDMO>(new LoadableDetachableTakeOnBalanceDMOModel(new TakeOnBalanceDMO())));
        add(new RegistrantPanel("registrantPanel", id));

        Form<TakeOnBalanceDMO> form = new Form<TakeOnBalanceDMO>("form", (IModel<TakeOnBalanceDMO>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    getModelObject().setId(id);
                    accountService.getRegistrantTakeOnBalance(getModelObject());
                    setResponsePage(new RegistrantViewPage(id));
                } catch (CouncilException e) {
                    error(e.getMessage());
                }
            }
        };

        form.add(new Label("yearLabel", "Year") {
            @Override
            protected void onConfigure() {
                if (!generalParametersService.get().getRegistrationByApplication()) {
                    setVisible(Boolean.FALSE);
                } else {
                    setVisible(Boolean.TRUE);
                }
            }
        });

        form.add(new TextField<String>("year") {
            @Override
            protected void onConfigure() {
                if (!generalParametersService.get().getRegistrationByApplication()) {
                    setVisible(Boolean.FALSE);
                } else {
                    setVisible(Boolean.TRUE);
                }
            }
        }.setRequired(Boolean.TRUE).add(new ErrorBehavior()));

        form.add(new Label("debitBalanceLabel", "Debit Balance") {
            @Override
            protected void onConfigure() {
                if (!generalParametersService.get().getRegistrationByApplication()) {
                    setVisible(Boolean.FALSE);
                } else {
                    setVisible(Boolean.TRUE);
                }
            }
        });

        form.add(new TextField<String>("debitBalance") {
            @Override
            protected void onConfigure() {
                if (!generalParametersService.get().getRegistrationByApplication()) {
                    setVisible(Boolean.FALSE);
                } else {
                    setVisible(Boolean.TRUE);
                }
            }
        });

        form.add(new TextField<String>("creditBalance"));
        form.add(new BookmarkablePageLink<Void>("registrantListPage", RegistrantListPage.class));
        add(form);
        add(new FeedbackPanel("feedback"));
    }

    private final class LoadableDetachableTakeOnBalanceDMOModel extends LoadableDetachableModel<TakeOnBalanceDMO> {

        public LoadableDetachableTakeOnBalanceDMOModel(TakeOnBalanceDMO takeOnBalanceDMO) {
        }

        @Override
        protected TakeOnBalanceDMO load() {
            TakeOnBalanceDMO takeOnBalanceDMO = new TakeOnBalanceDMO();

            return takeOnBalanceDMO;
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
