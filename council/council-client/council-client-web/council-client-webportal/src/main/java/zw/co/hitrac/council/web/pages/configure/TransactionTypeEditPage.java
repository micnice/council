package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Book;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.Effect;
import zw.co.hitrac.council.business.domain.accounts.TransactionType;
import zw.co.hitrac.council.business.service.accounts.AccountService;
import zw.co.hitrac.council.business.service.accounts.TransactionTypeService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.accounts.AccountsReceivablePage;
import zw.co.hitrac.council.web.pages.accounts.IAccountsReceivablePage;
import zw.co.hitrac.council.web.pages.accounts.TransactionTypeListPage;
import zw.co.hitrac.council.web.pages.accounts.TransactionTypeViewPage;

//~--- JDK imports ------------------------------------------------------------

import java.util.Arrays;

/**
 *
 * @author Matiashe Michael
 */
public class TransactionTypeEditPage extends IAccountsReceivablePage {
    @SpringBean
    private TransactionTypeService transactionTypeService;
    @SpringBean
    private AccountService         accountService;

    public TransactionTypeEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<TransactionType>(new LoadableDetachableTransactionTypeModel(id)));
        add(new BookmarkablePageLink<Void>("accountsReceivableLink", AccountsReceivablePage.class));

        Form<TransactionType> form = new Form<TransactionType>("form", (IModel<TransactionType>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                transactionTypeService.save(getModelObject());
                setResponsePage(new TransactionTypeViewPage(getModelObject().getId()));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new DropDownChoice("effect", Arrays.asList(Effect.values())));
        form.add(new DropDownChoice("book", Arrays.asList(Book.values())));
        form.add(new DropDownChoice("drLedger", accountService.getGeneralLedgerAccounts(),
                                    new AccountChoiceRenderer()));
        form.add(new DropDownChoice("crLedger", accountService.getGeneralLedgerAccounts(),
                                    new AccountChoiceRenderer()));
        form.add(new BookmarkablePageLink<Void>("returnLink", TransactionTypeListPage.class));
        add(form);
    }

    private static class AccountChoiceRenderer implements IChoiceRenderer<Account> {
        public Object getDisplayValue(Account account) {
            return account.getName();
        }

        public String getIdValue(Account account, int index) {
            return account.getId().toString();
        }
    }


    private final class LoadableDetachableTransactionTypeModel extends LoadableDetachableModel<TransactionType> {
        private Long id;

        public LoadableDetachableTransactionTypeModel(Long id) {
            this.id = id;
        }

        @Override
        protected TransactionType load() {
            if (id == null) {
                return new TransactionType();
            }

            return transactionTypeService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
