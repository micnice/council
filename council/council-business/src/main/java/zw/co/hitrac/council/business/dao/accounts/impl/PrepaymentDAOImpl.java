package zw.co.hitrac.council.business.dao.accounts.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.accounts.PrepaymentDAO;
import zw.co.hitrac.council.business.dao.repo.accounts.PrepaymentRepository;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.accounts.Prepayment;

/**
 * @author Michael Matiashe
 */
@Repository
public class PrepaymentDAOImpl implements PrepaymentDAO {

    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private PrepaymentRepository prepaymentRepository;

    public Prepayment save(Prepayment t) {
        return prepaymentRepository.save(t);
    }

    public List<Prepayment> findAll() {
        return prepaymentRepository.findAll();
    }

    public Prepayment get(Long id) {
        return prepaymentRepository.findOne(id);
    }

    public PrepaymentRepository getPrepaymentRepository() {
        return prepaymentRepository;
    }

    public void setPrepaymentRepository(PrepaymentRepository prepaymentRepository) {
        this.prepaymentRepository = prepaymentRepository;
    }

    public List<Prepayment> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Prepayment> findByInstitution(Institution institution) {
        return entityManager.createQuery("SELECT p FROM Prepayment p WHERE p.canceled=FALSE AND p.institution=:institution ").setParameter("institution", institution).getResultList();
    }

    public List<Prepayment> findAvailablePrepayments() {
        return entityManager.createQuery("SELECT p FROM Prepayment p WHERE p.balance<0 AND p.canceled=FALSE").getResultList();
    }

    public List<Prepayment> findAll(Date endDate) {
        return entityManager.createQuery("SELECT p FROM Prepayment p WHERE p.canceled=FALSE AND p.dateOfPayment<= :endDate ").setParameter("endDate", endDate).getResultList();
    }

    public BigDecimal getPrepaymentBalance(Date endDate, Prepayment prepayment) {
        return (BigDecimal) prepayment.getAmount().subtract(getReceiptItemUsedTotalByDate(endDate, prepayment));
    }

    private BigDecimal getReceiptItemUsedTotalByDate(Date endDate, Prepayment prepayment) {
        SimpleDateFormat format = new SimpleDateFormat("YYYY-MM-dd");
        BigDecimal amountUsed = (BigDecimal) entityManager.createNativeQuery("SELECT sum(receiptitem.amount)  FROM `receiptitem` inner join receiptheader on receiptheader.id=receiptitem.receiptHeader_id inner join paymentdetails on receiptheader.paymentDetails_id=paymentdetails.id inner join prepaymentdetails on prepaymentdetails.paymentDetails_id=paymentdetails.id WHERE paymentdetails.dateOfPayment<=? AND prepaymentdetails.prepayment_id=? and prepaymentdetails.cancelled=0 and receiptheader.reversed=0")
                .setParameter(1, format.format(endDate))
                .setParameter(2, prepayment.getId())
                .getSingleResult();
        if (amountUsed != null) {
            return amountUsed;
        } else {
            return new BigDecimal("0");
        }
    }

    public List<Institution> withPrepayments() {
        return entityManager.createQuery("SELECT p.institution FROM Prepayment p WHERE p.balance<0 AND p.canceled=FALSE").getResultList();
    }

    public List<Prepayment> findByCanceled(Boolean canceled) {
        return prepaymentRepository.findByCanceled(canceled);
    }

}
