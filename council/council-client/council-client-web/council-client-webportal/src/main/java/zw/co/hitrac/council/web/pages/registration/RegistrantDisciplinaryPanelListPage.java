/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.registration;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantDisciplinary;
import zw.co.hitrac.council.web.utility.CustomDateLabel;
import zw.co.hitrac.council.web.utility.CustomDateTimeLabel;

/**
 * @author hitrac
 */
public class RegistrantDisciplinaryPanelListPage extends Panel {

    /**
     * Constructor
     *
     * @param id
     */
    public RegistrantDisciplinaryPanelListPage(String id) {
        super(id);

        // - Used to Display a to list RegistrantDisciplinarys
        add(new PropertyListView<RegistrantDisciplinary>("registrantDisciplinaryList") {
            @Override
            protected void populateItem(ListItem<RegistrantDisciplinary> item) {
                Link<RegistrantDisciplinary> viewLink = new Link<RegistrantDisciplinary>
                        ("registrantDisciplinaryViewPage",
                                item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new RegistrantDisciplinaryViewPage(getModelObject().getId()));
                    }
                };

                item.add(viewLink);
                item.add(new CustomDateLabel("caseDate"));
                item.add(new CustomDateLabel("dateResolved"));
                item.add(new Label("misconductType"));
                item.add(new Label("caseOutcome"));
                item.add(new Label("createdBy"));
                item.add(new Label("modifiedBy"));
                item.add(CustomDateTimeLabel.forDate("dateCreated",
                        new PropertyModel<>(item.getModel(), "dateCreated")));
                item.add(CustomDateTimeLabel.forDate("dateModified",
                        new PropertyModel<>(item.getModel(), "dateModified")));


            }
        });
    }

    /**
     * * @param registrant
     */
    public RegistrantDisciplinaryPanelListPage(String id, final IModel<Registrant> registrantModel) {
        this(id);
        add(new Link<Registrant>("registrantDisciplinaryPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantDisciplinaryPage(null, registrantModel));
            }
        });
    }



}


//~ Formatted by Jindent --- http://www.jindent.com
