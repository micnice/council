package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.ModulePaperDAO;
import zw.co.hitrac.council.business.dao.repo.ModulePaperRepository;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.ModulePaper;

/**
 *
 * @author Matiashe Michael
 */
@Repository
public class ModulePaperDAOImpl implements ModulePaperDAO {

    @Autowired
    private ModulePaperRepository modulePaperRepository;

    public ModulePaper save(ModulePaper modulePaper) {
        return modulePaperRepository.save(modulePaper);
    }

    public List<ModulePaper> findAll() {
        return modulePaperRepository.findAll();
    }

    public ModulePaper get(Long id) {
        return modulePaperRepository.findOne(id);
    }

    /**
     * A setter method that will make mocking repo object easier
     *
     * @param ModulePaperRepository
     */
    public void setModulePaperRepository(ModulePaperRepository modulePaperRepository) {
        this.modulePaperRepository = modulePaperRepository;
    }
    @PersistenceContext
    EntityManager entityManager;

    public List<ModulePaper> findPapersInCourse(Course course) {
        return entityManager.createQuery("select m from ModulePaper m where m.course=:course").setParameter("course", course).getResultList();
    }

    public List<ModulePaper> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
