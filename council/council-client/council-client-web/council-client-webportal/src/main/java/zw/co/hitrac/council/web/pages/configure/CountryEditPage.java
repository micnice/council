package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.PageReference;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Country;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.service.CountryService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.registration.RegistrantAddressEditPage;

/**
 *
 * @author Charles Chigoriwa
 */
public class CountryEditPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private CountryService countryService;

    public CountryEditPage(Long id, final PageReference pageReference) {
        setDefaultModel(new CompoundPropertyModel<Country>(new LoadableDetachableCountryModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        Form<Country> form = new Form<Country>("form", (IModel<Country>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                countryService.save(getModelObject());
                setResponsePage(pageReference.getPage());
            }
        };
        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new TextField<String>("isoCode1"));
        form.add(new TextField<String>("isoCode2"));
        form.add(new TextField<String>("iana"));
        form.add(new TextField<String>("ioc"));
        form.add(new TextField<String>("itu"));
        form.add(new TextField<String>("un"));
        form.add(new TextField<String>("iso"));
        form.add(new Link<Void>("returnLink") {

            @Override
            public void onClick() {
                setResponsePage(pageReference.getPage());
            }
        });

        add(form);
    }

    public CountryEditPage(Long id, final IModel<Registrant> registrantModel, final IModel<Institution> institutionModel) {
        setDefaultModel(new CompoundPropertyModel<Country>(new LoadableDetachableCountryModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        Form<Country> form = new Form<Country>("form", (IModel<Country>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                countryService.save(getModelObject());
                if (registrantModel != null) {
                    setResponsePage(new RegistrantAddressEditPage(registrantModel));
                } else if (institutionModel != null) {
                    setResponsePage(new RegistrantAddressEditPage(institutionModel.getObject()));
                }
            }
        };
        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new TextField<String>("isoCode1"));
        form.add(new TextField<String>("isoCode2"));
        form.add(new TextField<String>("iana"));
        form.add(new TextField<String>("ioc"));
        form.add(new TextField<String>("itu"));
        form.add(new TextField<String>("un"));
        form.add(new TextField<String>("iso"));
        form.add(new BookmarkablePageLink<Void>("returnLink", CountryListPage.class));
        add(form);
    }

    private final class LoadableDetachableCountryModel extends LoadableDetachableModel<Country> {

        private Long id;

        public LoadableDetachableCountryModel(Long id) {
            this.id = id;
        }

        @Override
        protected Country load() {
            if (id == null) {
                return new Country();
            }
            return countryService.get(id);
        }
    }
}
