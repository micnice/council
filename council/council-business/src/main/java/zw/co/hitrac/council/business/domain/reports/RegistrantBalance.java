/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain.reports;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.Customer;

/**
 *
 * @author tdhlakama
 */
public class RegistrantBalance {

    private Registrant registrant;
    private Institution institution;
    private BigDecimal amountUsed = BigDecimal.ZERO;
    private BigDecimal carryforward = BigDecimal.ZERO;
    private BigDecimal currentBalance = BigDecimal.ZERO;
    private BigDecimal initialBalance = BigDecimal.ZERO;
    private String customer;
    private Customer customerAccount;
    private Date dateOfDeposit;
    private Date dateOfPayment;

    public BigDecimal getInitialBalance() {
        return initialBalance;
    }

    public void setInitialBalance(BigDecimal initialBalance) {
        this.initialBalance = initialBalance;
    }

    @Temporal(TemporalType.DATE)
    public Date getDateOfDeposit() {
        return dateOfDeposit;
    }

    public void setDateOfDeposit(Date dateOfDeposit) {
        this.dateOfDeposit = dateOfDeposit;
    }

    @Temporal(TemporalType.DATE)
    public Date getDateOfPayment() {
        return dateOfPayment;
    }

    public void setDateOfPayment(Date dateOfPayment) {
        this.dateOfPayment = dateOfPayment;
    }
    
    

    public Registrant getRegistrant() {
        return registrant;
    }

    public void setRegistrant(Registrant registrant) {
        this.registrant = registrant;
    }

    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    public BigDecimal getAmountUsed() {
        return amountUsed;
    }

    public void setAmountUsed(BigDecimal amountUsed) {
        this.amountUsed = amountUsed;
    }

    public BigDecimal getCarryforward() {
        return carryforward;
    }

    public void setCarryforward(BigDecimal carryforward) {
        this.carryforward = carryforward;
    }

    public BigDecimal getBalanceRemaining() {
        BigDecimal balance = BigDecimal.ZERO;
        balance = getAmountUsed().add(getCarryforward());
        if (balance == null) {
            return BigDecimal.ZERO;
        } else {
            return balance;
        }
    }

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    public Customer getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(Customer customerAccount) {
        this.customerAccount = customerAccount;
    }

    public String getCustomer() {
        if (registrant != null) {
            return registrant.getFullname();
        }
        if (institution != null) {
            return institution.getName();
        }
        return "";
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + (this.getCustomerAccount().getId() != null ? this.getCustomerAccount().getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RegistrantBalance other = (RegistrantBalance) obj;
        if ((this.getCustomerAccount().getId() == null) ? (other.getCustomerAccount().getId() != null) : !this.getCustomerAccount().getId().equals(other.getCustomerAccount().getId())) {
            return false;
        }
        return true;
    }

}
