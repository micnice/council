package zw.co.hitrac.council.business.dao.repo.accounts;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.accounts.TransactionComponent;

/**
 *
 * @author Judge Muzinda
 * @author Constance Mabaso
 */
public interface TransactionComponentReposiroty extends CrudRepository<TransactionComponent, Long> {
    
    public List<TransactionComponent> findAll();
}
