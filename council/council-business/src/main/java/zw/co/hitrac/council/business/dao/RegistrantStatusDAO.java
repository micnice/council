package zw.co.hitrac.council.business.dao;

import zw.co.hitrac.council.business.domain.RegistrantStatus;

/**
 *
 * @author Michael Matiashe
 */
public interface RegistrantStatusDAO extends IGenericDAO<RegistrantStatus> {
    
}
