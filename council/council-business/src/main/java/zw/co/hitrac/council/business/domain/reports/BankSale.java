/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain.reports;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import zw.co.hitrac.council.business.domain.accounts.TransactionType;

/**
 *
 * @author tdhlakama
 */
public class BankSale implements Serializable {

    private BigDecimal amountPaid = BigDecimal.ZERO;
    private BigDecimal carryForward = BigDecimal.ZERO;
    private TransactionType transactionType;
    private Date startDate;
    private Date endDate;

    public BigDecimal getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }

    public BigDecimal getCarryForward() {
        return carryForward;
    }

    public void setCarryForward(BigDecimal carryForward) {
        this.carryForward = carryForward;
    }
    
    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    
}
