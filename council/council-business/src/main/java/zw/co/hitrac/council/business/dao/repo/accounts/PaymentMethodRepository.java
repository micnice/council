
package zw.co.hitrac.council.business.dao.repo.accounts;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.accounts.PaymentMethod;


/**
 *
 * @author  Michael Matiashe
 */
public interface PaymentMethodRepository extends CrudRepository<PaymentMethod, Long> {

    public List<PaymentMethod> findAll();
}
