package zw.co.hitrac.council.webservices.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantQualification;
import zw.co.hitrac.council.business.service.GenericService;
import zw.co.hitrac.council.business.service.RegistrantQualificationService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.webservices.api.RegistrantQualificationWebService;
import zw.co.hitrac.council.webservices.api.domain.LeanRegistrantQualification;

/**
 *
 * @author Daniel Nkhoma
 */
@Component
public class RegistrantQualificationWebServiceImpl implements RegistrantQualificationWebService {

    @Autowired
    private RegistrantQualificationService registrantQualificationService;
    @Autowired
    private GenericService genericService;
    @Autowired
    private RegistrantService registrantService;

    @Override
    public List<RegistrantQualification> registrantQualifications(Registrant registrant) {
        return registrantQualificationService.getRegistrantQualifications(registrant);
    }

    @Override
    public List<RegistrantQualification> registrantQualifications(String registrationNumber) {
        Registrant registrant = registrantService.getRegistrantByRegistrationNumber(registrationNumber);
        return registrant != null ? registrantQualifications(registrant) : Collections.EMPTY_LIST;
    }

    @Override
    public List<RegistrantQualification> registrantQualifications() {
        return registrantQualificationService.findAll();
    }

    public List<LeanRegistrantQualification> leanRegistrantQualifications() {
        List<LeanRegistrantQualification> leanRegistrantQualifications = new ArrayList<LeanRegistrantQualification>();
        for (Long registrantQualificationId : genericService.getAllLean("RegistrantQualification")) {
            LeanRegistrantQualification leanRegistrantQualification = new LeanRegistrantQualification();
            leanRegistrantQualification.setRegistrantQualificationId(registrantQualificationId);
            leanRegistrantQualifications.add(leanRegistrantQualification);
        }
        return leanRegistrantQualifications;
    }

    @Override
    public RegistrantQualification registrantQualification(Long id) {
        return registrantQualificationService.get(id);
    }
}
