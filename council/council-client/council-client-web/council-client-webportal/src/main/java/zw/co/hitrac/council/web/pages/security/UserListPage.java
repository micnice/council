package zw.co.hitrac.council.web.pages.security;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.User;
import zw.co.hitrac.council.business.service.UserService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.configure.AdministerDatabasePage;
import zw.co.hitrac.council.web.pages.configure.IAdministerDatabaseBasePage;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;

/**
 *
 * @author Clive Gurure
 */
public class UserListPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private UserService userService;

    public UserListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new UserEditPage((null)));
            }
        });

        IModel<List<User>> model = new LoadableDetachableModel<List<User>>() {
            @Override
            protected List<User> load() {
                return userService.findAll();
            }
        };
        PropertyListView<User> eachItem = new PropertyListView<User>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<User> item) {
                Link<User> viewLink = new Link<User>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new UserViewPage(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(viewLink);
                viewLink.add(new Label("username"));
                               
            }
        };

        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
