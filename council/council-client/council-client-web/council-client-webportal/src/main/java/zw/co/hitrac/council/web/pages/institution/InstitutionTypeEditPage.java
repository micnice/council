package zw.co.hitrac.council.web.pages.institution;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxCheckBox;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.CheckBoxMultipleChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.InstitutionType;
import zw.co.hitrac.council.business.service.InstitutionTypeService;
import zw.co.hitrac.council.business.service.RequirementService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.RequirementListModel;
import zw.co.hitrac.council.web.pages.configure.AdministerDatabasePage;
import zw.co.hitrac.council.web.pages.configure.IAdministerDatabaseBasePage;

/**
 *
 * @author Kelvin Goredema
 */
public class InstitutionTypeEditPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private InstitutionTypeService institutionTypeService;
    @SpringBean
    private RequirementService requirementService;

    public InstitutionTypeEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<InstitutionType>(new LoadableDetachableInstitutionTypeModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));

        Form<InstitutionType> form = new Form<InstitutionType>("form", (IModel<InstitutionType>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                institutionTypeService.save(getModelObject());
                setResponsePage(new InstitutionTypeViewPage(getModelObject().getId()));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink", InstitutionTypeListPage.class));
        add(form);
        
        final WebMarkupContainer wmc = new WebMarkupContainer("wmc");
        wmc.setVisible(false);
        wmc.setOutputMarkupPlaceholderTag(true);
        form.add(wmc);
        form.add(new AjaxCheckBox("advanced", new PropertyModel(wmc, "visible")) {
            @Override
            protected void onUpdate(AjaxRequestTarget art) {
                art.add(wmc);
            }
        });
        wmc.add(new CheckBoxMultipleChoice("requirements", new RequirementListModel(requirementService)));
    }

    private final class LoadableDetachableInstitutionTypeModel extends LoadableDetachableModel<InstitutionType> {

        private Long id;

        public LoadableDetachableInstitutionTypeModel(Long id) {
            this.id = id;
        }

        @Override
        protected InstitutionType load() {
            if (id == null) {
                return new InstitutionType();
            }

            return institutionTypeService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
