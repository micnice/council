/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.reports;

import au.com.bytecode.opencsv.CSVWriter;
import net.sf.dynamicreports.report.builder.crosstab.CrosstabBuilder;
import net.sf.dynamicreports.report.builder.crosstab.CrosstabColumnGroupBuilder;
import net.sf.dynamicreports.report.builder.crosstab.CrosstabRowGroupBuilder;
import net.sf.dynamicreports.report.constant.Calculation;
import net.sf.dynamicreports.report.constant.PageOrientation;
import net.sf.dynamicreports.report.constant.PageType;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import org.apache.commons.io.IOUtils;
import org.apache.wicket.PageReference;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.reports.RegisterStatistic;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;
import zw.co.hitrac.council.web.utility.Templates;

import java.io.*;
import java.util.*;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;
import zw.co.hitrac.council.reports.RegistrantDataReport;
import zw.co.hitrac.council.reports.SummaryRegisterReport;

/**
 * @author tdhlakama
 */
public class ReportDurationPage extends TemplatePage {

    @SpringBean
    private CouncilDurationService councilDurationService;
    @SpringBean
    private RegistrantActivityService registrantActivityService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    private CouncilDuration councilDuration;
    @SpringBean
    private CourseService courseService;
    @SpringBean
    private RegistrationService registrationService;
    private HrisComparator hrisComparator = new HrisComparator();
    @SpringBean
    private RegisterService registerService;
    private GeneralParameters generalParameters = generalParametersService.get();
    Register main = generalParametersService.get().getMainRegister();
    Register provisional = generalParametersService.get().getProvisionalRegister();
    Register student = generalParametersService.get().getStudentRegister();
    Register intern = generalParametersService.get().getInternRegister();
    Register maintaince = generalParametersService.get().getMaintenanceRegister();
    Register specialit = generalParametersService.get().getSpecialistRegister();
    private boolean reportsIsByCourse = generalParameters.isReportsByCourse();
    private ContentType contentType;

    public ReportDurationPage(final PageReference pageReference) {

        menuPanelManager.getSearchPanel().setTopMenuCurrent(true);
        menuPanelManager.getSearchPanel().setSearchAdvanced(true);

        PropertyModel<CouncilDuration> councilDurationModel = new PropertyModel<CouncilDuration>(this, "councilDuration");
        PropertyModel<ContentType> contentTypeModel = new PropertyModel<ContentType>(this, "contentType");
        Form<?> form = new Form("form");

        form.add(new DropDownChoice("councilDuration", councilDurationModel, councilDurationService.findAll()).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("contentType", contentTypeModel, Arrays.asList(ContentType.values())));
        add(form);

        form.add(new Link("returnLink") {

            public void onClick() {

                setResponsePage(pageReference.getPage());
            }
        });

        form.add(new Button("process") {

            @Override
            public void onSubmit() {

                if (generalParametersService.get().getDynamicDisciplineReport()) {
                    showSummaryReportDynamic(reportsIsByCourse);
                } else {
                    showSummaryReport(reportsIsByCourse);
                }

            }
        }
        );

        form.add(new Button("showAsExcel") {

            Register studentRegister = generalParameters.getStudentRegister();

            @Override
            public void onSubmit() {

                try {

                    Set<RegistrantData> registrants = new HashSet<RegistrantData>();
                    Set<Register> registers = new HashSet<Register>();
                    registers.addAll(registerService.findAll());
                    registers.remove(studentRegister);

                    List<Duration> durations = new ArrayList<Duration>(councilDurationService.get(councilDuration.getId()).getDurations());

                    for (Register register : registers) {
                        for (Course course : courseService.findAll()) {
                            registrants.addAll(registrantActivityService.getListPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(course,
                                    register,
                                    durations,
                                    RegistrantActivityType.RENEWAL,
                                    Boolean.TRUE, null, null));
                        }
                    }

                    File file = File.createTempFile("register_list", ".excel");
                    CSVWriter writer = new CSVWriter(new FileWriter(file));
                    writer.writeNext(RegistrantData.getRegistrantMappedDataHeader());

                    for (RegistrantData registrant : registrants) {
                        writer.writeNext(registrant.getRegistrantMappedData());
                    }

                    writer.close();

                    ContentType contentType = ContentType.EXCEL;
                    ByteArrayResource resource = null;
                    FileInputStream fileInputStream = new FileInputStream(file);

                    try {
                        resource = ReportResourceUtils.getReportResource(contentType, IOUtils.toByteArray(fileInputStream), "Register_Active_List.excel");

                    } finally {
                        fileInputStream.close();
                        file.delete();
                    }

                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
        );

        add(new FeedbackPanel("feedback"));

        form.add(new Button("active") {

            @Override
            public void onSubmit() {

                try {

                    Set<RegistrantData> registrants = new HashSet<RegistrantData>();
                    Set<Register> registers = new HashSet<Register>();
                    registers.addAll(registerService.findAll());
                    registers.remove(student);

                    List<Duration> durations = new ArrayList<Duration>(councilDurationService.get(councilDuration.getId()).getDurations());

                    for (Register register : registers) {
                        for (Course course : courseService.findAll()) {
                            registrants.addAll(registrantActivityService.getListPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(course,
                                    register,
                                    durations,
                                    RegistrantActivityType.RENEWAL,
                                    Boolean.TRUE, null, null));
                        }
                    }

                    RegistrantDataReport registrantDataReport = new RegistrantDataReport();
                    Map parameters = new HashMap();
                    parameters.put("comment", "Active - " + RegistrantActivityType.RENEWAL + "s for Period " + councilDuration.getName());
                    parameters.put("activityType", RegistrantActivityType.RENEWAL);
                    ByteArrayResource resource
                            = ReportResourceUtils.getReportResource(registrantDataReport, contentType, registrants,
                                    parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }

            }
        }
        );

    }

    private void showSummaryReportDynamic(boolean reportsIsByCourse) {

        int totalRegisters = registerService.getRegisterDisciplines().size();

        final int widthPercentage = (100 / totalRegisters) * 5;

        List<RegisterStatistic> registerStatistics = generateRegisterStats(reportsIsByCourse);
        JRDataSource dataSource = createDataSource(registerStatistics);

        CrosstabRowGroupBuilder<String> rowGroup = ctab.rowGroup("course", String.class)
                .setTotalHeader("Total for Course");
        CrosstabColumnGroupBuilder<String> columnGroup = ctab.columnGroup("register", String.class);

        CrosstabBuilder crossTab = ctab.crosstab()
                .headerCell(cmp.text("Course / Register").setStyle(Templates.boldCenteredStyle))
                .rowGroups(rowGroup)
                .columnGroups(columnGroup).setCellWidth(widthPercentage)
                .measures(
                        ctab.measure("Active", "activeRegistrants", Integer.class, Calculation.SUM),
                        ctab.measure("InActive", "inActiveRegistrants", Integer.class, Calculation.SUM));

        try {

            File tempFile = File.createTempFile("register", ".pdf");
            FileOutputStream fileOutputStream = new FileOutputStream(tempFile);

            report()
                    .setPageFormat(PageType.A4, PageOrientation.LANDSCAPE)
                    .setTemplate(Templates.reportTemplate)
                    .title(Templates.createTitleComponent("Discipline Statistics - Renewal for Period " + councilDuration.getName()))
                    .summary(crossTab)
                    .pageFooter(Templates.footerComponent)
                    .setDataSource(dataSource)
                    .toPdf(fileOutputStream);

            fileOutputStream.close();

            FileInputStream fileInputStream = new FileInputStream(tempFile);

            ByteArrayResource resource = ReportResourceUtils.getReportResource(contentType,
                    IOUtils.toByteArray(fileInputStream), "Register_List.pdf");

            fileInputStream.close();

            IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                    RequestCycle.get().getResponse(), null);

            resource.respond(a);

            tempFile.delete();

            // To make Wicket stop processing form after sending responjsse
            RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

        } catch (DRException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void showSummaryReport(boolean reportsIsByCourse) {

        List<RegisterStatistic> registerStatistics = new ArrayList<RegisterStatistic>();

        RegistrantActivityType registrantActivityType = RegistrantActivityType.RENEWAL;

        for (Course c : courseService.findAll()) {
            RegisterStatistic statistic = new RegisterStatistic();
            statistic.setCourse(c);
            statistic.setMainRegister(main);
            statistic.setStudentRegister(student);
            statistic.setProvisionalRegister(provisional);
            if (intern != null) {
                statistic.setInternRegister(intern);
            }
            if (maintaince != null) {
                statistic.setMaintainanceRegister(maintaince);
            }

            if (specialit != null) {
                statistic.setSpecialistRegister(specialit);
            }
            statistic.setNumberOfRegistrantsInStudentRegister(statistic.getNumberOfRegistrantsInStudentRegister() + registrationService.getActiveRegistrations(statistic.getCourse(), student));
            List<Duration> durations = new ArrayList<Duration>();
            for (Duration d : councilDurationService.get(councilDuration.getId()).getDurations()) {
                durations.add(d);
            }

            if (reportsIsByCourse) {

                statistic.setNumberOfRegistrantsInMainRegister(registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getMainRegister(), durations, registrantActivityType, Boolean.TRUE, null));
                statistic.setNumberOfRegistrantsInProvisionalRegister(registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getProvisionalRegister(), durations, registrantActivityType, Boolean.TRUE, null));

                statistic.setNumberOfInActiveRegistrantsInMainRegister(registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getMainRegister(), durations, registrantActivityType, Boolean.FALSE, null));
                statistic.setNumberOfInActiveRegistrantsInProvisionalRegister(registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getProvisionalRegister(), durations, registrantActivityType, Boolean.FALSE, null));

                if (statistic.getInternRegister() != null) {
                    statistic.setNumberOfRegistrantsInInternRegister(registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getInternRegister(), durations, registrantActivityType, Boolean.TRUE, null));
                    statistic.setNumberOfInActiveRegistrantsInInternRegister(registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getInternRegister(), durations, registrantActivityType, Boolean.FALSE, null));
                }
                if (statistic.getMaintainanceRegister() != null) {
                    statistic.setNumberOfRegistrantsInMaintainceRegister(registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getMaintainanceRegister(), durations, registrantActivityType, Boolean.TRUE, null));
                    statistic.setNumberOfInActiveRegistrantsInMaintainceRegister(registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getMaintainanceRegister(), durations, registrantActivityType, Boolean.FALSE, null));
                }
                if (statistic.getSpecialistRegister() != null) {
                    statistic.setNumberOfRegistrantsInSpecialistRegister(registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getSpecialistRegister(), durations, registrantActivityType, Boolean.TRUE, null));
                    statistic.setNumberOfInActiveRegistrantsInSpecialistRegister(registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getSpecialistRegister(), durations, registrantActivityType, Boolean.FALSE, null));
                }
            } else {
                statistic.setNumberOfRegistrantsInMainRegister(registrantActivityService.getTotalPerCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getMainRegister(), durations, registrantActivityType, Boolean.TRUE));
                statistic.setNumberOfRegistrantsInProvisionalRegister(registrantActivityService.getTotalPerCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getProvisionalRegister(), durations, registrantActivityType, Boolean.TRUE));

                statistic.setNumberOfInActiveRegistrantsInMainRegister(registrantActivityService.getTotalPerCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getMainRegister(), durations, registrantActivityType, Boolean.FALSE));
                statistic.setNumberOfInActiveRegistrantsInProvisionalRegister(registrantActivityService.getTotalPerCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getProvisionalRegister(), durations, registrantActivityType, Boolean.FALSE));

                if (statistic.getInternRegister() != null) {
                    statistic.setNumberOfRegistrantsInInternRegister(registrantActivityService.getTotalPerCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getInternRegister(), durations, registrantActivityType, Boolean.TRUE));
                    statistic.setNumberOfInActiveRegistrantsInInternRegister(registrantActivityService.getTotalPerCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getInternRegister(), durations, registrantActivityType, Boolean.FALSE));
                }
                if (statistic.getMaintainanceRegister() != null) {
                    statistic.setNumberOfRegistrantsInMaintainceRegister(registrantActivityService.getTotalPerCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getMaintainanceRegister(), durations, registrantActivityType, Boolean.TRUE));
                    statistic.setNumberOfInActiveRegistrantsInMaintainceRegister(registrantActivityService.getTotalPerCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getMaintainanceRegister(), durations, registrantActivityType, Boolean.FALSE));
                }
                if (statistic.getSpecialistRegister() != null) {
                    statistic.setNumberOfRegistrantsInSpecialistRegister(registrantActivityService.getTotalPerCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getSpecialistRegister(), durations, registrantActivityType, Boolean.TRUE));
                    statistic.setNumberOfInActiveRegistrantsInSpecialistRegister(registrantActivityService.getTotalPerCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getSpecialistRegister(), durations, registrantActivityType, Boolean.FALSE));
                }
            }

            Integer active = statistic.getNumberOfRegistrantsInInternRegister() + statistic.getNumberOfRegistrantsInMainRegister() + statistic.getNumberOfRegistrantsInMaintainceRegister() + statistic.getNumberOfRegistrantsInProvisionalRegister() + statistic.getNumberOfRegistrantsInSpecialistRegister() + statistic.getNumberOfRegistrantsInStudentRegister();
            Integer inactive = statistic.getNumberOfInActiveRegistrantsInInternRegister() + statistic.getNumberOfInActiveRegistrantsInMainRegister() + statistic.getNumberOfInActiveRegistrantsInMaintainceRegister() + statistic.getNumberOfInActiveRegistrantsInProvisionalRegister() + statistic.getNumberOfInActiveRegistrantsInSpecialistRegister() + statistic.getNumberOfInActiveRegistrantsInStudentRegister();
            if ((active + inactive) != 0) {
                registerStatistics.add(statistic);
            }

        }

        try {
            Map parameters = new HashMap();
            parameters.put("comment", " Discipline Statistics - " + registrantActivityType + " for Period " + councilDuration.getName());

            SummaryRegisterReport disciplineStatisticReport = new SummaryRegisterReport();

            ByteArrayResource resource
                    = ReportResourceUtils.getReportResource(disciplineStatisticReport, contentType, registerStatistics,
                            parameters);
            IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                    RequestCycle.get().getResponse(), null);

            resource.respond(a);

            // To make Wicket stop processing form after sending response
            RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
        } catch (JRException ex) {
            ex.printStackTrace(System.out);
        }
    }

    private List<RegisterStatistic> generateRegisterStats(boolean reportsIsByCourse) {

        List<RegisterStatistic> registerStatistics = new ArrayList<RegisterStatistic>();

        int activeCount;
        int inActiveCount;

        int comservActive = 0;
        int comservInActive = 0;

        List<Register> neededRegisters = registerService.getRegisterDisciplines();
        Register studentRegister = generalParameters.getStudentRegister();

        try {

            List<Duration> durations = new ArrayList<Duration>(councilDurationService.get(councilDuration.getId()).getDurations());

            for (Course course : courseService.findAll()) {

                for (Register register : neededRegisters) {

                    RegisterStatistic registerStatistic = new RegisterStatistic();
                    registerStatistic.setCourse(course);
                    registerStatistic.setRegister(register);

                    Boolean hasComserv = false;
                    Conditions comserv = generalParametersService.get().getMainRegisterCondition();
                    if (comserv != null) {
                        if (registerStatistic.getRegister().getId().equals(specialit.getId())) {
                            registerStatistic.getRegister().setName("CommServe");
                            hasComserv = true;
                        }
                    }

                    if (register.equals(studentRegister)) {

                        activeCount = registrationService.getActiveRegistrations(course, register);

                        registerStatistic.setNumberOfActiveRegistrants(activeCount);

                    }
                    if (reportsIsByCourse) {

                        if (hasComserv) {
                            comservActive = registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(course, main, durations, RegistrantActivityType.RENEWAL, true, comserv);
                            comservInActive = registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(course, main, durations, RegistrantActivityType.RENEWAL, false, comserv);
                        }

                        if (registerStatistic.getRegister().getId().equals(main.getId())) {
                            activeCount = registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(course, register, durations, RegistrantActivityType.RENEWAL, true, null);
                            inActiveCount = registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(course, register, durations, RegistrantActivityType.RENEWAL, false, null);
                            activeCount = activeCount - comservActive;
                            inActiveCount = inActiveCount - comservInActive;
                        } else if (registerStatistic.getRegister().getId().equals(specialit.getId())) {
                            activeCount = registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(course, main, durations, RegistrantActivityType.RENEWAL, true, comserv);
                            inActiveCount = registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(course, main, durations, RegistrantActivityType.RENEWAL, false, comserv);
                        } else {
                            activeCount = registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(course, register, durations, RegistrantActivityType.RENEWAL, true, null);
                            inActiveCount = registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(course, register, durations, RegistrantActivityType.RENEWAL, false, null);
                        }

                        registerStatistic.setNumberOfActiveRegistrants(activeCount);
                        registerStatistic.setNumberOfInactiveRegistrants(inActiveCount);

                    } else if (!reportsIsByCourse) {

                        activeCount = registrantActivityService.getTotalPerCourseAndDurationAndRegisterAndRegistrantActivity(
                                course, register, durations, RegistrantActivityType.RENEWAL, true);

                        inActiveCount = registrantActivityService.getTotalPerCourseAndDurationAndRegisterAndRegistrantActivity(
                                course, register, durations, RegistrantActivityType.RENEWAL, false);

                        registerStatistic.setNumberOfActiveRegistrants(activeCount);
                        registerStatistic.setNumberOfInactiveRegistrants(inActiveCount);

                    }

                    registerStatistics.add(registerStatistic);

                }

            }
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }

        return registerStatistics;
    }

    /**
     * http://www.dynamicreports.org/examples/crosstabreport
     *
     * @param registerStatistics
     * @return a datasource for jasper
     */
    private JRDataSource createDataSource(List<RegisterStatistic> registerStatistics) {

        DRDataSource dataSource = new DRDataSource("course", "register", "activeRegistrants", "inActiveRegistrants");

        for (RegisterStatistic registerStatistic : registerStatistics) {

            dataSource.add(registerStatistic.getCourse().getName(), registerStatistic.getRegister().getName().replace("REGISTER", ""),
                    registerStatistic.getNumberOfActiveRegistrants(), registerStatistic.getNumberOfInactiveRegistrants());
        }

        return dataSource;
    }
}
