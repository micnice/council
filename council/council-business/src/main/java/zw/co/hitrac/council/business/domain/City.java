package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Michael Matiashe
 * @author Charles Chigoriwa
 */
@Entity
@Table(name="city")
@Audited
@XmlRootElement
public class City extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    private Country country;
    private District district;

    @ManyToOne
    @JoinColumn(name = "country_id", nullable = true, updatable = true)
    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @ManyToOne
    @JoinColumn(name = "district_id", nullable = true, updatable = true)
    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }
}
