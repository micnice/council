package zw.co.hitrac.council.web.pages.accounts.documents;

import java.util.ArrayList;
import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Check;
import org.apache.wicket.markup.html.form.CheckGroup;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.domain.accounts.Document;
import zw.co.hitrac.council.business.process.CreditNoteProcess;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.service.accounts.AccountService;
import zw.co.hitrac.council.business.service.accounts.CustomerService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.accounts.IAccountingPage;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;

/**
 *
 * @author Charles Chigoriwa
 * @author Michael Matiashe
 */
public class ViewRemovedNotesPage extends IAccountingPage {

    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    AccountService accountService;
    @SpringBean
    CustomerService customerAccountService;
    @SpringBean
    RegistrationService registrationService;
    @SpringBean
    DebtComponentService debtComponentService;
    @SpringBean
    private CreditNoteProcess creditNoteProcess;
    @SpringBean
    private GeneralParametersService generalParametersService;

    private List<DebtComponent> selectedDebtComponents = new ArrayList<DebtComponent>();

    public ViewRemovedNotesPage(PageParameters parameters) {
        this(parameters.get("registrantId").toLong());
    }

    public ViewRemovedNotesPage(Long id) {

        final CompoundPropertyModel<Registrant> registrantModel = new CompoundPropertyModel<Registrant>(new DetachableRegistrantModel(id, registrantService));
        setDefaultModel(registrantModel);
        add(new Label("fullname"));

        IModel<List<DebtComponent>> debtComponentsModel = new LoadableDetachableModel<List<DebtComponent>>() {

            @Override
            protected List<DebtComponent> load() {
                return debtComponentService.getViewRemovedDeptComponets(registrantModel.getObject().getCustomerAccount());
            }
        };

        PropertyListView<DebtComponent> eachItem = new PropertyListView<DebtComponent>("eachItem", debtComponentsModel) {
            @Override
            protected void populateItem(final ListItem<DebtComponent> item) {

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(new Label("product.name"));
                item.add(new Label("comment"));
                item.add(new Label("modifiedBy"));                
            }
        };
        add(eachItem);

        add(new Link<Registrant>("registrantPaymentPage") {
            @Override
            public void onClick() {
                setResponsePage(new DebtComponentsPage(registrantModel.getObject().getId()));
            }
        });

    }
}
