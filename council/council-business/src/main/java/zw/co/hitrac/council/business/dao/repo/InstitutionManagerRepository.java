/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.repo;

import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.InstitutionManager;

import java.util.List;

/**
 *
 * @author edwin
 */
public interface InstitutionManagerRepository extends CrudRepository<InstitutionManager, Long> {

}
