/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.process;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.business.service.accounts.AccountService;
import zw.co.hitrac.council.business.service.accounts.AccountsParametersService;
import zw.co.hitrac.council.business.utils.CouncilException;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Random;

/**
 * @author Matiashe Michael
 */
@Service
public class BillingProcess {

    @Autowired
    private ProductFinder productFinder;
    @Autowired
    private InvoiceProcess invoiceProcess;
    @Autowired
    private RegistrationProcess registrationProcess;
    @Autowired
    private GeneralParametersService generalParametersService;
    @Autowired
    private ApplicationProcess applicationProcess;
    @Autowired
    private RegistrationService registrationService;
    @Autowired
    private DurationService durationService;
    @Autowired
    private AccountsParametersService accountsParametersService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private RegistrantActivityService registrantActivityService;
    @Autowired
    private ProductIssuanceService productIssuanceService;

    public DebtComponent billRegistrationProduct(Registration registration, DebtComponent debtComponent) {
        if (!this.generalParametersService.get().isBillingModule()) {
            return null;
        }
        Product productFound = productFinder.searchRegistrationProduct(registration);
        if (productFound.getProductPrice() == null) {
            throw new CouncilException("Product price for " + productFound.getName() + " not set");
        }
        BigDecimal price = productFound.getProductPrice().getPrice();
        Customer customer = registration.getRegistrant().getCustomerAccount();
        Account salesAccount = productFound.getSalesAccount();
        if (salesAccount == null) {
            throw new CouncilException("Sales Account for " + productFound.getName() + " not set");
        }
        debtComponent = invoiceProcess.invoice(price, customer, salesAccount, productFound);
        return debtComponent;
    }

    public DebtComponent billRegistrationOfNewQualificationProduct(Registration registration, Course course) {
        if (!this.generalParametersService.get().isBillingModule()) {
            return null;
        }
        DebtComponent debtComponent = null;
        Product productFound = productFinder.searchRegistrationOfNewQualificationProduct(registration, course);
        if (productFound.getProductPrice() == null) {
            throw new CouncilException("Product price for " + productFound.getName() + " not set");
        }
        if (course.getProductIssuanceType() == null) {
            throw new CouncilException("Registrantion Certificate Type not Set for the Course");
        }
        BigDecimal price = productFound.getProductPrice().getPrice();
        Customer customer = registration.getRegistrant().getCustomerAccount();
        Account salesAccount = productFound.getSalesAccount();
        if (salesAccount == null) {
            throw new CouncilException("Sales Account for " + productFound.getName() + " not set");
        }
        debtComponent = invoiceProcess.invoice(price, customer, salesAccount, productFound);
        return debtComponent;
    }

    public DebtComponent billLateRegistrationProduct(Registration registration) {
        if (!this.generalParametersService.get().isBillingModule()) {
            return null;
        }
        Product productFound = productFinder.searchLateRegistrationProduct(registration);
        if (productFound.getProductPrice() == null) {
            throw new CouncilException("Product price for " + productFound.getName() + " not set");
        }
        BigDecimal price = productFound.getProductPrice().getPrice();
        Customer customer = registration.getRegistrant().getCustomerAccount();
        Account salesAccount = productFound.getSalesAccount();
        if (salesAccount == null) {
            throw new CouncilException("Sales Account for " + productFound.getName() + " not set");
        }
        DebtComponent debtComponent = invoiceProcess.invoice(price, customer, salesAccount, productFound);
        return debtComponent;
    }

    public DebtComponent billReRegistrationProduct(Registration registration, DebtComponent debtComponent) {
        if (!this.generalParametersService.get().isBillingModule()) {
            return null;
        }
        Product productFound = productFinder.searchReRegistrationProduct(registration);
        if (productFound.getProductPrice() == null) {
            throw new CouncilException("Product price for " + productFound.getName() + " not set");
        }
        BigDecimal price = productFound.getProductPrice().getPrice();
        Customer customer = registration.getRegistrant().getCustomerAccount();
        Account salesAccount = accountService.get(productFound.getSalesAccount().getId());
        if (salesAccount == null) {
            throw new CouncilException("Sales Account for " + productFound.getName() + " not set");
        }
        debtComponent = invoiceProcess.invoice(price, customer, salesAccount, productFound);
        return debtComponent;
    }


    public DebtComponent billReRegistrationProduct(Registration registration) {
        if (!this.generalParametersService.get().isBillingModule()) {
            return null;
        }
        Product productFound = productFinder.searchReRegistrationProduct(registration);
        if (productFound.getProductPrice() == null) {
            throw new CouncilException("Product price for " + productFound.getName() + " not set");
        }
        BigDecimal price = productFound.getProductPrice().getPrice();
        Customer customer = registration.getRegistrant().getCustomerAccount();
        Account salesAccount = accountService.get(productFound.getSalesAccount().getId());
        if (salesAccount == null) {
            throw new CouncilException("Sales Account for " + productFound.getName() + " not set");
        }
        DebtComponent debtComponent = null;
        debtComponent = invoiceProcess.invoice(price, customer, salesAccount, productFound);
        return debtComponent;
    }

    public DebtComponent billLateRegistrationProduct(Registrant registrant, Course course) {
        if (!this.generalParametersService.get().isBillingModule()) {
            return null;
        }
        Registration registration = new Registration();
        registration.setCourse(course);
        registration.setRegistrant(registrant);
        return billLateRegistrationProduct(registration);
    }

    public DebtComponent billReRegistrationProduct(Registrant registrant, Course course) {
        if (!this.generalParametersService.get().isBillingModule()) {
            return null;
        }
        Registration registration = new Registration();
        registration.setCourse(course);
        registration.setRegistrant(registrant);
        return billReRegistrationProduct(registration);
    }

    public DebtComponent billAnnualProduct(Registration registration, DebtComponent debtComponent, Application... applications) {
        if (!this.generalParametersService.get().isBillingModule()) {
            return null;
        }
        Product productFound1;
        Application application = null;
        if (applications != null && applications.length >= 1) {
            application = applications[0];
        }

        if (registration.getApplication() != null) {
            productFound1 = productFinder.searchAnnualProduct(registration, registration.getApplication().getQualificationType());
        } else {
            productFound1 = productFinder.searchAnnualProduct(registration);
        }
        if (productFound1.getProductPrice() == null) {
            throw new CouncilException("Product price for " + productFound1.getName() + " not set");
        }
        BigDecimal price1 = productFound1.getProductPrice().getPrice();
        Customer customer1 = registration.getRegistrant().getCustomerAccount();
        Account salesAccount1 = accountService.get(productFound1.getSalesAccount().getId());
        debtComponent = invoiceProcess.invoice(price1, customer1, salesAccount1, productFound1);
        return debtComponent;
    }

    public DebtComponent billPenaltyProduct(Registration registration) {
        if (!this.generalParametersService.get().isBillingModule()) {
            return null;
        }
        Product productFound = productFinder.searchPenaltyProduct(registration);
        if (productFound.getProductPrice() == null) {
            throw new CouncilException("Product price for " + productFound.getName() + " not set");
        }
        BigDecimal price = productFound.getProductPrice().getPrice();
        Customer customer = registration.getRegistrant().getCustomerAccount();
        Account salesAccount = productFound.getSalesAccount();
        if (salesAccount == null) {
            throw new CouncilException("Sales Account for " + productFound.getName() + " not set");
        }
        DebtComponent debtComponent = null;
        debtComponent = invoiceProcess.invoice(price, customer, salesAccount, productFound);
        return debtComponent;
    }

    public DebtComponent billInstitutionRegistrationProduct(Registration registration, DebtComponent debtComponent) {
        if (!this.generalParametersService.get().isBillingModule()) {
            return null;
        }
        Product productFound = productFinder.searchInstitutionRegistrationProduct(registration);
        if (productFound.getProductPrice() == null) {
            throw new CouncilException("Product price for " + productFound.getName() + "not set");
        }
        BigDecimal price = productFound.getProductPrice().getPrice();
        Customer customer = registration.getRegistrant().getCustomerAccount();
        Account salesAccount = productFound.getSalesAccount();
        debtComponent = invoiceProcess.invoice(price, customer, salesAccount, productFound);
        return debtComponent;
    }

    public DebtComponent billCGSProduct(Application application, DebtComponent debtComponent) {
        if (!this.generalParametersService.get().isBillingModule()) {
            return null;
        }
        Product productFound = productFinder.searchCGSApplicationProduct(application, registrationProcess.activeRegisterNotStudentRegister(application.getRegistrant()).getRegister());
        if (productFound.getProductPrice() == null) {
            throw new CouncilException("Product price for " + productFound.getName() + " not set");
        }
        BigDecimal price = productFound.getProductPrice().getPrice();
        Customer customer = application.getRegistrant().getCustomerAccount();
        Account salesAccount = productFound.getSalesAccount();
        debtComponent = invoiceProcess.invoice(price, customer, salesAccount, productFound);
        return debtComponent;

    }

    public DebtComponent billApplicationProduct(Application application, DebtComponent debtComponent) {
        if (!this.generalParametersService.get().isBillingModule()) {
            return null;
        }
        Product productFound = productFinder.searchApplicationProduct(application, applicationProcess.getApplicationRegister(application));
        if (productFound.getProductPrice() == null) {
            throw new CouncilException("Product price for " + productFound.getName() + " not set");
        }
        BigDecimal price = productFound.getProductPrice().getPrice();
        Customer customer = application.getRegistrant().getCustomerAccount();
        Account salesAccount = productFound.getSalesAccount();
        debtComponent = invoiceProcess.invoice(price, customer, salesAccount, productFound);
        return debtComponent;
    }

    public DebtComponent billApplicationForTransferProduct(Application application, DebtComponent debtComponent) {
        if (!this.generalParametersService.get().isBillingModule()) {
            return null;
        }
        Product productFound = productFinder.searchRegistrationTransferProduct(application);
        if (productFound.getProductPrice() == null) {
            throw new CouncilException("Product price for " + productFound.getName() + " not set");
        }
        BigDecimal price = productFound.getProductPrice().getPrice();
        Customer customer = application.getRegistrant().getCustomerAccount();
        Account salesAccount = productFound.getSalesAccount();
        debtComponent = invoiceProcess.invoice(price, customer, salesAccount, productFound);
        return debtComponent;
    }

    public DebtComponent billApplicationForChangeOfNameProduct(Application application, DebtComponent debtComponent) {
        if (!this.generalParametersService.get().isBillingModule()) {
            return null;
        }
        Product productFound = productFinder.searchChangeOfNameProduct(application, registrationProcess.activeRegisterNotStudentRegister(application.getRegistrant()).getRegister());
        if (productFound.getProductPrice() == null) {
            throw new CouncilException("Product price for " + productFound.getName() + " not set");
        }
        BigDecimal price = productFound.getProductPrice().getPrice();
        Customer customer = application.getRegistrant().getCustomerAccount();
        Account salesAccount = productFound.getSalesAccount();
        debtComponent = invoiceProcess.invoice(price, customer, salesAccount, productFound);
        return debtComponent;
    }

    public DebtComponent billApplicatonForInstitutionRegistrationProduct(Application application, DebtComponent debtComponent) {
        if (!this.generalParametersService.get().isBillingModule()) {
            return null;
        }
        Product productFound = productFinder.searchInstitutionApplicationProduct(application, generalParametersService.get().getInstitutionRegister());
        if (productFound.getProductPrice() == null) {
            throw new CouncilException("Product price for " + productFound.getName() + "not set");
        }
        BigDecimal price = productFound.getProductPrice().getPrice();
        Customer customer = application.getInstitution().getCustomerAccount();
        Account salesAccount = productFound.getSalesAccount();
        debtComponent = invoiceProcess.invoice(price, customer, salesAccount, productFound);
        return debtComponent;
    }

    public DebtComponent billQualificationProduct(Application application, DebtComponent debtComponent) {
        if (!this.generalParametersService.get().isBillingModule()) {
            return null;
        }
        Product productFound = productFinder.searchQualificationProduct(application);
        if (productFound.getProductPrice() == null) {
            throw new CouncilException("Product price for " + productFound.getName() + "not set");
        }
        BigDecimal price = productFound.getProductPrice().getPrice();
        Customer customer = application.getRegistrant().getCustomerAccount();
        Account salesAccount = productFound.getSalesAccount();
        debtComponent = invoiceProcess.invoice(price, customer, salesAccount, productFound);
        return debtComponent;
    }

    public DebtComponent billAdditionalQualificationProduct(Application application, DebtComponent debtComponent) {
        {
            if (!this.generalParametersService.get().isBillingModule()) {
                return null;
            }
            Product productFound = productFinder.searchAdditionalQualificationProduct(application);
            if (productFound.getProductPrice() == null) {
                throw new CouncilException("Product price for " + productFound.getName() + "not set");
            }
            BigDecimal price = productFound.getProductPrice().getPrice();
            Customer customer = application.getRegistrant().getCustomerAccount();
            Account salesAccount = productFound.getSalesAccount();
            debtComponent = invoiceProcess.invoice(price, customer, salesAccount, productFound);
            return debtComponent;
        }
    }


    public DebtComponent billApplicationForUnrestrictedPractiProduct(Application application, DebtComponent debtComponent) {
        if (!this.generalParametersService.get().isBillingModule()) {
            return null;
        }
        Product productFound = productFinder.searchUnrestrictedPracticingProduct(application, registrationProcess.activeRegisterNotStudentRegister(application.getRegistrant()).getRegister());
        if (productFound.getProductPrice() == null) {
            throw new CouncilException("Product price for " + productFound.getName() + " not set");
        }
        BigDecimal price = productFound.getProductPrice().getPrice();
        Customer customer = application.getRegistrant().getCustomerAccount();
        Account salesAccount = productFound.getSalesAccount();
        debtComponent = invoiceProcess.invoice(price, customer, salesAccount, productFound);
        return debtComponent;
    }

    public DebtComponent billTranscriptOfTrainingProduct(Application application, DebtComponent debtComponent) {
        if (!this.generalParametersService.get().isBillingModule()) {
            return null;
        }
        Product productFound = productFinder.searchTranscriptOfTrainingProduct(application);
        if (productFound.getProductPrice() == null) {
            throw new CouncilException("Product price for " + productFound.getName() + " not set");
        }
        BigDecimal price = productFound.getProductPrice().getPrice();
        Customer customer = application.getRegistrant().getCustomerAccount();
        Account salesAccount = productFound.getSalesAccount();
        debtComponent = invoiceProcess.invoice(price, customer, salesAccount, productFound);
        return debtComponent;
    }

    public DebtComponent billPracticingCertificateProduct(Registration registration, DebtComponent debtComponent) {
        if (!this.generalParametersService.get().isBillingModule()) {
            return null;
        }
        Product productFound = productFinder.searchPractisingCertificateProduct(registration);
        if (productFound.getProductPrice() == null) {
            throw new CouncilException("Product price for " + productFound.getName() + " not set");
        }
        BigDecimal price = productFound.getProductPrice().getPrice();
        Customer customer = registration.getRegistrant().getCustomerAccount();
        Account salesAccount = productFound.getSalesAccount();
        debtComponent = invoiceProcess.invoice(price, customer, salesAccount, productFound);
        return debtComponent;
    }

    public DebtComponent billApplicationForSupervisionProduct(Application application, DebtComponent debtComponent) {
        if (!this.generalParametersService.get().isBillingModule()) {
            return null;
        }
        Product productFound = productFinder.searchSupervisionProduct(application);
        if (productFound.getProductPrice() == null) {
            throw new CouncilException("Product price for " + productFound.getName() + " not set");
        }
        BigDecimal price = productFound.getProductPrice().getPrice();
        Customer customer = application.getRegistrant().getCustomerAccount();
        Account salesAccount = productFound.getSalesAccount();
        debtComponent = invoiceProcess.invoice(price, customer, salesAccount, productFound);
        return debtComponent;
    }

    public DebtComponent billStudentApplicationProduct(Application application, DebtComponent debtComponent) {
        if (!this.generalParametersService.get().isBillingModule()) {
            return null;
        }
        Product productFound = productFinder.searchStudentApplicationProduct(application, generalParametersService.get().getStudentRegister());
        if (productFound.getProductPrice() == null) {
            throw new CouncilException("Product price for " + productFound.getName() + " not set");
        }
        BigDecimal price = productFound.getProductPrice().getPrice();
        Customer customer = application.getRegistrant().getCustomerAccount();
        Account salesAccount = productFound.getSalesAccount();
        debtComponent = invoiceProcess.invoice(price, customer, salesAccount, productFound);
        return debtComponent;
    }

    public DebtComponent billDuplicateCertificate(ProductIssuance productIssuance) {

        DebtComponent debtComponent = null;
        ProductIssuance newProductIssuance = new ProductIssuance();
        int random = new Random().nextInt();
        random = random < 0 ? random * -1 : random;
        newProductIssuance.setBarCode(random);
        newProductIssuance.setProductNumber(String.valueOf(random));
        newProductIssuance.setSerialNumber(random);
        newProductIssuance.setDuplicate(true);
        newProductIssuance.setProductIssuance(productIssuance);
        newProductIssuance.setRegistrant(productIssuance.getRegistrant());
        newProductIssuance.setProductIssuanceType(productIssuance.getProductIssuanceType());
        newProductIssuance.setComment(productIssuance.getComment());
        newProductIssuance.setCourse(productIssuance.getCourse());
        newProductIssuance.setCreateDate(new Date());
        newProductIssuance.setExpiryDate(productIssuance.getExpiryDate());
        newProductIssuance.setExamDate(productIssuance.getExamDate());
        newProductIssuance.setDuration(productIssuance.getDuration());
        newProductIssuance.setPrefixName(productIssuance.getPrefixName());
        newProductIssuance.setRegister(productIssuance.getRegister());

        if (this.generalParametersService.get().isBillingModule()) {

            Product productFound = null;
            if (newProductIssuance.getProductIssuanceType() == ProductIssuanceType.REGISTRATION_CERTIFICATE) {
                productFound = productFinder.searchByTypeOfService(TypeOfService.DUPLICATE_REGISTRATION_CERTIFICATE);
            } else if (newProductIssuance.getProductIssuanceType() == ProductIssuanceType.PRACTICING_CERTIFICATE) {
                productFound = productFinder.searchByTypeOfService(TypeOfService.DUPLICATE_PRACTICE_CERTIFICATE);
            } else {
                productFound = productFinder.searchByTypeOfService(TypeOfService.DUPLICATE_QUALIFICATION_CERTIFICATE);
            }

            if (productFound != null && productFound.getProductPrice() == null) {
                throw new CouncilException("Product price for " + productFound.getName() + " not set");
            }
            BigDecimal price = productFound.getProductPrice().getPrice();
            Customer customer = newProductIssuance.getRegistrant().getCustomerAccount();
            Account salesAccount = productFound.getSalesAccount();
            if (salesAccount == null) {
                throw new CouncilException("Sales Account for " + productFound.getName() + " not set");
            }

            debtComponent = invoiceProcess.invoice(price, customer, salesAccount, productFound);
            newProductIssuance.setTransactionDebtComponent(debtComponent);
        }

        productIssuanceService.save(newProductIssuance);

        return debtComponent;
    }
}
