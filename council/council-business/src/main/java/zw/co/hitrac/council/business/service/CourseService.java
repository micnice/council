package zw.co.hitrac.council.business.service;

import java.util.List;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.CourseGroup;
import zw.co.hitrac.council.business.domain.CourseType;
import zw.co.hitrac.council.business.domain.ModulePaper;
import zw.co.hitrac.council.business.domain.Qualification;
import zw.co.hitrac.council.business.domain.Tier;

/**
 *
 * @author Charles Chigoriwa
 */
public interface CourseService extends IGenericService<Course> {

    public List<Course> getCourses(Boolean basic, CourseGroup courseGroup, Qualification qualification, CourseType courseType, Tier tier);

    public Course findByPrefixName(String prefixName);

    /**
     *
     * Get Number total number of Module Papers
     *
     * @param course
     * @return
     */
    public Long getNumberOfModulePapers(Course course);
}
