
package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.Province;

/**
 *
 * @author Kelvin Goredema
 */
public interface ProvinceService  extends IGenericService<Province> {
    
    
    
}
