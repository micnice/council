package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.RegistrantStatus;

/**
 *
 * @author Michael Matiashe
 */
public interface RegistrantStatusService extends IGenericService<RegistrantStatus> {
    
}
