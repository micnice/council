/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.menu;

import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import zw.co.hitrac.council.web.pages.search.SearchRegistrantAdvancedPage;
import zw.co.hitrac.council.web.pages.search.SearchPage;
import zw.co.hitrac.council.web.pages.search.SearchRegistrantPage;

/**
 *
 * @author tdhlakama
 */
public class SearchPanel extends MenuPanel {

    private Link<Void> searchLink;
    private Link<Void> searchSimpleLink;
    private Link<Void> searchAdvancedLink;
    private boolean searchSimple;
    private boolean searchAdvanced;
    private WebMarkupContainer childMenuLinks;

    public SearchPanel(String id) {
        super(id);
        add(searchLink = new BookmarkablePageLink<Void>("searchLink", SearchPage.class));
        add(childMenuLinks = new WebMarkupContainer("childMenuLinks"));
        childMenuLinks.add(searchSimpleLink = new BookmarkablePageLink<Void>("searchSimplePage", SearchRegistrantPage.class));
        childMenuLinks.add(searchAdvancedLink = new BookmarkablePageLink<Void>("searchAdvancedPage", SearchRegistrantAdvancedPage.class));
    }

    @Override
    protected void onConfigure() {
        addCurrentBehavior(searchLink, topMenuCurrent);
        childMenuLinks.setVisibilityAllowed(topMenuCurrent);
        addCurrentBehavior(searchSimpleLink, searchSimple);
        addCurrentBehavior(searchAdvancedLink, searchAdvanced);
    }

    public boolean isSearchSimple() {
        return searchSimple;
    }

    public void setSearchSimple(boolean searchSimple) {
        this.searchSimple = searchSimple;
    }

    public boolean isSearchAdvanced() {
        return searchAdvanced;
    }

    public void setSearchAdvanced(boolean searchAdvanced) {
        this.searchAdvanced = searchAdvanced;
    }
}
