
package zw.co.hitrac.council.business.dao.repo.accounts;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.accounts.PaymentDetails;

/**
 *
 * @author Michael Matiashe
 */
public interface PaymentDetailsRepository extends CrudRepository<PaymentDetails, Long> {

    public List<PaymentDetails> findAll();
    public List<PaymentDetails> findByUid(String uid);
}
