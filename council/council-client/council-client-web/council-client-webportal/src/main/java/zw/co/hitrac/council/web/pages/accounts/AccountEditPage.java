package zw.co.hitrac.council.web.pages.accounts;

import java.util.Arrays;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.AccountType;
import zw.co.hitrac.council.business.service.accounts.AccountService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Tatenda Chiwandire
 */
public class AccountEditPage extends IGeneralLedgerPage {

    @SpringBean
    private AccountService accountService;

    public AccountEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<Account>(new LoadableDetachableAccountModel(id)));
        Form<Account> form = new Form<Account>("form", (IModel<Account>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                accountService.save(getModelObject());
                setResponsePage(new AccountViewPage(getModelObject().getId()));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("code").add(new ErrorBehavior()));
        form.add(new DropDownChoice("accountType", Arrays.asList(AccountType.values())));
        form.add(new BookmarkablePageLink<Void>("returnLink", AccountListPage.class));
        add(form);
    }

    private final class LoadableDetachableAccountModel extends LoadableDetachableModel<Account> {

        private Long id;

        public LoadableDetachableAccountModel(Long id) {
            this.id = id;
        }

        @Override
        protected Account load() {
            if (id == null) {
                return new Account();
            }
            return accountService.get(id);
        }
    }
}
