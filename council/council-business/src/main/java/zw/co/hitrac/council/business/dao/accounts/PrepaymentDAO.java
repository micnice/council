package zw.co.hitrac.council.business.dao.accounts;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.repository.query.Param;
import zw.co.hitrac.council.business.dao.IGenericDAO;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.accounts.Prepayment;

/**
 *
 * @author Michael Matiashe
 */
public interface PrepaymentDAO extends IGenericDAO<Prepayment> {

    public List<Prepayment> findByInstitution(Institution institution);

    public List<Prepayment> findAvailablePrepayments();

    public List<Prepayment> findAll(Date endDate);

    public BigDecimal getPrepaymentBalance(Date endDate, Prepayment prepayment);

    public List<Institution> withPrepayments();

    public List<Prepayment> findByCanceled(Boolean canceled);
}
