package zw.co.hitrac.council.business.domain.reports;

import org.apache.commons.lang.StringUtils;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.utils.Util;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

/**
 * @author tdhlakama
 */
@Entity
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement
public class RegistrantData implements Serializable {

    private Long id;
    private String titleName;
    private String firstname;
    private String lastname;
    private String middlename;
    private String maidenname;
    private Date birthDate;
    private String gender;
    private String idNumber;
    private String registrationNumber;
    private String courseName;
    private String registerName;
    private Date lastPaymentDate;
    private String qualificationName;
    private String institutionName;
    private String businessAddress;
    private String residentialAddress;
    private String postalAddress;
    private String emailAddress;
    private String mobileNumber;
    private String detail;
    private int internationalEmploymentTotal;
    private int notEmploymentTotal;
    private String conditionName;
    private String nameOfInstitutionOwnedByRegistrant;
    private int conditionTotal;
    private boolean status;
    private String provinceName;
    private String districtName;
    private String cityName;
    private String business;
    private String mobile;
    private String email;
    private Date dateSaved;
    private String employmentTypeName;
    private String employerName;
    private Date registrationDate;
    private String address;
    private int privateEmploymentTotal;
    private int publicEmploymentTotal;
    private BigDecimal points;

    public RegistrantData() {

    }

    public RegistrantData(Long id) {

        this.id = id;
    }

    public RegistrantData(Registrant registrant) {

    }

    public RegistrantData(Long id, BigDecimal points) {
        this.id = id;
        this.points = points;
    }

    public BigDecimal getPoints() {
        return points;
    }

    public void setPoints(BigDecimal points) {
        this.points = points;
    }

    @Transient
    public String getNameOfInstitutionOwnedByRegistrant() {

        return nameOfInstitutionOwnedByRegistrant;
    }

    public void setNameOfInstitutionOwnedByRegistrant(String nameOfInstitutionOwnedByRegistrant) {

        this.nameOfInstitutionOwnedByRegistrant = nameOfInstitutionOwnedByRegistrant;
    }

    public RegistrantData(Long id, String firstname, String lastname, String middlename, String emailAddress, Date birthDate) {

        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.middlename = middlename;
        this.emailAddress = emailAddress;
        this.email = emailAddress;
        this.birthDate = birthDate;
    }

    public RegistrantData(String firstname, String lastname, String middlename, String nameOfInstitutionOwnedByRegistrant) {

        this.firstname = firstname;
        this.lastname = lastname;
        this.middlename = middlename;
        this.nameOfInstitutionOwnedByRegistrant = nameOfInstitutionOwnedByRegistrant;
    }

    public RegistrantData(Long id, String firstname, String lastname, String middlename, Date birthDate, String gender,
                          String idNumber, String registrationNumber) {

        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.middlename = middlename;
        this.birthDate = birthDate;
        this.gender = gender;
        this.idNumber = idNumber;
        this.registrationNumber = registrationNumber;
    }

    public RegistrantData(Long id, String firstname, String lastname, String middlename, String gender,
                          String idNumber, String registrationNumber, String conditionName) {

        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.middlename = middlename;
        this.gender = gender;
        this.idNumber = idNumber;
        this.registrationNumber = registrationNumber;
        this.conditionName = conditionName;
    }

    public RegistrantData(Long id, String firstname, String lastname, String middlename, Date birthDate, String gender,
                          String idNumber, String registrationNumber, String courseName, String institutionName) {

        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.middlename = middlename;
        this.birthDate = birthDate;
        this.gender = gender;
        this.idNumber = idNumber;
        this.registrationNumber = registrationNumber;
        this.courseName = courseName;
        this.institutionName = institutionName;
    }

    public RegistrantData(Long id, String firstname, String lastname, String middlename, Date birthDate,
                          String gender, String idNumber, String registrationNumber, String detail) {

        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.middlename = middlename;
        this.birthDate = birthDate;
        this.gender = gender;
        this.idNumber = idNumber;
        this.registrationNumber = registrationNumber;
        this.detail = detail;
    }

    @Transient
    public static String[] getRegistrantMappedDataHeader() {

        return new String[]{"Full Name", "Address", "Business Address", "Postal Address", "Residential Address",
                "Email Address", "Gender", "ID Number", "Mobile Number", "Business Number"};
    }

    @Transient
    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    @Transient
    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    @Transient
    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    @Transient
    public String getConditionName() {

        return conditionName;
    }

    public void setConditionName(String conditionName) {

        this.conditionName = conditionName;
    }

    @Transient
    public int getConditionTotal() {

        return conditionTotal;
    }

    public void setConditionTotal(int conditionTotal) {

        this.conditionTotal = conditionTotal;
    }

    @Transient
    public String getEmployerName() {

        return employerName;
    }

    public void setEmployerName(String employerName) {

        this.employerName = employerName;
    }

    @Transient
    public int getPrivateEmploymentTotal() {

        return privateEmploymentTotal;
    }

    public void setPrivateEmploymentTotal(Integer privateEmploymentTotal) {

        this.privateEmploymentTotal = privateEmploymentTotal;
    }

    @Transient
    public int getPublicEmploymentTotal() {

        return publicEmploymentTotal;
    }

    public void setPublicEmploymentTotal(Integer publicEmploymentTotal) {

        this.publicEmploymentTotal = publicEmploymentTotal;
    }

    @Transient
    public int getInternationalEmploymentTotal() {

        return internationalEmploymentTotal;
    }

    public void setInternationalEmploymentTotal(Integer internationalEmploymentTotal) {

        this.internationalEmploymentTotal = internationalEmploymentTotal;
    }

    @Transient
    public int getNotEmploymentTotal() {
        return notEmploymentTotal;
    }

    public void setNotEmploymentTotal(int notEmploymentTotal) {
        this.notEmploymentTotal = notEmploymentTotal;
    }

    @Id
    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    @Transient
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getRegistrationDate() {

        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {

        this.registrationDate = registrationDate;
    }

    @Transient
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getBirthDate() {

        return birthDate;
    }

    public void setBirthDate(Date birthDate) {

        this.birthDate = birthDate;
    }

    public String getRegistrationNumber() {

        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {

        this.registrationNumber = registrationNumber;
    }

    @Transient
    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getLastPaymentDate() {

        return lastPaymentDate;
    }

    public void setLastPaymentDate(Date lastPaymentDate) {

        this.lastPaymentDate = lastPaymentDate;
    }

    @Transient
    public String getCourseName() {

        return courseName;
    }

    public void setCourseName(String courseName) {

        this.courseName = courseName;
    }

    @Transient
    public String getRegisterName() {

        return registerName;
    }

    public void setRegisterName(String registerName) {

        this.registerName = registerName;
    }

    @Transient
    public String getQualificationName() {

        return qualificationName;
    }

    public void setQualificationName(String qualificationName) {

        this.qualificationName = qualificationName;
    }

    @Transient
    public String getInstitutionName() {

        return institutionName;
    }

    public void setInstitutionName(String institutionName) {

        this.institutionName = institutionName;
    }

    @Transient
    public String getDetail() {
        return StringUtils.defaultString(detail);
    }

    public void setDetail(String detail) {

        this.detail = detail;
    }

    @Transient
    public int getIntegerValueOfAge() {
        return Util.getIntegerValueOfAge(birthDate);
    }

    @Override
    public String toString() {

        return "RegistrantData{" +
                "courseName='" + courseName + '\'' +
                ", privateEmploymentTotal=" + privateEmploymentTotal +
                ", publicEmploymentTotal=" + publicEmploymentTotal +
                ", internationalEmploymentTotal=" + internationalEmploymentTotal +
                '}';
    }

    @Transient
    public Date getDateSaved() {

        return dateSaved;
    }

    public void setDateSaved(Date dateSaved) {

        this.dateSaved = dateSaved;
    }

    @Transient
    public String getEmploymentTypeName() {

        return employmentTypeName;
    }

    public void setEmploymentTypeName(String employmentTypeName) {

        this.employmentTypeName = employmentTypeName;
    }

    @Transient
    public String getFullName() {

        return getLastname() + " " + getFirstname() + " " + getMiddlename() + " " + getMaidenname();
    }

    public String getFirstname() {
        return StringUtils.defaultString(firstname);
    }

    public void setFirstname(String firstname) {

        this.firstname = firstname;
    }

    public String getLastname() {
        return StringUtils.defaultString(lastname);
    }

    public void setLastname(String lastname) {

        this.lastname = lastname;
    }

    public String getMiddlename() {
        return StringUtils.defaultString(middlename);
    }

    public void setMiddlename(String middlename) {

        this.middlename = middlename;
    }

    @Transient
    public String getMaidenname() {
        return StringUtils.defaultString(maidenname);
    }

    public void setMaidenname(String maidenname) {

        this.maidenname = maidenname;
    }

    @Transient
    public String getAddressDetail() {

        return getBusinessAddress() + " - " + getPostalAddress() + " - " + getResidentialAddress();
    }

    @Transient
    public String getBusinessAddress() {
        return StringUtils.defaultString(businessAddress);
    }

    public void setBusinessAddress(String businessAddress) {

        this.businessAddress = businessAddress;
    }

    @Transient
    public String getResidentialAddress() {
        return StringUtils.defaultString(residentialAddress);
    }

    public void setResidentialAddress(String residentialAddress) {

        this.residentialAddress = residentialAddress;
    }

    @Transient
    public String getPostalAddress() {
        return StringUtils.defaultString(postalAddress);
    }

    public void setPostalAddress(String postalAddress) {

        this.postalAddress = postalAddress;
    }

    @Transient
    public String getContactDetail() {

        return getEmail() + " - " + getMobile() + " - " + getBusiness();
    }

    @Transient
    public String getBusiness() {
        return StringUtils.defaultString(business);
    }

    public void setBusiness(String business) {

        this.business = business;
    }

    @Transient
    public String getMobile() {
        return StringUtils.defaultString(mobile);
    }

    public void setMobile(String mobile) {

        this.mobile = mobile;
    }

    @Transient
    public String getEmail() {
        return StringUtils.defaultString(email);
    }

    public void setEmail(String email) {

        this.email = email;
    }

    @Transient
    public String[] getRegistrantMappedData() {

        return new String[]{getFullname(), getAddress(), getBusinessAddress(), getPostalAddress(),
                getResidentialAddress(), getEmailAddress(), getGender(), getIdNumber(), getMobileNumber(), getBusiness()};
    }

    @Transient
    public String getGender() {

        return gender;
    }

    public void setGender(String gender) {

        this.gender = gender;
    }

    public String getIdNumber() {

        return idNumber;
    }

    public void setIdNumber(String idNumber) {

        this.idNumber = idNumber;
    }

    @Transient
    public String getFullname() {

        return getLastname() + " " + getFirstname() + " " + getMiddlename();
    }

    @Transient
    public String getEmailAddress() {
        return StringUtils.defaultString(emailAddress);
    }

    public void setEmailAddress(String emailAddress) {

        this.emailAddress = emailAddress;
    }

    @Transient
    public String getMobileNumber() {
        return StringUtils.defaultString(mobileNumber);
    }

    public void setMobileNumber(String mobileNumber) {

        this.mobileNumber = mobileNumber;
    }

    @Transient
    public String getAddress() {

        if (residentialAddress != null) {
            return residentialAddress;
        } else if (businessAddress != null) {
            return businessAddress;
        } else if (postalAddress != null) {
            return postalAddress;
        } else {
            return "";
        }

    }

    @Transient
    public String getTitleName() {
        return titleName;
    }

    public void setTitleName(String titleName) {
        this.titleName = titleName;
    }

    @Transient

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public int hashCode() {

        int hash = 5;
        hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RegistrantData other = (RegistrantData) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
}
