package zw.co.hitrac.council.web.pages.certification;

import java.util.ArrayList;
import java.util.List;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.ProductIssuance;
import zw.co.hitrac.council.business.service.ProductIssuanceService;
import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author tdhlakama
 */
public class AdminSearchCertificatePage extends TemplatePage {

    private String productNumber;
    private String searchText;
    @SpringBean
    private ProductIssuanceService productIssuanceService;

    public AdminSearchCertificatePage() {
        menuPanelManager.getViewReportsPanel().setTopMenuCurrent(true);
        PropertyModel<String> productNumberModel = new PropertyModel<String>(this, "productNumber");
        PropertyModel<String> searchTextModel = new PropertyModel<String>(this, "searchText");

        Form<?> form = new Form("form");
        form.add(new TextField<String>("searchText", searchTextModel));
        form.add(new TextField<String>("productNumber", productNumberModel));
        add(form);
        IModel<List<ProductIssuance>> model = new LoadableDetachableModel<List<ProductIssuance>>() {
            @Override
            protected List<ProductIssuance> load() {
                if (productNumber == null && searchText == null) {
                    return new ArrayList<ProductIssuance>();
                } else {
                    return productIssuanceService.getProductIssuanceList(searchText, productNumber);
                }
            }
        };

        add(new AdminCertificateDataListPanel("certificateDataListPanel", model));

    }
}
