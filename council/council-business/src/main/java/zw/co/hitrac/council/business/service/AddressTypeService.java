package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.AddressType;

/**
 *
 * @author Charles Chigoriwa
 */
public interface AddressTypeService extends IGenericService<AddressType> {
	public AddressType findByName(String name);
}
