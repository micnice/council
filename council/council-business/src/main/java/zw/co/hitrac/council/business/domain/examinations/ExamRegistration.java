/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain.examinations;

import org.apache.commons.lang.BooleanUtils;
import org.hibernate.annotations.Formula;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.reports.MarkComparator;
import zw.co.hitrac.council.business.domain.reports.NameComparator;
import zw.co.hitrac.council.business.utils.HrisComparator;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author tidza
 */
@Entity
@Table(name = "examregistration")
@Audited
public class ExamRegistration extends BaseIdEntity implements Serializable {

    public static String ATTENDANCEFAILURE = "ATTENDANCE FAILURE";
    public static String SUPPLEMENT = "SUPPLEMENT";
    public static String SUPPRESSED = "SUPPRESSED";
    public static String REVERSEDRESULT = "REVERSED RESULT";
    private String candidateNumber;
    private String passComment;
    private Registration registration;
    private ExamSetting examSetting;
    private Institution institution;
    private Set<ExamResult> examResults = new HashSet<ExamResult>();
    private Boolean suppressed = Boolean.FALSE;
    private String comment;
    private Boolean supplementaryExam = Boolean.FALSE;
    private Boolean closed = Boolean.FALSE;
    private Set<ExamRegistrationDebtComponent> examRegistrationDebtComponents = new HashSet<ExamRegistrationDebtComponent>();
    private Set<SupportDocument> supportDocuments = new HashSet<SupportDocument>();
    private final HrisComparator hrisComparator = new HrisComparator();
    private Integer institutionPosition = 0;
    private Integer CountryPosition = 0;
    private Boolean reversedResult = Boolean.FALSE; //Result will not be considered as a sit
    private BigDecimal remainingBalance = BigDecimal.ZERO;
    private Boolean disqualified = Boolean.FALSE;

    public Boolean getReversedResult() {
        return BooleanUtils.toBoolean(reversedResult);
    }

    public void setReversedResult(Boolean reversedResult) {
        this.reversedResult = reversedResult;
    }

    @Column(unique = true, nullable = false)
    public String getCandidateNumber() {
        return candidateNumber;
    }

    public void setCandidateNumber(String candidateNumber) {
        this.candidateNumber = candidateNumber;
    }

    @OneToMany(mappedBy = "examRegistration", cascade = CascadeType.ALL)
    public Set<ExamResult> getExamResults() {
        return examResults;
    }

    public void setExamResults(Set<ExamResult> examResults) {
        this.examResults = examResults;
    }

    @ManyToOne
    public Registration getRegistration() {
        return registration;
    }

    public void setRegistration(Registration registration) {
        this.registration = registration;
    }

    @ManyToOne
    public ExamSetting getExamSetting() {
        return examSetting;
    }

    public void setExamSetting(ExamSetting examSetting) {
        this.examSetting = examSetting;
    }

    @ManyToOne
    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    public String getComment() {
        if (comment != null) {
            return comment.toUpperCase();
        }
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Boolean getSupplementaryExam() {
        return BooleanUtils.toBoolean(supplementaryExam);
    }

    public void setSupplementaryExam(Boolean supplementaryExam) {
        this.supplementaryExam = supplementaryExam;
    }

    @Transient
    public String getSupplementaryExamStatus() {
        if (supplementaryExam != null) {
            return supplementaryExam ? "Supplementry" : "";
        } else {
            return "";
        }
    }

    @Transient
    public List<ExamResult> getExamResultsList() {
        List<ExamResult> items = new ArrayList<ExamResult>();
        getExamResults().size();
        if (examResults != null && !examResults.isEmpty()) {
            items.addAll(getExamResults());
        }
        List<ExamResult> itemLists = new ArrayList<ExamResult>();
        if (!items.isEmpty()) {
            itemLists.addAll((List<ExamResult>) hrisComparator.sortExaminationCards(items));
        }
        return itemLists;
    }

    @Transient
    public List<Exam> getExamList() {
        List<Exam> items = new ArrayList<Exam>();
        getExamSetting().getExams().size();
        if (getExamSetting().getExams() != null && !getExamSetting().getExams().isEmpty()) {
            items.addAll(getExamSetting().getExams());
        }
        return items;
    }

    @Transient
    public List<Exam> getExamFailedList() {
        Set<Exam> items = new HashSet<Exam>();
        for (ExamResult examResult : getExamResultsList()) {
            if (!examResult.getExamAttendance() && !examResult.getDisqualified()) {
                items.add(examResult.getExam());
            }
            if (!examResult.getExamStatus() && examResult.getExamAttendance() && !examResult.getDisqualified()) {
                items.add(examResult.getExam());
            }
        }
        return new ArrayList<Exam>(items);
    }

    @Transient
    public List<Exam> getDisqualifiedExamList() {
        Set<Exam> items = new HashSet<Exam>();
        for (ExamResult examResult : getExamResultsList()) {
            if (examResult.getDisqualified()) {
                items.add(examResult.getExam());
            }
        }
        return new ArrayList<Exam>(items);
    }

    @Transient
    public List<Exam> getSupplementaryList() {
        Set<Exam> items = new HashSet<Exam>();
        for (ExamResult examResult : getExamResultsList()) {
            if (!examResult.getSupplementaryExam()) { //show list of items not passed for supplementary
                items.add(examResult.getExam());
            }
        }
        return new ArrayList<Exam>(items);
    }

    @Transient
    public List<Exam> getPassedExams() {
        Set<Exam> items = new HashSet<Exam>();
        for (ExamResult examResult : getExamResultsList()) {
            if (examResult.getExamStatus()) {
                items.add(examResult.getExam());
            }
        }
        return new ArrayList<Exam>(items);
    }

    @Transient
    public List<ExamResult> getExamPassList() {
        Set<ExamResult> items = new HashSet<ExamResult>();
        for (ExamResult examResult : getExamResultsList()) {
            if (examResult.getExamStatus() && !examResult.getDisqualified()) {
                items.add(examResult);
            }
        }
        return new ArrayList<ExamResult>(items);
    }

    @Transient
    public List<ExamResult> getDisqualifiedResultList() {
        Set<ExamResult> items = new HashSet<ExamResult>();
        for (ExamResult examResult : getExamResultsList()) {
            if (examResult.getDisqualified()) {
                items.add(examResult);
            }
        }
        return new ArrayList<ExamResult>(items);
    }

    @Transient
    public Boolean getCompletedStatus() {
        for (ExamResult examResult : getExamResults()) {
            if (examResult.getStatus().equals(ExamResult.RESULTPENDING)) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

    @Transient
    public String getPassTextStatus() {
        if (getReversedResult()) {
            return REVERSEDRESULT;
        }
        if (getDisqualified()) {
            return ExamResult.DISQUALIFIED;
        }
        if (getSuppressed()) {
            return SUPPRESSED;
        }
        if (getCompletedStatus()) {
            return getPassStatus() ? ExamResult.PASS : ExamResult.FAIL;
        } else {
            return ExamResult.RESULTPENDING;
        }
    }

    public Boolean getSuppressed() {
        return BooleanUtils.toBoolean(suppressed);
    }

    public void setSuppressed(Boolean suppressed) {
        this.suppressed = suppressed;
    }

    @Transient
    public Boolean getPassStatus() {
        for (ExamResult examResult : getExamResults()) {
            if (!examResult.getExamStatus() || !examResult.getExamAttendance()) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

    @Transient
    public Boolean getAtLeastOneMarkStatus() {
        for (ExamResult examResult : getExamResults()) {
            if (examResult.getMark() != null && examResult.getExamAttendance()) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    @Transient
    public String getFailedPapers() {
        String c = "";
        for (Exam exam : getExamFailedList()) {
            c = c.concat(exam.getModulePaper().getName()) + ", ";
        }
        if (c == null || c.length() == 0) {
            c = ", ";
        }
        c = c.substring(0, c.length() - 2);
        return c;
    }

    @Transient
    public String getRegisteredSupplementaryPapers() {
        String c = "";
        for (Exam exam : getSupplementaryList()) {
            c = c.concat(exam.getModulePaper().getName()) + ", ";
        }
        if (c == null || c.length() == 0) {
            c = ", ";
        }
        c = c.substring(0, c.length() - 2);
        return c;
    }

    @Transient
    public Double getFinalMark() {
        if (getReversedResult() || getSuppressed()) {
            return 0.0;
        }
        Double total = 0.0;
        for (ExamResult examResult : getExamResults()) {
            if (examResult.getMark() != null && !examResult.getDisqualified()) {
                total += examResult.getMark();
            }
        }
        return total;
    }

    @Override
    public String toString() {
        return examSetting.toString();
    }

    public Boolean getClosed() {
        return BooleanUtils.toBoolean(closed);
    }

    public void setClosed(Boolean closed) {
        this.closed = closed;
    }

    @Transient
    public BigDecimal getAmountPaid() {
        BigDecimal totalPoints = new BigDecimal(0);
        for (ExamRegistrationDebtComponent examRegistrationDebtComponent : getExamRegistrationDebtComponents()) {
            if (examRegistrationDebtComponent != null && examRegistrationDebtComponent.getDebtComponent() != null) {
                totalPoints = totalPoints.add(examRegistrationDebtComponent.getDebtComponent().getTransactionComponent().getAmount());
            }
        }

        return totalPoints;
    }

    @OneToMany(mappedBy = "examRegistration")
    public Set<ExamRegistrationDebtComponent> getExamRegistrationDebtComponents() {
        return examRegistrationDebtComponents;
    }

    public void setExamRegistrationDebtComponents(Set<ExamRegistrationDebtComponent> examRegistrationDebtComponents) {
        this.examRegistrationDebtComponents = examRegistrationDebtComponents;
    }

    @OneToMany(mappedBy = "examRegistration")
    public Set<SupportDocument> getSupportDocuments() {
        return supportDocuments;
    }

    public void setSupportDocuments(Set<SupportDocument> supportDocuments) {
        this.supportDocuments = supportDocuments;
    }

    @Transient
    public String getExamDate() {
        //method will not work for supplementary exams 
        String statement = "";
        for (ExamResult e : getExamResultsList()) {
            statement = statement.concat(e.getExam().getExamDate().getDate() + " / ");
        }
        if (statement == null || statement.length() == 0) {
            statement = " / ";
        }
        statement = statement.substring(0, statement.length() - 3);
        return statement + examSetting.toString();
    }

    public String getPassComment() {
        if (passComment != null) {
            return passComment.toUpperCase();
        }
        if (passComment == null) {
            passComment = "";
        }
        return passComment;
    }

    public void setPassComment(String passComment) {
        this.passComment = passComment;
    }

    @Transient
    public Integer getInstitutionPosition() {
        return institutionPosition;
    }

    public void setInstitutionPosition(Integer institutionPosition) {
        this.institutionPosition = institutionPosition;
    }

    @Transient
    public Integer getCountryPosition() {
        return CountryPosition;
    }

    public void setCountryPosition(Integer CountryPosition) {
        this.CountryPosition = CountryPosition;
    }

    public static List<ExamRegistration> sortInstitutionExamRegistrations(List<ExamRegistration> examRegistrations) {
        List<ExamRegistration> finalExamRegistrations = new ArrayList<ExamRegistration>();
        Collections.sort(examRegistrations, new MarkComparator());
        NameComparator nameComparator = new NameComparator();
        //Determine position
        Double lastMark = null;
        Double currentMark;
        int currentPosition = 0;
        for (ExamRegistration examRegistration : examRegistrations) {
            if (!examRegistration.getDisqualified() && !examRegistration.getReversedResult() && !examRegistration.getSupplementaryExam()) {
                currentMark = examRegistration.getFinalMark();

                if (lastMark == null || currentMark < lastMark) {
                    List<ExamRegistration> group = getExamRegistrationsWithSameMark(currentMark, examRegistrations);
                    currentPosition = finalExamRegistrations.size() + 1;
                    if (group.size() == 1) {
                        examRegistration.setInstitutionPosition(currentPosition);
                        finalExamRegistrations.add(examRegistration);
                    } else {
                        Collections.sort(group, nameComparator);
                        for (ExamRegistration groupExamRegistration : group) {
                            groupExamRegistration.setInstitutionPosition(currentPosition);
                            finalExamRegistrations.add(groupExamRegistration);
                        }
                    }

                    lastMark = currentMark;
                }
            }
        }

        return finalExamRegistrations;
    }

    public static List<ExamRegistration> sortCountryExamRegistrations(List<ExamRegistration> examRegistrations) {
        List<ExamRegistration> finalExamRegistrations = new ArrayList<ExamRegistration>();
        Collections.sort(examRegistrations, new MarkComparator());
        NameComparator nameComparator = new NameComparator();
        //Determine position
        Double lastMark = null;
        Double currentMark;
        int currentPosition = 0;
        for (ExamRegistration examRegistration : examRegistrations) {
            if (!examRegistration.getDisqualified() && !examRegistration.getReversedResult() && !examRegistration.getSupplementaryExam()) {
                currentMark = examRegistration.getFinalMark();

                if (lastMark == null || currentMark < lastMark) {
                    List<ExamRegistration> group = getExamRegistrationsWithSameMark(currentMark, examRegistrations);
                    currentPosition = finalExamRegistrations.size() + 1;
                    if (group.size() == 1) {
                        examRegistration.setCountryPosition(currentPosition);
                        finalExamRegistrations.add(examRegistration);
                    } else {
                        Collections.sort(group, nameComparator);
                        for (ExamRegistration groupExamRegistration : group) {
                            groupExamRegistration.setCountryPosition(currentPosition);
                            finalExamRegistrations.add(groupExamRegistration);
                        }
                    }

                    lastMark = currentMark;
                }
            }
        }

        return finalExamRegistrations;
    }

    public static List<ExamRegistration> getExamRegistrationsWithSameMark(double mark, List<ExamRegistration> examRegistrations) {

        List<ExamRegistration> group = new ArrayList<ExamRegistration>();
        for (ExamRegistration examRegistration : examRegistrations) {
            if (!examRegistration.getDisqualified() && !examRegistration.getReversedResult() && !examRegistration.getSupplementaryExam()) {
                if (examRegistration.getFinalMark() == mark) {
                    group.add(examRegistration);
                } else if (examRegistration.getFinalMark() < mark) {
                    return group;
                }
            }
        }

        return group;
    }

    @Transient //Paper One for Exam
    public ExamResult getPaperOne() {
        for (ExamResult examResult : getExamResults()) {
            if (examResult.getExam().getModulePaper().getPosition() != null && examResult.getExam().getModulePaper().getPosition().equals(1l)) {
                return examResult;
            }
        }
        return null;
    }

    @Transient //Paper Two for Exam
    public ExamResult getPaperTwo() {
        for (ExamResult examResult : getExamResults()) {
            if (examResult.getExam().getModulePaper().getPosition() != null && examResult.getExam().getModulePaper().getPosition().equals(2l)) {
                return examResult;
            }
        }
        return null;
    }

    @Transient //Paper Three for Exam
    public ExamResult getPaperThree() {
        for (ExamResult examResult : getExamResults()) {
            if (examResult.getExam().getModulePaper().getPosition() != null && examResult.getExam().getModulePaper().getPosition().equals(3l)) {
                return examResult;
            }
        }
        return null;
    }

    @Transient //Paper Three for Exam
    public Boolean getHasPaperThree() {
        return getPaperThree() != null ? Boolean.TRUE : Boolean.FALSE;
    }

    @Transient //Writing Paper One
    public Boolean getWritingOne() {
        final ExamResult paper = getPaperOne();
        if (!paper.getDisqualified() && paper.getPreviousExamRegistration() != null && !paper.getPreviousExamRegistration().getDisqualified() && !paper.getPreviousExamRegistration().getPaperOne().getExamStatus()) {
            return Boolean.TRUE;
        }

        if (!paper.getDisqualified() && paper.getPreviousExamRegistration() == null) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    @Transient //Writing Paper Two
    public Boolean getWritingPaperTwo() {
        final ExamResult paper = getPaperTwo();
        if (!paper.getDisqualified() && paper.getPreviousExamRegistration() != null && !paper.getPreviousExamRegistration().getDisqualified() && !paper.getPreviousExamRegistration().getPaperTwo().getExamStatus()) {
            return Boolean.TRUE;
        }

        if (!paper.getDisqualified() && paper.getPreviousExamRegistration() == null) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    @Transient //Writing Paper Three
    public Boolean getWritingPaperThree() {
        if (getHasPaperThree()) {
            final ExamResult paper = getPaperThree();
            if (!paper.getDisqualified() && paper.getPreviousExamRegistration() != null && !paper.getPreviousExamRegistration().getDisqualified() && !paper.getPreviousExamRegistration().getPaperThree().getExamStatus()) {
                return Boolean.TRUE;
            }

            if (!paper.getDisqualified() && paper.getPreviousExamRegistration() == null) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    @Formula(value = "(select sum(d.remainingBalance) from examRegistrationdebtcomponent e inner join debtcomponent d on d.id = e.debtComponent_id where e.examRegistration_id = id)")
    @NotAudited //required for @Formula
    public BigDecimal getRemainingBalance() {
        return remainingBalance;
    }

    public void setRemainingBalance(BigDecimal remainingBalance) {
        this.remainingBalance = remainingBalance;
    }

    @Transient
    public Boolean getShow() {
        if (!getReversedResult() || getRemainingBalance() == null) {
            return Boolean.TRUE;
        }
        if (!getReversedResult() || getRemainingBalance().equals(BigDecimal.ZERO)) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    @Transient
    public String getFullyPaidStatus() {
        if (getShow()) {
            return "Paid UP";
        }
        return "In Complete Payment";
    }

    @Transient
    public Boolean getDisqualified() {
        for (ExamResult examResult : getExamResults()) {
            if (examResult.getDisqualified() && !this.getReversedResult()) {
                return true;
            }
        }
        return false;
    }

    public void setDisqualified(Boolean disqualified) {
        this.disqualified = disqualified;
    }

    @Transient
    public Boolean getFirstTimeFailedBothPapers() {
        if (getFirstTimeFailedPaperOne() && getFirstTimeFailedPaperTwo()) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    @Transient
    public Boolean getSecondTimeFailedBothPapers() {
        if (getSecondTimeFaildPaperOne() && getSecondTimeFaildPaperTwo()) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    @Transient
    public Boolean getFirstTimeFailedPaperOne() {
        if (!getSupplementaryExam() && !getPassStatus()) {
            if ((!getPaperOne().getExamStatus() && !getPaperOne().getDisqualified())) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    @Transient
    public Boolean getSecondTimeFaildPaperOne() {
        if (getSupplementaryExam() && !getPassStatus()) {
            if ((!getPaperOne().getExamStatus() && !getPaperOne().getDisqualified())) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    @Transient
    public Boolean getFirstTimeFailedPaperTwo() {
        if (!getSupplementaryExam() && !getPassStatus()) {
            if ((!getPaperTwo().getExamStatus() && !getPaperTwo().getDisqualified())) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    @Transient
    public Boolean getSecondTimeFaildPaperTwo() {
        if (getSupplementaryExam() && !getPassStatus()) {
            if ((!getPaperTwo().getExamStatus() && !getPaperTwo().getDisqualified())) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    @Transient
    public Boolean getFirstTimeFailedPaperThree() {
        if (!getSupplementaryExam() && !getPassStatus()) {
            if ((!getPaperThree().getExamStatus() && !getPaperThree().getDisqualified())) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    @Transient
    public Boolean getSecondTimeFaildPaperThree() {
        if (getSupplementaryExam() && !getPassStatus()) {
            if ((!getPaperThree().getExamStatus() && !getPaperThree().getDisqualified())) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

}
