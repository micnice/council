package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Michael Matiashe
 */
@XmlAccessorType(XmlAccessType.NONE)
@Entity
@Table(name = "councilDuration")
@Audited
@XmlRootElement
public class CouncilDuration extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    private Boolean active;
    private Set<Duration> durations = new HashSet<Duration>();

    public Boolean getActive() {

        return active;
    }

    public void setActive(Boolean active) {

        this.active = active;
    }

    @Transient
    public String getTextStatus() {

        if (active == null) {
            return "InActive";
        }
        return active ? "Active" : "InActive";
    }

    @OneToMany(mappedBy = "councilDuration", cascade = CascadeType.ALL)
    public Set<Duration> getDurations() {

        return durations;
    }

    public void setDurations(Set<Duration> durations) {

        this.durations = durations;
    }
}
