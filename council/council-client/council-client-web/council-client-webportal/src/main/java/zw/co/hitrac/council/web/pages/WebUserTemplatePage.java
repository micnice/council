package zw.co.hitrac.council.web.pages;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.authroles.authentication.AuthenticatedWebApplication;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.GeneralParameters;
import zw.co.hitrac.council.business.service.ApplicationService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrantConditionService;
import zw.co.hitrac.council.business.utils.StringFormattingUtil;
import zw.co.hitrac.council.web.config.CouncilApplication;
import zw.co.hitrac.council.web.menu.MenuPanelManager;
import zw.co.hitrac.council.web.models.VersionReadOnlyModel;
import zw.co.hitrac.council.web.pages.application.FiltedApplicationPendingPage;
import zw.co.hitrac.council.web.pages.registration.ConditionsPage;
import zw.co.hitrac.council.web.pages.research.MyDynamicImageResource;
import zw.co.hitrac.council.web.pages.security.SignOutPage;

/**
 * Template Page for all the system pages.
 *
 * @author Charles Chigoriwa
 */
public abstract class WebUserTemplatePage extends WebPage {

    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private ApplicationService applicationService;
    @SpringBean
    private RegistrantConditionService registrantConditionService;
    private String councilName;

    public String getCouncilName() {

        return councilName;
    }

    public WebUserTemplatePage() {

        GeneralParameters generalParameters = generalParametersService.get();
        this.councilName = generalParameters.getCouncilName();
        this.councilName = StringUtils.isEmpty(councilName) ? "Council" : councilName;

        add(new Label("displayCouncilName", this.councilName));
        add(new Image("siteLogoImage", new MyDynamicImageResource(generalParametersService)));
        add(new BookmarkablePageLink<Void>("syslogout", SignOutPage.class));

    }
}


//~ Formatted by Jindent --- http://www.jindent.com
