package zw.co.hitrac.council.business.domain;

import org.apache.commons.lang.BooleanUtils;
import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;

import javax.persistence.*;
import java.util.Date;

/**
 *
 * @author Charles Chigoriwa
 * @author Matiashe Michael
 */
@Entity
@Table(name = "productissuance")
@Audited
public class ProductIssuance extends BaseIdEntity {

    private Date createDate;
    private Date expiryDate;
    private Date issueDate;
    private Duration duration;
    private DebtComponent transactionDebtComponent;
    private DebtComponent penaltyDebtComponent;
    private Boolean issued = Boolean.FALSE;
    private ProductIssuanceType productIssuanceType;
    private Registrant registrant;
    private String productNumber;
    private Course course;
    private Integer barCode;
    private Register register;
    private String prefixName;
    private String conditions;
    private String examDate;
    private String practiceCertificateNumber;
    private String diplomaCertificateNumber;
    private String cgsCertificateNumber;
    private String comment;
    private String registrationCertificateNumber;
    private Boolean duplicate;
    private ProductIssuance productIssuance;
    private Integer serialNumber;


    @ManyToOne
    public ProductIssuance getProductIssuance() {
        return productIssuance;
    }

    public void setProductIssuance(ProductIssuance productIssuance) {
        this.productIssuance = productIssuance;
    }

    @ManyToOne
    public Duration getDuration() {

        return duration;

    }

    public void setDuration(Duration duration) {

        this.duration = duration;
    }

    public String getRegistrationCertificateNumber() {
        if (registrationCertificateNumber == null) {
            return "";
        } else {
            return registrationCertificateNumber;
        }
    }

    public void setRegistrationCertificateNumber(String registrationCertificateNumber) {
        this.registrationCertificateNumber = registrationCertificateNumber;
    }


    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    @ManyToOne
    public DebtComponent getTransactionDebtComponent() {
        return transactionDebtComponent;
    }

    public void setTransactionDebtComponent(DebtComponent transactionDebtComponent) {
        this.transactionDebtComponent = transactionDebtComponent;
    }

    @ManyToOne
    public DebtComponent getPenaltyDebtComponent() {
        return penaltyDebtComponent;
    }

    public void setPenaltyDebtComponent(DebtComponent penaltyDebtComponent) {
        this.penaltyDebtComponent = penaltyDebtComponent;
    }

    public Boolean getIssued() {
        if (issued == null) {
            return Boolean.FALSE;
        }
        return issued;
    }

    public void setIssued(Boolean issued) {
        this.issued = issued;
    }

    @Enumerated(EnumType.STRING)
    public ProductIssuanceType getProductIssuanceType() {
        return productIssuanceType;
    }

    public void setProductIssuanceType(ProductIssuanceType productIssuanceType) {
        this.productIssuanceType = productIssuanceType;
    }

    @ManyToOne
    public Registrant getRegistrant() {
        return registrant;
    }

    public void setRegistrant(Registrant registrant) {
        this.registrant = registrant;
    }

    @Column(unique = true)
    public String getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    @ManyToOne
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Column(unique = true)
    public Integer getBarCode() {
        return barCode;
    }

    public void setBarCode(Integer barCode) {
        this.barCode = barCode;
    }

    @Column(unique = true)
    public Integer getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(Integer serialNumber) {
        this.serialNumber = serialNumber;
    }

    @ManyToOne
    public Register getRegister() {
        return register;
    }

    public void setRegister(Register register) {
        this.register = register;
    }

    public String getPrefixName() {
        return prefixName;
    }

    public void setPrefixName(String prefixName) {
        this.prefixName = prefixName;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    public String getExamDate() {
        return examDate;
    }

    public void setExamDate(String examDate) {
        this.examDate = examDate;
    }

    @Column(unique = true)
    public String getPracticeCertificateNumber() {
        return practiceCertificateNumber;
    }

    public void setPracticeCertificateNumber(String practiceCertificateNumber) {
        this.practiceCertificateNumber = practiceCertificateNumber;
    }

    @Column(unique = true)
    public String getDiplomaCertificateNumber() {
        return diplomaCertificateNumber;
    }

    public void setDiplomaCertificateNumber(String diplomaCertificateNumber) {
        this.diplomaCertificateNumber = diplomaCertificateNumber;
    }

    @Column(unique = true)
    public String getCgsCertificateNumber() {
        return cgsCertificateNumber;
    }

    public void setCgsCertificateNumber(String cgsCertificateNumber) {
        this.cgsCertificateNumber = cgsCertificateNumber;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Boolean getDuplicate() {
        return BooleanUtils.toBoolean(duplicate);
    }

    public void setDuplicate(Boolean duplicate) {
        this.duplicate = duplicate;
    }

}
