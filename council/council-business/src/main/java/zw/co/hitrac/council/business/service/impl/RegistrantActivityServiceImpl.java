package zw.co.hitrac.council.business.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.RegistrantActivityDAO;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.accounts.ReceiptHeader;
import zw.co.hitrac.council.business.domain.accounts.ReceiptItem;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.CouncilDurationService;
import zw.co.hitrac.council.business.service.DurationService;
import zw.co.hitrac.council.business.service.RegistrantActivityService;
import zw.co.hitrac.council.business.service.accounts.ReceiptHeaderService;
import zw.co.hitrac.council.business.service.accounts.ReceiptItemService;

/**
 * @author Kelvin Goredema
 * @author Morris Baradza
 */
@Service
@Transactional
public class RegistrantActivityServiceImpl implements RegistrantActivityService {

    @Autowired
    private RegistrantActivityDAO registrantActivityDAO;
    @Autowired
    private DurationService durationService;
    @Autowired
    private RegistrationProcess registrationProcess;
    @Autowired
    private ReceiptHeaderService receiptHeaderService;
    @Autowired
    private ReceiptItemService receiptItemService;
    @Autowired
    private CouncilDurationService councilDurationService;

    @Transactional
    public RegistrantActivity save(RegistrantActivity registrantActivity) {

        return registrantActivityDAO.save(registrantActivity);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantActivity> findAll() {

        return registrantActivityDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantActivity get(Long id) {

        return registrantActivityDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantActivity> findAll(Boolean retired) {

        return registrantActivityDAO.findAll(retired);
    }

    public void setRegistrantActivityDAO(RegistrantActivityDAO registrantActivityDAO) {

        this.registrantActivityDAO = registrantActivityDAO;
    }

    @Transactional
    public void remove(RegistrantActivity registrantActivity) {

        registrantActivityDAO.remove(registrantActivity);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantActivity> getRegistrantActivities(Registrant registrant) {

        return registrantActivityDAO.getRegistrantActivities(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantActivity> getRegistrantActivities(Registrant registrant, RegistrantActivityType registrantActivityType) {

        return registrantActivityDAO.getRegistrantActivities(registrant, registrantActivityType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantActivity getLastRenewalActivity(Registrant registrant) {
        return registrantActivityDAO.getLastRenewalActivity(registrant);
    }

    public boolean checkRegistrantActivity(Registrant registrant, RegistrantActivityType registrantActivityType, Duration duration) {
        return registrantActivityDAO.checkRegistrantActivity(registrant, registrantActivityType, duration);
    }

    public boolean checkRegistrantActivity(Registrant registrant, RegistrantActivityType registrantActivityType, CouncilDuration councilDuration) {
        return registrantActivityDAO.checkRegistrantActivity(registrant, registrantActivityType, councilDuration);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<CouncilDuration> getRenewalPeriodsNotRenewed(Registrant registrant, RegistrantActivityType registrantActivityType) {

        final Registration currentRegistration = registrationProcess.activeRegisterNotStudentRegister(registrant);

        if (currentRegistration != null) {
            RegistrantActivity lastRenewal = getLastRenewalActivity(registrant);
            final Duration currentDuration = durationService.getCurrentCourseDuration(currentRegistration.getCourse());

            if (lastRenewal == null || currentDuration == null) {
                return new ArrayList<>();
            }


            List<CouncilDuration> periods = new ArrayList<>();
            try {
                final Integer yearRenewed = Integer.valueOf(lastRenewal.getDuration().getCouncilDuration().getName());

                List<CouncilDuration> councilDurations = councilDurationService.findAll();
                councilDurations = councilDurations.stream()
                        .filter(councilDuration -> Integer.valueOf(councilDuration.getName()) > yearRenewed)
                        .collect(Collectors.toList());


                for (CouncilDuration councilDuration : councilDurations) {
                    if (!checkRegistrantActivity(registrant, registrantActivityType, councilDuration)) {
                        periods.add(councilDuration);
                    }
                }

                for (CouncilDuration councilDuration : receiptHeaderService.getCouncilDurationsfromRecieptHeaders(registrant.getCustomerAccount().getAccount(), TypeOfService.ANNUAL_FEES)) {
                    periods.remove(councilDuration);
                }

                for (CouncilDuration councilDuration : receiptItemService.getCouncilDurationsPaidForByAccountAndTypeOfService(registrant.getCustomerAccount().getAccount(), TypeOfService.ANNUAL_FEES)) {
                    periods.remove(councilDuration);
                }
            }
            catch(Exception ex){

            }
            return periods;
        } else {
            return new ArrayList<>();
        }

    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantActivity getRegistrantActivityByDuration(Registrant registrant, Duration duration) {

        return registrantActivityDAO.getRegistrantActivityByDuration(registrant, duration);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getRegistrant(RegistrantActivityType registrantActivityType, Date startDate, Date endDate) {

        return registrantActivityDAO.getRegistrant(registrantActivityType, startDate, endDate);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Integer getTotalPerCourseAndDurationAndRegisterAndRegistrantActivity(Course course, Register register, List<Duration> durations, RegistrantActivityType registrantActivityType, Boolean active) {

        return registrantActivityDAO.getTotalPerCourseAndDurationAndRegisterAndRegistrantActivity(course, register, durations, registrantActivityType, active);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Integer getTotalPerQualificationAndDurationAndRegisterAndRegistrantActivity(Qualification qualification, Register register, Duration duration, RegistrantActivityType registrantActivityType) {

        return registrantActivityDAO.getTotalPerQualificationAndDurationAndRegisterAndRegistrantActivity(qualification, register, duration, registrantActivityType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Integer getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(Course course, Register register, List<Duration> durations, RegistrantActivityType registrantActivityType, Boolean active, Conditions condition) {

        return registrantActivityDAO.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(course, register, durations, registrantActivityType, active, condition);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getListPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(Course course, Register register, List<Duration> durations, RegistrantActivityType registrantActivityType, Boolean active, Enum<EmploymentType> e, Conditions condition) {

        return registrantActivityDAO.getListPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(course, register, durations, registrantActivityType, active, e, condition);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Integer getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(Course course, Register register, List<Duration> durations, RegistrantActivityType registrantActivityType, Boolean active, Enum<EmploymentType> e, Conditions condition) {

        return registrantActivityDAO.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(course, register, durations, registrantActivityType, active, e, condition);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantContact> getEmailAddressesPerQualificationMappedCourse(Course course, Register register, ContactType contactType) {

        return registrantActivityDAO.getEmailAddressesPerQualificationMappedCourse(course, register, contactType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getListPerCourseAndDurationAndRegisterAndRegistrantActivity(Course course, Register register, List<Duration> durations, RegistrantActivityType registrantActivityType, Boolean active) {

        return registrantActivityDAO.getListPerCourseAndDurationAndRegisterAndRegistrantActivity(course, register, durations, registrantActivityType, active);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getListPerQualificationAndDurationAndRegisterAndRegistrantActivity(Qualification qualification, Register register, Duration duration, RegistrantActivityType registrantActivityType) {

        return registrantActivityDAO.getListPerQualificationAndDurationAndRegisterAndRegistrantActivity(qualification, register, duration, registrantActivityType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantActivity> getRegistrantsByRegistrantActivityType(RegistrantActivityType registrantActivityType) {

        return registrantActivityDAO.getRegistrantsByRegistrantActivityType(registrantActivityType);
    }

    @Override
    public CouncilDuration getLastCouncilDurationRenewal(Registrant registrant) {
        return registrantActivityDAO.getLastCouncilDurationRenewal(registrant);
    }

    @Override
    public RegistrantActivity getRegistrantActivity(RegistrantActivityType registrantActivityType, Registrant registrant) {
        return registrantActivityDAO.getRegistrantActivity(registrantActivityType, registrant);
    }

    @Override
    public List<CouncilDuration> getRegisteredRenewalPeriods(String registrationNumber) {
        return registrantActivityDAO.getRegisteredRenewalPeriods(registrationNumber);
    }

    @Override
    public List<Registrant> getRegistrantsRenewedInCouncilDurationNotInListOfCouncilDurations(CouncilDuration councilDuration, List<CouncilDuration> councilDurations) {
        return registrantActivityDAO.getRegistrantsRenewedInCouncilDurationNotInListOfCouncilDurations(councilDuration, councilDurations);
    }

    @Override
    public List<Registrant> getRegistrantsRenewedInCouncilDuration(CouncilDuration councilDuration, Course course) {
        return registrantActivityDAO.getRegistrantsRenewedInCouncilDuration(councilDuration, course);
    }

    @Override
    public List<Registrant> getRegistrantsWhoNeverRenewed() {
        return registrantActivityDAO.getRegistrantsWhoNeverRenewed();
    }

    @Override
    public String getRegistrantActivityComment(Registrant registrant, RegistrantActivityType registrantActivityType) {
        RegistrantActivity registrantActivity = getRegistrantActivity(registrantActivityType, registrant);
        if (registrantActivity != null)
            return registrantActivity.getComment();
        else
            return "";
    }
}
