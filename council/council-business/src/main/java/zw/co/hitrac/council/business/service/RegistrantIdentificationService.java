/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service;

import java.util.List;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantIdentification;

/**
 *
 * @author kelvin
 */
public interface RegistrantIdentificationService extends IGenericService<RegistrantIdentification>{
   public List<RegistrantIdentification> getIdentities(Registrant registrant);
}
