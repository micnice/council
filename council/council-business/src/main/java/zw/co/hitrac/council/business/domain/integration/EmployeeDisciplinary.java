package zw.co.hitrac.council.business.domain.integration;

import java.util.Date;

/**
 * Created by tdhla on 3/2/2017.
 */
public class EmployeeDisciplinary {

    private Date caseDate;
    private String misconductType;

    public Date getCaseDate() {
        return caseDate;
    }

    public void setCaseDate(Date caseDate) {
        this.caseDate = caseDate;
    }

    public String getMisconductType() {
        return misconductType;
    }

    public void setMisconductType(String misconductType) {
        this.misconductType = misconductType;
    }
}
