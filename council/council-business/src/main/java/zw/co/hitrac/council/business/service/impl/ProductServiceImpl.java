package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.accounts.ProductDAO;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.business.domain.TypeOfService;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.domain.accounts.ProductGroup;
import zw.co.hitrac.council.business.service.accounts.ProductService;

import java.util.List;

/**
 *
 * @author Michael Matiashe
 */
@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDAO productDAO;

    @Transactional
    public Product save(Product t) {
        return productDAO.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Product> findAll() {
        return productDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Product get(Long id) {
        return productDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Product> findAll(Boolean retired) {

        return productDAO.findAll(retired);
    }

    public void setProductSaleDao(ProductDAO productSaleDAO) {
        this.productDAO = productSaleDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Product> findProducts(Product product) {
        return this.productDAO.findProducts(product);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Product> findProducts(TypeOfService typeOfService) {
        return this.productDAO.findProducts(typeOfService);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Product> findProducts(ProductGroup productGroup) {
        return productDAO.findProducts(productGroup);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Product> findProducts(ProductGroup productGroup, TypeOfService typeOfService, Register register) {
        return productDAO.findProducts(productGroup, typeOfService, register);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Product> findProducts(TypeOfService typeOfService, Register register, Course course) {
        return productDAO.findProducts(typeOfService, register, course);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Product> examProducts() {

        return productDAO.examProducts();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Product> findNoneExamProducts() {
        return productDAO.findNoneExamProducts();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Product> findAll(Boolean retired, Boolean directlyInvoicable) {
        return productDAO.findAll(retired, directlyInvoicable);
    }

    @Override
    public  Product getProductByCode(String productCode) {
        return productDAO.getProductByCode(productCode);
    }

    @Override
    public List<Product> findProductsWithoutPrices() {
        return productDAO.findProductsWithoutPrices();
    }

    @Override
    public void createProductAccounts() {
        productDAO.createProductAccounts();
    }

}
