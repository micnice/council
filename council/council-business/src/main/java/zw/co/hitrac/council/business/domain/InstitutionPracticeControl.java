/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.*;

/**
 *
 * @author tdhlakama
 */
@Entity
@Table(name = "institutionpracticecontrol")
@Audited
public class InstitutionPracticeControl extends BaseIdEntity {

    private Boolean institutionMeetsStandards = Boolean.FALSE;
    private Boolean constructionLocation = Boolean.FALSE;
    private Boolean equipmentFacility = Boolean.FALSE;
    private Boolean employedAndQualified = Boolean.FALSE;
    private Boolean registerdInstitution = Boolean.FALSE;
    private Boolean recommendationSentTOHPA = Boolean.FALSE;
    private String comment;
    private Institution institution;
    public static String DECISIONAPPROVED = "DECISION APPROVED";
    public static String DECISIONDECLINED = "DECISION DECLINED";
    public static String DECISIONPENDING = "DESICION PENDING";
    private String decisionStatus = DECISIONPENDING;
    private Application application;

    public Boolean getInstitutionMeetsStandards() {
        return institutionMeetsStandards;
    }

    public void setInstitutionMeetsStandards(Boolean institutionMeetsStandards) {
        this.institutionMeetsStandards = institutionMeetsStandards;
    }

    public Boolean getConstructionLocation() {
        return constructionLocation;
    }

    public void setConstructionLocation(Boolean constructionLocation) {
        this.constructionLocation = constructionLocation;
    }

    public Boolean getEquipmentFacility() {
        return equipmentFacility;
    }

    public void setEquipmentFacility(Boolean equipmentFacility) {
        this.equipmentFacility = equipmentFacility;
    }

    public Boolean getEmployedAndQualified() {
        return employedAndQualified;
    }

    public void setEmployedAndQualified(Boolean employedAndQualified) {
        this.employedAndQualified = employedAndQualified;
    }

    public Boolean getRegisterdInstitution() {
        return registerdInstitution;
    }

    public void setRegisterdInstitution(Boolean registerdInstitution) {
        this.registerdInstitution = registerdInstitution;
    }

    public Boolean getRecommendationSentTOHPA() {
        return recommendationSentTOHPA;
    }

    public void setRecommendationSentTOHPA(Boolean recommendationSentTOHPA) {
        this.recommendationSentTOHPA = recommendationSentTOHPA;
    }

    @Transient
    public Boolean getMeetsAllExpectations() {
        if (!institutionMeetsStandards) {
            return Boolean.FALSE;
        }
        if (!constructionLocation) {
            return Boolean.FALSE;
        }
        if (!equipmentFacility) {
            return Boolean.FALSE;
        }
        if (!employedAndQualified) {
            return Boolean.FALSE;
        }
        if (!institutionMeetsStandards) {
            return Boolean.FALSE;
        }
        if (!registerdInstitution) {
            return Boolean.FALSE;
        }
        if (!recommendationSentTOHPA) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    @Transient
    public String getMeetsAllRequirementsStatus() {
        return getMeetsAllExpectations() ? "YES" : "NO";
    }

    @Column(length = 1024)
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @ManyToOne
    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    public String getDecisionStatus() {
        return decisionStatus;
    }

    public void setDecisionStatus(String decisionStatus) {
        this.decisionStatus = decisionStatus;
    }

    @ManyToOne
    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }
}
