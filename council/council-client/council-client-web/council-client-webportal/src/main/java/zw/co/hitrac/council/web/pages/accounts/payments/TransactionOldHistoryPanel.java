/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.accounts.payments;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PageableListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.OldReceipt;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.accounts.OldReceiptService;
import zw.co.hitrac.council.web.utility.CustomDateLabel;

/**
 *
 * @author tdhlakama
 */
public class TransactionOldHistoryPanel extends Panel {

    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private OldReceiptService oldReceiptService;

    public TransactionOldHistoryPanel(String id, final Long registrantId) {
        super(id);

        PageableListView<OldReceipt> oldEachItem = new PageableListView<OldReceipt>("oldEachItem", oldReceiptService.getReceipts(null, registrantService.get(registrantId)), 20) {
            @Override
            protected void populateItem(ListItem<OldReceipt> item) {
                item.setModel(new CompoundPropertyModel<OldReceipt>(item.getModel()));
                item.add(new Label("receiptRecordID"));
                item.add(new Label("paymentMethod"));
                item.add(new Label("product"));
                item.add(new Label("lineTotal"));
                item.add(new CustomDateLabel("transactionDate"));
                item.add(new CustomDateLabel("dateOfReceipt"));
                item.add(new Label("paymentPurpose"));
//                item.add(new Label("user"));
            }
        };

        add(oldEachItem);

    }
}
