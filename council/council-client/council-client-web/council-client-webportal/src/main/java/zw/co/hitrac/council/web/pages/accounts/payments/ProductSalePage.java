/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.accounts.payments;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.domain.accounts.PaymentDetails;
import zw.co.hitrac.council.business.domain.accounts.PaymentMethod;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.domain.accounts.ReceiptItem;
import zw.co.hitrac.council.business.domain.reports.ProductSale;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.accounts.CustomerService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.business.service.accounts.PaymentDetailsService;
import zw.co.hitrac.council.business.service.accounts.PaymentMethodService;
import zw.co.hitrac.council.business.service.accounts.ProductService;
import zw.co.hitrac.council.business.service.accounts.ReceiptHeaderService;
import zw.co.hitrac.council.business.service.accounts.ReceiptItemService;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.reports.ProductBankDepositsReport;
import zw.co.hitrac.council.reports.ProductSaleNoneBankReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.PaymentMethodListModel;
import zw.co.hitrac.council.web.models.ProductListModel;
import zw.co.hitrac.council.web.pages.accounts.IAccountingPage;
import zw.co.hitrac.council.web.utility.DatePickerUtil;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 *
 * @author tdhlakama
 */
public class ProductSalePage extends IAccountingPage {

    private Date startDate, endDate;
    private Product product;
    private PaymentMethod paymentMethod;
    @SpringBean
    ProductService productService;
    @SpringBean
    ReceiptHeaderService receiptHeaderService;
    @SpringBean
    DebtComponentService debtComponentService;
    @SpringBean
    private CustomerService customerService;
    @SpringBean
    private ReceiptItemService receiptItemService;
    @SpringBean
    private RegistrationProcess registrationProcess;
    @SpringBean
    PaymentDetailsService paymentDetailsService;
    @SpringBean
    PaymentMethodService paymentMethodService;

    public ProductSalePage() {
        PropertyModel<Date> startDateModel = new PropertyModel<Date>(this, "startDate");
        PropertyModel<Date> endDateModel = new PropertyModel<Date>(this, "endDate");
        PropertyModel<String> productModel = new PropertyModel<String>(this, "product");
        PropertyModel<String> paymentMethodModel = new PropertyModel<String>(this, "paymentMethod");

        Form<?> form = new Form("form");
        form.add(new TextField<Date>("startDate", startDateModel).add(DatePickerUtil.getDatePicker()));
        form.add(new TextField<Date>("endDate", endDateModel).add(DatePickerUtil.getDatePicker()));
        form.add(new DropDownChoice("product", productModel, new ProductListModel(productService)).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("paymentMethod", paymentMethodModel, new PaymentMethodListModel(paymentMethodService)).setRequired(true).add(new ErrorBehavior()));
        add(form);
        add(new FeedbackPanel("feedback"));

        form.add(new Button("printBankCashReport") {
            @Override
            public void onSubmit() {

                List<ProductSale> productSales = new ArrayList<ProductSale>();
                List<ReceiptItem> receiptItems = receiptItemService.getReceiptItems(product, startDate, endDate, null, null);
                if (paymentMethod != null && paymentMethod.getDepositDateRequired()) {
                    for (ReceiptItem r : receiptItems) {
                        ProductSale productSale = new ProductSale();
                        PaymentDetails paymentDetails = paymentDetailsService.get(r.getReceiptHeader().getPaymentDetails().getId());
                        if (!r.getReceiptHeader().getPaymentDetails().getPaymentMethod().getDepositDateRequired()) {
                            final Registrant registrant = customerService.getRegistrant(r.getDebtComponent().getAccount());
                            if (registrant != null) {
                                productSale.setRegistrant(registrant);
                            } else {
                                Institution institution = customerService.getInstitution(paymentDetails.getCustomer().getAccount());
                                productSale.setInstitution(institution);
                            }
                            productSale.setDebtComponent(r.getDebtComponent());
                            productSale.setProduct(product);
                            productSale.setAmountPaid(r.getAmount());
                            productSale.setPaymentDetail(paymentDetails);
                            productSales.add(productSale);
                        }
                    }
                    String commnet = "";
                    if (startDate != null && endDate != null) {
                        commnet = "From - " + DateUtil.getDate(startDate) + " to " + DateUtil.getDate(endDate);
                    }
                    printBankReport(productSales, product.getName(), commnet);
                } else {
                    for (ReceiptItem r : receiptItems) {
                        ProductSale productSale = new ProductSale();
                        PaymentDetails paymentDetails = paymentDetailsService.get(r.getReceiptHeader().getPaymentDetails().getId());
                        if (paymentDetails.getPaymentMethod().equals(paymentMethod)) {
                            final Registrant registrant = customerService.getRegistrant(r.getDebtComponent().getAccount());
                            if (registrant != null) {
                                productSale.setRegistrant(registrant);
                            } else {
                                Institution institution = customerService.getInstitution(paymentDetails.getCustomer().getAccount());
                                productSale.setInstitution(institution);
                            }
                            productSale.setDebtComponent(r.getDebtComponent());
                            productSale.setProduct(product);
                            productSale.setAmountPaid(r.getAmount());
                            productSale.setPaymentDetail(paymentDetails);
                            productSales.add(productSale);
                        }

                    }
                    String commnet = "";
                    if (startDate != null && endDate != null) {
                        commnet = "From - " + DateUtil.getDate(startDate) + " to " + DateUtil.getDate(endDate);
                    }
                    printCashReport(productSales, commnet, product.getName());
                }
            }
        });
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    private void printBankReport(List<ProductSale> productSales, String product, String comment) {

        try {
            ProductBankDepositsReport productBankDepositsReport = new ProductBankDepositsReport();
            ContentType contentType = ContentType.PDF;
            Map parameters = new HashMap();

            parameters.put("comment", comment);
            parameters.put("item", product);
            ByteArrayResource resource = ReportResourceUtils.getReportResource(productBankDepositsReport,
                    contentType, productSales, parameters);
            IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                    RequestCycle.get().getResponse(), null);

            resource.respond(a);

            // To make Wicket stop processing form after sending response
            RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
        } catch (JRException ex) {
            ex.printStackTrace();
        }
    }

    private void printCashReport(List<ProductSale> productSales, String comment, String product) {
        try {
            ProductSaleNoneBankReport productSaleNoneBankReport = new ProductSaleNoneBankReport();
            ContentType contentType = ContentType.PDF;
            Map parameters = new HashMap();

            parameters.put("comment", comment);
            parameters.put("item", product);

            ByteArrayResource resource = ReportResourceUtils.getReportResource(productSaleNoneBankReport,
                    contentType, HrisComparator.sortProductSaleByReceiptNumber(productSales), parameters);
            IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                    RequestCycle.get().getResponse(), null);

            resource.respond(a);

            // To make Wicket stop processing form after sending response
            RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
        } catch (JRException ex) {
            ex.printStackTrace();
        }
    }
}
