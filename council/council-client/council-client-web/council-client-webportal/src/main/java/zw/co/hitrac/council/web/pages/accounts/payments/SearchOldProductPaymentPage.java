/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.accounts.payments;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.User;
import zw.co.hitrac.council.business.domain.accounts.OldReceipt;
import zw.co.hitrac.council.business.domain.accounts.PaymentMethod;
import zw.co.hitrac.council.business.domain.accounts.PaymentType;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.domain.reports.ProductItem;
import zw.co.hitrac.council.business.service.UserService;
import zw.co.hitrac.council.business.service.accounts.OldReceiptService;
import zw.co.hitrac.council.business.service.accounts.PaymentMethodService;
import zw.co.hitrac.council.business.service.accounts.PaymentTypeService;
import zw.co.hitrac.council.business.service.accounts.ProductService;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.reports.ProductItemReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.ProductListModel;
import zw.co.hitrac.council.web.pages.accounts.IAccountingPage;
import zw.co.hitrac.council.web.utility.DatePickerUtil;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 *
 * @author tdhlakama
 */
public class SearchOldProductPaymentPage extends IAccountingPage {

    private Long generatedReceiptNumber;
    private Date startDate, endDate;
    private User user;
    private String searchText;
    private PaymentMethod paymentMethod;
    private PaymentType paymentType;
    private Product product;
    @SpringBean
    private UserService userService;
    @SpringBean
    private OldReceiptService oldReceiptService;
    @SpringBean
    private PaymentMethodService paymentMethodService;
    @SpringBean
    private PaymentTypeService paymentTypeService;
    @SpringBean
    private ProductService productService;
    private String include;
    private HrisComparator hrisComparator = new HrisComparator();
    private String sourceReference;

    public SearchOldProductPaymentPage() {

        PropertyModel<Date> startDateModel = new PropertyModel<Date>(this, "startDate");
        PropertyModel<Date> endDateModel = new PropertyModel<Date>(this, "endDate");
        PropertyModel<String> productModel = new PropertyModel<String>(this, "product");
        PropertyModel<String> includeModel = new PropertyModel<String>(this, "include");
PropertyModel<String> sourceReferenceModel = new PropertyModel<String>(this, "sourceReference");
        
        Form<?> form = new Form("form");
        form.add(new TextField<Date>("startDate", startDateModel).setRequired(true).add(new ErrorBehavior()).add(DatePickerUtil.getDatePicker()));
        form.add(new TextField<Date>("endDate", endDateModel).setRequired(true).add(new ErrorBehavior()).add(DatePickerUtil.getDatePicker()));
        form.add(new DropDownChoice("product", productModel, new ProductListModel(productService)));
        List includelist = Arrays.asList(new String[]{"YES", "NO"});
        form.add(new DropDownChoice("include", includeModel, includelist));
        form.add(new DropDownChoice("sourceReference", sourceReferenceModel, oldReceiptService.getAccountSourceReferences()));

        add(form);
        add(new FeedbackPanel("feedback"));
        
        form.add(new Button("print") {
            @Override
            public void onSubmit() {
                try {
                    Set<ProductItem> productSales = new HashSet<ProductItem>();

                    if (startDate != null && endDate == null) {
                        endDate = new Date();
                    }
                    Boolean canceled = null;
                    if (include != null) {
                        if (include.equals("YES")) {
                            canceled = Boolean.TRUE;
                        } else {
                            canceled = Boolean.FALSE;
                        }
                    }
                    if (product != null) {
                        ProductItem productItem = new ProductItem();
                        productItem.setProduct(product);
                        productItem.setEndDate(endDate);
                        productItem.setStartDate(startDate);
                        BigDecimal totalPoints = new BigDecimal(0);
                        for (OldReceipt r : oldReceiptService.getReceipts(product, startDate, endDate, canceled, sourceReference)) {
                            totalPoints = totalPoints.add(new BigDecimal(r.getLineTotal()));
                        }
                        productItem.setAmountPaid(totalPoints);
                        productSales.add(productItem);
                    } else {
                        for (Product p : productService.findAll()) {
                            ProductItem productItem = new ProductItem();
                            productItem.setProduct(p);
                            productItem.setEndDate(endDate);
                            productItem.setStartDate(startDate);
                            BigDecimal totalPoints = new BigDecimal(0);
                            for (OldReceipt r : oldReceiptService.getReceipts(p, startDate, endDate, canceled, sourceReference)) {
                                totalPoints = totalPoints.add(new BigDecimal(r.getLineTotal()));
                            }
                            productItem.setAmountPaid(totalPoints);
                            productSales.add(productItem);
                        }
                    }

                    ProductItemReport productItemReport = new ProductItemReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    String commnet = "";
                    if (startDate != null && endDate != null) {
                        commnet = "Sales Done form From - " + DateUtil.getDate(startDate) + " to " + DateUtil.getDate(endDate);
                    }
                      String sourceReferenceLabel = "";
                    if (sourceReference != null) {
                        sourceReferenceLabel = " Bank " + sourceReference;
                    }
                    parameters.put("comment", commnet);

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(productItemReport,
                            contentType, hrisComparator.sortProductSale(new ArrayList<ProductItem>(productSales)), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

    }

    public Long getGeneratedReceiptNumber() {
        return generatedReceiptNumber;
    }

    public void setGeneratedReceiptNumber(Long generatedReceiptNumber) {
        this.generatedReceiptNumber = generatedReceiptNumber;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public PaymentMethodService getPaymentMethodService() {
        return paymentMethodService;
    }

    public void setPaymentMethodService(PaymentMethodService paymentMethodService) {
        this.paymentMethodService = paymentMethodService;
    }

    public PaymentTypeService getPaymentTypeService() {
        return paymentTypeService;
    }

    public void setPaymentTypeService(PaymentTypeService paymentTypeService) {
        this.paymentTypeService = paymentTypeService;
    }

    public String getInclude() {
        return include;
    }

    public void setInclude(String include) {
        this.include = include;
    }
}
