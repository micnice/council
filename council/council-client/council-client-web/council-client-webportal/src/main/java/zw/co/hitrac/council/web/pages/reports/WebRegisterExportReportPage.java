package zw.co.hitrac.council.web.pages.reports;

import net.sf.jasperreports.engine.JRException;
import org.apache.commons.io.IOUtils;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.springframework.beans.factory.annotation.Autowired;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.System.lineSeparator;
import static zw.co.hitrac.council.business.domain.RegistrantActivityType.RENEWAL;

/**
 * Created by ezinzombe on 2/23/17.
 */
public class WebRegisterExportReportPage extends TemplatePage {

    @SpringBean
    private CouncilDurationService councilDurationService;
    private CouncilDuration councilDuration;
    private ContentType contentType;
    private Course course;
    @SpringBean
    private DurationService durationService;
    @SpringBean
    private RegistrantActivityService registrantActivityService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private RegisterService registerService;
    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private DataSource dataSource;
    @SpringBean
    private RegistrationService registrationService;
    private HttpServletResponse response;
    @SpringBean
    private CourseService courseService;

    private static final char COLUMN_SEPARATOR = ',';

    @PersistenceContext
    private EntityManager entityManager;

    public WebRegisterExportReportPage() {

        PropertyModel<CouncilDuration> councilDurationModel = new PropertyModel<CouncilDuration>(this, "councilDuration");

        Form<?> form = new Form("form");

        form.add(new DropDownChoice("councilDuration", councilDurationModel, councilDurationService.findAll()));

        form.add(new Button("submitAllRegistrants") {
            @Autowired
            public void onSubmit() {

                List<RegistrantData> registrantsActive = new ArrayList<RegistrantData>();

                Set<Register> registers = new HashSet<Register>();

                registers.addAll(registerService.findAll());
                registers.remove(generalParametersService.get().getStudentRegister());

                List<Duration> durations = new ArrayList<Duration>(councilDurationService.get(councilDuration.getId()).getDurations());

                for (Register r : registers) {

                    for (Course c : courseService.findAll()) {
                        registrantsActive.addAll(registrantActivityService.getListPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(c, r, durations, RENEWAL, Boolean.TRUE, null, null));
                    }

                }
                registrantsActive.stream().forEach(r -> r.setStatus(true));

                List<RegistrantData> listWithoutDuplicatesList = registrantsActive.stream()
                        .sorted(Comparator.comparing(RegistrantData::getRegistrationDate,
                                Comparator.nullsFirst(Comparator.naturalOrder())))
                        .distinct()
                        .collect(Collectors.toList());

                exportCSV(response, Optional.ofNullable(listWithoutDuplicatesList), "list.csv");

            }

        });

        add(form);

    }

    public HttpServletResponse exportCSV(HttpServletResponse response, Optional<List<RegistrantData>> registrantDataList, String fileName) {

        try {

            File file = new File(fileName);
            FileWriter fw = new FileWriter(file);
            fw.append("Registration Number,Title,Firstname,Lastname,Gender,Discipline,Qualification,Status,Business Address, Conditions Of Practice");
            fw.append(System.getProperty("line.separator"));

            registrantDataList.get().stream().forEach(registrantData -> {
                System.out.println("registrantData ++++++::::" + registrantData.getQualificationName());
            });

            for (RegistrantData r : registrantDataList.get()) {

                fw.append(r.getRegistrationNumber());
                fw.append(COLUMN_SEPARATOR);
                fw.append(r.getTitleName());
                fw.append(COLUMN_SEPARATOR);
                fw.append(r.getFirstname());
                fw.append(COLUMN_SEPARATOR);
                fw.append(r.getLastname());
                fw.append(COLUMN_SEPARATOR);
                fw.append(r.getGender());
                fw.append(COLUMN_SEPARATOR);
                fw.append(r.getCourseName());
                fw.append(COLUMN_SEPARATOR);
                if (r.getQualificationName() != null && !r.getQualificationName().isEmpty()) {
                    fw.append(r.getQualificationName().replaceAll(",", " "));
                } else {
                    fw.append(null);
                }
                fw.append(COLUMN_SEPARATOR);
                fw.append("Active");
                fw.append(COLUMN_SEPARATOR);
                if (r.getBusinessAddress() != null || r.getBusinessAddress().isEmpty()) {
                    fw.append(r.getBusinessAddress().replaceAll(",", " "));
                } else {
                    fw.append(r.getResidentialAddress().replaceAll(",", " "));
                }
                fw.append(COLUMN_SEPARATOR);
                fw.append(r.getConditionName());
                fw.append(System.getProperty("line.separator"));

            }

            fw.flush();
            fw.close();
            FileInputStream fileInputStream = new FileInputStream(file);

            try {
                ByteArrayResource resource = ReportResourceUtils.getReportResource(contentType, IOUtils.toByteArray(fileInputStream), fileName);
                IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                        RequestCycle.get().getResponse(), null);
                resource.respond(a);

                RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
            } catch (JRException exception) {
                exception.getMessage();
            } finally {
                fileInputStream.close();
                file.delete();
            }
            response.getOutputStream();

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return response;
    }
}
