package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.PageReference;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.District;
import zw.co.hitrac.council.business.service.DistrictService;
import zw.co.hitrac.council.business.service.ProvinceService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 * @author Constance Mabaso
 */
public class DistrictEditPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private DistrictService districtService;
    @SpringBean
    private ProvinceService provinceService;

    public DistrictEditPage(Long id, final PageReference pageReference) {

        setDefaultModel(new CompoundPropertyModel<District>(new LoadableDetachableDistrictModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        Form<District> form = new Form<District>("form", (IModel<District>) getDefaultModel()) {

            @Override
            public void onSubmit() {

                districtService.save(getModelObject());
                setResponsePage(pageReference.getPage());
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new DropDownChoice("province", provinceService.findAll()));
        form.add(new Link<Void>("returnLink") {

            @Override
            public void onClick() {

                setResponsePage(pageReference.getPage());
            }
        });
        add(form);
    }

    private final class LoadableDetachableDistrictModel extends LoadableDetachableModel<District> {

        private Long id;

        public LoadableDetachableDistrictModel(Long id) {

            this.id = id;
        }

        @Override
        protected District load() {

            if (id == null) {
                return new District();
            }
            return districtService.get(id);
        }
    }

}
