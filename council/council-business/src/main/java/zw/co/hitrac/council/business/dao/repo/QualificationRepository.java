package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.Qualification;

/**
 *
 * @author Charles Chigoriwa
 */
public interface QualificationRepository extends CrudRepository<Qualification, Long> {

    public List<Qualification> findAll();
	public Qualification findByName(String name);
    
}
