package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.PageReference;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.City;
import zw.co.hitrac.council.business.domain.District;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.service.CityService;
import zw.co.hitrac.council.business.service.CountryService;
import zw.co.hitrac.council.business.service.DistrictService;
import zw.co.hitrac.council.business.service.ProvinceService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.CountryListModel;
import zw.co.hitrac.council.web.models.DistrictListModel;
import zw.co.hitrac.council.web.pages.registration.RegistrantAddressEditPage;

/**
 *
 * @author Matiashe Michael
 */
public class CityEditPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private CityService cityService;
    @SpringBean
    private CountryService countryService;
    @SpringBean
    private DistrictService districtService;

    public CityEditPage(Long id, final PageReference returnToPage) {
        setDefaultModel(new CompoundPropertyModel<City>(new LoadableDetachableCityModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        Form<City> form = new Form<City>("form", (IModel<City>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                cityService.save(getModelObject());
                if(returnToPage == null){
                    setResponsePage(new CityViewPage(getModelObject().getId()));
                }else{
                    setResponsePage(returnToPage.getPage());
                }
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new DropDownChoice("country", new CountryListModel(countryService)));
        form.add(new DropDownChoice("district", new DistrictListModel(districtService)));
        form.add(new Link("returnLink") {
            public void onClick() {
                setResponsePage(returnToPage.getPage());
            }
        });
        add(form);
    }

    public CityEditPage(Long id, final IModel<Registrant> registrantModel, final IModel<Institution> institutionModel) {

        setDefaultModel(new CompoundPropertyModel<City>(new LoadableDetachableCityModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        Form<City> form = new Form<City>("form", (IModel<City>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                cityService.save(getModelObject());
                if (registrantModel != null) {
                    setResponsePage(new RegistrantAddressEditPage(registrantModel));
                } else if (institutionModel != null) {
                    setResponsePage(new RegistrantAddressEditPage(institutionModel.getObject()));
                }
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new DropDownChoice("country", new CountryListModel(countryService)));
        form.add(new DropDownChoice("district", new DistrictListModel(districtService)));
        form.add(new BookmarkablePageLink<Void>("returnLink", CityListPage.class));
        add(form);
    }

    private final class LoadableDetachableCityModel extends LoadableDetachableModel<City> {

        private Long id;

        public LoadableDetachableCityModel(Long id) {
            this.id = id;
        }

        @Override
        protected City load() {
            if (id == null) {
                return new City();
            }
            return cityService.get(id);
        }
    }
}
