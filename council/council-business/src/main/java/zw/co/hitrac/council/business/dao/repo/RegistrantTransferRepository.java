package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.RegistrantTransfer;

/**
 *
 * @author Michael Matiashe
 */
public interface RegistrantTransferRepository extends CrudRepository<RegistrantTransfer, Long> {
    public List<RegistrantTransfer> findAll();
}
