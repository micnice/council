/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.RegistrantAssessment;

/**
 *
 * @author kelvin
 */
public interface RegistrantAssessmentRepository extends CrudRepository<RegistrantAssessment, Long>{
    
    public List<RegistrantAssessment> findAll();
}
