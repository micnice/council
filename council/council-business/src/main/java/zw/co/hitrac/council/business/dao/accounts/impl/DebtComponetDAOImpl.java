package zw.co.hitrac.council.business.dao.accounts.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.accounts.DebtComponentDAO;
import zw.co.hitrac.council.business.dao.repo.accounts.DebtComponentRepository;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.domain.TypeOfService;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.domain.accounts.Product;

/**
 * e
 *
 * @author Michael Matiashe
 */
@Repository
public class DebtComponetDAOImpl implements DebtComponentDAO {

    @Autowired
    private DebtComponentRepository debtComponentRepository;
    @PersistenceContext
    private EntityManager entityManager;

    public DebtComponent save(DebtComponent t) {
        return debtComponentRepository.save(t);
    }

    public List<DebtComponent> findAll() {
        return debtComponentRepository.findAll();
    }

    public DebtComponent get(Long id) {
        return debtComponentRepository.findOne(id);
    }

    public DebtComponentRepository getDebtComponentRepository() {
        return debtComponentRepository;
    }

    public void setDebtComponentRepository(DebtComponentRepository debtComponentRepository) {
        this.debtComponentRepository = debtComponentRepository;
    }

    public List<DebtComponent> getDebtComponents(Account account) {
        return entityManager.createQuery("select d from DebtComponent d where d.account=:account and d.remainingBalance>0").setParameter("account", account).getResultList();
    }

    public List<DebtComponent> getViewDebtComponentsRemoved(Account account) {
        return entityManager.createQuery("select d from DebtComponent d where d.account=:account and d.comment is not null and d.remainingBalance=0").setParameter("account", account).getResultList();
    }

    public List<DebtComponent> getDebtComponents(Customer customerAccount) {
        return getDebtComponents(customerAccount.getAccount());
    }

    public List<DebtComponent> getViewRemovedDeptComponets(Customer customerAccount) {
        return getViewDebtComponentsRemoved(customerAccount.getAccount());
    }

    public Boolean hasAccountDebtComponents(Customer customerAccount, TypeOfService typeOfService) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(DebtComponent.class);
        criteria.add(Restrictions.eq("account", customerAccount.getAccount()));
        criteria.createAlias("product", "p");
        criteria.add(Restrictions.eq("p.typeOfService", typeOfService));
        criteria.add(Restrictions.gt("remainingBalance", BigDecimal.ZERO));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list().isEmpty() ? Boolean.FALSE : Boolean.TRUE;
    }

    public BigDecimal getCustomerBalanceDue(Customer customerAccount) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(DebtComponent.class);
        criteria.add(Restrictions.eq("account", customerAccount.getAccount()));
        criteria.add(Restrictions.gt("remainingBalance", BigDecimal.ZERO));
        criteria.setProjection(Projections.sum("remainingBalance"));
        BigDecimal amount = (BigDecimal) criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).uniqueResult();
        if (amount == null) {
            return BigDecimal.ZERO;
        } else {
            return amount;
        }
    }

    public List<DebtComponent> getUnPaidDeptComponets() {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(DebtComponent.class);
        criteria.createAlias("account", "a");
        criteria.add(Restrictions.eq("a.personalAccount", Boolean.TRUE));
        criteria.add(Restrictions.gt("remainingBalance", BigDecimal.ZERO));        
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    public List<DebtComponent> getPenaltyDeptComponets() {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(DebtComponent.class);
        criteria.createAlias("product", "p");
        criteria.add(Restrictions.eq("p.typeOfService", TypeOfService.PENALTY_FEES));
        criteria.add(Restrictions.gt("remainingBalance", BigDecimal.ZERO));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    public List<DebtComponent> getDebtComponents(Product product, Date startDate, Date endDate) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(DebtComponent.class);
        if (product != null) {
            criteria.add(Restrictions.eq("product", product));
        }
        if (startDate != null && endDate != null) {
            criteria.add(Restrictions.between("dateCreated", startDate, endDate));
        } else if (startDate != null && endDate == null) {
            criteria.add(Restrictions.between("dateCreated", startDate, new Date()));
        }
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    public List<DebtComponent> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<DebtComponent> getRemovedDeptComponents() {
        return entityManager.createQuery("SELECT d from DebtComponent d WHERE d.comment IS NOT NULL").getResultList();
    }

}
