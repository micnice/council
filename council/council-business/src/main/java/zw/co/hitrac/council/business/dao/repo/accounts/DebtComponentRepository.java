
package zw.co.hitrac.council.business.dao.repo.accounts;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;

/**
 *
 * @author Tatenda Chiwandire
 * @author Edward Zengeni
 */
public interface DebtComponentRepository extends CrudRepository<DebtComponent, Long> {

    public List<DebtComponent> findAll();
}
