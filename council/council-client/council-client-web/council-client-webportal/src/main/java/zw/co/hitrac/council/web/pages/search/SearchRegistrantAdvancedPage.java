package zw.co.hitrac.council.web.pages.search;

//~--- non-JDK imports --------------------------------------------------------
import net.sf.jasperreports.engine.JRException;

import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Citizenship;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.CitizenshipService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.wicket.markup.html.form.Button;
import zw.co.hitrac.council.reports.RegistrantDataReport;
import zw.co.hitrac.council.web.pages.registration.RegistrantDataViewPanel;

/**
 *
 * @author takunda Dhlakama
 */
public class SearchRegistrantAdvancedPage extends TemplatePage {

    @SpringBean
    private CitizenshipService citizenshipService;
    @SpringBean
    private RegistrantService registrantService;
    private String searchText;
    private String gender;
    private Citizenship citizenship;
    private String include;

    public SearchRegistrantAdvancedPage() {
        menuPanelManager.getSearchPanel().setTopMenuCurrent(true);
        menuPanelManager.getSearchPanel().setSearchAdvanced(true);

        PropertyModel<Citizenship> citizenshipModel = new PropertyModel<Citizenship>(this, "citizenship");
        PropertyModel<String> searchTextModel = new PropertyModel<String>(this, "searchText");
        PropertyModel<String> genderModel = new PropertyModel<String>(this, "gender");
        PropertyModel<String> includeModel = new PropertyModel<String>(this, "include");
        Form<?> form = new Form("form");

        form.add(new DropDownChoice("citizenship", citizenshipModel, citizenshipService.findAll()));
        form.add(new TextField<String>("searchText", searchTextModel));

        List genderlist = Arrays.asList(new String[]{"M", "F"});

        form.add(new DropDownChoice("gender", genderModel, genderlist));

        List includelist = Arrays.asList(new String[]{"YES", "NO"});

        form.add(new DropDownChoice("include", includeModel, includelist));
        add(form);

        final IModel<List<RegistrantData>> model = new LoadableDetachableModel<List<RegistrantData>>() {
            @Override
            protected List<RegistrantData> load() {
                if ((gender == null) && (citizenship == null) && (searchText == null)
                        && (include == null)) {
                    return new ArrayList<RegistrantData>();
                } else {
                    Boolean dead = null;

                    if (include != null) {
                        if (include.equals("YES")) {
                            dead = Boolean.TRUE;
                        } else {
                            dead = Boolean.FALSE;
                        }
                    }
                    //TODO check  dead in table
                    return registrantService.getRegistrantList(searchText, gender, citizenship, null);
                }
            }
        };

        form.add(new Button("registrantlist") {
            @Override
            public void onSubmit() {
                Map parameters = new HashMap();
                parameters.put("comment", "Nurses");
                try {
                    RegistrantDataReport registrantDataReport = new RegistrantDataReport();
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(registrantDataReport, contentType, model.getObject(), new HashMap<String, Object>());
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

        // Registrant List Data View Panel
        add(new RegistrantDataViewPanel("registrantDataListPanel", model));
        add(new FeedbackPanel("feedback"));
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
