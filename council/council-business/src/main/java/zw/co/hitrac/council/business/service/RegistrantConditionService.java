/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service;

import java.util.List;

import zw.co.hitrac.council.business.domain.Conditions;
import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantCondition;

/**
 * @author kelvin
 */
public interface RegistrantConditionService extends IGenericService<RegistrantCondition> {

    List<RegistrantCondition> getRegistrantActiveConditions(Registrant registrant);

    List<RegistrantCondition> getCondition(Registrant registrant, Conditions conditions);

    List<RegistrantCondition> getAllRegistrantConditions(Registrant registrant);

    List<RegistrantCondition> getRegisterRegistrantConditions(Registrant registrant);

    Integer getNumberOfConditionsExpired();

    List<RegistrantCondition> getAllActiveRegistrantConditions();

    List<RegistrantCondition> getAllExpiredConditions();

    void saveRegisterConditions(Register register, Registrant registrant);

}
