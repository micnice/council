package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.ProductIssuance;

/**
 *
 * @author Edward Zengeni
 */
public interface ProductIssuanceRepository extends CrudRepository<ProductIssuance, Long> {

    public List<ProductIssuance> findAll();
}
