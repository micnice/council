/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.reports;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CaseOutcome;
import zw.co.hitrac.council.business.domain.MisconductType;
import zw.co.hitrac.council.business.domain.RegistrantDisciplinary;
import zw.co.hitrac.council.business.service.CaseOutcomeService;
import zw.co.hitrac.council.business.service.MisconductTypeService;
import zw.co.hitrac.council.business.service.RegistrantDisciplinaryService;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.reports.RegistrantDisciplinaryCaseReport;
import zw.co.hitrac.council.reports.RegistrantDisciplinaryMisconductTypeReport;
import zw.co.hitrac.council.reports.RegistrantDisciplinaryReport;
import zw.co.hitrac.council.reports.RegistrantDisciplinaryTypeReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.DatePickerUtil;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 * @author tdhlakama
 * @author emagodi
 */
public class DisciplinaryReportPage extends TemplatePage {

    @SpringBean
    private RegistrantDisciplinaryService registrantDisciplinaryService;
    private Date startDate, endDate;
    @SpringBean
    private CaseOutcomeService caseOutcomeService;
    @SpringBean
    private MisconductTypeService misconductTypeService;
    private CaseOutcome caseOutcome;
    private MisconductType misconductType;
    private ContentType contentType;
    private String caseStatus;



    public DisciplinaryReportPage() {

        PropertyModel<Date> startDateModel = new PropertyModel<Date>(this, "startDate");
        final PropertyModel<Date> endDateModel = new PropertyModel<Date>(this, "endDate");
        PropertyModel<String> caseOutcomeModel = new PropertyModel<String>(this, "caseOutcome");
        PropertyModel<String> misconductTypeModel = new PropertyModel<String>(this, "misconductType");
        PropertyModel<ContentType> contentTypeModel = new PropertyModel<ContentType>(this, "contentType");
        PropertyModel<String> caseStatusModel = new PropertyModel<String>(this, "caseStatus");
        Form<?> form = new Form("form");
                form.add(new DropDownChoice("contentType", contentTypeModel, Arrays.asList(ContentType.values())));
        form.add(new TextField<Date>("startDate", startDateModel).setRequired(true).add(new ErrorBehavior()).add(DatePickerUtil.getDatePicker()));
        form.add(new TextField<Date>("endDate", endDateModel).add(DatePickerUtil.getDatePicker()));
        form.add(new DropDownChoice("caseOutcome", caseOutcomeModel, caseOutcomeService.findAll()));
        form.add(new DropDownChoice("misconductType", misconductTypeModel, misconductTypeService.findAll()));
        form.add(new DropDownChoice("caseStatus", caseStatusModel,RegistrantDisciplinary.getCaseStatusList));
        add(form);

        form.add(new Button("report") {
            @Override
            public void onSubmit() {
                try {
                    Map parameters = new HashMap();

                        RegistrantDisciplinaryReport registrantDisciplinaryReport = new RegistrantDisciplinaryReport();
                        if(startDate!=null && endDate==null)
                        {
                            endDate = new Date();
                        }
                        parameters.put("Comment", "All Disciplinary Cases from " + DateUtil.getDate(startDate) + " to " + DateUtil.getDate(endDate));
                        ByteArrayResource resource = ReportResourceUtils.getReportResource(registrantDisciplinaryReport, contentType, registrantDisciplinaryService.getDisciplinariesByDates(startDate, endDate, caseOutcome, misconductType, caseStatus), parameters);
                        IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                RequestCycle.get().getResponse(), null);
                        resource.respond(a);
                        // To make Wicket stop processing form after sending response
                        RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

//                    if (caseOutcome != null && misconductType != null && caseStatus ==null) {
//                        RegistrantDisciplinaryTypeReport registrantDisciplinaryTypeReport = new RegistrantDisciplinaryTypeReport();
//                        if(startDate!=null && endDate==null)
//                        {
//                            endDate = new Date();
//                        }
//                        parameters.put("Comment", "Disciplinary Cases * Misconduct Type  - ( " + misconductType.getName() + " ).  Case Out Come - ( " + caseOutcome.getName() + " ). From " + DateUtil.getDate(startDate) + " to " + DateUtil.getDate(endDate));
//                        ByteArrayResource resource = ReportResourceUtils.getReportResource(registrantDisciplinaryTypeReport, contentType, registrantDisciplinaryService.getDisciplinariesByDates(startDate, endDate, caseOutcome, misconductType, caseStatus), parameters);
//                        IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
//                                RequestCycle.get().getResponse(), null);
//                        resource.respond(a);
//                        // To make Wicket stop processing form after sending response
//                        RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
//                    }
//
//                    if (caseOutcome != null && misconductType == null && caseStatus ==null) {
//                        RegistrantDisciplinaryCaseReport registrantDisciplinaryCaseReport = new RegistrantDisciplinaryCaseReport();
//                        if(startDate!=null && endDate==null)
//                        {
//                            endDate = new Date();
//                        }
//                        parameters.put("Comment",  "Disciplinary Cases * Case Out Come - ( "+ caseOutcome.getName()  +  " ). From " + DateUtil.getDate(startDate) + " to " + DateUtil.getDate(endDate));
//                        ByteArrayResource resource = ReportResourceUtils.getReportResource(registrantDisciplinaryCaseReport, contentType, registrantDisciplinaryService.getDisciplinariesByDates(startDate, endDate, caseOutcome, misconductType, caseStatus), parameters);
//                        IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
//                                RequestCycle.get().getResponse(), null);
//                        resource.respond(a);
//                        // To make Wicket stop processing form after sending response
//                        RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
//                    }
//
//                    if (caseOutcome == null && misconductType != null && caseStatus ==null) {
//                        RegistrantDisciplinaryMisconductTypeReport registrantDisciplinaryMisconductTypeReport = new RegistrantDisciplinaryMisconductTypeReport();
//                        if(startDate!=null && endDate==null)
//                        {
//                            endDate = new Date();
//                        }
//                        parameters.put("Comment", "Disciplinary Cases * Misconduct Type  - ( " + misconductType.getName() + " ). From " + DateUtil.getDate(startDate) + " to " + DateUtil.getDate(endDate));
//                        ByteArrayResource resource = ReportResourceUtils.getReportResource(registrantDisciplinaryMisconductTypeReport, contentType, registrantDisciplinaryService.getDisciplinariesByDates(startDate, endDate, caseOutcome, misconductType, caseStatus), parameters);
//                        IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
//                                RequestCycle.get().getResponse(), null);
//                        resource.respond(a);
//                        // To make Wicket stop processing form after sending response
//                        RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
//                    }



//
//                    if (caseOutcome == null && misconductType != null && caseStatus ==null) {
//                        RegistrantDisciplinaryMisconductTypeReport registrantDisciplinaryMisconductTypeReport = new RegistrantDisciplinaryMisconductTypeReport();
//                        if(startDate!=null && endDate==null)
//                        {
//                            endDate = new Date();
//                        }
//                        parameters.put("Comment", "Disciplinary Cases * Misconduct Type  - ( " + misconductType.getName() + " ). From " + DateUtil.getDate(startDate) + " to " + DateUtil.getDate(endDate));
//                        ByteArrayResource resource = ReportResourceUtils.getReportResource(registrantDisciplinaryMisconductTypeReport, contentType, registrantDisciplinaryService.getDisciplinariesByDates(startDate, endDate, caseOutcome, misconductType, caseStatus
//                        ), parameters);
//                        IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
//                                RequestCycle.get().getResponse(), null);
//                        resource.respond(a);
//                        // To make Wicket stop processing form after sending response
//                        RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
//                    }
//
//
//
//
//
//                    if (caseOutcome == null && misconductType != null && caseStatus ==null) {
//                        RegistrantDisciplinaryMisconductTypeReport registrantDisciplinaryMisconductTypeReport = new RegistrantDisciplinaryMisconductTypeReport();
//                        if(startDate!=null && endDate==null)
//                        {
//                            endDate = new Date();
//                        }
//                        parameters.put("Comment", "Disciplinary Cases * Misconduct Type  - ( " + caseStatus.toString() + " ). From " + DateUtil.getDate(startDate) + " to " + DateUtil.getDate(endDate));
//                        ByteArrayResource resource = ReportResourceUtils.getReportResource(registrantDisciplinaryMisconductTypeReport, contentType, registrantDisciplinaryService.getDisciplinariesByDates(startDate, endDate, caseOutcome, misconductType, caseStatus
//                        ), parameters);
//                        IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
//                                RequestCycle.get().getResponse(), null);
//                        resource.respond(a);
//                        // To make Wicket stop processing form after sending response
//                        RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
//                    }







                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

    }
}
