/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.AssessmentType;
import zw.co.hitrac.council.business.service.AssessmentTypeService;

/**
 *
 * @author kelvin
 */
public class AssessmentTypeViewPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private AssessmentTypeService addressTypeService;

    public AssessmentTypeViewPage(Long id) {
        CompoundPropertyModel<AssessmentType> model=new CompoundPropertyModel<AssessmentType>(new LoadableDetachableAssessmentModel(id));
        setDefaultModel(model);
        add(new Link<AssessmentType>("editLink",model){

            @Override
            public void onClick() {
                setResponsePage(new AssessmentTypeEditPage(getModelObject().getId()));
            }
            
        });
        add(new BookmarkablePageLink<Void>("returnLink", AssessmentTypeListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("assessmentType",model.bind("name")));
    }


    private final class LoadableDetachableAssessmentModel extends LoadableDetachableModel<AssessmentType> {

        private Long id;

        public LoadableDetachableAssessmentModel(Long id) {
            this.id = id;
        }

        @Override
        protected AssessmentType load() {

            return addressTypeService.get(id);
        }
    }
}
