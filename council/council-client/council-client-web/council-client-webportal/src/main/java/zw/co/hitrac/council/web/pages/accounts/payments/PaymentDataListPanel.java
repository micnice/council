/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.accounts.payments;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PageableListView;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigator;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.PaymentDetails;
import zw.co.hitrac.council.business.domain.accounts.ReceiptHeader;
import zw.co.hitrac.council.business.domain.accounts.ReceiptItem;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.accounts.PaymentDetailsService;
import zw.co.hitrac.council.business.service.accounts.ReceiptHeaderService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.reports.Receipt;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.utility.CustomDateLabel;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 * @author tdhlakama
 */
public class PaymentDataListPanel extends Panel {

    @SpringBean
    ReceiptHeaderService receiptHeaderService;
    @SpringBean
    PaymentDetailsService paymentDetailsService;
    private HrisComparator hrisComparator = new HrisComparator();
    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    private Boolean reversed = Boolean.FALSE;

    public PaymentDataListPanel(String id, IModel<List<ReceiptHeader>> model) {
        super(id);
        PageableListView<ReceiptHeader> eachItem = new PageableListView<ReceiptHeader>("eachItem", model, 20) {
            @Override
            final protected void populateItem(ListItem<ReceiptHeader> item) {
                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                item.setModel(new CompoundPropertyModel<ReceiptHeader>(item.getModel()));
                reversed = item.getModelObject().getReversed();
                item.add(new Label("id"));
                item.add(new CustomDateLabel("date"));
                item.add(new Label("totalAmountPaid"));
                item.add(new Label("carryForward"));
                item.add(new Label("transactionType"));
                item.add(new Label("generatedReceiptNumber"));
                final ReceiptHeader header = item.getModelObject();
                item.add(new Link<Void>("transactionDetailHistory") {
                    @Override
                    public void onClick() {
                        setResponsePage(new TransactionDetailHistory(header.getPaymentDetails().getId()));
                    }
                });
            }
        };
        add(new PagingNavigator("navigator", eachItem));
        add(eachItem);
    }

}
