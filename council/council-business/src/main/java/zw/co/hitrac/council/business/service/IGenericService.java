package zw.co.hitrac.council.business.service;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Charles Chigoriwa
 */
public interface IGenericService<T extends Serializable> extends Serializable {

    public T save(T t);

    public List<T> findAll();

    public T get(Long id);

    public List<T> findAll(Boolean retired);
}
