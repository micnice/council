package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.RegistrantQualification;

/**
 *
 * @author Michael Matiashe
 */
public interface RegistrantQualificationRepository extends CrudRepository<RegistrantQualification, Long> {
    public List<RegistrantQualification> findAll();
}
