package zw.co.hitrac.council.business.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.RegistrationDAO;
import zw.co.hitrac.council.business.dao.repo.RegistrationRepository;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.Citizenship;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Qualification;
import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantActivityType;
import zw.co.hitrac.council.business.domain.RegistrantStatus;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.DurationService;
import zw.co.hitrac.council.business.service.GeneralParametersService;

/**
 * @author Kelvin Goredema
 * @author Michael Matiashe
 */
@Repository
public class RegistrationDAOIpml implements RegistrationDAO {

    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    private RegistrationRepository registrationRepository;
    @Autowired
    private GeneralParametersService generalParametersService;
    @Autowired
    private DurationService durationService;

    public Registration save(Registration registration) {
        return registrationRepository.save(registration);
    }

    public List<Registration> getRegistrations(Registrant registrant) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registration.class);
        criteria.add(Restrictions.eq("registrant", registrant));
        criteria.addOrder(Order.asc("registrationDate"));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<Registration> findAll() {
        return registrationRepository.findAll();
    }

    public Registration get(Long id) {
        return registrationRepository.findOne(id);
    }

    public List<Registration> getRegistrations(Course course, Register register, Institution institution) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registration.class);
        if (course != null) {
            criteria.add(Restrictions.eq("course", course));
        }
        if (register != null) {
            criteria.add(Restrictions.eq("register", register));
        }
        if (institution != null) {
            criteria.add(Restrictions.eq("institution", institution));
        }
        criteria.addOrder(Order.desc("registrationDate"));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<Registration> getRegistrationsAll(Course course, Register register, Registrant registrant) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registration.class);
        criteria.add(Restrictions.eq("course", course));
        criteria.add(Restrictions.eq("register", register));
        criteria.add(Restrictions.eq("registrant", registrant));
        criteria.addOrder(Order.desc("registrationDate"));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public Integer getActiveRegistrations(Course course, Register register) {
        Long l = (Long) entityManager.createQuery("SELECT COUNT(r) from Registration r where r.register=:register and r.course=:course and deRegistrationDate IS NULL and r.registrant.dead=:dead").setParameter("register", register).setParameter("course", course).setParameter("dead", Boolean.FALSE).getSingleResult();
        return l.intValue();
    }

    public Integer getSuspendedRegistrations(Course course, Register register) {
        Long l = (Long) entityManager.createQuery("SELECT COUNT(r) from Registration r where r.register=:register and r.course=:course and deRegistrationDate IS NULL and r.registrant.dead=:dead and r.registrantStatus=:registrantStatus").setParameter("register", register).setParameter("course", course).setParameter("dead", Boolean.FALSE).setParameter("registrantStatus", generalParametersService.get().getSuspendedStatus()).getSingleResult();
        return l.intValue();
    }

    public List<Registrant> getRegistrants(Course course, Register register, Institution institution, String searchText, String gender, Citizenship citizenship, Date startDate, Date endDate) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registration.class);
        criteria.add(Restrictions.isNull("deRegistrationDate"));
        if (course != null) {
            criteria.add(Restrictions.eq("course", course));
        }
        if (register != null) {
            criteria.add(Restrictions.eq("register", register));
        }
        if (institution != null) {
            criteria.add(Restrictions.eq("institution", institution));
        }
        if (startDate != null && endDate != null) {
            criteria.add(Restrictions.between("registrationDate", startDate, endDate));
        } else if (startDate != null && endDate == null) {
            criteria.add(Restrictions.between("registrationDate", startDate, new Date()));
        }
        Criteria r = criteria.createCriteria("registrant");
        if (searchText != null && searchText.length() > 2) {
            //r.add(Restrictions.eq("dead", Boolean.FALSE));
            r.createAlias("customerAccount", "c");
            r.add(Restrictions.or(Restrictions.like("firstname", searchText, MatchMode.START), Restrictions.or(Restrictions.like("lastname", searchText, MatchMode.START), Restrictions.or(Restrictions.like("middlename", searchText,
                    MatchMode.START), Restrictions.eq("idNumber", searchText)), Restrictions.or(Restrictions.eq("c.code", searchText), Restrictions.eq("registrationNumber", searchText)))));
            r.setMaxResults(60);
        }
        if (gender != null) {
            r.add(Restrictions.eq("gender", gender));
        }
        if (citizenship != null) {
            r.add(Restrictions.eq("citizenship", citizenship));
        }
        criteria.addOrder(Order.desc("registrationDate"));
        Set<Registrant> registrants = new HashSet<Registrant>();
        for (Registration registration : (List<Registration>) criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list()) {
            registrants.add(registration.getRegistrant());
        }
        return new ArrayList<Registrant>(registrants);
    }

    public List<RegistrantData> getRegistrantList(Course course, Register register, Institution institution, RegistrantStatus registrantStatus, String searchText, String gender, Citizenship citizenship, Date startDate, Date endDate) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registration.class);
        criteria.add(Restrictions.isNull("deRegistrationDate"));
        if (course != null) {
            criteria.add(Restrictions.eq("course", course));
        }
        if (register != null) {
            criteria.add(Restrictions.eq("register", register));
        }
        if (institution != null) {
            criteria.add(Restrictions.eq("institution", institution));
        }
        if (registrantStatus != null) {
            criteria.add(Restrictions.eq("registrantStatus", registrantStatus));
        }
        if (startDate != null && endDate != null) {
            criteria.add(Restrictions.between("registrationDate", startDate, endDate));
        } else if (startDate != null && endDate == null) {
            criteria.add(Restrictions.between("registrationDate", startDate, new Date()));
        }
        criteria.createAlias("registrant", "r");
        if (searchText != null && searchText.length() > 2) {
            //r.add(Restrictions.eq("dead", Boolean.FALSE));
            criteria.add(Restrictions.or(Restrictions.like("r.firstname", searchText, MatchMode.START), Restrictions.or(Restrictions.like("r.lastname", searchText, MatchMode.START), Restrictions.or(Restrictions.like("r.middlename", searchText,
                    MatchMode.START), Restrictions.eq("r.idNumber", searchText)), Restrictions.or(Restrictions.eq("r.placeOfBirth", searchText), Restrictions.eq("r.registrationNumber", searchText)))));
        }
        if (gender != null) {
            criteria.add(Restrictions.eq("r.gender", gender));
        }
        if (citizenship != null) {
            criteria.add(Restrictions.eq("r.citizenship", citizenship));
        }
        criteria.addOrder(Order.desc("registrationDate"));
        criteria.createAlias("register", "q");
        criteria.createAlias("course", "c");
        ProjectionList proList = Projections.projectionList();
        proList.add(Projections.property("r.id"), "id");
        proList.add(Projections.property("r.firstname"), "firstname");
        proList.add(Projections.property("r.lastname"), "lastname");
        proList.add(Projections.property("r.middlename"), "middlename");
        proList.add(Projections.property("r.registrationNumber"), "registrationNumber");
        proList.add(Projections.property("r.idNumber"), "idNumber");
        proList.add(Projections.property("q.name"), "registerName");
        proList.add(Projections.property("c.name"), "courseName");
        proList.add(Projections.property("registrationDate"), "registrationDate");


        criteria.setProjection(proList);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        criteria.setResultTransformer(Transformers.aliasToBean(RegistrantData.class));
        return criteria.list();
    }

    public List<Registration> getRegistrations(Course course, Register register, Institution institution, String searchText, String gender, Citizenship citizenship) {
        List<Registration> list = new ArrayList<Registration>();

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registration.class);
        criteria.add(Restrictions.isNull("deRegistrationDate"));
        if (course != null) {
            criteria.add(Restrictions.eq("course", course));
        }
        if (register != null) {
            criteria.add(Restrictions.eq("register", register));
        }
        if (institution != null) {
            criteria.add(Restrictions.eq("institution", institution));
        }
        criteria.createAlias("registrant", "r");
        if (searchText != null && searchText.length() > 2) {
            criteria.add(Restrictions.or(Restrictions.like("r.firstname", searchText, MatchMode.START), Restrictions.or(Restrictions.like("r.lastname", searchText, MatchMode.START), Restrictions.or(Restrictions.like("r.middlename", searchText,
                    MatchMode.START), Restrictions.eq("r.idNumber", searchText)), Restrictions.or(Restrictions.eq("r.placeOfBirth", searchText), Restrictions.eq("r.registrationNumber", searchText)))));
        }
        if (gender != null) {
            criteria.add(Restrictions.eq("r.gender", gender));
        }
        if (citizenship != null) {
            criteria.add(Restrictions.eq("r.citizenship", citizenship));
        }
        criteria.addOrder(Order.desc("registrationDate"));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<Registration> getActiveRegistrations(Registrant registrant) {
        return entityManager.createQuery("select r from Registration r where r.registrant=:registrant and r.deRegistrationDate is null order by r.registrationDate desc ").setParameter("registrant", registrant).getResultList();
    }

    public Registration getCurrentRegistrationByRegister(Register register, Registrant registrant) {
        List<Registration> registrations = entityManager.createQuery("select r from Registration r where r.register=:register and r.registrant=:registrant and r.deRegistrationDate IS NULL").setParameter("register", register).setParameter("registrant", registrant).getResultList();
        if (registrations.isEmpty()) {
            return null;
        } else {
            return registrations.get(0);
        }
    }

    public Boolean hasCurrentRegistration(Registrant registrant) {
        List<Registration> list = entityManager.createQuery("select r from Registration r where r.registrant=:registrant and r.deRegistrationDate is null").setParameter("registrant", registrant).getResultList();
        if (list.isEmpty()) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }
    }

    public Registration getCurrentRegistrationByCourseAndRegister(Registrant registrant, Register register, Course course) {
        List<Registration> registrations = entityManager.createQuery("select r from Registration r where r.register=:register and r.registrant=:registrant and r.course=:course and r.deRegistrationDate IS NULL").setParameter("register", register).setParameter("registrant", registrant).setParameter("course", course).getResultList();
        if (registrations.isEmpty()) {
            return null;
        } else {
            return registrations.get(0);
        }

    }

    public Registration getCurrentRegistration(Registrant registrant) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Registration.class);
        criteria.add(Restrictions.eq("registrant", registrant));
        criteria.add(Restrictions.isNull("deRegistrationDate"));
        List<Registration> list = criteria.list();
        if (list.isEmpty()) {
            return null;
        } else {
            return (Registration) list.get(0);
        }
    }

    public Registration getCurrentRegistration(Institution institution) {
        List<Registration> registrations = entityManager.createQuery("select r from Registration r where r.institution=:institution and r.registrant IS NULL and r.deRegistrationDate IS NULL").setParameter("institution", institution).getResultList();
        if (registrations.isEmpty()) {
            return null;
        } else {
            return registrations.get(0);
        }
    }

    public Registration getRegistrationByApplication(Application application) {
        List<Registration> registrations = entityManager.createQuery("select r from Registration r where r.application=:application").setParameter("application", application).getResultList();
        if (registrations.isEmpty()) {
            return null;
        } else {
            return registrations.get(0);
        }
    }

    public Boolean getRegistrationIsDeRegistered(Registrant registrant, RegistrantStatus registrantStatus) {
        List<Registration> registrations = entityManager.createQuery("select r from Registration r where r.deRegistrationDate is not null and r.registrant=:registrant and r.registrantStatus=:registrantStatus").setParameter("registrant", registrant).setParameter("registrantStatus", registrantStatus).getResultList();
        if (registrations.isEmpty()) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }
    }

    public List<Registration> getActiveRegistrationByRegister(Course course, Register register) {
        return entityManager.createQuery("select r from Registration r where r.course=:course and r.register=:register and r.registrant.dead=FALSE and r.deRegistrationDate IS NULL").setParameter("course", course).setParameter("register", register).getResultList();
    }


    public List<Registration> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Course> getDistinctCourseFromRegistration() {
        return entityManager.createQuery("select DISTINCT course from Registration").getResultList();
    }

    public List<Registration> getActiveRegistrations(Course course) {
        return entityManager.createQuery("SELECT r FROM Registration r WHERE r.course=:course and r.deRegistrationDate IS NULL").setParameter("course", course).getResultList();
    }

    public List<Registration> getActiveRegistrationsByCourseAndCouncilDuration(Course course, CouncilDuration councilDuration) {
        Duration duration = durationService.getDurationByCourseAndCouncilDuration(course, councilDuration);
        return entityManager.createQuery("SELECT r from Registration r left join fetch r.course WHERE r.course=:course AND r.deRegistrationDate IS NULL AND r.registrant IN (SELECT ra.registrant FROM RegistrantActivity ra where ra.duration=:duration and ra.registrantActivityType=:at)").setParameter("course", course).setParameter("at", RegistrantActivityType.RENEWAL).setParameter("duration", duration).getResultList();
    }

    @Override
    public List<Registration> getActiveRegistrations(String idList) {
        return entityManager.createNativeQuery("select * from registration WHERE deRegistrationDate IS NULL AND registrant_id IN (?)", Registration.class).setParameter(1, idList).getResultList();
    }

    public List<Registration> getRegisteredInstitutionsRegistrations(Boolean closed) {
        if (closed) {
            return entityManager.createQuery("SELECT r FROM Registration r WHERE r.register=:register and r.deRegistrationDate is not null and r.institution.institutionType=:institutionType order by r.registrationDate")
                    .setParameter("register", generalParametersService.get().getInstitutionRegister())
                    .setParameter("institutionType", generalParametersService.get().getHealthInstitutionType())
                    .getResultList();
        } else {
            return entityManager.createQuery("SELECT r FROM Registration r WHERE r.register=:register and r.deRegistrationDate is null and r.institution.institutionType=:institutionType order by r.registrationDate")
                    .setParameter("register", generalParametersService.get().getInstitutionRegister())
                    .setParameter("institutionType", generalParametersService.get().getHealthInstitutionType())
                    .getResultList();
        }

    }

    public boolean hasBeenRegisteredBeforeCouncilDuration(CouncilDuration councilDuration, Long registrantId) {
        Long count = (Long) entityManager.createQuery("SELECT count(r) FROM Registration r WHERE r.register <>:register and FUNCTION('YEAR', r.registrationDate) <:year and r.registrant.id =:registrantId")
                .setParameter("registrantId", registrantId)
                .setParameter("register", generalParametersService.get().getStudentRegister())
                .setParameter("year", Integer.valueOf(councilDuration.getName().trim()))
                .getSingleResult();
        return count > 0;
    }

    public List<Long> getRegistrantIdsRegisteredBeforeCouncilDuration(CouncilDuration councilDuration) {
        List<Long> ids = entityManager.createQuery("SELECT distinct r.registrant.id FROM Registration r WHERE r.register <>:register and FUNCTION('YEAR', r.registrationDate) <:year")
                .setParameter("register", generalParametersService.get().getStudentRegister())
                .setParameter("year", Integer.valueOf(councilDuration.getName().trim()))
                .getResultList();
        return ids;
    }

}
