package zw.co.hitrac.council.business.domain.accounts;

import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.domain.BaseEntity;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Duration;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.math.BigDecimal;

/**
 *
 * @author Michael Matiashe
 * @author Charles Chigoriwa
 */
@Entity
@Table(name="receiptitem")
@Audited
public class ReceiptItem extends BaseEntity {

    private BigDecimal amount;
    private ReceiptHeader receiptHeader;
    private DebtComponent debtComponent;
    private Long code;
    private int quantity;
    private CouncilDuration paymentPeriod;

    @ManyToOne
    public CouncilDuration getPaymentPeriod() {
        return paymentPeriod;
    }

    public void setPaymentPeriod(CouncilDuration paymentPeriod) {
        this.paymentPeriod = paymentPeriod;
    }
    
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @ManyToOne(optional = true)
    public ReceiptHeader getReceiptHeader() {
        return receiptHeader;
    }

    public void setReceiptHeader(ReceiptHeader receiptHeader) {
        this.receiptHeader = receiptHeader;
    }

    @ManyToOne
    public DebtComponent getDebtComponent() {
        return debtComponent;
    }

    public void setDebtComponent(DebtComponent debtComponent) {
        this.debtComponent = debtComponent;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    @Transient
    public String getCodeValue() {
        if (code == null) {
            return "";
        }
        return String.valueOf(code);
    }
}
