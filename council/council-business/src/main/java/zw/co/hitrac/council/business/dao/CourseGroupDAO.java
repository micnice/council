package zw.co.hitrac.council.business.dao;

import zw.co.hitrac.council.business.domain.CourseGroup;

/**
 *
 * @author Charles Chigoriwa
 */
public interface CourseGroupDAO extends IGenericDAO<CourseGroup> {
    
}
