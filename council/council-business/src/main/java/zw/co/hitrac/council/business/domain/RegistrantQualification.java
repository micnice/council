package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Michael Matiashe
 */
@Entity
@Table(name = "registrantqualification")
@Audited
@XmlRootElement
public class RegistrantQualification extends BaseIdEntity implements Serializable {

    private Registrant registrant;
    private Qualification qualification;
    private Course course;
    private Institution institution;
    private Date dateAwarded;
    private Institution awardingInstitution;
    private String certificateNumber;
    private Date startDate; //expected training start date
    private Date EndDate; // expected training end date
    public static String NO_QUALIIFCATION = "NO QUALIFICATION";
    private QualificationType qualificationType;

    /**
     *
     * @return
     */
    @OneToOne
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public RegistrantQualification() {
    }

    public RegistrantQualification(Registrant registrant, Qualification qualification) {
        this.registrant = registrant;
        this.qualification = qualification;
    }

    public String getCertificateNumber() {
        return certificateNumber;
    }

    public void setCertificateNumber(String certificateNumber) {
        this.certificateNumber = certificateNumber;
    }

    @ManyToOne
    public Registrant getRegistrant() {
        return registrant;
    }

    public void setRegistrant(Registrant registrant) {
        this.registrant = registrant;
    }

    @OneToOne
    public Qualification getQualification() {
        return qualification;
    }

    public void setQualification(Qualification qualification) {
        this.qualification = qualification;
    }

    @OneToOne
    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDateAwarded() {
        return dateAwarded;
    }

    public void setDateAwarded(Date dateAwarded) {
        this.dateAwarded = dateAwarded;
    }

    @OneToOne
    public Institution getAwardingInstitution() {
        return awardingInstitution;
    }

    public void setAwardingInstitution(Institution awardingInstitution) {
        this.awardingInstitution = awardingInstitution;
    }

    @Enumerated(EnumType.STRING)
    public QualificationType getQualificationType() {
        return qualificationType;
    }

    public void setQualificationType(QualificationType qualificationType) {
        this.qualificationType = qualificationType;
    }

    @Override
    public String toString() {
        if (qualification != null) {
            return getQualification().getName();
        } else {
            return "";
        }
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getEndDate() {
        return EndDate;
    }

    public void setEndDate(Date EndDate) {
        this.EndDate = EndDate;
    }

    @XmlElement
    @Transient
    public String getFullDetail() {
        return getQualification().getName() + " " + getDateAwarded();
    }

}
