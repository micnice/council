package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantCpd;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Constance Mabaso
 */
public interface RegistrantCpdService extends IGenericService<RegistrantCpd> {
    BigDecimal getCurrentCPDs(Registrant registrant, Duration duration);

    List<RegistrantCpd> getCurrentCPDsList(Registrant registrant, Duration duration);

    List<RegistrantCpd> getCurrentCPDsList(Registrant registrant);

    BigDecimal getCurrentCPDs(Registrant registrant, CouncilDuration duration);

    List<RegistrantCpd> getCPDsList(Registrant registrant);

    List<RegistrantData> getPaidAnnualFeeAndInadequateCPDS(CouncilDuration duration);

}
