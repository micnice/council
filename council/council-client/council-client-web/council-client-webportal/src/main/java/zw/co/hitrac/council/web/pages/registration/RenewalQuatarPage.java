package zw.co.hitrac.council.web.pages.registration;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.process.InvoiceProcess;
import zw.co.hitrac.council.business.process.PaymentProcess;
import zw.co.hitrac.council.business.process.RenewalProcess;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.business.service.accounts.ReceiptItemService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import java.util.*;

/**
 * @author Takunda Dhlakama
 * @author Michael Matiashe
 */
public class RenewalQuatarPage extends TemplatePage {

    @SpringBean
    private DebtComponentService debtComponentService;
    @SpringBean
    private RenewalProcess renewalProcess;
    @SpringBean
    private CouncilDurationService councilDurationService;
    @SpringBean
    private DurationService durationService;
    @SpringBean
    private RegistrationService registrationService;
    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private InvoiceProcess invoiceProcess;
    @SpringBean
    private PaymentProcess paymentProcess;
    @SpringBean
    private ReceiptItemService receiptItemService;
    @SpringBean
    GeneralParametersService generalParametersService;

    private Course course;
    private Boolean notAccrued = Boolean.FALSE;

    private List<Duration> durations = new ArrayList<Duration>();


    public RenewalQuatarPage(final IModel<Registrant> registrantModel) {

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());
        add(new RegistrantPanel("registrantPanel", registrantModel.getObject().getId()));
        PropertyModel<Course> courseModel = new PropertyModel<Course>(this, "course");
        Form<?> form = new Form("form");

        IModel<List<Duration>> durationsModel = new LoadableDetachableModel<List<Duration>>() {
            @Override
            protected List<Duration> load() {
                durations = durationService.findAll();
                Collections.sort(durations, (d1, d2) -> d2.getEndDate().compareTo(d1.getEndDate()));
                return durations;
            }
        };

        Set<Course> courses = new HashSet<Course>();
        for (Registration r : registrationService.getActiveRegistrations(registrantModel.getObject())) {
            courses.add(r.getCourse());
        }

        form.add(new DropDownChoice("course", courseModel, new ArrayList<Course>(courses)));

        CheckGroup<Duration> selectedDurations = new CheckGroup<Duration>("durations", this.durations);
        form.add(selectedDurations);

        PropertyListView<Duration> eachItem = new PropertyListView<Duration>("eachItem", durationsModel) {
            @Override
            protected void populateItem(final ListItem<Duration> item) {

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }


                item.add(new Label("name", item.getModel().getObject().getName()));
           item.add(new Check("selected", item.getModel()));

            }
        };

        selectedDurations.add(eachItem);



        form.add(new Button("process") {
            @Override
            public void onSubmit() {
                try {
                    renewalProcess.renew(registrantModel.getObject(), durations, course);
            } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        });

        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });

        // method to add a new RegistrantCpd - Id is null
        form.add(new Button("registrantCpdPage") {
            @Override
            public void onSubmit() {
                setResponsePage(new RegistrantCpdPage(registrantModel));
            }

            @Override
            protected void onConfigure() {
                setVisible(!generalParametersService.get().getCaptureCPDsDirectlyAndInDirectly());
            }
        });


        add(new FeedbackPanel("feedback"));
        add(form);

    }
}


//~ Formatted by Jindent --- http://www.jindent.com
