/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.EmploymentAreaDAO;
import zw.co.hitrac.council.business.dao.repo.EmploymentAreaRepository;
import zw.co.hitrac.council.business.domain.EmploymentArea;

/**
 *
 * @author kelvin
 */
@Repository
public class EmploymentAreaDAOImpl implements EmploymentAreaDAO{
@Autowired
   private EmploymentAreaRepository employmentAreaRepository;
    
   public EmploymentArea save(EmploymentArea employmentArea){
       return employmentAreaRepository.save(employmentArea);
   }
   
   public List<EmploymentArea> findAll(){
       return employmentAreaRepository.findAll();
   }
   
   public EmploymentArea get(Long id){
       return employmentAreaRepository.findOne(id);
   }
 /**
     * A setter method that will make mocking repo object easier
     * @param employmentAreaRepository 
     */
   
   public void setEmploymentAreaRepository(EmploymentAreaRepository employmentAreaRepository) {
         this.employmentAreaRepository = employmentAreaRepository;
    }

    public List<EmploymentArea> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}