package zw.co.hitrac.council.business.utils;

/**
 *
 * @author Charles Chigoriwa
 */
public class CouncilException  extends RuntimeException{

    public CouncilException() {
    }

    public CouncilException(String message) {
        super(message);
    }

    public CouncilException(String message, Throwable cause) {
        super(message, cause);
    }

    public CouncilException(Throwable cause) {
        super(cause);
    }
    
    
    
}
