/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.models;

import java.util.List;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.District;
import zw.co.hitrac.council.business.domain.Province;
import zw.co.hitrac.council.business.service.DistrictService;
import zw.co.hitrac.council.business.utils.HrisComparator;

/**
 *
 * @author kelvin
 */
public class DistrictListModel extends LoadableDetachableModel<List<District>> {

    private final DistrictService districtService;
    private Province province;
    private HrisComparator hrisComparator = new HrisComparator();

    public DistrictListModel(DistrictService districtService) {
        this.districtService = districtService;
    }

    public DistrictListModel(DistrictService districtService, Province province) {
        this.districtService = districtService;
        this.province = province;
    }

    @Override
    protected List<District> load() {
        if (province == null) {
            return (List<District>) hrisComparator.sort(districtService.findAll());
        }
        return (List<District>) hrisComparator.sort(districtService.getDistrictsInProvince(province));
    }
}
