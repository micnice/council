package zw.co.hitrac.council.web.models;

import java.util.List;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.TransferRule;
import zw.co.hitrac.council.business.service.TransferRuleService;

/**
 *
 * @author Michael Matiashe
 */
public class TransferRuleListModel extends LoadableDetachableModel<List<TransferRule>> {
    
    private final TransferRuleService transferRuleService;

    public TransferRuleListModel(TransferRuleService transferRuleService) {
        this.transferRuleService = transferRuleService;
    }
    
    

    @Override
    protected List<TransferRule> load() {
      return this.transferRuleService.findAll();
    }
    
}
