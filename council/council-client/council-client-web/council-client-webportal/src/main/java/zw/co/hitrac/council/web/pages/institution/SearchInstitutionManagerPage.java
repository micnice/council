package zw.co.hitrac.council.web.pages.institution;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;

//~--- JDK imports ------------------------------------------------------------
import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PageableListView;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigator;
import org.apache.wicket.model.CompoundPropertyModel;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;

/**
 * @author Takunda Dhlakama
 */
public class SearchInstitutionManagerPage extends TemplatePage {

    private String searchtxt;
    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private InstitutionService institutionService;

    public SearchInstitutionManagerPage(final Long id) {

        final CompoundPropertyModel<Institution> institutionCompoundPropertyModel = new CompoundPropertyModel<Institution>(new LoadableDetachableInstitutionModel(id));

        setDefaultModel(institutionCompoundPropertyModel);
        add(new Label("name"));

        PropertyModel<String> messageModel = new PropertyModel<String>(this, "searchtxt");
        Form<?> form = new Form("form");

        form.add(new TextField<String>("searchtxt", messageModel).setRequired(true).add(new ErrorBehavior()));
        form.add(new Link<Institution>("institutionViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionViewPage(institutionCompoundPropertyModel.getObject().getId()));
            }
        });
        add(form);

        IModel<List<RegistrantData>> model2 = new LoadableDetachableModel<List<RegistrantData>>() {
            @Override
            protected List<RegistrantData> load() {
                if ((searchtxt == null) || searchtxt.equals("")) {
                    return new ArrayList<RegistrantData>();
                }
                if (searchtxt.length() > 2) {
                    return registrantService.getRegistrantList(searchtxt, null, null, null);
                }
                return new ArrayList<RegistrantData>();
            }
        };

        PageableListView<RegistrantData> eachItem = new PageableListView<RegistrantData>("eachItem", model2, 20) {
            @Override
            protected void populateItem(ListItem<RegistrantData> item) {

                item.add(new Link<RegistrantData>("addManager", item.getModel()) {
                    @Override
                    public void onClick() {
                        try {
                            Registrant newSupervisor = registrantService.get(getModelObject().getId());
                            final CompoundPropertyModel<Registrant> registrantCompoundPropertyModel = new CompoundPropertyModel<Registrant>(new DetachableRegistrantModel(newSupervisor.getId(), registrantService));

                            setResponsePage(new InstitutionManagerEditPage(null, institutionCompoundPropertyModel, registrantCompoundPropertyModel));
                        } catch (CouncilException e) {
                            error(e.getMessage());
                        }
                    }

                });

                if (item.getIndex()
                        % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(
                        new Label("lastname", item.getModelObject().getLastname()));

                item.add(
                        new Label("firstname", item.getModelObject().getFirstname()));
                item.add(
                        new Label("middlename", item.getModelObject().getMiddlename()));
                item.add(
                        new Label("idNumber", item.getModelObject().getIdNumber()));
                item.add(
                        new Label("registrationNumber", item.getModelObject().getRegistrationNumber()));
                item.add(
                        new Label("gender", item.getModelObject().getGender()));

            }
        };

        add(new PagingNavigator("navigator", eachItem));
        add(eachItem);
        add(new FeedbackPanel("feedback"));
    }

    private final class LoadableDetachableInstitutionModel extends LoadableDetachableModel<Institution> {

        private Long id;

        public LoadableDetachableInstitutionModel(Long id) {
            this.id = id;
        }

        @Override
        protected Institution load() {
            return institutionService.get(id);
        }
    }
}//~ Formatted by Jindent --- http://www.jindent.com
