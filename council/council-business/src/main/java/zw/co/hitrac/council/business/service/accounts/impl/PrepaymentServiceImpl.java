package zw.co.hitrac.council.business.service.accounts.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.accounts.PrepaymentDAO;
import zw.co.hitrac.council.business.dao.repo.accounts.PrepaymentRepository;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.accounts.Prepayment;
import zw.co.hitrac.council.business.process.BillingProcess;
import zw.co.hitrac.council.business.process.ProductFinder;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.service.accounts.PrepaymentService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author Michael Matiashe
 */
@Service
@Transactional
public class PrepaymentServiceImpl implements PrepaymentService {

    @Autowired
    BillingProcess billingProcess;
    @Autowired
    private PrepaymentDAO prepaymentDAO;
    @Autowired
    private PrepaymentRepository prepaymentRepository;
    @Autowired
    private RegistrantService registrantService;
    @Autowired
    private RegistrationService registrationService;
    @Autowired
    private ProductFinder productFinder;
    @Autowired
    private RegistrationProcess registrationProcess;
    @Autowired
    private GeneralParametersService generalParametersService;

    @Transactional
    public Prepayment save(Prepayment t) {
        return prepaymentDAO.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Prepayment> findAll() {
        return prepaymentDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Prepayment get(Long id) {
        return prepaymentDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Prepayment> findAll(Boolean retired) {
        return prepaymentRepository.findAll();
    }

    public void setPrepaymentDao(PrepaymentDAO prepaymentDAO) {

        this.prepaymentDAO = prepaymentDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Prepayment> findByInstitution(Institution institution) {
        return prepaymentDAO.findByInstitution(institution);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Prepayment> findByCanceled(Boolean canceled) {
        return prepaymentDAO.findByCanceled(canceled);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Prepayment> findAvailablePrepayments() {

        return prepaymentDAO.findAvailablePrepayments();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Prepayment> findAll(Date endDate) {
        return prepaymentDAO.findAll(endDate);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public BigDecimal getPrepaymentBalance(Date endDate, Prepayment prepayment) {
        return prepaymentDAO.getPrepaymentBalance(endDate, prepayment);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Institution> withPrepayments() {
        return prepaymentDAO.withPrepayments();
    }

}
