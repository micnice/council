package zw.co.hitrac.council.business.domain.accounts;

import org.apache.commons.lang.BooleanUtils;
import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.domain.BaseIdEntity;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 *
 * @author Kelvin Goredema
 * @author Michael Matiashe
 */
@Entity
@Table(name = "accountsparameters")
@Audited
public class AccountsParameters extends BaseIdEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    private TransactionType invoiceTransactionType;
    private PaymentMethod defaultPaymentMethod;
    private PaymentMethod manualPaymentMethod;
    private TransactionType creditNoteTransactionType;
    private TransactionType refundTransactionType;
    private TransactionType manualTransactionType;
    private TransactionType carryForwardTransactionType;
    private Boolean studentRegistrationPayment = Boolean.FALSE;
    private Boolean qualificationFee = Boolean.FALSE;
    private Boolean registrationApplicationPayment = Boolean.FALSE;
    private Boolean practisingCertificatePayment = Boolean.FALSE;
    private Integer payingPractitionersMaximumAge = 0;
    private Boolean payingMaximumAge = Boolean.FALSE;
    private Boolean printPDFReceipt = Boolean.FALSE; //Default PDF else Prints Word
    private Boolean allowCashOverpayment = Boolean.FALSE;
    private Boolean allowAccruals = Boolean.FALSE;
    private Boolean billEachPostBasicQualification = Boolean.FALSE;
    private Boolean printJavaScriptReceipt = Boolean.FALSE;
    private Boolean printReceiptNumberOnCertificate = Boolean.FALSE;

    public Integer getPayingPractitionersMaximumAge() {
        if (payingPractitionersMaximumAge == null) {
            //return current country default 
            return 65;
        }
        return payingPractitionersMaximumAge;
    }

    public void setPayingPractitionersMaximumAge(Integer payingPractitionersMaximumAge) {
        this.payingPractitionersMaximumAge = payingPractitionersMaximumAge;
    }

    public Boolean getPayingMaximumAge() {
        if (payingMaximumAge == null) {
            return Boolean.FALSE;
        }
        return payingMaximumAge;
    }

    public void setPayingMaximumAge(Boolean payingMaximumAge) {
        this.payingMaximumAge = payingMaximumAge;
    }

    @Transient
    public String getPractisingCertificatePaymentStatus() {
        if (practisingCertificatePayment == null) {
            return "";
        } else {
            return practisingCertificatePayment ? "YES" : "NO";
        }
    }

    @Transient
    public String getRegistrationApplicationPaymentStatus() {
        if (registrationApplicationPayment == null) {
            return "";
        } else {
            return registrationApplicationPayment ? "YES" : "NO";
        }
    }

    public Boolean getStudentRegistrationPayment() {
        return BooleanUtils.toBoolean(studentRegistrationPayment);
    }

    public void setStudentRegistrationPayment(Boolean studentRegistrationPayment) {
        this.studentRegistrationPayment = studentRegistrationPayment;
    }

    public Boolean getQualificationFee() {
        return BooleanUtils.toBoolean(qualificationFee);
    }

    public void setQualificationFee(Boolean qualificationFee) {
        this.qualificationFee = qualificationFee;
    }

    public Boolean getPractisingCertificatePayment() {
        return BooleanUtils.toBoolean(practisingCertificatePayment);
    }

    public void setPractisingCertificatePayment(Boolean practisingCertificatePayment) {
        this.practisingCertificatePayment = practisingCertificatePayment;
    }

    public Boolean getRegistrationApplicationPayment() {
        return BooleanUtils.toBoolean(registrationApplicationPayment);
    }

    public void setRegistrationApplicationPayment(Boolean registrationApplicationPayment) {
        this.registrationApplicationPayment = registrationApplicationPayment;
    }

    public Boolean getPrintPDFReceipt() {
        return BooleanUtils.toBoolean(printPDFReceipt);
    }

    public void setPrintPDFReceipt(Boolean printPDFReceipt) {
        this.printPDFReceipt = printPDFReceipt;
    }

    public Boolean isAllowCashOverpayment() {
        return BooleanUtils.toBoolean(allowCashOverpayment);
    }

    public void setAllowCashOverpayment(Boolean allowCashOverpayment) {
        this.allowCashOverpayment = allowCashOverpayment;
    }

    public Boolean getPrintJavaScriptReceipt() {
        return BooleanUtils.toBoolean(printJavaScriptReceipt);
    }

    public void setPrintJavaScriptReceipt(Boolean printJavaScriptReceipt) {
        this.printJavaScriptReceipt = printJavaScriptReceipt;
    }

    public Boolean getBillEachPostBasicQualification() {
        return BooleanUtils.toBoolean(billEachPostBasicQualification);
    }

    public void setBillEachPostBasicQualification(Boolean billEachPostBasicQualification) {
        this.billEachPostBasicQualification = billEachPostBasicQualification;
    }

    public Boolean isAllowAccruals() {
        return BooleanUtils.toBoolean(allowAccruals);
    }

    public void setAllowAccruals(Boolean allowAccruals) {
        this.allowAccruals = allowAccruals;
    }

    @OneToOne
    public TransactionType getCarryForwardTransactionType() {
        return carryForwardTransactionType;
    }

    public void setCarryForwardTransactionType(TransactionType carryForwardTransactionType) {
        this.carryForwardTransactionType = carryForwardTransactionType;
    }

    @OneToOne
    public TransactionType getRefundTransactionType() {
        return refundTransactionType;
    }

    public void setRefundTransactionType(TransactionType refundTransactionType) {
        this.refundTransactionType = refundTransactionType;
    }

    @OneToOne
    public TransactionType getCreditNoteTransactionType() {
        return creditNoteTransactionType;
    }

    public void setCreditNoteTransactionType(TransactionType creditNoteTransactionType) {
        this.creditNoteTransactionType = creditNoteTransactionType;
    }

    @OneToOne
    public TransactionType getInvoiceTransactionType() {
        return invoiceTransactionType;
    }

    public void setInvoiceTransactionType(TransactionType invoiceTransactionType) {
        this.invoiceTransactionType = invoiceTransactionType;
    }

    @OneToOne
    public PaymentMethod getDefaultPaymentMethod() {
        return defaultPaymentMethod;
    }

    public void setDefaultPaymentMethod(PaymentMethod defaultPaymentMethod) {
        this.defaultPaymentMethod = defaultPaymentMethod;
    }

    @OneToOne
    public PaymentMethod getManualPaymentMethod() {
        return manualPaymentMethod;
    }

    public void setManualPaymentMethod(PaymentMethod manualPaymentMethod) {
        this.manualPaymentMethod = manualPaymentMethod;
    }

    @OneToOne
    public TransactionType getManualTransactionType() {
        return manualTransactionType;
    }

    public void setManualTransactionType(TransactionType manualTransactionType) {
        this.manualTransactionType = manualTransactionType;
    }

    public Boolean getPrintReceiptNumberOnCertificate() {
        return BooleanUtils.toBoolean(printReceiptNumberOnCertificate);
    }

    public void setPrintReceiptNumberOnCertificate(Boolean printReceiptNumberOnCertificate) {
        this.printReceiptNumberOnCertificate = printReceiptNumberOnCertificate;
    }
}
