/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.RegistrantContactDAO;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.RegistrantContactService;

import java.util.List;

/**
 *
 * @author kelvin
 * @author morris baradza
 */
@Service
@Transactional
public class RegistrantContactServiceImpl implements RegistrantContactService {

    @Autowired
    private RegistrantContactDAO registrantContactDAO;

    @Transactional
    public RegistrantContact save(RegistrantContact registrantContact) {
        return registrantContactDAO.save(registrantContact);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantContact> findAll() {
        return registrantContactDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantContact get(Long id) {
        return registrantContactDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantContact> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setRegistrantContactDAO(RegistrantContactDAO registrantContactDAO) {
        this.registrantContactDAO = registrantContactDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantContact> getContacts(Registrant registrant, Institution institution) {
        return registrantContactDAO.getContacts(registrant, institution);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantContact> getMailContacts() {
        return registrantContactDAO.getMailContacts();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantContact> getContactsByContactType(Registrant registrant, ContactType contactType) {
        return registrantContactDAO.getContactsByContactType(registrant, contactType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantContact getActiveContactbyContactType(Registrant registrant, ContactType contactType) {
        return registrantContactDAO.getActiveContactbyContactType(registrant, contactType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getRegistrantDataAddress(ContactType contactType) {
        return registrantContactDAO.getRegistrantDataAddress(contactType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getRegistrantDataAddressCorrections(ContactType contactType) {
        return registrantContactDAO.getRegistrantDataAddressCorrections(contactType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getRegistrantDataContacts(ContactType contactType, Course course) {
        return registrantContactDAO.getRegistrantDataContacts(contactType, course);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getAllRegistrantDataAndContacts(ContactType contactType) {
        return registrantContactDAO.getAllRegistrantDataContacts( contactType);
    }
}
