package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.RegisterType;
import zw.co.hitrac.council.business.service.RegisterTypeService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Kelvin Goredema
 */
public class RegisterTypeEditPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private RegisterTypeService registerService;

    public RegisterTypeEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<RegisterType>(new LoadableDetachableRegisterTypeModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));

        Form<RegisterType> form = new Form<RegisterType>("form", (IModel<RegisterType>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                registerService.save(getModelObject());
                setResponsePage(new RegisterTypeViewPage(getModelObject().getId()));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("individualName").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("aliasName").setRequired(true).add(new ErrorBehavior()));        
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink", RegisterTypeListPage.class));
        add(form);
    }

    private final class LoadableDetachableRegisterTypeModel extends LoadableDetachableModel<RegisterType> {

        private Long id;

        public LoadableDetachableRegisterTypeModel(Long id) {
            this.id = id;
        }

        @Override
        protected RegisterType load() {
            if (id == null) {
                return new RegisterType();
            }

            return registerService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
