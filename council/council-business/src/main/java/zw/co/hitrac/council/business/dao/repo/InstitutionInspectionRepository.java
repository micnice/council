package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.InstitutionInspection;

/**
 *
 * @author Constance Mabaso
 */

    public interface InstitutionInspectionRepository extends CrudRepository<InstitutionInspection, Long> {
    public List<InstitutionInspection> findAll();
}


