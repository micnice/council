/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.registration;

import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.utility.CustomDateLabel;
import zw.co.hitrac.council.web.utility.CustomDateTimeLabel;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;

import java.util.*;

import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;

/**
 * @author tdhlakama
 */
public final class RegistrantSnapShot extends Panel {

    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private RegistrationService registrationService;
    @SpringBean
    private RegistrationProcess registrationProcess;
    @SpringBean
    private SupportDocumentService supportDocumentService;
    @SpringBean
    private EmploymentService employmentService;
    @SpringBean
    private InstitutionManagerService institutionManagerService;

    public RegistrantSnapShot(String id, final Long registrantId) {
        super(id);

        final CompoundPropertyModel<Registrant> model = new CompoundPropertyModel<Registrant>(new DetachableRegistrantModel(registrantId, registrantService));

        setDefaultModel(model);
        add(new Label("firstname"));
        add(new Label("lastname"));
        add(new Label("middlename"));
        add(new Label("maidenname"));
        add(new Label("gender"));
        add(new CustomDateLabel("birthDate"));
        add(new Label("idNumber"));
        add(new Label("identificationType"));
        add(new Label("registrationNumber"));
        add(new Label("placeOfBirth"));
        add(new Label("registrantQualificationList"));
        add(new Label("activeConditions"));
        add(new Label("createdBy") {

            @Override
            public boolean isVisible() {
                Boolean enabled = CouncilSession.get().getRoles().isEmpty();
                return !enabled;
            }

        }).setOutputMarkupId(true);

        add(new Label("modifiedBy"));
        add(CustomDateTimeLabel.forDate("dateCreated", new PropertyModel<>(model, "dateCreated")));
        add(CustomDateTimeLabel.forDate("dateModified", new PropertyModel<>(model, "dateModified")));

        IModel<List<InstitutionManager>> institutionManagerModelList = new LoadableDetachableModel<List<InstitutionManager>>() {

            @Override
            protected List<InstitutionManager> load() {

                return institutionManagerService.findInstitutionManagers(model.getObject(), null);
            }
        };

        add(new Label("institutionsSupervised", new LoadableDetachableModel<String>() {

            @Override
            protected String load() {
                String names = "";
                for (InstitutionManager institutionManager : institutionManagerModelList.getObject()) {
                    if (institutionManager.isInstitutionSupervisor() && institutionManager.getInstitutionSupervisorEndDate() == null) {
                        names = names + institutionManager.getInstitution().getName() + " - ";
                    }
                }
                if (names.isEmpty()) {
                    return "N/A";
                }
                names = names.substring(0, names.length() - 2);
                return names;
            }
        }));

        add(new Label("institutionsInCharge", new LoadableDetachableModel<String>() {

            @Override
            protected String load() {
                String names = "";
                for (InstitutionManager institutionManager : institutionManagerModelList.getObject()) {
                    if (institutionManager.isPractitionerInCharge() && institutionManager.getPractitionerInChargeEndDate() == null) {
                        names = names + institutionManager.getInstitution().getName() + " - ";
                    }
                }
                if (names.isEmpty()) {
                    return "N/A";
                }
                names = names.substring(0, names.length() - 2);
                return names;
            }

        }));

        String courseStatement = "";
        String registerStatement = "";
        String institutionStatement = "";
        String trainingStatement = "";
        Set<String> courses = new HashSet<String>();
        Set<String> registers = new HashSet<String>();
        Set<String> studentInfo = new HashSet<String>();
        for (Registration reg : registrationService.getActiveRegistrations(model.getObject())) {
            if (reg.getRegister() != null && !reg.getRegister().equals(generalParametersService.get().getStudentRegister())) {
                if (reg.getCourse() != null && reg.getCourse().getName() != null) {
                    courses.add(reg.getCourse().getName());
                }
            } else {
                if (reg.getInstitution() != null && reg.getCourse() != null) {
                    studentInfo.add(reg.getCourse() != null ? reg.getCourse().getName() : "" + " - " +
                            reg.getInstitution() != null ?
                            reg.getInstitution().getName() : "");

                }

            }
            registers.add(reg.getRegister().getName());
        }

        for (String s : courses) {
            courseStatement = courseStatement.concat(s + ", ");
        }
        if (courseStatement == null || courseStatement.length() == 0) {
            courseStatement = ", ";
        }
        courseStatement = courseStatement.substring(0, courseStatement.length() - 2);

        for (String s : registers) {
            registerStatement = registerStatement.concat(s + ", ");
        }
        if (registerStatement == null || registerStatement.length() == 0) {
            registerStatement = ", ";
        }
        registerStatement = registerStatement.substring(0, registerStatement.length() - 2);

        Employment currentEmployment = employmentService.getCurrentEmployed(model.getObject());
        if (currentEmployment != null) {
            if (currentEmployment.getInstitution() != null) {
                institutionStatement = currentEmployment.getInstitution().getName();
            } else {
                institutionStatement = currentEmployment.getEmployer();
            }
        }

        for (String s : studentInfo) {
            trainingStatement = trainingStatement.concat(s + ", ");
        }
        if (trainingStatement == null || trainingStatement.length() == 0) {
            trainingStatement = ", ";
        }
        trainingStatement = trainingStatement.substring(0, trainingStatement.length() - 2);
        EmploymentType employmentType = employmentService.getRegistrantEmployment(model.getObject());
        add(new Label("trainingInfo", trainingStatement));
        add(new Label("registers", registerStatement));
        add(new Label("courses", courseStatement));
        if (studentNoneRegistration(model)) {
            add(new Label("currentRegistrationDate", DateUtil.getDate(registrationProcess.activeRegisterNotStudentRegister(model.getObject()).getRegistrationDate())));
        } else {
            add(new Label("currentRegistrationDate", ""));
        }
        //current work environment
        add(new Label("institutions", institutionStatement));
        if (employmentType != null) {
            add(new Label("employmentType", employmentType.getName()));
        } else {
            add(new Label("employmentType", ""));
        }
        String documents = "";
        if (currentRegistration(model)) {
            List<Requirement> requirements = new ArrayList<Requirement>();
            if (!requirements.isEmpty()) {
                for (Requirement r : requirements) {
                    SupportDocument document = supportDocumentService.getSupportDocument(model.getObject(), r);
                    if (document == null) {
                        documents = documents + r.getName() + " - ";
                    }
                }
            }
        }
        add(new Label("documents", documents));
    }

    public Boolean currentRegistration(IModel<Registrant> registrantModel) {
        return registrationService.hasCurrentRegistration(registrantModel.getObject());
    }

    public Boolean studentNoneRegistration(IModel<Registrant> registrantModel) {
        return registrationProcess.studentNoneRegistration(registrantModel.getObject());
    }

    public Boolean provisionalRegistration(IModel<Registrant> registrantModel) {
        return registrationProcess.studentNoneRegistration(registrantModel.getObject());
    }

    public Boolean studentRegistration(IModel<Registrant> registrantModel) {
        return registrationProcess.studentRegistration(registrantModel.getObject());
    }
}
