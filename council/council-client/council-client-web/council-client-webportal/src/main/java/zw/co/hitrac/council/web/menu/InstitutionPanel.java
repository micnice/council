package zw.co.hitrac.council.web.menu;

import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import zw.co.hitrac.council.web.pages.institution.InstitutionEditPage;
import zw.co.hitrac.council.web.pages.institution.InstitutionListPage;
import zw.co.hitrac.council.web.pages.institution.InstitutionMapEditPage;
import zw.co.hitrac.council.web.pages.institution.InstitutionMenuPage;
import zw.co.hitrac.council.web.pages.search.SearchInstitutionPage;

/**
 *
 * @author Constance Mabaso
 */
public class InstitutionPanel extends MenuPanel {

    private Link<Void> institutionLink;
    private Link<Void> searchInstitutionLink;
    private boolean addInstitution;
    private boolean viewInstitution;
    private boolean deleteDuplicateInstitutions;
    private boolean retiremultipleInstitutions;
    private Link<Void> addInstitutionLink;
    private Link<Void> viewInstitutionLink;
    private Link<Void> deleteDuplicateInstitutionsLink;
    private Link<Void> retireMultipleInstitutionsLink;
    private WebMarkupContainer institutionChildLinks;

    public InstitutionPanel(String id) {
        super(id);
        add(institutionLink = new BookmarkablePageLink<Void>("institutionViewLink", InstitutionMenuPage.class));
        add(searchInstitutionLink = new BookmarkablePageLink<Void>("searchInstitutionLink", SearchInstitutionPage.class));
        
        add(institutionChildLinks = new WebMarkupContainer("childLinks"));
        institutionChildLinks.add(addInstitutionLink = new Link<Void>("addInstitution"){
          @Override
          public void onClick(){
              setResponsePage(new InstitutionEditPage(null));
          }
        });
        
        
        institutionChildLinks.add(viewInstitutionLink = new BookmarkablePageLink<Void>("InstitutionList", InstitutionListPage.class));
        institutionChildLinks.add(deleteDuplicateInstitutionsLink = new BookmarkablePageLink<Void>("institutionEdit", InstitutionMapEditPage.class));
        institutionChildLinks.add(retireMultipleInstitutionsLink = new Link<Void>("retireInstitution"){
        @Override
                public void onClick(){
                    setResponsePage(new InstitutionMapEditPage(Boolean.TRUE));
                }
        
        });
        
        
    }
    
     @Override
    protected void onConfigure() {
        addCurrentBehavior(institutionLink, topMenuCurrent);
        institutionChildLinks.setVisibilityAllowed(topMenuCurrent);
        addCurrentBehavior(addInstitutionLink, addInstitution);
        addCurrentBehavior(viewInstitutionLink, viewInstitution);
        addCurrentBehavior(deleteDuplicateInstitutionsLink, deleteDuplicateInstitutions);
        addCurrentBehavior(retireMultipleInstitutionsLink, retiremultipleInstitutions);
    }

    public Link<Void> getInstitutionLink() {
        return institutionLink;
    }

    public Link<Void> getSearchInstitutionLink() {
        return searchInstitutionLink;
    }

    public Link<Void> getAddInstitutionLink() {
        return addInstitutionLink;
    }

    public Link<Void> getViewInstitutionLink() {
        return viewInstitutionLink;
    }

    public Link<Void> getDeleteDuplicateInstitutionsLink() {
        return deleteDuplicateInstitutionsLink;
    }

    public Link<Void> getRetireMultipleInstitutionsLink() {
        return retireMultipleInstitutionsLink;
    }
    
    public boolean isAddInstitution(){
        return addInstitution;
    }
    
    public boolean isViewinstitution(){
        return viewInstitution;
    }
    
    public boolean isDeleteDuplicateinstitutions(){
        return deleteDuplicateInstitutions;
    }
    
    public boolean isRetireMultipleInstitutions(){
        return retiremultipleInstitutions;
    }

   
}
