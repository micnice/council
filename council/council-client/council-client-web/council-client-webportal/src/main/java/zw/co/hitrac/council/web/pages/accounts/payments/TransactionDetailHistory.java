/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.accounts.payments;

import com.googlecode.wicket.jquery.ui.form.button.AjaxButton;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButton;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButtons;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogIcon;
import com.googlecode.wicket.jquery.ui.widget.dialog.MessageDialog;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.PaymentDetails;
import zw.co.hitrac.council.business.domain.accounts.ReceiptHeader;
import zw.co.hitrac.council.business.process.ReversePaymentProcess;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.service.accounts.*;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.pages.TemplatePage;

import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;

/**
 *
 * @author tdhlakama
 */
public class TransactionDetailHistory extends TemplatePage {

    @SpringBean
    private PaymentDetailsService paymentDetailsService;
    @SpringBean
    AccountService accountService;
    @SpringBean
    CustomerService customerAccountService;
    @SpringBean
    RegistrationService registrationService;
    @SpringBean
    ReceiptHeaderService receiptHeaderService;
    @SpringBean
    ReversePaymentProcess reversePaymentProcess;

    public TransactionDetailHistory(Long id) {

        final CompoundPropertyModel<PaymentDetails> model = new CompoundPropertyModel<PaymentDetails>(paymentDetailsService.get(id));
        setDefaultModel(model);

        add(new TransactionDetailHistoryPanel("transactionDetailHistoryPanel", id));
        add(new Label("customer.customerName"));

        final MessageDialog warningDialog = new MessageDialog("warningDialog", "Warning", "Are you sure you want cancel receipt?", DialogButtons.YES_NO, DialogIcon.WARN) {
            private static final long serialVersionUID = 1L;

            @Override
            public void onClose(AjaxRequestTarget target, DialogButton button) {
                if (button != null && button.equals(LBL_YES)) {
                    ReceiptHeader receiptHeader = receiptHeaderService.get(model.getObject().getReceiptHeader().getId());
                    setResponsePage(new ReversePaymentCommentPage(receiptHeader.getId()));
                }
            }
        };

        this.add(warningDialog);

        final MessageDialog warningDialog1 = new MessageDialog("warningDialog1", "Warning", "Are you sure you want to change the bank account?", DialogButtons.YES_NO, DialogIcon.WARN) {
            private static final long serialVersionUID = 1L;

            @Override
            public void onClose(AjaxRequestTarget target, DialogButton button) {
                if (button != null && button.equals(LBL_YES)) {
                    ReceiptHeader receiptHeader = receiptHeaderService.get(model.getObject().getReceiptHeader().getId());
                    setResponsePage(new ChangeBankAccountPage(receiptHeader));
                }
            }
        };

        this.add(warningDialog1);

        final Form<Void> form = new Form<Void>("form");
        this.add(form);
        form.add(new AjaxButton("reverse") {
            private static final long serialVersionUID = 1L;

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                warningDialog.open(target);
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.FINANCIAL_ADMIN_EXECUTIVE)));
                return enabled;
            }

            @Override
            protected void onConfigure() {
                if (!model.getObject().getReceiptHeader().getReversed()) {
                    setVisible(Boolean.TRUE);

                } else {
                    setVisible(Boolean.FALSE);
                }
            }
        });

        form.add(new AjaxButton("changeBank") {
            private static final long serialVersionUID = 1L;

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                warningDialog1.open(target);
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.FINANCIAL_ADMIN_EXECUTIVE)));
                return enabled;
            }

            @Override
            protected void onConfigure() {
                if (!model.getObject().getReceiptHeader().getReversed()) {
                    setVisible(Boolean.TRUE);

                } else {
                    setVisible(Boolean.FALSE);
                }
            }
        });
    }
}
