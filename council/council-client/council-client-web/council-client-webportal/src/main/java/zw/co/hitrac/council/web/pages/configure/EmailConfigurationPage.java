package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.GeneralParameters;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Edward Zengeni
 * @author Michael Matiashe
 */
public class EmailConfigurationPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private GeneralParametersService generalParametersService;
    
    public EmailConfigurationPage() {
     
        setDefaultModel(new CompoundPropertyModel<GeneralParameters>(new LoadableDetachableGeneralParametersModel()));
        Form<GeneralParameters> form = new Form<GeneralParameters>("form", (IModel<GeneralParameters>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                generalParametersService.save(getModelObject());
                setResponsePage(new AdministerDatabasePage());
            }
        };
        form.add(new TextField<String>("email").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("emailPassword").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextArea<String>("emailSignature").setRequired(true).add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(form);
    }

    private final class LoadableDetachableGeneralParametersModel extends LoadableDetachableModel<GeneralParameters> {

        @Override
        protected GeneralParameters load() {
            return generalParametersService.get();
        }
    }
}
