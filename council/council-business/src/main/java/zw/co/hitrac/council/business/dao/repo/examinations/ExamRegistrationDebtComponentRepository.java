/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.repo.examinations;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistrationDebtComponent;

/**
 *
 * @author tidza
 */
public interface ExamRegistrationDebtComponentRepository extends CrudRepository<ExamRegistrationDebtComponent, Long> {
public List<ExamRegistrationDebtComponent> findAll();
}
