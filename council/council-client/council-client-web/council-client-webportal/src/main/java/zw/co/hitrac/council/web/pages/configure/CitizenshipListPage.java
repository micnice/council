/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.configure;

/**
 *
 * @author edward
 */
import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Citizenship;
import zw.co.hitrac.council.business.service.CitizenshipService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

public class CitizenshipListPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private CitizenshipService citizenshipService;

    public CitizenshipListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new CitizenshipEditPage(null));
            }
        });

        IModel<List<Citizenship>> model = new LoadableDetachableModel<List<Citizenship>>() {
            @Override
            protected List<Citizenship> load() {
                return citizenshipService.findAll();
            }
        };

        PropertyListView<Citizenship> eachItem = new PropertyListView<Citizenship>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<Citizenship> item) {
                Link<Citizenship> viewLink = new Link<Citizenship>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new CitizenshipViewPage(getModelObject().getId()));
                    }
                };
                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };

        add(eachItem);
    }
}
