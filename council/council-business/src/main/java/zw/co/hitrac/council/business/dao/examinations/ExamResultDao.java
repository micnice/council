/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.examinations;

import java.util.List;
import zw.co.hitrac.council.business.dao.IGenericDAO;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.examinations.Exam;
import zw.co.hitrac.council.business.domain.examinations.ExamResult;

/**
 *
 * @author tidza
 */
public interface ExamResultDao extends IGenericDAO<ExamResult> {

    public List<ExamResult> examResults(Exam exam);

    /**
     *
     * Method to Get Total Number of Candidates Per Registered Paper or and by
     * Institution
     *
     * @param exam
     * @param institution
     * @return
     */
    public Integer getTotalCandidates(Exam exam, Institution institution);

    /**
     *
     * Method to Get Total Number of Candidates Per Registered Paper who passed
     * or failed in first attempt or supplementary exam and by Institution
     *
     * @param exam
     * @param institution
     * @param supplementaryExam
     * @param pass
     * @return
     */
    public Integer getTotalCandidates(Exam exam, Institution institution, Boolean supplementaryExam, Boolean pass);
}
