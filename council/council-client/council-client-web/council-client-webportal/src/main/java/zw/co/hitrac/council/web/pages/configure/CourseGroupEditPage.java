package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CourseGroup;
import zw.co.hitrac.council.business.service.CourseGroupService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Michael Matiashe
 */
public class CourseGroupEditPage extends IAdministerDatabaseBasePage{
    
    @SpringBean
    private CourseGroupService courseGroupService;

    public CourseGroupEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<CourseGroup>(new LoadableDetachableCourseGroupModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink",AdministerDatabasePage.class));
        Form<CourseGroup> form=new Form<CourseGroup>("form",(IModel<CourseGroup>)getDefaultModel()) {
            @Override
            public void onSubmit(){
                courseGroupService.save(getModelObject());
                setResponsePage(new CourseGroupViewPage(getModelObject().getId()));
            }
        };
        
        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior())); 
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink",CourseGroupListPage.class));
        add(form);
    }
    
    
    private final class LoadableDetachableCourseGroupModel extends LoadableDetachableModel<CourseGroup> {

        private Long id;

        public LoadableDetachableCourseGroupModel(Long id) {
            this.id = id;
        }

        @Override
        protected CourseGroup load() {
            if(id==null){
                return new CourseGroup();
            }
            return courseGroupService.get(id);
        }
    }
    
    
}
