/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.configure;

import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CaseOutcome;
import zw.co.hitrac.council.business.service.CaseOutcomeService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

/**
 *
 * @author kelvin
 */
public class CaseOutcomeListPage  extends IAdministerDatabaseBasePage{
  @SpringBean
    private CaseOutcomeService caseOutcomeService;

    public CaseOutcomeListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink",AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink",AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {

            @Override
            public void onClick() {
               setResponsePage(new CaseOutcomeEditPage(null));
            }
        });
        
        IModel<List<CaseOutcome>> model=new LoadableDetachableModel<List<CaseOutcome>>() {

            @Override
            protected List<CaseOutcome> load() {
               return caseOutcomeService.findAll();
            }
        };
        
        PropertyListView<CaseOutcome> eachItem=new PropertyListView<CaseOutcome>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<CaseOutcome> item) {
                Link<CaseOutcome> viewLink=new Link<CaseOutcome>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                       setResponsePage(new CaseOutcomeViewPage(getModelObject().getId()));
                    }
                };
                if(item.getIndex()%2==0){
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };
        
        add(eachItem);
    }
}
    
    
    