package zw.co.hitrac.council.web.pages.registration;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantCondition;
import zw.co.hitrac.council.business.service.RegistrantConditionService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.utility.CustomDateLabel;
import zw.co.hitrac.council.web.utility.CustomDateTimeLabel;

/**
 *
 * @author kelvin
 */
public class RegistrantConditionPanelList extends Panel {

    @SpringBean
    private RegistrantConditionService registrantConditionService;

    public RegistrantConditionPanelList(String id, final IModel<Registrant> registrantModel) {
        super(id);

        add(new Link<Registrant>("registrantConditionLink") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantConditionEditPage(registrantModel, Boolean.TRUE));
            }
        });

        add(new Link<Registrant>("registrantPersonalConditionPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantPersonalConditionPage(registrantModel, Boolean.TRUE));
            }
        });

        add(new PropertyListView<RegistrantCondition>("registrantConditionList", registrantConditionService.getAllRegistrantConditions(registrantModel.getObject())) {
            @Override
            protected void populateItem(ListItem<RegistrantCondition> item) {

                Link<RegistrantCondition> viewLink = new Link<RegistrantCondition>("registrantConditionLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        if (getModel().getObject().getPersonal()) {
                            setResponsePage(new RegistrantPersonalConditionPage(getModelObject().getId()));
                        } else {
                            setResponsePage(new RegistrantConditionEditPage(getModelObject().getId()));
                        }
                    }

                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(viewLink);
                item.add(new CustomDateLabel("endDate"));
                item.add(new Label("conditionText"));
                item.add(new Label("statusText"));
                item.add(new CustomDateLabel("startDate"));
                item.add(new Label("createdBy"));
                item.add(new Label("modifiedBy"));
                item.add(new CustomDateLabel("dateCreated"));
                item.add(new CustomDateLabel("dateModified"));
            }
        });

    }
}
