/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao;

import java.util.List;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantIdentification;

/**
 *
 * @author kelvin
 */
public interface RegistrantIdentificationDAO extends IGenericDAO<RegistrantIdentification>{
    public List<RegistrantIdentification> getIdentities(Registrant registrant);
}
