package zw.co.hitrac.council.web.pages.registration;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.web.models.RegistrantTransferDataListModel;
import zw.co.hitrac.council.web.pages.TemplatePage;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;

import org.apache.wicket.markup.html.basic.Label;
import zw.co.hitrac.council.business.domain.GeneralParameters;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.web.models.RegistrantDataListModel;

/**
 *
 * @author Takunda Dhlakama
 */
public class RegistrantListPage extends TemplatePage {

    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private GeneralParametersService generalParametersService;

    public RegistrantListPage() {
        menuPanelManager.getRegistrantionPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantionPanel().setRegistrantListPageCurrent(true);
        
        IModel<List<RegistrantData>> model = new RegistrantDataListModel(registrantService);
        GeneralParameters generalParameters = generalParametersService.get();
        String name = generalParameters.getRegistrantName();

        if ((name == null) || name.trim().equals("")) {
            name = "Registrant";
        }

        add(new Label("name", name));
      
        // Registrant List Data View Panel
        add(new RegistrantDataViewPanel("registrantDataListPanel", model));
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
