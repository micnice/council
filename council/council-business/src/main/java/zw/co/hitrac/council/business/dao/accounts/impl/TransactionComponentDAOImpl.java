package zw.co.hitrac.council.business.dao.accounts.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.accounts.TransactionComponentDAO;
import zw.co.hitrac.council.business.dao.repo.accounts.TransactionComponentReposiroty;
import zw.co.hitrac.council.business.domain.accounts.TransactionComponent;

/**
 *
 * @author Judge Muzinda
 * @author Constance Mabaso
 */
@Repository
public class TransactionComponentDAOImpl implements TransactionComponentDAO{

    @Autowired
    private TransactionComponentReposiroty transactionReposiroty;
    
    public TransactionComponent save(TransactionComponent t) {
        return transactionReposiroty.save(t);
    }

    public List<TransactionComponent> findAll() {
        return transactionReposiroty.findAll();
    }

    public TransactionComponent get(Long id) {
        return transactionReposiroty.findOne(id);
    }

    public void setTransactionReposiroty(TransactionComponentReposiroty transactionReposiroty) {
        this.transactionReposiroty = transactionReposiroty;
    }

    public List<TransactionComponent> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
