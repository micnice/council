package zw.co.hitrac.council.web.pages.accounts.documents;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.Prepayment;
import zw.co.hitrac.council.business.domain.accounts.PrepaymentDetails;
import zw.co.hitrac.council.business.service.accounts.CustomerService;
import zw.co.hitrac.council.business.service.accounts.PrepaymentDetailsService;
import zw.co.hitrac.council.business.service.accounts.PrepaymentService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.reports.PrepaymentReceipt;
import zw.co.hitrac.council.reports.PrepaymentReceiptListReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.accounts.payments.TransactionHistoryPanel;
import zw.co.hitrac.council.web.pages.institution.InstitutionViewPage;
import zw.co.hitrac.council.web.utility.CustomDateLabel;
import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 * @author Michael Matiashe
 */
public class PrepaymentViewPage extends TemplatePage {

    @SpringBean
    private PrepaymentService prepaymentService;
    @SpringBean
    private PrepaymentDetailsService prepaymentDetailsService;
    @SpringBean
    private CustomerService customerService;

    public PrepaymentViewPage(final Long id) {
        final CompoundPropertyModel<Prepayment> model = new CompoundPropertyModel<Prepayment>(new LoadableDetachablePrepaymentModel(id));
        setDefaultModel(model);

        add(new BookmarkablePageLink<Void>("returnLink", PrepaymentListPage.class));
        add(new Label("institution"));
        add(new CustomDateLabel("dateOfPayment"));
        add(new CustomDateLabel("dateOfDeposit"));
        add(new Label("amount"));
        add(new Label("balance"));
        add(new Label("createdBy"));
        add(new Link("cancel") {

            @Override
            public void onClick() {
                try {
                    Prepayment prepayment = model.getObject();
                    List<PrepaymentDetails> prepaymentDetailses = prepaymentDetailsService.findByPrepayment(prepayment);
                    if (prepaymentDetailses.isEmpty()) {
                        prepayment.setCanceled(Boolean.TRUE);
                        prepaymentService.save(prepayment);
                        setResponsePage(new InstitutionViewPage(prepayment.getInstitution().getId()));
                    } else {
                        throw new CouncilException("Other payments were made using this prepayment");
                    }
                } catch (Exception e) {
                    error(e.getMessage());
                }

            }

            @Override
            protected void onConfigure() {

                setVisible(CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.ACCOUNTS_OFFICER, Role.FINANCIAL_ADMIN_EXECUTIVE))));

            }

        }).add(new AttributeModifier(
                "onclick", "return confirm(’cancel prepayment?’);"));
        add(new Link("print") {

            @Override
            public void onClick() {
                try {
                    Prepayment prepayment = prepaymentService.get(id);
                    List<Prepayment> list = new ArrayList<Prepayment>();
                    list.add(prepayment);
                    HashMap parameters = new HashMap();
                    PrepaymentReceipt prepaymentReport = new PrepaymentReceipt();
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(prepaymentReport, contentType, list, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(), RequestCycle.get().getResponse(), null);
                    resource.respond(a);
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException exception) {
                    exception.printStackTrace();
                }
            }
        });

        add(new Link("printList") {

            @Override
            public void onClick() {
                try {
                    List<PrepaymentDetails> details = prepaymentDetailsService.findByPrepayment(model.getObject());
                    for (PrepaymentDetails pd : details) {
                        pd.setRegistrant(customerService.getRegistrant(pd.getCustomer().getAccount()));
                    }
                    HashMap parameters = new HashMap();
                    parameters.put("institution", model.getObject().getInstitution().getName());
                    parameters.put("dateOfDeposit", model.getObject().getDateOfDeposit());
                    parameters.put("dateOfPayment", model.getObject().getDateOfPayment());
                    parameters.put("createdBy", model.getObject().getCreatedBy()!=null ? model.getObject().getCreatedBy().getFullname() : "");
                    parameters.put("transactionType", model.getObject().getTransactionType().getName());
                    parameters.put("balance", model.getObject().getBalance());
                    parameters.put("amount", model.getObject().getAmount());
                    PrepaymentReceiptListReport prepaymentReceipt_subreport = new PrepaymentReceiptListReport();
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(prepaymentReceipt_subreport, contentType, details, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(), RequestCycle.get().getResponse(), null);
                    resource.respond(a);
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException exception) {
                    exception.printStackTrace();
                }

            }
        });
        if(model.getObject().getBalance().compareTo(model.getObject().getAmount())>0){
        add(new Label("comment", "The prepayment balances are anot accurate"));
        } else {
            add(new Label("comment",""));
        }

        add(new FeedbackPanel("feedback"));
        add(new TransactionHistoryPanel("transactionHistoryPanel", model.getObject()));
    }

    private final class LoadableDetachablePrepaymentModel extends LoadableDetachableModel<Prepayment> {

        private Long id;

        public LoadableDetachablePrepaymentModel(Long id) {
            this.id = id;
        }

        @Override
        protected Prepayment load() {

            return prepaymentService.get(id);
        }
    }
}
