package zw.co.hitrac.council.webservices.impl;

import java.util.List;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zw.co.hitrac.council.business.domain.WebUser;
import zw.co.hitrac.council.business.service.WebUserService;
import zw.co.hitrac.council.webservices.api.WebUserWebService;

/**
 *
 * @author tdhlakama
 */
@Component
public class WebUserWebServiceImpl implements WebUserWebService {

    @Autowired
    private WebUserService webUserService;

    public void setWebUserService(WebUserService webUserService) {
        this.webUserService = webUserService;
    }

    @Override
    public WebUser get(String username, String password) {
        return webUserService.get(username, password);
    }

    @Override
    public Response createWebUser(String username, String password, String registrationNumber) {
        return Response.status(200).entity(webUserService.createWebUser(username, password, registrationNumber)).build();
    }

    @Override
    public Response loginWebUser(String username, String password) {
        return Response.status(200).entity(webUserService.loginWebUser(username, password)).build();
    }

    @Override
    public List<WebUser> getWebUsers() {
        return webUserService.findAll(Boolean.FALSE);
    }
}
