/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.registration;

import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButton;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButtons;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogIcon;
import com.googlecode.wicket.jquery.ui.widget.dialog.MessageDialog;
import org.apache.commons.io.IOUtils;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.SupportDocumentService;
import zw.co.hitrac.council.web.utility.CustomDateTimeLabel;

import javax.jcr.Node;
import javax.jcr.Repository;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;
import java.util.List;

/**
 * @author tdhlakama
 */
public class RegistrantSupportingDocumentPanel extends Panel {

    @SpringBean
    private SupportDocumentService supportDocumentService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    private SupportDocument supportDocument = null;

    /**
     * Constructor
     *
     * @param id
     */
    public RegistrantSupportingDocumentPanel(String id, final Registrant registrant, final Application application,
                                             final Requirement requirement, final RegistrantDisciplinary
                                                     registrantDisciplinary, final RegistrantQualification
                                                     registrantQualification, final ExamRegistration
                                                     examRegistration, Institution institution) {
        super(id);

        List<SupportDocument> supportDocuments = supportDocumentService.getSupportDocuments(registrant, application,
                requirement, registrantDisciplinary, registrantQualification, examRegistration, institution);

        add(new PropertyListView<SupportDocument>("documentList", supportDocuments) {
            @Override
            protected void populateItem(final ListItem<SupportDocument> item) {
                final Long documentId = item.getModelObject().getId();
                Link<SupportDocument> viewLink = new Link<SupportDocument>("viewDocument", item.getModel()) {
                    @Override
                    public void onClick() {
                        try {
                            Repository repository = JcrUtils.getRepository(generalParametersService.get()
                                    .getContent_Url());
                            Session session = repository.login(new SimpleCredentials("admin", "admin".toCharArray()));
                            try {
                                Node fileNode = session.getNode(this.getModel().getObject().getPathName());
                                fileNode.getProperty("jcr:data").getBinary().getStream();
                                ByteArrayResource resource = new ByteArrayResource(this.getModel().getObject()
                                        .getDocumentType(), IOUtils.toByteArray(fileNode.getProperty("jcr:data")
                                        .getBinary().getStream()), this.getModel().getObject().getName());
                                IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                        RequestCycle.get().getResponse(), null);
                                resource.respond(a);
                                RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                            } finally {
                                session.logout();
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace(System.out);
                        }
                    }

                    @Override
                    protected void onConfigure() {
                        if (getModel().getObject().getPathName() == null) {
                            setVisible(Boolean.FALSE);
                        } else {
                            setVisible(Boolean.TRUE);
                        }
                    }
                };
                item.add(viewLink);


                final MessageDialog warningDialog = new MessageDialog("warningDialog", "Warning", "Are you sure you " +
                        "want to remove document", DialogButtons.YES_NO, DialogIcon.WARN) {
                    private static final long serialVersionUID = 1L;

                    @Override
                    public void onClose(AjaxRequestTarget target, DialogButton button) {
                        if (button != null && button.equals(LBL_YES)) {
                            try {
                                SupportDocument su = supportDocumentService.get(documentId);
                                su.setRetired(Boolean.TRUE);
                                supportDocumentService.save(su);
                                this.setVisibilityAllowed(Boolean.FALSE);
                                setResponsePage(getPage());
                            } catch (Exception ex) {
                                ex.printStackTrace(System.out);
                            }
                        }
                    }
                };

                item.add(warningDialog);

                item.add(new AjaxLink("removeDocument") {
                    private static final long serialVersionUID = 1L;

                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        warningDialog.open(target);
                    }
                });

                item.add(new Label("name"));
                item.setVisible(!item.getModelObject().getRetired());
                item.add(new Label("createdBy"));
                item.add(new Label("modifiedBy"));
                item.add(CustomDateTimeLabel.forDate("dateCreated",
                        new PropertyModel<>(item.getModel(), "dateCreated")));
                item.add(CustomDateTimeLabel.forDate("dateModified",
                        new PropertyModel<>(item.getModel(), "dateModified")));


            }


        }).setOutputMarkupId(true);

    }
}