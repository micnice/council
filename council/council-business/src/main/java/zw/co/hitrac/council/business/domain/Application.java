package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kelvin Goredema
 * @author Michael Matiashe
 */
@Entity
@Table(name = "application")
@Audited
public class Application extends BaseIdEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    private Date applicationDate;
    private Registrant registrant;
    private Course course;
    private Course approvedCourse;
    private ApplicationPurpose applicationPurpose;
    private Institution institution;
    private DebtComponent debtComponent;
    private String applicationStatus = APPLICATIONPENDING;
    private Date dateApproved;
    private Date dateApprovedByBoard;
    private Date dateProcessed;
    private Date datePosted;
    private Date proposedCommencementDate;
    private Date startDate;
    private Date endDate;
    private User approvedBy;
    private User processedBy;
    private User approvedByboard;
    private String clientNotified;
    private String comment = "";
    private String ownerName;
    private String ownerProfession;
    private String ownerTelephone;
    private String ownerEmail;
    private TransferRule transferRule;
    private QualificationType qualificationType;
    public static String FOREIGN = "FOREIGN";
    public static String LOCAL = "LOCAL";
    public static String APPLICATIONAPPROVED = "APPLICATION APPROVED";
    public static String APPLICATIONDECLINED = "APPLICATION DECLINED";
    public static String APPLICATION_IN_CIRCULATION = "APPLICATION IN CIRCULATION";
    public static String APPLICATION_AWAITING_COUNCIL_APPROVAL = "AWAITING COUNCIL APPROVAL";
    public static String APPLICATIONUNDERREVIEW = "APPLICATION UNDER REVIEW";
    public static String APPLICATIONPENDING = "APPLICATION PENDING";
    public static String APPLICATIONREVERSED = "APPLICATION REVERSED";
    private String firstName;
    private String middleName;
    private String lastName;
    private String maidenName;
    private Long oldInstitutionApplicationID;
    private Set<Service> services = new HashSet<Service>();
    private Boolean used = Boolean.FALSE;
    private Boolean expired = Boolean.FALSE;
    private String inGoodStandingStatus;
    private Boolean photosSubmited;
    private EmploymentType employmentType;
    private Qualification qualification;
    private Date qualificationStartDate;
    private Date qualificationEndDate;
    private Date dateAwarded;
    private Institution trainingInstitution;
    private CouncilDuration councilDuration;

    public String getMaidenName() {
        return maidenName;
    }

    public void setMaidenName(String maidenName) {
        this.maidenName = maidenName;
    }

    public String getClientNotified() {
        return clientNotified;
    }

    public void setClientNotified(String clientNotified) {
        this.clientNotified = clientNotified;
    }

    public Boolean getPhotosSubmited() {
        return photosSubmited;
    }

    public void setPhotosSubmited(Boolean photosSubmited) {
        this.photosSubmited = photosSubmited;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDateApprovedByBoard() {
        return dateApprovedByBoard;
    }

    public void setDateApprovedByBoard(Date dateApprovedByBoard) {
        this.dateApprovedByBoard = dateApprovedByBoard;
    }

    @ManyToOne
    public User getApprovedByboard() {
        return approvedByboard;
    }

    public void setApprovedByboard(User approvedByboard) {
        this.approvedByboard = approvedByboard;
    }

    @ManyToMany
    public Set<Service> getServices() {
        return services;
    }

    public void setServices(Set<Service> services) {
        this.services = services;
    }

    public Boolean getUsed() {
        return used;
    }

    public void setUsed(Boolean used) {
        this.used = used;
    }

    public Boolean getExpired() {
        return expired;
    }

    public void setExpired(Boolean expired) {
        this.expired = expired;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDateProcessed() {
        return dateProcessed;
    }

    public void setDateProcessed(Date dateProcessed) {
        this.dateProcessed = dateProcessed;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDatePosted() {
        return datePosted;
    }

    public void setDatePosted(Date datePosted) {
        this.datePosted = datePosted;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @ManyToOne
    public User getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(User approvedBy) {
        this.approvedBy = approvedBy;
    }

    @ManyToOne
    public User getProcessedBy() {
        return processedBy;
    }

    public void setProcessedBy(User processedBy) {
        this.processedBy = processedBy;
    }

    public Long getOldInstitutionApplicationID() {
        return oldInstitutionApplicationID;
    }

    public void setOldInstitutionApplicationID(Long oldInstitutionApplicationID) {
        this.oldInstitutionApplicationID = oldInstitutionApplicationID;
    }

    @Enumerated(EnumType.STRING)
    public QualificationType getQualificationType() {
        return qualificationType;
    }

    public void setQualificationType(QualificationType qualificationType) {
        this.qualificationType = qualificationType;
    }

    public String getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(String applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }

    @ManyToOne
    public Registrant getRegistrant() {
        return registrant;
    }

    public void setRegistrant(Registrant registrant) {
        this.registrant = registrant;
    }

    @ManyToOne
    public Course getApprovedCourse() {
        return approvedCourse;
    }

    public void setApprovedCourse(Course approvedCourse) {
        this.approvedCourse = approvedCourse;
    }

    @ManyToOne
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Enumerated(EnumType.STRING)
    public ApplicationPurpose getApplicationPurpose() {
        return applicationPurpose;
    }

    public void setApplicationPurpose(ApplicationPurpose applicationPurpose) {
        this.applicationPurpose = applicationPurpose;
    }

    @ManyToOne
    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    @ManyToOne
    public Institution getTrainingInstitution() {
        return trainingInstitution;
    }

    public void setTrainingInstitution(Institution trainingInstitution) {
        this.trainingInstitution = trainingInstitution;
    }

    @OneToOne
    public DebtComponent getDebtComponent() {
        return debtComponent;
    }

    public void setDebtComponent(DebtComponent debtComponent) {
        this.debtComponent = debtComponent;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDateApproved() {
        return dateApproved;
    }

    public void setDateApproved(Date dateApproved) {
        this.dateApproved = dateApproved;
    }

    @Column(length = 1024)
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return getId().toString();
    }

    @Transient
    public String getApplicationTextStatus() {
        if (applicationStatus == null) {
            return APPLICATIONPENDING;
        }
        return getApplicationStatus();
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getProposedCommencementDate() {
        return proposedCommencementDate;
    }

    public void setProposedCommencementDate(Date proposedCommencementDate) {
        this.proposedCommencementDate = proposedCommencementDate;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerProfession() {
        return ownerProfession;
    }

    public void setOwnerProfession(String ownerProfession) {
        this.ownerProfession = ownerProfession;
    }

    public String getOwnerTelephone() {
        return ownerTelephone;
    }

    public void setOwnerTelephone(String ownerTelephone) {
        this.ownerTelephone = ownerTelephone;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    @OneToOne
    public TransferRule getTransferRule() {
        return transferRule;
    }

    public void setTransferRule(TransferRule transferRule) {
        this.transferRule = transferRule;
    }

    @Transient
    public String getInGoodStandingStatus() {
        return inGoodStandingStatus;
    }

    public void setInGoodStandingStatus(String inGoodStandingStatus) {
        this.inGoodStandingStatus = inGoodStandingStatus;
    }

    @Enumerated(EnumType.STRING)
    public EmploymentType getEmploymentType() {
        return employmentType;
    }

    public void setEmploymentType(EmploymentType employmentType) {
        this.employmentType = employmentType;
    }

    @ManyToOne
    public Qualification getQualification() {
        return qualification;
    }

    public void setQualification(Qualification qualification) {
        this.qualification = qualification;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getQualificationStartDate() {
        return qualificationStartDate;
    }

    public void setQualificationStartDate(Date qualificationStartDate) {
        this.qualificationStartDate = qualificationStartDate;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getQualificationEndDate() {
        return qualificationEndDate;
    }

    public void setQualificationEndDate(Date qualificationEndDate) {
        this.qualificationEndDate = qualificationEndDate;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDateAwarded() {
        return dateAwarded;
    }

    public void setDateAwarded(Date dateAwarded) {
        this.dateAwarded = dateAwarded;
    }

    @Transient
    public CouncilDuration getCouncilDuration() {
        return councilDuration;
    }

    public void setCouncilDuration(CouncilDuration councilDuration) {
        this.councilDuration = councilDuration;
    }

    @XmlTransient
    @Transient
    public List<RegistrantQualification> getRegistrantQualificationList() {
        return registrant != null ? getRegistrant().getRegistrantQualificationList() : new ArrayList();
    }


    public static List<String> getApplicationStates = Arrays.asList(new String[]{Application.APPLICATIONPENDING, Application.APPLICATIONAPPROVED,
            Application.APPLICATIONDECLINED, Application.APPLICATION_IN_CIRCULATION, Application.APPLICATION_AWAITING_COUNCIL_APPROVAL, Application.APPLICATIONUNDERREVIEW, Application.APPLICATIONREVERSED});

}
