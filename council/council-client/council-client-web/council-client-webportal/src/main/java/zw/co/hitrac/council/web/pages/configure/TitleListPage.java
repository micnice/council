package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Title;
import zw.co.hitrac.council.business.service.TitleService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;

/**
 *
 * @author Charles Chigoriwa
 */
public class TitleListPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private TitleService titleService;

    public TitleListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new TitleEditPage(null));
            }
        });

        IModel<List<Title>> model = new LoadableDetachableModel<List<Title>>() {
            @Override
            protected List<Title> load() {
                return titleService.findAll();
            }
        };
        PropertyListView<Title> eachItem = new PropertyListView<Title>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<Title> item) {
                Link<Title> viewLink = new Link<Title>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new TitleViewPage(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };

        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
