package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.City;
import zw.co.hitrac.council.business.service.CityService;

/**
 *
 * @author Matiashe Michael
 */
public class CityViewPage extends IAdministerDatabaseBasePage  {

    @SpringBean
    private CityService cityService;

    public CityViewPage(Long id) {
        CompoundPropertyModel<City> model=new CompoundPropertyModel<City>(new LoadableDetachableCityModel(id));
        setDefaultModel(model);
        add(new Link<City>("editLink",model){

            @Override
            public void onClick() {
                setResponsePage(new CityEditPage(getModelObject().getId(), getPageReference()));
            }
            
        });
        add(new BookmarkablePageLink<Void>("returnLink", CityListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("country"));
        add(new Label("city",model.bind("name")));
    }


    private final class LoadableDetachableCityModel extends LoadableDetachableModel<City> {

        private Long id;

        public LoadableDetachableCityModel(Long id) {
            this.id = id;
        }

        @Override
        protected City load() {

            return cityService.get(id);
        }
    }
}
