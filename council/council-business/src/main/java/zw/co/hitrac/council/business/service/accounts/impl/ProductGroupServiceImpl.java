/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.accounts.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.accounts.ProductGroupDAO;
import zw.co.hitrac.council.business.domain.accounts.ProductGroup;
import zw.co.hitrac.council.business.service.accounts.ProductGroupService;

import java.util.List;

/**
 *
 * @author tidza
 */
@Service
@Transactional
public class ProductGroupServiceImpl implements ProductGroupService {

    @Autowired
    private ProductGroupDAO productPriceDAO;

    @Transactional
    public ProductGroup save(ProductGroup t) {
        return productPriceDAO.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ProductGroup> findAll() {
        return productPriceDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public ProductGroup get(Long id) {
        return productPriceDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ProductGroup> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setProductGroupDao(ProductGroupDAO productPriceDAO) {

        this.productPriceDAO = productPriceDAO;
    }

}
