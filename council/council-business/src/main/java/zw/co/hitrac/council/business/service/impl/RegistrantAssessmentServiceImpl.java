/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.RegistrantAssessmentDAO;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantAssessment;
import zw.co.hitrac.council.business.service.RegistrantAssessmentService;

import java.util.List;

/**
 *
 * @author kelvin
 */
@Service
@Transactional
public class RegistrantAssessmentServiceImpl implements RegistrantAssessmentService {

    @Autowired
    private RegistrantAssessmentDAO registrantAssessmentDAO;

    @Transactional
    public RegistrantAssessment save(RegistrantAssessment registrantAssessment) {
        return registrantAssessmentDAO.save(registrantAssessment);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantAssessment> findAll() {
        return registrantAssessmentDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantAssessment get(Long id) {
        return registrantAssessmentDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantAssessment> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setRegistrantAssessmentDAO(RegistrantAssessmentDAO registrantAssessmentDAO) {
        this.registrantAssessmentDAO = registrantAssessmentDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantAssessment> getAssessments(Registrant registrant) {
        return this.registrantAssessmentDAO.getAssessments(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantAssessment> getAssessments(Institution institution) {
        return registrantAssessmentDAO.getAssessments(institution);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantAssessment> getAssessments(Application application) {
        return registrantAssessmentDAO.getAssessments(application);
    }
}
