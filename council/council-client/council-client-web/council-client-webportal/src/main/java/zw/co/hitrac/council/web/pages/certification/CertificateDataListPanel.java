package zw.co.hitrac.council.web.pages.certification;

import net.sf.jasperreports.engine.JRException;
import org.apache.commons.lang.BooleanUtils;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PageableListView;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigator;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.ReceiptHeader;
import zw.co.hitrac.council.business.domain.reports.ItemCase;
import zw.co.hitrac.council.business.process.ProductFinder;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.business.service.accounts.*;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.business.utils.StringFormattingUtil;
import zw.co.hitrac.council.reports.*;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.utility.CustomDateLabel;
import zw.co.hitrac.council.web.utility.CustomDateTimeLabel;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author tdhlakama
 */
public class CertificateDataListPanel extends Panel {

    private Logger logger = LoggerFactory.getLogger(CertificateDataListPanel.class);

    private static final String TOP_LOGO = "zw/co/hitrac/council/reports/images/logo1.jpg";
    private static final String WATERMARK_LOGO = "zw/co/hitrac/council/reports/images/logo.png";
    @SpringBean
    private ProductIssuanceService productIssuanceService;
    @SpringBean
    private ProductFinder productFinder;

    @SpringBean
    QualificationService qualificationService;

    @SpringBean
    private RegistrantQualificationService registrantQualificationService;
    @SpringBean
    private RegistrantAddressService registrantAddressService;
    @SpringBean
    private RegistrantConditionService registrantConditionService;
    @SpringBean

    private RegistrationService registrationService;
    @SpringBean
    private RegistrantDisciplinaryService registrantDisciplinaryService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private RegistrantClearanceService registrantClearanceService;
    private Boolean docExport = Boolean.FALSE;
    @SpringBean
    private CourseConditionService courseConditionService;
    @SpringBean
    private ReceiptHeaderService receiptHeaderService;
    @SpringBean
    private AccountsParametersService accountsParametersService;
    @SpringBean
    private ReceiptItemService receiptItemService;
    @SpringBean
    private RegistrationProcess registrationProcess;
    

    public CertificateDataListPanel(String id, IModel<List<ProductIssuance>> model) {

        super(id);
        if (!generalParametersService.get().getRegistrationByApplication()) {
            docExport = Boolean.TRUE;
        } else {
            docExport = Boolean.FALSE;
        }

        PageableListView<ProductIssuance> eachItem = new PageableListView<ProductIssuance>("eachItem", model, 20) {

            @Override
            protected void populateItem(ListItem<ProductIssuance> item) {

                item.setModel(new CompoundPropertyModel<ProductIssuance>(item.getModel()));
                item.add(new Link<ProductIssuance>("issueAndPrintLink", item.getModel()) {

                    @Override
                    public void onClick() {

                        try {
                            Report report = null;
                            HashMap<String, Object> params = new HashMap<String, Object>();
                            initParams(params, getModelObject().getRegistrant(), getModelObject());
                            List<Registrant> registrants = new ArrayList<Registrant>();
                            registrants.add(getModelObject().getRegistrant());
                            ProductIssuanceType productIssuanceType = getModel().getObject().getProductIssuanceType();
                            Register register = getModel().getObject().getRegister();
                            if (productIssuanceType.equals(ProductIssuanceType.PRACTICING_CERTIFICATE)
                                    && register.equals(generalParametersService.get().getProvisionalRegister())) {

                                report = new PracticingProvisionalCertificateReport();
                                ReportResourceUtils.processJavaBeanReport(report, ContentType.PDF, registrants, params);

                            } else if (productIssuanceType.equals(ProductIssuanceType.REGISTRATION_CERTIFICATE)
                                    && generalParametersService.get().getSpecialistRegister() != null
                                    && register.equals(generalParametersService.get().getSpecialistRegister())) {

                                report = new SpecialistRegistrationCertificateReport();
                                ReportResourceUtils.processJavaBeanReport(report, ContentType.PDF, registrants, params);

                            } else if (productIssuanceType.equals(ProductIssuanceType.PRACTICING_CERTIFICATE)) {

                                report = new PracticingCertificateReport();
                                ReportResourceUtils.processJavaBeanReport(report, ContentType.PDF, registrants, params);

                            } else if (productIssuanceType.equals(ProductIssuanceType.REGISTRATION_CERTIFICATE)) {

                                report = new RegistrationCertificateReport();
                                ReportResourceUtils.processJavaBeanReport(report, ContentType.PDF, registrants, params);

                            } else if (productIssuanceType.equals(ProductIssuanceType.QUALIFICATION_CERTIFICATE)) {

                                report = new QualificationCertificateReport();
                                ReportResourceUtils.processJavaBeanReport(report, ContentType.PDF, registrants, params);

                            } else if (productIssuanceType.equals(ProductIssuanceType.CERTIFICATE_OF_GOOD_STANDING)) {

                                report = new CertificateOfGoodStandingReport();
                                ReportResourceUtils.processJavaBeanReport(report, ContentType.PDF, registrants, params);

                            } else if (productIssuanceType.equals(ProductIssuanceType.ADDITIONAL_QUALIFICATION_CERTIFICATE)) {

                                report = new AdditionalQualificationCertificateReport();
                                ReportResourceUtils.processJavaBeanReport(report, ContentType.PDF,
                                        registrantQualificationService.getRegistrantQualificationCertificateType(getModelObject().getRegistrant(),
                                                ProductIssuanceType.ADDITIONAL_QUALIFICATION_CERTIFICATE), params);

                            } else if (productIssuanceType.equals(ProductIssuanceType.GROUP_ONE_QUALIFICATION_CERTIFICATE)) {

                                report = new GroupOneQualificationCertificateReport();
                                ReportResourceUtils.processJavaBeanReport(report, ContentType.PDF, registrants, params);

                            } else if (productIssuanceType.equals(ProductIssuanceType.GROUP_TWO_QUALIFICATION_CERTIFICATE)) {

                                report = new GroupTwoQualificationCertificateReport();
                                ReportResourceUtils.processJavaBeanReport(report, ContentType.PDF, registrants, params);

                            } else if (productIssuanceType.equals(ProductIssuanceType.GROUP_THREE_QUALIFICATION_CERTIFICATE)) {

                                report = new GroupThreeQualificationCertificateReport();
                                ReportResourceUtils.processJavaBeanReport(report, ContentType.PDF, registrants, params);

                            } else if (productIssuanceType.equals(ProductIssuanceType.GROUP_FOUR_QUALIFICATION_CERTIFICATE)) {

                                report = new GroupFourQualificationCertificateReport();
                                ReportResourceUtils.processJavaBeanReport(report, ContentType.PDF, registrants, params);

                            } else if (productIssuanceType.equals(ProductIssuanceType.GROUP_FIVE_QUALIFICATION_CERTIFICATE)) {

                                report = new GroupFiveQualificationCertificateReport();
                                ReportResourceUtils.processJavaBeanReport(report, ContentType.PDF, registrants, params);

                            } else if (productIssuanceType.equals(ProductIssuanceType.GROUP_SIX_QUALIFICATION_CERTIFICATE)) {

                                report = new GroupSixQualificationCertificateReport();
                                ReportResourceUtils.processJavaBeanReport(report, ContentType.PDF, registrants, params);

                            } else if (productIssuanceType.equals(ProductIssuanceType.GROUP_SEVEN_QUALIFICATION_CERTIFICATE)) {

                                report = new GroupSevenQualificationCertificateReport();
                                ReportResourceUtils.processJavaBeanReport(report, ContentType.PDF, registrants, params);

                            } else if (productIssuanceType.equals(ProductIssuanceType.GROUP_EIGHT_QUALIFICATION_CERTIFICATE)) {

                                report = new GroupEightQualificationCertificateReport();
                                ReportResourceUtils.processJavaBeanReport(report, ContentType.PDF, registrants, params);

                            } else if (productIssuanceType.equals(ProductIssuanceType.GROUP_NINE_QUALIFICATION_CERTIFICATE)) {
                                report = new GroupNineQualificationCertificateReport();
                                ReportResourceUtils.processJavaBeanReport(report, ContentType.PDF, registrants, params);
                            } else if (productIssuanceType.equals(ProductIssuanceType.GROUP_TEN_QUALIFICATION_CERTIFICATE)) {
                                report = new GroupTenQualificationCertificateReport();
                                ReportResourceUtils.processJavaBeanReport(report, ContentType.PDF, registrants, params);
                            } else if (productIssuanceType.equals(ProductIssuanceType.PCN_REGISTRATION_CERTIFICATE)) {
                                report = new PCNRegistrationCertificateReport();
                                ReportResourceUtils.processJavaBeanReport(report, ContentType.PDF, registrants, params);
                            } else if (productIssuanceType.equals(ProductIssuanceType.PROVISIONAL_REGISTRATION_CERTIFICATE)) {
                                report = new ProvisionalRegistrationCertificateReport();
                                ReportResourceUtils.processJavaBeanReport(report, ContentType.PDF, registrants, params);
                            } else if (getModelObject().getProductIssuanceType().equals(ProductIssuanceType.UNRESTRICTED_PRACTICING_CERTIFICATE)) {

                                report = new UnrestrictedPracticingCertificateReport();
                                ReportResourceUtils.processJavaBeanReport(report, ContentType.PDF, registrants, params);

                            } else if (productIssuanceType.equals(ProductIssuanceType.SPECIAL_PRACTICING_CERTIFICATE)) {
                                /*for medical labs nurse practising certificate*/
                                report = new PracticingCertificateNurse();
                                ReportResourceUtils.processJavaBeanReport(report, ContentType.PDF, registrants, params);
                            }

                            getModelObject().setIssueDate(new Date());
                            getModelObject().setIssued(Boolean.TRUE);
                            productIssuanceService.save(getModelObject());
                            setResponsePage(getPage());
                        } catch (JRException ex) {
                            ex.printStackTrace();
                        }
                    }
                });
                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                item.add(new Label("registrant.fullname"));
                item.add(new Label("productNumber"));
                item.add(new Label("productIssuanceType"));
                item.add(new CustomDateLabel("issueDate"));
                item.add(new Label("expiryDate"));
                item.add(new Label("createdBy"));
                item.add(new Label("modifiedBy"));
                item.add(new CustomDateLabel("dateCreated"));
                item.add(new CustomDateLabel("dateModified"));

                Form<ProductIssuance> form = new Form<ProductIssuance>("form", new CompoundPropertyModel<ProductIssuance>(item.getModel())) {

                    @Override
                    protected void onSubmit() {

                        this.getModelObject().setIssueDate(new Date());
                        getModel().getObject().setIssued(Boolean.TRUE);
                        productIssuanceService.save(this.getModelObject());
                        setResponsePage(getPage());
                    }
                };
                item.add(form);
                item.setVisible(!item.getModelObject().getIssued());
                form.add(new CheckBox("issued"));
            }
        };

        add(new PagingNavigator("navigator", eachItem));
        add(eachItem);
    }

    private void initParams(Map<String, Object> params, Registrant registrant, ProductIssuance productIssuance) {

     RegistrantQualification currentQualification = registrantQualificationService.getRegistrantCurrentQualifications(registrant);

        Registration registration = registrationProcess.activeRegisterNotStudentRegister(registrant);

        if (currentQualification == null) {


            throw new CouncilException("Practitioner does not have qualification details");
        }

        Date dateOfCompletion = registrantQualificationService.getRegistrantCurrentQualifications(registrant).getEndDate();


        Boolean provisionalRegister = Boolean.FALSE;
        if (productIssuance.getRegister() != null) {
            if (productIssuance.getRegister().equals(generalParametersService.get().getProvisionalRegister())) {
                provisionalRegister = Boolean.TRUE;
            }
        }

        params.put("topLogo", TOP_LOGO);
        params.put("watermarkLogo", WATERMARK_LOGO);
        params.put("REGISTRATION_NUMBER_REHAB", registrant.getFormattedRegistrationNumber());
        params.put("BAR_CODE", String.valueOf(productIssuance.getBarCode()));
        params.put("duplicate", BooleanUtils.toString(productIssuance.getDuplicate(), "Duplicate", ""));

        if (productIssuance.getProductIssuanceType().equals(ProductIssuanceType.REGISTRATION_CERTIFICATE) || productIssuance.getProductIssuanceType().equals(ProductIssuanceType.PCN_REGISTRATION_CERTIFICATE) || productIssuance.getProductIssuanceType().equals(ProductIssuanceType.PROVISIONAL_REGISTRATION_CERTIFICATE)) {
            if (registrant.getRegistrationNumber() != null) {
                params.put("CERTIFICATE_NUMBER", registrant.getRegistrationNumber());
            } else {
                params.put("CERTIFICATE_NUMBER", "Error Missing");
            }
            if (provisionalRegister) {
                params.put("CERTIFICATETITLE", "PROVISIONAL REGISTRATION CERTIFICATE");
                params.put("REGISTEREDON", "is registered on the Provisional Register of");
                params.put("EXPIRY_COMMENT", "Expiry Date: ");
                params.put("EXPIRY_DATE", productIssuance.getExpiryDate());
                params.put("DATE_ISSUED", productIssuance.getCreateDate());
                params.put("PRA", productIssuance.getCourse().getPrefixName() + " " + registrant
                        .getRegistrationNumber());
            } else {

                params.put("CERTIFICATETITLE", "REGISTRATION CERTIFICATE");
                params.put("REGISTEREDON", "is registered on the register of");
                params.put("EXPIRY_DATE", null);
               // params.put("DATE_REGISTERED", dateOfRegistration);
                params.put("DATE_REGISTERED", registration.getRegistrationDate());


                params.put("DATE_ISSUED", productIssuance.getCreateDate());
                params.put("PRA", productIssuance.getCourse().getPrefixName() + " " + registrant
                        .getRegistrationNumber());
            }
            if (productIssuance.getProductIssuanceType().equals(ProductIssuanceType.PROVISIONAL_REGISTRATION_CERTIFICATE)) {
                params.put("DATE_REGISTERED", registration.getRegistrationDate());
            }


            params.put("REG_NUMBER", productIssuance.getCourse().getPrefixName() + " " + registrant.getRegistrationNumber());
            //for allied registration certificate
            if (generalParametersService.get().getAllowCapitalizationOfFirstLetters()) {
                params.put("ALLIED_REGISTER", productIssuance.getCourse().getRegisterType().getFormattedName());
            }

            if (generalParametersService.get().getRegistrationCertificateSerialIsIncrimental()) {
                params.put("Certnumber", productIssuance.getSerialNumber());
            }
        }

        if (productIssuance.getProductIssuanceType().equals(ProductIssuanceType.PCN_REGISTRATION_CERTIFICATE)) {
            params.put("PCNNUMBER", productIssuance.getCourse().getPrefixName() + " " + registrant
                    .getRegistrationNumber());

        }
        if (productIssuance.getProductIssuanceType().equals(ProductIssuanceType.PROVISIONAL_REGISTRATION_CERTIFICATE)) {
            params.put("PROVNUMBER", productIssuance.getCourse().getPrefixName() + " " + registrant
                    .getRegistrationNumber());

        } else if (productIssuance.getProductIssuanceType().equals(ProductIssuanceType.PRACTICING_CERTIFICATE)) {
            String cNo = productIssuance.getProductNumber();
            Integer c = cNo.toCharArray().length;

            if (c == 2) {
                cNo = "0000";
                cNo.concat(productIssuance.getProductNumber());
            } else if (c == 3) {
                cNo = "000";
                cNo.concat(productIssuance.getProductNumber());
            } else if (c == 4) {
                cNo = "00";
                cNo.concat(productIssuance.getProductNumber());
            } else if (c == 5) {
                cNo = "0";
                cNo.concat(productIssuance.getProductNumber());
            } else {
                cNo = productIssuance.getProductNumber();
            }
            List<ItemCase> envList = new ArrayList<ItemCase>();
            Set<ItemCase> envSet = new HashSet<ItemCase>();
            if (productIssuance.getCourse().getPrefixName().contains("EH")) {
                params.put("asa", "is authorised to practise as an");
                ItemCase itemCase1 = new ItemCase();
                itemCase1.setName(productIssuance.getCourse().getRegisterType().getIndividualName());
                envSet.add(itemCase1);
            } else {
                params.put("asa", "is authorised to practise as a");
                ItemCase itemCase1 = new ItemCase();
                if (productIssuance != null && productIssuance.getCourse() != null && productIssuance.getCourse().getRegisterType() != null) {
                    itemCase1.setName(productIssuance.getCourse().getRegisterType().getIndividualName());
                }
                envSet.add(itemCase1);
            }
            List<ReceiptHeader> receipts = new ArrayList<ReceiptHeader>();
            if (accountsParametersService.get().getPrintReceiptNumberOnCertificate()) {
                if (productIssuance.getDuration() != null) {
                    receipts = receiptHeaderService.getReceiptHeaderByCouncilDurationAndAccountAndTypeOfService(productIssuance.getDuration()
                            .getCouncilDuration(), productIssuance.getRegistrant().getCustomerAccount().getAccount(), TypeOfService.ANNUAL_FEES);

                    if (generalParametersService.get().getStudentRegister() == productIssuance.getRegister()) {
                        receipts.addAll(receiptHeaderService.getReceiptHeaderByCouncilDurationAndAccountAndTypeOfService(productIssuance.getDuration()
                                .getCouncilDuration(), productIssuance.getRegistrant().getCustomerAccount().getAccount(), TypeOfService.STUDENT_APPLICATION_FEES));
                    }

                    String receiptNumbers = "";
                    String paymentMethods = "";
                    BigDecimal payedamount=null;

                    for (ReceiptHeader receipt : receipts) {

                        receiptNumbers = receiptNumbers + receipt.getId().toString() + "/";
                        paymentMethods = paymentMethods + receipt.getPaymentDetails().getPaymentMethod().toString() + "/";
                        payedamount=receipt.getTotalAmount();
                    }

                    if (paymentMethods.length() > 0) {
                        params.put("PAYMENT_METHOD", paymentMethods.substring(0, paymentMethods.lastIndexOf("/")));
                        params.put("AMOUNT",payedamount);
                    }

                    if (receiptNumbers.length() > 0) {
                        params.put("RECEIPT_NUMBER", "RC" + receiptItemService.ListOfPaidAnnualFeeForCouncilDuration(registrant.getCustomerAccount().getAccount(), productIssuance.getDuration().getCouncilDuration()).toString());
                    }

                }
            }
            for (ItemCase ic : envSet) {
                envList.add(ic);
            }

            params.put("SUB_DATA_SOURCE_ENV", envList);
            params.put("CERTIFICATE_NUMBER", "Certificate No: PRA " + cNo + productIssuance.getProductNumber());
            params.put("CERTIFICATE_NUMBER_REHAB", productIssuance.getPracticeCertificateNumber());
            params.put("REGISTRANT_NAME_REHAB", StringFormattingUtil.join(registrant.getTitle(), registrant
                    .getFullname()));

            int durationYear = 0;

            if (productIssuance.getDuration() != null) {
                durationYear = DateUtil.getYearFromDate(productIssuance.getDuration().getStartDate());
            }
            int durationYearPart = durationYear % 100;
            String certificateNumber = String.format("%s.%s/%s", productIssuance.getCourse().getPrefixName(),
                    productIssuance.getPracticeCertificateNumber(), durationYearPart);
            params.put("CERTIFICATE_NUMBER_PCZ", certificateNumber);

            if (productIssuance.getDuration() != null) {
                params.put("RENEWAL_PERIOD_FOR", DateUtil.getDate(productIssuance.getDuration().getStartDate())
                        + "   TO   " + DateUtil.getDate(productIssuance.getDuration().getEndDate()));
            }

            params.put("ANNUAL_FEE_AMOUNT", productFinder.searchAnnualProductPrice(productIssuance.getCourse(),
                    productIssuance.getRegister(), null, TypeOfService.ANNUAL_FEES).toString());

            params.put("PRA", "Certificate No: PRA" + productIssuance.getPracticeCertificateNumber());
            params.put("DATE_ISSUED", new Date());
            Set<ItemCase> list = new HashSet<ItemCase>();
            Set<ItemCase> lista = new HashSet<ItemCase>();
            //TODO : Added A list of Course to Remove the Required Courses
            ItemCase GN = new ItemCase("GENERAL NURSE");
            ItemCase PEADONLY = new ItemCase("PAEDIATRIC NURSE ONLY");
            ItemCase MD = new ItemCase("MIDWIFE");
            ItemCase MDONLY = new ItemCase("MIDWIFE ONLY");
            ItemCase PCN = new ItemCase("PRIMARY CARE NURSE");
            ItemCase PYSC = new ItemCase("PSYCHIATRIC NURSE");
            ItemCase PYSCONLY = new ItemCase("PSYCHIATRY NURSE ONLY");
            ItemCase PYSCDIPLOMA = new ItemCase("PSYCHIATRIC MENTAL HEALTH NURSE(THREE YEAR DIPLOMA)");
            ItemCase SCN = new ItemCase("STATE CERTIFIED NURSE");
            ItemCase SCTN = new ItemCase("STATE CERTIFIED TRAUMATOLOGY NURSE");
            ItemCase SCMN = new ItemCase("STATE CERTIFIED MATERNITY NURSE");
            ItemCase SCMNONLY = new ItemCase("STATE CERTIFIED MATERNITY NURSE ONLY");
            List<RegistrantQualification> qualifications = registrantQualificationService.getRegistrantQualifications(registrant);

            for (RegistrantQualification rq : qualifications) {
                if (rq.getCourse() != null && rq.getCourse().getRegisterType() != null && rq.getCourse()
                        .getRegisterType().getIndividualName() != null) {
                    ItemCase itemCase = new ItemCase();
                    itemCase.setName(rq.getCourse().getRegisterType().getIndividualName());
                    itemCase.setDateAwarded(rq.getDateAwarded());
                    if (itemCase.equals(GN)
                            || itemCase.equals(MD)
                            || itemCase.equals(MDONLY)
                            || itemCase.equals(PCN)
                            || itemCase.equals(PYSC)
                            || itemCase.equals(PYSCONLY)
                            || itemCase.equals(PYSCDIPLOMA)
                            || itemCase.equals(SCN)
                            || itemCase.equals(SCTN)
                            || itemCase.equals(SCMN)
                            || itemCase.equals(SCMNONLY)
                            || itemCase.equals(PEADONLY)) {
                        list.add(itemCase);
                    } else {
                        itemCase.setName(rq.getQualification().getName());
                        lista.add(itemCase);//PBQ List
                    }
                } else {
                    ItemCase itemCase = new ItemCase();
                    itemCase.setDateAwarded(rq.getDateAwarded());
                    itemCase.setName(rq.getQualification().getName());
                    lista.add(itemCase);//PBQ List
                }
            }

            List<ItemCase> qlist = new ArrayList<ItemCase>();
            for (ItemCase item : list) {
                qlist.add(item);
            }

            List<ItemCase> pbqlist = new ArrayList<ItemCase>();
            for (ItemCase item : lista) {
                pbqlist.add(item);
            }

            // ***********Remove the General Nurse the primary qualifications list ***************
            if (list.contains(GN)) {
                qlist.remove(GN);
            }

            // ***********Remove Basic if MD is in list  ***************
            if (list.contains(MD)) {
                qlist.remove(SCMN);
                qlist.remove(SCMNONLY);
            }

            // ***********Remove Basic if GN is in list  ***************
            if (list.contains(GN)) {
                qlist.remove(SCN);
                qlist.remove(SCTN);
            }

            // *********** Sort list Primary Qualifications ***************
            List<ItemCase> sortedItemCases = new ArrayList<ItemCase>((ArrayList<ItemCase>) HrisComparator
                    .sortItemCasesByDate(qlist));

            // ***********Add General Nurse as the first qualification in the primary qualifications list if obtained
            // ***************
            if (list.contains(GN)) {
                sortedItemCases.add(0, GN);
            }

            // *********** Sort list PBQ Qualifications ***************
            List<ItemCase> sortedPBQItemCases = new ArrayList<ItemCase>((ArrayList<ItemCase>) HrisComparator
                    .sortItemCasesByDate(pbqlist));

            params.put("SUB_DATA_SOURCE", sortedItemCases);
            params.put("SUB_DATA_SOURCE_ONE", new ArrayList<ItemCase>((ArrayList<ItemCase>) HrisComparator
                    .sortItemCasesByDate(sortedPBQItemCases)));
        } else if (productIssuance.getProductIssuanceType().equals(ProductIssuanceType.QUALIFICATION_CERTIFICATE)
                || productIssuance.getProductIssuanceType().equals(ProductIssuanceType.GROUP_ONE_QUALIFICATION_CERTIFICATE)
                || productIssuance.getProductIssuanceType().equals(ProductIssuanceType.GROUP_TWO_QUALIFICATION_CERTIFICATE)
                || productIssuance.getProductIssuanceType().equals(ProductIssuanceType.GROUP_THREE_QUALIFICATION_CERTIFICATE)
                || productIssuance.getProductIssuanceType().equals(ProductIssuanceType.GROUP_FOUR_QUALIFICATION_CERTIFICATE)
                || productIssuance.getProductIssuanceType().equals(ProductIssuanceType.GROUP_FIVE_QUALIFICATION_CERTIFICATE)
                || productIssuance.getProductIssuanceType().equals(ProductIssuanceType.GROUP_SIX_QUALIFICATION_CERTIFICATE)
                || productIssuance.getProductIssuanceType().equals(ProductIssuanceType.GROUP_SEVEN_QUALIFICATION_CERTIFICATE)
                || productIssuance.getProductIssuanceType().equals(ProductIssuanceType.GROUP_EIGHT_QUALIFICATION_CERTIFICATE)
                || productIssuance.getProductIssuanceType().equals(ProductIssuanceType.GROUP_NINE_QUALIFICATION_CERTIFICATE)
                || productIssuance.getProductIssuanceType().equals(ProductIssuanceType.GROUP_TEN_QUALIFICATION_CERTIFICATE)) {

            params.put("DATE_ISSUED", productIssuance.getExamDate());
            params.put("CLEARANCE_DATE", registrantClearanceService.getRegistrantClearanceDate(registrant,
                    productIssuance.getCourse()));
            params.put("enddate", dateOfCompletion);

            params.put("CERTIFICATE_NUMBER", productIssuance.getCourse().getPrefixName() + " : " + productIssuance
                    .getDiplomaCertificateNumber());
            params.put("PRA", productIssuance.getDiplomaCertificateNumber());
            params.put("REGISTER_TYPE", productIssuance.getCourse().getRegisterType().getAliasName());
        } else {
            params.put("DATE_ISSUED", new Date());
            params.put("CERTIFICATE_NUMBER", productIssuance.getProductNumber());
            params.put("PRA", productIssuance.getProductNumber());
        }

        if (generalParametersService.get().getAllowCapitalizationOfFirstLetters()) {
            params.put("REGISTRANT_NAME", registrant.getFormattedFullname());
            params.put("COURSE", productIssuance.getCourse().getFormattedName());
            if (productIssuance.getCourse().getRegisterType() != null) {
                params.put("COURSE_TYPE", productIssuance.getCourse().getRegisterType().getFormattedName());
            }
        } else {
            params.put("REGISTRANT_NAME", registrant.getFullname());
            params.put("COURSE", productIssuance.getCourse().getName());
            if (productIssuance.getCourse().getRegisterType() != null) {
                params.put("COURSE_TYPE", productIssuance.getCourse().getRegisterType().getName());
            }
        }
        params.put("REGISTRATION_NUMBER", registrant.getRegistrationNumber());
        params.put("PREFIX", productIssuance.getCourse().getPrefixName() + " NUMBER: ");
        params.put("EXPIRY_DATE", productIssuance.getExpiryDate());

        //---------------------certificate of good standing----------------
        RegistrantAddress registrantAddress = registrantAddressService.getActiveAddress(registrant);
        if (registrantAddress != null) {
            params.put("ADDRESS", registrantAddress.getFullAddress());
            params.put("ADD", registrantAddress.getAddress1() + " " + registrantAddress.getAddress2());

            params.put("CITY", registrantAddress.getCity() == null ? "" : registrantAddress.getCity().getName());
        } else {
            params.put("ADDRESS", "");
        }

        Calendar calendar = Calendar.getInstance();

        params.put("DAY", String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
        params.put("MONTH", String.valueOf(calendar.get(Calendar.MONTH)));
        params.put("YEAR", String.valueOf(calendar.get(Calendar.YEAR)));

        if (productIssuance.getProductIssuanceType().equals(ProductIssuanceType.ADDITIONAL_QUALIFICATION_CERTIFICATE)) {
            params.put("COURSE", "GENERAL NURSE");
            params.put("SUB_DATA_SOURCE", registrantQualificationService.getRegistrantQualificationCertificateType(registrant, ProductIssuanceType.ADDITIONAL_QUALIFICATION_CERTIFICATE));
        } else {
            if (productIssuance.getCourse().getRegisterType() != null) {
                params.put("PROFESSION", productIssuance.getCourse().getRegisterType().getIndividualName());
            }
        }

        StringBuilder conditions = new StringBuilder();
        if (generalParametersService.get().getHasProcessedByBoard()) {

            for (RegistrantCondition registrantCondition
                    : registrantConditionService.getRegistrantActiveConditions(registrant)) {

                conditions.append(registrantCondition.toString()).append(". ");

            }
            //print course && register Condition for Registrants without setting it for an each individual
            for (Conditions c : courseConditionService.getConditions(productIssuance.getCourse(), productIssuance
                    .getRegister())) {
                conditions.append(c.getComments()).append(". ");
            }

            params.put("conditions", conditions.toString());

        } else {
            //TODO : DANEIL CHOOSE REGISTER CONDITIONS
            if (productIssuance.getProductIssuanceType().equals(ProductIssuanceType.PRACTICING_CERTIFICATE)) {

                //all active conditions
                for (RegistrantCondition registrantCondition
                        : registrantConditionService.getRegistrantActiveConditions(registrant)) {

                    if (registrantCondition.getPersonal()) {
                        conditions.append(registrantCondition.getDescription()).append(". ");
                    } else {
                        conditions.append(registrantCondition.getCondition().getComments()).append(". ");
                    }

                }
            }
            if (productIssuance.getProductIssuanceType().equals(ProductIssuanceType.REGISTRATION_CERTIFICATE)) {

                //register conditions only
                for (RegistrantCondition registrantCondition
                        : registrantConditionService.getRegisterRegistrantConditions(registrant)) {

                    conditions.append(registrantCondition.getCondition().getComments()).append(". ");
                }
            }
            if (conditions.length() == 0) {
                params.put("conditions", "Nil");
            } else {
                params.put("conditions", conditions.toString());
            }
        }

        if (productIssuance.getProductIssuanceType().equals(ProductIssuanceType.CERTIFICATE_OF_GOOD_STANDING)) {
            Set<ItemCase> items = new HashSet<ItemCase>();
            if (!generalParametersService.get().getAllowPreRegistrantion() && !generalParametersService.get()
                    .getRegistrationByApplication()) {

                /* Selected Qualifications start awarded days and registered days
                 show transfer when obtaining a qualification
                 STATE CERTIFIED NURSE to DIPLOMA IN GENERAL NURSES
                 STATE CERTIFIED MATERNITY ASSISTANT to DIPLOMA IN GENERAL NURSES
                 STATE CERTIFIED MATERNITY NURSE to MIDWIFERY DIPLOMA */
                for (RegistrantQualification qualification : registrantQualificationService
                        .getRegistrantQualifications(registrant)) {
                    ItemCase itemCase = new ItemCase();
                    if (checkCourseRegisterTypeAndIndividualName(qualification)) {
                        if (qualification.getQualification().getName().equals("STATE CERTIFIED NURSE")
                                || qualification.getQualification().getName().equals("STATE CERTIFIED TRAUMATOLOGY "
                                        + "NURSE")) {
                            RegistrantQualification registrantQualification = registrantQualificationService.get(registrant, "DIPLOMA IN GENERAL NURSES");

                            if (registrantQualification != null) {
                                itemCase.setName(registrantQualification.getCourse().getRegisterType()
                                        .getIndividualName() + " (" + DateUtil.getDate(registrantQualification
                                                .getDateAwarded()) + " - " + DateUtil.getDate(productIssuance.getExpiryDate()
                                        ) + ")");
                            } else {
                                itemCase.setName(qualification.getCourse().getRegisterType().getIndividualName() + " "
                                        + "(" + DateUtil.getDate(qualification.getDateAwarded()) + " - " + DateUtil
                                        .getDate(productIssuance.getExpiryDate()) + ")");
                            }
                            items.add(itemCase);
                        } else if (qualification.getQualification().getName().equals("STATE CERTIFIED MATERNITY "
                                + "ASSISTANT")) {
                            RegistrantQualification registrantQualification = registrantQualificationService.get(registrant, "MIDWIFERY DIPLOMA");

                            if (registrantQualification != null) {
                                itemCase.setName(qualification.getCourse().getRegisterType().getIndividualName() + " "
                                        + "(" + DateUtil.getDate(registrantQualification.getDateAwarded()) + " - "
                                        + DateUtil.getDate(productIssuance.getExpiryDate()) + ")");
                            } else {
                                itemCase.setName(qualification.getCourse().getRegisterType().getIndividualName() + " "
                                        + "(" + DateUtil.getDate(qualification.getDateAwarded()) + " - " + DateUtil
                                        .getDate(productIssuance.getExpiryDate()) + ")");
                            }
                            items.add(itemCase);

                        } else {
                            itemCase.setName(qualification.getCourse().getRegisterType().getIndividualName() + " ("
                                    + DateUtil.getDate(qualification.getDateAwarded()) + " - " + DateUtil.getDate(productIssuance.getExpiryDate()) + ")"); //   discipline
                            items.add(itemCase);
                        }
                    }
                }
            } else {
                for (RegistrantQualification qualification : registrantQualificationService
                        .getRegistrantQualifications(registrant)) {
                    ItemCase itemCase = new ItemCase();
                    if (generalParametersService.get().getRegistrationByApplication()) {
                        itemCase.setName(qualification.getQualification().getName()); //   qualification
                    } else if (checkCourseRegisterTypeAndIndividualName(qualification)) {
                        itemCase.setName(qualification.getCourse().getRegisterType().getIndividualName() + " ("
                                + DateUtil.getDate(qualification.getDateAwarded()) + " - " + DateUtil.getDate(productIssuance.getExpiryDate()) + ")"); //   discipline
                    }
                    items.add(itemCase);
                }
            }

            //cgs pdq
            ItemCase GN = new ItemCase("GENERAL NURSE");
            ItemCase PEADONLY = new ItemCase("PAEDIATRIC NURSE ONLY");
            ItemCase MD = new ItemCase("MIDWIFE");
            ItemCase MDONLY = new ItemCase("MIDWIFE ONLY");
            ItemCase PCN = new ItemCase("PRIMARY CARE NURSE");
            ItemCase PYSC = new ItemCase("PSYCHIATRIC NURSE");
            ItemCase PYSCONLY = new ItemCase("PSYCHIATRY NURSE ONLY");
            ItemCase PYSCDIPLOMA = new ItemCase("PSYCHIATRIC MENTAL HEALTH NURSE(THREE YEAR DIPLOMA)");
            ItemCase SCN = new ItemCase("STATE CERTIFIED NURSE");
            ItemCase SCTN = new ItemCase("STATE CERTIFIED TRAUMATOLOGY NURSE");
            ItemCase SCMN = new ItemCase("STATE CERTIFIED MATERNITY NURSE");
            ItemCase SCMNONLY = new ItemCase("STATE CERTIFIED MATERNITY NURSE ONLY");

            List<RegistrantQualification> qualifications = registrantQualificationService.getRegistrantQualifications(registrant);
            Set<ItemCase> lista = new HashSet<ItemCase>();

            for (RegistrantQualification rq : qualifications) {
                if (rq.getCourse() != null && rq.getCourse().getRegisterType() != null && rq.getCourse()
                        .getRegisterType().getIndividualName() != null) {
                    ItemCase itemCase = new ItemCase();
                    itemCase.setName(rq.getCourse().getRegisterType().getIndividualName());
                    itemCase.setDateAwarded(rq.getDateAwarded());
                } else {
                    ItemCase itemCase = new ItemCase();
                    itemCase.setDateAwarded(rq.getDateAwarded());
                    itemCase.setName(rq.getQualification().getName() + "(" + DateUtil.getDate(rq.getDateAwarded()) + "- " + DateUtil.getDate(productIssuance.getExpiryDate()) + ")");
                    lista.add(itemCase);//PBQ List
                }
            }
            List<ItemCase> pbqlist = new ArrayList<ItemCase>();
            for (ItemCase item : lista) {
                pbqlist.add(item);
            }
            // *********** Sort list PBQ Qualifications ***************
            List<ItemCase> sortedPBQItemCases = new ArrayList<ItemCase>((ArrayList<ItemCase>) HrisComparator
                    .sortItemCasesByDate(pbqlist));

            params.put("SUB_DATA_SOURCE", new ArrayList<ItemCase>(items));
            params.put("SUB_DATA_SOURCE_2", new ArrayList<ItemCase>(pbqlist));
            params.remove("EXPIRY_DATE");
            params.put("EXPIRY_DATE", productIssuance.getExpiryDate());
            items = new HashSet<ItemCase>();
            Boolean disciplinary = Boolean.TRUE;
            for (RegistrantDisciplinary d : registrantDisciplinaryService.getGuiltyResolvedCases(registrant)) {
                ItemCase itemCase = new ItemCase();
                itemCase.setName(d.getMisconductType().getName());
                items.add(itemCase);
            }
            if (items.isEmpty()) {
                ItemCase itemCase = new ItemCase();
                disciplinary = Boolean.FALSE;
                itemCase.setName("N/A");
                items.add(itemCase);
            }
            params.put("SUB_DATA_SOURCE_1", new ArrayList<ItemCase>(items));
            String pendingCases = registrantDisciplinaryService.getPendingCases(registrant);
            params.put("cases", pendingCases);
            if (pendingCases.equalsIgnoreCase("YES") && !disciplinary) {
                params.put("state", "TBA");
            } else if (pendingCases.equalsIgnoreCase("YES") && disciplinary) {
                params.put("state", "NO");
            } else if (pendingCases.equalsIgnoreCase("NO") && disciplinary) {
                params.put("state", "NO");
            } else {
                params.put("state", "YES");
            }
            Date firstRegistrationDate = new Date();
            for (Registration r : registrationService.getRegistrations(registrant)) {
                if (!r.getRegister().equals(generalParametersService.get().getStudentRegister())) {
                    if (r.getRegistrationDate() != null) {
                        firstRegistrationDate = r.getRegistrationDate();
                        break;
                    }
                }
            }
            params.put("CLEARANCE_DATE", firstRegistrationDate);
            params.remove("PRA");
            params.put("PRA", "CGS Number : " + productIssuance.getCgsCertificateNumber());
        }
    }

    private boolean checkCourseRegisterTypeAndIndividualName(RegistrantQualification qualification) {
        return qualification.getCourse() != null && qualification.getCourse().getRegisterType() != null
                && qualification.getCourse().getRegisterType().getIndividualName() != null;
    }

}
