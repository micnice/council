package zw.co.hitrac.council.web.pages.accounts.payments;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.ProductIssuance;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.TypeOfService;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.domain.accounts.PaymentDetails;
import zw.co.hitrac.council.business.domain.accounts.ReceiptItem;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.process.RenewalProcess;
import zw.co.hitrac.council.business.service.DurationService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.ProductIssuanceService;
import zw.co.hitrac.council.business.service.RegistrantActivityService;
import zw.co.hitrac.council.business.service.RegistrantCpdItemConfigService;
import zw.co.hitrac.council.business.service.RegistrantCpdService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.business.service.accounts.PaymentDetailsService;
import zw.co.hitrac.council.business.service.accounts.ReceiptItemService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.certification.CertificateDataListPanel;
import zw.co.hitrac.council.web.pages.institution.InstitutionViewPage;
import zw.co.hitrac.council.web.pages.registration.RegistrantViewPage;

/**
 * @author Michael Matiashe
 */
public class PaymentConfirmationPage extends TemplatePage {

    @SpringBean
    PaymentDetailsService paymentDetailsService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    RegistrantService registrantService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private ReceiptItemService receiptItemService;
    @SpringBean
    private ProductIssuanceService productIssuanceService;
    @SpringBean
    private DebtComponentService debtComponentService;
    private Registrant registrant = null;
    private Institution institution = null;

    @SpringBean
    private DurationService durationService;
    @SpringBean
    private RegistrantCpdService registrantCpdService;
    @SpringBean
    private RegistrationService registrationService;
    @SpringBean
    private RegistrationProcess registrationProcess;
    @SpringBean
    private RegistrantCpdItemConfigService registrantCpdItemConfigService;
    @SpringBean
    private RegistrantActivityService registrantActivityService;
    @SpringBean
    private RenewalProcess renewalProcess;

    public PaymentConfirmationPage(final PaymentDetails paymentDetails, final Account personalAccount) {

        final CompoundPropertyModel<PaymentDetails> model = new CompoundPropertyModel<PaymentDetails>(new PaymentConfirmationPage.LoadableDetachablePaymentDetailsModel(paymentDetails.getId()));
        setDefaultModel(model);
        final CompoundPropertyModel<Customer> customerModel = new CompoundPropertyModel<Customer>(new PaymentConfirmationPage.LoadableDetachableCustomerDetailsModel(paymentDetails.getId()));

        add(new TransactionDetailHistoryPanel("transactionDetailHistoryPanel", model.getObject().getId()));

        add(new Label("penaltyClear", new LoadableDetachableModel<String>() {
            @Override
            protected String load() {
                if (institution != null) {
                    return "";
                } else if (renewalProcess.hasPendingAccountDebtComponentsByTypeOfService(registrant, TypeOfService.PENALTY_FEES)) {
                    return "Certificates Cannot be Printed, OutStanding Penalty Fees";
                } else {
                    return "";
                }
            }

        }));

        add(new Label("annualFeeArea", new LoadableDetachableModel<String>() {
            @Override
            protected String load() {
                if (institution != null) {
                    return "";
                } else if (renewalProcess.hasPendingAccountDebtComponentsByTypeOfService(registrant, TypeOfService.ANNUAL_FEES)) {
                    return "Certificates Cannot be Printed, OutStanding Annual Fees";
                } else {
                    return "";
                }
            }

        }));

        add(new Label("checkCurrentForCPDS", new LoadableDetachableModel<String>() {
            @Override
            protected String load() {
                if (institution != null) {
                    return "";
                } else if (!renewalProcess.hasCurrentAdequateCPDs(registrant)) {
                    return "Certificates Cannot be Printed, CPD Points Required for Current Period";
                } else {
                    return "";
                }
            }

        }));

        IModel<List<ProductIssuance>> practicingCertificateModel = new LoadableDetachableModel<List<ProductIssuance>>() {
            @Override
            protected List<ProductIssuance> load() {
                if (registrant != null) {
                    if (!generalParametersService.get().getHasProcessedByBoard()) {
                        return productIssuanceService.getProductAllIssuances(registrant);
                    }
                    if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                        return productIssuanceService.getProductAllIssuances(registrant);
                    }
                    return productIssuanceService.getUnIssuedPractisingCertificates(registrant);
                } else {
                    return new ArrayList<ProductIssuance>();
                }
            }
        };


        add(new CertificateDataListPanel("certificateDataListPanel", practicingCertificateModel) {
            @Override
            protected void onConfigure() {

                final Boolean hasPenalty = debtComponentService.hasAccountDebtComponents(customerModel.getObject(), TypeOfService.PENALTY_FEES);
                final Boolean annualFeeArea = debtComponentService.hasAccountDebtComponents(customerModel.getObject(), TypeOfService.ANNUAL_FEES);
                final Boolean hasAdequateCPDs = renewalProcess.hasCurrentAdequateCPDs(registrant);

                if (hasPenalty && annualFeeArea || !hasAdequateCPDs) {
                    setVisible(Boolean.FALSE);
                }
            }

            @Override
            public boolean isVisible() {
                if (institution != null) {
                    return false;
                }
                return true;
            }
        });

        final List<ReceiptItem> prReceiptItems = receiptItemService.getReceiptItemsWithNoReceiptHeader(personalAccount);

        add(new Link<Customer>("registrationOptionPage") {
            @Override
            public void onClick() {

                if (registrant != null) {
                    setResponsePage(new RegistrantViewPage(registrant.getId()));
                }

                if (institution != null) {
                    setResponsePage(new InstitutionViewPage(institution.getId()));
                }
            }
        });
        add(new PropertyListView<ReceiptItem>("prepaidReceiptItems", prReceiptItems) {
            @Override
            protected void populateItem(ListItem<ReceiptItem> item) {
                item.add(new Label("amount"));
                item.add(new Label("debtComponent.transactionComponent.account.name"));
            }

            @Override
            public boolean isVisible() {
                if (institution != null) {
                    return false;
                }
                return true;
            }
        });
        add(new FeedbackPanel("feedback"));
        add(new Label("customer.customerName"));

    }

    private final class LoadableDetachablePaymentDetailsModel extends LoadableDetachableModel<PaymentDetails> {

        private Long id;

        public LoadableDetachablePaymentDetailsModel(Long id) {
            this.id = id;
        }

        @Override
        protected PaymentDetails load() {
            return paymentDetailsService.get(id);
        }
    }

    private final class LoadableDetachableCustomerDetailsModel extends LoadableDetachableModel<Customer> {

        private Long id;

        public LoadableDetachableCustomerDetailsModel(Long id) {
            this.id = id;
        }

        @Override
        protected Customer load() {
            PaymentDetails paymentDetails = paymentDetailsService.get(id);
            registrant = registrantService.getRegistrant(paymentDetails.getCustomer());
            if (registrant != null) {
                return registrant.getCustomerAccount();
            }
            institution = institutionService.getInstitution(paymentDetails.getCustomer());
            if (institution != null) {
                return institution.getCustomerAccount();
            }
            throw new CouncilException("Account Not Found");
        }
    }

}
