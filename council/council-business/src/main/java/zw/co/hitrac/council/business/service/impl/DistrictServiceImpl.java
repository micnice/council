package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.DistrictDAO;
import zw.co.hitrac.council.business.domain.District;
import zw.co.hitrac.council.business.domain.Province;
import zw.co.hitrac.council.business.service.DistrictService;

import java.util.List;

/**
 * @author Constance Mabaso
 */
@Service
@Transactional
public class DistrictServiceImpl implements DistrictService {

    @Autowired
    private DistrictDAO districtDAO;

    @Transactional
    public District save(District district) {

        return districtDAO.save(district);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<District> findAll() {

        return districtDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public District get(Long id) {

        return districtDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<District> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setDistrictDAO(DistrictDAO districtDAO) {

        this.districtDAO = districtDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<District> getDistrictsInProvince(Province province) {

        return districtDAO.getDistrictsInProvince(province);
    }
}
