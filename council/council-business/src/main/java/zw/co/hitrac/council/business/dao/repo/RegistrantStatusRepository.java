package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.RegistrantStatus;

/**
 *
 * @author Michael Matiashe
 */
public interface RegistrantStatusRepository extends CrudRepository<RegistrantStatus, Long> {
    public List<RegistrantStatus> findAll();
}
