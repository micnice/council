/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.utils.CaseUtil;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 *
 * @author Constance Mabaso
 */
@Entity
@Table(name = "registrantcontact")
@Audited
@XmlRootElement
public class RegistrantContact extends BaseIdEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    private String contactDetail;
    private Boolean status = Boolean.FALSE;
    private ContactType contactType;
    private Registrant registrant;
    private Institution institution;

    public RegistrantContact() {
    }

    @Basic(optional = false)
    @Column(name = "contact_detail", nullable = false, length = 255)
    public String getContactDetail() {
        return (contactDetail);
    }

    public void setContactDetail(String contactDetail) {
        this.contactDetail = contactDetail;
    }

    @Basic(optional = false)
    @Column(name = "Status", nullable = false)
    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Transient
    public String getPreferred() {
        return status ? "Active" : "Inactive";
    }

    @ManyToOne
    public ContactType getContactType() {
        return contactType;
    }

    public void setContactType(ContactType contactType) {
        this.contactType = contactType;
    }

    @ManyToOne
    public Registrant getRegistrant() {
        return registrant;
    }

    public void setRegistrant(Registrant registrant) {
        this.registrant = registrant;
    }

    @ManyToOne
    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    @Override
    public String toString() {
        return getContactDetail();
    }

    @XmlElement
    @Transient
    public String getFullDetail() {
        return getContactType() + " : " + getContactDetail();
    }

}
