package zw.co.hitrac.council.web.menu;

import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.pages.configure.AdministerDatabasePage;
import zw.co.hitrac.council.web.pages.configure.ConfigureSystemPage;
import zw.co.hitrac.council.web.pages.configure.ReportViewsPage;
import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;

/**
 *
 * @author Charles Chigoriwa
 */
public class ConfigureMenuPanel extends MenuPanel {

    private boolean administerDatabaseMenuCurrent;
    private boolean reportViewsMenuCurrent;
    private Link<Void> configureSystemLink;
    private Link<Void> administerDatabaseLink;
    private Link<Void> reportViewsLink;
    private WebMarkupContainer childMenuLinks;

    public ConfigureMenuPanel(String id) {
        super(id);
        add(configureSystemLink = new BookmarkablePageLink<Void>("configureSystemLink", ConfigureSystemPage.class) {
            @Override
            protected void onConfigure() {
                setVisibilityAllowed(CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR))));
            }
        });
        add(childMenuLinks = new WebMarkupContainer("childMenuLinks"));
        childMenuLinks.add(administerDatabaseLink = new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        childMenuLinks.add(reportViewsLink = new BookmarkablePageLink<Void>("reportViewsLink", ReportViewsPage.class));
    }

    @Override
    protected void onConfigure() {
        childMenuLinks.setVisibilityAllowed(topMenuCurrent);
        addCurrentBehavior(configureSystemLink, topMenuCurrent);
        addCurrentBehavior(administerDatabaseLink, administerDatabaseMenuCurrent);
        addCurrentBehavior(reportViewsLink, reportViewsMenuCurrent);
    }

    public boolean isAdministerDatabaseMenuCurrent() {
        return administerDatabaseMenuCurrent;
    }

    public void setAdministerDatabaseMenuCurrent(boolean administerDatabaseMenuCurrent) {
        this.administerDatabaseMenuCurrent = administerDatabaseMenuCurrent;
    }

    public boolean isReportViewsMenuCurrent() {
        return reportViewsMenuCurrent;
    }

    public void setReportViewsMenuCurrent(boolean reportViewsMenuCurrent) {
        this.reportViewsMenuCurrent = reportViewsMenuCurrent;
    }
}
