package zw.co.hitrac.council.web.pages.institution;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.InstitutionCategory;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.CityListModel;
import zw.co.hitrac.council.web.models.DistrictListModel;
import zw.co.hitrac.council.web.pages.application.CgsApplicationPage;
import zw.co.hitrac.council.web.pages.configure.CityEditPage;
import zw.co.hitrac.council.web.pages.configure.CountryEditPage;
import zw.co.hitrac.council.web.pages.configure.DistrictEditPage;
import zw.co.hitrac.council.web.pages.configure.ProvinceEditPage;
import zw.co.hitrac.council.web.pages.registration.RegistrantQualificationPage;
import zw.co.hitrac.council.web.pages.search.SearchSupervisorPage;

import java.util.Arrays;

//~--- JDK imports ------------------------------------------------------------
/**
 *
 * @author Tatenda Chiwandire
 * @author Michael Matiashe
 * @author tdhlakama
 */
public class InstitutionEditPage extends IInstitutionView {

    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private InstitutionTypeService institutionTypeService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private DistrictService districtService;
    @SpringBean
    private CityService cityService;

    public InstitutionEditPage(Long id, final Registrant... registrants) {
        final CompoundPropertyModel<Institution> model = new CompoundPropertyModel<Institution>(new InstitutionEditPage.LoadableDetachableInstitutionModel(id));
        Boolean registrant = Boolean.FALSE;
        if (!generalParametersService.get().getGenerateRegnumberOnRegistrant());
        {
            if (registrants != null && registrants.length > 0) {
                model.getObject().setRegistrant(registrants[0]);
            }

            if (model.getObject().getRegistrant() != null) {
                registrant = Boolean.TRUE;
            }
        }

        final Boolean hasRegistrant = registrant;
        setDefaultModel(model);
        Form<Institution> form = new Form<Institution>("form", (IModel<Institution>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                if (registrants != null && registrants.length > 0) {
                    getModelObject().setRegistrant(registrants[0]);
                }

                if (getModelObject().getRetired()) {
                    getModelObject().setRegistrant(null);
                    getModelObject().setSupervisor(null);
                }
                
                Institution institution = institutionService.save(getModelObject());

                if (!generalParametersService.get().getHasProcessedByBoard()) {
                    setResponsePage(new InstitutionContactAddressPage(getModelObject().getId()));
                } else if (institution.getInstitutionType().equals(generalParametersService.get().getHealthInstitutionType())) {
                    setResponsePage(new InstitutionContactAddressPage(getModelObject().getId()));
                } else if (!generalParametersService.get().getRegistrationByApplication()) {
                    setResponsePage(new InstitutionContactAddressPage(getModelObject().getId()));
                } else {
                    setResponsePage(new SearchSupervisorPage(institution));
                }
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new Label("schoolNameLabel", "School Name") {
            @Override
            protected void onConfigure() {
                setVisible(!hasRegistrant);
            }
        });
        form.add(new TextField<String>("schoolName") {
            @Override
            protected void onConfigure() {
                setVisible(!hasRegistrant);
            }
        });
        form.add(new TextField<String>("code") {
            @Override
            protected void onConfigure() {
                setVisible(generalParametersService.get().getGenerateRegnumberOnRegistrant());
            }
        });
        form.add(new Label("codeLabel", "Code") {
            @Override
            protected void onConfigure() {
                setVisible(generalParametersService.get().getGenerateRegnumberOnRegistrant());
            }
        });
        form.add(new Label("registrantNameLabel", "Applicant") {
            @Override
            protected void onConfigure() {
                setVisible(hasRegistrant);
            }
        });
        form.add(new Label("registrant.fullname") {
            @Override
            protected void onConfigure() {
                setVisible(hasRegistrant);
            }
        });
        form.add(new DropDownChoice("institutionType", institutionTypeService.findAll()).setRequired(Boolean.TRUE).add(new ErrorBehavior()));
        form.add(new DropDownChoice("district", new DistrictListModel(districtService)));
        form.add(new DropDownChoice("city", new CityListModel(cityService)));
        form.add(new DropDownChoice("institutionCategory", Arrays.asList(InstitutionCategory.values())) {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                }
            }
        });

        form.add(new CheckBox("registered") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(!hasRegistrant);
                }
            }
        });
        form.add(new CheckBox("academic") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getHasProcessedByBoard()) {
                    if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                        setVisible(Boolean.TRUE);
                    } else {
                        setVisible(!hasRegistrant);
                    }
                } else {
                    setVisible(!hasRegistrant);
                }
            }
        });
        form.add(new CheckBox("trainingInstitution") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(!hasRegistrant);
                }
            }
        });
        form.add(new CheckBox("examiniationCenter") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(!hasRegistrant);
                }
            }
        });
        form.add(new CheckBox("foreignInstitution") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(!hasRegistrant);
                }
            }
        });
        form.add(new CheckBox("council") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(!hasRegistrant);
                }
            }
        });

        form.add(new CheckBox("retired") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(!hasRegistrant);
                }
            }
        });
        form.add(new Link<Void>("districtLink") {

            @Override
            public void onClick() {
                setResponsePage(new DistrictEditPage(null, getPageReference()));
            }
        });
        form.add(new Link<Void>("cityLink") {

            @Override
            public void onClick() {
                setResponsePage(new CityEditPage(null, getPageReference()));
            }
        });
        form.add(new Link<Void>("provinceLink") {

            @Override
            public void onClick() {
                setResponsePage(new ProvinceEditPage(null, getPageReference()));
            }
        });
        form.add(new Link<Void>("countryLink") {

            @Override
            public void onClick() {
                setResponsePage(new CountryEditPage(null, getPageReference()));
            }
        });
        form.add(new BookmarkablePageLink<Void>("returnLink", InstitutionListPage.class));
        add(form);

    }

    public InstitutionEditPage(Long id, final IModel<Registrant> registrantModel, final Boolean foreign) {
        final CompoundPropertyModel<Institution> model = new CompoundPropertyModel<Institution>(new InstitutionEditPage.LoadableDetachableInstitutionModel(id));

        final Boolean hasRegistrant = Boolean.FALSE;
        setDefaultModel(model);
        Form<Institution> form = new Form<Institution>("form", (IModel<Institution>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                Institution institution = institutionService.save(getModelObject());
                setResponsePage(new RegistrantQualificationPage(registrantModel, hasRegistrant));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new Label("schoolNameLabel", "School Name") {
            @Override
            protected void onConfigure() {
                setVisible(!hasRegistrant);
            }
        });
        form.add(new TextField<String>("schoolName") {
            @Override
            protected void onConfigure() {
                setVisible(!hasRegistrant);
            }
        });
        form.add(new TextField<String>("code") {
            @Override
            protected void onConfigure() {
                setVisible(generalParametersService.get().getGenerateRegnumberOnRegistrant());
            }
        });
        form.add(new Label("codeLabel", "Code") {
            @Override
            protected void onConfigure() {
                setVisible(generalParametersService.get().getGenerateRegnumberOnRegistrant());
            }
        });
        form.add(new Label("registrantNameLabel", "Applicant") {
            @Override
            protected void onConfigure() {
                setVisible(hasRegistrant);
            }
        });
        form.add(new Label("registrant.fullname") {
            @Override
            protected void onConfigure() {
                setVisible(hasRegistrant);
            }
        });
        form.add(new DropDownChoice("institutionType", institutionTypeService.findAll()).setRequired(Boolean.TRUE).add(new ErrorBehavior()));
        form.add(new DropDownChoice("district", new DistrictListModel(districtService)));
        form.add(new DropDownChoice("city", new CityListModel(cityService)));
        form.add(new DropDownChoice("institutionCategory", Arrays.asList(InstitutionCategory.values())) {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.FALSE);
                }
            }
        });
        form.add(new CheckBox("registered") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(!hasRegistrant);
                }
            }
        });
        form.add(new CheckBox("academic") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(!hasRegistrant);
                }
            }
        });
        form.add(new CheckBox("trainingInstitution") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(!hasRegistrant);
                }
            }
        });
        form.add(new CheckBox("examiniationCenter") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(!hasRegistrant);
                }
            }
        });
        form.add(new CheckBox("foreignInstitution") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(!hasRegistrant);
                }
            }
        });
        form.add(new CheckBox("council") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(!hasRegistrant);
                }
            }
        });

        form.add(new CheckBox("retired") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(!hasRegistrant);
                }
            }
        });

        form.add(new Link<Void>("returnLink") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantQualificationPage(registrantModel, hasRegistrant));
            }
        });
        form.add(new Link<Void>("districtLink") {

            @Override
            public void onClick() {
                setResponsePage(new DistrictEditPage(null, getPageReference()));
            }
        });
        form.add(new Link<Void>("cityLink") {

            @Override
            public void onClick() {
                setResponsePage(new CityEditPage(null, getPageReference()));
            }
        });
        form.add(new Link<Void>("provinceLink") {

            @Override
            public void onClick() {
                setResponsePage(new ProvinceEditPage(null, getPageReference()));
            }
        });
        form.add(new Link<Void>("countryLink") {

            @Override
            public void onClick() {
                setResponsePage(new CountryEditPage(null, getPageReference()));
            }
        });
        add(form);
    }

    public InstitutionEditPage(Long id, final IModel<Registrant> registrantModel, final Boolean foreign, final Boolean CGSApplication) {
        final CompoundPropertyModel<Institution> model = new CompoundPropertyModel<Institution>(new InstitutionEditPage.LoadableDetachableInstitutionModel(id));

        final Boolean hasRegistrant = Boolean.FALSE;
        setDefaultModel(model);
        Form<Institution> form = new Form<Institution>("form", (IModel<Institution>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                Institution institution = institutionService.save(getModelObject());
                setResponsePage(new CgsApplicationPage(registrantModel));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new Label("schoolNameLabel", "School Name") {
            @Override
            protected void onConfigure() {
                setVisible(!hasRegistrant);
            }
        });
        form.add(new TextField<String>("schoolName") {
            @Override
            protected void onConfigure() {
                setVisible(!hasRegistrant);
            }
        });
        form.add(new TextField<String>("code") {
            @Override
            protected void onConfigure() {
                setVisible(generalParametersService.get().getGenerateRegnumberOnRegistrant());
            }
        });
        form.add(new Label("codeLabel", "Code") {
            @Override
            protected void onConfigure() {
                setVisible(generalParametersService.get().getGenerateRegnumberOnRegistrant());
            }
        });
        form.add(new Label("registrantNameLabel", "Applicant") {
            @Override
            protected void onConfigure() {
                setVisible(hasRegistrant);
            }
        });
        form.add(new Label("registrant.fullname") {
            @Override
            protected void onConfigure() {
                setVisible(hasRegistrant);
            }
        });
        form.add(new DropDownChoice("institutionType", institutionTypeService.findAll()).setRequired(Boolean.TRUE).add(new ErrorBehavior()));
        form.add(new DropDownChoice("district", new DistrictListModel(districtService)));
        form.add(new DropDownChoice("city", new CityListModel(cityService)));
        form.add(new DropDownChoice("institutionCategory", Arrays.asList(InstitutionCategory.values())) {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.FALSE);
                }
            }
        });
        form.add(new CheckBox("registered") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(!hasRegistrant);
                }
            }
        });
        form.add(new CheckBox("academic") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(!hasRegistrant);
                }
            }
        });
        form.add(new CheckBox("trainingInstitution") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(!hasRegistrant);
                }
            }
        });
        form.add(new CheckBox("examiniationCenter") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(!hasRegistrant);
                }
            }
        });
        form.add(new CheckBox("foreignInstitution") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(!hasRegistrant);
                }
            }
        });
        form.add(new CheckBox("council") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(!hasRegistrant);
                }
            }
        });

        form.add(new CheckBox("retired") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(!hasRegistrant);
                }
            }
        });

        form.add(new Link<Void>("returnLink") {
            @Override
            public void onClick() {
                setResponsePage(new CgsApplicationPage(registrantModel));
            }
        });

        form.add(new Link<Void>("districtLink") {

            @Override
            public void onClick() {
                setResponsePage(new DistrictEditPage(null, getPageReference()));
            }
        });
        form.add(new Link<Void>("cityLink") {

            @Override
            public void onClick() {
                setResponsePage(new CityEditPage(null, getPageReference()));
            }
        });
        form.add(new Link<Void>("provinceLink") {

            @Override
            public void onClick() {
                setResponsePage(new ProvinceEditPage(null, getPageReference()));
            }
        });
        form.add(new Link<Void>("countryLink") {

            @Override
            public void onClick() {
                setResponsePage(new CountryEditPage(null, getPageReference()));
            }
        });

        add(form);

    }

    private final class LoadableDetachableInstitutionModel extends LoadableDetachableModel<Institution> {

        private Long id;

        public LoadableDetachableInstitutionModel(Long id) {
            this.id = id;
        }

        @Override
        protected Institution load() {
            if (id == null) {
                return new Institution();
            }
            return institutionService.get(id);
        }
    }
}
