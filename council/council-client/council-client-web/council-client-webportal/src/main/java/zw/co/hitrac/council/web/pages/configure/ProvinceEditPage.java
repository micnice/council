package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.PageReference;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Province;
import zw.co.hitrac.council.business.service.DistrictService;
import zw.co.hitrac.council.business.service.ProvinceService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Kelvin Goredema
 */
public class ProvinceEditPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private ProvinceService provinceService;
    @SpringBean
    private DistrictService districtService;

    public ProvinceEditPage(Long id, final PageReference pageReference) {
        setDefaultModel(new CompoundPropertyModel<Province>(new ProvinceEditPage.LoadableDetachableProvinceModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));

        Form<Province> form = new Form<Province>("form", (IModel<Province>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                provinceService.save(getModelObject());
                setResponsePage(pageReference.getPage());
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new Link<Void>("returnLink") {

            @Override
            public void onClick() {
                setResponsePage(pageReference.getPage());
            }
        });
        add(form);
    }

    private final class LoadableDetachableProvinceModel extends LoadableDetachableModel<Province> {
        private Long id;

        public LoadableDetachableProvinceModel(Long id) {
            this.id = id;
        }

        @Override
        protected Province load() {
            if (id == null) {
                return new Province();
            }

            return provinceService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
