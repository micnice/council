/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.accounts.Customer;

import java.util.List;

/**
 *
 * @author edwin
 */
public interface InstitutionManagerService extends IGenericService<InstitutionManager> {

    List<InstitutionManager> findInstitutionManagers(Registrant registrant, Institution institution);

    boolean checkDuplicate(Institution institution, Registrant registrant, Boolean supervisor, Boolean practitionerInCharge);

    }
