package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.CourseGroupDAO;
import zw.co.hitrac.council.business.domain.CourseGroup;
import zw.co.hitrac.council.business.service.CourseGroupService;

import java.util.List;

/**
 * @author Michael Matiashe
 */
@Service
@Transactional
public class CourseGroupServiceImpl implements CourseGroupService {

    @Autowired
    private CourseGroupDAO courseGroupDAO;

    @Transactional
    public CourseGroup save(CourseGroup courseGroup) {

        return courseGroupDAO.save(courseGroup);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<CourseGroup> findAll() {

        return courseGroupDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public CourseGroup get(Long id) {

        return courseGroupDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<CourseGroup> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setCourseGroupDAO(CourseGroupDAO courseGroupDAO) {

        this.courseGroupDAO = courseGroupDAO;
    }

}
