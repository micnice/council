package zw.co.hitrac.council.business.dao.accounts.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.accounts.CarryForwardLogDAO;
import zw.co.hitrac.council.business.dao.repo.accounts.CarryForwardLogRepository;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.CarryForwardLog;
import zw.co.hitrac.council.business.domain.accounts.Customer;

/**
 *
 * @author Charles Chigoriwa
 */
@Repository
public class CarryForwardLogDAOImpl implements CarryForwardLogDAO{
   @Autowired
   private CarryForwardLogRepository carryForwardLogRepository;
   
   @PersistenceContext
   private EntityManager entityManager;
    
   public CarryForwardLog save(CarryForwardLog carryForwardLog){
       return carryForwardLogRepository.save(carryForwardLog);
   }
   
   public List<CarryForwardLog> findAll(){
       return carryForwardLogRepository.findAll();
   }
   
   public CarryForwardLog get(Long id){
       return carryForwardLogRepository.findOne(id);
   }
 /**
     * A setter method that will make mocking repo object easier
     * @param carryForwardLogRepository 
     */
   
   public void setCarryForwardLogRepository(CarryForwardLogRepository carryForwardLogRepository) {
         this.carryForwardLogRepository = carryForwardLogRepository;
    }

    public List<CarryForwardLog> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<CarryForwardLog> carryForwardLogs(Registrant registrant, Date startDate, Date endDate) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(CarryForwardLog.class);
        criteria.createAlias("receiptHeader", "r");
        if(registrant!=null){
        criteria.createAlias("r.paymentdetails", "p");
        criteria.add(Restrictions.eq("p.registrant", registrant));
        }
        if (startDate!=null && endDate!=null){
        criteria.add(Restrictions.between("datePaid", startDate, endDate));
        }
        
        if (startDate!=null && endDate==null){
        criteria.add(Restrictions.between("datePaid", startDate, new Date()));
        }        
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        
    }

    public List<CarryForwardLog> carryForwardLogs() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<CarryForwardLog> getRegistrantCarryForwardLogs(Customer customer) {
        return entityManager.createQuery("SELECT c FROM CarryForwardLog c WHERE c.receiptHeader.paymentDetails.customer=:customer and c.used=FALSE and c.canceled=FALSE").setParameter("customer", customer).getResultList();
    }
   
}
