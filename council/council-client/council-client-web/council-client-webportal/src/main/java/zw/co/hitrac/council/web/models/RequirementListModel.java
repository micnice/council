package zw.co.hitrac.council.web.models;

import java.util.List;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.Requirement;
import zw.co.hitrac.council.business.service.RequirementService;
import zw.co.hitrac.council.business.utils.HrisComparator;

/**
 *
 * @author Michael Matiashe
 */
public class RequirementListModel extends LoadableDetachableModel<List<Requirement>> {

    private final RequirementService requirementService;
    private HrisComparator hrisComparator = new HrisComparator();

    public RequirementListModel(RequirementService requirementService) {
        this.requirementService = requirementService;
    }

    @Override
    protected List<Requirement> load() {
        return (List<Requirement>) hrisComparator.sort(requirementService.findAll());
    }
}
