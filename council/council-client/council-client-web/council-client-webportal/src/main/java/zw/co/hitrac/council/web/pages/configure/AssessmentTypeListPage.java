/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.configure;

import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.AssessmentType;
import zw.co.hitrac.council.business.service.AssessmentTypeService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

/**
 *
 * @author kelvin
 */
public class AssessmentTypeListPage extends IAdministerDatabaseBasePage{
    
    @SpringBean
    private AssessmentTypeService addressTypeService;

    public AssessmentTypeListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink",AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink",AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {

            @Override
            public void onClick() {
               setResponsePage(new AssessmentTypeEditPage(null));
            }
        });
        
        IModel<List<AssessmentType>> model=new LoadableDetachableModel<List<AssessmentType>>() {

            @Override
            protected List<AssessmentType> load() {
               return addressTypeService.findAll();
            }
        };
        
        PropertyListView<AssessmentType> eachItem=new PropertyListView<AssessmentType>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<AssessmentType> item) {
                Link<AssessmentType> viewLink=new Link<AssessmentType>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                       setResponsePage(new AssessmentTypeViewPage(getModelObject().getId()));
                    }
                };
                if(item.getIndex()%2==0){
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };
        
        add(eachItem);
    }
    
    
    
}
