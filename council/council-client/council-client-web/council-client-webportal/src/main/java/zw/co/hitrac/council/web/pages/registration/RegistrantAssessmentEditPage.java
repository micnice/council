/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.registration;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantAssessment;
import zw.co.hitrac.council.business.service.AssessmentTypeService;
import zw.co.hitrac.council.business.service.RegistrantAssessmentService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.CustomDateTextField;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

/**
 *
 * @author kelvin
 */
public class RegistrantAssessmentEditPage extends TemplatePage {

    @SpringBean
    private RegistrantAssessmentService registrantAssessmentService;
    @SpringBean
    private AssessmentTypeService assessmentTypeService;

    public RegistrantAssessmentEditPage(final IModel<Registrant> registrantModel) {

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());

        CompoundPropertyModel<RegistrantAssessment> model = new CompoundPropertyModel<RegistrantAssessment>(
                new RegistrantAssessmentEditPage.LoadableDetachableRegistrantAssessmentServiceModel(
                registrantModel.getObject()));

        setDefaultModel(model);

        Form<RegistrantAssessment> form = new Form<RegistrantAssessment>("form", (IModel<RegistrantAssessment>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    registrantAssessmentService.save(getModelObject());
                    setResponsePage(new RegistrantViewPage(getModelObject().getRegistrant().getId()));
                } catch (CouncilException ex) {
                    error(ex.getMessage());

                }

            }
        };
        /*  Link<RegistrantAssessment> link=new Link<RegistrantAssessment>("commentLink",model){

         @Override
         public void onClick() {
         setResponsePage(new RegistrantCommentPage(getModelObject().getId()));
         }
        
        
         }*/
        form.add(new DropDownChoice("assessmentType", assessmentTypeService.findAll()).setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("valueObtained").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("assessor").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextArea<String>("comments"));
        form.add(new CustomDateTextField("date").add(DatePickerUtil.getDatePicker()));


        add(form);
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });

        add(new Label("registrant.fullname"));
    }

    public RegistrantAssessmentEditPage(long id) {

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        CompoundPropertyModel<RegistrantAssessment> model = new CompoundPropertyModel<RegistrantAssessment>(
                new RegistrantAssessmentEditPage.LoadableDetachableRegistrantAssessmentServiceModel(
                id));
        final Long registrantId = model.getObject().getRegistrant().getId();

        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantId);



        setDefaultModel(model);

        Form<RegistrantAssessment> form = new Form<RegistrantAssessment>("form", (IModel<RegistrantAssessment>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    registrantAssessmentService.save(getModelObject());
                    setResponsePage(new RegistrantViewPage(getModelObject().getRegistrant().getId()));
                } catch (CouncilException ex) {
                    error(ex.getMessage());

                }

            }
        };
        form.add(new DropDownChoice("assessmentType", assessmentTypeService.findAll()).setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("valueObtained").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("assessor").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextArea<String>("comments"));
        form.add(new CustomDateTextField("date").add(DatePickerUtil.getDatePicker()));


        add(form);
        add(new Label("registrant.fullname"));

        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantId));
            }
        });

    }

    private final class LoadableDetachableRegistrantAssessmentServiceModel extends LoadableDetachableModel<RegistrantAssessment> {

        private Long id;
        private Registrant registrant;

        public LoadableDetachableRegistrantAssessmentServiceModel(Long id) {
            this.id = id;
        }

        public LoadableDetachableRegistrantAssessmentServiceModel(Registrant registrant) {
            this.registrant = registrant;
        }

        @Override
        protected RegistrantAssessment load() {
            RegistrantAssessment registrantAssessment = null;

            if (id == null) {
                registrantAssessment = new RegistrantAssessment();
                registrantAssessment.setRegistrant(registrant);
            } else {
                registrantAssessment = registrantAssessmentService.get(id);
            }

            return registrantAssessment;
        }
    }
}
