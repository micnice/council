package zw.co.hitrac.council.web.utility;

import net.sf.jasperreports.engine.JRException;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.handler.resource.ResourceStreamRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.ContentDisposition;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.util.resource.IResourceStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.reports.Report;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.reports.utils.IReportManager;
import zw.co.hitrac.council.reports.utils.ReportManager;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.Map;

/**
 * @author Charles Chigoriwa
 */
public class ReportResourceUtils implements Serializable {

    private static final Logger logger = LoggerFactory.getLogger(ReportResourceUtils.class);

    private static String SEPARATOR = ",";

    public static ByteArrayResource getReportResource(ContentType contentType, byte[] data, String fileName)
            throws JRException {

        if (contentType == null) {
            contentType = ContentType.PDF;
        }

        return new ByteArrayResource(contentType.getContentTypeString(), data, fileName);

    }

    public static void processJavaBeanReport(Report report, Collection<?> collection,
                                             Map<String, Object> parameters) throws JRException {

        processJavaBeanReport(report, ContentType.PDF, collection, parameters);
    }

    public static void processJavaBeanReport(Report report, ContentType contentType, Collection<?> collection,
                                             Map<String, Object> parameters) throws JRException {

        ByteArrayResource resource = getReportResource(report, contentType, collection, parameters);
        IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(), RequestCycle.get()
                .getResponse(), null);
        resource.respond(a);

        RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
    }

    /**
     * JavaBean report
     *
     * @param report
     * @param contentType
     * @param collection
     * @param parameters
     * @return
     * @throws JRException
     */
    public static ByteArrayResource getReportResource(Report report, ContentType contentType, Collection<?> collection,
                                                      Map<String, Object> parameters)
            throws JRException {

        IReportManager jasperReportManager = new ReportManager();
        ByteArrayResource resource;

        if (contentType == null) {
            contentType = ContentType.PDF;
        }

        switch (contentType) {

            case PDF:

                resource = new ByteArrayResource(contentType.getContentTypeString(),
                        jasperReportManager.getPdfReportBytes(parameters, report.getJRXMLReportPath(),
                                collection), report.getOutputReportName(contentType));
                break;

            case DOC:

                resource = new ByteArrayResource(contentType.getContentTypeString(),
                        jasperReportManager.getReportDoc(parameters, report.getJRXMLReportPath(),
                                collection), report.getOutputReportName(contentType));
                break;

            case EXCEL:

                resource = new ByteArrayResource(contentType.getContentTypeString(),
                        jasperReportManager.getReportXls(parameters, report.getJRXMLReportPath(),
                                collection), report.getOutputReportName(contentType));
                break;

            case HTML:

                resource = new ByteArrayResource(contentType.getContentTypeString(),
                        jasperReportManager.getReportHTML(parameters, report.getJRXMLReportPath(),
                                collection), report.getOutputReportName(contentType));
                break;

            default:

                resource = new ByteArrayResource(contentType.getContentTypeString(),
                        jasperReportManager.getPdfReportBytes(parameters, report.getJRXMLReportPath(),
                                collection), report.getOutputReportName(contentType));

        }

        return resource;

    }

    public static void processSQLReport(Report report, Connection connection,
                                        Map<String, Object> parameters) throws JRException {

        processSQLReport(report, ContentType.PDF, connection, parameters);
    }

    public static void processSQLReport(Report report, ContentType contentType, Connection connection,
                                        Map<String, Object> parameters) throws JRException {

        ByteArrayResource resource = getReportResource(report, contentType, connection, parameters);
        IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(), RequestCycle.get()
                .getResponse(), null);
        resource.respond(a);

        RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
    }

    /**
     * SQL report
     *
     * @param report
     * @param contentType
     * @param connection
     * @param parameters
     * @return
     * @throws JRException
     */
    public static ByteArrayResource getReportResource(Report report, ContentType contentType, Connection connection,
                                                      Map<String, Object> parameters)
            throws JRException {

        IReportManager jasperReportManager = new ReportManager();
        ByteArrayResource resource;

        if (contentType == null) {
            contentType = ContentType.PDF;
        }

        switch (contentType) {

            case PDF:

                resource = new ByteArrayResource(contentType.getContentTypeString(),
                        jasperReportManager.getPdfReportBytes(parameters, report.getJRXMLReportPath(),
                                connection), report.getOutputReportName(contentType));
                break;

            case DOC:

                resource = new ByteArrayResource(contentType.getContentTypeString(),
                        jasperReportManager.getReportDoc(parameters, report.getJRXMLReportPath(),
                                connection), report.getOutputReportName(contentType));
                break;

            case EXCEL:

                resource = new ByteArrayResource(contentType.getContentTypeString(),
                        jasperReportManager.getReportXls(parameters, report.getJRXMLReportPath(),
                                connection), report.getOutputReportName(contentType));
                break;

            case CSV:

                resource = new ByteArrayResource(contentType.getContentTypeString(),
                        jasperReportManager.getReportXls(parameters, report.getJRXMLReportPath(),
                                connection), report.getOutputReportName(contentType));
                break;

            case HTML:

                resource = new ByteArrayResource(contentType.getContentTypeString(),
                        jasperReportManager.getReportHTML(parameters, report.getJRXMLReportPath(),
                                connection), report.getOutputReportName(contentType));
                break;

            default:

                resource = new ByteArrayResource(contentType.getContentTypeString(),
                        jasperReportManager.getPdfReportBytes(parameters, report.getJRXMLReportPath(),
                                connection), report.getOutputReportName(contentType));

        }

        return resource;

    }

    public static void printJasperPrint(String printerName, Report report, Collection<?> collection, Map<String, Object> parameters)
            throws JRException {

        IReportManager jasperReportManager = new ReportManager();

        jasperReportManager.printJasperPrint(printerName, parameters, report.getJRXMLReportPath(), collection);

        // return new ByteArrayResource(contentType.getContentTypeString(), jasperReportManager.getPdfReportBytes(parameters, /*report.getJRXMLReportPath()*/report.getJasperReportPath(), collection), report.getOutputReportName(contentType));
    }

    /**
     * @param registrants List of registrants
     * @return comma separated value of registrant id's from registrants list
     */
    public static String getRegistrantIdListFromRegistrantList(Collection<RegistrantData> registrants) {

        StringBuilder builder = new StringBuilder();
        String registrantIdList = null;

        if (registrants != null && !registrants.isEmpty()) {

            for (RegistrantData d : registrants) {
                builder.append(d.getId().toString()).append(SEPARATOR);
            }

            registrantIdList = builder.substring(0, builder.lastIndexOf(SEPARATOR));
        }

        return StringUtils.defaultString(registrantIdList, "");
    }


    /**
     * @param registrants List of registrants
     * @return comma separated value of registrant id's from registrants list
     */
    public static String getRegistrantIdListFromRegistrantsList(Collection<Registrant> registrants) {

        StringBuilder builder = new StringBuilder();
        String registrantIdList = null;

        if (registrants != null && !registrants.isEmpty()) {

            for (Registrant d : registrants) {
                builder.append(d.getId().toString()).append(SEPARATOR);
            }

            registrantIdList = builder.substring(0, builder.lastIndexOf(SEPARATOR));
        }

        return StringUtils.defaultString(registrantIdList, "");
    }


    public static void respond(byte[] bytes, String reportName, RequestCycle requestCycle, ContentType contentType) {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);

        try (IResourceStream resourceStream = new ReportResourceStream(byteArrayInputStream, contentType)) {

            requestCycle.scheduleRequestHandlerAfterCurrent(
                    new ResourceStreamRequestHandler(resourceStream)
                            .setFileName(reportName)
                            .setContentDisposition(ContentDisposition.INLINE)
            );
        } catch (IOException e) {
            logger.error("Error generating report", e);
        }


    }
}

//~ Formatted by Jindent --- http://www.jindent.com
