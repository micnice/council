package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.PageReference;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CourseCondition;
import zw.co.hitrac.council.business.service.ConditionService;
import zw.co.hitrac.council.business.service.CourseConditionService;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.RegisterService;
import zw.co.hitrac.council.web.models.ConditionsListModel;
import zw.co.hitrac.council.web.models.CourseListModel;
import zw.co.hitrac.council.web.models.RegisterListModel;

/**
 *
 * @author tdhlakama
 */
public class CourseConditionEditPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private CourseConditionService courseConditionService;
    @SpringBean
    private RegisterService registerService;
    @SpringBean
    private CourseService courseService;
    @SpringBean
    private ConditionService conditionService;

    public CourseConditionEditPage(Long id, final PageReference returnToPage) {
        setDefaultModel(new CompoundPropertyModel<CourseCondition>(new LoadableDetachableCourseConditionModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        Form<CourseCondition> form = new Form<CourseCondition>("form", (IModel<CourseCondition>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                courseConditionService.save(getModelObject());
                setResponsePage(returnToPage.getPage());
            }
        };
        form.add(new DropDownChoice("register", new RegisterListModel(registerService)));
        form.add(new DropDownChoice("course", new CourseListModel(courseService)));
        form.add(new DropDownChoice("condition", new ConditionsListModel(conditionService)));
        form.add(new CheckBox("retired"));
        form.add(new Link("returnLink") {
            public void onClick() {
                setResponsePage(returnToPage.getPage());
            }
        });

//        form.add(new BookmarkablePageLink<Void>("returnLink", CourseConditionListPage.class));
        add(form);
    }

    private final class LoadableDetachableCourseConditionModel extends LoadableDetachableModel<CourseCondition> {

        private Long id;

        public LoadableDetachableCourseConditionModel(Long id) {
            this.id = id;
        }

        @Override
        protected CourseCondition load() {
            if (id == null) {
                return new CourseCondition();
            }
            return courseConditionService.get(id);
        }
    }
}
