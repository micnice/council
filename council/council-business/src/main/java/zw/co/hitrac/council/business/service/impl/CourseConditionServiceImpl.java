/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.CourseConditionDAO;
import zw.co.hitrac.council.business.domain.CourseCondition;
import zw.co.hitrac.council.business.service.CourseConditionService;

import java.util.List;
import zw.co.hitrac.council.business.domain.Conditions;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Register;

/**
 *
 * @author kelvin
 */
@Service
@Transactional
public class CourseConditionServiceImpl implements CourseConditionService {

    @Autowired
    private CourseConditionDAO courseConditionDAO;

    @Transactional
    public CourseCondition save(CourseCondition courseCondition) {
        return courseConditionDAO.save(courseCondition);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<CourseCondition> findAll() {
        return courseConditionDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public CourseCondition get(Long id) {
        return courseConditionDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<CourseCondition> findAll(Boolean retired) {
        return courseConditionDAO.findAll(retired);
    }

    public void setCourseConditionDAO(CourseConditionDAO courseConditionDAO) {
        this.courseConditionDAO = courseConditionDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Conditions> getConditions(Course course, Register register) {
        return courseConditionDAO.getConditions(course, register);
    }

}
