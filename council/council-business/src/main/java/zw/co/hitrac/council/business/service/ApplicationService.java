package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.ApplicationPurpose;
import zw.co.hitrac.council.business.domain.QualificationType;
import zw.co.hitrac.council.business.domain.Registrant;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Michael Matiashe
 */
public interface ApplicationService extends IGenericService<Application> {

    List<Application> getApplications(String query, String applicationStatus, Date startDate, Date endDate, ApplicationPurpose applicationPurpose, Boolean used, Boolean expired);

    public List<Application> getRegistrantApplicationStatus(String applicationStatus);

    public Integer getTotalApplicationsByStatus(String status);

    public List<Application> getExpiredApplications();
    
    public Integer getNumberOfPendingApplications();
    
    public List<Application> getFilteredPendingApplications();

    public List<Application> getApplicationsByYearApproved(int year);

    public List<Application> getApplicationsWithoutPayment();

    public void removeDebtApplicationsWithoutPayment();
    
    public Boolean hasPendingApplication(Registrant registrant, ApplicationPurpose applicationPurpose);
}
