
package zw.co.hitrac.council.business.dao;

import zw.co.hitrac.council.business.domain.PenaltyParameters;

/**
 *
 * @author Michael Matiashe
 * 
 */
public interface PenaltyParametersDAO  extends IGenericDAO<PenaltyParameters> {
    
    
    
}
