/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.Employment;
import zw.co.hitrac.council.business.domain.EmploymentStatus;

/**
 *
 * @author kelvin
 */
public interface EmploymentStatusRepository extends CrudRepository<EmploymentStatus, Long> {
      public List<EmploymentStatus> findAll();
}
