package zw.co.hitrac.council.web.models;

import java.util.List;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.District;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.utils.HrisComparator;

/**
 *
 * @author Charles Chigoriwa
 */
public class InstitutionListModel extends LoadableDetachableModel<List<Institution>> {

    private final InstitutionService institutionService;
    private HrisComparator hrisComparator = new HrisComparator();
    
    public InstitutionListModel(InstitutionService institutionService) {
        this.institutionService = institutionService;
    }
    
    @Override
    protected List<Institution> load() {
        return (List<Institution>) hrisComparator.sort(institutionService.findAll());
    }
}
