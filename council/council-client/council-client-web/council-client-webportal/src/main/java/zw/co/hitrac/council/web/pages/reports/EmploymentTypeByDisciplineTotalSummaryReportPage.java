package zw.co.hitrac.council.web.pages.reports;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.PageReference;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.business.utils.StringFormattingUtil;
import zw.co.hitrac.council.reports.EmploymentTypeByDisciplineTotalSummaryReport;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.RegisterListModel;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

import java.util.*;
import zw.co.hitrac.council.reports.utils.ContentType;

/**
 * @author tdhlakama
 */
public class EmploymentTypeByDisciplineTotalSummaryReportPage extends TemplatePage {

    @SpringBean
    private CouncilDurationService councilDurationService;
    @SpringBean
    private RegistrantActivityService registrantActivityService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    private CouncilDuration councilDuration;
    private Course course;
    private Register register;
    @SpringBean
    private RegisterService registerService;
    @SpringBean
    private CourseService courseService;
    private String status;
    private ContentType contentType;

    public EmploymentTypeByDisciplineTotalSummaryReportPage(final PageReference pageReference) {

        menuPanelManager.getSearchPanel().setTopMenuCurrent(true);
        menuPanelManager.getSearchPanel().setSearchAdvanced(true);

        Register institutionRegister = generalParametersService.get().getInstitutionRegister();

        PropertyModel<CouncilDuration> councilDurationModel = new PropertyModel<CouncilDuration>(this, "councilDuration");
        PropertyModel<String> statusModel = new PropertyModel<String>(this, "status");
        PropertyModel<ContentType> contentTypeModel = new PropertyModel<ContentType>(this, "contentType");
        final PropertyModel<Register> registerModel = new PropertyModel<Register>(this, "register");

        Form<?> form = new Form("form");
        form.add(new DropDownChoice("councilDuration", councilDurationModel, councilDurationService.findAll()).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("register", registerModel, new RegisterListModel(registerService)));
        form.add(new DropDownChoice("status", statusModel, Arrays.asList(new String[]{"Active", "InActive"}))
                .setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("contentType", contentTypeModel, Arrays.asList(ContentType.values())));
        add(form);

        form.add(new Link("returnLink") {

            public void onClick() {

                setResponsePage(pageReference.getPage());
            }
        });

        form.add(new Button("processList") {

            @Override
            public void onSubmit() {

                register = registerModel.getObject();

                if (StringUtils.isEmpty(status)) {

                    error("Select Status of Report -  Active or In Active");

                } else {

                    boolean isActive = status.contentEquals("Active");
                    printEmploymentSummaryReport(isActive, register);
                }

            }
        });

        add(new FeedbackPanel("feedback"));
    }

    public void printEmploymentSummaryReport(Boolean isActive, Register register) {

        int totalForEmploymentTypeAndCourseInAllRegisters;
        int count;
        List<RegistrantData> registrants = new ArrayList<RegistrantData>();
        List<Course> courses = courseService.findAll();
        List<Duration> durations = new ArrayList<Duration>(councilDurationService.get(councilDuration.getId()).getDurations());
        Set<Register> registers = new HashSet<Register>();

        if (register == null) {
            registers.addAll(registerService.findAll());
            registers.remove(generalParametersService.get().getStudentRegister());
            registers.remove(generalParametersService.get().getInstitutionRegister());

        } else {
            registers.add(register);
        }

        for (Course c : courses) {

            RegistrantData registrantData = new RegistrantData();
            registrantData.setCourseName(c.getName());

            for (EmploymentType e : EmploymentType.values()) {

                totalForEmploymentTypeAndCourseInAllRegisters = 0;

                for (Register r : registers) {

                    count = registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(
                            c, r, durations, RegistrantActivityType.RENEWAL, isActive, e, null);

                    totalForEmploymentTypeAndCourseInAllRegisters += count;

                }

                switch (e) {

                    case INTERNATIONAL:

                        registrantData.setInternationalEmploymentTotal(registrantData.getInternationalEmploymentTotal()
                                + totalForEmploymentTypeAndCourseInAllRegisters);
                        break;

                    case PRIVATE:

                        registrantData.setPrivateEmploymentTotal(registrantData.getPrivateEmploymentTotal()
                                + totalForEmploymentTypeAndCourseInAllRegisters);

                        break;

                    case PUBLIC:

                        registrantData.setPublicEmploymentTotal(registrantData.getPublicEmploymentTotal()
                                + totalForEmploymentTypeAndCourseInAllRegisters);

                        break;

                    case NOTEMPLOYED:

                        registrantData.setNotEmploymentTotal(registrantData.getNotEmploymentTotal()
                                + totalForEmploymentTypeAndCourseInAllRegisters);

                        break;

                    default:
                        throw new IllegalArgumentException("Uknown employment type");
                }

            }

            registrants.add(registrantData);

        }

        EmploymentTypeByDisciplineTotalSummaryReport summaryReport = new EmploymentTypeByDisciplineTotalSummaryReport();

        Map parameters = new HashMap();
        parameters.put("comment", StringFormattingUtil.join("Summary of All Registers by Discipline and Employment Type -",
                status, "-", councilDuration.getName(), "Period"));

        try {

            ReportResourceUtils.processJavaBeanReport(summaryReport, registrants, parameters);

        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }

    }

}

//~ Formatted by Jindent --- http://www.jindent.com
