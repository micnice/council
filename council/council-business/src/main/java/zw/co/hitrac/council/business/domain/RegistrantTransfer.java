/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Michael Matiashe
 */
@Entity
@Table(name="registranttransfer")
@Audited
public class RegistrantTransfer extends BaseIdEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    private Registrant registrant;
    private Date transferDate;
    private TransferRule transferRule;
    private Application application;
    private Course course;

    @OneToOne
    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    @OneToOne
    public Registrant getRegistrant() {
        return registrant;
    }

    public void setRegistrant(Registrant registrant) {
        this.registrant = registrant;
    }

    @Temporal(TemporalType.DATE)
    public Date getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(Date transferDate) {
        this.transferDate = transferDate;
    }

    @OneToOne
    public TransferRule getTransferRule() {
        return transferRule;
    }

    public void setTransferRule(TransferRule transferRule) {
        this.transferRule = transferRule;
    }
 
    @OneToOne
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
    
    
}
