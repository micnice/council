package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CourseCertificateNumber;
import zw.co.hitrac.council.business.service.CourseCertificateNumberService;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.RegisterService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Charles Chigoriwa
 */
public class CourseCertificateNumberEditPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private CourseCertificateNumberService courseCertificateNumberService;

    @SpringBean
    private CourseService courseService;

    @SpringBean
    private RegisterService registerService;

    public CourseCertificateNumberEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<CourseCertificateNumber>(new LoadableDetachableCourseCertificateNumberModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        Form<CourseCertificateNumber> form = new Form<CourseCertificateNumber>("form", (IModel<CourseCertificateNumber>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                courseCertificateNumberService.save(getModelObject());
                setResponsePage(new CourseCertificateNumberListPage());
            }
        };

        form.add(new TextField<String>("certificateNumber").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("coursePrefix").setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("course", courseService.findAll()).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("register", registerService.findAll()));
        form.add(new BookmarkablePageLink<Void>("returnLink", CourseCertificateNumberListPage.class));
        add(form);
    }

    private final class LoadableDetachableCourseCertificateNumberModel extends LoadableDetachableModel<CourseCertificateNumber> {

        private Long id;

        public LoadableDetachableCourseCertificateNumberModel(Long id) {
            this.id = id;
        }

        @Override
        protected CourseCertificateNumber load() {
            if (id == null) {
                return new CourseCertificateNumber();
            }
            return courseCertificateNumberService.get(id);
        }
    }

}
