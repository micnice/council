package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.TransferRule;

/**
 *
 * @author Michael Matiashe
 */
public interface TransferRuleService extends IGenericService<TransferRule> {
 
}
