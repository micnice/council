/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.RegistrantIdentificationDAO;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantIdentification;
import zw.co.hitrac.council.business.service.RegistrantIdentificationService;

import java.util.List;

/**
 * @author kelvin
 */
@Service
@Transactional
public class RegistrantIdentificationServiceImpl implements RegistrantIdentificationService {

    @Autowired
    private RegistrantIdentificationDAO registrantIdentificationDAO;

    @Transactional
    public RegistrantIdentification save(RegistrantIdentification registrantIdentification) {

        return registrantIdentificationDAO.save(registrantIdentification);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantIdentification> findAll() {

        return registrantIdentificationDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantIdentification get(Long id) {

        return registrantIdentificationDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantIdentification> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setRegistrantIdentificationDAO(RegistrantIdentificationDAO registrantIdentificationDAO) {

        this.registrantIdentificationDAO = registrantIdentificationDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantIdentification> getIdentities(Registrant registrant) {

        return registrantIdentificationDAO.getIdentities(registrant);
    }

}
