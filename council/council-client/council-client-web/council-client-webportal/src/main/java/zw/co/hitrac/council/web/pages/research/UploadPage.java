package zw.co.hitrac.council.web.pages.research;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.jackrabbit.value.BinaryImpl;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.lang.Bytes;
import sun.net.www.MimeTable;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.web.pages.TemplatePage;

import javax.jcr.Node;
import javax.jcr.Repository;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;
import java.util.Calendar;
import java.util.List;

//~--- JDK imports ------------------------------------------------------------

/**
 *
 * @author Charles Chigoriwa
 */
public class UploadPage extends TemplatePage {


    @SpringBean
    private GeneralParametersService generalParametersService;


    public UploadPage() {

        // Create feedback panels
        final FeedbackPanel uploadFeedback = new FeedbackPanel("uploadFeedback");

        // Add uploadFeedback to the page itself
        add(uploadFeedback);

        final FileUploadForm simpleUploadForm = new FileUploadForm("simpleUpload");

        add(simpleUploadForm);
        add(new Image("image", new MyDynamicImageResource(generalParametersService)));
    }

    private class FileUploadForm extends Form<Void> {
        FileUploadField fileUploadField;

        /**
         * Construct.
         *
         * @param name Component name
         */
        public FileUploadForm(String name) {
            super(name);

            // set this form to multipart mode (allways needed for uploads!)
            setMultiPart(true);

            // Add one file input field
            add(fileUploadField = new FileUploadField("fileInput"));

            // Set maximum size to 100K for demo purposes
            setMaxSize(Bytes.kilobytes(900));
        }

        /**
         * @see org.apache.wicket.markup.html.form.Form#onSubmit()
         */
        @Override
        protected void onSubmit() {
            final List<FileUpload> uploads = fileUploadField.getFileUploads();

            if (uploads != null) {
                for (FileUpload upload : uploads) {

                    // UploadPage.this.info("saved file: " + upload.getClientFileName());
                    try {

                        Repository repository = JcrUtils.getRepository(generalParametersService.get().getContent_Url());
                        Session session = repository.login(new SimpleCredentials("admin", "admin".toCharArray()));

                        String user = session.getUserID();
                        String name = repository.getDescriptor(Repository.REP_NAME_DESC);

                        System.out.println("Logged in as " + user + " to a " + name + " repository.");

                        try {
                            Node      root     = session.getRootNode();
                            String    fileName = upload.getClientFileName();
                            MimeTable mt       = MimeTable.getDefaultTable();
                            String    mimeType = mt.getContentTypeFor(fileName);

                            if (mimeType == null) {
                                mimeType = "application/octet-stream";
                            }

                            Node mainlogoNode;

                            if (root.hasNode("content/council/logos/main/mainlogo")) {
                                root.getNode("content/council/logos/main/mainlogo").remove();
                                session.save();
                            }

                            Node contentNode;

                            if (root.hasNode("content")) {
                                contentNode = root.getNode("content");
                            } else {
                                contentNode = root.addNode("content");
                            }

                            Node councilNode;

                            if (contentNode.hasNode("council")) {
                                councilNode = contentNode.getNode("council");
                            } else {
                                councilNode = contentNode.addNode("council");
                            }

                            Node logosNode;

                            if (councilNode.hasNode("logos")) {
                                logosNode = councilNode.getNode("logos");
                            } else {
                                logosNode = councilNode.addNode("logos");
                            }

                            Node mainNode;

                            if (logosNode.hasNode("main")) {
                                mainNode = logosNode.getNode("main");
                            } else {
                                mainNode = logosNode.addNode("main");
                            }

                            mainlogoNode = mainNode.addNode("mainlogo", "nt:file");

                            Node mainLogoContentNode = mainlogoNode.addNode("jcr:content", "nt:resource");

                            BinaryImpl binary = new BinaryImpl(upload.getInputStream());

                            mainLogoContentNode.setProperty("jcr:data", binary);
                            mainLogoContentNode.setProperty("jcr:lastModified", Calendar.getInstance());
                            mainLogoContentNode.setProperty("jcr:mimeType", mimeType);
                            session.save();

                            //Always call this to release resources
                            binary.dispose();

                        } finally {
                            session.logout();
                        }
                    } catch (Exception ex) {
                        error(ex.getMessage());
                        System.out.println(ex.getMessage());
                    }
                }
            }
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
