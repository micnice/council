package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CourseType;
import zw.co.hitrac.council.business.service.CourseTypeService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Charles Chigoriwa
 */
public class CourseTypeEditPage extends IAdministerDatabaseBasePage{
    
    @SpringBean
    private CourseTypeService courseTypeService;

    public CourseTypeEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<CourseType>(new LoadableDetachableCourseTypeModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink",AdministerDatabasePage.class));
        Form<CourseType> form=new Form<CourseType>("form",(IModel<CourseType>)getDefaultModel()) {
            @Override
            public void onSubmit(){
                courseTypeService.save(getModelObject());
                setResponsePage(new CourseTypeViewPage(getModelObject().getId()));
            }
        };
        
        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior())); 
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink",CourseTypeListPage.class));
        add(form);
    }
    
    
    private final class LoadableDetachableCourseTypeModel extends LoadableDetachableModel<CourseType> {

        private Long id;

        public LoadableDetachableCourseTypeModel(Long id) {
            this.id = id;
        }

        @Override
        protected CourseType load() {
            if(id==null){
                return new CourseType();
            }
            return courseTypeService.get(id);
        }
    }
    
    
}
