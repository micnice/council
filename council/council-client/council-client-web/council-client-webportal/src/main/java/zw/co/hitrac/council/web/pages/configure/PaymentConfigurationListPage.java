package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.PaymentConfiguration;
import zw.co.hitrac.council.business.service.PaymentConfigurationService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;

/**
 *
 * @author  Kelvin Goredema
 */
public class PaymentConfigurationListPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private PaymentConfigurationService paymentConfigurationService;

    public PaymentConfigurationListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new PaymentConfigurationEditPage(null));
            }
        });

        IModel<List<PaymentConfiguration>> model = new LoadableDetachableModel<List<PaymentConfiguration>>() {
            @Override
            protected List<PaymentConfiguration> load() {
                return paymentConfigurationService.findAll();
            }
        };
        PropertyListView<PaymentConfiguration> eachItem = new PropertyListView<PaymentConfiguration>("eachItem",
                                                              model) {
            @Override
            protected void populateItem(ListItem<PaymentConfiguration> item) {
                Link<PaymentConfiguration> viewLink = new Link<PaymentConfiguration>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new PaymentConfigurationViewPage(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };

        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
