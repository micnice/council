package zw.co.hitrac.council.web.pages;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.image.NonCachingImage;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.web.models.VersionReadOnlyModel;
import zw.co.hitrac.council.web.pages.registration.RegistrantPanel;
import zw.co.hitrac.council.web.pages.registration.RegistrantSnapShot;
import zw.co.hitrac.council.web.pages.registration.RegistrantViewPage;
import zw.co.hitrac.council.web.pages.research.CouncilSiteImageResource;
import zw.co.hitrac.council.web.pages.research.MyDynamicImageResource;
import zw.co.hitrac.council.web.pages.research.RegistrantImageResource;
import zw.co.hitrac.council.web.pages.security.SignOutPage;
import zw.co.hitrac.council.web.utility.CustomDateLabel;
import zw.co.hitrac.council.web.utility.CustomDateTimeLabel;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author charlesc
 */
public class RegistrantWebUserPage extends WebUserTemplatePage {

    @SpringBean
    private RegistrantService registrantService;

    private DetachableRegistrantModel detachableRegistrantModel;

    public RegistrantWebUserPage(String registrationNumber) {

        detachableRegistrantModel = new DetachableRegistrantModel(registrationNumber, registrantService);
        final CompoundPropertyModel<Registrant> model = new CompoundPropertyModel<Registrant>(detachableRegistrantModel);
        setDefaultModel(model);

        add(new RegistrantSnapShot("registrantSnapShot", model.getObject().getId()));

    }


}