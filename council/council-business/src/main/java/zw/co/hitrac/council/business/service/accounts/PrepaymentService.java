
package zw.co.hitrac.council.business.service.accounts;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.accounts.Prepayment;
import zw.co.hitrac.council.business.service.IGenericService;

/**
 *
 * @author Michael Matiashe

 */
public interface PrepaymentService extends IGenericService<Prepayment> {
    public List<Prepayment> findByInstitution(Institution institution);
    
    public List<Prepayment> findByCanceled(Boolean canceled);
    
    public List<Prepayment> findAvailablePrepayments();
    
    public List<Prepayment> findAll(Date endDate);

    public BigDecimal getPrepaymentBalance(Date endDate, Prepayment prepayment);

    public List<Institution> withPrepayments();
    
}
