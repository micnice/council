package zw.co.hitrac.council.web.pages.accounts;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.Bank;
import zw.co.hitrac.council.business.service.accounts.BankService;

/**
 *
 * @author Michael Matiashe
 */
public class BankAccountViewPage extends IBankBookPage {

    @SpringBean
    private BankService bankAccountServicee;

    public BankAccountViewPage(Long id) {
        CompoundPropertyModel<Bank> model=new CompoundPropertyModel<Bank>(new LoadableDetachableBankAccountModel(id));
        setDefaultModel(model);
        add(new Link<Bank>("editLink",model){

            @Override
            public void onClick() {
                setResponsePage(new BankAccountEditPage(getModelObject().getId()));
            }
            
        });
        add(new BookmarkablePageLink<Void>("returnLink", BankAccountListPage.class));
        add(new Label("name"));
        add(new Label("accNumber"));
        add(new Label("branch"));
        add(new Label("bank"));
    }


    private final class LoadableDetachableBankAccountModel extends LoadableDetachableModel<Bank> {

        private Long id;

        public LoadableDetachableBankAccountModel(Long id) {
            this.id = id;
        }

        @Override
        protected Bank load() {

            return bankAccountServicee.get(id);
        }
    }
}
