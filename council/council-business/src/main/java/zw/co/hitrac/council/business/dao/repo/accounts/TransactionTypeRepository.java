package zw.co.hitrac.council.business.dao.repo.accounts;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.accounts.TransactionType;

/**
 *
 * @author Charles Chigoriwa
 */
public interface TransactionTypeRepository extends CrudRepository<TransactionType, Long> {
    public List<TransactionType> findAll();
}
