package zw.co.hitrac.council.web.pages.accounts;

import org.apache.wicket.markup.html.link.BookmarkablePageLink;

/**
 *
 * @author Takunda Dhlakama
 * @author Michael Matiashe
 * @author Tatenda Chiwandire
 * @author Charles Chigoriwa
 */
public class GeneralLedgerPage extends IGeneralLedgerPage {
    
    public GeneralLedgerPage() {
        add(new BookmarkablePageLink<Void>("accountLink", AccountListPage.class));
    }
}