package zw.co.hitrac.council.business.domain;

/**
 *
 * @author Matiashe Michael
 */
public enum Groups {

    GROUP("Group"),
    GROUP_ONE("Group 1"),
    GROUP_TWO("Group 2"),
    GROUP_THREE("Group 3"),
    GROUP_FOUR("Group 4"),
    GROUP_FIVE("Group 5"),
    GROUP_SIX("Group 6"),
    GROUP_SEVEN("Group 7"),
    GROUP_EIGHT("Group 8"),
    GROUP_NINE("Group 9"),
    GROUP_TEN("Group 10");
    
    private Groups(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    private final String name;
}
