
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------
import com.googlecode.wicket.jquery.ui.panel.JQueryFeedbackPanel;
import java.util.ArrayList;
import net.sf.jasperreports.engine.JRException;

import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationService;
import zw.co.hitrac.council.business.service.examinations.ExamSettingService;
import zw.co.hitrac.council.reports.ExaminationCardReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

//~--- JDK imports ------------------------------------------------------------

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.examinations.ExamResult;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.web.models.CourseListModel;
import zw.co.hitrac.council.web.models.ExamSettingActiveListModel;

/**
 *
 * @author tdhlakama
 */
public class ExaminationCardPage extends IExaminationsPage {

    @SpringBean
    private ExamRegistrationService examRegistrationService;
    @SpringBean
    private ExamSettingService examSettingService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private CourseService courseService;
    private Institution institution;
    private ExamSetting examSetting;
    private String candidateNumber;
    private Course course;
    private HrisComparator hrisComparator = new HrisComparator();

    public ExaminationCardPage() {
        // FeedbackPanel //
        final FeedbackPanel feedback = new JQueryFeedbackPanel("feedback");
        add(feedback.setOutputMarkupId(true));
        PropertyModel<Institution> institutionModel = new PropertyModel<Institution>(this, "institution");
        PropertyModel<ExamSetting> examSettingModel = new PropertyModel<ExamSetting>(this, "examSetting");
        PropertyModel<Course> courseModel = new PropertyModel<Course>(this, "course");
        PropertyModel<String> candidateNumberModel = new PropertyModel<String>(this, "candidateNumber");
        Form<?> form = new Form("form") {
            @Override
            protected void onSubmit() {
                try {
                    Set<ExamResult> examResults = new HashSet<ExamResult>();
                    for (ExamResult er : examRegistrationService.getExamResults(examSetting, course, institution, candidateNumber, null, null)) {
                        if (er.getPreviousExamRegistration() == null) {
                            examResults.add(er);
                        }
                    }
                    List<ExamResult> results = (List<ExamResult>) hrisComparator.sortExaminationCards(new ArrayList<ExamResult>(examResults));

                        ExaminationCardReport examinationCardReport = new ExaminationCardReport();
                        ContentType contentType = ContentType.PDF;
                        Map parameters = new HashMap();
                        ByteArrayResource resource =
                                ReportResourceUtils.getReportResource(examinationCardReport, contentType, results, parameters);
                        IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                RequestCycle.get().getResponse(), null);

                        resource.respond(a);

                        // To make Wicket stop processing form after sending response
                        RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                    
                } catch (JRException ex) {
                    error(ex.getMessage());
                }
            }
        };

        form.add(new DropDownChoice("institution", institutionModel,
                new InstitutionsModel()).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("examSetting", examSettingModel, new ExamSettingActiveListModel(examSettingService)).setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("candidateNumber", candidateNumberModel));
        form.add(new DropDownChoice("course", courseModel, new CourseListModel(courseService), new ChoiceRenderer<Course>()) {
            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }

            @Override
            public boolean isRequired() {
                return true;
            }
        }.add(new ErrorBehavior()));
        add(form);
    }

    private class InstitutionsModel extends LoadableDetachableModel<List<? extends Institution>> {

        protected List<? extends Institution> load() {
            if (course != null) {
                return (List<Institution>) hrisComparator.sort((new ArrayList<Institution>(courseService.get(course.getId()).getTrainingInstitutions())));
            } else {
                return new ArrayList<Institution>();
            }
        }
    }
}
//~ Formatted by Jindent --- http://www.jindent.com
