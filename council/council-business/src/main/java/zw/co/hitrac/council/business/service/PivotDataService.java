package zw.co.hitrac.council.business.service;

import org.springframework.stereotype.*;
import org.springframework.stereotype.Service;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.reports.ProductSale;
import zw.co.hitrac.council.business.domain.reports.RegistrantBalance;
import zw.co.hitrac.council.business.process.RenewalProcess;

import java.util.List;

/**
 * Created by ezinzombe on 3/21/17.
 */
public interface PivotDataService {

    byte[] exportRegistrations();

    byte[] exportRenewals(List<RegistrantActivity>renewals);

    byte[] exportCarryForward(List<ProductSale> productSales);

    byte[] exportCarryForwardUsage(List<RegistrantBalance> registrantBalances);
}
