/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service;

import java.io.Serializable;
import zw.co.hitrac.council.business.domain.Title;

/**
 *
 * @author tidza
 */
public interface TitleService extends IGenericService<Title> {

	public Title findByName(String name);
}
