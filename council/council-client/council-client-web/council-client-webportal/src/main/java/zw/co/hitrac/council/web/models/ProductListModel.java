package zw.co.hitrac.council.web.models;

import java.util.List;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.service.accounts.ProductService;

/**
 *
 * @author Michael Matiashe
 */
public class ProductListModel extends LoadableDetachableModel<List<Product>> {

    private final ProductService productService;

    public ProductListModel(ProductService productService) {
        this.productService = productService;
    }

    @Override
    protected List<Product> load() {
        return this.productService.findAll(Boolean.FALSE);
    }
}
