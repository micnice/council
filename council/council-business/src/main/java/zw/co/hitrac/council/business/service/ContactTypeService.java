package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.ContactType;

/**
 *
 * @author Charles Chigoriwa
 */
public interface ContactTypeService extends IGenericService<ContactType> {
    
    
}
