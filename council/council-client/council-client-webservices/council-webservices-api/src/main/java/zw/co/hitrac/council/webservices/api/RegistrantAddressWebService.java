package zw.co.hitrac.council.webservices.api;

import java.io.Serializable;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import zw.co.hitrac.council.business.domain.RegistrantAddress;
import zw.co.hitrac.council.webservices.api.domain.LeanRegistrantAddress;

/**
 *
 * @author Daniel Nkhoma
 */
@Path("/registrantAddresses")
@Produces(MediaType.APPLICATION_XML)
@Consumes(MediaType.APPLICATION_XML)
public interface RegistrantAddressWebService extends Serializable{
    
    @GET
    @Path("list")
    public List<RegistrantAddress> registrantAddresses(Long registrantId);
    
    @GET
    @Path("addresslist")
    public List<RegistrantAddress> registrantAddresses(@QueryParam(value = "registrationNumber") String registrationNumber);
    
    @GET
    @Path("listAll")
    public List<RegistrantAddress> registrantAddresses();
    
    @GET
    @Path("lean")
    public List<LeanRegistrantAddress> leanRegistrantAddresses();
    
    @GET
    @Path("registrantAddress/{id}")
    public RegistrantAddress registrantAddress(@PathParam(value = "id") Long id);
    
}
