package zw.co.hitrac.council.web.pages.reports;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.CouncilDurationService;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.RegistrantQualificationService;
import zw.co.hitrac.council.reports.RegistrantDataDBReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.models.CouncilDurationListModel;
import zw.co.hitrac.council.web.models.CourseListModel;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 * @author tdhlakama
 */
public class CourseMappedCourseReport extends TemplatePage {

    private Course course;
    private Course course2;
    private CouncilDuration councilDuration;
    private String registrantStatus;
    @SpringBean
    private CourseService courseService;
    @SpringBean
    private DataSource dataSource;
    @SpringBean
    private CouncilDurationService councilDurationService;
    @SpringBean
    private RegistrantQualificationService registrantQualificationService;
    private ContentType contentType;
    
    public CourseMappedCourseReport() {
        menuPanelManager.getSearchPanel().setTopMenuCurrent(true);
        menuPanelManager.getSearchPanel
                ().setSearchAdvanced(true);

        PropertyModel<Course> courseModel = new PropertyModel<Course>(this, "course");
        PropertyModel<ContentType> contentTypeModel = new PropertyModel<ContentType>(this, "contentType");
        PropertyModel<Course> courseModel2 = new PropertyModel<Course>(this, "course2");
        PropertyModel<CouncilDuration> councilDurationModel = new PropertyModel<CouncilDuration>(this, "councilDuration");
        PropertyModel<String> registrantStatusModel = new PropertyModel<String>(this, "registrantStatus");
        Form<?> form = new Form("form");
        List statuslist = Arrays.asList(new String[]{"Active", "InActive"});
        form.add(new DropDownChoice("course", courseModel, new CourseListModel(courseService)));
        form.add(new DropDownChoice("course2", courseModel2, new CourseListModel(courseService)));
        form.add(new DropDownChoice("councilDuration", councilDurationModel, new CouncilDurationListModel(councilDurationService)));
        form.add(new DropDownChoice("registrantStatus", registrantStatusModel, statuslist));
        form.add(new DropDownChoice("contentType", contentTypeModel, Arrays.asList(ContentType.values())));
        
        add(form);

        form.add(new Button("process") {
            @Override
            public void onSubmit() {
                printReport();
            }
        });
        add(new FeedbackPanel("feedback"));
    }

    private void printReport() {
        Map parameters = new HashMap();
        RegistrantDataDBReport registrantDataDBReport = new RegistrantDataDBReport();
        try {

            List<RegistrantData> registrants = registrantQualificationService.getRegistrantDataListWithBoth(course, course2,councilDuration,registrantStatus);

            parameters.put("comment", course + " Detailed Register for those with the " + course2);

            String list = "";
            for (RegistrantData d : registrants) {
                list += d.getId().toString() + ",";
            }
            if (!list.isEmpty()) {
                list = list.substring(0, list.length() - 1);
            }
            parameters.put("list", list);
            
            final Connection connection = dataSource.getConnection(); //DBConnect.getConnection();
            ByteArrayResource resource = null;
            try {
                resource = ReportResourceUtils.getReportResource(registrantDataDBReport, contentType, connection, parameters);
            } finally {
                connection.close();
            }
            IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                    RequestCycle.get().getResponse(), null);

            resource.respond(a);

            // To make Wicket stop processing form after sending response
            RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
        } catch (JRException ex) {
            ex.printStackTrace(System.out);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }

    }

}


//~ Formatted by Jindent --- http://www.jindent.com
