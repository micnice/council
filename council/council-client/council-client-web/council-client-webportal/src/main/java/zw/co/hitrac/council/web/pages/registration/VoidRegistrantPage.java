package zw.co.hitrac.council.web.pages.registration;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author Takunda Dhlakama
 * @author Matiashe Michael
 */
public class VoidRegistrantPage extends TemplatePage {

    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private RegistrationProcess registrationProcess;

    public VoidRegistrantPage(Long id) {
        menuPanelManager.getRegistrantionPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantionPanel().setRegistrantEditPageCurrent(true);
        CompoundPropertyModel<Registrant> model =  new CompoundPropertyModel<Registrant>(new LoadableDetachableRegistrantModel(id));
        setDefaultModel(model);
        model.getObject().setComment("NO ADEQUATE INFORMATION");
        add(new RegistrantPanel("registrantPanel", id));        
        Form<Registrant> form = new Form<Registrant>("form", (IModel<Registrant>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    Registrant registrant = registrationProcess.voidedRegistrantActivity(getModelObject());
                    setResponsePage(new RegistrantViewPage(registrant.getId()));
                } catch (CouncilException e) {
                    error(e.getMessage());
                }
            }
        };
        form.add(new CheckBox("voided"));
        form.add(new TextField<String>("comment").add(new ErrorBehavior()));
        add(form);
        add(new FeedbackPanel("feedback"));
    }

    private final class LoadableDetachableRegistrantModel extends LoadableDetachableModel<Registrant> {

        private Long id;

        public LoadableDetachableRegistrantModel(Long id) {
            this.id = id;
        }

        @Override
        protected Registrant load() {
            Registrant registrant = null;

            if (id == null) {
                registrant = new Registrant();
            } else {
                registrant = registrantService.get(id);
            }
            return registrant;
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
