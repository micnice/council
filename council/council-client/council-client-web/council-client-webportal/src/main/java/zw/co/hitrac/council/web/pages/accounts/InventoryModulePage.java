package zw.co.hitrac.council.web.pages.accounts;

import org.apache.wicket.markup.html.link.BookmarkablePageLink;

/**
 *
 * @author Edward Zengeni
 */
public class InventoryModulePage extends IInventoryModulePage {

    public InventoryModulePage() {
        add(new BookmarkablePageLink<Void>("productPriceLink", ProductPriceListPage.class));
        add(new BookmarkablePageLink<Void>("productSaleLink", ProductListPage.class));
        add(new BookmarkablePageLink<Void>("productGroupListLink", ProductGroupListPage.class));
    }
}
