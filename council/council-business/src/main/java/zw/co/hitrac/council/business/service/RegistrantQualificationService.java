package zw.co.hitrac.council.business.service;

import java.util.Date;
import java.util.List;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.ProductIssuanceType;
import zw.co.hitrac.council.business.domain.Qualification;
import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.business.domain.RegisterType;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantQualification;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;

/**
 *
 * @author Michael Matiashe
 */
public interface RegistrantQualificationService extends IGenericService<RegistrantQualification> {

    public Date getDateAwarded(Registrant registrant, Course course);

    public boolean checkQualification(Registrant registrant, Course course);

    public Date getLastPBQDateAwarded(Registrant registrant);

    public RegistrantQualification get(Registrant registrant, Qualification qualification, Institution institution);

    public RegistrantQualification get(Registrant registrant, Qualification qualification);

    public RegistrantQualification get(Registrant registrant, String name);

    public List<RegistrantQualification> getRegistrantQualifications(Registrant registrant);

    public List<RegistrantData> getRegistrantQualifications();

    RegistrantQualification getRegistrantCurrentQualifications(Registrant registrant);

    public List<RegistrantData> getRegistrantQualificationIds(Qualification qualification);

    public Integer getRegistrantQualifications(Qualification qualification);

    public List<RegistrantQualification> getRegistrantQualifications(RegisterType registerType);

    public List<RegistrantQualification> getRegistrantQualifications(Qualification qualification, Institution institution, Date startDate, Date endDate);

    public List<Registrant> getRegistrants(RegisterType registerType);

    public RegistrantQualification getRegistrantQualificationByRegistrantAndCourse(Registrant registrant, Course course);

    public List<Registrant> getRegistrants(Qualification qualification, Institution institution, Date startDate, Date endDate);

    public List<RegistrantData> getRegistrantQualificationList(Qualification qualification, Institution institution, Date startDate, Date endDate);

    public List<RegistrantQualification> getRegistrantQualificationCertificateType(Registrant registrant, ProductIssuanceType productIssuanceType);

    public List<Qualification> getDistinctQualificationsInRegistrantQualification();

    public Integer getTotalPerQualificationMappedCourseAndRegister(Course course, Register register);

    public List<Institution> getDistinctInstitutionsInRegistrantQualification();

    public List<RegistrantQualification> getRegistrantPracticeQualificationList(Registrant registrant);

    public List<RegistrantQualification> getRegistrantAdditionalPracticeQualificationList(Registrant registrant);

    public List<RegistrantData> getRegistrantDataListWithBoth(Course course, Course course2, CouncilDuration councilDuration, String status);

    public List<RegistrantData> getRegistrantDataListWithBoth(Qualification qualification, Qualification qualification2);
    
    public List<Course> getRegisteredDisciplines(Registrant registrant);
}
