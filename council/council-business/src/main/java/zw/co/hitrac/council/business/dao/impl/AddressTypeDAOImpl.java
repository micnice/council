package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.AddressTypeDAO;
import zw.co.hitrac.council.business.dao.repo.AddressTypeRepository;
import zw.co.hitrac.council.business.domain.AddressType;

/**
 *
 * @author Charles Chigoriwa
 */
@Repository
public class AddressTypeDAOImpl implements AddressTypeDAO {

    @Autowired
    private AddressTypeRepository addressTypeRepository;

    public AddressType save(AddressType addressType) {
        return addressTypeRepository.save(addressType);
    }

    public List<AddressType> findAll() {
        return addressTypeRepository.findAll();
    }

    public AddressType get(Long id) {
        return addressTypeRepository.findOne(id);
    }

    /**
     * A setter method that will make mocking repo object easier
     *
     * @param addressTypeRepository
     */

    public void setAddressTypeRepository(AddressTypeRepository addressTypeRepository) {
        this.addressTypeRepository = addressTypeRepository;
    }

    public List<AddressType> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AddressType findByName(String name) {
        return addressTypeRepository.findByName(name);
    }
}
