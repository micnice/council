package zw.co.hitrac.council.business.process;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.accounts.*;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.business.service.accounts.AccountsParametersService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.business.utils.Role;

import java.math.BigDecimal;
import java.util.*;

/**
 *
 * @author Michael Matiashe
 */
@Service
public class ApplicationProcess {

    @Autowired
    private GeneralParametersService generalParametersService;
    @Autowired
    private ApplicationService applicationService;
    @Autowired
    private RegistrationService registrationService;
    @Autowired
    private RegistrantService registrantService;
    @Autowired
    private ProductFinder productFinder;
    @Autowired
    private ProductIssuer productIssuer;
    @Autowired
    private RegistrantActivityProcess registrantActivityProcess;
    @Autowired
    private CourseService courseService;
    @Autowired
    private SupportDocumentService supportDocumentService;
    @Autowired
    private BillingProcess billingProcess;
    @Autowired
    private InstitutionService institutionService;
    @Autowired
    private RegistrationProcess registrationProcess;
    @Autowired
    private TransferProcess transferProcess;
    @Autowired
    private RegistrantConditionService registrantConditionService;
    @Autowired
    private RegisterService registerService;
    @Autowired
    private DurationService durationService;
    @Autowired
    private RenewalProcess renewalProcess;
    @Autowired
    private RegistrantQualificationService registrantQualificationService;
    @Autowired
    private RegistrantClearanceService registrantClearanceService;
    @Autowired
    private AccountsParametersService accountsParametersService;

    @Transactional
    public Application apply(Application application) {
        DebtComponent debtComponent = null;
        Product productFound = null;
        if (application.getApplicationPurpose() != ApplicationPurpose.CERTIFICATE_OF_GOOD_STANDING
                && application.getApplicationPurpose() != ApplicationPurpose.INSTITUTION_REGISTRATION
                && application.getApplicationPurpose() != ApplicationPurpose.TRANSFER
                && application.getApplicationPurpose() != ApplicationPurpose.CHANGE_OF_NAME
                && application.getApplicationPurpose() != ApplicationPurpose.PROVISIONAL_QUALIIFICATION
                && application.getApplicationPurpose() != ApplicationPurpose.STUDENT_REGISTRATION
                && application.getApplicationPurpose() != ApplicationPurpose.SUPERVISION
                && application.getApplicationPurpose() != ApplicationPurpose.ADDITIONAL_QUALIIFICATION
                ) {
            //register registration

            if (application.getApplicationPurpose().equals(ApplicationPurpose.PROVISIONAL_REGISTRATION) && !generalParametersService.get().getAllowMultipleRegistrations()) {
                Registration mainRegistration = registrationService.getCurrentRegistrationByRegister(generalParametersService.get().getMainRegister(), application.getRegistrant());

                if (mainRegistration != null) {

                    throw new CouncilException("Already in main register");

                }

            }
            if (application.getApplicationPurpose().equals(ApplicationPurpose.PROVISIONAL_REGISTRATION) || accountsParametersService.get().getRegistrationApplicationPayment()) {
                debtComponent = billingProcess.billApplicationProduct(application, debtComponent);
            }
        }

        if (application.getApplicationPurpose() == ApplicationPurpose.ADDITIONAL_QUALIIFICATION) {
            debtComponent = billingProcess.billAdditionalQualificationProduct(application, debtComponent);
        }

        if (application.getApplicationPurpose() == ApplicationPurpose.STUDENT_REGISTRATION) {
            debtComponent = billingProcess.billStudentApplicationProduct(application, debtComponent);
        }

        if (application.getApplicationPurpose() == ApplicationPurpose.CERTIFICATE_OF_GOOD_STANDING) {
            Registration currentRegistration = registrationProcess.activeRegisterNotStudentRegister(application.getRegistrant());

            if (currentRegistration != null) {

                Course currentCourse = currentRegistration.getCourse();

                if (application.getCreatedBy().getRoles().contains(Role.REGISTRY_CLERK) || application.getCreatedBy().getRoles().contains(Role.DATA_CAPTURE_CLERK)) {
                    if (!currentCourse.getHasCGSApplication()) {
                        throw new CouncilException("Current Discipline cannot apply for CGS");
                    }

                    if (generalParametersService.get().getExamClearance()) {
                        RegistrantClearance rc = registrantClearanceService.getRegistrantClearance(currentRegistration.getRegistrant(), currentCourse);
                        if (rc != null) {

                            if (rc.getCourse().equals(currentCourse)) {

                                if (new Date().before(DateUtils.addYears(currentRegistration.getRegistrationDate(), generalParametersService.get().getMinimumYearsForCGSApplication()))) {
                                    throw new CouncilException("Minimun Number of Years for CGS Application not met");
                                }

                            }
                        }

                        if (registrantClearanceService.getHasPBQExamClearance(currentRegistration.getRegistrant())) {

                            Date dateAwarded = registrantQualificationService.getLastPBQDateAwarded(currentRegistration.getRegistrant());

                            if (dateAwarded != null) {
                                if (new Date().before(DateUtils.addYears(dateAwarded, generalParametersService.get().getMinimumYearsForPBQCGSApplication()))) {
                                    throw new CouncilException("Minimun Number of Years for CGS Application After Post Basic Qualification not met");
                                }
                            } else {
                                throw new CouncilException("Correct Date(s) Awarded for Qualifications or Qualification Mapped Discipline");
                            }

                        }
                    }
                }

                Duration d = durationService.getDurationByCourseAndCouncilDuration(currentRegistration.getCourse(), application.getCouncilDuration());
                if (d != null) {
                    if (renewalProcess.hasCurrentRenewalActivity(currentRegistration.getRegistrant(), d)) {
                        debtComponent = billingProcess.billCGSProduct(application, debtComponent);
                    } else {
                        throw new CouncilException("Renewal Needed First For Selected Period");
                    }
                } else {
                    throw new CouncilException("Duration Not Found");
                }

            } else {
                throw new CouncilException("No Current Registration Found");
            }
        }

        if (application.getApplicationPurpose() == ApplicationPurpose.PROVISIONAL_QUALIIFICATION) {
            debtComponent = billingProcess.billQualificationProduct(application, debtComponent);
        }
        if (application.getApplicationPurpose().equals(ApplicationPurpose.CHANGE_OF_NAME)) {
            debtComponent = billingProcess.billApplicationForChangeOfNameProduct(application, debtComponent);
        }
        if (application.getApplicationPurpose().equals(ApplicationPurpose.TRANSCRIPT_OF_TRAINING)) {
            debtComponent = billingProcess.billTranscriptOfTrainingProduct(application, debtComponent);
        }
        if (application.getApplicationPurpose().equals(ApplicationPurpose.SUPERVISION)) {
            //  debtComponent = billingProcess.billApplicationForSupervisionProduct(application, debtComponent);
        }
        if (application.getApplicationPurpose() == ApplicationPurpose.TRANSFER) {

            if (generalParametersService.get().getRegisterQualificationBeforeTransfer()) {
                if (!registrantQualificationService.checkQualification(application.getRegistrant(), application.getTransferRule().getToCourse())) {
                    throw new CouncilException("Qualifications Required");
                }
            }
            if (application.getTransferRule().getProduct() != null) {
                debtComponent = billingProcess.billApplicationForTransferProduct(application, debtComponent);
            }
            if (generalParametersService.get().getDirectRegistration()) {
                application.setDebtComponent(debtComponent);
                application = applicationService.save(application);
                RegistrantTransfer registrantTransfer = new RegistrantTransfer();
                registrantTransfer.setRegistrant(application.getRegistrant());
                registrantTransfer.setTransferDate(new Date());
                registrantTransfer.setTransferRule(application.getTransferRule());
                registrantTransfer.setApplication(application);
                Registration currentRegistration = registrationProcess.activeRegisterNotStudentRegister(application.getRegistrant());
                if (currentRegistration != null && application.getTransferRule().getToCourse() == null) {
                    registrantTransfer.setCourse(currentRegistration.getCourse());
                } else {
                    registrantTransfer.setCourse(application.getTransferRule().getToCourse());
                }
                registrantTransfer.setCreatedBy(registrantTransfer.getCreatedBy());
                registrantTransfer = transferProcess.transfer(registrantTransfer);
                application.setUsed(Boolean.TRUE);
                application.setProcessedBy(application.getCreatedBy());
                application.setApprovedBy(application.getCreatedBy());
                application.setApplicationStatus(Application.APPLICATIONAPPROVED);
                application = applicationService.save(application);
                Registrant registrant = registrantService.get(application.getRegistrant().getId());

                for (RegistrantCondition rc : registrantConditionService.getRegisterRegistrantConditions(registrant)) {
                    rc.setStatus(Boolean.FALSE);
                    registrantConditionService.save(rc);
                }
                Register register = registerService.get(registrantTransfer.getTransferRule().getToRegister().getId());
                registrantConditionService.saveRegisterConditions(register, registrant);

                return application;
            }

        }
        //--kelvin---application for unrestricted practicing certificate product finder
        if (application.getApplicationPurpose().equals(ApplicationPurpose.UNRESTRICTED_PRACTICING_CERTIFICATE)) {
            debtComponent = billingProcess.billApplicationForUnrestrictedPractiProduct(application, debtComponent);
        }

        if (application.getApplicationPurpose() == ApplicationPurpose.INSTITUTION_REGISTRATION) {
            if (checkRegistrantExperience(application.getRegistrant()) < generalParametersService.get().getMinimumYearsForInstitutionApplication()) {
                throw new CouncilException("Working Experience below requirements level - " + generalParametersService.get().getMinimumYearsForInstitutionApplication() + " years required");
            }

            Registration currentRegistration = registrationProcess.activeRegisterNotStudentRegister(application.getRegistrant());
            if (currentRegistration != null) {
                Duration d = durationService.getDurationByCourseAndCouncilDuration(currentRegistration.getCourse(), application.getCouncilDuration());
                if (d != null) {
                    if (renewalProcess.hasCurrentRenewalActivity(currentRegistration.getRegistrant(), d)) {
                        if (application.getInstitution().getCustomerAccount() == null || application.getInstitution().getCustomerAccount().getId() == null) {
                            application.getInstitution().setCustomerAccount(new Customer());
                            application.getInstitution().getCustomerAccount().setCode(UUID.randomUUID().toString());
                            application.getInstitution().getCustomerAccount().setAccount(new Account());
                            application.getInstitution().getCustomerAccount().getAccount().setBalance(BigDecimal.ZERO);
                            application.getInstitution().getCustomerAccount().getAccount().setAccountType(AccountType.ACCOUNTS_RECEIVABLE);
                            application.getInstitution().getCustomerAccount().getAccount().setCode(UUID.randomUUID().toString());
                            application.getInstitution().getCustomerAccount().setCustomerName(application.getInstitution().getName());
                            application.getInstitution().getCustomerAccount().getAccount().setName(application.getInstitution().getName());
                        }
                        institutionService.save(application.getInstitution());
                        debtComponent = billingProcess.billApplicatonForInstitutionRegistrationProduct(application, debtComponent);

                    } else {
                        throw new CouncilException("Renewal Needed First For Selected Period");
                    }

                } else {
                    throw new CouncilException("Duration Not Found");
                }

            } else {
                throw new CouncilException("No Current Registration Found");
            }
        }

        //Done with billing side
        //Register now
        application.setDebtComponent(debtComponent);

        if (generalParametersService.get().getRegistrationByApplication()) {
            application.setApprovedCourse(application.getCourse());
        }
        if (application.getQualificationType() != null && generalParametersService.get().getDirectApproval() != null) {
            if (generalParametersService.get().getDirectApproval() && application.getQualificationType().equals(QualificationType.LOCAL)) {
                application.setApprovedBy(application.getCreatedBy());
                application.setApprovedCourse(application.getCourse());
                application.setDateProcessed(new Date());
                application.setProcessedBy(application.getCreatedBy());
                application.setApplicationStatus(Application.APPLICATIONAPPROVED);
                application = applicationService.save(application);
            } else {

                application = applicationService.save(application);
            }
        } else {
            application = applicationService.save(application);
        }
        application = applicationService.save(application);
        return application;
    }

    @Transactional
    public Application approve(Application application) {
        //Check if application fee is fully paid

        if (application.getApplicationPurpose().equals(ApplicationPurpose.PROVISIONAL_REGISTRATION) || accountsParametersService.get().getRegistrationApplicationPayment()) {
            if (application.getDebtComponent() != null) {
                if (application.getDebtComponent().getRemainingBalance().compareTo(BigDecimal.ZERO) != 0) {
                    throw new CouncilException("Application fee is not yet fully paid");
                }
            }
        }
        if (generalParametersService.get().getRegistrationByApplication()) {
            if ((int) DateUtil.DifferenceInYears(application.getApplicationDate(), new Date()) >= 1) {
                application.setExpired(Boolean.TRUE);
                applicationService.save(application);
                throw new CouncilException("The Application Is Already Expired");

            }
        }

        if (generalParametersService.get().getRegistrationByApplication()) {
            if (application.getApplicationPurpose() != ApplicationPurpose.CERTIFICATE_OF_GOOD_STANDING
                    && application.getApplicationPurpose() != ApplicationPurpose.INSTITUTION_REGISTRATION
                    && application.getApplicationPurpose() != ApplicationPurpose.TRANSFER
                    && application.getApplicationPurpose() != ApplicationPurpose.CHANGE_OF_NAME
                    && application.getApplicationPurpose() != ApplicationPurpose.PROVISIONAL_QUALIIFICATION
                    && application.getApplicationPurpose() != ApplicationPurpose.TRANSCRIPT_OF_TRAINING
                    && application.getApplicationPurpose() != ApplicationPurpose.STUDENT_REGISTRATION
                    && application.getApplicationPurpose() != ApplicationPurpose.SUPERVISION) {

                Set<Requirement> totalRequirements = new HashSet<Requirement>();
                List<Requirement> courseRequirements = new ArrayList<Requirement>(courseService.get(application.getCourse().getId()).getRequirements());
                List<Requirement> registerRequirements = new ArrayList<Requirement>(registerService.get(getApplicationRegister(application).getId()).getRequirements());
                totalRequirements.addAll(courseRequirements);
                totalRequirements.addAll(registerRequirements);
                Set<Requirement> requirements = new HashSet<Requirement>();
                for (Requirement r : totalRequirements) {
                    SupportDocument document = supportDocumentService.getSupportDocument(application, r);
                    if (document == null) {
                        requirements.add(r);
                    }
                }

                final Set<Requirement> finalRequirementsSet = new HashSet<Requirement>(requirements);
                if (!generalParametersService.get().getHasProcessedByBoard()) {
                    Set<Requirement> finalRequirementsSet1 = new HashSet<Requirement>(requirements);
                    RegistrantQualification registrantQualification = registrantQualificationService.getRegistrantQualificationByRegistrantAndCourse(application.getRegistrant(), application.getApprovedCourse());
                    if (registrantQualification != null) {
                        if (registrantQualification.getQualificationType().equals(QualificationType.LOCAL)) {
                            for (Requirement r : finalRequirementsSet1) {
                                if (r.getQualificationType() != null && r.getQualificationType().equals(QualificationType.FOREIGN)) {
                                    finalRequirementsSet.remove(r);
                                }
                            }
                        }
                    }
                }

            }
        }

        DebtComponent debtComponent = application.getDebtComponent();

        if (application.getApplicationStatus().equals(Application.APPLICATIONAPPROVED)) {

            if (application.getDateApproved() == null) {
                throw new CouncilException("Enter Descision Date");
            }

            if (application.getApprovedBy() == null) {
                throw new CouncilException("Enter Approved By");
            }

            if (application.getDateProcessed() == null) {
                throw new CouncilException("Enter Processed Date");
            }

            if (application.getProcessedBy() == null) {
                throw new CouncilException("Enter Processed By");
            }

            if (application.getApplicationPurpose().equals(ApplicationPurpose.CERTIFICATE_OF_GOOD_STANDING)) {
                Boolean checkApp = Boolean.FALSE;
                Registration currentRegistration = registrationProcess.activeRegisterNotStudentRegister(application.getRegistrant());
                if (currentRegistration != null) {
                    Duration d = durationService.getDurationByCourseAndCouncilDuration(currentRegistration.getCourse(), application.getCouncilDuration());
                    if (d != null) {
                        checkApp = Boolean.TRUE;
                        productIssuer.issue_Product_CERTIFICATE_OF_GOOD_STANDING(application, debtComponent, d);
                    }
                }
                if (checkApp == Boolean.FALSE) {
                    productIssuer.issue_Product_CERTIFICATE_OF_GOOD_STANDING(application, debtComponent);
                }
            }

            if (application.getApplicationPurpose().equals(ApplicationPurpose.STUDENT_REGISTRATION)) {

                Registration studentRegistration = new Registration();
                studentRegistration.setRegister(generalParametersService.get().getStudentRegister());
                studentRegistration.setApplication(application);
                studentRegistration.setCourse(application.getCourse());
                studentRegistration.setDebtComponent(debtComponent);
                studentRegistration.setInstitution(application.getInstitution());
                studentRegistration.setRegistrant(application.getRegistrant());
                studentRegistration.setRegistrationDate(new Date());
                studentRegistration.setDuration(durationService.getCurrentCourseDuration(application.getCourse()));

                if (!generalParametersService.get().getHasProcessedByBoard()) {
                    billingProcess.billAnnualProduct(studentRegistration, debtComponent, application);
                }

                studentRegistration = registrationProcess.register(studentRegistration);

                registrantActivityProcess.registrationAndRenewalActivity(studentRegistration, debtComponent, studentRegistration.getDuration());
                productIssuer.issueProductPracticeCertificate(studentRegistration, debtComponent, durationService.getCurrentCourseDuration(application.getCourse()));
                productIssuer.issueRegistrationOrAdditionalQualficationCertificate(studentRegistration, debtComponent);
            }

            if (application.getApplicationPurpose().equals(ApplicationPurpose.UNRESTRICTED_PRACTICING_CERTIFICATE)) {
                productIssuer.issueUnrestrictedPractisingCertificate(registrationProcess.activeRegisterNotStudentRegister(application.getRegistrant()), debtComponent);
            }

            if (application.getApplicationPurpose().equals(ApplicationPurpose.CHANGE_OF_NAME)) {

                Registrant registrant = application.getRegistrant();
                registrantActivityProcess.changeOfNameRegistrantActivity(application, debtComponent);
                if (application.getFirstName() != null) {
                    registrant.setFirstname(application.getFirstName());
                }
                if (application.getMiddleName() != null) {
                    registrant.setMiddlename(application.getMiddleName());
                }
                if (application.getLastName() != null) {
                    registrant.setLastname(application.getLastName());
                }

                if (application.getMaidenName() != null) {
                    registrant.setMaidenname(application.getMaidenName());
                }
                registrantService.save(registrant);
            }

            if (application.getApplicationPurpose().equals(ApplicationPurpose.SUPERVISION)) {
                Registrant registrant = application.getRegistrant();
                registrant.setSupervisor(Boolean.TRUE);
                registrantService.save(registrant);
            }

            if (application.getApplicationPurpose().equals(ApplicationPurpose.PROVISIONAL_REGISTRATION) || application.getApplicationPurpose().equals(ApplicationPurpose.INTERN_REGISTRATION) || application.getApplicationPurpose().equals(ApplicationPurpose.MAIN_REGISTRATION) || application.getApplicationPurpose().equals(ApplicationPurpose.SPECIALIST_REGISTRATION)) {
                if (application.getApprovedCourse() == null) {
                    throw new CouncilException("Enter Approved Course");
                }
            }

            if (application.getApplicationStatus().equals(Application.APPLICATIONAPPROVED)) {
                application.setUsed(Boolean.TRUE);
            }

            return applicationService.save(application);
        } else if (application.getApplicationStatus().equals(Application.APPLICATIONDECLINED)) {
            if (application.getComment() != null) {
                application.setUsed(Boolean.TRUE);
                return applicationService.save(application);
            } else {
                throw new CouncilException("Comment is required for Declined Application ");
            }

        } else {
            return applicationService.save(application);
        }
    }

    public int checkRegistrantExperience(Registrant registrant) {
        int years = 0;
        for (Registration r : registrationService.getRegistrations(registrant)) {
            if (r.getRegister().equals(generalParametersService.get().getMainRegister())) {
                if (r.getRegistrationDate() != null) {
                    years = (int) DateUtil.DifferenceInYears(r.getRegistrationDate(), new Date());
                }
            }
        }
        return years;
    }

    public Register getApplicationRegister(Application application) {
        Register register = null;
        if (application.getApplicationPurpose().equals(ApplicationPurpose.INTERN_REGISTRATION)) {
            register = generalParametersService.get().getInternRegister();
        }
        if (application.getApplicationPurpose().equals(ApplicationPurpose.MAIN_REGISTRATION)) {
            register = generalParametersService.get().getMainRegister();
        }
        if (application.getApplicationPurpose().equals(ApplicationPurpose.PROVISIONAL_REGISTRATION)) {
            register = generalParametersService.get().getProvisionalRegister();
        }
        if (application.getApplicationPurpose().equals(ApplicationPurpose.SPECIALIST_REGISTRATION)) {
            register = generalParametersService.get().getSpecialistRegister();
        }
        if (application.getApplicationPurpose().equals(ApplicationPurpose.STUDENT_REGISTRATION)) {
            register = generalParametersService.get().getStudentRegister();
        }
        if (application.getApplicationPurpose().equals(ApplicationPurpose.INSTITUTION_REGISTRATION)) {
            register = generalParametersService.get().getInstitutionRegister();
        }
        if (application.getApplicationPurpose().equals(ApplicationPurpose.TRANSFER)) {
            register = registrationProcess.activeRegisterNotStudentRegister(application.getRegistrant()).getRegister();
        }
        if (register == null) {
            register = generalParametersService.get().getMainRegister();
        }
        return register;
    }
}
