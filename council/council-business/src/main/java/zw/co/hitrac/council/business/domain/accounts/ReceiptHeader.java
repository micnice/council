package zw.co.hitrac.council.business.domain.accounts;

import org.hibernate.annotations.Formula;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import zw.co.hitrac.council.business.domain.BaseIdEntity;
import zw.co.hitrac.council.business.domain.User;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Michael Matiashe
 */
@Entity
@Table(name = "receiptheader")
@Audited
public class ReceiptHeader extends BaseIdEntity {

    private Set<ReceiptItem> receiptItems = new HashSet<ReceiptItem>();
    private Date date;
    private BigDecimal totalAmount; //total amount paid
    private TransactionType transactionType;
    private Long generatedReceiptNumber;
    private PaymentDetails paymentDetails;
    private Boolean reversed = Boolean.FALSE;
    private Boolean printed = Boolean.FALSE;
    private String comment;
    private User canceledBy;
    private Date canceledDate;
    private BigDecimal carryForward = BigDecimal.ZERO;  //carryForward used or unused  
    private BigDecimal totalAmountPaid = BigDecimal.ZERO; //total amount used for receipt items
    private TransactionType transactionTypeChange;//If wrong account is used, old transaction Type

    @ManyToOne
    public TransactionType getTransactionTypeChange() {
        return transactionTypeChange;
    }

    public void setTransactionTypeChange(TransactionType transactionTypeChange) {
        this.transactionTypeChange = transactionTypeChange;
    }

    public Boolean getPrinted() {
        return printed;
    }

    public void setPrinted(Boolean printed) {
        this.printed = printed;
    }

    public Boolean getReversed() {
        return reversed;
    }

    public void setReversed(Boolean reversed) {
        this.reversed = reversed;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    @OneToMany(mappedBy = "receiptHeader")
    public Set<ReceiptItem> getReceiptItems() {
        return receiptItems;
    }

    public void setReceiptItems(Set<ReceiptItem> receiptItems) {
        this.receiptItems = receiptItems;
    }

    @ManyToOne
    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    @OneToOne
    public PaymentDetails getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(PaymentDetails paymentDetails) {
        this.paymentDetails = paymentDetails;
    }

    @Column(unique = true)
    public Long getGeneratedReceiptNumber() {
        return generatedReceiptNumber;
    }

    public void setGeneratedReceiptNumber(Long generatedReceiptNumber) {
        this.generatedReceiptNumber = generatedReceiptNumber;
    }

    @ManyToOne
    public User getCanceledBy() {
        return canceledBy;
    }

    public void setCanceledBy(User canceledBy) {
        this.canceledBy = canceledBy;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getCanceledDate() {
        return canceledDate;
    }

    public void setCanceledDate(Date canceledDate) {
        this.canceledDate = canceledDate;
    }

    //Total items cost total - value charged
    @Formula(value = "(select sum(r.amount) from receiptitem r where r.receiptHeader_id = id)")
    @NotAudited //required for @Formula
    public BigDecimal getTotalAmountPaid() {
        return totalAmountPaid;
    }

    public void setTotalAmountPaid(BigDecimal totalAmountPaid) {
        this.totalAmountPaid = totalAmountPaid;
    }

    @Formula(value = "totalAmount - (select sum(r.amount) from receiptitem r where r.receiptHeader_id = id)")
    @NotAudited //required for @Formula
    public BigDecimal getCarryForward() {
        return carryForward;
    }

    public void setCarryForward(BigDecimal carryForward) {
        this.carryForward = carryForward;
    }

    @Transient
    public String getStatus() {
        return reversed ? "Canceled - * " + getComment() : "";
    }

    @Transient
    public String getCanceled() {
        return reversed ? "Canceled" : "";
    }

    @Column(length = 500)
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Transient
    public BigDecimal getCalculatedTotalAmountPaid() {
        BigDecimal value = new BigDecimal(0);
        for (ReceiptItem item : receiptItems) {
            value = value.add(item.getAmount());
        }
        return value;
    }

    @Transient
    public BigDecimal getCalculatedCarryForward() {
        BigDecimal value = getTotalAmount().subtract(getCalculatedTotalAmountPaid());
        return value;
    }

}
