package zw.co.hitrac.council.web.pages.configure;

/**
 *
 * @author edward
 */
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Citizenship;
import zw.co.hitrac.council.business.service.CitizenshipService;

/**
 *
 * @author charlesc
 */
public class CitizenshipViewPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private CitizenshipService citizenshipService;

    public CitizenshipViewPage(Long id) {
        CompoundPropertyModel<Citizenship> model = new CompoundPropertyModel<Citizenship>(new zw.co.hitrac.council.web.pages.configure.CitizenshipViewPage.LoadableDetachableCitizenshipModel(id));
        setDefaultModel(model);
        add(new Link<Citizenship>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new CitizenshipEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", CitizenshipListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("citizenship", model.bind("name")));
    }

    private final class LoadableDetachableCitizenshipModel extends LoadableDetachableModel<Citizenship> {

        private Long id;

        public LoadableDetachableCitizenshipModel(Long id) {
            this.id = id;
        }

        @Override
        protected Citizenship load() {

            return citizenshipService.get(id);
        }
    }
}
