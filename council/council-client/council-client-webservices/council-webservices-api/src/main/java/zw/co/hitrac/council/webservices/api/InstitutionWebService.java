package zw.co.hitrac.council.webservices.api;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.webservices.api.domain.LeanInstitution;

/**
 *
 * @author Daniel Nkhoma
 */
@Path("/institutions")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public interface InstitutionWebService {

    @GET
    @Path("list")
    public List<Institution> getInstitutions();
    
    @GET
    @Path("registered")
    public List<Institution> getRegisteredInstitutions();

    @GET
    @Path("lean")
    public List<LeanInstitution> getLeanInstitution();

    @GET
    @Path(value = "institution/{id}")
    public Institution getInstitution(@PathParam(value = "id") Long id);
  
}
