/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.PenaltyType;
import zw.co.hitrac.council.business.service.PenaltyTypeService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author kelvin
 */
public class PenaltyTypeEditPage extends IAdministerDatabaseBasePage{
     @SpringBean
    private PenaltyTypeService penaltyTypeService;

    public PenaltyTypeEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<PenaltyType>(new PenaltyTypeEditPage.LoadableDetachablePenaltyTypeModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink",AdministerDatabasePage.class));
        Form<PenaltyType> form=new Form<PenaltyType>("form",(IModel<PenaltyType>)getDefaultModel()) {
            @Override
            public void onSubmit(){
                penaltyTypeService.save(getModelObject());
                setResponsePage(new PenaltyTypeViewPage(getModelObject().getId()));
            }
        };
        
        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior())); 
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink",PenaltyTypeListPage.class));
        add(form);
    }
    
    
    private final class LoadableDetachablePenaltyTypeModel extends LoadableDetachableModel<PenaltyType> {

        private Long id;

        public LoadableDetachablePenaltyTypeModel(Long id) {
            this.id = id;
        }

        @Override
        protected PenaltyType load() {
            if(id==null){
                return new PenaltyType();
            }
            return penaltyTypeService.get(id);
        }
    }
    
    
}
