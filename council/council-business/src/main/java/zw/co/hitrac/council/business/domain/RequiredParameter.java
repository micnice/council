package zw.co.hitrac.council.business.domain;

import javax.persistence.Entity;
import java.io.Serializable;

/**
 * Created by tndangana on 2/13/17.
 */
@Entity
public class RequiredParameter extends BaseIdEntity implements Serializable  {

    private boolean registrantBirthDate;
    private boolean  registrantIdNumber;

    public boolean isRegistrantBirthDate() {
        return registrantBirthDate;
    }

    public void setRegistrantBirthDate(boolean registrantBirthDate) {
        this.registrantBirthDate = registrantBirthDate;
    }

    public boolean isRegistrantIdNumber() {
        return registrantIdNumber;
    }

    public void setRegistrantIdNumber(boolean registrantIdNumber) {
        this.registrantIdNumber = registrantIdNumber;
    }
}
