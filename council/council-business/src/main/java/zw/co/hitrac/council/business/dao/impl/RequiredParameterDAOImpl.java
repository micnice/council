package zw.co.hitrac.council.business.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.RequiredParameterDAO;
import zw.co.hitrac.council.business.dao.RequirementDAO;
import zw.co.hitrac.council.business.dao.repo.RequiredParameterRepository;
import zw.co.hitrac.council.business.domain.RequiredParameter;
import zw.co.hitrac.council.business.domain.Requirement;

import java.util.List;

/**
 * Created by tndangana on 2/13/17.
 */
@Repository
public class RequiredParameterDAOImpl implements RequiredParameterDAO {
    @Autowired
    private RequiredParameterRepository  requiredParameterRepository;


    @Override
    public RequiredParameter save(RequiredParameter requiredParameter) {
        return requiredParameterRepository.save(requiredParameter);
    }

    @Override
    public List<RequiredParameter> findAll() {
        return requiredParameterRepository.findAll();
    }

    @Override
    public RequiredParameter get(Long id) {
        return requiredParameterRepository.findOne(id);
    }

    @Override
    public List<RequiredParameter> findAll(Boolean retired) {
        return null;
    }
}
