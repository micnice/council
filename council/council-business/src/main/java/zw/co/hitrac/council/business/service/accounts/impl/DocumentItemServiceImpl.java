
package zw.co.hitrac.council.business.service.accounts.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.accounts.DocumentItemDAO;
import zw.co.hitrac.council.business.domain.accounts.DocumentItem;
import zw.co.hitrac.council.business.service.accounts.DocumentItemService;

import java.util.List;

/**
 *
 * @author Tatenda Chiwandire
 */
@Service
@Transactional
public class DocumentItemServiceImpl implements DocumentItemService {

    @Autowired
    private DocumentItemDAO documentItemDAO;

    @Transactional
    public DocumentItem save(DocumentItem t) {
        return documentItemDAO.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<DocumentItem> findAll() {
        return documentItemDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public DocumentItem get(Long id) {
        return documentItemDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<DocumentItem> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setDocumentItemDao(DocumentItemDAO documentItemDAO) {

        this.documentItemDAO = documentItemDAO;
    }


}
