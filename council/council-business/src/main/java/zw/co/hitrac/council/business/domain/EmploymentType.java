package zw.co.hitrac.council.business.domain;

/**
 *
 * @author micnice
 */
public enum EmploymentType {

    PUBLIC("Public"),
    PRIVATE("Private"),
    INTERNATIONAL("International"),                
    NOTEMPLOYED("Not Employed");

    private EmploymentType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    private final String name;
}
