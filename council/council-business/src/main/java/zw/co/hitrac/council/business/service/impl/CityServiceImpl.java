package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.CityDAO;
import zw.co.hitrac.council.business.domain.City;
import zw.co.hitrac.council.business.domain.Country;
import zw.co.hitrac.council.business.service.CityService;

import java.util.List;

/**
 *
 * @author Charles Chigoriwa
 * @auther Michael Matiashe
 */
@Service
@Transactional
public class CityServiceImpl implements CityService {

    @Autowired
    private CityDAO cityDAO;

    @Transactional
    public City save(City city) {
        return cityDAO.save(city);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<City> findAll() {
        return cityDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public City get(Long id) {
        return cityDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<City> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

      public void setCityDAO(CityDAO cityDAO) {
        this.cityDAO = cityDAO;
      }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<City> getCities(Country country) {
        return cityDAO.getCities(country);
    }

   
}
