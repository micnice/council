package zw.co.hitrac.council.webservices.api.domain;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Daniel Nkhoma
 */
@XmlRootElement
public class LeanInstitution {

    private Long institutionId;

    public LeanInstitution() {
    }

    public LeanInstitution(Long institutionId) {
        this.institutionId = institutionId;
    }

    public Long getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(Long institutionId) {
        this.institutionId = institutionId;
    }
}
