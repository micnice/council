package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.DistrictDAO;
import zw.co.hitrac.council.business.dao.repo.DistrictRepository;
import zw.co.hitrac.council.business.domain.District;
import zw.co.hitrac.council.business.domain.Province;

/**
 *
 * @author Constance Mabaso
 */
@Repository
public class DistrictDAOImpl implements DistrictDAO {

    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    private DistrictRepository districtRepository;

    public District save(District district) {
        return districtRepository.save(district);
    }

    public List<District> findAll() {
        return districtRepository.findAll();
    }

    public District get(Long id) {
        return districtRepository.findOne(id);
    }

    public void setDistrictRepository(DistrictRepository districtRepository) {
        this.districtRepository = districtRepository;

    }

    public List<District> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<District> getDistrictsInProvince(Province province) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(District.class);
        criteria.add(Restrictions.eq("province", province));
        criteria.addOrder(Order.desc("name"));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }
}