package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import java.util.Arrays;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Conditions;
import zw.co.hitrac.council.business.domain.ProductIssuanceType;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.service.ConditionService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.institution.InstitutionEditPage;
import zw.co.hitrac.council.web.pages.registration.RegistrantConditionEditPage;

/**
 *
 * @author Michael Matiashe
 */
public class ConditionEditPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private ConditionService conditionService;

    public ConditionEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<Conditions>(new LoadableDetachableConditionModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));

        Form<Conditions> form = new Form<Conditions>("form", (IModel<Conditions>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                conditionService.save(getModelObject());
                setResponsePage(new ConditionViewPage(getModelObject().getId()));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextArea<String>("comments").add(new ErrorBehavior()));        
        form.add(new CheckBox("generalCondition"));
        form.add(new CheckBox("registerCondition"));        
        form.add(new CheckBox("examCondition"));
        form.add(new DropDownChoice("productIssuanceType", Arrays.asList(ProductIssuanceType.values())).setNullValid(true));
        form.add(new BookmarkablePageLink<Void>("returnLink", ConditionListPage.class));
        add(form);
    }

    public ConditionEditPage(Long id, final IModel<Registrant> registrantModel) {
        setDefaultModel(new CompoundPropertyModel<Conditions>(new LoadableDetachableConditionModel(id)));
        

        add(new Link<Void>("administerDatabaseLink") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantConditionEditPage(registrantModel, Boolean.FALSE));
            }

            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        Form<Conditions> form = new Form<Conditions>("form", (IModel<Conditions>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                conditionService.save(getModelObject());
                setResponsePage(new RegistrantConditionEditPage(registrantModel, Boolean.FALSE));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextArea<String>("comments").add(new ErrorBehavior()));        
        form.add(new CheckBox("generalCondition"));
        form.add(new CheckBox("registerCondition")); 
        form.add(new CheckBox("examCondition"));
        form.add(new DropDownChoice("productIssuanceType", Arrays.asList(ProductIssuanceType.values())).setNullValid(true));
        
        form.add(new Link<Void>("returnLink") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantConditionEditPage(registrantModel, Boolean.FALSE));
            }


        });
        add(form);
    }
    
    private final class LoadableDetachableConditionModel extends LoadableDetachableModel<Conditions> {
        private Long id;

        public LoadableDetachableConditionModel(Long id) {
            this.id = id;
        }

        @Override
        protected Conditions load() {
            if (id == null) {
                return new Conditions();
            }

            return conditionService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
