package zw.co.hitrac.council.business.service.accounts;

import java.util.Date;
import java.util.List;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.business.domain.accounts.CarryForwardLog;
import zw.co.hitrac.council.business.domain.accounts.Customer;

/**
 *
 * @author Charles Chigoriwa
 */
public interface CarryForwardLogService extends IGenericService<CarryForwardLog> {
   public List<CarryForwardLog> getRegistrantCarryForwardLogs(Customer customer);
   public List<CarryForwardLog> carryForwardLogs(Registrant registrant, Date startDate, Date endDate);
}
