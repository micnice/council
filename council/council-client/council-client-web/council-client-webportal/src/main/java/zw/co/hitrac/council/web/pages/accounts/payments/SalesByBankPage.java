package zw.co.hitrac.council.web.pages.accounts.payments;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Book;
import zw.co.hitrac.council.business.domain.accounts.ReceiptHeader;
import zw.co.hitrac.council.business.domain.accounts.TransactionType;
import zw.co.hitrac.council.business.domain.reports.BankSale;
import zw.co.hitrac.council.business.domain.reports.ProductSale;
import zw.co.hitrac.council.business.service.accounts.AccountService;
import zw.co.hitrac.council.business.service.accounts.PaymentDetailsService;
import zw.co.hitrac.council.business.service.accounts.ProductService;
import zw.co.hitrac.council.business.service.accounts.ReceiptHeaderService;
import zw.co.hitrac.council.business.service.accounts.ReceiptItemService;
import zw.co.hitrac.council.business.service.accounts.TransactionTypeService;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.reports.BankSalesReport;
import zw.co.hitrac.council.reports.ProductSaleBankReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.models.TransactionTypeListModel;
import zw.co.hitrac.council.web.pages.accounts.IAccountingPage;
import zw.co.hitrac.council.web.utility.DatePickerUtil;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 *
 * @author tdhlakama
 */
public class SalesByBankPage extends IAccountingPage {

    private Date startDate, endDate;
    private String searchText;
    @SpringBean
    private TransactionTypeService transactionTypeService;
    @SpringBean
    AccountService accountService;
    @SpringBean
    ReceiptHeaderService receiptHeaderService;
    @SpringBean
    ReceiptItemService receiptItemService;
    @SpringBean
    ProductService productService;
    @SpringBean
    PaymentDetailsService paymentDetailsService;

    public SalesByBankPage() {
        PropertyModel<Date> startDateModel = new PropertyModel<Date>(this, "startDate");
        PropertyModel<Date> endDateModel = new PropertyModel<Date>(this, "endDate");

        Form<?> form = new Form("form");
        form.add(new TextField<Date>("startDate", startDateModel).add(DatePickerUtil.getDatePicker()));
        form.add(new TextField<Date>("endDate", endDateModel).add(DatePickerUtil.getDatePicker()));
        add(form);
        add(new FeedbackPanel("feedback"));
        form.add(new Button("receiptHeaderReport") {
            @Override
            public void onSubmit() {
                try {
                    Set<BankSale> bankSales = new HashSet<BankSale>();
                    if (startDate != null && endDate == null) {
                        endDate = new Date();
                    }
                    for (TransactionType t : new TransactionTypeListModel(transactionTypeService, Book.CB).getObject()) {
                        BankSale b = new BankSale();
                        b.setTransactionType(t);
                        b.setEndDate(endDate);
                        b.setStartDate(startDate);
                        BigDecimal totalPoints = new BigDecimal(0);
                        for (ReceiptHeader r : receiptHeaderService.getReceiptHeaders(null, null, null, startDate, endDate, null, null, Boolean.FALSE)) {
                            if (t.equals(r.getTransactionType())) {
                                totalPoints = totalPoints.add(r.getTotalAmount());
                            }

                        }
                        b.setAmountPaid(totalPoints);
                        bankSales.add(b);
                    }
                    
                    BankSalesReport bankSalesReport = new BankSalesReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();
                    String commnet = "";
                    if (startDate != null && endDate != null) {
                        commnet = "Sales Done form From - " + DateUtil.getDate(startDate) + " to " + DateUtil.getDate(endDate);
                    }
                    parameters.put("comment", commnet);


                    ByteArrayResource resource = ReportResourceUtils.getReportResource(bankSalesReport,
                            contentType, bankSales, parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });


    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

}
