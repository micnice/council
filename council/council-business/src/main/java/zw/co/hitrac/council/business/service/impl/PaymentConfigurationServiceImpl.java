/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.PaymentConfigurationDAO;
import zw.co.hitrac.council.business.domain.PaymentConfiguration;
import zw.co.hitrac.council.business.service.PaymentConfigurationService;

import java.util.List;

/**
 *
 * @author tidza
 */
@Service
@Transactional
public class PaymentConfigurationServiceImpl implements PaymentConfigurationService {

    @Autowired
    private PaymentConfigurationDAO paymentConfigurationDao;

    @Transactional
    public PaymentConfiguration save(PaymentConfiguration t) {
        return paymentConfigurationDao.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<PaymentConfiguration> findAll() {
        return paymentConfigurationDao.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public PaymentConfiguration get(Long id) {
        return paymentConfigurationDao.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<PaymentConfiguration> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setPaymentConfigurationDAO(PaymentConfigurationDAO paymentConfigurationDAO) {

        this.paymentConfigurationDao = paymentConfigurationDAO;
    }
}
