package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.InstitutionPracticeControl;

/**
 *
 * @author Constance Mabaso
 */

    public interface InstitutionPracticeControlRepository extends CrudRepository<InstitutionPracticeControl, Long> {
    public List<InstitutionPracticeControl> findAll();
}


