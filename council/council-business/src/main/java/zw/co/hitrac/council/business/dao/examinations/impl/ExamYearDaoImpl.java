package zw.co.hitrac.council.business.dao.examinations.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.examinations.ExamYearDao;
import zw.co.hitrac.council.business.dao.repo.examinations.ExamYearRepository;
import zw.co.hitrac.council.business.domain.examinations.ExamYear;

/**
 *
 * @author tidza
 */
@Repository
public class ExamYearDaoImpl implements ExamYearDao {

    @Autowired
    private ExamYearRepository examYearRepository;

    public ExamYear save(ExamYear t) {
        return examYearRepository.save(t);
    }

    public List<ExamYear> findAll() {
        return examYearRepository.findAll();
    }

    public ExamYear get(Long id) {
        return examYearRepository.findOne(id);
    }

    public void setExamYearRepository(ExamYearRepository examYearRepository) {
        this.examYearRepository = examYearRepository;
    }

    public List<ExamYear> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
