package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 *
 * @author charlesc
 */
@Entity
@Table(name="addresstype")
@Audited
@XmlRootElement
public class AddressType extends  BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;
     
}
