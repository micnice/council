package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Service;
import zw.co.hitrac.council.business.service.ServiceService;

/**
 *
 * @author charlesc
 */
public class ServiceViewPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private ServiceService serviceService;

    public ServiceViewPage(Long id) {
        CompoundPropertyModel<Service> model =
            new CompoundPropertyModel<Service>(new LoadableDetachableServiceModel(id));

        setDefaultModel(model);
        add(new Link<Service>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new ServiceEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", ServiceListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("service", model.bind("name")));
    }

    private final class LoadableDetachableServiceModel extends LoadableDetachableModel<Service> {
        private Long id;

        public LoadableDetachableServiceModel(Long id) {
            this.id = id;
        }

        @Override
        protected Service load() {
            return serviceService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
