package zw.co.hitrac.council.business.process;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.domain.accounts.*;
import zw.co.hitrac.council.business.service.accounts.*;
import zw.co.hitrac.council.business.utils.CouncilException;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author Charles Chigoriwa
 * @author Michael Matiashe
 */
@Service
public class CreditNoteProcess {

    @Autowired
    private TransactionHeaderService transactionHeaderService;
    @Autowired
    private TransactionComponentService transactionComponentService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private DocumentService documentService;
    @Autowired
    private DocumentItemService documentItemService;
    @Autowired
    private DebtComponentService debtComponentService;
    @Autowired
    private AccountsParametersService accountsParametersService;
    @Autowired
    private CustomerService customerService;

    @Transactional
    public void processCreditNote(Customer customer, Document document, List<DebtComponent> debtComponents) {
        TransactionType creditNoteTransactionType = this.accountsParametersService.get().getCreditNoteTransactionType();
        saveCreditNoteDocument(customer, debtComponents);
        for (DebtComponent debtComponent : debtComponents) {
            customer = this.customerService.get(customer.getId());
            BigDecimal price = debtComponent.getRemainingBalance();
            //Just Value
            Account salesAccount = debtComponent.getProduct().getSalesAccount();
            processCommonCreditNoteOperations(creditNoteTransactionType, salesAccount, customer, price, debtComponent);
        }
    }

    private DebtComponent processCommonCreditNoteOperations(TransactionType creditNoteTransactionType, Account salesAccount, Customer customer, BigDecimal price, DebtComponent debtComponent) {

        //Save new balances
        saveNewBalances(creditNoteTransactionType, salesAccount, customer, price);

        //Post now to effect double entry
        post(customer, salesAccount, creditNoteTransactionType, price);
        debtComponent.setRemainingBalance(new BigDecimal("0"));
        debtComponent.setRetired(Boolean.TRUE);
        debtComponentService.save(debtComponent);

        return debtComponent;
    }

    private void post(Customer customerAccount, Account salesAccount, TransactionType creditNoteTransactionType, BigDecimal price) {

        Account drAccount = creditNoteTransactionType.getDrLedger();

        TransactionHeader transactionHeader = postTransactionHeader(creditNoteTransactionType);

        postTransactionComponent(transactionHeader, customerAccount.getAccount(), price, new Date());

        postTransactionComponent(transactionHeader, drAccount, price, new Date());

        TransactionComponent creditNoteTransactionComponent = postTransactionComponent(transactionHeader, salesAccount, price, new Date());

    }

    private TransactionHeader postTransactionHeader(TransactionType creditNoteTransactionType) {
        TransactionHeader transactionHeader = new TransactionHeader();
        transactionHeader.setDueDate(new Date());
        transactionHeader.setTransactionType(creditNoteTransactionType);
        transactionHeader.setDescription(creditNoteTransactionType.getDescription());
        transactionHeader = transactionHeaderService.save(transactionHeader);
        return transactionHeader;
    }

    private TransactionComponent postTransactionComponent(TransactionHeader transactionHeader, Account account, BigDecimal amount, Date dueDate) {
        TransactionComponent transactionComponent = new TransactionComponent();
        transactionComponent.setTransactionHeader(transactionHeader);
        transactionComponent.setDueDate(dueDate);
        transactionComponent.setAccount(account);
        transactionComponent.setAmount(amount);
        transactionComponentService.save(transactionComponent);
        return transactionComponent;
    }

    private void saveNewBalances(TransactionType creditNoteTransactionType, Account salesAccount, Customer customerAccount, BigDecimal amount) {
        //saveCustomerNewBalance(customerAccount, creditNoteTransactionType, amount);
        Account crAccount = accountService.get(creditNoteTransactionType.getCrLedger().getId());
        crAccount.credit(amount);
        accountService.save(crAccount);
        Account drAccount = accountService.get(creditNoteTransactionType.getDrLedger().getId());
        drAccount.debit(amount);
        accountService.save(drAccount);
        salesAccount.debit(amount);
        accountService.save(salesAccount);

    }

    private void saveCustomerNewBalance(Customer customer, TransactionType creditNoteTransactionType, BigDecimal price) {
        if (creditNoteTransactionType == null) {
            throw new CouncilException("Credit Note transaction type not set up !!!");
        }

        if (creditNoteTransactionType.getEffect().equals(Effect.CR)) {
            customer.getAccount().credit(price);
            accountService.save(customer.getAccount());
        } else {
            throw new CouncilException("Invalid effect on Credit Note transaction type");
        }

    }

    private void saveCreditNoteDocument(Customer customer, List<DebtComponent> debtComponents) {
        Document document = new Document();
        document.setCustomer(customer);
        document.setDate(new Date());
        document.setDocumentType(DocumentType.CREDIT_NOTE);

        for (DebtComponent debtComponent : debtComponents) {
            DocumentItem documentItem = new DocumentItem();
            documentItem.setAccount(debtComponent.getProduct().getSalesAccount());
            documentItem.setDocument(document);
            documentItem.setPrice(debtComponent.getRemainingBalance());
            documentItem.setProduct(debtComponent.getProduct());
            documentItem.setDebtComponent(debtComponent);
            documentItem.setQuantity(1);
            documentItemService.save(documentItem);
            document.getDocumentItems().add(documentItem);
        }
        documentService.save(document);
    }

}
