package zw.co.hitrac.council.web.pages.registration;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.validator.DateValidator;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.Title;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.business.utils.Gender;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.CustomDateTextField;
import zw.co.hitrac.council.web.utility.DatePickerUtil;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;

import java.util.ArrayList;
import java.util.List;

//~--- JDK imports ------------------------------------------------------------

/**
 * @author Takunda Dhlakama
 * @author Matiashe Michael
 */
public class RegistrantEditPage extends TemplatePage {

    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private MaritalStatusService maritalStatusService;
    @SpringBean
    private CitizenshipService citizenshipService;
    @SpringBean
    private TitleService titleService;
    @SpringBean
    private CityService cityService;
    @SpringBean
    private IdentificationTypeService identificationTypeService;
    @SpringBean
    private RegistrationProcess registrationProcess;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private RequiredParameterService requiredParameterService;
    private Title title;
    private Gender gender;

    public RegistrantEditPage() {

        this(null, Boolean.FALSE);
    }

    public RegistrantEditPage(Long id, final Boolean show) {

        menuPanelManager.getRegistrantionPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantionPanel().setRegistrantEditPageCurrent(true);
        Boolean mandatory = generalParametersService.get().getDirectRegistration();
        setDefaultModel(new CompoundPropertyModel<Registrant>(new LoadableDetachableRegistrantModel(id)));

        final Registrant object = ((IModel<Registrant>) getDefaultModel()).getObject();

        //Set up for existing registrant
        title = object.getTitle();
        gender = Gender.getGenderFromString(object.getGender());

        Form<Registrant> form = new Form<Registrant>("form", (IModel<Registrant>) getDefaultModel()) {

            @Override
            public void onSubmit() {

                try {

                    Registrant registrant = getModelObject();

                    if (requiredParameterService.get().isRegistrantBirthDate() && registrant.getBirthDate() == null) {
                        error("Enter Date of Birth");
                        return;
                    }
                    registrant.setTitle(title);
                    registrant.setGender(gender.toString());

                    //direct to upload page if id is null - direct to options page if edit
                    if (registrant.getId() != null) {
                        registrant = registrationProcess.saveRegistrant(registrant);
                        setResponsePage(new RegistrantViewPage(registrant.getId()));
                    } else {
                        registrant = registrationProcess.saveRegistrant(registrant);
                        setResponsePage(new RegistrantImageUploadPage(registrant.getId()));
                    }
                } catch (CouncilException e) {
                    error(e.getMessage());
                }
            }
        };

        DropDownChoice<Title> titles = new DropDownChoice<Title>("title", new PropertyModel<Title>(RegistrantEditPage.this, "title"), new RegistrantEditPage.TitlesModel(),
                new ChoiceRenderer<Title>("name", "id")) {

            @Override
            protected void onSelectionChanged(Title newSelection) {

                Registrant registrant = (Registrant) RegistrantEditPage.this.getDefaultModelObject();

                registrant.setGender(null);
            }

            @Override
            protected boolean wantOnSelectionChangedNotifications() {

                return true;
            }
        };

        titles.setRequired(true).add(new ErrorBehavior());
        form.add(titles);
        DropDownChoice<Gender> genders = new DropDownChoice<Gender>("gender",
                new PropertyModel<Gender>(RegistrantEditPage.this, "gender"), new RegistrantEditPage.GendersModel(),
                new ChoiceRenderer<Gender>("name"));
        genders.setRequired(true).add(new ErrorBehavior());
        form.add(genders);
        form.add(new DropDownChoice("maritalStatus", maritalStatusService.findAll()));
        form.add(new DropDownChoice("identificationType", identificationTypeService.findAll()));
        form.add(new DropDownChoice("citizenship", citizenshipService.findAll()).setRequired(!mandatory));
        form.add(new TextField<String>("firstname").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("lastname").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("middlename"));
        form.add(new TextField<String>("maidenname"));
        form.add(new TextField<String>("placeOfBirth"));
        form.add(new CustomDateTextField("birthDate").add(DatePickerUtil.getDatePicker()).add(new DateValidator(DateUtil.getDateFromAge(110), DateUtil.getDateFromAge(generalParametersService.get().getMinimumRegistrationAge()))));
        form.add(new TextField<String>("idNumber") {

            @Override
            public boolean isRequired() {
                return requiredParameterService.get().isRegistrantIdNumber();
            }
        });
        form.add(new TextField<String>("registrationNumber") {

            @Override
            protected void onConfigure() {

                if (show != null) {
                    setVisible(show);
                }
            }
        });

        form.add(new BookmarkablePageLink<Void>("registrantListPage", RegistrantListPage.class));
        //check Registration By Application - Else Default
        Boolean applicationRegistrationByApplication = Boolean.FALSE;
        if (generalParametersService.get().getRegistrationByApplication() != null) {
            applicationRegistrationByApplication = generalParametersService.get().getRegistrationByApplication();
        }
        final Boolean application = applicationRegistrationByApplication;
        add(form);
        add(new FeedbackPanel("feedback"));
    }

    private class GendersModel extends LoadableDetachableModel<List<? extends Gender>> {

        protected List<? extends Gender> load() {

            if (title != null) {
                return new ArrayList<Gender>(title.getGenders());
            } else {
                return new ArrayList<Gender>();
            }
        }
    }

    private final class TitlesModel extends LoadableDetachableModel<List<? extends Title>> {

        protected List<? extends Title> load() {

            return titleService.findAll();
        }
    }

    private final class LoadableDetachableRegistrantModel extends LoadableDetachableModel<Registrant> {

        private Long id;

        public LoadableDetachableRegistrantModel(Long id) {

            this.id = id;
        }

        @Override
        protected Registrant load() {

            Registrant registrant = null;

            if (id == null) {
                registrant = new Registrant();
            } else {
                registrant = registrantService.get(id);
            }

            return registrant;
        }
    }
}

//~ Formatted by Jindent --- http://www.jindent.com
