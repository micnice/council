package zw.co.hitrac.council.business.dao.accounts;

import java.util.List;
import zw.co.hitrac.council.business.dao.IGenericDAO;
import zw.co.hitrac.council.business.domain.Book;
import zw.co.hitrac.council.business.domain.accounts.PaymentMethod;
import zw.co.hitrac.council.business.domain.accounts.TransactionType;



/**
 **
 * @author Charles Chigoriwa
 * @author Michael Matiashe
 */
public interface TransactionTypeDAO extends IGenericDAO<TransactionType> {
    public List<TransactionType> find(Book book);
    public List<TransactionType> find(Book book, PaymentMethod paymentMethod);  
    public List<TransactionType> findBankAccounts();
}
