package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.MaritalStatus;
import zw.co.hitrac.council.business.service.MaritalStatusService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;

/**
 *
 * @author Edward Zengeni
 */
public class MaritalStatusListPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private MaritalStatusService maritalStatusService;

    public MaritalStatusListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new MaritalStatusEditPage(null));
            }
        });

        IModel<List<MaritalStatus>> model = new LoadableDetachableModel<List<MaritalStatus>>() {
            @Override
            protected List<MaritalStatus> load() {
                return maritalStatusService.findAll();
            }
        };
        PropertyListView<MaritalStatus> eachItem = new PropertyListView<MaritalStatus>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<MaritalStatus> item) {
                Link<MaritalStatus> viewLink = new Link<MaritalStatus>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new MaritalStatusViewPage(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };

        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
