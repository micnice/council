package zw.co.hitrac.council.web.pages.certification;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.ProductIssuance;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.service.ProductIssuanceService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.accounts.ReceiptHeaderService;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.registration.RegistrantPanel;
import zw.co.hitrac.council.web.pages.registration.RetireRegistrantPage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by edwin magodi
 */
public class CertificateDuplicatePage extends TemplatePage {

    @SpringBean
    private ReceiptHeaderService receiptHeaderService;
    @SpringBean
    private ProductIssuanceService productIssuanceService;
    @SpringBean
    private RegistrantService registrantService;

    public CertificateDuplicatePage(Long id) {
        menuPanelManager.getRegistrantionPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantionPanel().setRegistrantEditPageCurrent(true);
        final CompoundPropertyModel<Registrant> model  = new CompoundPropertyModel<Registrant>(new LoadableDetachableRegistrantModel(id));
       setDefaultModel(model);
        add(new RegistrantPanel("registrantPanel", id));

        IModel<List<ProductIssuance>> listModel = new LoadableDetachableModel<List<ProductIssuance>>() {
            @Override
            protected List<ProductIssuance> load() {

                    return productIssuanceService.getProductAllIssuances(model.getObject());

            }
        };

        add(new CertificateDuplicateListPanel("certificateDuplicateListPanel", listModel));

    }

    private final class LoadableDetachableRegistrantModel extends LoadableDetachableModel<Registrant> {

        private Long id;

        public LoadableDetachableRegistrantModel(Long id) {
            this.id = id;
        }

        @Override
        protected Registrant load() {
            Registrant registrant = null;

            if (id == null) {
                registrant = new Registrant();
            } else {
                registrant = registrantService.get(id);
            }
            return registrant;
        }
    }


}
