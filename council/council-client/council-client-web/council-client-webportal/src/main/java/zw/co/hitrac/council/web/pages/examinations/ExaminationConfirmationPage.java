
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------
import com.googlecode.wicket.jquery.ui.panel.JQueryFeedbackPanel;
import java.util.ArrayList;
import net.sf.jasperreports.engine.JRException;

import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationService;
import zw.co.hitrac.council.business.service.examinations.ExamSettingService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.reports.ConfirmationListReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

//~--- JDK imports ------------------------------------------------------------

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.web.models.CourseListModel;
import zw.co.hitrac.council.web.models.ExamSettingActiveListModel;

/**
 *
 * @author tdhlakama
 */
public class ExaminationConfirmationPage extends IExaminationsPage {

    @SpringBean
    private ExamRegistrationService examRegistrationService;
    @SpringBean
    private ExamSettingService examSettingService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private CourseService courseService;
    private Institution institution;
    private ExamSetting examSetting;
    private String candidateNumber;
    private String idNumber;
    private Course course;
    private HrisComparator hrisComparator = new HrisComparator();

    public ExaminationConfirmationPage() {
        final FeedbackPanel feedback = new JQueryFeedbackPanel("feedback");
        add(feedback.setOutputMarkupId(true));
        PropertyModel<Institution> institutionModel = new PropertyModel<Institution>(this, "institution");
        PropertyModel<ExamSetting> examSettingModel = new PropertyModel<ExamSetting>(this, "examSetting");
        PropertyModel<Course> courseModel = new PropertyModel<Course>(this, "course");
        Form<?> form = new Form("form") {
            @Override
            protected void onSubmit() {
                try {
                    ConfirmationListReport confirmationListReport = new ConfirmationListReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();

                    if (course.getQualification() != null) {
                        parameters.put("course", course.getQualification().getName());
                    }
                    if (examSetting != null) {
                        parameters.put("examSetting", examSetting.toString());
                    }

                    if (institution != null) {
                        parameters.put("institution", institution.toString());
                    }

                    ByteArrayResource resource;
                    List<ExamRegistration> examRegistrations = (List<ExamRegistration>) hrisComparator.sortExamRegistrationBySurname(examRegistrationService.getAllExamCandidates(examSetting, course, institution, candidateNumber, idNumber, null,Boolean.FALSE,null,Boolean.FALSE,null));
                   
                        try {
                            resource = ReportResourceUtils.getReportResource(confirmationListReport, contentType, examRegistrations, parameters);

                            IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                    RequestCycle.get().getResponse(), null);

                            resource.respond(a);
                        } catch (JRException ex) {
                            Logger.getLogger(ExaminationConfirmationPage.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        // To make Wicket stop processing form after sending response
                        RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                    
                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };

        form.add(new DropDownChoice("institution", institutionModel, new InstitutionsModel()).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("examSetting", examSettingModel, new ExamSettingActiveListModel(examSettingService)).setRequired(true).add(new ErrorBehavior()));

        form.add(new DropDownChoice("course", courseModel, new CourseListModel(courseService), new ChoiceRenderer<Course>()) {
            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }

            @Override
            public boolean isRequired() {
                return true;
            }
        }.add(new ErrorBehavior()));
        add(form);
    }

    private class InstitutionsModel extends LoadableDetachableModel<List<? extends Institution>> {

        protected List<? extends Institution> load() {
            if (course != null) {
                return (List<Institution>) hrisComparator.sort((new ArrayList<Institution>(courseService.get(course.getId()).getTrainingInstitutions())));
            } else {
                return new ArrayList<Institution>();
            }
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
