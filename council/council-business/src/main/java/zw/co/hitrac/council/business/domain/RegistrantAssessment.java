/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author kelvin
 */
@Entity
@Table(name = "registrantassessment")
@Audited
public class RegistrantAssessment extends BaseIdEntity implements Serializable {

    private Integer valueObtained;
    private AssessmentType assessmentType;
    private Registrant registrant;
    private String comments;
    private String assessor;
    private Date date;
    private Institution institution;
    private Application application;
    public static String DECISIONAPPROVED = "DECISION APPROVED";
    public static String DECISIONDECLINED = "DECISION DECLINED";
    public static String DECISIONPENDING = "DESICION PENDING";
    private String decisionStatus = DECISIONPENDING;

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getAssessor() {
        return assessor;
    }

    public void setAssessor(String assessor) {
        this.assessor = assessor;
    }

    @ManyToOne
    public Registrant getRegistrant() {
        return registrant;
    }

    public void setRegistrant(Registrant registrant) {
        this.registrant = registrant;
    }

    public Integer getValueObtained() {
        if (valueObtained == null) {
            return valueObtained = 0;
        }
        return valueObtained;
    }

    public void setValueObtained(Integer valueObtained) {
        this.valueObtained = valueObtained;
    }

    @ManyToOne
    public AssessmentType getAssessmentType() {
        return assessmentType;
    }

    public void setAssessmentType(AssessmentType assessmentType) {
        this.assessmentType = assessmentType;
    }

    @ManyToOne
    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    @ManyToOne
    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    public String getDecisionStatus() {
        return decisionStatus;
    }

    public void setDecisionStatus(String decisionStatus) {
        this.decisionStatus = decisionStatus;
    }
}
