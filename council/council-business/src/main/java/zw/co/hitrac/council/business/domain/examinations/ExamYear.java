/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain.examinations;

import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.domain.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 *
 * @author tdhlakama
 */
@Entity
@Table(name="examyear")
@Audited
public class ExamYear extends BaseEntity implements Serializable {

}
