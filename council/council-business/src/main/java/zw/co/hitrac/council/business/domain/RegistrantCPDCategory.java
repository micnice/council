package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Constance Mabaso
 */
@Entity
@Table(name = "registrantcpdcategory")
@Audited
public class RegistrantCPDCategory extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    private Set<RegistrantCPDItem> registrantCPDItems = new HashSet<RegistrantCPDItem>();
    private BigDecimal maximumPoints =BigDecimal.ZERO;

    @OneToMany(mappedBy = "registrantCPDCategory")
    public Set<RegistrantCPDItem> getRegistrantCPDItems() {
        return registrantCPDItems;
    }

    public void setRegistrantCPDItems(Set<RegistrantCPDItem> registrantCPDItems) {
        this.registrantCPDItems = registrantCPDItems;
    }

    public BigDecimal getMaximumPoints() {
        return maximumPoints;
    }

    public void setMaximumPoints(BigDecimal maximumPoints) {
        this.maximumPoints = maximumPoints;
    }

}
