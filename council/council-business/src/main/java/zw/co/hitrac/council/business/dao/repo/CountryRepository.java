package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.Country;

/**
 *
 * @author Charles Chigoriwa
 */
public interface CountryRepository extends CrudRepository<Country, Long> {

    public List<Country> findAll();
    
}
