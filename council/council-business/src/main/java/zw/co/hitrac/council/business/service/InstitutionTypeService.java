/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.InstitutionType;

/**
 *
 * @author Kelvin Goredema
 */
public interface InstitutionTypeService extends IGenericService<InstitutionType> {
}
