package zw.co.hitrac.council.web.models;

import java.util.List;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.service.RegistrationService;

/**
 *
 * @author Matiashe Michael
 */
public class RegistrationListModel extends LoadableDetachableModel<List<Registration>> {
    
    private final RegistrationService registrationService;

    public RegistrationListModel(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @Override
    protected List<Registration> load() {
      return this.registrationService.findAll();
    }
    
}
