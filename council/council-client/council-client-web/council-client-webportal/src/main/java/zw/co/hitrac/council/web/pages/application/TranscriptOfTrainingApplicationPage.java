package zw.co.hitrac.council.web.pages.application;

//~--- non-JDK imports -------------------------------------------------------
import org.apache.wicket.extensions.yui.calendar.DatePicker;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.ApplicationPurpose;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.process.ApplicationProcess;
import zw.co.hitrac.council.business.service.ApplicationService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.CustomDateTextField;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

//~--- JDK imports ------------------------------------------------------------

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.UserService;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.models.CourseListModel;
import zw.co.hitrac.council.web.pages.accounts.documents.DebtComponentsPage;
import zw.co.hitrac.council.web.pages.registration.RegistrantViewPage;

/**
 *
 * @author Michael Matiashe
 * @author Kelvin Goredema
 */
public class TranscriptOfTrainingApplicationPage extends TemplatePage {

    @SpringBean
    private ApplicationProcess applicationProcess;
    @SpringBean
    private ApplicationService applicationService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private UserService userService;
    @SpringBean
    private CourseService courseService;
    Boolean show = Boolean.FALSE;

    public TranscriptOfTrainingApplicationPage(final IModel<Registrant> registrantModel) {
        menuPanelManager.getApplicationPanel().setTopMenuCurrent(true);

        show = generalParametersService.get().getRegistrationByApplication();
        CompoundPropertyModel<Application> model = new CompoundPropertyModel<Application>(new Application());

        setDefaultModel(model);
        model.getObject().setRegistrant(registrantModel.getObject());
        model.getObject().setApplicationPurpose(ApplicationPurpose.TRANSCRIPT_OF_TRAINING);
        model.getObject().setApplicationDate(new Date());

        Form<Application> form = new Form<Application>("form", (IModel<Application>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    getModelObject().setCreatedBy(CouncilSession.get().getUser());
                    applicationProcess.apply(getModelObject());
                    //Select items to pay
                    setResponsePage(new DebtComponentsPage(registrantModel.getObject().getId()));
                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };
        form.add(new Label("currentStatus", model.getObject().getApplicationTextStatus()));
        form.add(new Label("approvedByLabel", "Decision By") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });

        form.add(new DropDownChoice("approvedBy", userService.getAuthorisers()) {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new Label("processedByLabel", "Prossesed By") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });

        form.add(new CustomDateTextField("dateProcessed") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new DropDownChoice("processedBy", userService.getApplicationpProcessors()) {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new Label("dateProcessedLabel", "Final Processing Date") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new Label("dateApprovedLabel", "Decision Date") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });

        form.add(new CustomDateTextField("dateApproved") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });

        form.add(new DropDownChoice("applicationStatus", Application.getApplicationStates) {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        
        form.add(new DropDownChoice("course",
                new CourseListModel(courseService)).setRequired(true).add(new ErrorBehavior()));
        form.add(new Label("applicationPurpose", model.getObject().getApplicationPurpose()));
        form.add(
                new CustomDateTextField("applicationDate").setRequired(true).add(new ErrorBehavior()).add(new DatePicker()));
        form.add(new Label("commentLabel", "Comment") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new TextArea("comment") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new Label("applicationStatusLabel", "Application Decision") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {

                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });
        add(form);
        add(new Label("registrant.fullname"));
        add(new FeedbackPanel("feedback"));
    }

    public TranscriptOfTrainingApplicationPage(long id) {
        menuPanelManager.getApplicationPanel().setTopMenuCurrent(true);

        if (!generalParametersService.get().getRegistrationByApplication()) {
            show = Boolean.FALSE;
        } else {
            show = Boolean.TRUE;
        }
        final CompoundPropertyModel<Application> model =
                new CompoundPropertyModel<Application>(applicationService.get(id));

        setDefaultModel(model);



        Form<Application> form = new Form<Application>("form", (IModel<Application>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {

                    Application application = applicationProcess.approve(getModelObject());

                    // check if not Application for Change of name
                    if ((application.getApplicationPurpose().equals(ApplicationPurpose.CHANGE_OF_NAME))
                            && application.getApplicationStatus().equals(Application.APPLICATIONAPPROVED)) {
                        setResponsePage(new RegistrantViewPage(model.getObject().getRegistrant().getId()));
                    }

                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };

        form.add(new CustomDateTextField("dateProcessed"));
        form.add(new Label("dateProcessedLabel", "Final Processing Date"));
        form.add(new Label("currentStatus", model.getObject().getApplicationTextStatus()));
        form.add(new Label("course", model.getObject().getCourse()));
        form.add(new Label("approvedByLabel", "Approved By"));
        form.add(new DropDownChoice("approvedBy", userService.getAuthorisers()));
        form.add(new Label("processedByLabel", "Prossesed By"));
        form.add(new DropDownChoice("processedBy", userService.getApplicationpProcessors()));
        form.add(new Label("dateApprovedLabel", "Decision Date"));
        form.add(new CustomDateTextField("dateApproved").add(
                DatePickerUtil.getDatePicker()));
        final List<String> applicationStates = Arrays.asList(new String[]{Application.APPLICATIONPENDING, Application.APPLICATIONAPPROVED,
            Application.APPLICATIONDECLINED, Application.APPLICATIONUNDERREVIEW});
        form.add(new DropDownChoice("applicationStatus", applicationStates).setRequired(true).add(new ErrorBehavior()));
        form.add(new Label("applicationPurpose", model.getObject().getApplicationPurpose()));
        form.add(new Label("applicationStatusLabel", "Application Decision"));
        form.add(new CustomDateTextField("applicationDate").setRequired(true).add(new ErrorBehavior()).add(new DatePicker()).setEnabled(Boolean.FALSE));
        form.add(new TextArea("comment"));
          form.add(new Label("commentLabel", "Comment") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.TRUE);
            }
        });
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(model.getObject().getRegistrant().getId()));
            }
        });
        add(form);
        add(new Label("registrant.fullname"));
        add(new FeedbackPanel("feedback"));
    }
}

//~ Formatted by Jindent --- http://www.jindent.com
