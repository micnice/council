package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.ejb.HibernateEntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.ConditionDAO;
import zw.co.hitrac.council.business.dao.repo.ConditionRepository;
import zw.co.hitrac.council.business.domain.Conditions;
import zw.co.hitrac.council.business.domain.RegistrantActivity;

/**
 *
 * @author Michael Matiashe
 */
@Repository
public class ConditionDAOImpl implements ConditionDAO {

    @Autowired
    private ConditionRepository conditionRepository;
    @PersistenceContext
    EntityManager entityManager;

    public Conditions save(Conditions condition) {
        return conditionRepository.save(condition);
    }

    public List<Conditions> findAll() {
        return conditionRepository.findAll();
    }

    public Conditions get(Long id) {
        return conditionRepository.findOne(id);
    }

    /**
     * A setter method that will make mocking repo object easier
     *
     * @param conditionRepository
     */
    public void setConditionRepository(ConditionRepository conditionRepository) {
        this.conditionRepository = conditionRepository;
    }

    public List<Conditions> findAll(Boolean examCondition) {
       return entityManager.createQuery("Select distinct c from Conditions c where c.examCondition=:examCondition")
               .setParameter("examCondition", examCondition)
                .getResultList();
    }

    public List<Conditions> getRegisterConditions() {
        return entityManager.createQuery("Select distinct c from Conditions c where c.registerCondition = true ").getResultList();
    }
}
