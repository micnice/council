/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.utils.CaseUtil;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author tidza
 */
@Entity
@Table(name = "employment")
@Audited
@XmlRootElement
public class Employment extends BaseIdEntity implements Serializable {

    private Date startDate;
    private Date endDate;
    private Registrant registrant;
    private Post post;
    private EmploymentType employmentType;
    private Institution institution;
    private EmploymentStatus employmentStatus;
    private EmploymentArea employmentArea;
    private String employer;
    private Boolean active = Boolean.FALSE;
    private City city;

    public String getEmployer() {
        return CaseUtil.upperCase(employer);
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    @ManyToOne
    public EmploymentStatus getEmploymentStatus() {
        return employmentStatus;
    }

    public void setEmploymentStatus(EmploymentStatus employmentStatus) {
        this.employmentStatus = employmentStatus;
    }

    @Enumerated(EnumType.STRING)
    public EmploymentType getEmploymentType() {
        return employmentType;
    }

    public void setEmploymentType(EmploymentType employmentType) {
        this.employmentType = employmentType;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @ManyToOne
    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    @ManyToOne
    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    @ManyToOne
    public Registrant getRegistrant() {
        return registrant;
    }

    public void setRegistrant(Registrant registrant) {
        this.registrant = registrant;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Boolean getActive() {
        if (active == null) {
            return Boolean.FALSE;
        }
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @ManyToOne
    public EmploymentArea getEmploymentArea() {
        return employmentArea;
    }

    public void setEmploymentArea(EmploymentArea employmentArea) {
        this.employmentArea = employmentArea;
    }

    @ManyToOne
    public City getCity() {
        return this.city;
    }

    public void setCity(City city) {
        this.city = city;
    }

}
