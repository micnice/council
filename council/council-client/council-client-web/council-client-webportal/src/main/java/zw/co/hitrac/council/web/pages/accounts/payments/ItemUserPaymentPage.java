/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.accounts.payments;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.User;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.domain.accounts.TransactionType;
import zw.co.hitrac.council.business.domain.reports.ProductItem;
import zw.co.hitrac.council.business.service.UserService;
import zw.co.hitrac.council.business.service.accounts.AccountsParametersService;
import zw.co.hitrac.council.business.service.accounts.PaymentDetailsService;
import zw.co.hitrac.council.business.service.accounts.ProductService;
import zw.co.hitrac.council.business.service.accounts.ReceiptHeaderService;
import zw.co.hitrac.council.business.service.accounts.ReceiptItemService;
import zw.co.hitrac.council.business.service.accounts.TransactionTypeService;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.reports.ProductItemReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.DatePickerUtil;
import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 *
 * @author tdhlakama
 */
public class ItemUserPaymentPage extends TemplatePage {

    private Date startDate, endDate;
    private User user;
    @SpringBean
    private ProductService productService;
    @SpringBean
    private PaymentDetailsService paymentDetailsService;
    @SpringBean
    private ReceiptItemService receiptItemService;
    @SpringBean
    private UserService userService;
    @SpringBean
    private TransactionTypeService transactionTypeService;
    @SpringBean
    private AccountsParametersService accountsParametersService;
    private HrisComparator hrisComparator = new HrisComparator();
    @SpringBean
    private ReceiptHeaderService receiptHeaderService;

    public ItemUserPaymentPage() {
        PropertyModel<Date> startDateModel = new PropertyModel<Date>(this, "startDate");
        PropertyModel<Date> endDateModel = new PropertyModel<Date>(this, "endDate");
        PropertyModel<String> userModel = new PropertyModel<String>(this, "user");
        Form<?> form = new Form("form");
        form.add(new TextField<Date>("startDate", startDateModel).add(DatePickerUtil.getDatePicker()));
        form.add(new TextField<Date>("endDate", endDateModel).add(DatePickerUtil.getDatePicker()));
        form.add(new DropDownChoice("user", userModel, userService.findAll()));

        add(form);
        add(new FeedbackPanel("feedback"));
        form.add(new Button("print") {
            @Override
            public void onSubmit() {
                try {
                    Set<ProductItem> productSales = new HashSet<ProductItem>();
                    Set<TransactionType> bankAccounts = new HashSet<TransactionType>();
                    bankAccounts.addAll(transactionTypeService.findBankAccounts());
                    TransactionType manualTransactionType = accountsParametersService.get().getManualTransactionType();
                    if (manualTransactionType != null) {
                        bankAccounts.remove(manualTransactionType);
                    }
                    if (startDate != null && endDate == null) {
                        endDate = new Date();
                    }
                    for (Product p : receiptItemService.getDistinctProductsInSales()) {
                        ProductItem productItem = new ProductItem();
                        productItem.setProduct(p);
                        productItem.setEndDate(endDate);
                        productItem.setStartDate(startDate);
                        BigDecimal totalPoints = new BigDecimal(0);
                        for (TransactionType tt : bankAccounts) {
                            totalPoints = totalPoints.add(receiptItemService.getCalcualtedReceiptTotal(p, startDate, endDate, tt, user));
                        }
                        productItem.setAmountPaid(totalPoints);
                        productSales.add(productItem);
                    }
                    ProductItemReport productItemReport = new ProductItemReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();
                    //BigDecimal carryForwad = receiptHeaderService.getCarryForwardBalance(startDate, endDate, null, null, Boolean.FALSE);
                    //parameters.put("carryForward",  carryForwad.toString() + " *CF");
                    String commnet = "";
                    if (startDate != null && endDate != null) {
                        commnet = "Done By - " + user.getUsername() + " - Sales Done form From - " + DateUtil.getDate(startDate) + " to " + DateUtil.getDate(endDate);
                    }
                    
                    Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK)));
                    if (enabled) {                      
                        commnet = commnet +". Total Number of Receipts - (" + receiptHeaderService.getNumberOfReceipts(endDate, user) + ") Done." ;
                    }
                    
                    parameters.put("comment", commnet);
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(productItemReport,
                            contentType, hrisComparator.sortProductSale(new ArrayList<ProductItem>(productSales)), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
