/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain.examinations;

import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.domain.BaseIdEntity;
import zw.co.hitrac.council.business.domain.ModulePaper;
import zw.co.hitrac.council.business.domain.Registrant;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author clive
 */
@Entity
@Table(name = "oldexamresult") //used only for those who passed through the system
@Audited
public class OldExamResult extends BaseIdEntity implements Serializable {

    private ModulePaper modulePaper;
    private Date dateOfResult;
    private Date dateOfExam;
    private Registrant registrant;

    @ManyToOne(fetch = FetchType.LAZY)
    public ModulePaper getModulePaper() {
        return modulePaper;
    }

    public void setModulePaper(ModulePaper modulePaper) {
        this.modulePaper = modulePaper;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDateOfResult() {
        return dateOfResult;
    }

    public void setDateOfResult(Date dateOfResult) {
        this.dateOfResult = dateOfResult;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDateOfExam() {
        return dateOfExam;
    }

    public void setDateOfExam(Date dateOfExam) {
        this.dateOfExam = dateOfExam;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Registrant getRegistrant() {
        return registrant;
    }

    public void setRegistrant(Registrant registrant) {
        this.registrant = registrant;
    }
}
