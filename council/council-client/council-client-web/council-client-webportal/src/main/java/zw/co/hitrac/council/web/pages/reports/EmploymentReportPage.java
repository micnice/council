package zw.co.hitrac.council.web.pages.reports;

//~--- non-JDK imports --------------------------------------------------------
import java.sql.Connection;
import java.sql.SQLException;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.web.pages.TemplatePage;

//~--- JDK imports ------------------------------------------------------------
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import zw.co.hitrac.council.business.domain.City;
import zw.co.hitrac.council.business.domain.District;
import zw.co.hitrac.council.business.domain.EmploymentArea;
import zw.co.hitrac.council.business.domain.EmploymentStatus;
import zw.co.hitrac.council.business.domain.EmploymentType;
import zw.co.hitrac.council.business.domain.Post;
import zw.co.hitrac.council.business.domain.Province;
import zw.co.hitrac.council.business.service.CityService;
import zw.co.hitrac.council.business.service.DistrictService;
import zw.co.hitrac.council.business.service.EmploymentAreaService;
import zw.co.hitrac.council.business.service.EmploymentService;
import zw.co.hitrac.council.business.service.EmploymentStatusService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.PostService;
import zw.co.hitrac.council.business.service.ProvinceService;
import zw.co.hitrac.council.business.utils.StringFormattingUtil;
import zw.co.hitrac.council.reports.RegistrantDataDBReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.models.DistrictListModel;
import zw.co.hitrac.council.web.models.InstitutionListModel;
import zw.co.hitrac.council.web.models.ProvinceListModel;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 * @author takunda Dhlakama
 */
public class EmploymentReportPage extends TemplatePage {

    private Date startDate, endDate;
    private Post post;
    private EmploymentType employmentType;
    private Province province;
    private District district;
    private Institution institution;
    private EmploymentStatus employmentStatus;
    private EmploymentArea employmentArea;
    private City city;
    private Boolean active;
    @SpringBean
    private EmploymentStatusService employmentStatusService;
    @SpringBean
    private EmploymentAreaService employmentAreaService;
    @SpringBean
    private PostService postService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private EmploymentService employmentService;
    @SpringBean
    private ProvinceService provinceService;
    @SpringBean
    private DistrictService districtService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private CityService cityService;
    private ContentType contentType;
    private String searchTerm;
    @SpringBean
    private DataSource dataSource;

    public EmploymentReportPage() {
        menuPanelManager.getSearchPanel().setTopMenuCurrent(true);
        menuPanelManager.getSearchPanel().setSearchAdvanced(true);

        PropertyModel<ContentType> contentTypeModel = new PropertyModel<ContentType>(this, "contentType");
        PropertyModel<Post> postModel = new PropertyModel<Post>(this, "post");
        PropertyModel<EmploymentType> employmentTypeModel = new PropertyModel<EmploymentType>(this, "employmentType");
        PropertyModel<EmploymentStatus> employmentStatusModel = new PropertyModel<EmploymentStatus>(this, "employmentStatus");
        PropertyModel<EmploymentArea> employmentAreaModel = new PropertyModel<EmploymentArea>(this, "employmentArea");
        PropertyModel<Institution> institutionModel = new PropertyModel<Institution>(this, "institution");
        PropertyModel<City> cityModel = new PropertyModel<City>(this, "city");
        PropertyModel<District> districtModel = new PropertyModel<District>(this, "district");
        PropertyModel<Province> provinceModel = new PropertyModel<Province>(this, "province");
        PropertyModel<String> searchTermModel = new PropertyModel<String>(this, "searchTerm");

        Form<?> form = new Form("form");

        form.add(new DropDownChoice("contentType", contentTypeModel, Arrays.asList(ContentType.values())));
        form.add(new DropDownChoice("employmentType", employmentTypeModel, Arrays.asList(EmploymentType.values())));
        form.add(new DropDownChoice("district", districtModel, new DistrictListModel(districtService, null)));
        form.add(new DropDownChoice("province", provinceModel, new ProvinceListModel(provinceService)));
        form.add(new DropDownChoice("city", cityModel, cityService.findAll()));
        form.add(new TextField<String>("searchTerm", searchTermModel));

        form.add(new Label("postLabel", "Post") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else if (!generalParametersService.get().getRegistrationByApplication()) {
                    setVisible(Boolean.FALSE);
                } else {
                    setVisible(Boolean.TRUE);
                }
            }
        });
        form.add(new DropDownChoice("post", postModel, postService.findAll()) {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else if (!generalParametersService.get().getRegistrationByApplication()) {
                    setVisible(Boolean.FALSE);
                } else {
                    setVisible(Boolean.TRUE);
                }
            }
        });

        form.add(new Label("employmentAreaLabel", "EmploymentArea") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else if (!generalParametersService.get().getRegistrationByApplication()) {
                    setVisible(Boolean.FALSE);
                } else {
                    setVisible(Boolean.TRUE);
                }
            }
        });
        form.add(new DropDownChoice("employmentArea", employmentAreaModel, employmentAreaService.findAll()) {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else if (!generalParametersService.get().getRegistrationByApplication()) {
                    setVisible(Boolean.FALSE);
                } else {
                    setVisible(Boolean.TRUE);
                }
            }
        });

        form.add(new Label("employmentStatusLabel", "Employment Status") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else if (!generalParametersService.get().getRegistrationByApplication()) {
                    setVisible(Boolean.FALSE);
                } else {
                    setVisible(Boolean.TRUE);
                }
            }
        });
        form.add(new DropDownChoice("employmentStatus", employmentStatusModel, employmentStatusService.findAll()) {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.TRUE);
                } else if (!generalParametersService.get().getRegistrationByApplication()) {
                    setVisible(Boolean.FALSE);
                } else {
                    setVisible(Boolean.TRUE);
                }
            }
        });

        form.add(new Label("institutionLabel", "Institution") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.FALSE);
                }
            }
        });
        form.add(new DropDownChoice("institution", institutionModel, new InstitutionListModel(institutionService)) {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                    setVisible(Boolean.FALSE);
                }
            }

        });

        add(form);

        form.add(new Button("process") {
            @Override
            public void onSubmit() {
                printDetailReport(employmentService.getEmployments(searchTerm, province, district, employmentType, employmentArea, institution, employmentStatus, city, post, startDate, endDate, active));
            }
        });

        add(new FeedbackPanel("feedback"));

    }

    private void printDetailReport(List<RegistrantData> registrants) {

        Map parameters = new HashMap();
        RegistrantDataDBReport registrantDataDBReport = new RegistrantDataDBReport();
        try {

            String list = "";
            for (RegistrantData d : registrants) {
                list += d.getId().toString() + ",";
            }
            if (!list.isEmpty()) {
                list = list.substring(0, list.length() - 1);
            }

            String postLabel = post != null ? "* Post : " + post.getName() : "";
            String area = employmentArea != null ? "* Employment Area : " + employmentArea.getName() : "";
            String type = employmentType != null ? "* Employment Type : " + employmentType.getName() : "";
            String cityLabel = city != null ? "* City : " + city.getName() : "";
            String districtLabel = district != null ? "* District : " + district.getName() : "";
            String employmentStatusLabel = employmentStatus != null ? "* Employment Status : " + employmentStatus.getName() : "";
            String provinceLabel = province != null ? "* Province : " + province.getName() : "";
            String instituionLabel = institution != null ? "* Institution : " + institution.getName() : "";

            final String reportTitle = StringFormattingUtil.join(postLabel, area, type,
                    cityLabel, districtLabel, employmentStatusLabel, provinceLabel, instituionLabel);

            parameters.put("comment", reportTitle);

            parameters.put("list", list);
            final Connection connection = dataSource.getConnection(); //DBConnect.getConnection();
            ByteArrayResource resource = null;
            try {
                resource = ReportResourceUtils.getReportResource(registrantDataDBReport, contentType, connection, parameters);
            } finally {
                connection.close();
            }
            IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                    RequestCycle.get().getResponse(), null);

            resource.respond(a);

            // To make Wicket stop processing form after sending response
            RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
        } catch (JRException ex) {
            ex.printStackTrace(System.out);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        }

    }
}



//~ Formatted by Jindent --- http://www.jindent.com
