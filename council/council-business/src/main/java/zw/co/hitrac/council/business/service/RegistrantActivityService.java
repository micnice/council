package zw.co.hitrac.council.business.service;

import java.util.Date;
import java.util.List;
import zw.co.hitrac.council.business.domain.Conditions;
import zw.co.hitrac.council.business.domain.ContactType;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.domain.EmploymentType;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Qualification;
import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantActivity;
import zw.co.hitrac.council.business.domain.RegistrantActivityType;
import zw.co.hitrac.council.business.domain.RegistrantContact;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;

/**
 *
 * @author Kelvin Goredema
 */
public interface RegistrantActivityService extends IGenericService<RegistrantActivity> {

     void remove(RegistrantActivity registrantActivity);

     List<RegistrantActivity> getRegistrantActivities(Registrant registrant);

     RegistrantActivity getLastRenewalActivity(Registrant registrant);

     boolean checkRegistrantActivity(Registrant registrant, RegistrantActivityType registrantActivityType, Duration duration);

     List<RegistrantActivity> getRegistrantActivities(Registrant registrant, RegistrantActivityType registrantActivityType);

     List<CouncilDuration> getRenewalPeriodsNotRenewed(Registrant registrant, RegistrantActivityType registrantActivityType);

     RegistrantActivity getRegistrantActivityByDuration(Registrant registrant, Duration duration);

     List<RegistrantData> getRegistrant(RegistrantActivityType registrantActivityType, Date startDate, Date endDate);

     Integer getTotalPerCourseAndDurationAndRegisterAndRegistrantActivity(Course course, Register register, List<Duration> durations, RegistrantActivityType registrantActivityType, Boolean active);

     Integer getTotalPerQualificationAndDurationAndRegisterAndRegistrantActivity(Qualification qualification, Register register, Duration duration, RegistrantActivityType registrantActivityType);

     Integer getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(Course course, Register register, List<Duration> durations, RegistrantActivityType registrantActivityType, Boolean active, Conditions conditions);

     List<RegistrantData> getListPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(Course course, Register register, List<Duration> durations, RegistrantActivityType registrantActivityType, Boolean active, Enum<EmploymentType> e, Conditions condition);

     Integer getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(Course course, Register register, List<Duration> durations, RegistrantActivityType registrantActivityType, Boolean active, Enum<EmploymentType> e, Conditions condition);

     List<RegistrantContact> getEmailAddressesPerQualificationMappedCourse(Course course, Register register, ContactType contactType);

     List<RegistrantData> getListPerCourseAndDurationAndRegisterAndRegistrantActivity(Course course, Register register, List<Duration> durations, RegistrantActivityType registrantActivityType, Boolean active);

     List<RegistrantData> getListPerQualificationAndDurationAndRegisterAndRegistrantActivity(Qualification qualification, Register register, Duration duration, RegistrantActivityType registrantActivityType);

     List<RegistrantActivity> getRegistrantsByRegistrantActivityType(RegistrantActivityType registrantActivityType);

     CouncilDuration getLastCouncilDurationRenewal(Registrant registrant);

     RegistrantActivity getRegistrantActivity(RegistrantActivityType registrantActivityType, Registrant registrant);

     List<CouncilDuration> getRegisteredRenewalPeriods(String registrationNumber);

     List<Registrant> getRegistrantsRenewedInCouncilDurationNotInListOfCouncilDurations(CouncilDuration councilDuration, List<CouncilDuration> councilDurations);

     List<Registrant> getRegistrantsRenewedInCouncilDuration(CouncilDuration councilDuration, Course course);

     List<Registrant> getRegistrantsWhoNeverRenewed();

     String getRegistrantActivityComment(Registrant registrant, RegistrantActivityType registrantActivityType);
}
