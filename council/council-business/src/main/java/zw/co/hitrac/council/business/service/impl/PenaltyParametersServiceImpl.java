package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.PenaltyParametersDAO;
import zw.co.hitrac.council.business.domain.PenaltyParameters;
import zw.co.hitrac.council.business.service.PenaltyParametersService;

import java.util.List;

/**
 *
 * @author Michael Matiashe
 */
@Service
@Transactional
public class PenaltyParametersServiceImpl implements PenaltyParametersService {

    @Autowired
    private PenaltyParametersDAO penaltyParametersDAO;

    @Transactional
    public PenaltyParameters save(PenaltyParameters penaltyParameters) {
        List<PenaltyParameters> list=findAll();
        if (penaltyParameters.getId() == null && !list.isEmpty()) {
            throw new IllegalStateException("Only one row is allowed for parameters!!!!!");
        }else if(penaltyParameters.getId()!=null && get(penaltyParameters.getId())==null){
            throw new IllegalStateException("Row does not exist");
        }else if(list.size()>=2){
            throw new IllegalStateException("Only one row is allowed for parameters!!!!!");
        }
        return penaltyParametersDAO.save(penaltyParameters);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<PenaltyParameters> findAll() {
        return penaltyParametersDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public PenaltyParameters get(Long id) {
        return penaltyParametersDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<PenaltyParameters> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
     public PenaltyParameters get() {
        List<PenaltyParameters> parameters = penaltyParametersDAO.findAll();
        if (parameters.isEmpty()) {
            return new PenaltyParameters();
        } else if (parameters.size()== 1) {
            return parameters.get(0);
        } else {
            throw new IllegalStateException("Only one row is allowed for parameters!!!!!");
        }
    }

    public void setPenaltyParametersDAO(PenaltyParametersDAO penaltyParametersDAO) {
        this.penaltyParametersDAO = penaltyParametersDAO;
    }
}
