package zw.co.hitrac.council.web.pages.registrar;

import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.EmailMessage;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.EmailMessageService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrantContactService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author Takunda Dhlakama
 */
public class EmailMessageListPage extends TemplatePage {
    
    @SpringBean
    private EmailMessageService emailMessageService;
    @SpringBean
    private RegistrantContactService registrantContactService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    
    public EmailMessageListPage() {
        
        IModel<List<EmailMessage>> model = new LoadableDetachableModel<List<EmailMessage>>() {
            
            @Override
            protected List<EmailMessage> load() {
                return emailMessageService.findAll();
            }
        };
        
        PropertyListView<EmailMessage> eachItem = new PropertyListView<EmailMessage>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<EmailMessage> item) {
                Link<EmailMessage> viewLink = new Link<EmailMessage>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new EmailMessageEditPage(getModelObject().getId()));
                    }
                };
                Link<EmailMessage> link = new Link<EmailMessage>("sendEmailLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new SendEmailPage(getModelObject()));
                    }
                };
                Link<EmailMessage> sendAll = new Link<EmailMessage>("sendToAll", item.getModel()) {
                    @Override
                    public void onClick() {
                        try {
                            String bodyMessage = String.format(getModelObject().getParagraphOne() + " " + getModelObject().getParagraphTwo() + "\n" + " " + generalParametersService.get().getEmailSignature());
                            //TODO: Set Directly to Registrant Data not Registrant Contact
                            List<RegistrantData> list = registrantContactService.getRegistrantDataAddress(generalParametersService.get().getEmailContactType());
                            for (RegistrantData data : list) {
                                data.setEmailAddress(data.getContactDetail());
                            }
                            emailMessageService.send(list, getModelObject().getSubject(), bodyMessage);
                            info("Emails Sent Successfully");
                        } catch (Exception e) {
                           error("Emails not sent -  Try Again");
                        }
                    }
                };
                item.add(viewLink);
                item.add(sendAll);
                item.add(link);
                viewLink.add(new Label("subject"));
                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
            }
        };
        
        add(eachItem);
    }
    
}
