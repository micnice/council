package zw.co.hitrac.council.business.dao.repo.accounts;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.accounts.Customer;

/**
 *
 * @author Takunda Dhlakama
 * @author Michael Matiashe
 * @author Judge Muzinda
 */
public interface CustomerRepository extends CrudRepository<Customer, Long> {

    public List<Customer> findAll();
}
