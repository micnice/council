package zw.co.hitrac.council.business.domain;

import org.apache.commons.lang.BooleanUtils;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author tdhlakama
 */
@Entity
@Table(name = "webuser")
@XmlRootElement
public class WebUser implements Serializable {

    private Long id;
    private String username; //set email adddress
    private String password;
    private String registrantNumber;
    private Boolean retired = Boolean.FALSE;

    public WebUser() {
    }

    public WebUser(Long id, String username, String password) {
        this.id = id;
        this.username = username;        
        this.password = password;
    }
    
    @XmlElement
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    @XmlElement
    @Column(unique = true)
    public String getRegistrantNumber() {
        return registrantNumber;
    }

    public void setRegistrantNumber(String registrantNumber) {
        this.registrantNumber = registrantNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return getUsername();
    }
    
    @XmlElement
    @Column(unique = true)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @XmlElement
    public Boolean getRetired() {
        return retired;
    }

    public void setRetired(Boolean retired) {
        this.retired = retired;
    }
    
}
