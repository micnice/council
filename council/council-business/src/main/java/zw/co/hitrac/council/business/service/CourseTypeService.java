package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.CourseType;

/**
 *
 * @author Charles Chigoriwa
 */
public interface CourseTypeService extends IGenericService<CourseType> {
    
}
