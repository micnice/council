/*
 * @author Michael Matiashe
 */
package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "qualification")
@Audited
@XmlRootElement
public class Qualification extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    private Set<Course> courses = new HashSet<Course>();
    private QualificationType qualificationType;
    private String prefixName;
    private RegisterType registerType;
    private String isoCode;
    private Boolean pbq = Boolean.FALSE;
    private Set<Qualification> preReq = new HashSet<Qualification>();
    private ProductIssuanceType productIssuanceType;
    private Course course;

    @OneToMany(fetch = FetchType.LAZY)
    public Set<Qualification> getPreReq() {
        return preReq;
    }

    public void setPreReq(Set<Qualification> preReq) {
        this.preReq = preReq;
    }

    public Boolean getPbq() {
        if (pbq == null) {
            return Boolean.FALSE;
        }
        return pbq;
    }

    public void setPbq(Boolean pbq) {
        this.pbq = pbq;
    }

    @Transient
    public String getPbqTextStatus() {
        return pbq ? "YES" : "NO";
    }

    public String getPrefixName() {
        return prefixName;
    }

    public void setPrefixName(String prefixName) {
        this.prefixName = prefixName;
    }

    public Qualification() {
    }

    @OneToMany(fetch = FetchType.LAZY)
    public Set<Course> getCourses() {
        return courses;
    }

    public void setCourses(Set<Course> courses) {
        this.courses = courses;
    }

    public QualificationType getQualificationType() {
        return qualificationType;
    }

    public void setQualificationType(QualificationType qualificationType) {
        this.qualificationType = qualificationType;
    }

    @ManyToOne
    public RegisterType getRegisterType() {
        return registerType;
    }

    public void setRegisterType(RegisterType registerType) {
        this.registerType = registerType;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

    @Enumerated(EnumType.STRING)
    public ProductIssuanceType getProductIssuanceType() {
        return productIssuanceType;
    }

    public void setProductIssuanceType(ProductIssuanceType productIssuanceType) {
        this.productIssuanceType = productIssuanceType;
    }

    @ManyToOne
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
}
