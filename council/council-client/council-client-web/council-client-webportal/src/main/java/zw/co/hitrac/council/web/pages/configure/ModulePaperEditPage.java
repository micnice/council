package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.ModulePaper;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.ModulePaperService;
import zw.co.hitrac.council.business.service.accounts.ProductService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Matiashe Michael
 */
public class ModulePaperEditPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private ModulePaperService  modulePaperService;
    @SpringBean
    private CourseService courseService;
    @SpringBean
    private ProductService      productService;

    public ModulePaperEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<ModulePaper>(new LoadableDetachableModulePaperModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));

        Form<ModulePaper> form = new Form<ModulePaper>("form", (IModel<ModulePaper>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                modulePaperService.save(getModelObject());
                setResponsePage(new ModulePaperViewPage(getModelObject().getId()));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new TextField<String>("position").setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("course", courseService.findAll()));
        form.add(new DropDownChoice("product", productService.findAll()));
        form.add(new BookmarkablePageLink<Void>("returnLink", ModulePaperListPage.class));
        add(form);
    }

    private final class LoadableDetachableModulePaperModel extends LoadableDetachableModel<ModulePaper> {
        private Long id;

        public LoadableDetachableModulePaperModel(Long id) {
            this.id = id;
        }

        @Override
        protected ModulePaper load() {
            if (id == null) {
                return new ModulePaper();
            }

            return modulePaperService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
