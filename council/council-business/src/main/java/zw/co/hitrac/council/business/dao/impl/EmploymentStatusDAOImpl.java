/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.EmploymentStatusDAO;
import zw.co.hitrac.council.business.dao.repo.EmploymentStatusRepository;
import zw.co.hitrac.council.business.domain.EmploymentStatus;

/**
 *
 * @author kelvin
 */
@Repository
public class EmploymentStatusDAOImpl implements EmploymentStatusDAO{
@Autowired
   private EmploymentStatusRepository employmentStatusRepository;
    
   public EmploymentStatus save(EmploymentStatus employmentStatus){
       return employmentStatusRepository.save(employmentStatus);
   }
   
   public List<EmploymentStatus> findAll(){
       return employmentStatusRepository.findAll();
   }
   
   public EmploymentStatus get(Long id){
       return employmentStatusRepository.findOne(id);
   }
 /**
     * A setter method that will make mocking repo object easier
     * @param employmentStatusRepository 
     */
   
   public void setEmploymentStatusRepository(EmploymentStatusRepository employmentStatusRepository) {
         this.employmentStatusRepository = employmentStatusRepository;
    }

    public List<EmploymentStatus> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}