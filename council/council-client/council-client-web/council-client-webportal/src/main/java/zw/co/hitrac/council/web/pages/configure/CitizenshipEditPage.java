/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.configure;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Citizenship;
import zw.co.hitrac.council.business.service.CitizenshipService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;


public class CitizenshipEditPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private CitizenshipService citizenshipService;

    public CitizenshipEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<Citizenship>(new zw.co.hitrac.council.web.pages.configure.CitizenshipEditPage.LoadableDetachableCitizenshipModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        Form<Citizenship> form = new Form<Citizenship>("form", (IModel<Citizenship>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                citizenshipService.save(getModelObject());
                setResponsePage(new CitizenshipViewPage(getModelObject().getId()));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink", CitizenshipListPage.class));
        add(form);
    }

    private final class LoadableDetachableCitizenshipModel extends LoadableDetachableModel<Citizenship> {

        private Long id;

        public LoadableDetachableCitizenshipModel(Long id) {
            this.id = id;
        }

        @Override
        protected Citizenship load() {
            if (id == null) {
                return new Citizenship();
            }
            return citizenshipService.get(id);
        }
    }
}
