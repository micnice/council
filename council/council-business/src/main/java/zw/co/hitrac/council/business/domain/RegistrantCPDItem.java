package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author Constance Mabaso
 */
@Entity
@Table(name="registrantcpditem")
@Audited
public class RegistrantCPDItem extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    private RegistrantCPDCategory registrantCPDCategory;
    private BigDecimal points;

    @ManyToOne
    public RegistrantCPDCategory getRegistrantCPDCategory() {
        return registrantCPDCategory;
    }

    public void setRegistrantCPDCategory(RegistrantCPDCategory registrantCPDCategory) {
        this.registrantCPDCategory = registrantCPDCategory;
    }

    public BigDecimal getPoints() {
        return points;
    }

    public void setPoints(BigDecimal points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return getName() + " " + getPoints() + " Points";
     }
}
