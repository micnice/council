package zw.co.hitrac.council.web.pages.security;

//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;

/**
 *
 * @author Clive Gurure
 */
public interface AuthenticatedWebPage extends Serializable {}


//~ Formatted by Jindent --- http://www.jindent.com
