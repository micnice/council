package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.CourseTypeDAO;
import zw.co.hitrac.council.business.dao.repo.CourseTypeRepository;
import zw.co.hitrac.council.business.domain.CourseType;

/**
 *
 * @author Charles Chigoriwa
 */
@Repository
public class CourseTypeDAOImpl implements CourseTypeDAO {

    @Autowired
    private CourseTypeRepository courseTypeRepository;

    public CourseType save(CourseType courseType) {
        return courseTypeRepository.save(courseType);
    }

    public List<CourseType> findAll() {
        return courseTypeRepository.findAll();
    }

    public CourseType get(Long id) {
        return courseTypeRepository.findOne(id);
    }

    public void setCourseTypeRepository(CourseTypeRepository courseTypeRepository) {
        this.courseTypeRepository = courseTypeRepository;
    }

    public List<CourseType> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
  
}
