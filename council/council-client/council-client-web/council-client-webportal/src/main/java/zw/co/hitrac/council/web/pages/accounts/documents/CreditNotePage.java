package zw.co.hitrac.council.web.pages.accounts.documents;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Check;
import org.apache.wicket.markup.html.form.CheckGroup;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.TypeOfService;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.domain.accounts.Document;
import zw.co.hitrac.council.business.process.CreditNoteProcess;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.service.accounts.AccountService;
import zw.co.hitrac.council.business.service.accounts.CustomerService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.accounts.IAccountingPage;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;
import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;

/**
 *
 * @author Charles Chigoriwa
 * @author Michael Matiashe
 */
public class CreditNotePage extends IAccountingPage {

    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    AccountService accountService;
    @SpringBean
    CustomerService customerAccountService;
    @SpringBean
    RegistrationService registrationService;
    @SpringBean
    DebtComponentService debtComponentService;
    @SpringBean
    private CreditNoteProcess creditNoteProcess;
    private String comment;

    private List<DebtComponent> selectedDebtComponents = new ArrayList<DebtComponent>();

    public CreditNotePage(PageParameters parameters) {
        this(parameters.get("registrantId").toLong());
    }

    public CreditNotePage(Long id) {

        final CompoundPropertyModel<Registrant> registrantModel = new CompoundPropertyModel<Registrant>(new DetachableRegistrantModel(id, registrantService));
        setDefaultModel(registrantModel);
        PropertyModel<String> commentModel = new PropertyModel<String>(this, "comment");
        add(new Label("fullname"));

        IModel<List<DebtComponent>> debtComponentsModel = new LoadableDetachableModel<List<DebtComponent>>() {

            @Override
            protected List<DebtComponent> load() {
                return debtComponentService.getDebtComponents(registrantModel.getObject().getCustomerAccount());
            }
        };

        Form<Void> form = new Form<Void>("form") {

            @Override
            protected void onSubmit() {
                if (!selectedDebtComponents.isEmpty()) {
                    Document document = new Document();
                    for (DebtComponent db : selectedDebtComponents) {
                        db.setComment(comment);
                        db.setModifiedBy(CouncilSession.get().getUser());
                        db.setDateModified(new Date());
                    }
                    if (CouncilSession.get().getUser() != null) {
                        if (getDiscilinaryItems(selectedDebtComponents) && CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.FINANCIAL_ADMIN_EXECUTIVE, Role.SYSTEM_ADMINISTRATOR, Role.ACCOUNTS_OFFICER)))) {
                            creditNoteProcess.processCreditNote(registrantModel.getObject().getCustomerAccount(), document, selectedDebtComponents);
                        } else {
                            if (!getDiscilinaryItems(selectedDebtComponents)) {
                                creditNoteProcess.processCreditNote(registrantModel.getObject().getCustomerAccount(), document, selectedDebtComponents);
                            }
                        }
                    }
                    setResponsePage(new ViewRemovedNotesPage(registrantModel.getObject().getId()));
                }
            }

        };

        form.add(new TextArea<String>("comment", commentModel) {
            @Override
            protected void onConfigure() {
                setVisibilityAllowed(CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.ACCOUNTS_OFFICER))));
            }

            @Override
            public boolean isRequired() {
                return true;
            }
        }.add(new ErrorBehavior()));

        CheckGroup<DebtComponent> selectedDebtComponentsGroup = new CheckGroup<DebtComponent>("selectedDebtComponents", this.selectedDebtComponents);
        form.add(selectedDebtComponentsGroup);
        add(form);

        PropertyListView<DebtComponent> eachItem = new PropertyListView<DebtComponent>("eachItem", debtComponentsModel) {
            @Override
            protected void populateItem(final ListItem<DebtComponent> item) {

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(new Check("selected", item.getModel()));
                item.add(new Label("product.name"));
                item.add(new Label("remainingBalance"));
            }
        };

        selectedDebtComponentsGroup.add(eachItem);

        form.add(new Link<Registrant>("registrantPaymentPage") {
            @Override
            public void onClick() {
                setResponsePage(new DebtComponentsPage(registrantModel.getObject().getId()));
            }
        });

    }

    public Boolean getDiscilinaryItems(List<DebtComponent> debtComponents) {
        for (DebtComponent db : selectedDebtComponents) {
            if (db.getProduct().getTypeOfService().equals(TypeOfService.DISCIPLINARY_FEES)) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }
}
