package zw.co.hitrac.council.web.pages.registration;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.RegistrantTransfer;
import zw.co.hitrac.council.business.process.TransferProcess;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.TransferRuleService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.TransferRuleListModel;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.CustomDateTextField;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

//~--- JDK imports ------------------------------------------------------------

import java.util.Date;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.ApplicationService;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.web.models.CourseListModel;
import zw.co.hitrac.council.web.pages.certification.CertificationPage;

/**
 *
 * @author Michael Matiashe
 */
public class TransferPage extends TemplatePage {

    @SpringBean
    private TransferRuleService transferRuleService;
    @SpringBean
    GeneralParametersService generalParametersService;
    @SpringBean
    DebtComponentService debtComponentService;
    @SpringBean
    RegistrantService registrantService;
    @SpringBean
    private TransferProcess transferProcess;
    @SpringBean
    private CourseService courseService;
    @SpringBean
    private ApplicationService applicationService;
    @SpringBean
    private RegistrationProcess registrationProcess;

    public TransferPage(final Application application) {
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(application.getRegistrant().getId());
        CompoundPropertyModel<RegistrantTransfer> model = new CompoundPropertyModel<RegistrantTransfer>(new RegistrantTransfer());
        setDefaultModel(model);
        model.getObject().setRegistrant(application.getRegistrant());
        model.getObject().setTransferDate(new Date());
        model.getObject().setTransferRule(application.getTransferRule());
        model.getObject().setApplication(application);
        Registration r = registrationProcess.activeRegisterNotStudentRegister(application.getRegistrant());
        if (application.getTransferRule().getToCourse() != null){
        model.getObject().setCourse(application.getTransferRule().getToCourse());
        } else if (r != null){
            model.getObject().setCourse(r.getCourse());
        }
        Form<RegistrantTransfer> form = new Form<RegistrantTransfer>("form",
                (IModel<RegistrantTransfer>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    transferProcess.transfer(getModelObject());
                    Application application1 = application;
                    application1.setUsed(Boolean.TRUE);
                    applicationService.save(application1);
                    setResponsePage(new CertificationPage(getModelObject().getRegistrant().getId()));
                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };

        form.add(
                new DropDownChoice("transferRule", new TransferRuleListModel(transferRuleService)).setEnabled(Boolean.FALSE));
        form.add(
                new DropDownChoice("course", new CourseListModel(courseService)).setRequired(true).add(
                new ErrorBehavior()));
        form.add(
                new CustomDateTextField("transferDate").setRequired(true).add(new ErrorBehavior()).add(
                DatePickerUtil.getDatePicker()));
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(application.getRegistrant().getId()));
            }
        });
        add(form);
        add(new Label("registrant.fullname"));
        add(new FeedbackPanel("feedback"));
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
