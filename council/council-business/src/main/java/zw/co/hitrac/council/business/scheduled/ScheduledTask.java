package zw.co.hitrac.council.business.scheduled;

/**
 * Created by clive on 7/11/15.
 */
public interface ScheduledTask {

    void execute();
}
