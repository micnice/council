package zw.co.hitrac.council.web.utility;

import org.apache.wicket.datetime.DateConverter;
import org.apache.wicket.datetime.PatternDateConverter;

import java.io.Serializable;
import java.util.Locale;

/**
 * @author Charles Chigoriwa
 */
public class DateTimeUtils implements Serializable {

    private static String US_DATE_PATTERN = "MM/dd/yyyy";
    private static String UK_DATE_PATTERN = "dd/MM/yyyy";
    private static final String UK_DATE_TIME_PATTERN = "dd/MM/yyyy hh:mm a";
    private static String RECEIPT_DATE_PATTEN = "dd-MMM-yyyy";
    private static final DateConverter councilDateTimeConverter = new PatternDateConverter(UK_DATE_TIME_PATTERN, true);

    /**
     * Get a DateConverter using the default locale, careful when changing this
     * because it affects system-wide display of dates
     *
     * @return a DateConverter instance
     */
    public static DateConverter getConverter() {

        return new PatternDateConverter(UK_DATE_PATTERN, true);
    }

    /**
     * Specific Request - Receipt Pattern
     *
     * @return a DateConverter instance
     */
    public static DateConverter getReceiptConverter() {

        return new PatternDateConverter(RECEIPT_DATE_PATTEN, true);
    }

    /**
     * Get a DateConverter using the supplied locale
     *
     * @return a DateConverter instance
     */
    public static DateConverter getConverter(Locale locale) {

        if (Locale.US.equals(locale)) {

            return new PatternDateConverter(US_DATE_PATTERN, true);

        } else if (Locale.UK.equals(locale)) {

            return new PatternDateConverter(UK_DATE_PATTERN, true);
        } else {

            return new PatternDateConverter(UK_DATE_PATTERN, true);
        }
    }

    public static DateConverter getDateTimeConverter() {

        return councilDateTimeConverter;
    }
}
