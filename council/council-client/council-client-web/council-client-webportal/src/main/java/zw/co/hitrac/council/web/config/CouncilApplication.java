package zw.co.hitrac.council.web.config;

import org.apache.wicket.Page;
import org.apache.wicket.authroles.authentication.AbstractAuthenticatedWebSession;
import org.apache.wicket.authroles.authentication.AuthenticatedWebApplication;
import org.apache.wicket.authroles.authorization.strategies.role.RoleAuthorizationStrategy;
import org.apache.wicket.authroles.authorization.strategies.role.metadata.MetaDataRoleAuthorizationStrategy;
import org.apache.wicket.devutils.stateless.StatelessChecker;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.settings.def.ExceptionSettings;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.web.pages.HomePage;
import zw.co.hitrac.council.web.pages.accounts.AccountingPage;
import zw.co.hitrac.council.web.pages.accounts.documents.CreditNotePage;
import zw.co.hitrac.council.web.pages.accounts.documents.InvoiceAmountPage;
import zw.co.hitrac.council.web.pages.accounts.documents.InvoicePage;
import zw.co.hitrac.council.web.pages.accounts.payments.FinancePage;
import zw.co.hitrac.council.web.pages.accounts.payments.PaymentPage;
import zw.co.hitrac.council.web.pages.application.ApplicationListPage;
import zw.co.hitrac.council.web.pages.configure.AdministerDatabasePage;
import zw.co.hitrac.council.web.pages.examinations.ExaminationsPage;
import zw.co.hitrac.council.web.pages.registrar.RegistrarPage;
import zw.co.hitrac.council.web.pages.search.SearchRegistrantPage;
import zw.co.hitrac.council.web.pages.security.LoginPage;
import zw.co.hitrac.council.web.pages.security.UserRolesAuthorizationStrategy;

import static zw.co.hitrac.council.business.utils.Role.*;
import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;

/**
 *
 * @author Charles Chigoriwa
 */
public class CouncilApplication extends AuthenticatedWebApplication {

    @Override
    public Class<? extends Page> getHomePage() {
        return HomePage.class;
    }

    @Override
    protected void init() {
        super.init();
        this.getComponentInstantiationListeners().add(new SpringComponentInjector(this));
        mountPage("home", HomePage.class);
        mountPage("login", LoginPage.class);
        mountPage("searchSimplePage", SearchRegistrantPage.class);
        mountPage("financePage", FinancePage.class);
        mountPage("registrarLink", RegistrarPage.class);
        mountPage("applications", ApplicationListPage.class);
        getSecuritySettings().setAuthorizationStrategy(new RoleAuthorizationStrategy(new UserRolesAuthorizationStrategy()));

        //By default production mode does not show error pages with stack traces, disable this behaviour
        getExceptionSettings().setUnexpectedExceptionDisplay(ExceptionSettings.SHOW_EXCEPTION_PAGE);

        MetaDataRoleAuthorizationStrategy.authorize(HomePage.class, getRoles(Role.values()));
        MetaDataRoleAuthorizationStrategy.authorize(AdministerDatabasePage.class, getRoles(SYSTEM_ADMINISTRATOR, Role.IT_OFFICER));
        MetaDataRoleAuthorizationStrategy.authorize(RegistrarPage.class, getRoles(SYSTEM_ADMINISTRATOR, Role.IT_OFFICER, Role.REGISTRY_CLERK));
        MetaDataRoleAuthorizationStrategy.authorize(AccountingPage.class, getRoles(SYSTEM_ADMINISTRATOR, ACCOUNTS_OFFICER, Role.IT_OFFICER));
        MetaDataRoleAuthorizationStrategy.authorize(FinancePage.class, getRoles(SYSTEM_ADMINISTRATOR, ACCOUNTS_OFFICER, Role.IT_OFFICER));
        MetaDataRoleAuthorizationStrategy.authorize(ExaminationsPage.class, getRoles(SYSTEM_ADMINISTRATOR, ACCOUNTS_OFFICER, EDUCATION_OFFICER, Role.IT_OFFICER));
        MetaDataRoleAuthorizationStrategy.authorize(InvoicePage.class, getRoles(SYSTEM_ADMINISTRATOR, REGISTRY_CLERK, Role.IT_OFFICER));
        MetaDataRoleAuthorizationStrategy.authorize(InvoiceAmountPage.class, getRoles(SYSTEM_ADMINISTRATOR, REGISTRY_CLERK, Role.IT_OFFICER));
        MetaDataRoleAuthorizationStrategy.authorize(CreditNotePage.class, getRoles(SYSTEM_ADMINISTRATOR, REGISTRY_CLERK, Role.IT_OFFICER));
        MetaDataRoleAuthorizationStrategy.authorize(PaymentPage.class, getRoles(SYSTEM_ADMINISTRATOR, REGISTRY_CLERK, Role.IT_OFFICER));
        this.getMarkupSettings().setStripWicketTags(true); //IMPORTANT!
        getComponentPreOnBeforeRenderListeners().add(new StatelessChecker());
    }

    @Override
    protected Class<? extends AbstractAuthenticatedWebSession> getWebSessionClass() {
        return CouncilSession.class;
    }

    @Override
    protected Class<? extends WebPage> getSignInPageClass() {
        return LoginPage.class;
    }
}
