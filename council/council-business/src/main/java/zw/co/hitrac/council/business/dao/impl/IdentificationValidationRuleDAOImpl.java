package zw.co.hitrac.council.business.dao.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.IdentificationValidationRuleDAO;
import zw.co.hitrac.council.business.dao.repo.IdentificationValidationRuleRepository;
import zw.co.hitrac.council.business.domain.Citizenship;
import zw.co.hitrac.council.business.domain.IdentificationType;
import zw.co.hitrac.council.business.domain.IdentificationValidationRule;
import zw.co.hitrac.council.business.utils.CouncilException;

import java.util.List;

/**
 * Created by clive on 6/16/15.
 */

@Repository
public class IdentificationValidationRuleDAOImpl implements IdentificationValidationRuleDAO {

    @Autowired
    private IdentificationValidationRuleRepository ruleRepository;

    @Override
    public IdentificationValidationRule save(IdentificationValidationRule identificationValidationRule) {

        if(StringUtils.isBlank(identificationValidationRule.getExample())){

            throw new CouncilException("Cannot save validation rule without a validation expression");

        }else if(identificationValidationRule.getCitizenship() == null
                && identificationValidationRule.getIdentificationType() == null) {

            throw new CouncilException("Cannot save validation rule without both a citizenship and an identification type");

        }else {

            return ruleRepository.save(identificationValidationRule);
        }
    }

    @Override
    public List<IdentificationValidationRule> findAll() {

        return (List<IdentificationValidationRule>) ruleRepository.findAll();
    }

    @Override
    public IdentificationValidationRule get(Long id) {

        return ruleRepository.findOne(id);
    }

    @Override
    public List<IdentificationValidationRule> findAll(Boolean retired) {

        return (List<IdentificationValidationRule>) ruleRepository.findByRetired(retired);
    }

    public List<IdentificationValidationRule> findByCitizenshipAndIdentificationType(Citizenship citizenship,
                                                                               IdentificationType identificationType){

        return ruleRepository.findByCitizenshipAndIdentificationTypeAndActiveTrue(citizenship, identificationType);
    }
}
