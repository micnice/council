package zw.co.hitrac.council.business.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.InstitutionManagerDAO;
import zw.co.hitrac.council.business.dao.repo.InstitutionManagerRepository;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.InstitutionManager;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.InstitutionService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

/**
 * @author tidza
 * @author ezinzombe
 */
@Repository
public class InstitutionManagerDAOImpl implements InstitutionManagerDAO {

    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private InstitutionManagerRepository institutionManagerRepository;
    @Autowired
    GeneralParametersService generalParametersService;
    @Autowired
    private InstitutionService institutionService;

    @Override
    public List<InstitutionManager> findInstitutionManagers(Registrant registrant, Institution institution) {

        if (registrant != null) {
            return findByRegistrant(registrant);
        } else if (institution != null) {
            return findByInstitution(institution);
        } else
            return new ArrayList<>();
    }



    private List<InstitutionManager> findByRegistrant(Registrant registrant) {
        return entityManager.createQuery("SELECT i FROM InstitutionManager i WHERE i.registrant =:registrant")
                .setParameter("registrant", registrant)
                .getResultList();
    }

    private List<InstitutionManager> findByInstitution(Institution institution) {
        return entityManager.createQuery("SELECT i FROM InstitutionManager i WHERE i.institution =:institution")
                .setParameter("institution", institution)
                .getResultList();
    }

    @Override
    public InstitutionManager save(InstitutionManager institutionManager) {
        return (InstitutionManager) institutionManagerRepository.save(institutionManager);
    }

    @Override
    public List<InstitutionManager> findAll() {

        return (List<InstitutionManager>) institutionManagerRepository.findAll();
    }

    @Override
    public InstitutionManager get(Long id) {
        return institutionManagerRepository.findOne(id);
    }

    @Override
    public List<InstitutionManager> findAll(Boolean retired) {
        return null;
    }


    public boolean checkDuplicate(Institution institution, Registrant registrant, Boolean supervisor, Boolean practitionerInCharge) {
        List<InstitutionManager> list = entityManager.createQuery("SELECT i FROM InstitutionManager i WHERE i.institution =:institution and  i.registrant =:registrant ")
                .setParameter("institution", institution)
                .setParameter("registrant",registrant)
                .getResultList();

        if (list.isEmpty()) {
            return false;
        }

        for (InstitutionManager manager : list) {
            if (manager.isInstitutionSupervisor() == supervisor) {
                return true;
            }

            if (manager.isPractitionerInCharge() == practitionerInCharge) {
                return true;
            }

        }
        return false;
    }


    public List<Institution> findAllInstitutions() {
        return entityManager.createQuery("SELECT i FROM Institution i").getResultList();
    }

    public void copyDataFromInstitutionToInstitutionManager() {
        List<Institution> institutions = institutionService.getInstitutions(generalParametersService.get().getHealthInstitutionType());
        for (Institution institution : institutions) {

            if (institution.getSupervisor() != null) {
                if (!this.checkDuplicate(institution, institution.getSupervisor(), true, false)) {
                    InstitutionManager institutionManager = new InstitutionManager();
                    institutionManager.setInstitution(institution);
                    institutionManager.setRegistrant(institution.getSupervisor());
                    institutionManager.setInstitutionSupervisor(true);
                    institutionManagerRepository.save(institutionManager);
                }
            }

            if (institution.getRegistrant() != null) {
                if (!this.checkDuplicate(institution, institution.getRegistrant(), false, true)) {
                    InstitutionManager institutionManager = new InstitutionManager();
                    institutionManager.setInstitution(institution);
                    institutionManager.setRegistrant(institution.getRegistrant());
                    institutionManager.setPractitionerInCharge(true);
                    institutionManagerRepository.save(institutionManager);
                }
            }

        }

    }
}