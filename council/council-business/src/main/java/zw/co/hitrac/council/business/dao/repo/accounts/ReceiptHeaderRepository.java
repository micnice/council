
package zw.co.hitrac.council.business.dao.repo.accounts;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.ReceiptHeader;

/**
 *
 * @author Michael Matiashe
 */
public interface ReceiptHeaderRepository extends CrudRepository<ReceiptHeader, Long> {

    public List<ReceiptHeader> findAll();
    
}
