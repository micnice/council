package zw.co.hitrac.council.web.pages.accounts;

import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author Charles Chigoriwa
 */
public abstract class IAccountsReceivablePage extends IAccountingPage {

    public IAccountsReceivablePage() {
        menuPanelManager.getAccountingPanel().setAccountsReceivable(true);
    }
}
