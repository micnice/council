package zw.co.hitrac.council.business.utils;

import com.google.common.base.Joiner;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

/**
 * Created by clive on 5/28/15.
 */
public class StringFormattingUtil {
    private static final Joiner noSpaceJoiner = Joiner.on(StringUtils.EMPTY).skipNulls();
    private static final Logger logger = LoggerFactory.getLogger(StringFormattingUtil.class);
    public static String joinStrings(boolean separateWithSpace, Object... objects) {

        logger.debug("Joining Strings separate space={}", separateWithSpace);

        Objects.requireNonNull(objects, "objects to join are required");

        if (separateWithSpace) {

            return joiner.join(objects);

        } else {

            return noSpaceJoiner.join(objects);
        }
    }
    public static String joinStrings(String customSeparatorString, Object... objects) {

        logger.debug("Joining Strings with customer separator={}", customSeparatorString);

        Validate.notEmpty(customSeparatorString, "Custom separator string is required");
        Objects.requireNonNull(objects, "objects to join are required");

        Joiner customJoiner = Joiner.on(customSeparatorString).skipNulls();
        return customJoiner.join(objects);
    }


    private static Joiner joiner = Joiner.on(" ").skipNulls();

    public static String join(Object... objects) {
        return joiner.join(objects);
    }
    public static String getMissingFieldMessage(final String fieldName) {

        return joinStrings(true, "Validation Error.", fieldName, "is required but none was " +
                "provided");

    }

    public static String upperCaseFirst(String value) {

        // Convert String to char array.
        char[] array = value.toCharArray();

        if (array.length > 0) {
            // Modify first element in array.
            array[0] = Character.toUpperCase(array[0]);

            // then capitalize if space on left.
            for (int x = 1; x < array.length; x++) {
                if (array[x - 1] == ' ') {
                    array[x] = Character.toUpperCase(array[x]);
                }
            }
            // Return string.
            return new String(array);

        }
        return "";
    }

}
