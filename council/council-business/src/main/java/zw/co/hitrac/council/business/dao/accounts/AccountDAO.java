
package zw.co.hitrac.council.business.dao.accounts;

import java.util.List;
import zw.co.hitrac.council.business.dao.IGenericDAO;
import zw.co.hitrac.council.business.domain.accounts.Account;

/**
 *
 * @author Tatenda Chiwandire
 * @author Michael Matiashe
 */
public interface AccountDAO extends IGenericDAO<Account> {
    
    public List<Account> getGLAccounts();
    
    public List<Account> getPersonalAccountsErrors();
    
}
