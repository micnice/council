/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.UserDAO;
import zw.co.hitrac.council.business.domain.User;
import zw.co.hitrac.council.business.service.UserService;
import zw.co.hitrac.council.business.utils.Util;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Clive Gurure
 * @author Matiashe Michael
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDAO;

    @Transactional
    public User save(User t) {
        return userDAO.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<User> findAll() {
        return userDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public User get(Long id) {
        return userDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<User> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<User> getAuthorisers() {
        return userDAO.getAuthorisers();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<User> getApplicationpProcessors() {
        return userDAO.getApplicationpProcessors();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<User> getCouncilProcessors() {
        return userDAO.getCouncilProcessors();
    }

    @Transactional
    public void encryptAllPasswords() {
        userDAO.encryptAllPasswords();
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Optional<User> getCurrentUser() {

        return Util.getCurrentUser();
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Optional<User> findByNameIgnoreCase(String userName) {

        Objects.requireNonNull(userName, "User name is required");
        return userDAO.findByNameIgnoreCase(userName);
    }
}
