package zw.co.hitrac.council.web.pages.registration;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.process.BillingProcess;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.accounts.documents.DebtComponentsPage;
import zw.co.hitrac.council.web.utility.CustomDateLabel;
import zw.co.hitrac.council.web.utility.CustomDateTimeLabel;

import java.util.List;

/**
 * @author Michael Matiashe
 */
public class RegistrationsDataListPanel extends Panel {

    /**
     * * @param registrant
     */
    @SpringBean
    private BillingProcess billingProcess;
    @SpringBean
    private RegistrantService registrantService;

    public RegistrationsDataListPanel(String id, final IModel<List<Registration>> model) {
        super(id);
        add(new PropertyListView<Registration>("eachItem", model) {
            @Override
            protected void populateItem(final ListItem<Registration> item) {

                PageParameters params = new PageParameters();
                params.add("registrantionId", item.getModelObject().getId());
                item.add(new BookmarkablePageLink("registrationOptionPage", RegistrantViewPage.class, params).add(new
                        Label("registrant", item.getModelObject().getRegistrant().getFullname())));

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                item.add(new CustomDateLabel("registrationDate"));
                item.add(new Label("institution"));
                item.add(new Label("registrant.registrationNumber"));
                item.add(new Label("register"));
                item.add(new Label("course"));
                item.add(new Label("createdBy"));
                item.add(new Label("modifiedBy"));
                item.add(CustomDateTimeLabel.forDate("dateCreated",
                        new PropertyModel<>(item.getModel(), "dateCreated")));
                item.add(CustomDateTimeLabel.forDate("dateModified",
                        new PropertyModel<>(item.getModel(), "dateModified")));


                //Administrator only no payment for these changes
                Link<Registration> viewLink3 = new Link<Registration>("bill", item.getModel()) {
                    // any application
                    @Override
                    public void onClick() {

                    }

                    @Override
                    protected void onConfigure() {
                        billingProcess.billAnnualProduct(item.getModelObject(), new DebtComponent());
                        setResponsePage(new DebtComponentsPage(registrantService.get(item.getModelObject()
                                .getRegistrant().getId()).getId()));
                    }
                };
                item.add(viewLink3);
            }
        });
        add(new Link("billAll") {
            @Override
            public void onClick() {
                for (Registration registration : model.getObject()) {
                    billingProcess.billAnnualProduct(registration, new DebtComponent());
                }
            }
        });
    }
}