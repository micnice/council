/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.accounts.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.accounts.CustomerDAO;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.service.accounts.CustomerService;

import java.util.List;

/**
 *
 * @author tdhlakama
 */
@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDAO customerAccountDAO;

    @Transactional
    public Customer save(Customer customerAccount) {
        return customerAccountDAO.save(customerAccount);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Customer> findAll() {
        return customerAccountDAO.findAll();

    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Customer get(Long id) {
        return customerAccountDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Customer> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setCustomerAccountDAO(CustomerDAO customerAccountDAO) {
        this.customerAccountDAO = customerAccountDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Customer get(Account account) {
        return customerAccountDAO.get(account);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Registrant getRegistrant(Account account) {
        return customerAccountDAO.getRegistrant(account);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Institution getInstitution(Account account) {
        return customerAccountDAO.getInstitution(account);
    }
}
