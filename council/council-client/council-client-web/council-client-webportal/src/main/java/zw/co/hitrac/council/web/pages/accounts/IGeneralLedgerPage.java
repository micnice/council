package zw.co.hitrac.council.web.pages.accounts;

/**
 *
 * @author tdhlakama
 */
public abstract class IGeneralLedgerPage extends IAccountingPage {

    public IGeneralLedgerPage() {
        menuPanelManager.getAccountingPanel().setGeneralLedger(true);
    }
}
