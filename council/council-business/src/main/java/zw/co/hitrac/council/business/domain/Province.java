/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Kelvin Goredema
 */
@Entity
@Table(name="province")
@Audited
@XmlRootElement
public class Province extends BaseEntity{
   private static final long serialVersionUID = 1L;
   
    private Set<District> districts = new HashSet<District>();
  
   @OneToMany(mappedBy = "province")
    public Set<District> getDistricts() {
        return  districts;
    }

    public void setDistricts(Set<District> districts) {
        this.districts = districts;
    }
}
    

    
    
    
    
    
    

