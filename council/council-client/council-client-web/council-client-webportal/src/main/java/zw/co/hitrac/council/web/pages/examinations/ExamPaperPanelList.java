
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;

import zw.co.hitrac.council.business.domain.ModulePaper;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;

/**
 *
 * @author tdhlakama
 */
public class ExamPaperPanelList extends Panel {

    /**
     * Constructor
     *
     * @param id
     */
    public ExamPaperPanelList(String id) {
        super(id);

        // - Used to Display a to list of ModulePaper Papers in ModulePaper Registration
        add(new PropertyListView<ModulePaper>("paperList") {
            @Override
            protected void populateItem(ListItem<ModulePaper> item) {
                item.add(new Label("name"));
            }
        });
    }

    /**
     * Constructor
     *
     * @param id
     * @param registrant
     */
    public ExamPaperPanelList(String id, final ExamSetting examSetting) {
        this(id);

        // method to add a new Registration - Id is null
    }

    /**
     * Constructor
     *
     * @param id
     * @param ExamList
     */
    public ExamPaperPanelList(String id, IModel<List<ModulePaper>> model) {
        super(id);

        PropertyListView<ModulePaper> eachItem = new PropertyListView<ModulePaper>("paperList", model) {
            @Override
            protected void populateItem(ListItem<ModulePaper> item) {
                item.add(new Label("name"));
                item.setModel(new CompoundPropertyModel<ModulePaper>(item.getModel()));
            }
        };

        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
