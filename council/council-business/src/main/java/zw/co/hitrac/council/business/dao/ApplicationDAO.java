package zw.co.hitrac.council.business.dao;

import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.ApplicationPurpose;
import zw.co.hitrac.council.business.domain.QualificationType;
import zw.co.hitrac.council.business.domain.Registrant;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Michael Matiashe
 */
public interface ApplicationDAO extends IGenericDAO<Application> {

    public List<Application> getApplicationsByYearApproved(int year);

    List<Application> getApplications(String query, String applicationStatus, Date startDate, Date endDate, ApplicationPurpose applicationPurpose, Boolean used, Boolean expired);

    public List<Application> getRegistrantApplicationStatus(String applicationStatus);

    public Integer getTotalApplicationsByStatus(String status);
    
    public Integer getNumberOfPendingApplications();
    
    public List<Application> getFilteredPendingApplications();
    
    public List<Application> getExpiredApplications();

    public List<Application> getApplicationsWithoutPayment();
    
    public Boolean hasPendingApplication(Registrant registrant, ApplicationPurpose applicationPurpose);
}
