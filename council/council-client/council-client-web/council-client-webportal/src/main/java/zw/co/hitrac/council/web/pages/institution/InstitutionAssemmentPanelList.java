package zw.co.hitrac.council.web.pages.institution;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Application;

import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.RegistrantAssessment;
import zw.co.hitrac.council.business.service.RegistrantAssessmentService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.utility.CustomDateLabel;

/**
 *
 * @author Constance Mabaso
 */
public class InstitutionAssemmentPanelList extends Panel {

    @SpringBean
    RegistrantAssessmentService registrantAssessmentService;
  
    /**
     * Constructor
     *
     * @param id
     * @param institution
     */
    public InstitutionAssemmentPanelList(String id, final IModel<Application> applicationModel) {
        super(id);

        // method to add a new RegistrantAssessment - Id is null
        add(new Link<Institution>("institutionAssemmentPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionAssessmentPage(applicationModel));
            }
        });
        add(new PropertyListView<RegistrantAssessment>("registrantAssessmentList", registrantAssessmentService.getAssessments(applicationModel.getObject())) {
            @Override
            protected void populateItem(ListItem<RegistrantAssessment> item) {
                Link<RegistrantAssessment> viewLink = new Link<RegistrantAssessment>("institutionAssemmentPage",
                        item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new InstitutionAssessmentPage(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(viewLink);
                item.add(new CustomDateLabel("date"));
                item.add(new Label("assessor"));
                item.add(new Label("decisionStatus"));
            }
        });
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
