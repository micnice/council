$(function () {

    $("#au_dialog").dialog({
        autoOpen: false,
        width: 800,
        buttons: {
            close: function () {
                $("#au_dialog").dialog("close");
            }
        }
    });

    $("#transhistory").click(function () {
        $("#au_dialog").dialog("open");
    });

    $("#tableList").dataTable({
        "bFilter": true,
        "sPaginationType": "full_numbers"
    });

});
