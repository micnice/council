package zw.co.hitrac.council.web.config;

import org.apache.wicket.protocol.http.WicketFilter;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;
import zw.co.hitrac.council.business.utils.SpringProfiles;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


/**
 * Created by scott on 06/08/16.
 */
public class CouncilWebInitializer implements WebApplicationInitializer {

    private static final String ACTIVE_SPRING_PROFILE = "spring.profiles.active";
    private static final String DEFAULT_SPRING_PROFILE = "spring.profiles.default";
    private static final int SESSION_DURATION_IN_SEC = 60 * 60 * 60 * 1; //1hour

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {

        AnnotationConfigWebApplicationContext webApplicationContext = new AnnotationConfigWebApplicationContext();
        servletContext.setInitParameter(DEFAULT_SPRING_PROFILE, SpringProfiles.DEV.getName());

        //Change to your council profile
        servletContext.setInitParameter(ACTIVE_SPRING_PROFILE, SpringProfiles.DEV.getName());

        servletContext.addListener(new ContextLoaderListener(webApplicationContext));

        servletContext.getSessionCookieConfig().setMaxAge(SESSION_DURATION_IN_SEC);

        webApplicationContext.register(CouncilWebConfiguration.class);
        webApplicationContext.setServletContext(servletContext);
        webApplicationContext.refresh();

        FilterRegistration springFilter = servletContext.addFilter("springSecurityFilterChain",
                DelegatingFilterProxy.class);

        //Specify isMatchAfter=false to add Spring filter before all other filters thereby injecting the context
        springFilter.addMappingForUrlPatterns(null, false, "/*");

        FilterRegistration OEIVFilter =
                servletContext.addFilter("openEntityManagerInViewFilter", OpenEntityManagerInViewFilter.class);
        OEIVFilter.addMappingForUrlPatterns(null, true, "/*");

        FilterRegistration wicketFilter = servletContext.addFilter("wicketFilter", WicketFilter.class);

        final String activeProfile = servletContext.getInitParameter(ACTIVE_SPRING_PROFILE);
        wicketFilter.setInitParameters(getWicketFilterInitParameters(activeProfile));
        wicketFilter.addMappingForUrlPatterns(null, true, "/*");


    }

    private Map<String, String> getWicketFilterInitParameters(final String activeProfile) {

        Objects.requireNonNull(activeProfile, "Must specify a Spring activeProfile to determine deployment type");

        Map<String, String> initParameters = new HashMap<>();
        initParameters.put("applicationClassName", CouncilApplication.class.getName());
        initParameters.put(WicketFilter.FILTER_MAPPING_PARAM, "/*");

        if (isRunningInProductionMode(activeProfile)) {
            initParameters.put("configuration", "deployment");
        } else {
            initParameters.put("configuration", "development");
        }

        return initParameters;
    }

    private boolean isRunningInProductionMode(final String activeProfile) {
        return !SpringProfiles.DEV.getName().equals(activeProfile);
    }
}
