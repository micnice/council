
package zw.co.hitrac.council.business.service;

import java.util.List;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.InstitutionPracticeControl;

/**
 *
 * @author Constance Mabaso
 */
public interface InstitutionPracticeControlService extends IGenericService<InstitutionPracticeControl> {
  public List<InstitutionPracticeControl> get(Application application);
}
