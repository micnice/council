package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.domain.accounts.Product;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author Michael Matiashe
 */
@Entity
@Table(name="transferrule")
@Audited
public class TransferRule implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String name;
    private Register fromRegister;
    private Register toRegister;
    private Course fromCourse;
    private Course toCourse;
    private long version;
    private Product product;
    
    @ManyToOne(optional = true)
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne(optional = false)
    public Register getFromRegister() {
        return fromRegister;
    }

    public void setFromRegister(Register fromRegister) {
        this.fromRegister = fromRegister;
    }

    @ManyToOne(optional = false)
    public Register getToRegister() {
        return toRegister;
    }

    public void setToRegister(Register toRegister) {
        this.toRegister = toRegister;
    }

    @ManyToOne(optional = true)
    public Course getFromCourse() {
        return fromCourse;
    }

    public void setFromCourse(Course fromCourse) {
        this.fromCourse = fromCourse;
    }

    @ManyToOne(optional = true)
    public Course getToCourse() {
        return toCourse;
    }

    public void setToCourse(Course toCourse) {
        this.toCourse = toCourse;
    }

    @Version
    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TransferRule other = (TransferRule) obj;
        if ((this.id == null) ? (other.id != null) : !this.id.equals(other.id)) {
            return false;
        }
        return true;
    }
}
