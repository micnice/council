package zw.co.hitrac.council.web.utility;

import org.apache.wicket.util.resource.AbstractResourceStream;
import org.apache.wicket.util.resource.ResourceStreamNotFoundException;
import zw.co.hitrac.council.reports.utils.ContentType;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by scott on 29/03/16.
 */
public class ReportResourceStream extends AbstractResourceStream {

    private ByteArrayInputStream inputStream;
    private ContentType contentType;

    public ReportResourceStream(ByteArrayInputStream inputStream, ContentType contentType) {
        this.inputStream = inputStream;
        this.contentType = contentType;
    }

    @Override
    public InputStream getInputStream() throws ResourceStreamNotFoundException {
        return inputStream;
    }

    @Override
    public void close() throws IOException {
        inputStream.close();
    }

    @Override
    public String getContentType() {
        return contentType.getContentTypeString();
    }
}
