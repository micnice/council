/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.registration;

import java.util.List;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.RegistrantCondition;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrantConditionService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author Artwell Mamvura
 */
public class ConditionsPage extends TemplatePage {

    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private RegistrantConditionService registrantConditionService;

    public ConditionsPage() {
        add(new ConditionsPanelList("conditionPanelList", registrantConditionService.getAllExpiredConditions()));
    }

}
