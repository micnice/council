/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.utils;

import org.apache.commons.lang.StringUtils;

/**
 *
 * @author tdhlakama
 */
public final class CaseUtil {
    
 static Boolean uppercaseSet = Boolean.TRUE;

    public static String upperCase(String c) {

        return StringUtils.upperCase(c);
    }

    public static String lowerCase(String c) {

	       return StringUtils.lowerCase(c);
    }
}
