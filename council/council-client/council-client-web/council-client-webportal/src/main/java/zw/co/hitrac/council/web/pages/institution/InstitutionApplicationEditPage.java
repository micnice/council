package zw.co.hitrac.council.web.pages.institution;

import java.util.Arrays;
import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.service.ApplicationService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author Michael Matiashe
 */
public class InstitutionApplicationEditPage extends TemplatePage {

    @SpringBean
    private ApplicationService applicationService;

    public InstitutionApplicationEditPage(Long id) {
        menuPanelManager.getApplicationPanel().setTopMenuCurrent(true);
        final CompoundPropertyModel<Application> model = new CompoundPropertyModel<Application>(applicationService.get(id));
        setDefaultModel(model);

        Form<Application> form = new Form<Application>("form", (IModel<Application>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    applicationService.save(getModelObject());
                    setResponsePage(new InstitutionApplicationViewPage(getModelObject().getId()));

                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };
        form.add(new Label("currentStatus", model.getObject().getApplicationTextStatus()));
        form.add(new DropDownChoice("applicationStatus", Application.getApplicationStates));
        form.add(new TextArea<String>("comment"));
        add(form);
        add(new Label("institution.registrant.fullname"));
        add(new FeedbackPanel("feedback"));
        form.add(
                new Link<Void>("institutionApplicationViewPage") {
            @Override
            public void onClick() {
               setResponsePage(new InstitutionApplicationViewPage(model.getObject().getId()));
            }
        });
    }
}
