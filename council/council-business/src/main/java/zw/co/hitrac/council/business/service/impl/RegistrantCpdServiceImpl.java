package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.RegistrantCpdDAO;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantCpd;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.RegistrantCpdService;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Constance Mabaso
 */
@Service
@Transactional
public class RegistrantCpdServiceImpl implements RegistrantCpdService {

    @Autowired
    private RegistrantCpdDAO registrantCpdDAO;

    @Transactional
    public RegistrantCpd save(RegistrantCpd registrantCpd) {
        return registrantCpdDAO.save(registrantCpd);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantCpd> findAll() {
        return registrantCpdDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantCpd get(Long id) {
        return registrantCpdDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantCpd> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setRegistrantCpdDAO(RegistrantCpdDAO registrantCpdDAO) {
        this.registrantCpdDAO = registrantCpdDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public BigDecimal getCurrentCPDs(Registrant registrant, Duration duration) {
        return registrantCpdDAO.getCurrentCPDs(registrant, duration);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantCpd> getCurrentCPDsList(Registrant registrant, Duration duration) {
        return registrantCpdDAO.getCurrentCPDsList(registrant, duration);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantCpd> getCurrentCPDsList(Registrant registrant) {
        return registrantCpdDAO.getCurrentCPDsList(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public BigDecimal getCurrentCPDs(Registrant registrant, CouncilDuration duration) {
        return registrantCpdDAO.getCurrentCPDs(registrant, duration);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantCpd> getCPDsList(Registrant registrant) {
        return registrantCpdDAO.getCPDsList(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getPaidAnnualFeeAndInadequateCPDS(CouncilDuration councilDuration) {
        return registrantCpdDAO.getPaidAnnualFeeAndInadequateCPDS(councilDuration);
    }

}
