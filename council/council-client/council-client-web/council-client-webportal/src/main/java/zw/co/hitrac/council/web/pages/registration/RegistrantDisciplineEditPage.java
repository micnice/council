/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.registration;

import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.jackrabbit.value.BinaryImpl;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.lang.Bytes;
import sun.net.www.MimeTable;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.domain.SupportDocument;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.DatePickerUtil;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;

import javax.jcr.Node;
import javax.jcr.Repository;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author kelvin
 */
public class RegistrantDisciplineEditPage extends TemplatePage {
    
    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private CourseService courseService;
    @SpringBean
    private SupportDocumentService supportDocumentService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    RegistrationService registrationService;
    @SpringBean
    private RegistrationProcess registrationProcess;
    @SpringBean
    private RegistrantDisciplineService registrantDisciplineService;
    
    public RegistrantDisciplineEditPage(final IModel<Registrant> registrant) {
        
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        final CompoundPropertyModel<Registrant> registrantModel = new CompoundPropertyModel<Registrant>(new DetachableRegistrantModel(registrant.getObject().getId(), registrantService));
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());
        
        final FeedbackPanel uploadFeedback = new FeedbackPanel("uploadFeedback");

        // Add uploadFeedback to the page itself
        add(uploadFeedback);
        final FileUploadForm simpleUploadForm = new FileUploadForm("simpleUpload", Model.of(registrantModel.getObject()));
        add(simpleUploadForm);
        add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });
        
        add(new Label("fullname", registrantModel.getObject().getFullname()));
    }
    
    private class FileUploadForm extends Form<Registrant> {
        
        FileUploadField fileUploadField;
        SupportDocument supportDocument;
        private Date clearanceDate;
        private Course course;

        /**
         * Construct.
         *
         * @param name Component name
         */
        public FileUploadForm(String name, final Model<Registrant> registrantModel) {
            super(name);
            PropertyModel<Course> courseModel = new PropertyModel<Course>(this, "course");
            PropertyModel<Date> clearanceDateModel = new PropertyModel<Date>(this, "clearanceDate");
            // set this form to multipart mode (allways needed for uploads!)
            setMultiPart(true);

            // Add one file input field
            add(fileUploadField = new FileUploadField("fileInput"));
            add(new TextField<Date>("clearanceDate", clearanceDateModel).setRequired(true).add(DatePickerUtil.getDatePicker()).add(new ErrorBehavior()));
            add(new DropDownChoice("course", courseModel, courseService.findAll()).setRequired(true).add(new ErrorBehavior()));
            supportDocument = new SupportDocument();
            setDefaultModel(registrantModel);

            // Set maximum size to 100K for demo purposes
            setMaxSize(Bytes.kilobytes(10000));
            
            
            
        }
        
        @Override
        protected void onSubmit() {
            
            final List<FileUpload> uploads = fileUploadField.getFileUploads();
            
            if (uploads != null) {
                for (FileUpload upload : uploads) {

                    // UploadPage.this.info("saved file: " + upload.getClientFileName());
                    try {
                        System.out.println("================Supporting Documents====================");
                        
                        Repository repository = JcrUtils.getRepository(generalParametersService.get().getContent_Url());
                        
                        Session session = repository.login(new SimpleCredentials("admin", "admin".toCharArray()));
                        String user = session.getUserID();
                        String name = repository.getDescriptor(Repository.REP_NAME_DESC);
                        
                        System.out.println("Logged in as " + user + " to a " + name + " repository.");
                        
                        try {
                            Node root = session.getRootNode();
                            String fileName = upload.getClientFileName();
                            MimeTable mt = MimeTable.getDefaultTable();
                            String mimeType = mt.getContentTypeFor(fileName);
                            
                            if (mimeType == null) {
                                mimeType = "application/octet-stream";
                            }
                            
                            String id = String.valueOf(this.getModelObject().getId());
                            
                            Node contentNode;
                            
                            if (root.hasNode("content")) {
                                contentNode = root.getNode("content");
                            } else {
                                contentNode = root.addNode("content");
                            }
                            
                            Node councilNode;
                            
                            if (contentNode.hasNode("council")) {
                                councilNode = contentNode.getNode("council");
                            } else {
                                councilNode = contentNode.addNode("council");
                            }
                            
                            Node registrantsNode;
                            
                            if (councilNode.hasNode("registrants")) {
                                registrantsNode = councilNode.getNode("registrants");
                            } else {
                                registrantsNode = councilNode.addNode("registrants");
                            }
                            
                            Node idNode;
                            
                            if (registrantsNode.hasNode(id)) {
                                idNode = registrantsNode.getNode(id);
                            } else {
                                idNode = registrantsNode.addNode(id);
                            }
                            
                            Node supportingDocumentsNode;
                            
                            if (idNode.hasNode("supportingDocuments")) {
                                supportingDocumentsNode = idNode.getNode("supportingDocuments");
                            } else {
                                supportingDocumentsNode = idNode.addNode("supportingDocuments");
                            }
                            
                            Node documentsNode = supportingDocumentsNode.addNode(upload.getClientFileName(), "nt:file");
                            
                            Node profileContentNode = documentsNode.addNode("jcr:content", "nt:resource");


                            BinaryImpl binary = new BinaryImpl(upload.getInputStream());
                            profileContentNode.setProperty("jcr:data", binary);

                            profileContentNode.setProperty("jcr:lastModified", Calendar.getInstance());
                            profileContentNode.setProperty("jcr:mimeType", mimeType);
                            
                            
                            supportDocument.setName(upload.getClientFileName());
                            supportDocument.setPathName(profileContentNode.getPath());
                            supportDocument.setDocumentType(mimeType);
                            supportDocument.setDateCreated(new Date());
                            supportDocument.setRegistrant(getModelObject());
                            supportDocument.setCreatedBy(CouncilSession.get().getUser());
                            supportDocument.setSubmitted(Boolean.TRUE);
                            supportDocument = supportDocumentService.save(supportDocument);
                            
//                            RegistrantDiscipline registrantDiscipline = new RegistrantDiscipline();
//                            registrantDiscipline.setSupportDocument(supportDocument);
//                            registrantDiscipline.setDisciplineDate(clearanceDate);
//                            registrantDiscipline.setCourse(course);
//                            registrantDiscipline.setRegistrant(getModelObject());
//                            registrantDisciplineService.save(registrantDiscipline);
                            
                            session.save();

                            Registrant r = registrantService.get(getModel().getObject().getId());
                            Registration registration = registrationProcess.activeRegisterNotStudentRegister(r);
                            if (registration != null) {
                                setResponsePage(new RequirementConfirmationPage(registration.getId()));
                            } else {
                                setResponsePage(new RegistrantViewPage(this.getModelObject().getId()));
                            }

                            //Always call this to release resources
                            binary.dispose();

                        } finally {
                            session.logout();
                        }
                    } catch (Exception ex) {
                        this.error("Failed to upload document, Please Try again ");
                    }
                }
            }
        }
    }
}
