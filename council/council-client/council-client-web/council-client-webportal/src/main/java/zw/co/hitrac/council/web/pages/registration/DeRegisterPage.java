package zw.co.hitrac.council.web.pages.registration;

import org.apache.wicket.Component;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;

public class DeRegisterPage extends TemplatePage {

    @SpringBean
    private RegistrantService registrantService;

    @SpringBean
    private RegistrationProcess registrationProcess;

    public DeRegisterPage(Long id) {
        this.menuPanelManager.getRegistrantionPanel().setTopMenuCurrent(true);
        this.menuPanelManager.getRegistrantionPanel().setRegistrantEditPageCurrent(true);
        setDefaultModel(new CompoundPropertyModel(new LoadableDetachableRegistrantModel(id)));
        add(new RegistrantPanel("registrantPanel", id));
        Form form = new Form("form", getDefaultModel()) {
            public void onSubmit() {
                try {
                    Registrant registrant = DeRegisterPage.this.registrationProcess.deRegisterRegistrant((Registrant) getModelObject());
                    setResponsePage(new RegistrantViewPage(registrant.getId()));
                } catch (CouncilException e) {
                    error(e.getMessage());
                }
            }
        };
        form.add(new Component[]{new TextField("comment").setRequired(true).add(new Behavior[]{new ErrorBehavior()})});
        add(form);
        add(new FeedbackPanel("feedback"));
    }

    private final class LoadableDetachableRegistrantModel extends LoadableDetachableModel<Registrant> {

        private Long id;

        public LoadableDetachableRegistrantModel(Long id) {
            this.id = id;
        }

        protected Registrant load() {
            Registrant registrant = null;

            if (this.id == null) {
                registrant = new Registrant();
            } else {
                registrant = (Registrant) DeRegisterPage.this.registrantService.get(this.id);
            }
            return registrant;
        }
    }
}
