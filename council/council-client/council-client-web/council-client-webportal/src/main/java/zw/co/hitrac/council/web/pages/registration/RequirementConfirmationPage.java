package zw.co.hitrac.council.web.pages.registration;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.domain.Requirement;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.business.process.PaymentProcess;

//~--- JDK imports ------------------------------------------------------------
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.QualificationType;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantClearance;
import zw.co.hitrac.council.business.domain.RegistrantQualification;
import zw.co.hitrac.council.business.domain.SupportDocument;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.domain.accounts.PaymentDetails;
import zw.co.hitrac.council.business.domain.accounts.ReceiptItem;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.business.process.ApplicationProcess;
import zw.co.hitrac.council.business.process.InvoiceProcess;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.process.RenewalProcess;
import zw.co.hitrac.council.business.service.ApplicationService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.InstitutionTypeService;
import zw.co.hitrac.council.business.service.RegisterService;
import zw.co.hitrac.council.business.service.RegistrantClearanceService;
import zw.co.hitrac.council.business.service.RegistrantQualificationService;
import zw.co.hitrac.council.business.service.SupportDocumentService;
import zw.co.hitrac.council.business.service.accounts.AccountsParametersService;
import zw.co.hitrac.council.business.service.accounts.ReceiptItemService;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationService;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.accounts.documents.DebtComponentsPage;
import zw.co.hitrac.council.web.pages.accounts.payments.PaymentConfirmationPage;
import zw.co.hitrac.council.web.pages.institution.InstitutionApplicationPage;
import zw.co.hitrac.council.web.pages.institution.InstitutionViewPage;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;
import zw.co.hitrac.council.web.utility.DetachableRegistrationModel;

/**
 *
 * @author Takunda Dhlakama
 * @author Michael Matiashe
 * @author Daniel Nkhoma
 */
public class RequirementConfirmationPage extends TemplatePage {

    @SpringBean
    private RegistrationService registrationService;
    @SpringBean
    private InstitutionTypeService institutionTypeService;
    @SpringBean
    private CourseService courseService;
    @SpringBean
    private DebtComponentService debtComponentService;
    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private SupportDocumentService supportDocumentService;
    @SpringBean
    private ApplicationService applicationService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private RegisterService registerService;
    @SpringBean
    private ApplicationProcess applicationProcess;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private ExamRegistrationService examRegistrationService;
    @SpringBean
    private RegistrationProcess registrationProcess;
    @SpringBean
    private RegistrantClearanceService registrantClearanceService;
    @SpringBean
    private InvoiceProcess invoiceProcess;
    @SpringBean
    private ReceiptItemService receiptItemService;
    @SpringBean
    private RegistrantQualificationService registrantQualificationService;
    @SpringBean
    private RenewalProcess renewalProcess;
    @SpringBean
    private AccountsParametersService accountsParametersService;
    @SpringBean
    private PaymentProcess paymentProcess;

    public RequirementConfirmationPage(long id) {

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        final CompoundPropertyModel<Registration> model = new CompoundPropertyModel<Registration>(new DetachableRegistrationModel(id, registrationService));
        setDefaultModel(model);
        final CompoundPropertyModel<Registrant> registrantModel = new CompoundPropertyModel<Registrant>(new DetachableRegistrantModel(model.getObject().getRegistrant().getId(), registrantService));
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(model.getObject().getRegistrant().getId());

        add(new Label("registrant.fullname"));
        add(new FeedbackPanel("feedback"));
        Set<Requirement> totalRequirements = new HashSet<Requirement>();
        List<Requirement> courseRequirements = new ArrayList<Requirement>(courseService.get(registrationProcess.activeRegisterNotStudentRegister(registrantModel.getObject()).getCourse().getId()).getRequirements());
       List<Requirement> registerRequirements = new ArrayList<Requirement>(registerService.get(registrationProcess.activeRegisterNotStudentRegister(registrantModel.getObject()).getRegister().getId()).getRequirements());
       totalRequirements.addAll(courseRequirements);
       totalRequirements.addAll(registerRequirements);

        Set<Requirement> requirements = new HashSet<Requirement>();
        for (Requirement r : totalRequirements) {
            if (r.getGeneralRequirement()) {
                SupportDocument document = supportDocumentService.getSupportDocument(registrantModel.getObject(), r);
                if (document == null) {
                    requirements.add(r);
                }
            }
        }
        final Set<Requirement> finalRequirementsSet = new HashSet<Requirement>(requirements);
        RegistrantQualification registrantQualification = registrantQualificationService.getRegistrantQualificationByRegistrantAndCourse(model.getObject().getRegistrant(), model.getObject().getCourse());
        if (registrantQualification != null) {
            if (registrantQualification.getDateAwarded() != null) {
                if (generalParametersService.get().isRequireAffidavit()) {
                    int differenceInMonths = renewalProcess.monthsBetween(new Date(), registrantQualification.getDateAwarded());
                    if (generalParametersService.get().getAffidavit() != null && generalParametersService.get().getMonthsToRequireAffidavit() != null) {
                        if (differenceInMonths >= generalParametersService.get().getMonthsToRequireAffidavit()) {
                            totalRequirements.add(generalParametersService.get().getAffidavit());
                        }

                    }
                }
            }
        }
        add(new Link<Registrant>("processPayment") {
            @Override
            public void onClick() {
                Customer customer = registrantService.get(registrantModel.getObject().getId()).getCustomerAccount();
                if (generalParametersService.get().getRegistrationByApplication()) {

                    List<DebtComponent> debtComponents = debtComponentService.getDebtComponents(customer);

                    if (debtComponents.isEmpty()) {

                        setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
                    } else {
                        //selet items to pay for
                        setResponsePage(new DebtComponentsPage(registrantService.getRegistrant(customer).getId()));
                    }
                } else {
                    List<DebtComponent> debtComponents = debtComponentService.getDebtComponents(customer.getAccount());
                    List<ReceiptItem> receiptItemList = receiptItemService.getReceiptItemsWithNoReceiptHeader(customer.getAccount());

                    if (debtComponents.isEmpty() && !receiptItemList.isEmpty()) {
                        Set<ReceiptItem> receiptItems = new HashSet<ReceiptItem>();

                        if (receiptItemList.isEmpty()) {
                            setResponsePage(new RegistrantViewPage(registrantService.getRegistrant(customer).getId()));
                        } else {
                            for (ReceiptItem receiptItem : receiptItemList) {
                                receiptItems.add(receiptItem);
                            }
                            PaymentDetails paymentDetails = paymentProcess.accountBallancePaysAll(receiptItems, CouncilSession.get().getUser(), customer, null);

                            setResponsePage(new PaymentConfirmationPage(paymentDetails, customer.getAccount()));
                        }
                    } else if (!debtComponents.isEmpty() && !receiptItemList.isEmpty()) {
                        Set<ReceiptItem> receiptItems = new HashSet<ReceiptItem>();
                        if (receiptItemList.isEmpty()) {
                            setResponsePage(new RegistrantViewPage(registrantService.getRegistrant(customer).getId()));
                        } else {
                            for (ReceiptItem receiptItem : receiptItemList) {
                                receiptItems.add(receiptItem);
                            }
                            paymentProcess.accountBalancePaysLess(receiptItems, CouncilSession.get().getUser(), customer, null);
                        }
                        setResponsePage(new DebtComponentsPage(registrantService.getRegistrant(customer).getId()));

                    } else {
                        //Select the items you want to pay for
                        setResponsePage(new DebtComponentsPage(registrantService.getRegistrant(customer).getId()));
                    }
                }
            }

            @Override
            protected void onConfigure() {
                if (finalRequirementsSet.isEmpty()) {
                    setVisible(Boolean.TRUE);
                } else if (accountsParametersService.get().getRegistrationApplicationPayment() || model.getObject().getRegister().equals(generalParametersService.get().getProvisionalRegister())) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(Boolean.FALSE);
                }
            }
        });

        add(new Link<Registrant>("next") {
            @Override
            public void onClick() {
            }

            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });

        add(new Link<Registrant>("addCleranceLetter") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantClearanceEditPage(registrantModel));
            }

            @Override
            protected void onConfigure() {

                if (!generalParametersService.get().getRegistrationByApplication()) {
                    RegistrantClearance registrantClearance = registrantClearanceService.getRegistrantClearance(registrantModel.getObject(), model.getObject().getCourse());
                    if (registrantClearance == null) {
                        setVisible(Boolean.TRUE);
                    } else {
                        setVisible(Boolean.FALSE);
                    }
                } else {
                    setVisible(Boolean.FALSE);
                }
            }
        });

        add(new Link<Registrant>("registrationOptionPage") {
            @Override
            public void onClick() {

                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));

            }
        });

        PropertyListView<Requirement> eachItem = new PropertyListView<Requirement>("requirementsList", new ArrayList<Requirement>(finalRequirementsSet)) {
            @Override
            protected void populateItem(final ListItem<Requirement> item) {
                Link<Requirement> viewLink = new Link<Requirement>("registrantDocumentPage", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new RegistrantDocumentPage(getModelObject(), registrantModel));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                item.add(new Label("name"));

                Form<SupportDocument> form = new Form<SupportDocument>("form", new CompoundPropertyModel<SupportDocument>(new SupportDocument())) {
                    @Override
                    protected void onSubmit() {
                        this.getModelObject().setRegistrant(registrantModel.getObject());
                        this.getModelObject().setRequirement(item.getModelObject());
                        this.getModelObject().setSubmitted(Boolean.TRUE);
                        supportDocumentService.save(getModelObject());
                        setResponsePage(new RequirementConfirmationPage(model.getObject().getId()));
                    }

                    @Override
                    protected void onConfigure() {
                        if (getModel().getObject().getSubmitted()) {
                            setVisible(Boolean.FALSE);
                        } else {
                            if (item.getModelObject().getUpload()) {
                                setVisible(Boolean.TRUE);
                            } else {
                                setVisible(Boolean.FALSE);
                            }
                        }
                    }
                };
                form.add(new CheckBox("submitted"));
                item.add(form);
            }

            @Override
            protected void onConfigure() {
                if (!finalRequirementsSet.isEmpty()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(Boolean.FALSE);
                }
            }
        };

        add(eachItem);

    }

    public RequirementConfirmationPage(final ExamRegistration examRegistration) {

        final CompoundPropertyModel<ExamRegistration> examRegistrationModel = new CompoundPropertyModel<ExamRegistration>(
                examRegistrationService.get(examRegistration.getId()));

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        final CompoundPropertyModel<Registration> model = new CompoundPropertyModel<Registration>(new DetachableRegistrationModel(examRegistrationModel.getObject().getRegistration().getId(), registrationService));
        setDefaultModel(model);
        final CompoundPropertyModel<Registrant> registrantModel = new CompoundPropertyModel<Registrant>(new DetachableRegistrantModel(model.getObject().getRegistrant().getId(), registrantService));
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(model.getObject().getRegistrant().getId());
        add(new Label("registrant.fullname"));
        add(new FeedbackPanel("feedback"));

        Set<Requirement> totalRequirements = new HashSet<Requirement>();
        Set<Requirement> requirements = new HashSet<Requirement>();
        List<Requirement> registerRequirements = new ArrayList<Requirement>(registerService.get(examRegistration.getRegistration().getRegister().getId()).getRequirements());
        totalRequirements.addAll(registerRequirements);
        for (Requirement r : totalRequirements) {
            SupportDocument document = supportDocumentService.getSupportDocument(examRegistration, r);
            if (document == null) {
                requirements.add(r);
            }
        }

        final Set<Requirement> finalRequirementsSet = new HashSet<Requirement>(requirements);
        add(new Link<Registrant>("processPayment") {
            @Override
            public void onClick() {
                Customer customer = registrantService.get(registrantModel.getObject().getId()).getCustomerAccount();
                List<DebtComponent> debtComponents = debtComponentService.getDebtComponents(customer);

                if (debtComponents.isEmpty()) {

                    setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
                } else {
                    //selet items to pay for
                    setResponsePage(new DebtComponentsPage(registrantService.getRegistrant(customer).getId()));
                }
            }
        });

        add(new Link<Registrant>("next") {
            @Override
            public void onClick() {
            }

            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });

        add(new Link<Registrant>("addCleranceLetter") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantClearanceEditPage(registrantModel));
            }

            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });

        add(new Link<Registrant>("registrationOptionPage") {
            @Override
            public void onClick() {

                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));

            }
        });

        PropertyListView<Requirement> eachItem = new PropertyListView<Requirement>("requirementsList", new ArrayList<Requirement>(finalRequirementsSet)) {
            @Override
            protected void populateItem(final ListItem<Requirement> item) {
                Link<Requirement> viewLink = new Link<Requirement>("registrantDocumentPage", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new RegistrantDocumentPage(examRegistrationModel.getObject(), registrantModel, getModelObject()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                item.add(new Label("name"));
                Form<SupportDocument> form = new Form<SupportDocument>("form", new CompoundPropertyModel<SupportDocument>(new SupportDocument())) {
                    @Override
                    protected void onSubmit() {
                        this.getModelObject().setRegistrant(registrantModel.getObject());
                        this.getModelObject().setRequirement(item.getModelObject());
                        this.getModelObject().setExamRegistration(examRegistrationModel.getObject());
                        this.getModelObject().setSubmitted(Boolean.TRUE);
                        setResponsePage(new RequirementConfirmationPage(examRegistrationModel.getObject()));
                    }

                    @Override
                    protected void onConfigure() {
                        if (getModel().getObject().getSubmitted()) {
                            setVisible(Boolean.FALSE);
                        } else {
                            if (item.getModelObject().getUpload()) {
                                setVisible(Boolean.TRUE);
                            } else {
                                setVisible(Boolean.FALSE);
                            }
                        }
                    }
                };
                form.add(new CheckBox("submitted"));
                item.add(form);
            }

            @Override
            protected void onConfigure() {
                if (!finalRequirementsSet.isEmpty()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(Boolean.FALSE);
                }
            }
        };

        add(eachItem);

    }

    public RequirementConfirmationPage(final Application application) {

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        final CompoundPropertyModel<Application> model = new CompoundPropertyModel<Application>(applicationService.get(application.getId()));
        setDefaultModel(model);
        final CompoundPropertyModel<Registrant> registrantModel = new CompoundPropertyModel<Registrant>(registrantService.get(application.getRegistrant().getId()));
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(model.getObject().getRegistrant().getId());
        add(new Label("registrant.fullname"));
        add(new FeedbackPanel("feedback"));

        Set<Requirement> totalRequirements = new HashSet<Requirement>();
        List<Requirement> courseRequirements = new ArrayList<Requirement>(courseService.get(application.getCourse().getId()).getRequirements());
        List<Requirement> registerRequirements = new ArrayList<Requirement>(registerService.get(applicationProcess.getApplicationRegister(application).getId()).getRequirements());
        totalRequirements.addAll(courseRequirements);
        totalRequirements.addAll(registerRequirements);

        Set<Requirement> requirements = new HashSet<Requirement>();
        for (Requirement r : totalRequirements) {
            SupportDocument document = supportDocumentService.getSupportDocument(application, r);
            if (document == null) {
                requirements.add(r);
            }
        }

        final Set<Requirement> finalRequirementsSet = new HashSet<Requirement>(requirements);
        RegistrantQualification registrantQualification = registrantQualificationService.getRegistrantQualificationByRegistrantAndCourse(model.getObject().getRegistrant(), model.getObject().getCourse());
        if (!generalParametersService.get().getHasProcessedByBoard()) {
            Set<Requirement> finalRequirementsSet1 = new HashSet<Requirement>(requirements);

            if (registrantQualification != null) {
                if (registrantQualification.getQualificationType() != null) {
                    if (registrantQualification.getQualificationType().equals(QualificationType.LOCAL)) {
                        for (Requirement r : finalRequirementsSet1) {
                            if (r.getQualificationType() != null && r.getQualificationType().equals(QualificationType.FOREIGN)) {
                                finalRequirementsSet.remove(r);
                            }
                        }
                    }
                }
            }
        }
        if (registrantQualification != null) {
            if (registrantQualification.getDateAwarded() != null) {
                if (generalParametersService.get().isRequireAffidavit()) {
                    int differenceInMonths = renewalProcess.monthsBetween(new Date(), registrantQualification.getDateAwarded());
                    if (generalParametersService.get().getAffidavit() != null && generalParametersService.get().getMonthsToRequireAffidavit() != null) {
                        if (differenceInMonths >= generalParametersService.get().getMonthsToRequireAffidavit()) {
                            finalRequirementsSet.add(generalParametersService.get().getAffidavit());
                        }

                    }
                }
            }
        }

        add(new Link<Registrant>("processPayment") {
            @Override
            public void onClick() {
                Customer customer = registrantService.get(registrantModel.getObject().getId()).getCustomerAccount();
                List<DebtComponent> debtComponents = debtComponentService.getDebtComponents(customer.getAccount());
                List<ReceiptItem> receiptItemList = receiptItemService.getReceiptItemsWithNoReceiptHeader(customer.getAccount());

                if (debtComponents.isEmpty() && !receiptItemList.isEmpty()) {
                    Set<ReceiptItem> receiptItems = new HashSet<ReceiptItem>();

                    if (receiptItemList.isEmpty()) {
                        setResponsePage(new RegistrantViewPage(registrantService.getRegistrant(customer).getId()));
                    } else {
                        for (ReceiptItem receiptItem : receiptItemList) {
                            receiptItems.add(receiptItem);
                        }
                        PaymentDetails paymentDetails = paymentProcess.accountBallancePaysAll(receiptItems, CouncilSession.get().getUser(), customer, null);

                        setResponsePage(new PaymentConfirmationPage(paymentDetails, customer.getAccount()));
                    }
                } else if (!debtComponents.isEmpty() && !receiptItemList.isEmpty()) {
                    Set<ReceiptItem> receiptItems = new HashSet<ReceiptItem>();
                    if (receiptItemList.isEmpty()) {
                        setResponsePage(new RegistrantViewPage(registrantService.getRegistrant(customer).getId()));
                    } else {
                        for (ReceiptItem receiptItem : receiptItemList) {
                            receiptItems.add(receiptItem);
                        }

                        paymentProcess.accountBalancePaysLess(receiptItems, CouncilSession.get().getUser(), customer, null);
                    }
                    setResponsePage(new DebtComponentsPage(registrantService.getRegistrant(customer).getId()));

                } else {
                    //Select the items you want to pay for
                    setResponsePage(new DebtComponentsPage(registrantService.getRegistrant(customer).getId()));
                }
            }
        });
        add(new Link<Registrant>("next") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }

            @Override
            protected void onConfigure() {
                setVisible(Boolean.TRUE);
            }
        });

        add(new Link<Registrant>("addCleranceLetter") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantClearanceEditPage(registrantModel));
            }

            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });

        add(new Link<Registrant>("registrationOptionPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });

        PropertyListView<Requirement> eachItem = new PropertyListView<Requirement>("requirementsList", new ArrayList<Requirement>(finalRequirementsSet)) {
            @Override
            protected void populateItem(final ListItem<Requirement> item) {
                Link<Requirement> viewLink = new Link<Requirement>("registrantDocumentPage", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new RegistrantDocumentPage(model.getObject(), registrantModel, getModelObject()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                item.add(new Label("name"));
                Form<SupportDocument> form = new Form<SupportDocument>("form", new CompoundPropertyModel<SupportDocument>(new SupportDocument())) {
                    @Override
                    protected void onSubmit() {
                        this.getModelObject().setRegistrant(registrantModel.getObject());
                        this.getModelObject().setRequirement(item.getModelObject());
                        this.getModelObject().setApplication(model.getObject());
                        this.getModelObject().setSubmitted(Boolean.TRUE);
                        supportDocumentService.save(getModelObject());
                        setResponsePage(new RequirementConfirmationPage(model.getObject()));
                    }

                    @Override
                    protected void onConfigure() {
                        if (getModel().getObject().getSubmitted()) {
                            setVisible(Boolean.FALSE);
                        } else {
                            if (item.getModelObject().getUpload()) {
                                setVisible(Boolean.TRUE);
                            } else {
                                setVisible(Boolean.FALSE);
                            }
                        }
                    }
                };
                form.add(new CheckBox("submitted"));
                item.add(form);
            }

            @Override
            protected void onConfigure() {
                if (!finalRequirementsSet.isEmpty()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(Boolean.FALSE);
                }
            }
        };

        add(eachItem);

    }

    public RequirementConfirmationPage(final Institution institution) {

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        final CompoundPropertyModel<Institution> model = new CompoundPropertyModel<Institution>(institutionService.get(institution.getId()));
        setDefaultModel(model);
        final CompoundPropertyModel<Registrant> registrantModel = new CompoundPropertyModel<Registrant>(registrantService.get(institution.getRegistrant().getId()));
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(model.getObject().getRegistrant().getId());
        add(new Label("registrant.fullname"));
        add(new FeedbackPanel("feedback"));

        Set<Requirement> totalRequirements = new HashSet<Requirement>();
        List<Requirement> registerRequirements = new ArrayList<Requirement>(registerService.get(generalParametersService.get().getInstitutionRegister().getId()).getRequirements());
        List<Requirement> institutionTypeRequirements = new ArrayList<Requirement>(model.getObject().getInstitutionType().getRequirements());

        totalRequirements.addAll(registerRequirements);
        totalRequirements.addAll(institutionTypeRequirements);

        Set<Requirement> requirements = new HashSet<Requirement>();
        for (Requirement r : totalRequirements) {
            SupportDocument document = supportDocumentService.getSupportDocument(model.getObject(), r);
            if (document == null) {
                requirements.add(r);
            }
        }
        final Set<Requirement> finalRequirementsSet = new HashSet<Requirement>(requirements);

        add(new Link<Registrant>("processPayment") {
            @Override
            public void onClick() {
                setResponsePage(new DebtComponentsPage(registrantModel.getObject().getId()));
            }

            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });

        add(new Link<Void>("next") {
            @Override
            public void onClick() {

                setResponsePage(new InstitutionApplicationPage(institution));

            }

            @Override
            protected void onConfigure() {
                if (finalRequirementsSet.isEmpty()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(Boolean.FALSE);
                }
            }
        });

        add(new Link<Registrant>("addCleranceLetter") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantClearanceEditPage(registrantModel));
            }

            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });

        add(new Link<Registrant>("registrationOptionPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionViewPage(institution.getId()));
            }
        });

        PropertyListView<Requirement> eachItem = new PropertyListView<Requirement>("requirementsList", new ArrayList<Requirement>(finalRequirementsSet)) {
            @Override
            protected void populateItem(final ListItem<Requirement> item) {
                Link<Requirement> viewLink = new Link<Requirement>("registrantDocumentPage", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new RegistrantDocumentPage(model.getObject(), registrantModel, getModelObject()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                item.add(new Label("name"));
                Form<SupportDocument> form = new Form<SupportDocument>("form", new CompoundPropertyModel<SupportDocument>(new SupportDocument())) {
                    @Override
                    protected void onSubmit() {
                        this.getModelObject().setRegistrant(registrantModel.getObject());
                        this.getModelObject().setRequirement(item.getModelObject());
                        this.getModelObject().setInstitution(model.getObject());
                        this.getModelObject().setSubmitted(Boolean.TRUE);
                        SupportDocument s = supportDocumentService.save(getModelObject());
                        setResponsePage(new RequirementConfirmationPage(s.getInstitution()));
                    }

                    @Override
                    protected void onConfigure() {
                        if (getModel().getObject().getSubmitted()) {
                            setVisible(Boolean.FALSE);
                        } else {
                            setVisible(Boolean.TRUE);
                        }
                    }
                };
                form.add(new CheckBox("submitted"));
                item.add(form);
            }

            @Override
            protected void onConfigure() {
                if (!finalRequirementsSet.isEmpty()) {
                    setVisible(Boolean.TRUE);
                } else {
                    setVisible(Boolean.FALSE);
                }
            }
        };

        add(eachItem);

    }
}

//~ Formatted by Jindent --- http://www.jindent.com
