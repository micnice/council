package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.TransferRule;
import zw.co.hitrac.council.business.service.TransferRuleService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;

/**
 *
 * @author Michael Matiashe
 */
public class TransferRuleListPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private TransferRuleService transferRuleService;

    public TransferRuleListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new TransferRuleEditPage(null));
            }
        });

        IModel<List<TransferRule>> model = new LoadableDetachableModel<List<TransferRule>>() {
            @Override
            protected List<TransferRule> load() {
                return transferRuleService.findAll();
            }
        };
        PropertyListView<TransferRule> eachItem = new PropertyListView<TransferRule>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<TransferRule> item) {
                Link<TransferRule> viewLink = new Link<TransferRule>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new TransferRuleViewPage(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };

        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
