/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.MisconductTypeDAO;
import zw.co.hitrac.council.business.domain.MisconductType;
import zw.co.hitrac.council.business.service.MisconductTypeService;

import java.util.List;

/**
 *
 * @author hitrac
 */
@Service
@Transactional
public class MisconductTypeServiceImpl implements MisconductTypeService{
    
    @Autowired
    private MisconductTypeDAO misconductTypeDAO;
    

    @Transactional
    public MisconductType save(MisconductType misconductType) {
       return misconductTypeDAO.save(misconductType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<MisconductType> findAll() {
        return misconductTypeDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public MisconductType get(Long id) {
       return misconductTypeDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<MisconductType> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public void setMisconductTypeDAO(MisconductTypeDAO misconductTypeDAO) {

        this.misconductTypeDAO = misconductTypeDAO;
    }
    
}
