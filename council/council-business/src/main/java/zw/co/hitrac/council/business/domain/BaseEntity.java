package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.utils.CaseUtil;

import javax.persistence.MappedSuperclass;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import javax.persistence.Transient;
import zw.co.hitrac.council.business.utils.StringFormattingUtil;

/**
 *
 * @author tdhlakama
 * @author Charles Chigoriwa
 */
@XmlAccessorType(XmlAccessType.NONE)
@MappedSuperclass
@Audited
public abstract class BaseEntity extends BaseIdEntity implements Serializable {

    private String name;
    private String description;

    @XmlElement
    public String getName() {
        return CaseUtil.upperCase(name);
    }

    @XmlElement
    @Transient
    public String getFormattedName() {

        return StringFormattingUtil.upperCaseFirst(CaseUtil.lowerCase(getName()));

    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement
    public String getDescription() {
        return CaseUtil.upperCase(description);
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return getName();
    }

}
