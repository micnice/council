
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.institution;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PageableListView;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigator;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;


//~--- JDK imports ------------------------------------------------------------

import java.util.List;
import org.apache.wicket.markup.html.link.Link;
import zw.co.hitrac.council.business.domain.Institution;

/**
 *
 * @author tdhlakama
 */
public class InstitutionDataViewPanel extends Panel {

    public InstitutionDataViewPanel(String id, IModel<List<Institution>> model) {
        super(id);

        PageableListView<Institution> eachItem = new PageableListView<Institution>("eachItem", model, 20) {
            @Override
            protected void populateItem(ListItem<Institution> item) {

                Link<Institution> viewLink = new Link<Institution>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new InstitutionViewPage(getModelObject().getId()));
                    }
                };

                item.add(viewLink);
                viewLink.add(new Label("name",item.getModelObject().getName()));
                item.add(new Label("code",item.getModelObject().getCode()));
                item.add(new Label("institutionType",item.getModelObject().getInstitutionType()));
                item.add(new Label("institutionCategory",item.getModelObject().getInstitutionCategory()));
                item.add(new Label("retired",item.getModelObject().getRetired()));
            }
        };

        add(new PagingNavigator("navigator", eachItem));
        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
