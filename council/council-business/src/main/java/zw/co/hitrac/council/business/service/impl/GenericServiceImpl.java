/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.GenericDAO;
import zw.co.hitrac.council.business.service.GenericService;

import java.util.List;

/**
 *
 * @author tdhlakama
 */
@Service
@Transactional
public class GenericServiceImpl implements GenericService {

    @Autowired
    private GenericDAO genericDAO;

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Long> getAllLean(String ClassName) {
        return genericDAO.getAllLean(ClassName);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Object getLean(String ClassName, Long id) {
        return genericDAO.getLean(ClassName, id);
    }

}
