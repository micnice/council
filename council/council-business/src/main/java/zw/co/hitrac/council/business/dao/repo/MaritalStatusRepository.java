package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.MaritalStatus;

/**
 *
 * @author Edward Zengeni
 */

    public interface MaritalStatusRepository extends CrudRepository<MaritalStatus, Long> {
    public List<MaritalStatus> findAll();
	public MaritalStatus findByName(String name);
}


