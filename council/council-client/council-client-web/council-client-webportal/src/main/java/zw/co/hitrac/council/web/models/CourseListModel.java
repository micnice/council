package zw.co.hitrac.council.web.models;

import java.util.List;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.utils.HrisComparator;

/**
 *
 * @author Charles Chigoriwa
 */
public class CourseListModel extends LoadableDetachableModel<List<Course>> {

    private final CourseService courseService;
    private HrisComparator hrisComparator = new HrisComparator();

    public CourseListModel(CourseService courseService) {
        this.courseService = courseService;
    }

    @Override
    protected List<Course> load() {
        return (List<Course>) hrisComparator.sort(courseService.findAll());
    }
}
