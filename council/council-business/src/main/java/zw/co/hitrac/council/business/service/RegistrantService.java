package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Takunda Dhlakama
 * @author Matiashe Michael
 */
public interface RegistrantService extends IGenericService<Registrant> {

    public List<RegistrantData> getRegistrantsWithBirthDays();

    public List<RegistrantData> getRegistrantsWithBirthDays(Date date);

    public List<Registrant> getApprovingRegistrants();

    public Registrant createUids(Registrant registrant);

    public void createRegistrantAccounts();

    public void updateCustomerAndAccountDetails();

    public List<Registrant> getRegistrants(String query, String gender, Citizenship citizenship, Boolean dead);

    public List<RegistrantData> getRegistrantList(String query, String gender, Citizenship citizenship, Boolean dead);

    public List<RegistrantData> getSupervisorlist(String query, Boolean dead);

    public List<Registrant> getDeRegistered();
    
    public List<RegistrantData> getVoidedRegistrants();

    public List<RegistrantData> getRegistrants();

    public List<RegistrantData> getRegistrantAgeByParameter(Integer lowerLimit, Integer upperLimit);

    public Registrant getRegistrant(Customer customer);

    public Registrant getRegistrantByAccount(Customer customer);

    public Registrant getRegistrantByAccountData(Account account);

    public Boolean getUniqueRegistrationNumber(String registrationNumber);

    public Boolean getRegistrantIdentificationNumber(String idNumber);

    public Registrant getRegistrantByRegistrationNumber(String registrationNumber);

    public Registrant getRegistrantByIdentificationNumber(String idNumber);

    public List<Long> getIds();

    public List<Registrant> getRegistrantWithDebts();

    public List<Registrant> getRegistrantWithCarryForwards();

    public Registrant activateRegistrantManually(Registrant registrant);

    public List<RegistrantData> getRegistrantCorrections();

    public List<Registrant> getRegistrantsWithNoOrInvalidNationalIDs(Citizenship citizenship,
                                                                     IdentificationType nationalIdentificationType);


    public boolean hasValidIdentifications(Registrant registrant, List<IdentificationValidationRule> validationRules);

    public Registrant findByIdNumber(String idNumber);

    public List<RegistrantData> getRegistrantDateCorrections();

    public List<RegistrantData> getDeceasedRegistrants();

    public List<RegistrantData> getRegistrantsWithFirstRenewalWithoutSecondRenewal(Course course,
                                                                                   CouncilDuration firstDuration,
                                                                                   CouncilDuration secondDuration,
                                                                                   AddressType addressType);

    public List<RegistrantData> getRegistrantsWithFirstRenewalWithoutSecondRenewalInRegistration(Course course,
                                                                                                 CouncilDuration firstDuration,
                                                                                                 CouncilDuration secondDuration,
                                                                                                 AddressType addressType);
    public List<RegistrantData> getRetiredRegistrants();

    public List<Registrant> findByCitizenship(Citizenship citizenship);

    public List<RegistrantData> getRegistrantsWithConditions(CouncilDuration councilDuration);
    
    public List<RegistrantData> getRegistrantsWithConditions(CouncilDuration councilDuration, Conditions condition);

    public List<RegistrantData> getRegistrantsWhoOwnInstitutions();

    public List<RegistrantData> getRegistrantsWhoSuperviseInstitutions();

}
