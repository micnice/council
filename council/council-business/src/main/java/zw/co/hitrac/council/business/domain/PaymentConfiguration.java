package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;

/**
 *
 * @author Kelvin Goredema
 */
@Entity
@Table(name="paymentconfiguration")
@Audited
public class PaymentConfiguration extends BaseEntity {

    private Course course;
    private Date dueDate;
    private TypeOfService typeOfService;

    public PaymentConfiguration() {
    }

    @OneToOne
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }
     @Enumerated(EnumType.STRING)
    public TypeOfService getTypeOfService() {
        return typeOfService;
    }

    public void setTypeOfService(TypeOfService typeOfService) {
        this.typeOfService = typeOfService;
    }
}
