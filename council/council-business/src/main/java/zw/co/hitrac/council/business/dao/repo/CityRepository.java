package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.City;

/**
 *
 * @author Charles Chigoriwa
 */
public interface CityRepository extends CrudRepository<City, Long> {

    public List<City> findAll();
}
