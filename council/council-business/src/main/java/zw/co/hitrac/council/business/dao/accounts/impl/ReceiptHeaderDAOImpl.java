package zw.co.hitrac.council.business.dao.accounts.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.accounts.ReceiptHeaderDAO;
import zw.co.hitrac.council.business.dao.repo.accounts.ReceiptHeaderRepository;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.TypeOfService;
import zw.co.hitrac.council.business.domain.User;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.accounts.PaymentMethod;
import zw.co.hitrac.council.business.domain.accounts.PaymentType;
import zw.co.hitrac.council.business.domain.accounts.ReceiptHeader;
import zw.co.hitrac.council.business.domain.accounts.ReceiptItem;
import zw.co.hitrac.council.business.domain.accounts.TransactionType;

/**
 * @author Michael Matiashe
 */
@Repository
public class ReceiptHeaderDAOImpl implements ReceiptHeaderDAO {

    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private ReceiptHeaderRepository receiptHeaderRepository;

    public ReceiptHeader save(ReceiptHeader t) {
        return receiptHeaderRepository.save(t);
    }

    public List<ReceiptHeader> findAll() {
        return receiptHeaderRepository.findAll();
    }

    public ReceiptHeader get(Long id) {
        return receiptHeaderRepository.findOne(id);
    }

    public List<ReceiptHeader> getReceiptHeaders(Long receiptNumber, PaymentMethod paymentMethod, PaymentType paymentType, Date startDate, Date endDate, User user, TransactionType transactionType, Boolean reversed) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ReceiptHeader.class);
        if (receiptNumber != null) {
            criteria.add(Restrictions.or(Restrictions.eq("id", receiptNumber), Restrictions.eq("generatedReceiptNumber", receiptNumber)));
        }
        if (reversed != null) {
            criteria.add(Restrictions.eq("reversed", reversed));
        }
        //code used for both cancelled and none cancelled Receipts
        if (reversed != null) {
            if (!reversed) {
                if (startDate != null && endDate != null) {
                    criteria.add(Restrictions.between("date", startDate, endDate));
                } else if (startDate != null && endDate == null) {
                    criteria.add(Restrictions.between("date", startDate, new Date()));
                }
                if (user != null) {
                    criteria.add(Restrictions.eq("createdBy", user));
                }
            } else {
                if (startDate != null && endDate != null) {
                    criteria.add(Restrictions.between("canceledDate", startDate, endDate));
                } else if (startDate != null && endDate == null) {
                    criteria.add(Restrictions.between("canceledDate", startDate, new Date()));
                }
                if (user != null) {
                    criteria.add(Restrictions.eq("canceledBy", user));
                }
            }
        }

        if (transactionType != null) {
            criteria.add(Restrictions.eq("transactionType", transactionType));
        }

        criteria.createAlias("paymentDetails", "r");
        if (paymentMethod != null) {
            criteria.add(Restrictions.eq("r.paymentMethod", paymentMethod));
        }
        if (paymentType != null) {
            criteria.add(Restrictions.eq("r.paymentType", paymentType));
        }
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();

    }

    public List<ReceiptHeader> getReceiptHeadersDeposit(Long receiptNumber, PaymentMethod paymentMethod, PaymentType paymentType, Date startDate, Date endDate, User user, TransactionType transactionType, Boolean reversed) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ReceiptHeader.class);
        if (receiptNumber != null) {
            criteria.add(Restrictions.or(Restrictions.eq("id", receiptNumber), Restrictions.eq("generatedReceiptNumber", receiptNumber)));
        }
        if (user != null) {
            criteria.add(Restrictions.eq("createdBy", user));
        }
        if (transactionType != null) {
            criteria.add(Restrictions.eq("transactionType", transactionType));
        }
        if (reversed != null) {
            criteria.add(Restrictions.eq("reversed", reversed));
        }
        criteria.createAlias("paymentDetails", "r");
        if (paymentMethod != null) {
            criteria.add(Restrictions.eq("r.paymentMethod", paymentMethod));
        }
        if (startDate != null && endDate != null) {
            criteria.add(Restrictions.between("r.dateOfDeposit", startDate, endDate));
        } else if (startDate != null && endDate == null) {
            criteria.add(Restrictions.between("r.dateOfDeposit", startDate, new Date()));
        }
        if (paymentType != null) {
            criteria.add(Restrictions.eq("r.paymentType", paymentType));
        }
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();

    }

    public List<ReceiptHeader> getReceiptHeadersAllDates(PaymentMethod paymentMethod, PaymentType paymentType, Date startDate, Date endDate, Date depositStartDate, Date depositEndDate, User user, TransactionType transactionType, Boolean reversed) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ReceiptHeader.class);
        if (user != null) {
            criteria.add(Restrictions.eq("createdBy", user));
        }
        if (transactionType != null) {
            criteria.add(Restrictions.eq("transactionType", transactionType));
        }
        if (reversed != null) {
            criteria.add(Restrictions.eq("reversed", reversed));
        }
        criteria.createAlias("paymentDetails", "r");
        if (paymentMethod != null) {
            criteria.add(Restrictions.eq("r.paymentMethod", paymentMethod));
        }
        if (startDate != null && endDate != null) {
            criteria.add(Restrictions.between("date", startDate, endDate));
        } else if (startDate != null && endDate == null) {
            criteria.add(Restrictions.between("date", startDate, new Date()));
        }
        if (depositStartDate != null && depositEndDate != null) {
            criteria.add(Restrictions.between("r.dateOfDeposit", depositStartDate, depositEndDate));
        } else if (depositStartDate != null && depositEndDate == null) {
            criteria.add(Restrictions.between("r.dateOfDeposit", depositStartDate, new Date()));
        }
        if (paymentType != null) {
            criteria.add(Restrictions.eq("r.paymentType", paymentType));
        }
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();

    }

    public List<ReceiptHeader> getCarryForwardsList(Date startDate, Date endDate, User user, TransactionType transactionType, Boolean utilised) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ReceiptHeader.class);
        if (user != null) {
            criteria.add(Restrictions.eq("createdBy", user));
        }
        if (transactionType != null) {
            criteria.add(Restrictions.eq("transactionType", transactionType));
        }
        if (startDate != null && endDate != null) {
            criteria.add(Restrictions.between("date", startDate, endDate));
        } else if (startDate != null && endDate == null) {
            criteria.add(Restrictions.between("date", startDate, new Date()));
        }
        if (utilised) {
            criteria.add(Restrictions.le("carryForward", BigDecimal.ZERO));
        } else {
            criteria.add(Restrictions.gt("carryForward", BigDecimal.ZERO));
        }
        criteria.add(Restrictions.eq("reversed", Boolean.FALSE));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    public Integer getTotalNumberOfReceipts(PaymentMethod paymentMethod, Date startDate, Date endDate, User user, TransactionType transactionType, Boolean reversed) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ReceiptHeader.class);
        ProjectionList p = Projections.projectionList().add(Projections.rowCount());
        Conjunction exist = (Conjunction) Restrictions.conjunction();
        if (user != null) {
            exist.add(Restrictions.eq("createdBy", user));
        }
        if (transactionType != null) {
            exist.add(Restrictions.eq("transactionType", transactionType));
        }
        if (reversed != null) {
            exist.add(Restrictions.eq("reversed", reversed));
        }
        criteria.createAlias("paymentDetails", "r");
        if (paymentMethod != null) {
            exist.add(Restrictions.eq("r.paymentMethod", paymentMethod));
        }
        if (startDate != null && endDate != null) {
            exist.add(Restrictions.between("date", startDate, endDate));
        } else if (startDate != null && endDate == null) {
            exist.add(Restrictions.between("date", startDate, new Date()));
        }
        criteria.setProjection(p).add(exist);
        return ((Long) criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).uniqueResult()).intValue();
    }

    public BigDecimal getCarryForwardBalance(Date endDate, Boolean utilised, Customer customer) {
        BigDecimal balance = BigDecimal.ZERO;
        if (utilised && customer != null) {//used
            balance = (BigDecimal) entityManager.createQuery("select DISTINCT sum(r.carryForward) from ReceiptHeader r where r.date <= :endDate and r.carryForward < 0.0 and r.paymentDetails.customer=:customer and r.reversed = false").setParameter("endDate", endDate).setParameter("customer", customer).getSingleResult();
        }
        if (utilised && customer == null) {//used
            balance = (BigDecimal) entityManager.createQuery("select DISTINCT sum(r.carryForward) from ReceiptHeader r where r.date <= :endDate and r.carryForward < 0.0 and r.reversed = false").setParameter("endDate", endDate).getSingleResult();
        }
        if (!utilised && customer != null) { //collected
            balance = (BigDecimal) entityManager.createQuery("select DISTINCT sum(r.carryForward) from ReceiptHeader r where r.date <= :endDate and r.carryForward > 0.0 and r.paymentDetails.customer=:customer and r.reversed = false").setParameter("endDate", endDate).setParameter("customer", customer).getSingleResult();
        }
        if (!utilised && customer == null) {//collected
            balance = (BigDecimal) entityManager.createQuery("select DISTINCT sum(r.carryForward) from ReceiptHeader r where r.date <= :endDate and r.carryForward > 0.0 and r.reversed = false").setParameter("endDate", endDate).getSingleResult();
        }
        //if return zero might be an error
        if (balance == null) {
            return BigDecimal.ZERO;
        } else {
            return balance;
        }
    }

    public BigDecimal getCarryForwardBalance(Date endDate, Boolean utilised, Customer customer, TransactionType transactionType) {

        if (transactionType == null) {
            return getCarryForwardBalance(endDate, utilised, customer);
        }
        BigDecimal balance = BigDecimal.ZERO;
        if (utilised && customer != null) {//used
            balance = (BigDecimal) entityManager.createQuery("select DISTINCT sum(r.carryForward) from ReceiptHeader r where r.date <= :endDate and r.carryForward < 0.0 and r.paymentDetails.customer=:customer and r.reversed = false and r.transactionType=:transactionType").setParameter("endDate", endDate).setParameter("customer", customer).setParameter("transactionType", transactionType).getSingleResult();
        }
        if (utilised && customer == null) {//used
            balance = (BigDecimal) entityManager.createQuery("select DISTINCT sum(r.carryForward) from ReceiptHeader r where r.date <= :endDate and r.carryForward < 0.0 and r.reversed = false and r.transactionType=:transactionType").setParameter("endDate", endDate).setParameter("transactionType", transactionType).getSingleResult();
        }
        if (!utilised && customer != null) { //collected
            balance = (BigDecimal) entityManager.createQuery("select DISTINCT sum(r.carryForward) from ReceiptHeader r where r.date <= :endDate and r.carryForward > 0.0 and r.paymentDetails.customer=:customer and r.reversed = false and r.transactionType=:transactionType").setParameter("endDate", endDate).setParameter("customer", customer).setParameter("transactionType", transactionType).getSingleResult();
        }
        if (!utilised && customer == null) {//collected
            balance = (BigDecimal) entityManager.createQuery("select DISTINCT sum(r.carryForward) from ReceiptHeader r where r.date <= :endDate and r.carryForward > 0.0 and r.reversed = false and r.transactionType=:transactionType").setParameter("endDate", endDate).setParameter("transactionType", transactionType).getSingleResult();
        }
        //if return zero might be an error
        if (balance == null) {
            return BigDecimal.ZERO;
        } else {
            return balance;
        }
    }

    public Long getNumberOfReceipts(Date endDate, User user) {
        return (Long) entityManager.createQuery("select DISTINCT COUNT (r.id) from ReceiptHeader r where r.date=:endDate and r.reversed = false and r.createdBy=:user").setParameter("user", user).setParameter("endDate", endDate).getSingleResult();
    }

    public List<ReceiptHeader> getRegistrantCarryForwardCollected(Date endDate) {
        return entityManager.createQuery("select DISTINCT r from ReceiptHeader r where r.date <= :endDate and r.carryForward > 0.0 and r.reversed = false").setParameter("endDate", endDate).getResultList();
    }

    public List<ReceiptHeader> getRegistrantCarryForwardCollected(Date endDate, TransactionType transactionType) {
        if (transactionType == null) {
            return getRegistrantCarryForwardCollected(endDate);
        } else {
            return entityManager.createQuery("select DISTINCT r from ReceiptHeader r where r.date <= :endDate and r.carryForward > 0.0 and r.reversed = false and r.transactionType =:transactionType")
                    .setParameter("endDate", endDate)
                    .setParameter("transactionType", transactionType).getResultList();
        }
    }


    public List<ReceiptHeader> getCarryForwardBalanceList(Date endDate, Boolean utilised) {
        if (utilised) {//true all used from system 
            return entityManager.createQuery("select DISTINCT r from ReceiptHeader r where r.date <= :endDate and r.carryForward < 0.0 and r.reversed = false").setParameter("endDate", endDate).getResultList();
        } else {//all collected 
            return entityManager.createQuery("select DISTINCT r from ReceiptHeader r where r.date <= :endDate and r.carryForward > 0.0 and r.reversed = false").setParameter("endDate", endDate).getResultList();
        }
    }

    public List<ReceiptHeader> getCarryForwardBalanceList(Date endDate, Boolean utilised, TransactionType transactionType) {
        if (transactionType != null) {
            if (utilised) {//true all used from system
                return entityManager.createQuery("select DISTINCT r from ReceiptHeader r where r.date <= :endDate and r.carryForward < 0.0 and r.reversed = false and r.transactionType=:transactionType")
                        .setParameter("endDate", endDate)
                        .setParameter("transactionType", transactionType)
                        .getResultList();
            } else {//all collected
                return entityManager.createQuery("select DISTINCT r from ReceiptHeader r where r.date <= :endDate and r.carryForward > 0.0 and r.reversed = false and r.transactionType=:transactionType")
                        .setParameter("endDate", endDate)
                        .setParameter("transactionType", transactionType)
                        .getResultList();
            }
        } else {
            return getCarryForwardBalanceList(endDate, utilised);
        }
    }


    public List<ReceiptHeader> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<ReceiptHeader> getChangedBanksList(TransactionType transactionTypeChange, Date startDate, Date endDate) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ReceiptHeader.class);
        if (transactionTypeChange == null && startDate == null) {
            return new ArrayList<ReceiptHeader>();
        }
        criteria.add(Restrictions.isNotNull("transactionTypeChange"));
        if (transactionTypeChange != null) {
            criteria.add(Restrictions.eq("transactionTypeChange", transactionTypeChange));
        }
        if (startDate != null && endDate != null) {
            criteria.add(Restrictions.between("date", startDate, endDate));
        } else if (startDate != null && endDate == null) {
            criteria.add(Restrictions.between("date", startDate, new Date()));
        }
        criteria.add(Restrictions.eq("reversed", Boolean.FALSE));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    public List<ReceiptHeader> getReceiptListComments() {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ReceiptHeader.class);
        criteria.add(Restrictions.or(Restrictions.ne("comment", ""), Restrictions.or(Restrictions.isNotNull("comment"))));
        criteria.add(Restrictions.eq("reversed", Boolean.FALSE));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    public List<ReceiptHeader> getReceiptHeaderByCouncilDurationAndAccountAndTypeOfService(CouncilDuration councilDuration, Account account, TypeOfService typeOfService) {
        return entityManager.createQuery("SELECT distinct r from ReceiptHeader r join r.receiptItems rc where r.paymentDetails.councilDuration=:councilDuration AND rc.debtComponent.product.typeOfService=:typeOfService AND r.reversed=FALSE AND r.paymentDetails.customer.account=:account")
                .setParameter("councilDuration", councilDuration)
                .setParameter("account", account)
                .setParameter("typeOfService", typeOfService)
                .getResultList();
    }

    public List<CouncilDuration> getCouncilDurationsfromRecieptHeaders(Account account, TypeOfService typeOfService) {
        return entityManager.createQuery("SELECT distinct r.paymentDetails.councilDuration from ReceiptHeader r join r.receiptItems rc where rc.debtComponent.product.typeOfService=:typeOfService AND r.reversed=FALSE AND r.paymentDetails.customer.account=:account")
                .setParameter("account", account)
                .setParameter("typeOfService", typeOfService)
                .getResultList();
    }


    public List<ReceiptHeader> getRegistrantCarryForwardCollectedForReport(Date startDate, Date endDate) {


        return entityManager.createQuery("select DISTINCT r from ReceiptHeader r where r.carryForward < 0.0 and(r.date between :startDate and :endDate) and r.reversed = false").setParameter("startDate", startDate).setParameter("endDate", endDate).getResultList();
    }

    public List<ReceiptHeader> getRegistrantCarryForwardCollectedForReport(Date startDate, Date endDate, TransactionType transactionType) {
        if (transactionType == null) {
            return getRegistrantCarryForwardCollectedForReport(startDate, endDate);
        } else {

            return entityManager.createQuery("select DISTINCT r from ReceiptHeader r where r.date BETWEEN :startDate and :endDate and r.carryForward < 0.0 and r.reversed = false and r.transactionType=:transactionType")
                    .setParameter("startDate", startDate)
                    .setParameter("endDate", endDate)
                    .setParameter("transactionType", transactionType).getResultList();
        }
    }


    public List<ReceiptHeader> getCarryForwardBalanceListForReport(Date startDate, Date endDate, Boolean utilised) {
        if (utilised) {//true all used from system

            System.out.println("startDate is :" + startDate + "endDate is :" + endDate);

            return entityManager.createQuery("select DISTINCT r from ReceiptHeader r where r.date <= :endDate and r.carryForward < 0.0 and r.reversed = false").setParameter("endDate", endDate).getResultList();
        } else {//all collected
            return entityManager.createQuery("select DISTINCT r from ReceiptHeader r where r.date <= :endDate and r.carryForward > 0.0 and r.reversed = false").setParameter("endDate", endDate).getResultList();
        }
    }

    public List<ReceiptHeader> getCarryForwardBalanceListForReport(Date startDate, Date endDate, Boolean utilised, TransactionType transactionType) {
        if (transactionType != null) {

            if (utilised) {//true all used from system
                return entityManager.createQuery("select DISTINCT r from ReceiptHeader r where r.date between :startDate and :endDate and r.carryForward < 0.0 and r.reversed = false and r.transactionType=:transactionType")
                        .setParameter("endDate", endDate)
                        .setParameter("startDate", startDate)
                        .setParameter("transactionType", transactionType)
                        .getResultList();
            } else {//all collected
                return entityManager.createQuery("select DISTINCT r from ReceiptHeader r where r.date between :startDate and :endDate and r.carryForward > 0.0 and r.reversed = false and r.transactionType=:transactionType")
                        .setParameter("endDate", endDate)
                        .setParameter("startDate", startDate)
                        .setParameter("transactionType", transactionType)
                        .getResultList();
            }
        } else {

            return getCarryForwardBalanceListForReport(startDate, endDate, utilised);
        }
    }


    public BigDecimal getCarryForwardBalanceForReport(Date startDate, Date endDate, Boolean utilised, Customer customer) {
        BigDecimal balance = BigDecimal.ZERO;
        if (utilised && customer != null) {//used

            System.out.println("startDate is :" + startDate + "endDate is :" + endDate);

            balance = (BigDecimal) entityManager.createQuery("select DISTINCT sum(r.carryForward) from ReceiptHeader r where r.date between :startDate and :endDate and r.carryForward < 0.0 and r.paymentDetails.customer=:customer and r.reversed = false").setParameter("endDate", endDate).setParameter("startDate", startDate).setParameter("customer", customer).getSingleResult();
        }
        if (utilised && customer == null) {//used
            balance = (BigDecimal) entityManager.createQuery("select DISTINCT sum(r.carryForward) from ReceiptHeader r where r.date between :startDate and :endDate and r.carryForward < 0.0 and r.reversed = false").setParameter("endDate", endDate).setParameter("startDate", startDate).getSingleResult();
        }
        if (!utilised && customer != null) { //collected
            balance = (BigDecimal) entityManager.createQuery("select DISTINCT sum(r.carryForward) from ReceiptHeader r where r.date between :startDate and :endDate and r.carryForward > 0.0 and r.paymentDetails.customer=:customer and r.reversed = false").setParameter("endDate", endDate).setParameter("startDate", startDate).setParameter("customer", customer).getSingleResult();
        }
        if (!utilised && customer == null) {//collected
            balance = (BigDecimal) entityManager.createQuery("select DISTINCT sum(r.carryForward) from ReceiptHeader r where r.date between :startDate and :endDate and r.carryForward > 0.0 and r.reversed = false").setParameter("endDate", endDate).setParameter("startDate", startDate).setParameter("startDate", startDate).getSingleResult();
        }
        //if return zero might be an error
        if (balance == null) {
            return BigDecimal.ZERO;
        } else {
            return balance;
        }
    }

    public BigDecimal getCarryForwardBalanceForReport(Date startDate, Date endDate, Boolean utilised, Customer customer, TransactionType transactionType) {

        if (transactionType == null) {
            return getCarryForwardBalanceForReport(startDate, endDate, utilised, customer);
        }
        BigDecimal balance = BigDecimal.ZERO;
        if (utilised && customer != null) {//used
            balance = (BigDecimal) entityManager.createQuery("select DISTINCT sum(r.carryForward) from ReceiptHeader r where r.date between :startDate and :endDate and r.carryForward < 0.0 and r.paymentDetails.customer=:customer and r.reversed = false and r.transactionType=:transactionType").setParameter("endDate", endDate).setParameter("startDate", startDate).setParameter("customer", customer).setParameter("transactionType", transactionType).getSingleResult();
        }
        if (utilised && customer == null) {//used
            balance = (BigDecimal) entityManager.createQuery("select DISTINCT sum(r.carryForward) from ReceiptHeader r where r.date between :startDate and :endDate and r.carryForward < 0.0 and r.reversed = false and r.transactionType=:transactionType").setParameter("endDate", endDate).setParameter("startDate", startDate).setParameter("transactionType", transactionType).getSingleResult();
        }
        if (!utilised && customer != null) { //collected
            balance = (BigDecimal) entityManager.createQuery("select DISTINCT sum(r.carryForward) from ReceiptHeader r where r.date between :startDate and :endDate and r.carryForward > 0.0 and r.paymentDetails.customer=:customer and r.reversed = false and r.transactionType=:transactionType").setParameter("startDate", startDate).setParameter("endDate", endDate).setParameter("customer", customer).setParameter("transactionType", transactionType).getSingleResult();
        }
        if (!utilised && customer == null) {//collected
            balance = (BigDecimal) entityManager.createQuery("select DISTINCT sum(r.carryForward) from ReceiptHeader r where r.date between :startDate and :endDate and r.carryForward > 0.0 and r.reversed = false and r.transactionType=:transactionType").setParameter("endDate", endDate).setParameter("startDate", startDate).setParameter("transactionType", transactionType).getSingleResult();
        }
        //if return zero might be an error
        if (balance == null) {
            return BigDecimal.ZERO;
        } else {
            return balance;
        }
    }

    public void correctMissingChangeOfBankReport() {
        List<ReceiptHeader> receiptHeaders = entityManager.createQuery("SELECT distinct r from ReceiptHeader r where r.dateModified is not null and r.comment is not null")
                .getResultList();

        receiptHeaders.forEach(r ->
        {

            if (r.getTransactionTypeChange() == null && !r.getTransactionType().equals(r.getPaymentDetails().getTransactionHeader().getTransactionType())) {
                r.setTransactionTypeChange(r.getPaymentDetails().getTransactionHeader().getTransactionType());
                r.setModifiedBy(r.getModifiedBy());
                save(r);
            }

        });

    }


}
