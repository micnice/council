package zw.co.hitrac.council.web.pages;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.devutils.stateless.StatelessComponent;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.StatelessForm;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import zw.co.hitrac.council.business.domain.GeneralParameters;
import zw.co.hitrac.council.business.domain.WebUser;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.WebUserService;
import zw.co.hitrac.council.web.pages.research.MyDynamicImageResource;

import java.util.Optional;

/**
 * Web User Login - for registrants in the system
 */
public class WebUserPortalLoginPage extends WebUserTemplatePage {

    @SpringBean
    private WebUserService webUserService;
    private static final Logger logger = LoggerFactory.getLogger(WebUserPortalLoginPage.class);

    public WebUserPortalLoginPage() {
        add(new SignInForm("signInForm"));
    }

    /**
     * Sign in form
     */
    public final class SignInForm extends StatelessForm<Void> {

        private transient String email = "";
        private transient String password = "";

        /**
         * Constructor
         *
         * @param id id of the form component
         */
        public SignInForm(final String id) {
            super(id);
            add(new FeedbackPanel("errorMessage"));

            add(new TextField<String>("email", new PropertyModel<>(this, "email")));
            add(new PasswordTextField("password", new PropertyModel<>(this, "password")));

            add(new BookmarkablePageLink<Void>("register", WebUserRegistrationPage.class));
            
             add(new BookmarkablePageLink<Void>("forgotPasswordPage", WebUserForgotPasswordPage.class));
      
        }

        /**
         * @see org.apache.wicket.markup.html.form.Form#onSubmit()
         */
        @Override
        public final void onSubmit() {
            // Sign the user in
            Optional<WebUser> userOptional = Optional.ofNullable(webUserService.get(email, password));
            if (userOptional.isPresent()) {
                final String registrationNumber = userOptional.get().getRegistrantNumber();
                setResponsePage(new RegistrantWebUserPage(registrationNumber));
            } else {
                // Get the error message from the properties file associated with the Component
                String errmsg = getString("loginError", null, "Unable to sign you in");
                // Register the error message with the feedback panel
                error(errmsg);
            }
        }

    }
}