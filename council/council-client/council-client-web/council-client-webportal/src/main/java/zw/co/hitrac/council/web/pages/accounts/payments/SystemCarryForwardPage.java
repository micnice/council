package zw.co.hitrac.council.web.pages.accounts.payments;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.accounts.ReceiptHeader;
import zw.co.hitrac.council.business.domain.accounts.TransactionType;
import zw.co.hitrac.council.business.domain.reports.ProductSale;
import zw.co.hitrac.council.business.domain.reports.RegistrantBalance;
import zw.co.hitrac.council.business.service.PivotDataService;
import zw.co.hitrac.council.business.service.accounts.AccountService;
import zw.co.hitrac.council.business.service.accounts.CustomerService;
import zw.co.hitrac.council.business.service.accounts.ReceiptHeaderService;
import zw.co.hitrac.council.business.service.accounts.TransactionTypeService;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.reports.BalanceReport;
import zw.co.hitrac.council.reports.RegistrantBalanceReport;
import zw.co.hitrac.council.reports.SaleBankReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.BankAccountsModel;
import zw.co.hitrac.council.web.pages.accounts.IAccountingPage;
import zw.co.hitrac.council.web.utility.DatePickerUtil;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 * @author tdhlakama
 */
public class SystemCarryForwardPage extends IAccountingPage {

    private Date startDate, endDate;
    private TransactionType transactionType;
    @SpringBean
    private ReceiptHeaderService receiptHeaderService;
    @SpringBean
    private CustomerService customerService;
    @SpringBean
    private AccountService accountService;
    private BigDecimal balance = BigDecimal.ZERO;
    @SpringBean
    private TransactionTypeService transactionTypeService;
    @SpringBean
    private PivotDataService pivotDataService;

    public SystemCarryForwardPage() {
        PropertyModel<Date> endDateModel = new PropertyModel<Date>(this, "endDate");
        PropertyModel<Date> startDateModel = new PropertyModel<>(this, "startDate");
        PropertyModel<BigDecimal> balanceModel = new PropertyModel<BigDecimal>(this, "balance");
        PropertyModel<TransactionType> transactionTypePropertyModel = new PropertyModel<>(this, "transactionType");
        Form<?> form = new Form("form");
        form.add(new TextField<Date>("startDate", startDateModel).add(DatePickerUtil.getDatePicker()));
        form.add(new TextField<Date>("endDate", endDateModel).add(DatePickerUtil.getDatePicker()));
        form.add(new DropDownChoice<TransactionType>("transactionType", transactionTypePropertyModel, new BankAccountsModel(transactionTypeService)).setNullValid(true));

        form.add(new Button("print") {
            @Override
            public void onSubmit() {

                if (endDate == null) {
                    endDate = new Date();
                }
                BigDecimal used = receiptHeaderService.getCarryForwardBalanceForReport(startDate, endDate, Boolean.TRUE, null, transactionType);
                BigDecimal collected = receiptHeaderService.getCarryForwardBalanceForReport(startDate, endDate, Boolean.FALSE, null, transactionType);
                balance = BigDecimal.ZERO;
                balance = collected.add(used);
            }
        });

        form.add(new Button("used") {
            @Override
            public void onSubmit() {
                String commnet = "";
                if (endDate == null) {
                    endDate = new Date();
                    commnet = "Used Carry Forward as Of " + DateUtil.getDate(endDate);
                }

                List<ProductSale> productSales = getProductSales(startDate, endDate, Boolean.TRUE, transactionType);
                printByNameReport(productSales, commnet);
            }
        });

        form.add(new Button("collected") {
            @Override
            public void onSubmit() {
                String commnet = "";

                if (endDate == null) {
                    endDate = new Date();
                    commnet = "Carry Forward Collected as Of " + DateUtil.getDate(endDate) + (transactionType != null ? " for" + transactionType.toString() : "");

                }
                List<ProductSale> productSales = getProductSales(startDate, endDate, Boolean.FALSE, transactionType);
                printByNameReport(productSales, commnet);
            }
        });

        form.add(new Button("creditors") {
            @Override
            public void onSubmit() {
                try {
                    if (endDate == null) {
                        endDate = new Date();
                    }

                    Set<RegistrantBalance> finalbalances =
                            getRegistrantBalanceList(startDate, endDate, transactionType)
                                    .stream().filter(rb -> rb.getBalanceRemaining().doubleValue() != new Double("0.00")).collect(Collectors.toSet());

                    BalanceReport registrantBalanceReport = new BalanceReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();
                    parameters.put("comment", "Carry Forward Remaining In System " + DateUtil.convertDateToString(endDate) + (transactionType != null ? " for " + transactionType.toString() : ""));

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(registrantBalanceReport,
                            contentType, HrisComparator.sortProductSaleByCustomerName(finalbalances.stream().collect(Collectors.toList())), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);
                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Button("creditorsUsage") {
            @Override
            public void onSubmit() {
                try {
                    if (endDate == null) {
                        endDate = new Date();
                    }
                    Set<RegistrantBalance> finalbalances = getRegistrantBalanceList(startDate, endDate, transactionType);
                    RegistrantBalanceReport registrantBalanceReport = new RegistrantBalanceReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();
                    parameters.put("comment", "Carry Forward Collected and Used As of " + DateUtil.convertDateToString(endDate) + (transactionType != null ? " for " + transactionType.toString() : ""));

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(registrantBalanceReport,
                            contentType, HrisComparator.sortProductSaleByCustomerName(finalbalances.stream().collect(Collectors.toList())), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);
                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

        form.add(new Label("balance", balanceModel));


        form.add(new Link("usedCSV") {

            @Override
            public void onClick() {

                if (endDate == null) {
                    endDate = new Date();
                }

                List<ProductSale> productSales = getProductSales(startDate, endDate, Boolean.TRUE, transactionType);

                byte[] bytes = pivotDataService.exportCarryForward(productSales);

                ReportResourceUtils.respond(bytes, "Used.csv", getRequestCycle(), ContentType.CSV);
            }

        });

        form.add(new Link("usageCSV") {

            @Override
            public void onClick() {

                if (endDate == null) {
                    endDate = new Date();
                }

                Set<RegistrantBalance> finalbalances = getRegistrantBalanceList(startDate, endDate, transactionType);

                byte[] bytes = pivotDataService.exportCarryForwardUsage(finalbalances.stream().collect(Collectors.toList()));

                ReportResourceUtils.respond(bytes, "Usage.csv", getRequestCycle(), ContentType.CSV);
            }

        });

        add(new FeedbackPanel("feedback"));
        add(form);
    }

    private void printByNameReport(List<ProductSale> productSales, String comment) {
        try {
            SaleBankReport saleBankReport = new SaleBankReport();
            ContentType contentType = ContentType.PDF;
            Map parameters = new HashMap();

            parameters.put("comment", comment);

            parameters.put("bank", (transactionType != null ? transactionType.toString() : " Carry Forward"));

            ByteArrayResource resource = ReportResourceUtils.getReportResource(saleBankReport,
                    contentType, HrisComparator.sortProductSaleByCustomerName(productSales), parameters);
            IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                    RequestCycle.get().getResponse(), null);

            resource.respond(a);

            // To make Wicket stop processing form after sending response
            RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
        } catch (JRException ex) {
            ex.printStackTrace();
        }
    }

    public List<ProductSale> getProductSales(Date startDate, Date endDate, Boolean usage, TransactionType transactionType) {

        List<ReceiptHeader> receiptHeaders = receiptHeaderService.getCarryForwardBalanceListForReport(startDate, endDate, usage, transactionType);
        return receiptHeaders.stream().map(r -> {
            ProductSale productSale = new ProductSale();
            productSale.setPaymentDetail(r.getPaymentDetails());
            productSale.setAmountPaid(r.getTotalAmountPaid());
            productSale.setCarryForward(r.getCarryForward());
            final Registrant registrant = customerService.getRegistrant(r.getPaymentDetails().getCustomer().getAccount());
            if (registrant != null) {
                productSale.setRegistrant(registrant);
            } else {
                Institution institution = customerService.getInstitution(r.getPaymentDetails().getCustomer().getAccount());
                productSale.setInstitution(institution);
            }
            return productSale;
        }).collect(Collectors.toList());

    }

    public Set<RegistrantBalance> getRegistrantBalanceList(Date startDate, Date endDate, TransactionType transactionType) {

        Set<RegistrantBalance> balances = new HashSet<RegistrantBalance>();

        List<ReceiptHeader> receiptHeaders = receiptHeaderService.getRegistrantCarryForwardCollectedForReport(startDate, endDate, transactionType);
        return receiptHeaders.stream().map(r -> {
            RegistrantBalance rb = new RegistrantBalance();
            Registrant registrant = customerService.getRegistrant(r.getPaymentDetails().getCustomer().getAccount());
            if (registrant != null) {
                rb.setRegistrant(registrant);
                rb.setAmountUsed(receiptHeaderService.getCarryForwardBalanceForReport(startDate, endDate, Boolean.TRUE, rb.getRegistrant().getCustomerAccount(), transactionType));
                rb.setCarryforward(receiptHeaderService.getCarryForwardBalanceForReport(startDate, endDate, Boolean.FALSE, rb.getRegistrant().getCustomerAccount(), transactionType));

            } else {
                Institution institution = customerService.getInstitution(r.getPaymentDetails().getCustomer().getAccount());
                rb.setInstitution(institution);
                rb.setAmountUsed(receiptHeaderService.getCarryForwardBalanceForReport(startDate, endDate, Boolean.TRUE, rb.getInstitution().getCustomerAccount(), transactionType));
                rb.setCarryforward(receiptHeaderService.getCarryForwardBalanceForReport(startDate, endDate, Boolean.FALSE, rb.getInstitution().getCustomerAccount(), transactionType));

            }
            rb.setCustomerAccount(r.getPaymentDetails().getCustomer());
            return rb;
        }).collect(Collectors.toSet());
    }
}
