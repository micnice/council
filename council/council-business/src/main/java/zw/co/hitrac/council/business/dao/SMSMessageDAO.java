package zw.co.hitrac.council.business.dao;

import zw.co.hitrac.council.business.domain.SMSMessage;

/**
 *
 * @author Michael Matiashe
 */
public interface SMSMessageDAO extends IGenericDAO<SMSMessage> {
}
