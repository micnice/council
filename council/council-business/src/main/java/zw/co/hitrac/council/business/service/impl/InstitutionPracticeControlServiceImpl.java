package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.InstitutionPracticeControlDAO;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.InstitutionPracticeControl;
import zw.co.hitrac.council.business.service.InstitutionPracticeControlService;

import java.util.List;

/**
 *
 * @author Constance Mabaso
 */
@Service
@Transactional
public class InstitutionPracticeControlServiceImpl implements InstitutionPracticeControlService {

    @Autowired
    private InstitutionPracticeControlDAO institutionPracticeControlDAO;

    @Transactional
    public InstitutionPracticeControl save(InstitutionPracticeControl institutionPracticeControl) {
        return institutionPracticeControlDAO.save(institutionPracticeControl);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<InstitutionPracticeControl> findAll() {
        return institutionPracticeControlDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public InstitutionPracticeControl get(Long id) {
        return institutionPracticeControlDAO.get(id);
    }

    public List<InstitutionPracticeControl> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setInstitutionPracticeControlDAO(InstitutionPracticeControlDAO institutionPracticeControlDAO) {
        this.institutionPracticeControlDAO = institutionPracticeControlDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<InstitutionPracticeControl> get(Application application) {
        return institutionPracticeControlDAO.get(application);
    }
}
