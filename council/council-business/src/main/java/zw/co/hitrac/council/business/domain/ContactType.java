package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author charlesc
 */
@Entity
@Table(name="contacttype")
@Audited
@XmlRootElement
public class ContactType extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;   
}
