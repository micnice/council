package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.TitleDAO;
import zw.co.hitrac.council.business.dao.repo.TitleRepository;
import zw.co.hitrac.council.business.domain.Title;

/**
 *
 * @author tidza
 */
@Repository
public class TitleDAOImpl implements TitleDAO {

    @Autowired
    private TitleRepository titleRepository;

    public Title save(Title t) {
        return titleRepository.save(t);
    }

    public List<Title> findAll() {
        return titleRepository.findAll();
    }

    public Title get(Long id) {
        return titleRepository.findOne(id);
    }

    public void setTitleRepository(TitleRepository titleRepository) {
        this.titleRepository = titleRepository;
    }

    public List<Title> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

	@Override
	public Title findByName(String name) {
		return titleRepository.findByName(name);
	}
}
