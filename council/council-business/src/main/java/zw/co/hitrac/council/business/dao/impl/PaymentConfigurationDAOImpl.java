
package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.PaymentConfigurationDAO;
import zw.co.hitrac.council.business.dao.repo.accounts.PaymentConfigurationRepository;
import zw.co.hitrac.council.business.domain.PaymentConfiguration;

/**
 *
 * @author Kelvin Goredema
 */
@Repository
public class PaymentConfigurationDAOImpl implements PaymentConfigurationDAO{
      @Autowired
    private PaymentConfigurationRepository paymentConfigurationRepository;

    public PaymentConfiguration save(PaymentConfiguration t) {
        return paymentConfigurationRepository.save(t);
    }

    public List<PaymentConfiguration> findAll() {
        return paymentConfigurationRepository.findAll();
    }

    public PaymentConfiguration get(Long id) {
        return paymentConfigurationRepository.findOne(id);
    }

    public void setPaymentConfigurationRepository(PaymentConfigurationRepository paymentConfigurationRepository) {
        this.paymentConfigurationRepository = paymentConfigurationRepository;
    }

    public List<PaymentConfiguration> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
   
}
