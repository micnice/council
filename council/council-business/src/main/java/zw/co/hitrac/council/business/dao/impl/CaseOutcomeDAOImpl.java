/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.CaseOutcomeDAO;
import zw.co.hitrac.council.business.dao.repo.CaseOutcomeRepository;

import zw.co.hitrac.council.business.domain.CaseOutcome;

    
/**
 *
 * @author kelvin
 */
@Repository
public class CaseOutcomeDAOImpl implements  CaseOutcomeDAO {
    @Autowired
    private CaseOutcomeRepository caseOutcomeRepository;
    
   public CaseOutcome save(CaseOutcome caseOutcome){
       return caseOutcomeRepository.save(caseOutcome);
   }
   
   public List<CaseOutcome> findAll(){
       return caseOutcomeRepository.findAll();
   }
   
   public CaseOutcome get(Long id){
       return caseOutcomeRepository.findOne(id);
   }
 /**
     * A setter method that will make mocking repo object easier
     * @param caseOutcomeRepository 
     */
   
   public void setCaseOutcomeRepository(CaseOutcomeRepository caseOutcomeRepository) {
         this.caseOutcomeRepository = caseOutcomeRepository;
    }

    public List<CaseOutcome> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

