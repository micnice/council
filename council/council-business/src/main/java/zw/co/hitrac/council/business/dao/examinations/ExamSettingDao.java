/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.examinations;

import java.util.List;
import zw.co.hitrac.council.business.dao.IGenericDAO;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.domain.examinations.ExamYear;

/**
 *
 * @author tidza
 */
public interface ExamSettingDao extends IGenericDAO<ExamSetting> {
    public List<ExamSetting> getExamSettings(Boolean active);

    public List<ExamSetting> findByExamYear(ExamYear examYear);
}
