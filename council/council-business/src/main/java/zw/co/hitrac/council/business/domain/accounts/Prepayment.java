package zw.co.hitrac.council.business.domain.accounts;

import org.hibernate.annotations.Formula;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import zw.co.hitrac.council.business.domain.BaseIdEntity;
import zw.co.hitrac.council.business.domain.Institution;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Michael Matiashe
 */
@Entity
@Table(name = "prepayment")
@Audited
@XmlRootElement
public class Prepayment extends BaseIdEntity implements Serializable {

    private Date dateOfPayment;
    private Date dateOfDeposit;
    private BigDecimal balance = BigDecimal.ZERO;
    private BigDecimal amount = BigDecimal.ZERO;
    private BigDecimal amountUsed = BigDecimal.ZERO;
    private Institution institution;
    private TransactionType transactionType;
    private Boolean canceled = Boolean.FALSE;
    private long version;

    @ManyToOne
    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    @Override
    public String toString() {
        return transactionType + " " + dateOfDeposit;
    }

    public Boolean isCanceled() {
        return canceled;
    }

    public void setCanceled(Boolean canceled) {
        this.canceled = canceled;
    }

    @OneToOne
    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    @Formula(value = "IFNULL((select sum(r.amount) from receiptitem r where r.receiptHeader_id IN (select p.id from receiptheader p where p.paymentDetails_id in (select q.paymentDetails_id from prepaymentdetails q where q.prepayment_id=id and q.cancelled=0))),0)-amount")
    @NotAudited //required for @Formula
    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Formula(value = "IFNULL((select sum(r.amount) from receiptitem r where r.receiptHeader_id IN (select p.id from receiptheader p where p.paymentDetails_id in (select q.paymentDetails_id from prepaymentdetails q where q.prepayment_id=id and q.cancelled=0))),0)")
    @NotAudited //required for @Formula
    public BigDecimal getAmountUsed() {
        return amountUsed;
    }

    public void setAmountUsed(BigDecimal amountUsed) {
        this.amountUsed = amountUsed;
    }

    @Version
    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDateOfPayment() {
        return dateOfPayment;
    }

    public void setDateOfPayment(Date dateOfPayment) {
        this.dateOfPayment = dateOfPayment;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDateOfDeposit() {
        return dateOfDeposit;
    }

    public void setDateOfDeposit(Date dateOfDeposit) {
        this.dateOfDeposit = dateOfDeposit;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

}
