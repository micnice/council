package zw.co.hitrac.council.business.config;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.lang.BooleanUtils;
import org.jasypt.springsecurity3.authentication.encoding.PasswordEncoder;
import org.jasypt.util.password.ConfigurablePasswordEncryptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.orm.hibernate4.HibernateExceptionTranslator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import zw.co.hitrac.council.business.domain.BaseEntity;

import javax.annotation.Resource;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

/**
 * @author Charles Chigoriwa
 */
@Configuration
@ComponentScan(basePackages = {"zw.co.hitrac.council.business.dao",
    "zw.co.hitrac.council.business.service",
    "zw.co.hitrac.council.business.scheduled",
    "zw.co.hitrac.council.business.process"})
@EnableJpaRepositories(basePackages = {"zw.co.hitrac.council.business.dao.repo",
    "zw.co.hitrac.council.business.dao.repo.accounts"})
@EnableJpaAuditing(auditorAwareRef = "securityAuditorAware", modifyOnCreate = false)
@EnableTransactionManagement
//Spring Scheduling-specific annotations, enable with caution
@EnableScheduling
@EnableAsync
@Import({DBConfiguration.class, EmailConfiguration.class})
public class CouncilBusinessConfiguration {

  @Resource
  Environment environment;

  @Bean
  public PlatformTransactionManager transactionManager() {

    JpaTransactionManager transactionManager = new JpaTransactionManager();
    transactionManager.setEntityManagerFactory(entityManagerFactory());
    return transactionManager;
  }

  @Bean
  public EntityManagerFactory entityManagerFactory() {

    //use Hibernate as persistence provider
    HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();

    vendorAdapter.setGenerateDdl(Boolean.TRUE);
    vendorAdapter.setShowSql(BooleanUtils.toBoolean(environment.getRequiredProperty("hibernate.show_sql")));

    LocalContainerEntityManagerFactoryBean entityManagerFactory
        = new LocalContainerEntityManagerFactoryBean();
    entityManagerFactory.setJpaVendorAdapter(vendorAdapter);
    entityManagerFactory.setPackagesToScan(new String[]{BaseEntity.class.getPackage().getName()});
    entityManagerFactory.setDataSource(dataSource());

    //Additional properties
    Properties jpaProperties = new Properties();
    jpaProperties.put("hibernate.format_sql",
        BooleanUtils.toBoolean(environment.getRequiredProperty("hibernate.format_sql")));
    jpaProperties.put("hibernate.use_sql_comments",
        BooleanUtils.toBoolean(environment.getRequiredProperty("hibernate.use_sql_comments")));
    jpaProperties.put("hibernate.hbm2ddl.auto",
        environment.getRequiredProperty("hibernate.hbm2ddl.auto"));
    entityManagerFactory.setJpaProperties(jpaProperties);
    entityManagerFactory.afterPropertiesSet();  //post initialization of a initialising bean to set all properties

    return entityManagerFactory.getObject();
  }

  @Bean(destroyMethod = "close")
  public DataSource dataSource() {

    BasicDataSource dataSource = new BasicDataSource();
    dataSource.setDriverClassName(environment.getRequiredProperty("jdbc.driverClassName"));
    dataSource.setUrl(environment.getRequiredProperty("jdbc.url"));
    dataSource.setUsername(environment.getRequiredProperty("jdbc.username"));
    dataSource.setPassword(environment.getRequiredProperty("jdbc.password"));
    dataSource.setValidationQuery("SELECT 1");
    dataSource.setTestOnBorrow(true);
    dataSource.setTestWhileIdle(true);

    return dataSource;
  }

  //For Hibernate 4+ Exception translation
  @Bean
  public HibernateExceptionTranslator hibernateExceptionTranslator() {

    return new HibernateExceptionTranslator();
  }

  /**
   * A reasonably strong password encoder, also configurable so we can change to another JVM bundled algorithm or also
   * use non non-JVM JCE providers such as Bouncy Castle for even more algorithms
   *
   * @return An encoder instance
   */

  @Bean
  public PasswordEncoder encoder() {

    PasswordEncoder encoder = new PasswordEncoder();

    ConfigurablePasswordEncryptor passwordEncryptor = new ConfigurablePasswordEncryptor();
    passwordEncryptor.setAlgorithm(environment.getRequiredProperty("digest.algorithm"));
    passwordEncryptor.setPlainDigest(false);    //should be false, to use digest salt and default iteration count
//        passwordEncryptor.setProvider();  //For using providers such as Bouncy Castle

    encoder.setPasswordEncryptor(passwordEncryptor);
    return encoder;

  }

  /**
   * Used for sending of emails in conjunction with EmailMessageService Not returning the interface MailSender to enable
   * injecting user name and password of sender later
   *
   * @return an instance of the mail sender implementation
   */
  @Bean
  public JavaMailSenderImpl mailSender() {

    JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
    mailSender.setHost(environment.getRequiredProperty("mail.host"));
    mailSender.setPort(environment.getRequiredProperty("mail.port", Integer.class));

    Properties javaMailProperties = new Properties();
    javaMailProperties.put("mail.smtp.auth", environment.getRequiredProperty("mail.smtp.auth"));
    javaMailProperties.put("mail.smtp.starttls.enable", environment.getRequiredProperty("mail.smtp.starttls.enable"));
    javaMailProperties.put("mail.smtp.starttls.required", environment.getRequiredProperty("mail.smtp.starttls.required"));

    javaMailProperties.put("http.proxyUser", environment.getProperty("http.proxyUser"));
    javaMailProperties.put("http.proxyPassword", environment.getProperty("http.proxyPassword"));
    javaMailProperties.put("http.proxyPort", environment.getProperty("http.proxyPort"));
    javaMailProperties.put("http.proxyHost", environment.getProperty("http.proxyHost"));

    mailSender.setJavaMailProperties(javaMailProperties);

    return mailSender;
  }

}
