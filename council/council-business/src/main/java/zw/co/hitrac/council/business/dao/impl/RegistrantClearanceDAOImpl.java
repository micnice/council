/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.RegistrantClearanceDAO;
import zw.co.hitrac.council.business.dao.repo.RegistrantClearanceRepository;
import zw.co.hitrac.council.business.domain.*;

/**
 *
 * @author kelvin
 */
@Repository
public class RegistrantClearanceDAOImpl implements RegistrantClearanceDAO {

    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    private RegistrantClearanceRepository registrantClearanceRepository;

    public RegistrantClearance save(RegistrantClearance registrantClearance) {
        return registrantClearanceRepository.save(registrantClearance);
    }

    public List<RegistrantClearance> findAll() {
        return registrantClearanceRepository.findAll();
    }

    public RegistrantClearance get(Long id) {
        return registrantClearanceRepository.findOne(id);
    }

    /**
     * A setter method that will make mocking repo object easier
     *
     * @param registrantClearanceRepository
     */
    public void setRegistrantClearanceRepository(RegistrantClearanceRepository registrantClearanceRepository) {
        this.registrantClearanceRepository = registrantClearanceRepository;
    }

    public List<RegistrantClearance> getClearances(Registrant registrant) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantClearance.class);
        if (registrant == null) {
            return new ArrayList<RegistrantClearance>();
        } else {
            criteria.add(Restrictions.eq("registrant", registrant));
        }
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public Date getRegistrantClearanceDate(Registrant registrant, Course course) {
        if (course == null) {
            return new Date();
        }
        if (registrant == null) {
            return new Date();
        }
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantClearance.class);
        criteria.add(Restrictions.eq("registrant", registrant));
        criteria.add(Restrictions.eq("course", course));
        List<RegistrantClearance> list = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        if (list.isEmpty()) {
            return new Date();
        }
        return list.get(0).getClearanceDate();
    }

    public Boolean getHasPBQExamClearance(Registrant registrant) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantClearance.class);
        criteria.add(Restrictions.eq("registrant", registrant));
        criteria.createAlias("course", "c");
        criteria.add(Restrictions.eq("c.basic", Boolean.FALSE));
        criteria.addOrder(Order.desc("clearanceDate"));
        return !criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list().isEmpty();
    }

    public Boolean getHasExamClearance(Registrant registrant) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantClearance.class);
        criteria.add(Restrictions.eq("registrant", registrant));
        criteria.createAlias("course", "c");
        return !criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list().isEmpty();
    }

    public RegistrantClearance getRegistrantClearance(Registrant registrant, Course course) {
        if (course == null) {
            return null;
        }
        if (registrant == null) {
            return null;
        }
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantClearance.class);
        criteria.add(Restrictions.eq("registrant", registrant));
        criteria.add(Restrictions.eq("course", course));
        List<RegistrantClearance> list = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    public List<RegistrantClearance> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Date getLastClearanceDate(Registrant registrant) {
        Date date = (Date) this.entityManager.createQuery("Select MAX(a.clearanceDate) from RegistrantActivity a where a.registrant=:registrant")
                .setParameter("registrant", registrant)
                .getSingleResult();
        return date;
    }
}
