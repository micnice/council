package zw.co.hitrac.council.web.pages.accounts;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.ProductPrice;
import zw.co.hitrac.council.business.service.accounts.ProductPriceService;

/**
 *
 * @author Charles Chigoriwa
 */
public class ProductPriceViewPage extends IInventoryModulePage {

    @SpringBean
    private ProductPriceService productPriceService;

    public ProductPriceViewPage(Long id) {
        CompoundPropertyModel<ProductPrice> model = new CompoundPropertyModel<ProductPrice>(new LoadableDetachableProductPriceModel(id));
        setDefaultModel(model);
        add(new Link<ProductPrice>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new ProductPriceEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", ProductPriceListPage.class));
        add(new Label("product.name"));
        add(new Label("price"));
    }

    private final class LoadableDetachableProductPriceModel extends LoadableDetachableModel<ProductPrice> {

        private Long id;

        public LoadableDetachableProductPriceModel(Long id) {
            this.id = id;
        }

        @Override
        protected ProductPrice load() {

            return productPriceService.get(id);
        }
    }
}
