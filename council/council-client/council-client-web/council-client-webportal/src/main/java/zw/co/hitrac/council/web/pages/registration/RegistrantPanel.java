/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.registration;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.NonCachingImage;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.process.RenewalProcess;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.web.pages.accounts.payments.TransactionHistoryPanel;
import zw.co.hitrac.council.web.pages.research.RegistrantImageResource;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * @author tdhlakama
 */
public class RegistrantPanel extends Panel {

    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private RegistrationService registrationService;
    @SpringBean
    private RegistrationProcess registrationProcess;
    @SpringBean
    private DurationService durationService;
    @SpringBean
    private RegistrantActivityService registrantActivityService;
    @SpringBean
    private RenewalProcess renewalProcess;
    @SpringBean
    private DebtComponentService debtComponentService;
    @SpringBean
    private RegistrantDisciplinaryService disciplinaryService;
    @SpringBean
    private RegistrantCpdService registrantCpdService;
    @SpringBean
    private RegistrantCpdItemConfigService registrantCpdItemConfigService;
    private LoadableDetachableModel<Registrant> registrantModel;

    public RegistrantPanel(String id, final Long registrantId) {
        super(id);

        registrantModel = new DetachableRegistrantModel(registrantId, registrantService);

        final CompoundPropertyModel<Registrant> model = new CompoundPropertyModel<>(registrantModel);
        setDefaultModel(model);

        final Boolean suspended = disciplinaryService.registrantSuspendedStatus(registrantModel.getObject());

        final Boolean deRegistered = registrationService.hasBeenDeRegistered(registrantModel.getObject());

        final Boolean hasPenalty = debtComponentService.hasAccountDebtComponents(registrantModel.getObject().getCustomerAccount(), TypeOfService.PENALTY_FEES);


        final boolean hasCurrentRenewal = renewalProcess.hasCurrentRenewal(registrantModel.getObject());

        final Boolean annualFeeArea = debtComponentService.hasAccountDebtComponents(registrantModel.getObject().getCustomerAccount(), TypeOfService.ANNUAL_FEES);

        CouncilDuration lastCouncilDuration = registrantActivityService.getLastCouncilDurationRenewal(registrantModel.getObject());

        final Boolean hasAnnualDebt = debtComponentService.hasAccountDebtComponents(registrantModel.getObject().getCustomerAccount(), TypeOfService.ANNUAL_FEES);


        add(new Link<Registrant>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        }.add(new Label("fullname")));
        add(new Label("integerValueOfAge"));
        add(new Label("registrationNumber"));
        add(new Label("identificationType"));
        add(new Label("idNumber"));
        add(new Label("customerAccount.account.balance"));
        add(new Label("infoMessages", new LoadableDetachableModel<String>() {

            @Override
            protected String load() {
                StringBuilder messages = new StringBuilder();
                final Registrant registrant = registrantModel.getObject();

                if (deRegistered) {
                    messages.append(getFormattedInfoMessage("Is " + RegistrantActivityType.DEREGISTRATION.getName(), "red"));
                }

                if (registrant.getVoided()) {
                    messages.append(getFormattedInfoMessage(RegistrantActivityType.VIODED.getName() + " - " + registrantActivityService.getRegistrantActivityComment(registrant, RegistrantActivityType.VIODED) + " ", "red"));
                }

                if (registrant.getDead()) {
                    messages.append(getFormattedInfoMessage("Is - " + registrant.getDeadStatus() + " ", "blue"));
                }

                if (registrant.getRetired()) {
                    messages.append(getFormattedInfoMessage("Is " + registrantModel.getObject().getRetiredStatus(), "blue"));
                }

                if (hasPenalty) {
                    messages.append(getFormattedInfoMessage("Has Unpaid Penalties", "red"));
                }

                if (hasAnnualDebt) {
                    messages.append(getFormattedInfoMessage("Payment Required for Annual Fees Billed", "red"));
                }

                Registration registration = registrationProcess.activeRegisterNotStudentRegister(registrant);
                if (registration != null) {
                    Register register = registration.getRegister();
                    if (register.equals(generalParametersService.get().getProvisionalRegister())) {
                        final boolean isDueForTransferToMainRegister =
                                registrationService.isDueForTransferFromProvisionalToMainRegister(registration);

                        if (isDueForTransferToMainRegister) {
                            messages.append(getFormattedInfoMessage("Is Due For Transfer To Main Register", "green"));
                        }

                        if (suspended) {
                            messages.append(getFormattedInfoMessage("Is " + generalParametersService.get().getSuspendedStatus(), "blue"));
                        }

                    }
                }
                return messages.toString();
            }

        }).setEscapeModelStrings(false));

        add(new Label("lastRenewal", new Model<String>() {
            @Override
            public String getObject() {
//                if (generalParametersService.get().getlastRenewalPeriodShowsEndDate()) {
//                    RegistrantActivity registrantActivity = registrantActivityService.getLastRenewalActivity(registrantModel.getObject());
//                    if (registrantActivity != null) {
//                        return registrantActivity.getDuration().getStartDate() + " - " + registrantActivity.getDuration().getEndDate();
//                    }
//                }

                if (lastCouncilDuration != null) {
                    if ((debtComponentService.getCustomerBalanceDue(registrantModel.getObject().getCustomerAccount()).compareTo(BigDecimal.ZERO) == 0) ||
                            (debtComponentService.getCustomerBalanceDue(registrantModel.getObject().getCustomerAccount()).compareTo(BigDecimal.ZERO) == -1)) {


                        return lastCouncilDuration.getName();

                    } else {
                        return "";

                    }

                } else {
                    return "";
                }
            }
        }));

        add(new Label("courses", new Model<String>() {
            @Override
            public String getObject() {

                String courseStatement = "";
                Set<String> courses = new HashSet<String>();
                for (Registration reg : registrationService.getActiveRegistrations(registrantModel.getObject())) {
                    if (reg.getCourse() != null && reg.getCourse().getName() != null) {
                        courses.add(reg.getCourse().getName());
                    }
                }
                for (String s : courses) {
                    courseStatement = courseStatement.concat(s + ", ");
                }
                if (courseStatement.isEmpty()) {
                    courseStatement = ", ";
                }

                courseStatement = courseStatement.substring(0, courseStatement.length() - 2);

                return courseStatement;
            }
        }));


        add(new Label("balanceDue", debtComponentService.getCustomerBalanceDue(registrantModel.getObject().getCustomerAccount())));
        add(new NonCachingImage("registrantImage", new RegistrantImageResource(registrantModel.getObject(), generalParametersService)));

        add(new Label("registrantStatus", new Model<String>() {

            @Override
            public String getObject() {

                if (hasCurrentRenewal && !annualFeeArea) {
                    return "-- Active --";
                } else {
                    return "-- InActive --";
                }
            }

        }));

        add(new TransactionHistoryPanel("transactionHistoryPanel", registrantModel.getObject().getId()));

    }

    private String getFormattedInfoMessage(String rawMessage, String color) {

        return String.format("<p style=\"color:%s\">%s</p>", color, rawMessage);
    }
}
