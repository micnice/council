package zw.co.hitrac.council.web.utility;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.service.RegistrantService;

/**
 * @author Takunda Dhlakama
 */
public class DetachableRegistrantModel extends LoadableDetachableModel<Registrant> {

    private static final long serialVersionUID = 1L;
    /**
     * database identity of the registrant
     */
    private long id;
    private String registrationNumber;
    /**
     * service dao reference - must be a wicket-wrapped proxy, holding onto a
     * reference to the real service dao will cause its serialization into
     * session or a not-serializable exception when the servlet container
     * serializes the session.
     */
    private final RegistrantService service;

    /**
     * Constructor
     *
     * @param id
     * @param service
     */
    public DetachableRegistrantModel(Long id, RegistrantService service) {
        this.id = id;
        this.service = service;
    }

    /**
     * Constructor
     *
     * @param registrant
     * @param service
     */
    public DetachableRegistrantModel(Registrant registrant, RegistrantService service) {
        id = registrant.getId();
        this.service = service;
    }

    public DetachableRegistrantModel(String registrationNumber, RegistrantService service) {
        this.registrationNumber = registrationNumber;
        this.service = service;
    }

    /**
     * Loads the registrant from the database
     *
     * @see LoadableDetachableModel#load()
     */
    @Override
    protected Registrant load() {
        if (registrationNumber != null && registrationNumber.length() != 0) {
            return service.getRegistrantByRegistrationNumber(registrationNumber);
        }
        Registrant r = service.get(id);

        if (r == null) {
            return new Registrant();
        } else {
            return r;
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
