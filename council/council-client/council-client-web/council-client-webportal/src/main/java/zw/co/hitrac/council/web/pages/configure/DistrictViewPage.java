package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.District;
import zw.co.hitrac.council.business.service.DistrictService;

/**
 *
 * @author Constance Mabaso
 */
public class DistrictViewPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private DistrictService districtService;

    public DistrictViewPage(Long id) {
        CompoundPropertyModel<District> model=new CompoundPropertyModel<District>(new LoadableDetachableDistrictModel(id));
        setDefaultModel(model);
        add(new Link<District>("editLink",model){

            @Override
            public void onClick() {
                setResponsePage(new DistrictEditPage(getModelObject().getId(), getPageReference()));
            }
            
        });
        add(new BookmarkablePageLink<Void>("returnLink", DistrictListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("province"));
        add(new Label("district",model.bind("name")));
    }


    private final class LoadableDetachableDistrictModel extends LoadableDetachableModel<District> {

        private Long id;

        public LoadableDetachableDistrictModel(Long id) {
            this.id = id;
        }

        @Override
        protected District load() {

            return districtService.get(id);
        }
    }
}
