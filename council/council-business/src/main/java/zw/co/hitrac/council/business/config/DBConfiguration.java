package zw.co.hitrac.council.business.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

/**
 *
 * @author Daniel Nkhoma
 */
@Configuration
public class DBConfiguration {

    @Configuration
    @Profile("dev")
    @PropertySource("classpath:zw/co/hitrac/council/business/config/jdbc_dev.properties")
    static class PersistenceDevelopment {
    }

}
