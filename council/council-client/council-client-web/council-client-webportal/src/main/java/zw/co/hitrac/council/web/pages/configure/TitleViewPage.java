package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Title;
import zw.co.hitrac.council.business.service.TitleService;

/**
 *
 * @author charlesc
 */
public class TitleViewPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private TitleService titleService;

    public TitleViewPage(Long id) {
        CompoundPropertyModel<Title> model = new CompoundPropertyModel<Title>(new LoadableDetachableTitleModel(id));

        setDefaultModel(model);
        add(new Link<Title>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new TitleEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", TitleListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("title", model.bind("name")));
    }

    private final class LoadableDetachableTitleModel extends LoadableDetachableModel<Title> {
        private Long id;

        public LoadableDetachableTitleModel(Long id) {
            this.id = id;
        }

        @Override
        protected Title load() {
            return titleService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
