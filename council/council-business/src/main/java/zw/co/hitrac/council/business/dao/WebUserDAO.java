/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao;

import zw.co.hitrac.council.business.domain.WebUser;

/**
 *
 * @author tdhlakama
 *
 */
public interface WebUserDAO extends IGenericDAO<WebUser> {

    WebUser get(String username, String password);

    WebUser getByRegistrationNumber(String registrationNumber);

    WebUser getWebUserByUsername(String username);
    
    String createWebUser(String username, String password, String registrationNumber);
    
    String loginWebUser(String username, String password);
   
    void changeWebUserPassword(WebUser webUser, String password);

}
