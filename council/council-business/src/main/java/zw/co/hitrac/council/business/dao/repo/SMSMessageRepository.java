package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.SMSMessage;

/**
 *
 * @author Michael Matiashe
 */
public interface SMSMessageRepository extends CrudRepository<SMSMessage, Long> {
    public List<SMSMessage> findAll();
}
