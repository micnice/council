package zw.co.hitrac.council.business.dao.examinations.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.examinations.ExamPeriodDao;
import zw.co.hitrac.council.business.dao.repo.examinations.ExamPeriodRepository;
import zw.co.hitrac.council.business.domain.examinations.ExamPeriod;

/**
 *
 * @author tidza
 */
@Repository
public class ExamPeriodDaoImpl implements ExamPeriodDao {

    @Autowired
    private ExamPeriodRepository examPeriodRepository;

    public ExamPeriod save(ExamPeriod t) {
        return examPeriodRepository.save(t);
    }

    public List<ExamPeriod> findAll() {
        return examPeriodRepository.findAll();
    }

    public ExamPeriod get(Long id) {
        return examPeriodRepository.findOne(id);
    }

    public void setExamPeriodRepository(ExamPeriodRepository examPeriodRepository) {
        this.examPeriodRepository = examPeriodRepository;
    }

    public List<ExamPeriod> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
