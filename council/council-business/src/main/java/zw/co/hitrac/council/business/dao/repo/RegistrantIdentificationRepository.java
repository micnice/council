/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.AddressType;
import zw.co.hitrac.council.business.domain.RegistrantIdentification;

/**
 *
 * @author kelvin
 */
public interface RegistrantIdentificationRepository extends CrudRepository<RegistrantIdentification, Long>{
     public List<RegistrantIdentification> findAll();   
}
