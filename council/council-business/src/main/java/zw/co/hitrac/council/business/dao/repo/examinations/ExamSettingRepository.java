/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.repo.examinations;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.domain.examinations.ExamYear;

/**
 *
 * @author tidza
 */
public interface ExamSettingRepository extends CrudRepository<ExamSetting, Long> {
public List<ExamSetting> findAll();

public List<ExamSetting> findByExamYear(ExamYear examYear);
}
