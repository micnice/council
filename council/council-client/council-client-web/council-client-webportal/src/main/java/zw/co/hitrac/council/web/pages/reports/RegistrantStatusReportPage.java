/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.reports;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.RegisterService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.reports.RegistrantStatusReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 * @author kelvin
 */
public class RegistrantStatusReportPage extends TemplatePage {

    @SpringBean
    private RegisterService registerService;
    @SpringBean
    private RegistrationService registrationService;
    private Course course;
    private Register register;
    @SpringBean
    private CourseService courseService;
    private ContentType contentType;

    public RegistrantStatusReportPage() {

        PropertyModel<Register> registerModel = new PropertyModel<Register>(this, "register");
        PropertyModel<Course> courseModel = new PropertyModel<Course>(this, "course");
        PropertyModel<ContentType> contentTypeModel = new PropertyModel<ContentType>(this, "contentType");
        Form form = new Form("form");
        form.add(new DropDownChoice("course",courseModel, courseService.findAll()));
        form.add(new DropDownChoice("register",registerModel ,registerService.findAll()));
        form.add(new DropDownChoice("contentType", contentTypeModel, Arrays.asList(ContentType.values())));
        add(form);
      

        form.add(new Button("report") {
            @Override
            public void onSubmit() {
                try {
                    Map parameters = new HashMap();
                  if (course!= null && register != null) {
                        RegistrantStatusReport registrantStatusReport = new RegistrantStatusReport();
                        
                        parameters.put("comment", "All active registrations " + register + " to " + course);
                        ByteArrayResource resource = ReportResourceUtils.getReportResource(registrantStatusReport, contentType, registrationService.getActiveRegistrationByRegister(course, register), parameters);
                        IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                RequestCycle.get().getResponse(), null);
                        resource.respond(a);
                        // To make Wicket stop processing form after sending response
                        RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                    }
                }
             catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }

                    
        }

    });
}
}
