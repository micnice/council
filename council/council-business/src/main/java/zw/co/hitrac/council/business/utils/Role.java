package zw.co.hitrac.council.business.utils;

/**
 *
 * @author Clive Gurure
 */
public enum Role {

    REGISTRAR("Registrar"),
    SYSTEM_ADMINISTRATOR("Systems Administrator"),
    EDUCATION_OFFICER("Education Officer"),
    ACCOUNTS_OFFICER("Accounts Officer"),
    REGISTRY_CLERK("Registry Clerk"),
    ///Role Not Used - Appear but not Used
    DEPUTY_REGISTRAR("Deputy Registrar"),
    FINANCIAL_ADMIN_EXECUTIVE("Financial AdminExecutive"),
    REGISTRY_SUPERVISOR("Registry Supervisor"),
    BOOKKEEPER("Book Keeper"),
    IT_OFFICER("IT Officer"),
    DATA_CAPTURE_CLERK("Data Capture Clerk"),
    CERTIFICATION_CLERK("Certification Clerk");

    private Role(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }

    @Override
    public String toString() {
        return roleName;
    }
    private final String roleName;
}
