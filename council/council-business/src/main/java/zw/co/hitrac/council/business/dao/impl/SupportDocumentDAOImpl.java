/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.SupportDocumentDAO;
import zw.co.hitrac.council.business.dao.repo.SupportDocumentRepository;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantDisciplinary;
import zw.co.hitrac.council.business.domain.RegistrantQualification;
import zw.co.hitrac.council.business.domain.Requirement;

import zw.co.hitrac.council.business.domain.SupportDocument;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;

/**
 *
 * @author kelvin
 */
@Repository
public class SupportDocumentDAOImpl implements SupportDocumentDAO {

    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    private SupportDocumentRepository supportDocumentRepository;

    public SupportDocument save(SupportDocument supportDocument) {
        return supportDocumentRepository.save(supportDocument);
    }

    public List<SupportDocument> findAll() {
        return supportDocumentRepository.findAll();
    }

    public SupportDocument get(Long id) {
        return supportDocumentRepository.findOne(id);
    }
    
    

    /**
     * A setter method that will make mocking repo object easier
     *
     * @param supportDocumentRepository
     */
    public void setSupportDocumentRepository(SupportDocumentRepository supportDocumentRepository) {
        this.supportDocumentRepository = supportDocumentRepository;
    }

    public List<SupportDocument> getSupportDocuments(Registrant registrant, Application application, Requirement requirement, RegistrantDisciplinary registrantDisciplinary, RegistrantQualification registrantQualification, ExamRegistration examRegistration, Institution institution) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(SupportDocument.class);
        if (registrant != null) {
            criteria.add(Restrictions.eq("registrant", registrant));
        }
        if (application != null) {
            criteria.add(Restrictions.eq("application", application));
        }
        if (requirement != null) {
            criteria.add(Restrictions.eq("requirement", requirement));
        }
        if (registrantDisciplinary != null) {
            criteria.add(Restrictions.eq("registrantDisciplinary", registrantDisciplinary));
        }

        if (registrantQualification != null) {
            criteria.add(Restrictions.eq("registrantQualification", registrantQualification));
        }
        if (examRegistration != null) {
            criteria.add(Restrictions.eq("examRegistration", examRegistration));
        }
        if (institution != null) {
            criteria.add(Restrictions.eq("institution", institution));
        }
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        criteria.addOrder(Order.asc("dateCreated"));        
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public SupportDocument getSupportDocument(Registrant registrant, Requirement requirement) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(SupportDocument.class);
        criteria.add(Restrictions.eq("registrant", registrant));
        criteria.add(Restrictions.eq("requirement", requirement));
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        List<SupportDocument> list = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        if (list.isEmpty()) {
            return null;
        } else {
            return (SupportDocument) list.get(0);
        }
    }

    public SupportDocument getClearanceDate(Registrant registrant, Course course) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(SupportDocument.class);
        criteria.add(Restrictions.eq("registrant", registrant));
        criteria.add(Restrictions.eq("course", course));
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        List<SupportDocument> list = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
       if (list.isEmpty()) {
            return null;
        } else {
            return (SupportDocument) list.get(0);
        }
    }

    public SupportDocument getSupportDocument(Institution institution, Requirement requirement) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(SupportDocument.class);
        criteria.add(Restrictions.eq("institution", institution));
        criteria.add(Restrictions.eq("requirement", requirement));
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        List<SupportDocument> list = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        if (list.isEmpty()) {
            return null;
        } else {
            return (SupportDocument) list.get(0);
        }
    }

    public SupportDocument getSupportDocument(ExamRegistration examRegistration, Requirement requirement) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(SupportDocument.class);
        criteria.add(Restrictions.eq("examRegistration", examRegistration));
        criteria.add(Restrictions.eq("requirement", requirement));
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        List<SupportDocument> list = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        if (list.isEmpty()) {
            return null;
        } else {
            return (SupportDocument) list.get(0);
        }

    }

    public SupportDocument getSupportDocument(Application application, Requirement requirement) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(SupportDocument.class);
        criteria.add(Restrictions.eq("application", application));
        criteria.add(Restrictions.eq("requirement", requirement));
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        List<SupportDocument> list = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        if (list.isEmpty()) {
            return null;
        } else {
            return (SupportDocument) list.get(0);
        }

    }

    public List<SupportDocument> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
