package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.DurationDAO;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.service.DurationService;

import java.util.Date;
import java.util.List;

/**
 * @author Charles Chigoriwa
 */
@Service
@Transactional
public class DurationServiceImpl implements DurationService {

    @Autowired
    private DurationDAO durationDAO;

    @Transactional
    public Duration save(Duration duration) {
        return durationDAO.save(duration);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Duration> findAll() {
        return durationDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Duration get(Long id) {
        return durationDAO.get(id);
    }

    public List<Duration> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setDurationDAO(DurationDAO durationDAO) {
        this.durationDAO = durationDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Duration getCurrentCourseDuration(Course course) {
        return this.durationDAO.getCurrentCourseDuration(course);
    }

    public List<Duration> getCourseDurations(Course course) {
        return this.durationDAO.getCourseDurations(course);
    }

    public Duration getLastRenewalDuration(Course course, Date startDate) {
        return durationDAO.getLastRenewalDuration(course, startDate);
    }

    public Duration getDurationByCourseAndCouncilDuration(Course course, CouncilDuration councilDuration) {

        return durationDAO.getDurationByCourseAndCouncilDuration(course, councilDuration);
    }

    @Override
    public List<Duration> getDurations(CouncilDuration councilDuration) {
        return durationDAO.getDurations(councilDuration);
    }

    public List<Duration> getDurationsAfterDuration(Duration duration, Course course) {
        return durationDAO.getDurationsAfterDuration(duration, course);
    }

    public List<Duration> getDurationsBeforeDuration(Duration duration, Course course) {
        return durationDAO.getDurationsBeforeDuration(duration, course);
    }


}
