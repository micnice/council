package zw.co.hitrac.council.web.pages.accounts;

import zw.co.hitrac.council.web.pages.configure.*;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.ProductGroup;
import zw.co.hitrac.council.business.service.accounts.ProductGroupService;

/**
 *
 * @author charlesc
 */
public class ProductGroupViewPage extends IBankBookPage {

    @SpringBean
    private ProductGroupService productGroupService;

    public ProductGroupViewPage(Long id) {
        CompoundPropertyModel<ProductGroup> model=new CompoundPropertyModel<ProductGroup>(new LoadableDetachableProductGroupModel(id));
        setDefaultModel(model);
        add(new Link<ProductGroup>("editLink",model){

            @Override
            public void onClick() {
                setResponsePage(new ProductGroupEditPage(getModelObject().getId()));
            }
            
        });
        add(new BookmarkablePageLink<Void>("returnLink", ProductGroupListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("course"));
        add(new Label("productGroup",model.bind("name")));
    }


    private final class LoadableDetachableProductGroupModel extends LoadableDetachableModel<ProductGroup> {

        private Long id;

        public LoadableDetachableProductGroupModel(Long id) {
            this.id = id;
        }

        @Override
        protected ProductGroup load() {

            return productGroupService.get(id);
        }
    }
}
