/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.MisconductType;

/**
 *
 * @author hitrac
 */
public interface MisconductTypeService extends IGenericService<MisconductType> {
}
