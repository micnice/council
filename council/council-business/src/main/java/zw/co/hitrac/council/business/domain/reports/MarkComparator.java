/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain.reports;

import java.math.BigDecimal;
import java.util.Comparator;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;

/**
 *
 * @author tdhlakama
 */
public class MarkComparator implements Comparator<ExamRegistration> {

    @Override
    public int compare(ExamRegistration o1, ExamRegistration o2) {
        return new BigDecimal(o2.getFinalMark()).compareTo(new BigDecimal(o1.getFinalMark()));
    }
}
