/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain.reports;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import zw.co.hitrac.council.business.domain.accounts.Product;

/**
 *
 * @author tdhlakama
 */
public class ProductItem implements Serializable {

    private BigDecimal amountPaid = BigDecimal.ZERO;
    private Product product;
    private Date startDate;
    private Date endDate;

    public ProductItem() {
    }
    
    
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }

    public Boolean getShow() {
        if (amountPaid.equals(BigDecimal.ZERO)) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }
    }
}
