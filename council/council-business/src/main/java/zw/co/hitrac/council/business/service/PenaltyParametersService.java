
package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.PenaltyParameters;

/**
 *
 * @author Matiashe Michael
 */
public interface PenaltyParametersService extends IGenericService<PenaltyParameters>{
    public PenaltyParameters get();
}
