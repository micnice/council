package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.domain.WebUser;

import java.util.List;
import zw.co.hitrac.council.business.dao.WebUserDAO;
import zw.co.hitrac.council.business.service.WebUserService;

/**
 *
 * @author tdhlakama
 */
@Service
@Transactional
public class WebUserServiceImpl implements WebUserService {

    @Autowired
    private WebUserDAO webUserDao;

    @Transactional
    public WebUser save(WebUser t) {
        return webUserDao.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<WebUser> findAll() {
        return webUserDao.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public WebUser get(Long id) {
        return webUserDao.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<WebUser> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public WebUser get(String username, String password) {
        return webUserDao.get(username, password);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public WebUser getByUsername(String username) {
        return webUserDao.getWebUserByUsername(username);
    }

    @Override
    public WebUser get(String registrationNumber) {
        return webUserDao.getByRegistrationNumber(registrationNumber);
    }

    @Transactional(readOnly = false, propagation = Propagation.SUPPORTS)
    public String createWebUser(String username, String password, String registrationNumber) {
        return webUserDao.createWebUser(username, password, registrationNumber);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public String loginWebUser(String username, String password) {
        return webUserDao.loginWebUser(username, password);
    }

    @Transactional(readOnly = false, propagation = Propagation.SUPPORTS)
    public void changeWebUserPassword(WebUser webUser, String password) {
        webUserDao.changeWebUserPassword(webUser, password);
    }

}
