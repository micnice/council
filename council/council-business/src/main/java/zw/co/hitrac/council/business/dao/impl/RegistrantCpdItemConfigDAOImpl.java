/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.RegistrantCpdItemConfigDAO;
import zw.co.hitrac.council.business.dao.repo.RegistrantCpdItemConfigRepository;
import zw.co.hitrac.council.business.domain.RegistrantCpdItemConfig;

/**
 *
 * @author hitrac
 */
@Repository
public class RegistrantCpdItemConfigDAOImpl implements RegistrantCpdItemConfigDAO{
   @Autowired
   private RegistrantCpdItemConfigRepository addressTypeRepository;
    
   public RegistrantCpdItemConfig save(RegistrantCpdItemConfig addressType){
       return addressTypeRepository.save(addressType);
   }
   
   public List<RegistrantCpdItemConfig> findAll(){
       return addressTypeRepository.findAll();
   }
   
   public RegistrantCpdItemConfig get(Long id){
       return addressTypeRepository.findOne(id);
   }
 /**
     * A setter method that will make mocking repo object easier
     * @param addressTypeRepository 
     */
   
   public void setRegistrantCpdItemConfigRepository(RegistrantCpdItemConfigRepository addressTypeRepository) {
         this.addressTypeRepository = addressTypeRepository;
    }

    public List<RegistrantCpdItemConfig> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
