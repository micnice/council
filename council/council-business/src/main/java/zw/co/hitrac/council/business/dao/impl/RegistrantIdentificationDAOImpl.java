/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.RegistrantIdentificationDAO;
import zw.co.hitrac.council.business.dao.repo.RegistrantIdentificationRepository;
import zw.co.hitrac.council.business.domain.Registrant;

import zw.co.hitrac.council.business.domain.RegistrantIdentification;

/**
 *
 * @author kelvin
 */
@Repository
public class RegistrantIdentificationDAOImpl implements RegistrantIdentificationDAO {

    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    private RegistrantIdentificationRepository registrantIdentificationRepository;

    public RegistrantIdentification save(RegistrantIdentification registrantIdentification) {
        return registrantIdentificationRepository.save(registrantIdentification);
    }

    public List<RegistrantIdentification> findAll() {
        return registrantIdentificationRepository.findAll();
    }

    public RegistrantIdentification get(Long id) {
        return registrantIdentificationRepository.findOne(id);
    }

    /**
     * A setter method that will make mocking repo object easier
     *
     * @param registrantIdentificationRepository
     */
    public void setRegistrantIdentificationRepository(RegistrantIdentificationRepository registrantIdentificationRepository) {
        this.registrantIdentificationRepository = registrantIdentificationRepository;
    }

    public List<RegistrantIdentification> getIdentities(Registrant registrant) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantIdentification.class);
        criteria.add(org.hibernate.criterion.Restrictions.eq("registrant", registrant));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<RegistrantIdentification> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
