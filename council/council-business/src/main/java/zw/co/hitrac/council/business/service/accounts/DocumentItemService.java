
package zw.co.hitrac.council.business.service.accounts;

import zw.co.hitrac.council.business.domain.accounts.DocumentItem;
import zw.co.hitrac.council.business.service.IGenericService;

/**
 *
 * @author Michael 

 */
public interface DocumentItemService extends IGenericService<DocumentItem> {
    
}
