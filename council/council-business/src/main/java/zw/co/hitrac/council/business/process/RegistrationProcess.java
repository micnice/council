package zw.co.hitrac.council.business.process;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.business.service.accounts.AccountService;
import zw.co.hitrac.council.business.service.accounts.AccountsParametersService;
import zw.co.hitrac.council.business.service.accounts.CustomerService;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.business.utils.DateUtil;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author Charles Chigoriwa
 * @author Michael Matiashe
 */
@Service
public class RegistrationProcess {

    @Autowired
    private GeneralParametersService generalParametersService;
    @Autowired
    private RegistrationService registrationService;
    @Autowired
    private ExamRegistrationService examRegistrationService;
    @Autowired
    private RegistrantQualificationService registrantQualificationService;
    @Autowired
    private DurationService durationService;
    @Autowired
    private RegistrantService registrantService;
    @Autowired
    private ProductIssuer productIssuer;
    @Autowired
    private RegistrantActivityProcess registrantActivityProcess;
    @Autowired
    private BillingProcess billingProcess;
    @Autowired
    private ApplicationService applicationService;
    @Autowired
    private RegistrantClearanceService registrantClearanceService;
    @Autowired
    private RegistrantDisciplinaryService registrantDisciplinaryService;
    @Autowired
    private CourseService courseService;
    @Autowired
    private RegisterService registerService;
    @Autowired
    private RegistrantConditionService registrantConditionService;
    @Autowired
    private RenewalProcess renewalProcess;
    @Autowired
    private SupExamRegistrationProcess supExamRegistrationProcess;
    @Autowired
    private IdentificationValidationRuleService validationRuleService;
    @Autowired
    private AccountsParametersService accountsParametersService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private AccountService accountService;

    @Transactional
    public Registration register(Registration registration, Application... applications) {

        if (generalParametersService.get().getLastRegistrationNumber() == null) {
            throw new CouncilException("Administrator MUST Configure Registration numbers");
        }
        Date now = new Date();

        DebtComponent debtComponent = null;
        DebtComponent debtComponent1 = null;

        boolean postbasic = false;
        boolean issueQualificationCertficate = false;

        Course course = registration.getCourse();

        Registration studentRegistration = registrationService.getCurrentRegistrationByRegister(generalParametersService.get().getStudentRegister(), registration.getRegistrant());
        Registration mainRegistration = registrationService.getCurrentRegistrationByRegister(generalParametersService.get().getMainRegister(), registration.getRegistrant());
        Registration provisionalRegistration = registrationService.getCurrentRegistrationByRegister(generalParametersService.get().getProvisionalRegister(), registration.getRegistrant());
        if ((studentRegistration != null && mainRegistration != null) || (studentRegistration != null && provisionalRegistration != null)) {
            postbasic = true;
        }

        //if there is a current student registration
        if (studentRegistration != null) {

            //clearance Letter required if student registration by pass
            checkClearance(studentRegistration);

            //Check student registration has a corresponding successful examination registration
            checkPassedExamniation(studentRegistration);

            issueQualificationCertficate = true;

            //Cheking if cources match
            if (!course.getId().equals(studentRegistration.getCourse().getId())) {
                throw new CouncilException("Courses are Mismatching");
            } else {
                //deregister studentRegistration
                studentRegistration.setDeRegistrationDate(now);
                registrationService.save(studentRegistration);

                if (postbasic) {
                    registration = registerPostBasic(registration);
                    return registration;
                }
            }
        }

        //Registering non students
        if (!isStudentRegister(registration.getRegister())) {
            if (registrantQualificationService.getRegistrantQualifications(registration.getRegistrant()).isEmpty()) {
                throw new CouncilException("Cannot Register without any Qualifications");
            }
        }

        //transfer from one register to another close registration
        if (!generalParametersService.get().getRegistrationByApplication()) {
            if (registration.getApplication() != null) {
                if (mainRegistration == null && provisionalRegistration != null) {
                    if (registration.getApplication().getApplicationPurpose().equals(ApplicationPurpose.TRANSFER)) {
                        //deregister provisional register
                        provisionalRegistration.setDeRegistrationDate(now);
                        registrationService.save(provisionalRegistration);
                    }
                }
            }
        }
        //You can not have two or more active registrations in the same register
        if (!generalParametersService.get().getRegistrationByApplication()) {
            if (!accountsParametersService.get().getStudentRegistrationPayment()) {
                if (registrationService.getCurrentRegistrationByRegister(registration.getRegister(), registration.getRegistrant()) != null) {
                    throw new CouncilException("Already registered");
                }
            }
        }
        //register registration
        boolean checkStudentRegister = isStudentRegister(registration.getRegister());
        Duration duration = durationService.getCurrentCourseDuration(course);
        if (!checkStudentRegister) {
            if (duration == null) {
                throw new CouncilException("No Registration Period set for that Course");
            }
        }

        Application application = null;
        if (!checkStudentRegister) { //If it is not student registration then create invoice and register

            // If registration is not by application, bill registrationfee and annual fee
            /*@ TODO defaulting conditions to main
             * @ Judge method
             */
            if ((isMainRegister(registration.getRegister()) && (!generalParametersService.get().getRegistrationByApplication())) || ((isSpecialistRegister(registration.getRegister()) && (!generalParametersService.get().getRegistrationByApplication()))) || (isProvisionalRegister(registration.getRegister()) && (!generalParametersService.get().getRegistrationByApplication()))) {
                //billing for initial registration
                if (!postbasic) {
                    debtComponent = billingProcess.billRegistrationProduct(registration, debtComponent);

                    productIssuer.issueProducts(registration, debtComponent, issueQualificationCertficate, course);

                    registration.setDebtComponent(debtComponent);

                    if (applications != null && applications.length >= 1) {
                        application = applications[0];
                        debtComponent1 = billingProcess.billAnnualProduct(registration, debtComponent1, application);
                    } else {
                        debtComponent1 = billingProcess.billAnnualProduct(registration, debtComponent1);
                    }
                    //Done with billing side
                    //Register now
                    registration.setDeptComponent1(debtComponent1);
                    productIssuer.issueProductPracticeCertificate(registration, debtComponent1);
                } else {
                    if (!generalParametersService.get().getRegistrationByApplication()) {
                        debtComponent = billingProcess.billRegistrationProduct(registration, debtComponent);
                        productIssuer.issueProducts(registration, debtComponent, issueQualificationCertficate, course);
                        registration.setDebtComponent(debtComponent);
                    }

                }
            }

            if (generalParametersService.get().getRegistrationByApplication()) {// Billing only application fee since registration fee is included in the application fee

                if (accountsParametersService.get().getPractisingCertificatePayment()) {
                    debtComponent = billingProcess.billPracticingCertificateProduct(registration, debtComponent);
                }
                if (!(accountsParametersService.get().getRegistrationApplicationPayment() || registration.getApplication().getApplicationPurpose().equals(ApplicationPurpose.PROVISIONAL_REGISTRATION))) {
                    debtComponent = billingProcess.billRegistrationProduct(registration, debtComponent);
                    registration.setDebtComponent(debtComponent);
                }

                if (registration.getApplication() != null) {
                    debtComponent1 = billingProcess.billAnnualProduct(registration, debtComponent1, registration.getApplication());
                } else {
                    debtComponent1 = billingProcess.billAnnualProduct(registration, debtComponent1);
                }
                productIssuer.issueProducts(registration, debtComponent1, issueQualificationCertficate, course);
                productIssuer.issueProductPracticeCertificate(registration, debtComponent);
                registration.setDeptComponent1(debtComponent1);
            }
        }

        //create registration numbers for none students
        if (!checkStudentRegister) {
            createRegistrationNumberByCourse(registration.getRegistrant(), registration.getCourse());
            registrantActivityProcess.registrationAndRenewalActivity(registration, debtComponent, duration);
        }

        //create registration numbers for students and not specific processed by board
        if (checkStudentRegister && !generalParametersService.get().getHasProcessedByBoard()) {
            createRegistrationNumberByCourse(registration.getRegistrant(), registration.getCourse());
            registrantActivityProcess.registrationAndRenewalActivity(registration, debtComponent, duration);
        }


        // Use existing registration i it exist
        if (!generalParametersService.get().getAllowMultipleRegistrations()) {
            if (mainRegistration != null) {
                registration = mainRegistration;
            }
        }

        if (generalParametersService.get().getRegistrationByApplication()) {
            if (registration.getApplication() != null) {
                Registrant registrant = registrantService.get(registration.getRegistrant().getId());
                for (RegistrantCondition rc : registrantConditionService.getRegisterRegistrantConditions(registrant)) {
                    rc.setStatus(Boolean.FALSE);
                    registrantConditionService.save(rc);
                }
                Register register = registerService.get(registration.getRegister().getId());
                registrantConditionService.saveRegisterConditions(register, registrant);
                registration.getApplication().setUsed(Boolean.TRUE);
                applicationService.save(registration.getApplication());
            }
        }

        registration = registrationService.save(registration);
        return registration;
    }

    @Transactional
    public Registration registerPostBasic(Registration registration) {

        DebtComponent debtComponent = null;
        Duration duration = null;

        Registration mainRegistration = registrationService.getCurrentRegistrationByRegister(generalParametersService.get().getMainRegister(), registration.getRegistrant());
        boolean issueQualificationCertficate = true;
        Course course = registration.getCourse();

        debtComponent = billingProcess.billRegistrationProduct(registration, debtComponent);
        productIssuer.issueProducts(registration, debtComponent, issueQualificationCertficate, course);

        if (mainRegistration != null) {
            duration = durationService.getCurrentCourseDuration(mainRegistration.getCourse());
            if (renewalProcess.hasCurrentRenewalActivity(registration.getRegistrant(), duration)) {
                if (course.getProductIssuanceType().equals(ProductIssuanceType.REGISTRATION_CERTIFICATE)) {
                    productIssuer.issueProductPracticeCertificate(mainRegistration, debtComponent);
                }
            }
        }
        registrantActivityProcess.registrationActivity(registration, debtComponent, durationService.getCurrentCourseDuration(course));
        //code for none different council required, process after exam deregistration
        if (mainRegistration != null) {
            return mainRegistration;
        }
        return registration;
    }

    public Registration registerUpGrade(Registration registration) {

        DebtComponent debtComponent = null;
        Date now = new Date();
        boolean issueQualificationCertficate = true;
        Course course = registration.getCourse();

        Registration studentRegistration = registrationService.getCurrentRegistrationByRegister(generalParametersService.get().getStudentRegister(), registration.getRegistrant());
        Registration mainRegistration = registrationService.getCurrentRegistrationByRegister(generalParametersService.get().getMainRegister(), registration.getRegistrant());

        if (mainRegistration == null) {
            throw new CouncilException("Cannot Upgrade - No Registration to Upgrade From");
        }

        if (studentRegistration == null) {
            throw new CouncilException("Cannot Upgrade - No Exam Registration found");
        }

        //clearance Letter required if student registration by pass
        checkClearance(studentRegistration);

        //Check student registration has a corresponding successful examination registration
        checkPassedExamniation(studentRegistration);

        if (!course.getId().equals(studentRegistration.getCourse().getId())) {
            throw new CouncilException("Courses are Mismatching");
        } else {

            //deregister studentRegistration
            studentRegistration.setDeRegistrationDate(now);
            registrationService.save(studentRegistration);

            //deregister mainRegistration
            mainRegistration.setDeRegistrationDate(now);
            registrationService.save(mainRegistration);

            //save upgrade registration
            registration = registrationService.save(registration);

            debtComponent = billingProcess.billRegistrationProduct(registration, debtComponent);
            productIssuer.issueProducts(registration, debtComponent, issueQualificationCertficate, course);

        }

        registrantActivityProcess.registrationActivity(registration, debtComponent, durationService.getCurrentCourseDuration(course));

        return registration;
    }

    public void checkClearance(Registration studentRegistration) {

        if (generalParametersService.get().getExamClearance()) {
            RegistrantClearance registrantClearance = registrantClearanceService.getRegistrantClearance(studentRegistration.getRegistrant(), studentRegistration.getCourse());
            if (registrantClearance == null || registrantClearance.getClearanceDate() == null) {
                throw new CouncilException("Please Upload Clearance Letter First");
            }

            if (DateUtil.DifferenceInDays(registrantClearance.getClearanceDate(), new Date()) > 30) {
                billingProcess.billLateRegistrationProduct(studentRegistration);
            }
            qualificationsNotAdd(studentRegistration.getRegistrant());
        }
    }

    public void checkPassedExamniation(Registration studentRegistration) {
        if (getPassedExamination(studentRegistration) == null) {
            if (!generalParametersService.get().getRegistrationByApplication()) {
                throw new CouncilException("Cannot register if no pass");
            }
        }
        qualificationsNotAdd(studentRegistration.getRegistrant());
    }

    public Registration captureFromFile(Registration registration) {

        registration = registrationService.save(registration);
        return registration;
    }

    public Registration captureFromPreRegFile(Registration registration) {

        registration = captureFromFile(registration);
        if (generalParametersService.get().getAllowPreRegistrantion()) {
            if (registration.getCourse().getProductIssuanceType().equals(ProductIssuanceType.REGISTRATION_CERTIFICATE)) {
                productIssuer.issueProductPracticeCertificate(registration, null);
                productIssuer.issueRegistrationOrAdditionalQualficationCertificate(registration, null);
                registrantActivityProcess.registrationAndRenewalActivity(registration, null, durationService.getCurrentCourseDuration(registration.getCourse()));
            }

        }
        return registration;
    }

    @Transactional
    public Registration registerInstitution(Registration registration) {

        DebtComponent debtComponent = null;

        if (registration.getRegister() == generalParametersService.get().getInstitutionRegister()) {

            debtComponent = billingProcess.billInstitutionRegistrationProduct(registration, debtComponent);

            registration.setDebtComponent(debtComponent);
        }

        registration = registrationService.save(registration);

        return registration;

    }


    public void qualificationsNotAdd(Registrant registrant) {

        for (ExamRegistration e : examRegistrationService.getExamRegistrations(registrant)) {
            if (e.getPassStatus() && e.getClosed()) {
                RegistrantQualification registrantQualification = registrantQualificationService.get(registrant, e.getRegistration().getCourse().getQualification());
                if (registrantQualification == null) {
                    registrantQualification = updateQualification(e, new RegistrantQualification());
                } else {
                    registrantQualification = updateQualification(e, registrantQualification);
                }
                registrantQualificationService.save(registrantQualification);
            }
        }

        //just in case exam passed marks change - removes qualification
        Registration registration = activeStudentRegister(registrant);
        if (registration != null) {
            if (!supExamRegistrationProcess.getFailedModulePapers(registration).isEmpty()) {
                for (RegistrantQualification rq : registrantQualificationService.getRegistrantQualifications(registrant)) {
                    Qualification q = registration.getCourse().getQualification();
                    if (q != null) {
                        if (rq.getQualification().equals(q)) {
                            rq.setRetired(Boolean.TRUE);
                            registrantQualificationService.save(rq);
                        }
                    }
                }
            }
        }

    }

    public RegistrantQualification updateQualification(ExamRegistration e, RegistrantQualification registrantQualification) {
        if (registrantQualification.getDateAwarded() == null) {
            registrantQualification.setDateAwarded(new Date());
        }
        registrantQualification.setStartDate(e.getRegistration().getRegistrationDate());
        registrantQualification.setEndDate(registrantClearanceService.getRegistrantClearanceDate(e.getRegistration().getRegistrant(), e.getRegistration().getCourse()));
        registrantQualification.setRegistrant(e.getRegistration().getRegistrant());
        registrantQualification.setQualification(e.getRegistration().getCourse().getQualification());
        registrantQualification.setInstitution(e.getRegistration().getInstitution());
        registrantQualification.setAwardingInstitution(generalParametersService.get().getInstitution());
        registrantQualification.setCourse(e.getRegistration().getCourse());
        return registrantQualification;
    }

    public void checkMimimumAge(Registrant registrant) {

        if (generalParametersService.get().getMinimumRegistrationAge() == null) {
            throw new CouncilException("Minimum Registration age not set");
        }
        if (registrant.getIntegerValueOfAge() < generalParametersService.get().getMinimumRegistrationAge()) {
            throw new CouncilException("Please Check date of Birth. Individual's age below set system age");
        }
    }

    public Registration activeRegisterNotStudentRegister(Registrant registrant) {
        //Underlying Rule can only be two active registrations -
        //One in Student and The other registration not in student register
        for (Registration registration : registrationService.getActiveRegistrations(registrant)) {
            if (!registration.getRegister().equals(generalParametersService.get().getStudentRegister())) {
                return registration;
            }
        }
        return null;
    }

    public Registration activeStudentRegister(Registrant registrant) {
        //Underlying Rule can only be two active registrations -
        //One in Student and The other registration not in student register
        for (Registration registration : registrationService.getActiveRegistrations(registrant)) {
            if (registration.getRegister().equals(generalParametersService.get().getStudentRegister())) {
                return registration;
            }
        }
        return null;
    }

    public Boolean studentRegistration(Registrant registrant) {

        Registration registration = activeStudentRegister(registrant);
        if (registration != null) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    public Boolean studentNoneRegistration(Registrant registrant) {

        Registration registration = activeRegisterNotStudentRegister(registrant);
        if (registration == null) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }
    }

    @Transactional
    public Registrant saveRegistrant(Registrant registrant) {

        if (registrant.getBirthDate() != null) {
            checkMimimumAge(registrant);
        }
        if (registrant.getIdNumber() != null) {
            validateIdentification(registrant);
        }

        if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
            if (registrant.getRegistrationNumber() == null) {
                registrant.setRegistrationNumber(generatedRegistrationNumber());

                saveGeneratedRegistrationNumber();
            }
        }

        updateCustomerAndAccountDetails(registrant);

        return registrantService.save(registrant);
    }


    @Transactional
    public Registrant reportDeathRegistrant(Registrant registrant) {

        registrantActivityProcess.deathRegistrationRegistrantActivity(registrant, registrant.getComment());
        return registrantService.save(registrant);
    }

    @Transactional
    public Registrant retiredRegistrantActivity(Registrant registrant) {
        if (registrant.getRetired()) {
            if (StringUtils.isEmpty(registrant.getComment())) {
                throw new CouncilException("Comment Required");
            }
            registrantActivityProcess.retiredRegistrantActivity(registrant, registrant.getComment());
        }
        return registrantService.save(registrant);
    }

    @Transactional
    public Registrant voidedRegistrantActivity(Registrant registrant) {
        if (registrant.getVoided()) {
            if (StringUtils.isEmpty(registrant.getComment())) {
                throw new CouncilException("Comment Required");
            }
            registrantActivityProcess.voidedRegistrantActivity(registrant, registrant.getComment());
            Registration registration = activeRegisterNotStudentRegister(registrant);
            if (registration != null) {
                registration.setRegistrantStatus(this.generalParametersService.get().getVoidedStatus());
                registration.setDeRegistrationDate(new Date());
                this.registrationService.save(registration);
            }
            registration = activeStudentRegister(registrant);
            if (registration != null) {
                registration.setRegistrantStatus(this.generalParametersService.get().getVoidedStatus());
                registration.setDeRegistrationDate(new Date());
                this.registrationService.save(registration);
            }
        }
        return registrantService.save(registrant);
    }

    @Transactional
    public Registrant preRegistrant(Registrant registrant) {

        if (generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
            if (registrant.getRegistrationNumber() == null) {
                registrant.setRegistrationNumber(generatedRegistrationNumber());
                saveGeneratedRegistrationNumber();
            }
        }
        return registrantService.save(registrant);
    }

    public Registrant save(Registrant registrant) {

        return registrantService.save(registrant);
    }

    @Transactional
    public Registrant createRegistrationNumber(Registrant registrant) {

        if (registrant.getRegistrationNumber() == null) {
            if (generalParametersService.get().getRegistrationNumberByYear()) {
                final Calendar c = Calendar.getInstance();
                String month = "" + (c.get(Calendar.MONTH) + 1);
                if (month.length() == 1) {
                    month = "0".concat(month);
                }
                String year = "" + c.get(Calendar.YEAR);
                year = year.concat(month);
                year = year.concat(generatedRegistrationNumber());
                registrant.setRegistrationNumber(year);
            } else {
                registrant.setRegistrationNumber(generatedRegistrationNumber());
            }
            registrant = registrantService.save(registrant);
            saveGeneratedRegistrationNumber();
        }
        return registrantService.save(registrant);
    }

    public String generatedRegistrationNumber() {

        int lastRegistrationNumber = generalParametersService.get().getLastRegistrationNumber();
        if (generalParametersService.get().getRegistrationNumberIncrement() != null && generalParametersService.get().getRegistrationNumberIncrement() != 0) {
            lastRegistrationNumber = lastRegistrationNumber + generalParametersService.get().getRegistrationNumberIncrement();
        } else {
            lastRegistrationNumber = lastRegistrationNumber + 1;
        }
        return String.valueOf(lastRegistrationNumber);

    }

    @Transactional
    public void saveGeneratedRegistrationNumber() {

        int lastRegistrationNumber = generalParametersService.get().getLastRegistrationNumber();
        GeneralParameters generalParameters = generalParametersService.findAll().get(0);
        lastRegistrationNumber = lastRegistrationNumber + 1;
        generalParameters.setLastRegistrationNumber(lastRegistrationNumber);
        generalParametersService.save(generalParameters);
    }

    /**
     * Method to validate identification types, define your own rules!
     *
     * @param registrant
     */
    public void validateIdentification(Registrant registrant) {

        List<IdentificationValidationRule> validationRules
                = validationRuleService.findByCitizenshipAndIdentificationType(registrant.getCitizenship(),
                registrant.getIdentificationType());

        if (!registrantService.hasValidIdentifications(registrant, validationRules)) {

            throw new CouncilException("Invalid Identification Number, check identification number format");

        }
    }

    public void processDisciplinaryCase(RegistrantDisciplinary registrantDisciplinary) {

        RegistrantStatus suspendedStatus = generalParametersService.get().getSuspendedStatus();
        if (suspendedStatus == null) {
            throw new CouncilException("Suspended Status Parameter not Set");
        }
        if (registrantDisciplinary.getCaseOutcome() != null) {
            if (registrantDisciplinary.getCaseOutcome().isDates()) {
                if (registrantDisciplinary.getPenaltyStartDate() == null) {
                    throw new CouncilException("Penalty Start date is required");
                }
                if (registrantDisciplinary.getPenaltyEndDate() == null) {
                    throw new CouncilException("Penalty End date is required");
                }
            }
            if (suspendedStatus.equals(registrantDisciplinary.getCaseOutcome().getRegistrantStatus())) {
                Registration registration = activeRegisterNotStudentRegister(registrantDisciplinary.getRegistrant());
                if (registration != null) {
                    registration.setRegistrantStatus(suspendedStatus);
                    registration = registrationService.save(registration);
                    registrantActivityProcess.suspenedRegistrationRegistrantActivity(registration, registrantDisciplinary);
                }
                registration = activeStudentRegister(registrantDisciplinary.getRegistrant());
                if (registration != null) {
                    registration.setRegistrantStatus(suspendedStatus);
                    registrationService.save(registration);
                    registrantActivityProcess.suspenedRegistrationRegistrantActivity(registration, registrantDisciplinary);
                }
            }

            registrantDisciplinaryService.save(registrantDisciplinary);
        } else {
            registrantDisciplinaryService.save(registrantDisciplinary);
        }

    }

    @Transactional
    public Registrant deRegisterRegistrant(Registrant registrant) {

        this.registrantActivityProcess.deRegistrationRegistrantActivity(registrant, registrant.getComment());
        Registration registration = activeRegisterNotStudentRegister(registrant);
        //none student registration
        deRegister(registration);
        registration = activeStudentRegister(registrant);
        //none student registration
        deRegister(registration);
        return registrant;

    }

    private void deRegister(Registration registration) {
        if (this.generalParametersService.get().getDeRegisteredStatus() == null) {
            throw new CouncilException("De-Registration Status Not Set");
        }
        if (registration != null) {
            registration.setRegistrantStatus(this.generalParametersService.get().getDeRegisteredStatus());
            registration.setDeRegistrationDate(new Date());
            this.registrationService.save(registration);
        }
    }

    private boolean isStudentRegister(Register register) {

        Register studentRegister = generalParametersService.get().getStudentRegister();
        if (studentRegister == null) {
            throw new CouncilException("Student Register not set");
        }
        return register.equals(studentRegister);
    }

    private boolean isMainRegister(Register register) {

        Register mainRegister = generalParametersService.get().getMainRegister();
        if (mainRegister == null) {
            throw new CouncilException("Main Register not set");
        }
        return register.equals(mainRegister);
    }

    private boolean isProvisionalRegister(Register register) {

        Register provisionalRegister = generalParametersService.get().getProvisionalRegister();
        if (provisionalRegister == null) {
            throw new CouncilException("Provisional Register not set");
        }
        return register.equals(provisionalRegister);
    }

    private boolean isSpecialistRegister(Register register) {

        Register specialistRegister = generalParametersService.get().getSpecialistRegister();
        if (specialistRegister == null) {
            throw new CouncilException("Specialist Register not set");
        }
        return register.equals(specialistRegister);
    }

    public ExamRegistration getPassedExamination(Registration studentRegistration) {

        Set<ExamRegistration> examRegistrations = studentRegistration.getExamRegistrations();
        for (ExamRegistration examRegistration : examRegistrations) {
            if (examRegistration.getPassStatus() && examRegistration.getClosed()) {
                return examRegistration;
            }

        }
        return null;
    }

    @Transactional
    public Registrant createRegistrationNumberByCourse(Registrant registrant, Course course) {

        if (generalParametersService.get().getRegistrationnumberIsByCourse()) {
            if (registrant.getRegistrationNumber() == null || generalParametersService.get().getAllowChangeOfRegistrationNumberAfterTransfer()) {
                registrant.setRegistrationNumber(getGeneratedRegistrationNumberByCourse(registrant, course));
                registrant = registrantService.save(registrant);
                saveGeneratedCourseRegistrationNumber(course);
            }
        } else {
            registrant = createRegistrationNumber(registrant);
        }
        return registrant;
    }

    public String getGeneratedRegistrationNumberByCourse(Registrant registrant, Course course) {
        String n = "";
        n = n.concat(generalParametersService.get().getCouncilPrefixForRegistrationNumber());
        n = n.concat(course.getPrefixName());
        n = n.concat(generatedCourseRegistrationNumber(course));
        registrant.setRegistrationNumber(n);
        return n;
    }

    public String generatedCourseRegistrationNumber(Course course) {

        int lastCourseRegistrationNumber = course.getLastCourseRegistrationNumber();
        int lengthOfRegNumber = 6;


        if (course.getLastCourseRegistrationNumber() != null) {
            lastCourseRegistrationNumber = lastCourseRegistrationNumber + 1;
        } else {
            throw new CouncilException("Last Course Registration Number not Set");
        }

        if (generalParametersService.get().getRegistrationNumberHasZeroPrefix()) {
            String formattedLastCourseRegistrationNumber = String.valueOf(lastCourseRegistrationNumber);
            formattedLastCourseRegistrationNumber = org.apache.commons.lang.StringUtils.leftPad(formattedLastCourseRegistrationNumber, lengthOfRegNumber, "0");
            return formattedLastCourseRegistrationNumber;
        } else {
            return String.valueOf(lastCourseRegistrationNumber);
        }
    }

    @Transactional
    public void saveGeneratedCourseRegistrationNumber(Course course) {

        int lastCourseRegistrationNumber = course.getLastCourseRegistrationNumber();
        lastCourseRegistrationNumber = lastCourseRegistrationNumber + 1;
        course.setLastCourseRegistrationNumber(lastCourseRegistrationNumber);
        courseService.save(course);
    }

    public Boolean checkInitialRegistrationForCPD(Registrant registrant, Duration duration) {
        Registration registration = activeRegisterNotStudentRegister(registrant);
        if (registration == null) {
            return false;
        }
        if (registration.getRegistrationDate().after(duration.getStartDate())) {
            return false;
        } else {
            return true;
        }
    }

    public Boolean validateCPDSRequired(Registrant registrant, CouncilDuration councilDuration) {
        //TODO: Validate CPDS for individuals already registered and without clearance date
        Registration registration = activeRegisterNotStudentRegister(registrant);
        Set<Duration> durationsInCouncilDurations = councilDuration.getDurations();
        if (generalParametersService.get().getExamClearance()) {
            Date clearanceDate = registrantClearanceService.getLastClearanceDate(registrant);
            if (clearanceDate != null) {
                for (Duration duration : durationsInCouncilDurations) {
                    if (duration.getEndDate().before(clearanceDate)) {
                        return false;
                    }
                }

            }
        }

        for (Duration duration : durationsInCouncilDurations) {
            if (duration.getEndDate().before(registration.getRegistrationDate())) {
                return false;
            }
        }

        return true;
    }

    public void editClearanceDate(Registrant registrant, Course course, Date clearanceDate) {

        RegistrantClearance registrantClearance = registrantClearanceService.getRegistrantClearance(registrant, course);
        if (registrantClearance == null) {
            throw new CouncilException("Clearance date cannot be changed, cannot find required record");
        }
        registrantClearance.setClearanceDate(clearanceDate);
        registrantClearanceService.save(registrantClearance);
        throw new CouncilException("Clearance date changed to " + DateUtil.convertDateToString(clearanceDate));
    }

    public void updateCustomerAndAccountDetails(Registrant registrant) {

        if (registrant.getCustomerAccount() != null && registrant.getCustomerAccount().getAccount() != null) {
            Customer customer = customerService.get(registrant.getCustomerAccount().getId());
            Account account = accountService.get(customer.getAccount().getId());

            if (!customer.getCustomerName().equalsIgnoreCase(registrant.getFullname())) {
                customer.setCustomerName(registrant.getFullname());
                customerService.save(customer);
            }
            if (!account.getName().equalsIgnoreCase(registrant.getFullname())) {
                account.setName(registrant.getFullname());
                accountService.save(account);
            }
        }

    }

}
