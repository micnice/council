package zw.co.hitrac.council.web.pages.institution;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxCheckBox;
import org.apache.wicket.extensions.yui.calendar.DatePicker;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBoxMultipleChoice;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.ApplicationPurpose;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.process.ApplicationProcess;
import zw.co.hitrac.council.business.service.ApplicationService;
import zw.co.hitrac.council.business.service.CouncilDurationService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.ServiceService;
import zw.co.hitrac.council.business.service.UserService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.CouncilDurationListModel;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.accounts.payments.PaymentPage;
import zw.co.hitrac.council.web.pages.registration.RegistrantViewPage;
import zw.co.hitrac.council.web.utility.CustomDateTextField;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

/**
 *
 * @author Michael Matiashe
 */
public class InstitutionApplicationPage extends TemplatePage {

    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private ApplicationProcess applicationProcess;
    @SpringBean
    private DebtComponentService debtComponentService;
    @SpringBean
    private ServiceService serviceService;
    @SpringBean
    private ApplicationService applicationService;
    @SpringBean
    private UserService userService;
    @SpringBean
    private CouncilDurationService councilDurationService;
    @SpringBean
    private GeneralParametersService generalParametersService;

    public InstitutionApplicationPage(final Institution institution) {
        menuPanelManager.getApplicationPanel().setTopMenuCurrent(true);
        CompoundPropertyModel<Application> model = new CompoundPropertyModel<Application>(new Application());
        setDefaultModel(model);
        model.getObject().setInstitution(institution);
        model.getObject().setRegistrant(institution.getRegistrant());
        model.getObject().setApplicationPurpose(ApplicationPurpose.INSTITUTION_REGISTRATION);
        model.getObject().setApplicationDate(new Date());
        model.getObject().setApplicationStatus(Application.APPLICATIONPENDING);

        Form<Application> form = new Form<Application>("form", (IModel<Application>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    Institution institution1 = institutionService.get(institution.getId());
                    institution1.setServices(getModelObject().getServices());
                    institutionService.save(institution1);
                    Application application = applicationProcess.apply(getModelObject());
                    setResponsePage(new PaymentPage(institution.getCustomerAccount(), debtComponentService.getDebtComponents(institution.getCustomerAccount())));

                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };
        form.add(new Label("currentStatus", model.getObject().getApplicationTextStatus()));
        form.add(new CustomDateTextField("proposedCommencementDate") {
            @Override
            protected void onConfigure() {
                if (generalParametersService.get().getHasProcessedByBoard()) {
                    setVisible(Boolean.FALSE);
                } else {
                    setRequired(true).add(new ErrorBehavior()).add(new DatePicker());
                }
            }
        });
        form.add(new Label("applicationDate", DateUtil.getDate(model.getObject().getApplicationDate())));
        form.add(new Label("institution.name"));
        form.add(new Label("registrant.registrationNumber", institution.getRegistrant().getRegistrationNumber()));
        form.add(new Label("institution.institutionCategory", institution.getInstitutionCategory()));
        form.add(new Label("institution.institutionType", institution.getInstitutionType()));


        final List<String> applicationStates = Arrays.asList(new String[]{Application.APPLICATIONPENDING});

        form.add(new DropDownChoice("applicationStatus", applicationStates));
        final WebMarkupContainer wmc = new WebMarkupContainer("wmc");
        wmc.setVisible(false);
        wmc.setOutputMarkupPlaceholderTag(true);
        form.add(wmc);
        form.add(new AjaxCheckBox("advanced", new PropertyModel(wmc, "visible")) {
            @Override
            protected void onUpdate(AjaxRequestTarget art) {
                art.add(wmc);
            }
        });
        wmc.add(new CheckBoxMultipleChoice("services", serviceService.findAll()));
        form.add(new TextField<String>("ownerName"));
        form.add(new TextField<String>("ownerProfession"));
        form.add(new TextField<String>("ownerTelephone"));
        form.add(new TextField<String>("ownerEmail"));


        form.add(new Label("approvedByLabel", "Approved By") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new DropDownChoice("approvedBy", userService.getAuthorisers()) {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new DropDownChoice("processedBy", userService.getApplicationpProcessors()) {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new Label("dateApprovedLabel", "Decision Date") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new Label("dateProcessedLabel", "Final Processing Date") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new Label("processedByLabel", "Prossesed By") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new CustomDateTextField("dateApproved") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new CustomDateTextField("dateProcessed") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new Label("serviceOffered", "Service"));

        add(form);

        add(new Label("institution.registrant.fullname"));
        add(new FeedbackPanel("feedback"));
        form.add(
                new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {

                setResponsePage(new RegistrantViewPage(institution.getRegistrant().getId()));


            }
        });

        form.add(new Label("currentDurationLabel", "Duration"));

        form.add(new DropDownChoice("councilDuration", new CouncilDurationListModel(councilDurationService)).setRequired(true));
    }

    public InstitutionApplicationPage(Long id) {
        menuPanelManager.getApplicationPanel().setTopMenuCurrent(true);
        final CompoundPropertyModel<Application> model = new CompoundPropertyModel<Application>(applicationService.get(id));
        setDefaultModel(model);
        Institution institution = model.getObject().getInstitution();

        Form<Application> form = new Form<Application>("form", (IModel<Application>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    Application application = applicationProcess.approve(getModelObject());
                    Institution institution = institutionService.save(getModelObject().getInstitution());
                    setResponsePage(new InstitutionApplicationViewPage(getModelObject().getId()));

                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };
        form.add(new Label("currentStatus", model.getObject().getApplicationTextStatus()));
        form.add(new CustomDateTextField("proposedCommencementDate"){
            @Override
            protected void onConfigure() {
                setVisible(!generalParametersService.get().getHasProcessedByBoard());
            }
        });
        form.add(new Label("applicationDate", DateUtil.getDate(model.getObject().getApplicationDate())));
        form.add(new Label("institution.name"));
        form.add(new Label("registrant.registrationNumber", institution.getRegistrant().getRegistrationNumber()));
        form.add(new Label("institution.institutionCategory", institution.getInstitutionCategory().getName()));
        form.add(new Label("institution.institutionType", institution.getInstitutionType()));
        form.add(new Label("approvedByLabel", "Approved By"));
        form.add(new DropDownChoice("approvedBy", userService.getCouncilProcessors()));
        form.add(new DropDownChoice("processedBy", userService.getAuthorisers()));
        form.add(new Label("dateApprovedLabel", "Decision Date"));
        form.add(new Label("dateProcessedLabel", "Final Processing Date"));
        form.add(new Label("processedByLabel", "Prossesed By"));
        form.add(new CustomDateTextField("dateApproved").add(DatePickerUtil.getDatePicker()));
        form.add(new CustomDateTextField("dateProcessed").add(DatePickerUtil.getDatePicker()));


        final List<String> applicationStates = Arrays.asList(new String[]{Application.APPLICATIONPENDING, Application.APPLICATIONAPPROVED,
            Application.APPLICATIONDECLINED, Application.APPLICATION_AWAITING_COUNCIL_APPROVAL, Application.APPLICATIONUNDERREVIEW, Application.APPLICATION_IN_CIRCULATION});

        form.add(new DropDownChoice("applicationStatus", applicationStates));
        form.add(new Label("serviceOffered", "Service") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.TRUE);
            }
        });

        final WebMarkupContainer wmc = new WebMarkupContainer("wmc");
        wmc.setVisible(false);
        wmc.setOutputMarkupPlaceholderTag(true);
        form.add(wmc.setVisible(Boolean.FALSE));
        form.add(new AjaxCheckBox("advanced", new PropertyModel(wmc, "visible")) {
            @Override
            protected void onUpdate(AjaxRequestTarget art) {
                art.add(wmc);
            }

            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        wmc.add(new CheckBoxMultipleChoice("services") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new TextField<String>("ownerName"));
        form.add(new TextField<String>("ownerProfession"));
        form.add(new TextField<String>("ownerTelephone"));
        form.add(new TextField<String>("ownerEmail"));


        add(form);

        add(new Label("institution.registrant.fullname"));
        add(new FeedbackPanel("feedback"));
        form.add(
                new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {

                setResponsePage(new RegistrantViewPage(model.getObject().getRegistrant().getId()));


            }
        });

        form.add(new Label("currentDurationLabel", "Duration"));

        form.add(new DropDownChoice("councilDuration", new CouncilDurationListModel(councilDurationService)).setRequired(true));
    }
}
