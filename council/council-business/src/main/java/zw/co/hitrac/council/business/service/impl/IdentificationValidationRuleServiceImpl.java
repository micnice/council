package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.IdentificationValidationRuleDAO;
import zw.co.hitrac.council.business.domain.Citizenship;
import zw.co.hitrac.council.business.domain.IdentificationType;
import zw.co.hitrac.council.business.domain.IdentificationValidationRule;
import zw.co.hitrac.council.business.service.IdentificationValidationRuleService;

import java.util.List;

/**
 * Created by clive on 6/16/15.
 */

@Service
@Transactional
public class IdentificationValidationRuleServiceImpl implements IdentificationValidationRuleService{

    @Autowired
    private IdentificationValidationRuleDAO identificationValidationRuleDAO;

    @Override
    @Transactional
    public IdentificationValidationRule save(IdentificationValidationRule identificationValidationRule) {

        return identificationValidationRuleDAO.save(identificationValidationRule);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    @Override
    public List<IdentificationValidationRule> findAll() {

        return identificationValidationRuleDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    @Override
    public IdentificationValidationRule get(Long id) {

        return identificationValidationRuleDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    @Override
    public List<IdentificationValidationRule> findAll(Boolean retired) {

        return identificationValidationRuleDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    @Override
    public List<IdentificationValidationRule> findByCitizenshipAndIdentificationType(Citizenship citizenship,
                                                                               IdentificationType identificationType){
        return identificationValidationRuleDAO.findByCitizenshipAndIdentificationType(citizenship, identificationType);
    }
}
