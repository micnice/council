/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author clive
 */
@Entity
@Table(name = "oldcertificateissued")
@Audited
public class OldCertificateIssued extends BaseIdEntity implements Serializable {

    private Long certificateProcessRecordID;
    private Registrant registrant;
    private Date dateRequested;
    private String certificateType;
    private String shortDescription;

    public Long getCertificateProcessRecordID() {
        return certificateProcessRecordID;
    }

    public void setCertificateProcessRecordID(Long certificateProcessRecordID) {
        this.certificateProcessRecordID = certificateProcessRecordID;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    public Registrant getRegistrant() {
        return registrant;
    }

    public void setRegistrant(Registrant registrant) {
        this.registrant = registrant;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDateRequested() {
        return dateRequested;
    }

    public void setDateRequested(Date dateRequested) {
        this.dateRequested = dateRequested;
    }

    public String getCertificateType() {
        return certificateType;
    }

    public void setCertificateType(String certificateType) {
        this.certificateType = certificateType;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }
}