package zw.co.hitrac.council.business.dao.examinations.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.examinations.ExamDao;
import zw.co.hitrac.council.business.dao.repo.examinations.ExamRepository;
import zw.co.hitrac.council.business.domain.ModulePaper;
import zw.co.hitrac.council.business.domain.examinations.Exam;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;

/**
 *
 * @author tidza
 */
@Repository
public class ExamDaoImpl implements ExamDao {

    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    private ExamRepository examRepository;

    public Exam save(Exam t) {
        return examRepository.save(t);
    }

    public List<Exam> findAll() {
        return examRepository.findAll();
    }

    public Exam get(Long id) {
        return examRepository.findOne(id);
    }

    public void setExamRepository(ExamRepository examRepository) {
        this.examRepository = examRepository;
    }

    public List<Exam> findAll(ExamSetting examSetting) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Exam.class);
        criteria.add(Restrictions.eq("examSetting", examSetting));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    public List<ModulePaper> findExamPapers(ExamSetting examSetting) {
        return entityManager.createQuery("select DISTINCT e.modulePaper from Exam e where e.examSetting=:examSetting").setParameter("examSetting", examSetting).getResultList();
    }

    public Exam getExam(ExamSetting examSetting, ModulePaper modulePaper) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Exam.class);
        criteria.add(Restrictions.eq("examSetting", examSetting));
        criteria.add(Restrictions.eq("modulePaper", modulePaper));
        return (Exam) criteria.uniqueResult();
    }

    public List<Exam> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
