package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;

import javax.persistence.Table;
import java.io.Serializable;

/**
 *
 * @author Charles Chigoriwa
 */
@Table(name="debtcomponents")
@Audited
public class DebtComponents implements Serializable {
    
    private DebtComponent penaltyComponent;
    private DebtComponent transactionComponent;

    public DebtComponent getPenaltyComponent() {
        return penaltyComponent;
    }

    public void setPenaltyComponent(DebtComponent penaltyComponent) {
        this.penaltyComponent = penaltyComponent;
    }

    public DebtComponent getTransactionComponent() {
        return transactionComponent;
    }

    public void setTransactionComponent(DebtComponent transactionComponent) {
        this.transactionComponent = transactionComponent;
    }
    
    
}
