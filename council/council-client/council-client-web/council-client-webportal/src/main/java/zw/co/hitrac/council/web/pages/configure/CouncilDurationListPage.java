package zw.co.hitrac.council.web.pages.configure;

import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.service.CouncilDurationService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

/**
 *
 * @author Charles Chigoriwa
 */
public class CouncilDurationListPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private CouncilDurationService councilDurationService;

    public CouncilDurationListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new CouncilDurationEditPage(null));
            }
        });

        IModel<List<CouncilDuration>> model = new LoadableDetachableModel<List<CouncilDuration>>() {
            @Override
            protected List<CouncilDuration> load() {
                return councilDurationService.findAll();
            }
        };

        PropertyListView<CouncilDuration> eachItem = new PropertyListView<CouncilDuration>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<CouncilDuration> item) {
                Link<CouncilDuration> viewLink = new Link<CouncilDuration>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new CouncilDurationViewPage(getModelObject().getId()));
                    }
                };
                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                viewLink.add(new Label("name"));
                item.add(new Label("textStatus"));
            }
        };

        add(eachItem);
    }
}
