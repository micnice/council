/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao;

import java.util.List;
import zw.co.hitrac.council.business.domain.Conditions;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.CourseCondition;
import zw.co.hitrac.council.business.domain.Register;

/**
 *
 * @author kelvin
 */
public interface CourseConditionDAO extends IGenericDAO<CourseCondition> {

    public List<Conditions> getConditions(Course course, Register register);
}
