package zw.co.hitrac.council.business.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.CouncilDurationDAO;
import zw.co.hitrac.council.business.dao.repo.CouncilDurationRepository;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Duration;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Michael Matiashe
 */
@Repository
public class CouncilDurationDAOImpl implements CouncilDurationDAO {

    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    private CouncilDurationRepository durationRepository;

    public CouncilDuration save(CouncilDuration duration) {
        return durationRepository.save(duration); //Make sure every duration is in one active period only!
    }

    public List<CouncilDuration> findAll() {
        return durationRepository.findAll();
    }

    public CouncilDuration get(Long id) {
        return durationRepository.findOne(id);
    }

    /**
     * A setter method that will make mocking repo object easier
     *
     * @param durationRepository
     */
    public void setDurationRepository(CouncilDurationRepository durationRepository) {
        this.durationRepository = durationRepository;
    }

    public CouncilDuration getCouncilDurationFromDuration(Duration duration) {

        return duration.getCouncilDuration();
    }

    public List<CouncilDuration> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
       public List<CouncilDuration> getActiveCouncilDurations() {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(CouncilDuration.class);
            criteria.add(Restrictions.eq("active", true));        
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }
}
