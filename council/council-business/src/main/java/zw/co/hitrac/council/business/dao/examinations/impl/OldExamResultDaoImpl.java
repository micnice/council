package zw.co.hitrac.council.business.dao.examinations.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.examinations.OldExamResultDao;
import zw.co.hitrac.council.business.dao.repo.examinations.OldExamResultRepository;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.examinations.Exam;
import zw.co.hitrac.council.business.domain.examinations.OldExamResult;

/**
 *
 * @author tidza
 */
@Repository
public class OldExamResultDaoImpl implements OldExamResultDao {

    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    private OldExamResultRepository oldExamResultRepository;

    public OldExamResult save(OldExamResult t) {
        return oldExamResultRepository.save(t);
    }

    public List<OldExamResult> findAll() {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(OldExamResult.class);
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public OldExamResult get(Long id) {
        return oldExamResultRepository.findOne(id);
    }

    public void setOldExamResultRepository(OldExamResultRepository oldExamResultRepository) {
        this.oldExamResultRepository = oldExamResultRepository;
    }
    
    public List<OldExamResult> examResults(Registrant registrant){
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(OldExamResult.class);
        criteria.add(Restrictions.eq("registrant", registrant));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<OldExamResult> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
