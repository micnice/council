package zw.co.hitrac.council.webservices.api;

import java.io.Serializable;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import zw.co.hitrac.council.business.domain.Title;

/**
 *
 * @author Daniel Nkhoma
 */
@Path("/titles")
@Produces(MediaType.APPLICATION_XML)
@Consumes(MediaType.APPLICATION_XML)
public interface TitleWebService extends Serializable {

    @GET
    @Path("list")
    public List<Title> titles();
    
    @GET
    @Path(value = "title/{id}")
    public Title getTitle(@PathParam(value = "id") Long id);
}
