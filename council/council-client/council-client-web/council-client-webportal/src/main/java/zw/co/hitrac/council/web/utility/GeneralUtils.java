package zw.co.hitrac.council.web.utility;

//~--- non-JDK imports --------------------------------------------------------

import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.business.utils.Role;

//~--- JDK imports ------------------------------------------------------------

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author charlesc
 */
public class GeneralUtils implements Serializable {

    private static final String SEPARATOR = ",";

    public static String getRoles(Role... roles) {
        if ((roles == null) || (roles.length < 1)) {
            throw new CouncilException("Roles can not be empty");
        }

        StringBuilder strRoles = new StringBuilder();

        for (Role role : roles) {
            strRoles.append(role.getRoleName()).append(SEPARATOR);
        }

        return strRoles.substring(0, strRoles.lastIndexOf(SEPARATOR));
    }

    /**
     *
     * @param start year to start in
     * @param end year to end in inclusive
     *
     * Generates a list of integers from start to end
     * @return
     */
    public static List<Integer> generateYearList(int start, int end) {

        List<Integer> yearList = new ArrayList<Integer>();

        for (int year = start; year <= end; year++) {

            yearList.add(year);
        }

        return yearList;
    }

    public static List<Integer> generateYearList() {

        int endYear = DateUtil.getYearFromDate(new Date());
        int startYear = endYear - 3;

        return generateYearList(startYear, endYear);
    }
    


}


//~ Formatted by Jindent --- http://www.jindent.com
