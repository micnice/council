package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.RegistrantCPDCategoryDAO;
import zw.co.hitrac.council.business.domain.RegistrantCPDCategory;
import zw.co.hitrac.council.business.service.RegistrantCPDCategoryService;

import java.util.List;

/**
 *
 * @author Constance Mabaso
 */
@Service
@Transactional
public class RegistrantCPDCategoryServiceImpl implements RegistrantCPDCategoryService {

    @Autowired
    private RegistrantCPDCategoryDAO registrantCPDCategoryDAO;

    @Transactional
    public RegistrantCPDCategory save(RegistrantCPDCategory registrantCPDCategory) {
        return registrantCPDCategoryDAO.save(registrantCPDCategory);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantCPDCategory> findAll() {
        return registrantCPDCategoryDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantCPDCategory get(Long id) {
        return registrantCPDCategoryDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantCPDCategory> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setRegistrantCPDCategoryDAO(RegistrantCPDCategoryDAO registrantCPDCategoryDAO) {

        this.registrantCPDCategoryDAO = registrantCPDCategoryDAO;
    }
}
