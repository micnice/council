package zw.co.hitrac.council.web.pages.accounts;

import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.service.accounts.ProductService;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.reports.ProductListReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Charles Chigoriwa
 */
public class ProductListPage extends IInventoryModulePage {

    @SpringBean
    private ProductService productService;
    private HrisComparator hrisComparator = new HrisComparator();

    public ProductListPage() {

        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new ProductEditPage(null));
            }
        });

        final IModel<List<Product>> model = new LoadableDetachableModel<List<Product>>() {
            @Override
            protected List<Product> load() {
                return ((List<Product>) hrisComparator.sort(productService.findAll()));
            }
        };

        PropertyListView<Product> eachItem = new PropertyListView<Product>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<Product> item) {
                Link<Product> viewLink = new Link<Product>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new ProductViewPage(getModelObject().getId()));
                    }
                };
                item.add(viewLink);
                viewLink.add(new Label("name"));
                item.add(new Label("register"));
                item.add(new Label("code"));
                item.add(new Label("retired"));
            }
        };

        add(eachItem);

        add(new Link("report") {
            @Override
            public void onClick() {
                Map parameters = new HashMap();
                ProductListReport productListReport = new ProductListReport();
                try {
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(productListReport, contentType, ((List<Product>) hrisComparator.sort(productService.findAll(Boolean.FALSE))), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);
                    resource.respond(a);
                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });
        
        add(new Link("retired") {
            @Override
            public void onClick() {
                Map parameters = new HashMap();
                ProductListReport productListReport = new ProductListReport();
                try {
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(productListReport, contentType, ((List<Product>) hrisComparator.sort(productService.findAll(Boolean.TRUE))), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);
                    resource.respond(a);
                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });
    }
}
