/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.*;

import java.util.Date;
import java.util.List;

/**
 *
 * @author hitrac
 */
public interface RegistrantDisciplinaryService extends IGenericService<RegistrantDisciplinary> {

    public List<RegistrantDisciplinary> getCurrentUnResolved(Registrant registrant);

    public List<RegistrantDisciplinary> find(MisconductType misconductType, int startYear, int endYear);

    public List<RegistrantDisciplinary> find(MisconductType misconductType, int yearReported);

    public String getPendingCases(Registrant registrant);
    
    public List<RegistrantDisciplinary> getGuiltyResolvedCases(Registrant registrant);

    public List<RegistrantDisciplinary> getRegistrantDisciplinaries(Registrant registrant);

    public List<RegistrantDisciplinary> getDisciplinariesByDates(Date startDate, Date endDate, CaseOutcome caseOutcome, MisconductType misconductType, String caseStatus);
    
    public Boolean registrantSuspendedStatus(Registrant registrant);
}
