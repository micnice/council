
package zw.co.hitrac.council.business.dao.accounts;

import zw.co.hitrac.council.business.dao.IGenericDAO;
import zw.co.hitrac.council.business.domain.accounts.AccountsParameters;

/**
 *
 * @author Kelvin Goredema
 * 
 */
public interface AccountsParametersDAO  extends IGenericDAO<AccountsParameters> {
    
    
    
}
