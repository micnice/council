package zw.co.hitrac.council.web.utility;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.datetime.DateConverter;
import org.apache.wicket.datetime.PatternDateConverter;
import org.apache.wicket.datetime.markup.html.form.DateTextField;
import org.apache.wicket.model.IModel;

import java.util.Date;
import java.util.Locale;

//~--- JDK imports ------------------------------------------------------------

/**
 *
 * @author Charles Chigoriwa
 */
public class CustomDateTextField extends DateTextField {

    private static String UK_DATE_PATTERN = "dd/MM/yyyy";

    public CustomDateTextField(String id) {
        this(id, new PatternDateConverter(UK_DATE_PATTERN, true));
    }

    public CustomDateTextField(String id, DateConverter converter) {
        super(id, converter);
    }

    public CustomDateTextField(String id, Locale locale) {
        super(id, DateTimeUtils.getConverter(locale));
    }

    public CustomDateTextField(String id, IModel<Date> model, DateConverter converter) {
        super(id, model, converter);
    }
    
}


//~ Formatted by Jindent --- http://www.jindent.com
