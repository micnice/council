/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.RegistrantClearance;

/**
 *
 * @author kelvin
 */
public interface RegistrantClearanceRepository extends CrudRepository<RegistrantClearance, Long>{
     public List<RegistrantClearance> findAll();   
}
