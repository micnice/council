/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.repo;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.Service;

/**
 *
 * @author tidza
 */
public interface ServiceRepository extends CrudRepository<Service, Long> {

    public List<Service> findAll();
}
