/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.examinations;

import java.util.List;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.examinations.OldExamResult;
import zw.co.hitrac.council.business.service.IGenericService;

/**
 *
 * @author tidza
 */
public interface OldExamResultService extends IGenericService<OldExamResult> {
    
    public List<OldExamResult> examResults(Registrant registrant);
}
