/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.application;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.ApplicationPurpose;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.service.ApplicationService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author artwell
 */
public class ApplicationPendingPage extends TemplatePage {

    @SpringBean
    private ApplicationService applicationService;
    private ApplicationPurpose applicationPurpose;
    private DebtComponent debtComponent;
    @SpringBean
    private GeneralParametersService generalParametersService;

    public ApplicationPendingPage() {

        menuPanelManager.getApplicationPanel().setTopMenuCurrent(true);
        menuPanelManager.getApplicationPanel().setApplicationPending(true);

        PropertyModel<ApplicationPurpose> applicationPurposeModel = new PropertyModel<ApplicationPurpose>(this, "applicationPurpose");

        Form<?> form = new Form("form");

        List<ApplicationPurpose> applicationPurposes = new ArrayList<ApplicationPurpose>(Arrays.asList(ApplicationPurpose.values()));
        if (!generalParametersService.get().getRegistrationByApplication()) {
            applicationPurposes.remove(ApplicationPurpose.INTERN_REGISTRATION);
            applicationPurposes.remove(ApplicationPurpose.MAIN_REGISTRATION);
            applicationPurposes.remove(ApplicationPurpose.SPECIALIST_REGISTRATION);
            applicationPurposes.remove(ApplicationPurpose.STUDENT_REGISTRATION);
            applicationPurposes.remove(ApplicationPurpose.UNRESTRICTED_PRACTICING_CERTIFICATE);
        }
        form.add(new DropDownChoice("applicationPurpose", applicationPurposeModel, applicationPurposes) {
            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }
        });
        add(form);

        IModel<List<Application>> model = new LoadableDetachableModel<List<Application>>() {
            @Override
            protected List<Application> load() {
                if ((applicationPurpose == null)) {
                    return new ArrayList<Application>();
                } else {
                    
                    return applicationService.getApplications(null, Application.APPLICATIONPENDING, null, null, applicationPurpose, Boolean.FALSE, Boolean.FALSE);
                }
            }
        };
        // Registrant List Data View Panel
        add(new ApplicationPendingAlertPage("applicationPendingAlertPage", model));
        add(new FeedbackPanel("feedback"));
    }
}
