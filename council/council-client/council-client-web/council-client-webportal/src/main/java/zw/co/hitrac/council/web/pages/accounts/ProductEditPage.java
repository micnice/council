package zw.co.hitrac.council.web.pages.accounts;

import java.util.Arrays;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.EmploymentType;
import zw.co.hitrac.council.business.domain.QualificationType;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.domain.TypeOfService;
import zw.co.hitrac.council.business.service.accounts.AccountService;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.CourseTypeService;
import zw.co.hitrac.council.business.service.QualificationService;
import zw.co.hitrac.council.business.service.accounts.ProductService;
import zw.co.hitrac.council.business.service.RegisterService;
import zw.co.hitrac.council.business.service.TierService;
import zw.co.hitrac.council.business.service.accounts.ProductGroupService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Charles Chigoriwa
 */
public class ProductEditPage extends IInventoryModulePage {

    @SpringBean
    private ProductService productService;
    @SpringBean
    private AccountService accountService;
    @SpringBean
    private CourseService courseService;
    @SpringBean
    private RegisterService registerService;
    @SpringBean
    private CourseTypeService courseTypeService;
    @SpringBean
    private TierService tierService;
    @SpringBean
    private QualificationService qualificationService;
    @SpringBean
    private ProductGroupService productGroupService;

    public ProductEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<Product>(new LoadableDetachableProductModel(id)));

        Form<Product> form = new Form<Product>("form", (IModel<Product>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                productService.save(getModelObject());
                setResponsePage(new ProductViewPage(getModelObject().getId()));
            }
        };
        form.add(new TextField("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField("description"));
        form.add(new TextField("code"));
        form.add(new DropDownChoice("course", courseService.findAll()).setNullValid(true));
        form.add(new DropDownChoice("register", registerService.findAll()).setNullValid(true));
        form.add(new DropDownChoice("employmentType", Arrays.asList(EmploymentType.values())).setNullValid(true));
        form.add(new DropDownChoice("courseType", courseTypeService.findAll()).setNullValid(true));
        form.add(new DropDownChoice("tier", tierService.findAll()).setNullValid(true));
        form.add(new DropDownChoice("qualificationType", Arrays.asList(QualificationType.values())).setNullValid(true));
        form.add(new DropDownChoice("typeOfService", Arrays.asList(TypeOfService.values())).setNullValid(true));
        form.add(new DropDownChoice("salesAccount", accountService.getGeneralLedgerAccounts()).setNullValid(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("productGroup", productGroupService.findAll()).setNullValid(true));
        form.add(new BookmarkablePageLink<Void>("returnLink", ProductListPage.class));
        form.add(new CheckBox("directlyInvoicable"));
        form.add(new CheckBox("examProduct"));
        form.add(new CheckBox("retired"));
        add(form);
    }

    private final class LoadableDetachableProductModel extends LoadableDetachableModel<Product> {

        private Long id;

        public LoadableDetachableProductModel(Long id) {
            this.id = id;
        }

        @Override
        protected Product load() {
            if (id == null) {
                return new Product();
            }
            return productService.get(id);
        }
    }
}
