/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.reports;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.service.accounts.ReceiptItemService;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.reports.IncomeReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.DatePickerUtil;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 * @author kelvin
 */
public class IncomeReportPage extends TemplatePage {

    private Date startDate, endDate;
    private ContentType contentType;

    @SpringBean
    private ReceiptItemService receiptItemService;

    public IncomeReportPage() {
        PropertyModel<Date> startDateModel = new PropertyModel<Date>(this, "startDate");
        PropertyModel<Date> endDateModel = new PropertyModel<Date>(this, "endDate");
        PropertyModel<ContentType> contentTypeModel = new PropertyModel<ContentType>(this, "contentType");
        Form<?> form = new Form("form");
        add(form);
        form.add(new TextField<Date>("startDate", startDateModel).setRequired(true).add(new ErrorBehavior()).add(DatePickerUtil.getDatePicker()));
        form.add(new TextField<Date>("endDate", endDateModel).add(DatePickerUtil.getDatePicker()));
        form.add(new DropDownChoice("contentType", contentTypeModel, Arrays.asList(ContentType.values())));
        
        form.add(new Button("report") {
            @Override
            public void onSubmit() {
                try {
                    Map parameters = new HashMap();
                    if (startDate != null && endDate != null) {
                        IncomeReport incomeReport = new IncomeReport();

                        ContentType contentType = ContentType.PDF;
                        parameters.put("comment", "All Disciplinary Cases from " + DateUtil.getDate(startDate) + " to " + DateUtil.getDate(endDate));

                        ByteArrayResource resource = ReportResourceUtils.getReportResource(incomeReport, contentType, receiptItemService.getIncomeGenerated(startDate, endDate), parameters);
                        IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                RequestCycle.get().getResponse(), null);
                        resource.respond(a);
                        // To make Wicket stop processing form after sending response
                        RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                    }
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });
    }
}
