package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.CertificateReprint;

/**
 *
 * @author Michael Matiashe
 */
public interface CertificateReprintRepository extends CrudRepository<CertificateReprint, Long> {
    public List<CertificateReprint> findAll();
}
