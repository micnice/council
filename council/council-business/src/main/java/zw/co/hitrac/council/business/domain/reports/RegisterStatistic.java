package zw.co.hitrac.council.business.domain.reports;

import zw.co.hitrac.council.business.domain.*;

import java.io.Serializable;

/**
 *
 * @author Constance Mabaso
 */
public class RegisterStatistic implements Serializable {

    private Register mainRegister, provisionalRegister, studentRegister, internRegister, maintainanceRegister, specialistRegister;
    private Course course;
    private Qualification qualification;
    private RegisterType registerType;
    private CourseType CourseType;
    private Register register;
    private EmploymentType employmentType;
    private Integer numberOfRegistrantsInMainRegister = 0;
    private Integer numberOfRegistrantsInProvisionalRegister = 0;
    private Integer numberOfRegistrantsInStudentRegister = 0;
    private Integer numberOfRegistrantsInInternRegister = 0;
    private Integer numberOfRegistrantsInMaintainceRegister = 0;
    private Integer numberOfRegistrantsInSpecialistRegister = 0;
    private Integer numberOfSuspenedRegistrantsInMainRegister = 0;
    private Integer numberOfSuspenedRegistrantsInProvisionalRegister = 0;
    private Integer numberOfSuspenedRegistrantsInStudentRegister = 0;
    private Integer numberOfSuspenedRegistrantsInInternRegister = 0;
    private Integer numberOfSuspenedRegistrantsInMaintainceRegister = 0;
    private Integer numberOfSuspenedRegistrantsInSpecialistRegister = 0;
    private Integer numberOfInActiveRegistrantsInMainRegister = 0;
    private Integer numberOfInActiveRegistrantsInProvisionalRegister = 0;
    private Integer numberOfInActiveRegistrantsInStudentRegister = 0;
    private Integer numberOfInActiveRegistrantsInInternRegister = 0;
    private Integer numberOfInActiveRegistrantsInMaintainceRegister = 0;
    private Integer numberOfInActiveRegistrantsInSpecialistRegister = 0;
    private Integer numberOfRegistrants = 0;
    private int numberOfActiveRegistrants = 0;
    private int numberOfInactiveRegistrants = 0;
    private Integer numberOfDeceasedRegistrants = 0;

    public int getNumberOfActiveRegistrants() {

        return numberOfActiveRegistrants;
    }

    public void setNumberOfActiveRegistrants(int numberOfActiveRegistrants) {

        this.numberOfActiveRegistrants = numberOfActiveRegistrants;
    }

    public int getNumberOfInactiveRegistrants() {

        return numberOfInactiveRegistrants;
    }

    public void setNumberOfInactiveRegistrants(int numberOfInactiveRegistrants) {

        this.numberOfInactiveRegistrants = numberOfInactiveRegistrants;
    }

    public Integer getNumberOfDeceasedRegistrants() {
        return numberOfDeceasedRegistrants;
    }

    public void setNumberOfDeceasedRegistrants(Integer numberOfDeceasedRegistrants) {
        this.numberOfDeceasedRegistrants = numberOfDeceasedRegistrants;
    }

    public Register getMainRegister() {
        return mainRegister;
    }

    public void setMainRegister(Register mainRegister) {
        this.mainRegister = mainRegister;
    }

    public Register getProvisionalRegister() {
        return provisionalRegister;
    }

    public void setProvisionalRegister(Register provisionalRegister) {
        this.provisionalRegister = provisionalRegister;
    }

    public Register getStudentRegister() {
        return studentRegister;
    }

    public void setStudentRegister(Register studentRegister) {
        this.studentRegister = studentRegister;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Qualification getQualification() {
        return qualification;
    }

    public void setQualification(Qualification qualification) {
        this.qualification = qualification;
    }

    public Integer getNumberOfRegistrantsInMainRegister() {
        return numberOfRegistrantsInMainRegister;
    }

    public void setNumberOfRegistrantsInMainRegister(Integer numberOfRegistrantsInMainRegister) {
        this.numberOfRegistrantsInMainRegister = numberOfRegistrantsInMainRegister;
    }

    public Integer getNumberOfRegistrantsInProvisionalRegister() {
        return numberOfRegistrantsInProvisionalRegister;
    }

    public void setNumberOfRegistrantsInProvisionalRegister(Integer numberOfRegistrantsInProvisionalRegister) {
        this.numberOfRegistrantsInProvisionalRegister = numberOfRegistrantsInProvisionalRegister;
    }

    public Integer getNumberOfRegistrantsInStudentRegister() {
        return numberOfRegistrantsInStudentRegister;
    }

    public void setNumberOfRegistrantsInStudentRegister(Integer numberOfRegistrantsInStudentRegister) {
        this.numberOfRegistrantsInStudentRegister = numberOfRegistrantsInStudentRegister;
    }

    public RegisterType getRegisterType() {
        return registerType;
    }

    public void setRegisterType(RegisterType registerType) {
        this.registerType = registerType;
    }

    public CourseType getCourseType() {
        return CourseType;
    }

    public void setCourseType(CourseType CourseType) {
        this.CourseType = CourseType;
    }

    public Integer getNumberOfRegistrants() {
        return numberOfRegistrants;
    }

    public void setNumberOfRegistrants(Integer numberOfRegistrants) {
        this.numberOfRegistrants = numberOfRegistrants;
    }

    public Register getInternRegister() {
        return internRegister;
    }

    public void setInternRegister(Register internRegister) {
        this.internRegister = internRegister;
    }

    public Register getMaintainanceRegister() {
        return maintainanceRegister;
    }

    public void setMaintainanceRegister(Register maintainanceRegister) {
        this.maintainanceRegister = maintainanceRegister;
    }

    public Register getSpecialistRegister() {
        return specialistRegister;
    }

    public void setSpecialistRegister(Register specialistRegister) {
        this.specialistRegister = specialistRegister;
    }

    public Integer getNumberOfRegistrantsInInternRegister() {
        return numberOfRegistrantsInInternRegister;
    }

    public void setNumberOfRegistrantsInInternRegister(Integer numberOfRegistrantsInInternRegister) {
        this.numberOfRegistrantsInInternRegister = numberOfRegistrantsInInternRegister;
    }

    public Integer getNumberOfRegistrantsInMaintainceRegister() {
        return numberOfRegistrantsInMaintainceRegister;
    }

    public void setNumberOfRegistrantsInMaintainceRegister(Integer numberOfRegistrantsInMaintainceRegister) {
        this.numberOfRegistrantsInMaintainceRegister = numberOfRegistrantsInMaintainceRegister;
    }

    public Integer getNumberOfRegistrantsInSpecialistRegister() {
        return numberOfRegistrantsInSpecialistRegister;
    }

    public void setNumberOfRegistrantsInSpecialistRegister(Integer numberOfRegistrantsInSpecialistRegister) {
        this.numberOfRegistrantsInSpecialistRegister = numberOfRegistrantsInSpecialistRegister;
    }

    public Integer getNumberOfInActiveRegistrantsInMainRegister() {
        return numberOfInActiveRegistrantsInMainRegister;
    }

    public void setNumberOfInActiveRegistrantsInMainRegister(Integer numberOfInActiveRegistrantsInMainRegister) {
        this.numberOfInActiveRegistrantsInMainRegister = numberOfInActiveRegistrantsInMainRegister;
    }

    public Integer getNumberOfInActiveRegistrantsInProvisionalRegister() {
        return numberOfInActiveRegistrantsInProvisionalRegister;
    }

    public void setNumberOfInActiveRegistrantsInProvisionalRegister(Integer numberOfInActiveRegistrantsInProvisionalRegister) {
        this.numberOfInActiveRegistrantsInProvisionalRegister = numberOfInActiveRegistrantsInProvisionalRegister;
    }

    public Integer getNumberOfInActiveRegistrantsInStudentRegister() {
        return numberOfInActiveRegistrantsInStudentRegister;
    }

    public void setNumberOfInActiveRegistrantsInStudentRegister(Integer numberOfInActiveRegistrantsInStudentRegister) {
        this.numberOfInActiveRegistrantsInStudentRegister = numberOfInActiveRegistrantsInStudentRegister;
    }

    public Integer getNumberOfInActiveRegistrantsInInternRegister() {
        return numberOfInActiveRegistrantsInInternRegister;
    }

    public void setNumberOfInActiveRegistrantsInInternRegister(Integer numberOfInActiveRegistrantsInInternRegister) {
        this.numberOfInActiveRegistrantsInInternRegister = numberOfInActiveRegistrantsInInternRegister;
    }

    public Integer getNumberOfInActiveRegistrantsInMaintainceRegister() {
        return numberOfInActiveRegistrantsInMaintainceRegister;
    }

    public void setNumberOfInActiveRegistrantsInMaintainceRegister(Integer numberOfInActiveRegistrantsInMaintainceRegister) {
        this.numberOfInActiveRegistrantsInMaintainceRegister = numberOfInActiveRegistrantsInMaintainceRegister;
    }

    public Integer getNumberOfInActiveRegistrantsInSpecialistRegister() {
        return numberOfInActiveRegistrantsInSpecialistRegister;
    }

    public void setNumberOfInActiveRegistrantsInSpecialistRegister(Integer numberOfInActiveRegistrantsInSpecialistRegister) {
        this.numberOfInActiveRegistrantsInSpecialistRegister = numberOfInActiveRegistrantsInSpecialistRegister;
    }

    public Integer getNumberOfSuspenedRegistrantsInMainRegister() {
        return numberOfSuspenedRegistrantsInMainRegister;
    }

    public void setNumberOfSuspenedRegistrantsInMainRegister(Integer numberOfSuspenedRegistrantsInMainRegister) {
        this.numberOfSuspenedRegistrantsInMainRegister = numberOfSuspenedRegistrantsInMainRegister;
    }

    public Integer getNumberOfSuspenedRegistrantsInProvisionalRegister() {
        return numberOfSuspenedRegistrantsInProvisionalRegister;
    }

    public void setNumberOfSuspenedRegistrantsInProvisionalRegister(Integer numberOfSuspenedRegistrantsInProvisionalRegister) {
        this.numberOfSuspenedRegistrantsInProvisionalRegister = numberOfSuspenedRegistrantsInProvisionalRegister;
    }

    public Integer getNumberOfSuspenedRegistrantsInStudentRegister() {
        return numberOfSuspenedRegistrantsInStudentRegister;
    }

    public void setNumberOfSuspenedRegistrantsInStudentRegister(Integer numberOfSuspenedRegistrantsInStudentRegister) {
        this.numberOfSuspenedRegistrantsInStudentRegister = numberOfSuspenedRegistrantsInStudentRegister;
    }

    public Integer getNumberOfSuspenedRegistrantsInInternRegister() {
        return numberOfSuspenedRegistrantsInInternRegister;
    }

    public void setNumberOfSuspenedRegistrantsInInternRegister(Integer numberOfSuspenedRegistrantsInInternRegister) {
        this.numberOfSuspenedRegistrantsInInternRegister = numberOfSuspenedRegistrantsInInternRegister;
    }

    public Integer getNumberOfSuspenedRegistrantsInMaintainceRegister() {
        return numberOfSuspenedRegistrantsInMaintainceRegister;
    }

    public void setNumberOfSuspenedRegistrantsInMaintainceRegister(Integer numberOfSuspenedRegistrantsInMaintainceRegister) {
        this.numberOfSuspenedRegistrantsInMaintainceRegister = numberOfSuspenedRegistrantsInMaintainceRegister;
    }

    public Integer getNumberOfSuspenedRegistrantsInSpecialistRegister() {
        return numberOfSuspenedRegistrantsInSpecialistRegister;
    }

    public void setNumberOfSuspenedRegistrantsInSpecialistRegister(Integer numberOfSuspenedRegistrantsInSpecialistRegister) {
        this.numberOfSuspenedRegistrantsInSpecialistRegister = numberOfSuspenedRegistrantsInSpecialistRegister;
    }

    public EmploymentType getEmploymentType() {
        return employmentType;
    }

    public void setEmploymentType(EmploymentType employmentType) {
        this.employmentType = employmentType;
    }

    public Register getRegister() {
        return register;
    }

    public void setRegister(Register register) {
        this.register = register;
    }

    public Boolean getShow() {
        if (getNumberOfRegistrants() == 0) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }
    }
}
