package zw.co.hitrac.council.business.domain.accounts;

import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.domain.BaseIdEntity;
import zw.co.hitrac.council.business.domain.CouncilDuration;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 *
 * @author Charles Chigoriwa
 * @author Michael Matiashe
 */
@Entity
@Table(name = "debtcomponent")
@Audited
public class DebtComponent extends BaseIdEntity {
    private DocumentItem documentItem;
   
    private TransactionComponent transactionComponent;
    private BigDecimal remainingBalance;
    private Account account;
    private Product product;
    private Boolean reversed = Boolean.FALSE;
    private String comment;
    private CouncilDuration paymentPeriod;

    public Boolean getReversed() {
        return reversed;
    }

    public void setReversed(Boolean reversed) {
        this.reversed = reversed;
    }

    @Transient
    public CouncilDuration getPaymentPeriod() {
        return paymentPeriod;
    }

    public void setPaymentPeriod(CouncilDuration paymentPeriod) {
        this.paymentPeriod = paymentPeriod;
    }

    @OneToOne
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @ManyToOne
    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @OneToOne
    public TransactionComponent getTransactionComponent() {
        return transactionComponent;
    }

    public void setTransactionComponent(TransactionComponent transactionComponent) {
        this.transactionComponent = transactionComponent;
    }

    public BigDecimal getRemainingBalance() {
        return remainingBalance;
    }

    public void setRemainingBalance(BigDecimal remainingBalance) {
        this.remainingBalance = remainingBalance;
    }

    public void reduceRemainingBalance(BigDecimal amount) {
        this.remainingBalance = this.remainingBalance.subtract(amount);
    }

    @Column(length = 500)
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @OneToOne(mappedBy = "debtComponent")
    public DocumentItem getDocumentItem() {
        return documentItem;
    }

    public void setDocumentItem(DocumentItem documentItem) {
        this.documentItem = documentItem;
    }

}
