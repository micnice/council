
package zw.co.hitrac.council.business.dao.accounts;

import zw.co.hitrac.council.business.dao.IGenericDAO;
import zw.co.hitrac.council.business.domain.accounts.Allocation;

/**
 *
 * @author Constance Mabaso
 */
public interface AllocationDAO extends IGenericDAO<Allocation> {
    
}
