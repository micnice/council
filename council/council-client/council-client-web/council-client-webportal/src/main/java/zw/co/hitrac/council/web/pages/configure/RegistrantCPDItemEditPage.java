package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.RegistrantCPDItem;
import zw.co.hitrac.council.business.service.RegistrantCPDCategoryService;
import zw.co.hitrac.council.business.service.RegistrantCPDItemService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Constance Mabaso
 */
public class RegistrantCPDItemEditPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private RegistrantCPDItemService     registrantCPDItemService;
    @SpringBean
    private RegistrantCPDCategoryService registrantCPDCategoryService;

    public RegistrantCPDItemEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<RegistrantCPDItem>(new LoadableDetachableRegistrantCPDItemModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));

        Form<RegistrantCPDItem> form = new Form<RegistrantCPDItem>("form",
                                           (IModel<RegistrantCPDItem>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                registrantCPDItemService.save(getModelObject());
                setResponsePage(new RegistrantCPDItemViewPage(getModelObject().getId()));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new TextField<String>("points").setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("registrantCPDCategory", registrantCPDCategoryService.findAll()));
        form.add(new BookmarkablePageLink<Void>("returnLink", RegistrantCPDItemListPage.class));
        add(form);
    }

    private final class LoadableDetachableRegistrantCPDItemModel extends LoadableDetachableModel<RegistrantCPDItem> {
        private Long id;

        public LoadableDetachableRegistrantCPDItemModel(Long id) {
            this.id = id;
        }

        @Override
        protected RegistrantCPDItem load() {
            if (id == null) {
                return new RegistrantCPDItem();
            }

            return registrantCPDItemService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
