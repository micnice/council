package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.CourseDAO;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.service.CourseService;

import java.util.List;

/**
 * @author Charles Chigoriwa
 */
@Service
@Transactional
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CourseDAO courseDAO;

    @Transactional
    public Course save(Course course) {
        return courseDAO.save(course);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Course> findAll() {
        return courseDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Course get(Long id) {
        return courseDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Course> findAll(Boolean retired) {

        return courseDAO.findAll(retired);
    }

    public void setCourseDAO(CourseDAO courseDAO) {
        this.courseDAO = courseDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Course> getCourses(Boolean basic, CourseGroup courseGroup, Qualification qualification, CourseType courseType, Tier tier) {
        return courseDAO.getCourses(basic, courseGroup, qualification, courseType, tier);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Course findByPrefixName(String prefixName) {
        return courseDAO.findByPrefixName(prefixName);
    }

    @Override
    public Long getNumberOfModulePapers(Course course) {
        return courseDAO.getNumberOfModulePapers(course);
    }
}
