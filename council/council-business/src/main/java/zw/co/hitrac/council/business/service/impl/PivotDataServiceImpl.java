package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.reports.ProductSale;
import zw.co.hitrac.council.business.domain.reports.RegistrantBalance;
import zw.co.hitrac.council.business.service.*;

import javax.inject.Named;
import java.nio.charset.Charset;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Created by ezinzombe on 3/21/17. Created by pkamota on 3/21/17
 */
@Service
@Named
public class PivotDataServiceImpl implements PivotDataService {

    private static final char COLUMN_SEPARATOR = ',';
    private static final String CHARSET = "UTF-8";

    @Autowired
    GeneralParametersService generalParametersService;
    @Autowired
    private RegistrationService registrationService;
    @Autowired
    RegistrantQualificationService registrantQualificationService;
    @Autowired
    RegistrantAddressService registrantAddressService;
    @Autowired
    RegistrantContactService registrantContactService;
    @Autowired
    RegistrantActivityService registrantActivityService;

    @Override
    public byte[] exportRenewals(List<RegistrantActivity> renewals) {

        GeneralParameters generalParameters = generalParametersService.get();

        Objects.requireNonNull(renewals, ("RenewalProcesses"));

        final StringBuilder fw = new StringBuilder();

        fw.append("Title,Full Name,Gender,Marital Status,Registration Number,Active Status,Start Date,End Date,Duration,Renewal Date,"
                + " Registrant Address Type,"
                + "Business Address,Contact Type, Business Telephone, City,  Birth Date,Place of Birth,Citizenship,Disciplines"
        );
        fw.append(System.getProperty("line.separator"));

        renewals.stream().distinct()
                .forEach(r -> {

                    final Registrant registrant = r.getRegistrant();

                    fw.append(registrant.getTitle());
                    fw.append(COLUMN_SEPARATOR);
                    fw.append(registrant.getFullname());
                    fw.append(COLUMN_SEPARATOR);
                    fw.append(registrant.getGender());
                    fw.append(COLUMN_SEPARATOR);
                    fw.append(registrant.getMaritalStatus());
                    fw.append(COLUMN_SEPARATOR);
                    fw.append(registrant.getRegistrationNumber());
                    fw.append(COLUMN_SEPARATOR);
                    fw.append(r.getRegistrant().getActiveStatus());
                    fw.append(COLUMN_SEPARATOR);
                    fw.append(r.getStartDate());
                    fw.append(COLUMN_SEPARATOR);
                    fw.append(r.getEndDate());
                    fw.append(COLUMN_SEPARATOR);
                    fw.append(r.getDuration());
                    fw.append(COLUMN_SEPARATOR);
                    Date date = r.getDateCreated();
                    if (date != null) {
                        Instant instant = date.toInstant();
                        ZoneId defaultZoneId = ZoneId.systemDefault();
                        LocalDate renewalDate = instant.atZone(defaultZoneId).toLocalDate();
                        fw.append(renewalDate != null ? renewalDate : "");
                    }

                    fw.append(COLUMN_SEPARATOR);
                    RegistrantAddress registrantAddress = registrantAddressService.getActiveAddressbyAddressType(registrant, generalParameters.getBusinessAddressType());
                    fw.append(registrantAddress != null ? registrantAddress.getAddressType() : "");
                    fw.append(COLUMN_SEPARATOR);
                    fw.append(registrantAddress != null ? registrantAddress.getFullAddress().replaceAll(",", " ") : "");
                    fw.append(COLUMN_SEPARATOR);
                    RegistrantContact registrantContact = registrantContactService.getActiveContactbyContactType(registrant, generalParameters.getMobileNumberContactType());
                    fw.append(registrantContact != null ? registrantContact.getContactType() : "");
                    fw.append(COLUMN_SEPARATOR);
                    fw.append(registrantContact != null ? registrantContact.getContactDetail().replaceAll(",", " ") : "");
                    fw.append(COLUMN_SEPARATOR);
                    fw.append(registrantAddress != null ? registrantAddress.getCity() : "");
                    fw.append(COLUMN_SEPARATOR);
                    fw.append(registrant != null ? registrant.getBirthDate() : " ");
                    fw.append(COLUMN_SEPARATOR);
                    fw.append(registrant != null ? registrant.getPlaceOfBirth() : " ");
                    fw.append(COLUMN_SEPARATOR);
                    fw.append(registrant.getCitizenship());
                    fw.append(COLUMN_SEPARATOR);
                    fw.append(registrant != null ? registrant.getDisciplines() : " ");
                    fw.append(COLUMN_SEPARATOR);
                    fw.append(COLUMN_SEPARATOR);
                    fw.append(System.getProperty("line.separator"));
                });

        return export(fw);
    }

    @Override
    public byte[] exportRegistrations() {

        List<Registration> registrations = registrationService.findAll();

        GeneralParameters generalParameters = generalParametersService.get();

        StringBuilder fw = new StringBuilder();

        fw.append("Title,Full Name,Gender,Marital Status,Registration Number,Citizenship,Register,Current Discipline, First Discipline,"
                + "Registration Date,DeRegistration Date, Registrant Address Type,"
                + "Business Address,Contact Type, Business Telephone,Email Address,Contact Number,City ,Place of Birth ,Country of Birth");

        fw.append(System.getProperty("line.separator"));

        registrations.stream()
                .filter(r -> r.getRegistrant() != null).forEach(registration
                -> {
            final Registrant registrant = registration.getRegistrant();

            fw.append(registrant.getTitle());
            fw.append(COLUMN_SEPARATOR);
            fw.append(registrant.getFullname());
            fw.append(COLUMN_SEPARATOR);
            fw.append(registrant.getGender());
            fw.append(COLUMN_SEPARATOR);
            fw.append(registrant.getMaritalStatus());
            fw.append(COLUMN_SEPARATOR);
            fw.append(registrant.getRegistrationNumber());
            fw.append(COLUMN_SEPARATOR);
            fw.append(registrant.getCitizenship());
            fw.append(COLUMN_SEPARATOR);
            fw.append(registration.getRegister());
            fw.append(COLUMN_SEPARATOR);
            fw.append(registrant != null ? registrant.getDisciplines() : " ");
            fw.append(COLUMN_SEPARATOR);
            fw.append(COLUMN_SEPARATOR);
            fw.append(registration.getRegistrationDate());
            fw.append(COLUMN_SEPARATOR);
            fw.append(registration.getDeRegistrationDate());
            fw.append(COLUMN_SEPARATOR);
            RegistrantAddress registrantAddress = registrantAddressService.getActiveAddressbyAddressType(registrant, generalParameters.getBusinessAddressType());
            fw.append(registrantAddress != null ? registrantAddress.getAddressType() : "");
            fw.append(COLUMN_SEPARATOR);
            fw.append(registrantAddress != null ? registrantAddress.getFullAddress().replaceAll(",", " ") : "");
            fw.append(COLUMN_SEPARATOR);
            RegistrantContact registrantContact = registrantContactService.getActiveContactbyContactType(registrant, generalParameters.getBusinessNumberContactType());
            fw.append(registrantContact != null ? registrantContact.getContactType() : "");
            fw.append(COLUMN_SEPARATOR);
            fw.append(registrantContact != null ? registrantContact.getContactDetail().replaceAll(",", " ") : "");
            fw.append(COLUMN_SEPARATOR);
            registrantContact = registrantContactService.getActiveContactbyContactType(registrant, generalParameters.getEmailContactType());
            fw.append(registrantContact != null ? registrantContact.getContactDetail().replaceAll(",", " ") : "");
            fw.append(COLUMN_SEPARATOR);
            registrantContact = registrantContactService.getActiveContactbyContactType(registrant, generalParameters.getMobileNumberContactType());
            fw.append(registrantContact != null ? registrantContact.getContactDetail().replaceAll(",", " ") : "");
            fw.append(COLUMN_SEPARATOR);
            fw.append(registrantAddress != null ? registrantAddress.getCity() : "");
            fw.append(COLUMN_SEPARATOR);
            fw.append(registrant != null ? registrant.getPlaceOfBirth() : " ");
            fw.append(System.getProperty("line.separator"));

        });

        return export(fw);
    }

    private byte[] export(StringBuilder str) {
        return str.toString().getBytes(Charset.forName(CHARSET));
    }

    public byte[] exportCarryForward(List<ProductSale> productSales) {

        final StringBuilder fw = new StringBuilder();

        fw.append("ID,RegNumber,Full Name,Amount Paid,Carry forward");
        fw.append(System.getProperty("line.separator"));

        productSales.stream().filter(r -> r.getRegistrant() != null)
                .forEach(r -> {

                    fw.append(r.getRegistrant().getIdNumber() == null ? "" : r.getRegistrant().getIdNumber());
                    fw.append(COLUMN_SEPARATOR);
                    fw.append(r.getRegistrant().getRegistrationNumber() == null ? "" : r.getRegistrant().getRegistrationNumber());
                    fw.append(COLUMN_SEPARATOR);
                    fw.append(r.getRegistrant().getFullname());
                    fw.append(COLUMN_SEPARATOR);
                    fw.append(r.getAmountPaid());
                    fw.append(COLUMN_SEPARATOR);
                    fw.append(r.getCarryForward());
                    fw.append(System.getProperty("line.separator"));
                });

        System.out.println(fw);
        return export(fw);
    }

    public byte[] exportCarryForwardUsage(List<RegistrantBalance> registrantBalances) {

        final StringBuilder fw = new StringBuilder();

        fw.append("ID,RegNumber,Full Name,Amount Paid,Carry forward");
        fw.append(System.getProperty("line.separator"));

        registrantBalances.stream().filter(r -> r.getRegistrant() != null)
                .forEach(r -> {

                    fw.append(r.getRegistrant().getIdNumber() == null ? "" : r.getRegistrant().getIdNumber());
                    fw.append(COLUMN_SEPARATOR);
                    fw.append(r.getRegistrant().getRegistrationNumber() == null ? "" : r.getRegistrant().getRegistrationNumber());
                    fw.append(COLUMN_SEPARATOR);
                    fw.append(r.getRegistrant().getFullname());
                    fw.append(COLUMN_SEPARATOR);
                    fw.append(r.getAmountUsed());
                    fw.append(COLUMN_SEPARATOR);
                    fw.append(r.getBalanceRemaining());
                    fw.append(System.getProperty("line.separator"));

                });

        System.out.println(fw);

        return export(fw);
    }
}
