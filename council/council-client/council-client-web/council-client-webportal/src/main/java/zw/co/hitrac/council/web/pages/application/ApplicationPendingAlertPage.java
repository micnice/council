package zw.co.hitrac.council.web.pages.application;

import org.apache.wicket.Page;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PageableListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.ApplicationPurpose;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.institution.InstitutionApplicationViewPage;

import java.util.List;
import zw.co.hitrac.council.web.utility.CustomDateLabel;

/**
 * @author artwell
 */
public final class ApplicationPendingAlertPage extends Panel {

    public ApplicationPendingAlertPage(String id, IModel<List<Application>> model) {

        super(id);

        PageableListView<Application> eachItem = new PageableListView<Application>("eachItem", model, 20) {

            @Override
            protected void populateItem(ListItem<Application> item) {

                Link<Application> viewLink = new Link<Application>("applicationViewPage", item.getModel()) {

                    @Override
                    public void onClick() {

                        Application application = getModelObject();
                        ApplicationPurpose applicationPurpose = application.getApplicationPurpose();
                        Page nextPage;

                        switch (applicationPurpose) {

                            case INSTITUTION_REGISTRATION:

                                nextPage = new InstitutionApplicationViewPage(application.getId());
                                break;

                            case CERTIFICATE_OF_GOOD_STANDING:

                                nextPage = new CgsApplicationViewPage(application.getId());
                                break;

                            case CHANGE_OF_NAME:

                                nextPage = new ChangeOfNameApplicationViewPage(application.getId());
                                break;

                            case PROVISIONAL_QUALIIFICATION:

                                nextPage = new QualificationApplicationViewPage(application.getId());
                                break;

                            case TRANSFER:

                                nextPage = new TransferApplicationViewPage(application.getId());
                                break;

                            default:

                                nextPage = new ApplicationViewPage(application.getId());

                        }

                        setResponsePage(nextPage);
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.setModel(new CompoundPropertyModel<Application>(item.getModel()));
                item.add(viewLink);
                item.add(new Label("registrant.fullname"));
                viewLink.add(new Label("applicationPurpose"));
                item.add(new CustomDateLabel("applicationDate"));
                item.add(new Label("institution"));
                item.add(new Label("course"));
            }
        };

        add(eachItem);
    }
}
