package zw.co.hitrac.council.web.pages.security;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.authroles.authorization.strategies.role.IRoleCheckingStrategy;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;

import zw.co.hitrac.council.web.config.CouncilSession;

/**
 *
 * @author charlesc
 */
public class UserRolesAuthorizationStrategy implements IRoleCheckingStrategy {

    public boolean hasAnyRole(Roles roles) {

        CouncilSession authSession = CouncilSession.get();

        return (authSession != null) && authSession.hasAnyRole(roles);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
