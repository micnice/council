package zw.co.hitrac.council.business.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import zw.co.hitrac.council.business.domain.User;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Util {

    private static Log log = LogFactory.getLog(Util.class);

    private static Map<Locale, SimpleDateFormat> dateFormatCache = new HashMap<Locale, SimpleDateFormat>();

    public static Date lastSecondOfDay(Date date) {
        if (date == null) {
            return null;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        // TODO: figure out the right way to do this (or at least set milliseconds to zero)
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.add(Calendar.DAY_OF_MONTH, 1);
        c.add(Calendar.SECOND, -1);
        return c.getTime();
    }

    public static SimpleDateFormat getDateFormat() {
        return getDateFormat();
    }

    public static SimpleDateFormat getDateFormat(Locale locale) {
        if (dateFormatCache.containsKey(locale)) {
            return (SimpleDateFormat) dateFormatCache.get(locale).clone();
        }

        SimpleDateFormat sdf = (SimpleDateFormat) DateFormat.getDateInstance(DateFormat.SHORT, locale);
        String pattern = sdf.toPattern();

        if (!pattern.contains("yyyy")) {
            // otherwise, change the pattern to be a four digit year
            pattern = pattern.replaceFirst("yy", "yyyy");
            sdf.applyPattern(pattern);
        }
        if (!pattern.contains("MM")) {
            // change the pattern to be a two digit month
            pattern = pattern.replaceFirst("M", "MM");
            sdf.applyPattern(pattern);
        }
        if (!pattern.contains("dd")) {
            // change the pattern to be a two digit day
            pattern = pattern.replaceFirst("d", "dd");
            sdf.applyPattern(pattern);
        }

        dateFormatCache.put(locale, sdf);

        return (SimpleDateFormat) sdf.clone();
    }

    public static boolean collectionContains(Collection<?> objects, Object obj) {
        if (obj == null || objects == null) {
            return false;
        }

        for (Object o : objects) {
            if (o != null && o.equals(obj)) {
                return true;
            }
        }

        return false;
    }

    public static boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }

    public static Optional<User> getCurrentUser() {

        SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication authentication = securityContext.getAuthentication();
        final User springSecurityUser;


        if ((authentication == null) || (authentication.getPrincipal() == null)) {
            springSecurityUser = null;
        } else {
            springSecurityUser = (User) authentication.getPrincipal();
        }

        return Optional.ofNullable(springSecurityUser);
    }

    public static int getIntegerValueOfAge(Date birthDate) {

        if (birthDate == null) {
            return 0;
        }

        Calendar birthCalendar = Calendar.getInstance();
        birthCalendar.setTime(birthDate);

        Calendar todayCalendar = Calendar.getInstance();

        int age = todayCalendar.get(Calendar.YEAR) - birthCalendar.get(Calendar.YEAR);

        if (todayCalendar.get(Calendar.MONTH) < birthCalendar.get(Calendar.MONTH)) {
            age--;
        } else if (todayCalendar.get(Calendar.MONTH) == birthCalendar.get(Calendar.MONTH)
                && todayCalendar.get(Calendar.DAY_OF_MONTH) < birthCalendar.get(Calendar.DAY_OF_MONTH)) {
            age--;
        }

        return age;
    }

}
