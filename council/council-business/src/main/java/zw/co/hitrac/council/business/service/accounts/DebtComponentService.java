package zw.co.hitrac.council.business.service.accounts;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.TypeOfService;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.service.IGenericService;

/**
 * @author Tatenda Chiwandire
 * @author Kelvin Goredema
 * @author Charles Chigoriwa
 */
public interface DebtComponentService extends IGenericService<DebtComponent> {

    List<DebtComponent> getDebtComponents(Account account);

    List<DebtComponent> getDebtComponents(Customer customerAccount);

    Boolean hasAccountDebtComponents(Customer customerAccount, TypeOfService typeOfService);

    List<DebtComponent> getDebtComponents(Product product, Date startDate, Date endDate);

    List<DebtComponent> getViewRemovedDeptComponets(Customer customerAccount);

    List<DebtComponent> getViewDebtComponentsRemoved(Account account);

    List<DebtComponent> getPenaltyDeptComponets();

    BigDecimal getCustomerBalanceDue(Customer customerAccount);

    List<DebtComponent> getUnPaidDeptComponets();

    List<DebtComponent> getRemovedDeptComponents();

}
