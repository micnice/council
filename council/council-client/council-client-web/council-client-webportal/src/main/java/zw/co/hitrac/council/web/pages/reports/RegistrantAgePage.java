
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.reports;

//~--- non-JDK imports --------------------------------------------------------
import java.util.Arrays;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.service.RegistrantService;

//~--- JDK imports ------------------------------------------------------------

import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import zw.co.hitrac.council.reports.RegistrantAgeReport;
import zw.co.hitrac.council.reports.RegistrantDataReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 *
 * @author tdhlakama
 */
public class RegistrantAgePage extends IReportPage {

    private Integer lowerLimit;
    private Integer upperLimit;
    @SpringBean
    private RegistrantService registrantService;
    private ContentType contentType;
    
    public RegistrantAgePage() {

        PropertyModel<Integer> lowerLimitModel = new PropertyModel<Integer>(this, "lowerLimit");
        PropertyModel<Integer> upperLimitModel = new PropertyModel<Integer>(this, "upperLimit");
        PropertyModel<ContentType> contentTypeModel = new PropertyModel<ContentType>(this, "contentType");
        Form<?> form = new Form("form");
        form.add(new TextField<Integer>("lowerLimit", lowerLimitModel));
        form.add(new TextField<Integer>("upperLimit", upperLimitModel));
        form.add(new DropDownChoice("contentType", contentTypeModel, Arrays.asList(ContentType.values())));
        add(form);

        form.add(new Button("report") {
            @Override
            public void onSubmit() {
                try {
                    Map parameters = new HashMap();
                    
                    RegistrantDataReport registrantDataReport = new RegistrantDataReport();
                    parameters.put("comment", "Registrants between the age of " + lowerLimit + " years to " + upperLimit + " years ");
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(registrantDataReport, contentType, registrantService.getRegistrantAgeByParameter(lowerLimit, upperLimit), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);
                    resource.respond(a);
                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
