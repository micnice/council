/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.registration;

import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Employment;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author kelvin
 */
public class GeneralRegistrationEditPage extends TemplatePage{
    
    @SpringBean
    private RegistrantService registrantService;
    


    public GeneralRegistrationEditPage(final IModel<Registrant> registrantModel) {
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());

        CompoundPropertyModel<Registrant> model = new CompoundPropertyModel<Registrant>(
                                                      new GeneralRegistrationEditPage.LoadableDetachableRegistrantModel(
                                                          registrantModel.getObject().getId()));    
   
    
    }
    

  private final class LoadableDetachableRegistrantModel extends LoadableDetachableModel<Registrant> {

        private Long id;

        public LoadableDetachableRegistrantModel(Long id) {
            this.id = id;
        }

        @Override
        protected Registrant load() {
            Registrant registrant = null;

            if (id == null) {
                registrant = new Registrant();
            } else {
                registrant = registrantService.get(id);
            }
            return registrant;
        }
  }
}
