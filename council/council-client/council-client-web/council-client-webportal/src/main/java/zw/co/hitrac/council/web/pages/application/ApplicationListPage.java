package zw.co.hitrac.council.web.pages.application;

//~--- non-JDK imports --------------------------------------------------------
import java.util.Arrays;
import java.util.List;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.process.ApplicationProcess;
import zw.co.hitrac.council.business.service.ApplicationService;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.web.config.CouncilSession;

import zw.co.hitrac.council.web.pages.TemplatePage;
import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;

/**
 *
 * @author Michael Matiashe
 */
public class ApplicationListPage extends TemplatePage {

    @SpringBean
    private ApplicationService applicationService;
    @SpringBean
    private ApplicationProcess applicationProcess;

    public ApplicationListPage() {

        menuPanelManager.getApplicationPanel().setTopMenuCurrent(true);
        add(new BookmarkablePageLink<Void>("applicationSimpleSearchPage", ApplicationSearchPage.class) {
            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.EDUCATION_OFFICER)));
                return enabled;
            }
        });
        add(new BookmarkablePageLink<Void>("applicationPendingPage", ApplicationPendingPage.class) {
            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.EDUCATION_OFFICER)));
                return enabled;
            }
        });
        add(new BookmarkablePageLink<Void>("approvedApplicationSearchPage", SearchApprovedApplicationPage.class) {
            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.EDUCATION_OFFICER, Role.REGISTRY_CLERK)));
                return enabled;
            }
        });
        add(new BookmarkablePageLink<Void>("expiredApplicationPage", ExpiredApplicationPage.class) {
            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.EDUCATION_OFFICER)));
                return enabled;
            }
        });


        add(new PropertyListView<String>("applicationsTotal", Application.getApplicationStates) {
            @Override
            protected void populateItem(ListItem<String> item) {
                item.add(new Label("name", item.getModelObject()));
                item.add(new Label("total", applicationService.getTotalApplicationsByStatus(item.getModelObject())));
            }//~ Formatted by Jindent --- http://www.jindent.com
        });

    }
}


//~ Formatted by Jindent --- http://www.jindent.com
