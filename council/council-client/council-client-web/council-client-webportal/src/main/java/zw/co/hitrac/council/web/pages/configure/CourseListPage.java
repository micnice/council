package zw.co.hitrac.council.web.pages.configure;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.reports.CourseReportList;
import zw.co.hitrac.council.reports.QualificationReportList;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 * @author Charles Chigoriwa
 */
public class CourseListPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private CourseService courseService;

    public CourseListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new CourseEditPage(null));
            }
        });

        IModel<List<Course>> model = new LoadableDetachableModel<List<Course>>() {
            @Override
            protected List<Course> load() {
                return courseService.findAll();
            }
        };

        PropertyListView<Course> eachItem = new PropertyListView<Course>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<Course> item) {
                Link<Course> viewLink = new Link<Course>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new CourseViewPage(getModelObject().getId()));
                    }
                };
                item.add(viewLink);
                viewLink.add(new Label("name"));
                item.add(new Label("courseType"));
                item.add(new Label("qualification"));
                item.add(new Label("basicCourse"));
                item.add(new Label("retired"));
            }
        };

        add(eachItem);

        add(new Link("report") {
            @Override
            public void onClick() {
                Map parameters = new HashMap();
                parameters.put("comment", "Active Courses");
                CourseReportList courseReportList = new CourseReportList();
                try {
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(courseReportList, contentType, courseService.findAll(Boolean.FALSE), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);
                    resource.respond(a);
                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

        add(new Link("retired") {
            @Override
            public void onClick() {
                Map parameters = new HashMap();
                parameters.put("comment", "Retired Courses");
                CourseReportList courseReportList = new CourseReportList();
                try {
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(courseReportList, contentType, courseService.findAll(Boolean.TRUE), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);
                    resource.respond(a);
                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });


    }
}
