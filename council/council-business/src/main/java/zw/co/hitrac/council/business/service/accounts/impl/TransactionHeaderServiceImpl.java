
package zw.co.hitrac.council.business.service.accounts.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.accounts.TransactionHeaderDAO;
import zw.co.hitrac.council.business.domain.accounts.TransactionHeader;
import zw.co.hitrac.council.business.service.accounts.TransactionHeaderService;

import java.util.List;

/**
 *
 * @author Edward Zengeni
 */
@Service
@Transactional
public class TransactionHeaderServiceImpl  implements TransactionHeaderService {
    @Autowired
    private TransactionHeaderDAO transactionHeaderDAO;

    @Transactional
    public TransactionHeader save(TransactionHeader transactionHeader) {
        return transactionHeaderDAO.save(transactionHeader);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<TransactionHeader> findAll() {
        return transactionHeaderDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public TransactionHeader get(Long id) {
        return transactionHeaderDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<TransactionHeader> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * A DAO setter method to facilitate mocking
     * @param transactionHeaderDAO
     */
    public void setTransactionHeaderDAO(TransactionHeaderDAO transactionHeaderDAO) {
        this.transactionHeaderDAO = transactionHeaderDAO;
    }

}
