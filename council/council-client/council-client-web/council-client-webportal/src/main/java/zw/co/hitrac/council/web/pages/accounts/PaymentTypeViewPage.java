package zw.co.hitrac.council.web.pages.accounts;

import zw.co.hitrac.council.web.pages.configure.*;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.PaymentType;
import zw.co.hitrac.council.business.service.accounts.PaymentTypeService;

/**
 *
 * @author charlesc
 */
public class PaymentTypeViewPage extends IBankBookPage {

    @SpringBean
    private PaymentTypeService paymentTypeService;

    public PaymentTypeViewPage(Long id) {
        CompoundPropertyModel<PaymentType> model=new CompoundPropertyModel<PaymentType>(new LoadableDetachablePaymentTypeModel(id));
        setDefaultModel(model);
        add(new Link<PaymentType>("editLink",model){

            @Override
            public void onClick() {
                setResponsePage(new PaymentTypeEditPage(getModelObject().getId()));
            }
            
        });
        add(new BookmarkablePageLink<Void>("returnLink", PaymentTypeListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("paymentType",model.bind("name")));
    }


    private final class LoadableDetachablePaymentTypeModel extends LoadableDetachableModel<PaymentType> {

        private Long id;

        public LoadableDetachablePaymentTypeModel(Long id) {
            this.id = id;
        }

        @Override
        protected PaymentType load() {

            return paymentTypeService.get(id);
        }
    }
}
