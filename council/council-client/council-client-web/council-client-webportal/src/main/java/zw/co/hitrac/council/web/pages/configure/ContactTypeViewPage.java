package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.ContactType;
import zw.co.hitrac.council.business.service.ContactTypeService;

/**
 *
 * @author Matiashe Michael
 */
public class ContactTypeViewPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private ContactTypeService contactTypeService;

    public ContactTypeViewPage(Long id) {
        CompoundPropertyModel<ContactType> model=new CompoundPropertyModel<ContactType>(new LoadableDetachableContactTypeModel(id));
        setDefaultModel(model);
        add(new Link<ContactType>("editLink",model){

            @Override
            public void onClick() {
                setResponsePage(new ContactTypeEditPage(getModelObject().getId()));
            }
            
        });
        add(new BookmarkablePageLink<Void>("returnLink", ContactTypeListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("contactType",model.bind("name")));
    }


    private final class LoadableDetachableContactTypeModel extends LoadableDetachableModel<ContactType> {

        private Long id;

        public LoadableDetachableContactTypeModel(Long id) {
            this.id = id;
        }

        @Override
        protected ContactType load() {

            return contactTypeService.get(id);
        }
    }
}
