package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.TransferRuleDAO;
import zw.co.hitrac.council.business.domain.TransferRule;
import zw.co.hitrac.council.business.service.TransferRuleService;

import java.util.List;

/**
 *
 * @author Michael Matiashe
 */
@Service
@Transactional
public class TransferRuleServiceImpl implements TransferRuleService{
    
    @Autowired
    private TransferRuleDAO transferRuleDAO;
    

    @Transactional
    public TransferRule save(TransferRule transferRule) {
       return transferRuleDAO.save(transferRule);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<TransferRule> findAll() {
        return transferRuleDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public TransferRule get(Long id) {
       return transferRuleDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<TransferRule> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setTransferRuleDAO(TransferRuleDAO transferRuleDAO) {

        this.transferRuleDAO = transferRuleDAO;
    }
    
}
