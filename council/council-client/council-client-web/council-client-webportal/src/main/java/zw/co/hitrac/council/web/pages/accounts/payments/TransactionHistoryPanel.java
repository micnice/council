/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.accounts.payments;

import java.util.ArrayList;
import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.OldReceipt;
import zw.co.hitrac.council.business.domain.accounts.PaymentDetails;
import zw.co.hitrac.council.business.domain.accounts.Prepayment;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.accounts.OldReceiptService;
import zw.co.hitrac.council.business.service.accounts.PaymentDetailsService;
import zw.co.hitrac.council.business.service.accounts.PrepaymentDetailsService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.utility.CustomDateLabel;
import zw.co.hitrac.council.web.utility.DetachableRegistrantModel;

/**
 *
 * @author tdhlakama
 */
public class TransactionHistoryPanel extends Panel {

    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private PaymentDetailsService paymentDetailsService;
    @SpringBean
    private OldReceiptService oldReceiptService;
    @SpringBean
    private PrepaymentDetailsService prepaymentDetailsService;

    public TransactionHistoryPanel(String id, final Long registrantId) {
        super(id);

        final CompoundPropertyModel<Registrant> registrantModel = new CompoundPropertyModel<Registrant>(new DetachableRegistrantModel(registrantId, registrantService));
        setDefaultModel(registrantModel);

        IModel<List<PaymentDetails>> model = new LoadableDetachableModel<List<PaymentDetails>>() {
            @Override
            protected List<PaymentDetails> load() {
                return paymentDetailsService.transactionHistory(registrantModel.getObject().getCustomerAccount());
            }
        };

        IModel<List<OldReceipt>> oldmodel = new LoadableDetachableModel<List<OldReceipt>>() {
            @Override
            protected List<OldReceipt> load() {
                return oldReceiptService.getReceipts(null, registrantModel.getObject());
            }
        };

        ListView<PaymentDetails> eachItem = new ListView<PaymentDetails>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<PaymentDetails> item) {
                Link<PaymentDetails> viewLink = new Link<PaymentDetails>("transactionDetailHistory", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new TransactionDetailHistory(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.setModel(new CompoundPropertyModel<PaymentDetails>(item.getModel()));
                item.add(viewLink);
                item.setModel(new CompoundPropertyModel<PaymentDetails>(item.getModel()));
                item.add(new Label("paymentMethod"));
                item.add(new CustomDateLabel("dateOfPayment"));
                item.add(new CustomDateLabel("dateOfDeposit"));
                item.add(new Label("receiptHeader.totalAmount"));
                item.add(new Label("receiptHeader.id"));
                item.add(new Label("receiptHeader.carryForward"));
                item.add(new Label("receiptHeader.canceled"));
            }
        };

        add(eachItem);

        ListView<OldReceipt> oldEachItem = new ListView<OldReceipt>("oldEachItem", oldmodel) {
            @Override
            protected void populateItem(ListItem<OldReceipt> item) {
                item.setModel(new CompoundPropertyModel<OldReceipt>(item.getModel()));
                item.add(new Label("receiptRecordID"));
                item.add(new Label("paymentMethod"));
                item.add(new Label("product"));
                item.add(new Label("lineTotal"));
                item.add(new CustomDateLabel("transactionDate"));
                item.add(new CustomDateLabel("dateOfReceipt"));
                item.add(new Label("paymentPurpose"));
                item.add(new Label("user"));
            }
        };
        add(oldEachItem);

    }

    public TransactionHistoryPanel(String id, final Prepayment prepayment) {
        super(id);

        IModel<List<PaymentDetails>> model = new LoadableDetachableModel<List<PaymentDetails>>() {
            @Override
            protected List<PaymentDetails> load() {
                return prepaymentDetailsService.findReceiptHeaders(prepayment);
            }
        };

        ListView<PaymentDetails> eachItem = new ListView<PaymentDetails>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<PaymentDetails> item) {
                Link<PaymentDetails> viewLink = new Link<PaymentDetails>("transactionDetailHistory", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new TransactionDetailHistory(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.setModel(new CompoundPropertyModel<PaymentDetails>(item.getModel()));
                item.add(viewLink);
                item.setModel(new CompoundPropertyModel<PaymentDetails>(item.getModel()));
                item.add(new Label("paymentMethod"));
                item.add(new CustomDateLabel("dateOfPayment"));
                item.add(new CustomDateLabel("dateOfDeposit"));
                item.add(new Label("receiptHeader.totalAmount"));
                item.add(new Label("receiptHeader.id"));
                item.add(new Label("receiptHeader.carryForward"));
                item.add(new Label("receiptHeader.canceled"));
            }
        };
        add(eachItem);

        ListView<OldReceipt> oldEachItem = new ListView<OldReceipt>("oldEachItem", new ArrayList<OldReceipt>()) {
            @Override
            protected void populateItem(ListItem<OldReceipt> item) {
                item.setModel(new CompoundPropertyModel<OldReceipt>(item.getModel()));
                item.add(new Label("receiptRecordID"));
                item.add(new Label("paymentMethod"));
                item.add(new Label("product"));
                item.add(new Label("lineTotal"));
                item.add(new CustomDateLabel("transactionDate"));
                item.add(new CustomDateLabel("dateOfReceipt"));
                item.add(new Label("paymentPurpose"));
               item.add(new Label("user"));
            }
        };
        add(oldEachItem);
    }
}
