/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service;

import java.util.Date;
import java.util.List;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantClearance;

/**
 *
 * @author kelvin
 */
public interface RegistrantClearanceService extends IGenericService<RegistrantClearance> {

    public List<RegistrantClearance> getClearances(Registrant registrant);
    
    public Date getRegistrantClearanceDate(Registrant registrant, Course course);
    
    public RegistrantClearance getRegistrantClearance(Registrant registrant, Course course);
    
    public Boolean getHasPBQExamClearance(Registrant registrant);
    
    public Boolean getHasExamClearance(Registrant registrant);

    public Date getLastClearanceDate(Registrant registrant);
}
