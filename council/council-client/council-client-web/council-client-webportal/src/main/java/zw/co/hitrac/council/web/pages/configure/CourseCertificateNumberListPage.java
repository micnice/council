package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CourseCertificateNumber;
import zw.co.hitrac.council.business.service.CourseCertificateNumberService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

import java.util.List;

/**
 *
 * @author Charles Chigoriwa
 */
public class CourseCertificateNumberListPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private CourseCertificateNumberService courseCertificateNumberService;

    public CourseCertificateNumberListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {

            @Override
            public void onClick() {
                setResponsePage(new CourseCertificateNumberEditPage(null));
            }
        });

        IModel<List<CourseCertificateNumber>> model = new LoadableDetachableModel<List<CourseCertificateNumber>>() {

            @Override
            protected List<CourseCertificateNumber> load() {
                return courseCertificateNumberService.findAll();
            }
        };

        PropertyListView<CourseCertificateNumber> eachItem = new PropertyListView<CourseCertificateNumber>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<CourseCertificateNumber> item) {

                Link<CourseCertificateNumber> viewLink = new Link<CourseCertificateNumber>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new CourseCertificateNumberEditPage(getModelObject().getId()));
                    }
                };
                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                viewLink.add(new Label("coursePrefix"));
                item.add(new Label("course"));
                item.add(new Label("register"));
            }
        };

        add(eachItem);
    }

}
