/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.EmploymentStatus;
import zw.co.hitrac.council.business.service.EmploymentStatusService;

/**
 *
 * @author kelvin
 */
public class EmploymentStatusViewPage  extends IAdministerDatabaseBasePage {

    @SpringBean
    private EmploymentStatusService employmentStatusService;

    public EmploymentStatusViewPage(Long id) {
        CompoundPropertyModel<EmploymentStatus> model=new CompoundPropertyModel<EmploymentStatus>(new LoadableDetachableEmploymentStatusModel(id));
        setDefaultModel(model);
        add(new Link<EmploymentStatus>("editLink",model){

            @Override
            public void onClick() {
                setResponsePage(new EmploymentStatusEditPage(getModelObject().getId()));
            }
            
        });
        add(new BookmarkablePageLink<Void>("returnLink", EmploymentStatusListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("employmentStatus",model.bind("name")));
    }


    private final class LoadableDetachableEmploymentStatusModel extends LoadableDetachableModel<EmploymentStatus> {

        private Long id;

        public LoadableDetachableEmploymentStatusModel(Long id) {
            this.id = id;
        }

        @Override
        protected EmploymentStatus load() {

            return employmentStatusService.get(id);
        }
    }
}
