/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.registration;

import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.RegistrantCondition;
import zw.co.hitrac.council.business.service.RegistrantConditionService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.utility.CustomDateLabel;

/**
 *
 * @author kelvin
 */
public class ConditionsPanelList extends Panel {

    public ConditionsPanelList(String id, final List <RegistrantCondition> items) {
        super(id);


        add(new PropertyListView<RegistrantCondition>("registrantConditionList", items) {
            @Override
            protected void populateItem(ListItem<RegistrantCondition> item) {
                Link<RegistrantCondition> viewLink = new Link<RegistrantCondition>("registrantConditionLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new RegistrantConditionEditPage(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(viewLink);
                item.add(new Label("condition"));                
                item.add(new Label("registrant.fullname"));                
                item.add(new CustomDateLabel("endDate"));

            }
        });

    }
}
