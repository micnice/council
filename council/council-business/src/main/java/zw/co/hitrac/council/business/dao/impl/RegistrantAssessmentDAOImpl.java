/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.RegistrantAssessmentDAO;
import zw.co.hitrac.council.business.dao.repo.RegistrantAssessmentRepository;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantAssessment;

/**
 *
 * @author kelvin
 */
@Repository
public class RegistrantAssessmentDAOImpl implements RegistrantAssessmentDAO {

    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    private RegistrantAssessmentRepository registrantAssessmentRepository;

    public RegistrantAssessment save(RegistrantAssessment registrantAssessment) {
        return registrantAssessmentRepository.save(registrantAssessment);
    }

    public List<RegistrantAssessment> findAll() {
        return registrantAssessmentRepository.findAll();
    }

    public RegistrantAssessment get(Long id) {
        return registrantAssessmentRepository.findOne(id);
    }

    /**
     * A setter method that will make mocking repo object easier
     *
     * @param registrantAssessmentRepository
     */
    public void setRegistrantAssessmentRepository(RegistrantAssessmentRepository registrantAssessmentRepository) {
        this.registrantAssessmentRepository = registrantAssessmentRepository;
    }

    public List<RegistrantAssessment> getAssessments(Registrant registrant) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantAssessment.class);
        criteria.add(Restrictions.eq("registrant", registrant));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();

    }

    public List<RegistrantAssessment> getAssessments(Institution institution) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantAssessment.class);
        criteria.add(Restrictions.eq("institution", institution));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<RegistrantAssessment> getAssessments(Application application) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantAssessment.class);
        criteria.add(Restrictions.eq("application", application));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<RegistrantAssessment> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
