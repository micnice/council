package zw.co.hitrac.council.web.config;

import org.jasypt.springsecurity3.authentication.encoding.PasswordEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;

import java.util.Objects;

import javax.inject.Inject;

import zw.co.hitrac.council.business.config.CouncilBusinessConfiguration;

/**
 * Created by scott on 06/08/16.
 */

@Configuration
@EnableWebSecurity
@Import(CouncilBusinessConfiguration.class)
public class CouncilWebConfiguration extends WebSecurityConfigurerAdapter {

  @Inject
  private UserDetailsService userDetailsService;
  @Inject
  private PasswordEncoder encoder;

  @Override
  protected void configure(HttpSecurity http) throws Exception {

    http
        .csrf().disable()
    ;
  }

  @Override
  public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {

    Objects.requireNonNull(userDetailsService, "User Details Service was required but none was provided");

    authenticationManagerBuilder
        .userDetailsService(userDetailsService)
        .passwordEncoder(encoder);
  }


  @Override
  @Bean(name = "councilAuthenticationManager")
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

  @Bean
  public SecurityContextPersistenceFilter securityContextPersistenceFilter() {
    //Important!!! For Wicket-Spring integration session management
    return new SecurityContextPersistenceFilter();
  }

}
