
package zw.co.hitrac.council.web.pages.registration;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantContact;
import zw.co.hitrac.council.business.service.ContactTypeService;
import zw.co.hitrac.council.business.service.RegistrantContactService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.institution.InstitutionContactAddressPage;
import zw.co.hitrac.council.web.pages.institution.InstitutionViewPage;

/**
 *
 * @author kelvin
 */
public class RegistrantContactEditPage extends TemplatePage {

    @SpringBean
    private RegistrantContactService registrantContactService;
    @SpringBean
    private ContactTypeService contactTypeService;

    public RegistrantContactEditPage(final IModel<Registrant> registrantModel) {

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());

        CompoundPropertyModel<RegistrantContact> model = new CompoundPropertyModel<RegistrantContact>(
                new RegistrantContactEditPage.LoadableDetachableRegistrantContactServiceModel(
                registrantModel.getObject()));

        setDefaultModel(model);

        Form<RegistrantContact> form = new Form<RegistrantContact>("form", (IModel<RegistrantContact>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    registrantContactService.save(getModelObject());
                    setResponsePage(new RegistrantContactAddressPage(getModelObject().getRegistrant().getId()));
                } catch (CouncilException ex) {
                    error(ex.getMessage());

                }

            }
        };

        form.add(new DropDownChoice("contactType", contactTypeService.findAll()).setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("contactDetail").setRequired(true).add(new ErrorBehavior()));

        add(form);
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantContactAddressPage(registrantModel.getObject().getId()));
            }
        });

        add(new Label("registrant.fullname"));
        add(new Label("institution.name") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
    }

    public RegistrantContactEditPage(final Institution institution) {

        CompoundPropertyModel<RegistrantContact> model = new CompoundPropertyModel<RegistrantContact>(
                new RegistrantContactEditPage.LoadableDetachableInstitutionContactServiceModel(institution));
        setDefaultModel(model);

        Form<RegistrantContact> form = new Form<RegistrantContact>("form", (IModel<RegistrantContact>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    registrantContactService.save(getModelObject());
                    setResponsePage(new InstitutionContactAddressPage(institution.getId()));
                } catch (CouncilException ex) {
                    error(ex.getMessage());

                }

            }
        };

        form.add(new DropDownChoice("contactType", contactTypeService.findAll()).setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("contactDetail").setRequired(true).add(new ErrorBehavior()));

        add(form);
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionViewPage(institution.getId()));
            }
        });

        add(new Label("registrant.fullname") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        add(new Label("institution.name"));
    }

    public RegistrantContactEditPage(long id) {

        CompoundPropertyModel<RegistrantContact> model = new CompoundPropertyModel<RegistrantContact>(
                new RegistrantContactEditPage.LoadableDetachableRegistrantContactServiceModel(id));
        Long rId = null;
        Long iId = null;
        if (model.getObject().getRegistrant() != null) {
            rId = model.getObject().getRegistrant().getId();
        } else {
            iId = model.getObject().getInstitution().getId();
        }

        final Long registrantId = rId;
        final Long instituionId = iId;

        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantId);

        setDefaultModel(model);

        Form<RegistrantContact> form = new Form<RegistrantContact>("form", (IModel<RegistrantContact>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    registrantContactService.save(getModelObject());
                    if (registrantId != null) {
                        setResponsePage(new RegistrantContactAddressPage(registrantId));
                    }
                    if (instituionId != null) {
                        setResponsePage(new InstitutionViewPage(instituionId));
                    }
                } catch (CouncilException ex) {
                    error(ex.getMessage());

                }

            }
        };

        form.add(new DropDownChoice("contactType", contactTypeService.findAll()).setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("contactDetail").setRequired(true).add(new ErrorBehavior()));
        add(form);

        if (registrantId != null) {
            add(new Label("registrant.fullname"));
            add(new Label("institution.name") {
                @Override
                protected void onConfigure() {
                    setVisible(Boolean.FALSE);
                }
            });
        }
        if (instituionId != null) {
            add(new Label("registrant.fullname") {
                @Override
                protected void onConfigure() {
                    setVisible(Boolean.FALSE);
                }
            });
            add(new Label("institution.name"));
        }
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                if (registrantId != null) {
                    setResponsePage(new RegistrantContactAddressPage(registrantId));
                }
                if (instituionId != null) {
                    setResponsePage(new InstitutionViewPage(instituionId));
                }
            }
        });

    }

    private final class LoadableDetachableRegistrantContactServiceModel extends LoadableDetachableModel<RegistrantContact> {

        private Long id;
        private Registrant registrant;

        public LoadableDetachableRegistrantContactServiceModel(Long id) {
            this.id = id;
        }

        public LoadableDetachableRegistrantContactServiceModel(Registrant registrant) {
            this.registrant = registrant;
        }

        @Override
        protected RegistrantContact load() {
            RegistrantContact registrantContact = null;

            if (id == null) {
                registrantContact = new RegistrantContact();
                registrantContact.setRegistrant(registrant);
            } else {
                registrantContact = registrantContactService.get(id);
            }

            return registrantContact;
        }
    }

    private final class LoadableDetachableInstitutionContactServiceModel extends LoadableDetachableModel<RegistrantContact> {

        private Long id;
        private Institution institution;

        public LoadableDetachableInstitutionContactServiceModel(Long id) {
            this.id = id;
        }

        public LoadableDetachableInstitutionContactServiceModel(Institution institution) {
            this.institution = institution;
        }

        @Override
        protected RegistrantContact load() {
            RegistrantContact registrantContact = null;

            if (id == null) {
                registrantContact = new RegistrantContact();
                registrantContact.setInstitution(institution);
            } else {
                registrantContact = registrantContactService.get(id);
            }

            return registrantContact;
        }
    }
}
