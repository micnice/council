/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package zw.co.hitrac.council.business.domain.accounts;

import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.domain.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author micnice
 */
@Entity
@Table(name = "paymentdata")
@Audited
public class PaymentData extends BaseEntity implements Serializable{
    private String receiptNumber;
    private Date dateOfPayment;
    private String regNumber;
    private String name;
    private String description;
    private String code;
    private BigDecimal amount;
    private BigDecimal totalAmount;
    private Boolean postedToPersonalAccountt = Boolean.FALSE;
    private Boolean postedToSalesAccount = Boolean.FALSE;

    public Boolean isPostedToPersonalAccountt() {
        return postedToPersonalAccountt;
    }

    public void setPostedToPersonalAccountt(Boolean postedToPersonalAccountt) {
        this.postedToPersonalAccountt = postedToPersonalAccountt;
    }

    public Boolean isPostedToSalesAccount() {
        return postedToSalesAccount;
    }

    public void setPostedToSalesAccount(Boolean postedToSalesAccount) {
        this.postedToSalesAccount = postedToSalesAccount;
    }
    
    @Column(updatable = true)
    public String getReceiptNumber() {
        return receiptNumber;
    }

    public void setReceiptNumber(String receiptNumber) {
        this.receiptNumber = receiptNumber;
    }

    public Date getDateOfPayment() {
        return dateOfPayment;
    }

    public void setDateOfPayment(Date dateOfPayment) {
        this.dateOfPayment = dateOfPayment;
    }

    public String getRegNumber() {
        return regNumber;
    }

    public void setRegNumber(String regNumber) {
        this.regNumber = regNumber;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getAmount() {
        if(amount == null){
            return BigDecimal.ZERO;
        }
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getTotalAmount() {
        if(totalAmount == null){
            return BigDecimal.ZERO;
        }
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }
    
}
