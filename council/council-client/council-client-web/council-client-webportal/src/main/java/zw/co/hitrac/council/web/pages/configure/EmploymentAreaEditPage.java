/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.EmploymentArea;
import zw.co.hitrac.council.business.service.EmploymentAreaService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author kelvin
 */
public class EmploymentAreaEditPage extends IAdministerDatabaseBasePage{
    
    @SpringBean
    private EmploymentAreaService employmentAreaService;

    public EmploymentAreaEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<EmploymentArea>(new EmploymentAreaEditPage.LoadableDetachableEmploymentAreaModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink",AdministerDatabasePage.class));
        Form<EmploymentArea> form=new Form<EmploymentArea>("form",(IModel<EmploymentArea>)getDefaultModel()) {
            @Override
            public void onSubmit(){
                employmentAreaService.save(getModelObject());
                setResponsePage(new EmploymentAreaViewPage(getModelObject().getId()));
            }
        };
        
        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior())); 
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink",EmploymentAreaListPage.class));
        add(form);
    }
    
    
    private final class LoadableDetachableEmploymentAreaModel extends LoadableDetachableModel<EmploymentArea> {

        private Long id;

        public LoadableDetachableEmploymentAreaModel(Long id) {
            this.id = id;
        }

        @Override
        protected EmploymentArea load() {
            if(id==null){
                return new EmploymentArea();
            }
            return employmentAreaService.get(id);
        }
    }
    
    
}

    

