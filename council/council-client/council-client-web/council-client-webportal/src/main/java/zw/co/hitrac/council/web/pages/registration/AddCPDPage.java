package zw.co.hitrac.council.web.pages.registration;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantCpd;
import zw.co.hitrac.council.business.process.RenewalProcess;
import zw.co.hitrac.council.business.service.CouncilDurationService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrantActivityService;
import zw.co.hitrac.council.business.service.RegistrantCpdService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.certification.CertificationPage;
import zw.co.hitrac.council.web.utility.CustomDateTextField;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tdhlakama
 */
public class AddCPDPage extends TemplatePage {

    @SpringBean
    private RegistrantCpdService registrantCpdService;
    @SpringBean
    private RenewalProcess renewalProcess;
    @SpringBean
    private CouncilDurationService councilDurationService;

    private CouncilDuration councilDuration;
    @SpringBean
    private GeneralParametersService generalParametersService;

    @SpringBean
    private RegistrantActivityService registrantActivityService;

    public AddCPDPage(final IModel<Registrant> registrantModel) {
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());

        PropertyModel<CouncilDuration> councilDurationModel = new PropertyModel<CouncilDuration>(this, "councilDuration");

        CompoundPropertyModel<RegistrantCpd> model = new CompoundPropertyModel<RegistrantCpd>(
                new AddCPDPage.LoadableDetachableRegistrantCpdModel(
                        registrantModel.getObject()));

        setDefaultModel(model);

        Form<RegistrantCpd> form = new Form<RegistrantCpd>("form", (IModel<RegistrantCpd>) getDefaultModel()) {
            @Override
            public void onSubmit() {

                try {
                    List<RegistrantCpd> items = new ArrayList<RegistrantCpd>();
                    items.add(getModel().getObject());
                    renewalProcess.addCPDs(registrantModel.getObject(), councilDurationModel.getObject(), items);
                    if (generalParametersService.get().getAllowRenewalBeforeCPDs()) {
                        CouncilDuration lastCouncilDuration = registrantActivityService.getLastCouncilDurationRenewal(registrantModel.getObject());
                        if (lastCouncilDuration != null && lastCouncilDuration.equals(councilDuration)) {
                            setResponsePage(new CertificationPage(getModelObject().getRegistrant().getId()));
                        } else {
                            setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
                        }
                    } else {
                        setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
                    }
                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };
        if(generalParametersService.get().getShowActiveDurationsOnly()){
            form.add(new DropDownChoice("councilDuration", councilDurationModel, councilDurationService.getActiveCouncilDurations()).setRequired(true).add(new ErrorBehavior()));
        }
        else
        {  form.add(new DropDownChoice("councilDuration", councilDurationModel, councilDurationService.findAll()).setRequired(true).add(new ErrorBehavior()));}

        form.add(new TextField("point").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<Integer>("activityNo"));
        form.add(new TextField<String>("organisation"));
        form.add(new CustomDateTextField("issuedDate").add(DatePickerUtil.getDatePicker()));
        form.add(new CustomDateTextField("expiryDate").add(DatePickerUtil.getDatePicker()));
        form.add(new TextField<String>("activityAttended"));

        add(new FeedbackPanel("feedback"));

        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });
        add(form);
        add(new Label("registrant.fullname"));

    }

    private final class LoadableDetachableRegistrantCpdModel extends LoadableDetachableModel<RegistrantCpd> {

        private Long id;
        private Registrant registrant;

        public LoadableDetachableRegistrantCpdModel(Long id) {
            this.id = id;
        }

        public LoadableDetachableRegistrantCpdModel(Registrant registrant) {
            this.registrant = registrant;
        }

        @Override
        protected RegistrantCpd load() {
            RegistrantCpd registrantCpd = null;

            if (id == null) {
                registrantCpd = new RegistrantCpd();
                registrantCpd.setRegistrant(registrant);
            } else {
                registrantCpd = registrantCpdService.get(id);
            }

            return registrantCpd;
        }
    }
}
