package zw.co.hitrac.council.business.domain;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 *
 * @author Kelvin Goredema
 * @author Charles Chigoriwa
 * @author Michael Matiashe
 */
@Entity
@Table(name = "registration")
@Audited
public class Registration extends BaseIdEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    private Date registrationDate;
    private Date deRegistrationDate;
    private Registrant registrant;
    private Application application;
    private Course course;
    private Register register;
    private Institution institution;
    private DebtComponent debtComponent;
    private DebtComponent deptComponent1;
    private Boolean reRegistration = Boolean.FALSE;
    private Duration duration;
    private RegistrantStatus registrantStatus;
    private Set<ExamRegistration> examRegistrations = new HashSet<ExamRegistration>();

    @ManyToOne
    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    @Temporal(TemporalType.DATE)
    public Date getRegistrationDate() {
        return registrationDate;
    }

    @OneToOne
    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    @Temporal(TemporalType.DATE)
    public Date getDeRegistrationDate() {
        return deRegistrationDate;
    }

    public void setDeRegistrationDate(Date deRegistrationDate) {
        this.deRegistrationDate = deRegistrationDate;
    }

    @ManyToOne
    public Registrant getRegistrant() {
        return registrant;
    }

    public void setRegistrant(Registrant registrant) {
        this.registrant = registrant;
    }

    @ManyToOne
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @ManyToOne
    public Register getRegister() {
        return register;
    }

    public void setRegister(Register register) {
        this.register = register;
    }

    @Cascade(CascadeType.DETACH)
    @ManyToOne
    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    @OneToOne
    public DebtComponent getDeptComponent1() {
        return deptComponent1;
    }

    public void setDeptComponent1(DebtComponent deptComponent1) {
        this.deptComponent1 = deptComponent1;
    }

    @OneToOne
    public DebtComponent getDebtComponent() {
        return debtComponent;
    }

    public void setDebtComponent(DebtComponent debtComponent) {
        this.debtComponent = debtComponent;
    }

    @OneToMany(mappedBy = "registration", cascade = javax.persistence.CascadeType.ALL)
    public Set<ExamRegistration> getExamRegistrations() {
        return examRegistrations;
    }

    public void setExamRegistrations(Set<ExamRegistration> examRegistrations) {
        this.examRegistrations = examRegistrations;
    }

    public Boolean isReRegistration() {
        if (reRegistration == null) {
            return Boolean.FALSE;
        }
        return reRegistration;
    }

    public void setReRegistration(Boolean reRegistration) {
        this.reRegistration = reRegistration;
    }
    
    @ManyToOne
    public RegistrantStatus getRegistrantStatus() {
        return registrantStatus;
    }

    public void setRegistrantStatus(RegistrantStatus registrantStatus) {
        this.registrantStatus = registrantStatus;
    }
    
    @Transient
    public List<ExamRegistration> getExamRegistrationsList() {
        List<ExamRegistration> items = new ArrayList<ExamRegistration>();
        if (examRegistrations != null && !examRegistrations.isEmpty()) {
            getExamRegistrations().size();
            items.addAll(getExamRegistrations());
        }
        return items;
    }

    @Override
    public String toString() {
        return getId().toString();
    }
}
