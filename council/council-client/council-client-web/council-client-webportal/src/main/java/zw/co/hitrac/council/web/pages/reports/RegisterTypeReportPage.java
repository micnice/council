
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.reports;

//~--- non-JDK imports --------------------------------------------------------
import java.util.Arrays;
import net.sf.jasperreports.engine.JRException;

import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

//~--- JDK imports ------------------------------------------------------------

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import zw.co.hitrac.council.business.domain.RegisterType;
import zw.co.hitrac.council.business.service.RegisterTypeService;
import zw.co.hitrac.council.business.service.RegistrantQualificationService;
import zw.co.hitrac.council.reports.RegistrantsReport;
import zw.co.hitrac.council.web.models.RegisterTypeListModel;

/**
 *
 * @author tdhlakama
 */
public class RegisterTypeReportPage extends IReportPage {

    @SpringBean
    private RegisterTypeService RegisterTypeService;
    @SpringBean
    private RegistrantQualificationService registrantQualificationService;
    private RegisterType registerType;
    private ContentType contentType;

    public RegisterTypeReportPage() {
        PropertyModel<RegisterType> registerTypeModel = new PropertyModel<RegisterType>(this, "registerType");
        PropertyModel<ContentType> contentTypeModel = new PropertyModel<ContentType>(this, "contentType");
        Form<?> form = new Form("form") {
            @Override
            protected void onSubmit() {
                try {
                    RegistrantsReport registrantsReport = new RegistrantsReport();
                    
                    Map parameters = new HashMap();

                    parameters.put("comment", "List of Individuals who are authorised to practice as a " + registerType.getName());

                    ByteArrayResource resource;

                    try {
                        resource = ReportResourceUtils.getReportResource(registrantsReport, contentType,
                                registrantQualificationService.getRegistrants(registerType),
                                parameters);

                        IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                RequestCycle.get().getResponse(), null);

                        resource.respond(a);
                    } catch (JRException ex) {
                         Logger.getLogger(RegistrantsReport.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };

        form.add(new DropDownChoice("registerType", registerTypeModel, new RegisterTypeListModel(RegisterTypeService)).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("contentType", contentTypeModel, Arrays.asList(ContentType.values())));
        
        add(form);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
