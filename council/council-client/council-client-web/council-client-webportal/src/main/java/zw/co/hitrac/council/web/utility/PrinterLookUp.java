package zw.co.hitrac.council.web.utility;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;

/**
 *
 * @author tdhlakama
 */
public class PrinterLookUp {

    public static void main(String[] args) {
        PrintService[] printServices = PrintServiceLookup.lookupPrintServices(null, null);
        System.out.println("Number of print services: " + printServices.length);

        for (PrintService printer : printServices) {
            System.out.println("Printer: " + printer.getName());
        }
    }
}
