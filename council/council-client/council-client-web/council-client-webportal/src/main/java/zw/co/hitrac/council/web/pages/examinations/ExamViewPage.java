
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.examinations.Exam;
import zw.co.hitrac.council.business.service.examinations.ExamService;
import zw.co.hitrac.council.business.service.examinations.ExamSettingService;
import zw.co.hitrac.council.web.utility.CustomDateLabel;

/**
 *
 * @author tdhlakama
 */
public class ExamViewPage extends IExaminationsPage {

    @SpringBean
    private ExamService examService;
    @SpringBean
    private ExamSettingService examSettingService;

    public ExamViewPage(Long id) {
        CompoundPropertyModel<Exam> model =
                new CompoundPropertyModel<Exam>(new ExamViewPage.LoadableDetachableExamModel(id));

        setDefaultModel(model);
        add(new Link<Exam>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new ExamEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", ExamListPage.class));
        add(new Label("modulePaper"));
        add(new Label("examSetting"));
        add(new CustomDateLabel("examDate"));
        add(new Label("passMark"));
    }

    private final class LoadableDetachableExamModel extends LoadableDetachableModel<Exam> {

        private Long id;

        public LoadableDetachableExamModel(Long id) {
            this.id = id;
        }

        @Override
        protected Exam load() {
            return examService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
