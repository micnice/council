
package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.GeneralParameters;
import zw.co.hitrac.council.business.domain.PaymentConfiguration;

/**
 *
 * @author Kelvin Goredema
 */
public interface PaymentConfigurationService extends IGenericService<PaymentConfiguration> {

}
