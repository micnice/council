package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.MaritalStatusDAO;
import zw.co.hitrac.council.business.dao.repo.MaritalStatusRepository;
import zw.co.hitrac.council.business.domain.MaritalStatus;

/**
 *
 * @author Edawrd Zengeni
 */
@Repository
public class MaritalStatusDAOImpl implements MaritalStatusDAO {

    @Autowired
    private MaritalStatusRepository maritalStatusRepository;

    public MaritalStatus save(MaritalStatus maritalStatus) {
        return maritalStatusRepository.save(maritalStatus);
    }

    public List<MaritalStatus> findAll() {
        return maritalStatusRepository.findAll();
    }

    public MaritalStatus get(Long id) {
        return maritalStatusRepository.findOne(id);
    }

    public void setMaritalStatusRepository(MaritalStatusRepository maritalStatusRepository) {
        this.maritalStatusRepository = maritalStatusRepository;
    }

    public List<MaritalStatus> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

	@Override
	public MaritalStatus findByName(String name) {
		return maritalStatusRepository.findByName(name);
	}
}
