package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.RegistrantStatus;
import zw.co.hitrac.council.business.service.RegistrantStatusService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Charles Chigoriwa
 */
public class RegistrantStatusEditPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private RegistrantStatusService registrantStatusService;

    public RegistrantStatusEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<RegistrantStatus>(new LoadableDetachableRegistrantStatusModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));

        Form<RegistrantStatus> form = new Form<RegistrantStatus>("form", (IModel<RegistrantStatus>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                registrantStatusService.save(getModelObject());
                setResponsePage(new RegistrantStatusViewPage(getModelObject().getId()));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink", RegistrantStatusListPage.class));
        add(form);
    }

    private final class LoadableDetachableRegistrantStatusModel extends LoadableDetachableModel<RegistrantStatus> {
        private Long id;

        public LoadableDetachableRegistrantStatusModel(Long id) {
            this.id = id;
        }

        @Override
        protected RegistrantStatus load() {
            if (id == null) {
                return new RegistrantStatus();
            }

            return registrantStatusService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
