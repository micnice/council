package zw.co.hitrac.council.business.domain.accounts;

import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.domain.BaseIdEntity;
import zw.co.hitrac.council.business.domain.CouncilDuration;

import javax.persistence.*;
import java.util.Date;

/**
 *
 * @author Matiashe Michael
 * @author Charles Chigoriwa
 */
@Entity
@Table(name="paymentdetails", uniqueConstraints = @UniqueConstraint(columnNames = {"uid"}))
@Audited
public class PaymentDetails extends BaseIdEntity{

    private Customer customer;
    private PaymentMethod paymentMethod;
    private Date dateOfPayment;
    private Date dateOfDeposit;
    private Account bankAccount;
    private TransactionHeader transactionHeader;
    private ReceiptHeader receiptHeader;
    private PaymentType paymentType;
    private CouncilDuration councilDuration;

    @ManyToOne
    public CouncilDuration getCouncilDuration() {
        return councilDuration;
    }

    public void setCouncilDuration(CouncilDuration councilDuration) {
        this.councilDuration = councilDuration;
    }

    
    public PaymentDetails() {
    }

    public PaymentDetails(Customer customer) {
        this.customer = customer;
    }

    @ManyToOne
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer= customer;
    }

    @OneToOne
    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    @OneToOne(mappedBy = "paymentDetails")
    public ReceiptHeader getReceiptHeader() {
        return receiptHeader;
    }

    public void setReceiptHeader(ReceiptHeader receiptHeader) {
        this.receiptHeader = receiptHeader;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDateOfPayment() {
        return dateOfPayment;
    }

    public void setDateOfPayment(Date dateOfPayment) {
        this.dateOfPayment = dateOfPayment;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDateOfDeposit() {
        return dateOfDeposit;
    }

    public void setDateOfDeposit(Date dateOfDeposit) {
        this.dateOfDeposit = dateOfDeposit;
    }

    @OneToOne
    public TransactionHeader getTransactionHeader() {
        return transactionHeader;
    }

    public void setTransactionHeader(TransactionHeader transactionHeader) {
        this.transactionHeader = transactionHeader;
    }

    @ManyToOne
    public Account getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(Account bankAccount) {
        this.bankAccount = bankAccount;
    }

   

    @ManyToOne
    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }
    
   
}
