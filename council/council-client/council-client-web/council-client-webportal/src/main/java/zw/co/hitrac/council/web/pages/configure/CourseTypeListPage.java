package zw.co.hitrac.council.web.pages.configure;

import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CourseType;
import zw.co.hitrac.council.business.service.CourseTypeService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

/**
 *
 * @author Charles Chigoriwa
 */
public class CourseTypeListPage extends IAdministerDatabaseBasePage{
    
    @SpringBean
    private CourseTypeService courseTypeService;

    public CourseTypeListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink",AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink",AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {

            @Override
            public void onClick() {
               setResponsePage(new CourseTypeEditPage(null));
            }
        });
        
        IModel<List<CourseType>> model=new LoadableDetachableModel<List<CourseType>>() {

            @Override
            protected List<CourseType> load() {
               return courseTypeService.findAll();
            }
        };
        
        PropertyListView<CourseType> eachItem=new PropertyListView<CourseType>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<CourseType> item) {
                Link<CourseType> viewLink=new Link<CourseType>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                       setResponsePage(new CourseTypeViewPage(getModelObject().getId()));
                    }
                };
                if(item.getIndex()%2==0){
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };
        
        add(eachItem);
    }
    
    
    
}
