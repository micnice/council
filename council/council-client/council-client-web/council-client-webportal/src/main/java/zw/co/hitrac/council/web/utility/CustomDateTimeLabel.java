package zw.co.hitrac.council.web.utility;

import org.apache.wicket.datetime.markup.html.basic.DateLabel;
import org.apache.wicket.model.IModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 * Created by clive on 7/8/15.
 * <p>
 * Allows setting custom date format
 */
public class CustomDateTimeLabel extends DateLabel {

    private static DateFormat timeFormat = new SimpleDateFormat("HH:mm");

    public CustomDateTimeLabel(String id) {

        super(id, DateTimeUtils.getDateTimeConverter());
    }

    /**
     * Handle removal of midnight values due to conversion from old dates without times to new dates with datetime
     * format
     *
     * @param id
     * @param dateModel
     * @return
     */
    public static DateLabel forDate(String id, IModel<Date> dateModel) {

        Objects.requireNonNull(dateModel, "Date model is required");
        Date date = dateModel.getObject();

        if (date == null || isMidnight(date)) {
            return new DateLabel(id, DateTimeUtils.getConverter());
        }

        return new DateLabel(id, DateTimeUtils.getDateTimeConverter());
    }

    private static boolean isMidnight(Date date) {

        if (date == null) {
            return false;
        }
        System.out.println(timeFormat.format(date));
        return timeFormat.format(date).equals("00:00");
    }

}
