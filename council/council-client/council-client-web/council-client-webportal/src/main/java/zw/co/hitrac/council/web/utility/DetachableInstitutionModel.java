package zw.co.hitrac.council.web.utility;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.service.InstitutionService;

/**
 *
 * @author Michael Matiashe
 */
public class DetachableInstitutionModel extends LoadableDetachableModel<Institution> {

    private static final long serialVersionUID = 1L;
    /**
     * database identity of the registrant
     */
    private final long id;
    /**
     * service dao reference - must be a wicket-wrapped proxy, holding onto a
     * reference to the real service dao will cause its serialization into
     * session or a not-serializable exception when the servlet container
     * serializes the session.
     */
    private final InstitutionService institutionService;

    /**
     * Constructor
     *
     * @param id
     * @param institutionService 
     */
    public DetachableInstitutionModel(Long id, InstitutionService institutionService) {
        this.id = id;
        this.institutionService = institutionService;
    }

    /**
     * Constructor
     *
     * @param institution 
     * @param institutionService 
     */
    public DetachableInstitutionModel(Institution institution, InstitutionService institutionService) {
        id = institution.getId();
        this.institutionService = institutionService;
    }

    /**
     * Loads the institution from the database
     *
     * @see LoadableDetachableModel#load()
     */
    @Override
    protected Institution load() {
        Institution i = institutionService.get(id);

        if (i == null) {
            return new Institution();
        } else {
            return i;
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
