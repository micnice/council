package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Tier;
import zw.co.hitrac.council.business.service.TierService;

/**
 *
 * @author Edward Zengeni
 */
public class TierViewPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private TierService tierService;

    public TierViewPage(Long id) {
        CompoundPropertyModel<Tier> model=new CompoundPropertyModel<Tier>(new LoadableDetachableTierModel(id));
        setDefaultModel(model);
        add(new Link<Tier>("editLink",model){

            @Override
            public void onClick() {
                setResponsePage(new TierEditPage(getModelObject().getId()));
            }
            
        });
        add(new BookmarkablePageLink<Void>("returnLink", TierListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("code"));
        add(new Label("tier",model.bind("name")));
    }


    private final class LoadableDetachableTierModel extends LoadableDetachableModel<Tier> {

        private Long id;

        public LoadableDetachableTierModel(Long id) {
            this.id = id;
        }

        @Override
        protected Tier load() {

            return tierService.get(id);
        }
    }
}
