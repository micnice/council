package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.service.DurationService;
import zw.co.hitrac.council.web.utility.CustomDateLabel;

/**
 *
 * @author charlesc
 */
public class DurationViewPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private DurationService durationService;

    public DurationViewPage(Long id) {
        CompoundPropertyModel<Duration> model = new CompoundPropertyModel<Duration>(new LoadableDetachableDurationModel(id));
        setDefaultModel(model);
        add(new Link<Duration>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new DurationEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", DurationListPage.class));
        add(new Label("name"));
        add(new Label("councilDuration"));
        add(new CustomDateLabel("startDate"));
        add(new CustomDateLabel("endDate"));
        add(new Label("courses"));
        add(new Label("duration", model.bind("name")));
        add(new Label("textStatus"));
    }

    private final class LoadableDetachableDurationModel extends LoadableDetachableModel<Duration> {

        private Long id;

        public LoadableDetachableDurationModel(Long id) {
            this.id = id;
        }

        @Override
        protected Duration load() {

            return durationService.get(id);
        }
    }
}
