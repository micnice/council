/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.registrar;

import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.accounts.payments.FinancePage;
import zw.co.hitrac.council.web.pages.certification.SearchCertificatePage;
import zw.co.hitrac.council.web.pages.examinations.ExaminationsPage;

import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;

/**
 * @author kelvin
 * @author Michael Matiashe
 */
public class RegistrarPage extends TemplatePage {

    public RegistrarPage() {

        add(new BookmarkablePageLink<Void>("searchCertificatePage", SearchCertificatePage.class));

        add(new BookmarkablePageLink<Void>("financePage", FinancePage.class) {
            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRAR, Role.DEPUTY_REGISTRAR)));
                return enabled;
            }
            });

        add(new BookmarkablePageLink<Void>("examinationsLink", ExaminationsPage.class) {
            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRAR, Role.DEPUTY_REGISTRAR)));
                return enabled;
            }
        });

        add(new BookmarkablePageLink<Void>("emailMessageListPage", EmailMessageListPage.class) {
            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRAR, Role.DEPUTY_REGISTRAR)));
                return enabled;
            }
        });

        add(new BookmarkablePageLink<Void>("SMSMessageListPage", SMSMessageListPage.class) {
            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRAR, Role.DEPUTY_REGISTRAR)));
                return enabled;
            }
        });

        add(new BookmarkablePageLink<Void>("testEmailPage", TestEmailPage.class) {
            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRAR, Role.DEPUTY_REGISTRAR)));
                return enabled;
            }
        });

        add(new Link<Void>("emailMessageEditPage") {

            @Override
            public void onClick() {
                setResponsePage(new EmailMessageEditPage(null));
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRAR, Role.DEPUTY_REGISTRAR)));
                return enabled;
            }
        });

        add(new Link("SMSMessageEditPage") {

            @Override
            public void onClick() {
                setResponsePage(new SMSMessageEditPage(null));
            }

            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRAR, Role.DEPUTY_REGISTRAR)));
                return enabled;
            }
        });

    };

}
