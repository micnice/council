package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.RegistrantTransferDAO;
import zw.co.hitrac.council.business.domain.RegistrantTransfer;
import zw.co.hitrac.council.business.service.RegistrantTransferService;

import java.util.List;

/**
 *
 * @author Michael Matiashe
 */
@Service
@Transactional
public class RegistrantTransferServiceImpl implements RegistrantTransferService{
    
    @Autowired
    private RegistrantTransferDAO registrantTransferDAO;
    

    @Transactional
    public RegistrantTransfer save(RegistrantTransfer registrantTransfer) {
       return registrantTransferDAO.save(registrantTransfer);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantTransfer> findAll() {
        return registrantTransferDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantTransfer get(Long id) {
       return registrantTransferDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantTransfer> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setRegistrantTransferDAO(RegistrantTransferDAO registrantTransferDAO) {

        this.registrantTransferDAO = registrantTransferDAO;
    }
    
}
