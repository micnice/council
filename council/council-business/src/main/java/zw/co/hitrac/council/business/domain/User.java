package zw.co.hitrac.council.business.domain;

import org.apache.commons.lang.BooleanUtils;
import org.hibernate.envers.Audited;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import zw.co.hitrac.council.business.utils.CaseUtil;
import zw.co.hitrac.council.business.utils.Role;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Clive Gurure
 * @author Michael Matiashe
 */
@Entity
@Table(name = "user")
@Audited
@XmlRootElement
public class User extends BaseIdEntity implements UserDetails {

  private String username;
  private String firstName;
  private String surname;
  private String email;
  private String password;
  private String oldPassword;
  private String newPassword;
  private String printerName;
  private Set<Role> roles = new HashSet<Role>();
  private Boolean authoriser = Boolean.FALSE;
  private Boolean isEncrypted = Boolean.FALSE;
  private Boolean applicationProcessor = Boolean.FALSE;
  private Boolean councilProcessor = Boolean.FALSE;
  private Boolean enabled = Boolean.TRUE;
  private Boolean accountNonExpired = Boolean.TRUE;
  private Boolean accountNonLocked = Boolean.TRUE;
  private Boolean credentialsNonExpired = Boolean.TRUE;

  public User() {

  }

  public Boolean isIsEncrypted() {

    return BooleanUtils.toBoolean(isEncrypted);
  }

  public void setIsEncrypted(Boolean isEncrypted) {

    this.isEncrypted = BooleanUtils.toBoolean(isEncrypted);
  }

  public Boolean getApplicationProcessor() {

    return applicationProcessor;
  }

  public void setApplicationProcessor(Boolean applicationProcessor) {

    this.applicationProcessor = applicationProcessor;
  }

  public Boolean getAuthoriser() {

    return authoriser;
  }

  public void setAuthoriser(Boolean authoriser) {

    this.authoriser = authoriser;
  }

  public String getEmail() {

    return email;
  }

  public void setEmail(String email) {

    this.email = email;
  }

  public String getPassword() {

    return password;
  }

  public void setPassword(String password) {

    this.password = password;
  }

  // has to be eager for Wicket to load roles when user is loaded, Hibernate won't do this automatically
  @ElementCollection(targetClass = Role.class, fetch = FetchType.EAGER)
  @Enumerated(EnumType.STRING) // Possibly optional (I'm not sure) but defaults to ORDINAL.
  @CollectionTable(name = "userrole")
  @Column(name = "roles") // Column name in user_role
  public Set<Role> getRoles() {

    return roles;
  }

  public void setRoles(Set<Role> roles) {

    this.roles = roles;
  }

  public String getPrinterName() {

    return printerName;
  }

  public void setPrinterName(String printerName) {

    this.printerName = printerName;
  }

  @Override
  public String toString() {

    return getUsername();
  }

  @Column(unique = true)
  public String getUsername() {

    return username;
  }

  public void setUsername(String username) {

    this.username = username;
  }

  public Boolean getCouncilProcessor() {

    return BooleanUtils.toBoolean(councilProcessor);
  }

  public void setCouncilProcessor(Boolean councilProcessor) {

    this.councilProcessor = councilProcessor;
  }

  @Transient
  public String getOldPassword() {

    return oldPassword;
  }

  public void setOldPassword(String oldPassword) {

    this.oldPassword = oldPassword;
  }

  @Transient
  public String getNewPassword() {

    return newPassword;
  }

  public void setNewPassword(String newPassword) {

    this.newPassword = newPassword;
  }

  @Transient
  public String getFullname() {

    return getSurname() + " " + getFirstName();
  }

  public String getSurname() {

    return CaseUtil.upperCase(surname);
  }

  public void setSurname(String surname) {

    this.surname = surname;
  }

  public String getFirstName() {

    return CaseUtil.upperCase(firstName);
  }

  public void setFirstName(String firstName) {

    this.firstName = firstName;
  }

  @Override
  @Column(nullable = true)
  public boolean isAccountNonExpired() {

    return BooleanUtils.toBooleanDefaultIfNull(accountNonExpired, true);
  }

  public void setAccountNonExpired(Boolean accountNonExpired) {
    this.accountNonExpired = accountNonExpired;
  }

  @Override
  @Column(nullable = true)
  public boolean isAccountNonLocked() {
    return BooleanUtils.toBooleanDefaultIfNull(accountNonLocked, true);
  }

  public void setAccountNonLocked(Boolean accountNonLocked) {
    this.accountNonLocked = accountNonLocked;
  }

  @Override
  @Column(nullable = true)
  public boolean isCredentialsNonExpired() {
    return BooleanUtils.toBooleanDefaultIfNull(credentialsNonExpired, true);
  }

  public void setCredentialsNonExpired(Boolean credentialsNonExpired) {
    this.credentialsNonExpired = credentialsNonExpired;
  }

  @Override
  @Column(nullable = true)
  public boolean isEnabled() {
    return BooleanUtils.toBooleanDefaultIfNull(enabled, true);
  }

  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }


      @Override
      @Transient
      public Collection<? extends GrantedAuthority> getAuthorities() {
          if (roles == null || roles.isEmpty()) {
              return Collections.emptyList();
          }
          List<GrantedAuthority> roleList = new ArrayList<>();
          for (Role r : roles) {
              roleList.add(new SimpleGrantedAuthority(r.getRoleName()));
          }
          return roleList;
      }

  }
