package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.RegisterTypeDAO;
import zw.co.hitrac.council.business.dao.repo.RegisterTypeRepository;
import zw.co.hitrac.council.business.domain.RegisterType;

/**
 *
 * @author kelvin
 */
@Repository
public class RegisterTypeDAOImpl implements RegisterTypeDAO {

    @Autowired
    private RegisterTypeRepository registerTypeRepository;

    public RegisterType save(RegisterType registerType) {
        return registerTypeRepository.save(registerType);
    }

    public List<RegisterType> findAll() {
        return registerTypeRepository.findAll();


    }

    public RegisterType get(Long id) {
        return registerTypeRepository.findOne(id);

    }

    public void setRegisterTypeRepository(RegisterTypeRepository registerTypeRepository) {
        this.registerTypeRepository = registerTypeRepository;
    }

    public List<RegisterType> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}