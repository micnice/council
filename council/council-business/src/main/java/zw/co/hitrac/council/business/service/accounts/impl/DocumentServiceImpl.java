
package zw.co.hitrac.council.business.service.accounts.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.accounts.DocumentDAO;
import zw.co.hitrac.council.business.domain.accounts.Document;
import zw.co.hitrac.council.business.service.accounts.DocumentService;

import java.util.List;

/**
 *
 * @author Tatenda Chiwandire
 */
@Service
@Transactional
public class DocumentServiceImpl implements DocumentService {

    @Autowired
    private DocumentDAO documentDAO;

    @Transactional
    public Document save(Document t) {
        return documentDAO.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Document> findAll() {
        return documentDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Document get(Long id) {
        return documentDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Document> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setDocumentDao(DocumentDAO documentDAO) {

        this.documentDAO = documentDAO;
    }


}
