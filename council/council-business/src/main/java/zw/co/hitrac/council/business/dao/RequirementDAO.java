package zw.co.hitrac.council.business.dao;

import java.util.List;
import zw.co.hitrac.council.business.domain.Requirement;

/**
 *
 * @author Michael Matiashe
 */
public interface RequirementDAO extends IGenericDAO<Requirement> {
    public List<Requirement> findAll(Boolean examRequirement);
}
