
package zw.co.hitrac.council.business.dao.accounts;

import java.util.List;
import zw.co.hitrac.council.business.dao.IGenericDAO;
import zw.co.hitrac.council.business.domain.accounts.Bank;

/**
 *
 * 
 * @author Michael Matiashe
 */
public interface BankDAO extends IGenericDAO<Bank> {
    
}
