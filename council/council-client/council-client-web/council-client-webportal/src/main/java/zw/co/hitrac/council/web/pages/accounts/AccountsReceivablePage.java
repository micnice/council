package zw.co.hitrac.council.web.pages.accounts;

import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author Takunda Dhlakama
 */
public class AccountsReceivablePage extends IAccountsReceivablePage {

    public AccountsReceivablePage() {      
        add(new BookmarkablePageLink<Void>("transactionTypeLink", TransactionTypeListPage.class));       
    }
}
