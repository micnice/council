/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.accounts;

import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author tdhlakama
 */
public abstract class IAccountingPage extends TemplatePage {

    public IAccountingPage() {
        menuPanelManager.getAccountingPanel().setTopMenuCurrent(true);        
    }
}
