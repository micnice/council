/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.models;

import java.util.List;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.Qualification;
import zw.co.hitrac.council.business.service.QualificationService;
import zw.co.hitrac.council.business.utils.HrisComparator;

/**
 *
 * @author tdhlakama
 */
public class QualificationListModel extends LoadableDetachableModel<List<Qualification>> {

    private final QualificationService qualificationService;
    private HrisComparator hrisComparator = new HrisComparator();

    public QualificationListModel(QualificationService qualificationService) {
        this.qualificationService = qualificationService;
    }

    @Override
    protected List<Qualification> load() {
        return (List<Qualification>) hrisComparator.sort(qualificationService.findAll());
    }
}
