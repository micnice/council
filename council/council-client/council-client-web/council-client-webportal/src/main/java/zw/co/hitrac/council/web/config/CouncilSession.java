package zw.co.hitrac.council.web.config;

import org.apache.commons.lang3.Validate;
import org.apache.wicket.Session;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.injection.Injector;
import org.apache.wicket.request.Request;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;

import java.util.Locale;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import zw.co.hitrac.council.business.domain.User;
import zw.co.hitrac.council.business.service.UserService;

/**
 * @author Clive Gurure
 */
public class CouncilSession extends AuthenticatedWebSession {

  private static final Logger logger = LoggerFactory.getLogger(CouncilSession.class);

  @SpringBean(name = "councilAuthenticationManager")
  private transient AuthenticationManager authenticationManager;
  @SpringBean
  private transient UserService userService;
  private transient HttpSession httpSession;

  public CouncilSession(Request request) {

    super(request);

    this.setLocale(Locale.UK);
    injectDependencies();

    this.httpSession = HttpServletRequest.class.cast(request.getContainerRequest()).getSession();
  }

  public static CouncilSession get() {

    logger.debug("Calling method session.get");

    final CouncilSession councilSession = CouncilSession.class.cast(Session.get());

    return councilSession;
  }

  private void injectDependencies() {
    Injector.get().inject(this);
    Objects.requireNonNull(authenticationManager, "Auth manager cannot be null");
  }

  @Override
  public boolean authenticate(String username, String password) {

    logger.debug("Calling method authenticate");
    boolean authenticated;

    try {

      logger.debug("Attempting authentication");
      Authentication authentication =
          authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
      authenticated = authentication.isAuthenticated();

      if (authenticated) {

        SecurityContextHolder.getContext().setAuthentication(authentication);
        httpSession.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY,
            SecurityContextHolder.getContext());

        //Tomcat session identifier
        httpSession.setAttribute("userName", (authentication == null) ? "Unknown" : authentication.getName());
        httpSession.setAttribute("remember-me", false);

      }

      logger.debug("User was authenticated = {}", authenticated);

    } catch (AuthenticationException e) {

      //don't use catch-all otherwise page may not redirect,
      //org.apache.wicket.NonResettingRestartException
      logger.error("User failed to login", e);
      authenticated = false;
    }

    return authenticated;

  }

  public boolean hasAnyRole(Roles roles) {

    logger.debug("Calling method hasAnyRole");
    final User user = getUser();
    if ((user == null) || (user.getRoles() == null)) {
      return false;
    }
    return getRoles().hasAnyRole(roles);
  }

  @Override
  public Roles getRoles() {
    logger.debug("Calling method getRoles");
    Roles roles = new Roles();
    getRolesIfSignedIn(roles);
    return roles;
  }

  private void getRolesIfSignedIn(Roles roles) {
    logger.debug("Calling method getRolesIfSignedIn");
    if (isSignedIn()) {
      Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
      addRolesFromAuthentication(roles, authentication);
    }
  }

  private void addRolesFromAuthentication(Roles roles, Authentication authentication) {

    logger.debug("Calling method addRolesFromAuthentication");
    Objects.requireNonNull(roles, "Roles cannot be null");
    Objects.requireNonNull(authentication, "Authentication cannot be null");
    Objects.requireNonNull(authentication.getPrincipal(), "Principal for auth cannot be null");

    User user = (User) authentication.getPrincipal();

    Objects.requireNonNull(user.getRoles(), "User does not have roles");
    Validate.isTrue(!user.getRoles().isEmpty(), "User does not have roles");

    user.getRoles().stream().forEach(r -> roles.add(r.getRoleName()));
  }

  public User getUser() {

    logger.debug("Calling method getUser");
    if (isSignedIn()) {

      return userService.getCurrentUser().orElseThrow(() ->
          new IllegalStateException("No current user could be found even though a session is active"));

    } else {

      logger.warn("Attempting to get a user obj when not signed in");
      return null;
    }

  }

}
