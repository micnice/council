package zw.co.hitrac.council.web.pages;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.devutils.stateless.StatelessComponent;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.StatelessForm;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zw.co.hitrac.council.business.domain.GeneralParameters;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.User;
import zw.co.hitrac.council.business.domain.WebUser;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.models.VersionReadOnlyModel;
import zw.co.hitrac.council.web.pages.research.CouncilSiteImageResource;
import zw.co.hitrac.council.web.pages.research.MyDynamicImageResource;

import java.util.Optional;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;

/**
 * Web User Login - for registrants in the system
 */
public class WebUserRegistrationPage extends WebUserTemplatePage {

    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private WebUserService webUserService;
    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private EmailMessageService emailMessageService;

    private static final Logger logger = LoggerFactory.getLogger(WebUserRegistrationPage.class);

    public WebUserRegistrationPage() {
        add(new SignInForm("signInForm"));
    }

    /**
     * Sign in form
     */
    public final class SignInForm extends StatelessForm<Void> {

        private transient String email = "";
        private transient String registrationNumber = "";
        private transient String password = "";


        /**
         * Constructor
         *
         * @param id id of the form component
         */
        public SignInForm(final String id) {
            super(id);
            add(new FeedbackPanel("errorMessage"));

            add(new TextField<String>("email", new PropertyModel<>(this, "email")));
            add(new TextField<String>("registrationNumber", new PropertyModel<>(this, "registrationNumber")));
            add(new PasswordTextField("password", new PropertyModel<>(this, "password")));
        }

        @Override
        public final void onSubmit() {
            // Sign the user in
            try {
                Optional<Registrant> userOptional = Optional.ofNullable(registrantService.getRegistrantByRegistrationNumber(registrationNumber));

                if (userOptional.isPresent()) {

                    //emailMessageService.send(email, "PCZ Registration", "Password : NewUsrX");
                    webUserService.createWebUser(email, password, registrationNumber);
                    setResponsePage(WebUserPortalLoginPage.class);
                } else {
                    // Get the error message from the properties file associated with the Component
                    String errmsg = getString("loginError", null, "Unable to sign you in");
                    // Register the error message with the feedback panel
                    error(errmsg);
                }
            } catch (CouncilException ex) {
                error(ex.getMessage());
            }
        }

    }
}
