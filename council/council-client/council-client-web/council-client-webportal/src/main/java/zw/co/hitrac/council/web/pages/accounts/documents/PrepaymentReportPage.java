package zw.co.hitrac.council.web.pages.accounts.documents;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.Prepayment;
import zw.co.hitrac.council.business.domain.reports.RegistrantBalance;
import zw.co.hitrac.council.business.service.accounts.PrepaymentService;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.reports.PrepaymentReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.DatePickerUtil;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 * @author Michael Matiashe
 */
public class PrepaymentReportPage extends TemplatePage {

    @SpringBean
    private PrepaymentService prepaymentService;
    private Date endDate;

    public PrepaymentReportPage() {
        PropertyModel<Date> endDateModel = new PropertyModel<Date>(this, "endDate");
        Form<?> form = new Form("form");
        form.add(new TextField<Date>("endDate", endDateModel).add(DatePickerUtil.getDatePicker()));

        add(form);
        add(new FeedbackPanel("feedback"));

        form.add(new Button("print") {
            @Override
            public void onSubmit() {
                try {
                    if (endDate == null) {
                        endDate = new Date();
                    }
                    List<RegistrantBalance> finalbalances = new ArrayList<RegistrantBalance>();
                    List<Prepayment> prepayments = prepaymentService.findAll(endDate);
                    for (Prepayment pre : prepayments) {
                        RegistrantBalance rb = new RegistrantBalance();
                        rb.setInstitution(pre.getInstitution());
                        rb.setDateOfDeposit(pre.getDateOfDeposit());
                        rb.setDateOfPayment(pre.getDateOfPayment());
                        rb.setInitialBalance(pre.getAmount());
                        rb.setCustomerAccount(pre.getInstitution().getCustomerAccount());
                        rb.setCurrentBalance(prepaymentService.getPrepaymentBalance(endDate, pre));
                        if (!rb.getCurrentBalance().equals(BigDecimal.ZERO)) {
                            finalbalances.add(rb);
                        }
                    }

                    PrepaymentReport prepaymentReport = new PrepaymentReport();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();
                    parameters.put("comment", "Prepayment Remaining as of " + DateUtil.convertDateToString(endDate));

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(prepaymentReport,
                            contentType, (List<RegistrantBalance>) HrisComparator.sortInstitutionBalance(new ArrayList<RegistrantBalance>(finalbalances)), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);
                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }
        });

    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

}
