package zw.co.hitrac.council.web.pages.institution;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import zw.co.hitrac.council.web.pages.configure.CityListPage;
import zw.co.hitrac.council.web.pages.configure.CountryListPage;
import zw.co.hitrac.council.web.pages.configure.DistrictListPage;

import zw.co.hitrac.council.web.pages.configure.ProvinceListPage;

/**
 *
 * @author Michael Matiashe
 */
public class InstitutionMenuPage extends IInstitutionView {

    public InstitutionMenuPage() {
        add(new Link<Void>("institutionEditPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionEditPage(null));
            }
        });
        add(new Link<Void>("institutionView") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionListPage());
            }
        });
        add(new Link<Void>("institutionMapEditPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionMapEditPage());
            }
        });

        add(new Link<Void>("institutionRetireEditPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionMapEditPage(Boolean.TRUE));
            }
        });
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
