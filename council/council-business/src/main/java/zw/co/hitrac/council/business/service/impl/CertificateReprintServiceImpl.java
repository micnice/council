package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.CertificateReprintDAO;
import zw.co.hitrac.council.business.domain.CertificateReprint;
import zw.co.hitrac.council.business.service.CertificateReprintService;

import java.util.List;

/**
 *
 * @author Charles Chigoriwa
 */
@Service
@Transactional
public class CertificateReprintServiceImpl implements CertificateReprintService{
    
    @Autowired
    private CertificateReprintDAO certificateReprintDAO;
    

    @Transactional
    public CertificateReprint save(CertificateReprint certificateReprint) {
       return certificateReprintDAO.save(certificateReprint);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<CertificateReprint> findAll() {
        return certificateReprintDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public CertificateReprint get(Long id) {
       return certificateReprintDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<CertificateReprint> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setCertificateReprintDAO(CertificateReprintDAO certificateReprintDAO) {

        this.certificateReprintDAO = certificateReprintDAO;
    }
    
}
