package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxCheckBox;
import org.apache.wicket.extensions.yui.calendar.DatePicker;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.service.CouncilDurationService;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.DurationService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.CouncilDurationListModel;
import zw.co.hitrac.council.web.models.CourseListModel;
import zw.co.hitrac.council.web.utility.CustomDateTextField;

/**
 *
 * @author Michael Matiashe
 */
public class DurationEditPage extends IAdministerDatabaseBasePage {
    
    @SpringBean
    private DurationService durationService;
    @SpringBean
    private CouncilDurationService councilDurationService;
    @SpringBean
    private CourseService courseService;
    
    public DurationEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<Duration>(new LoadableDetachableDurationModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        Form<Duration> form = new Form<Duration>("form", (IModel<Duration>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                durationService.save(getModelObject());
                setResponsePage(new DurationViewPage(getModelObject().getId()));
            }
        };
        
        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new  CustomDateTextField("startDate").setRequired(true).add(new ErrorBehavior()).add(new DatePicker()));
        form.add(new  CustomDateTextField("endDate").setRequired(true).add(new ErrorBehavior()).add(new DatePicker()));
        form.add(new DropDownChoice("councilDuration", new CouncilDurationListModel(councilDurationService)).setRequired(true).add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink", DurationListPage.class));
        form.add(new CheckBox("active"));
        add(form);
        
        final WebMarkupContainer wmc = new WebMarkupContainer("wmc");
        wmc.setVisible(false);
        wmc.setOutputMarkupPlaceholderTag(true);
        form.add(wmc);
        form.add(new AjaxCheckBox("advanced", new PropertyModel(wmc, "visible")) {
            @Override
            protected void onUpdate(AjaxRequestTarget art) {
                art.add(wmc);
            }
        });
        
        wmc.add(new CheckBoxMultipleChoice("courses", new CourseListModel(courseService)));
    }
    
    private final class LoadableDetachableDurationModel extends LoadableDetachableModel<Duration> {
        
        private Long id;
        
        public LoadableDetachableDurationModel(Long id) {
            this.id = id;
        }
        
        @Override
        protected Duration load() {
            if (id == null) {
                return new Duration();
            }
            return durationService.get(id);
        }
    }
}
