package zw.co.hitrac.council.business.service.accounts.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.accounts.AccountDAO;
import zw.co.hitrac.council.business.dao.repo.accounts.AccountRepository;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.process.BillingProcess;
import zw.co.hitrac.council.business.process.ProductFinder;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.service.accounts.AccountService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.business.utils.TakeOnBalanceDMO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author Tatenda Chiwandire
 */
@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    @Autowired
    BillingProcess billingProcess;
    @Autowired
    private AccountDAO accountDAO;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private RegistrantService registrantService;
    @Autowired
    private RegistrationService registrationService;
    @Autowired
    private ProductFinder productFinder;
    @Autowired
    private RegistrationProcess registrationProcess;
    @Autowired
    private GeneralParametersService generalParametersService;
    @PersistenceContext
    private EntityManager entityManager;

    public void setAccountDao(AccountDAO accountDAO) {

        this.accountDAO = accountDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Account> getGeneralLedgerAccounts() {

        return accountDAO.getGLAccounts();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Account> getAccountsByPersonalAccountType(Boolean personalAccount) {

        return accountRepository.findByPersonalAccount(personalAccount);
    }

    @Transactional
    public void getRegistrantTakeOnBalance(TakeOnBalanceDMO item) {
        /**
         * @param id registrant identifier once we have link to registrant get
         * account from customer if take on balance already updated exit
         */
        BigDecimal balance = BigDecimal.ZERO;
        Registrant registrant = registrantService.get(item.getId());
        Account account = registrant.getCustomerAccount().getAccount();

        if (generalParametersService.get().getRegistrationByApplication()) {
            if (item.getCreditBalance().doubleValue() > 0 && item.getDebitBalance().doubleValue() > 0) {
                throw new CouncilException("Both credit and debit balances cannot have values");
            }
            /**
             * reaching this stage means everything went well
             */
            //Registration reg = registrationService.getCurrentRegistration(registrant, course);
            Registration reg = registrationProcess.activeRegisterNotStudentRegister(registrant);
            if (reg == null) {
                throw new CouncilException("Practitioner has not been assigned a discipline as yet.");
            }
            if (item.getYear() > 0) {
                Product product = productFinder.searchAnnualProduct(reg, null);
                if (item.getYear() != 2014) {
                    balance = product.getProductPrice().getPrice().multiply(new BigDecimal(getNumYears(item.getYear(), reg)));
                    balance = balance.add(productFinder.searchReRegistrationProduct(reg).getProductPrice().getPrice());
                }
                balance = balance.add(item.getDebitBalance()).subtract(item.getCreditBalance());
                account.setBalance(balance);
                account.setDateModified(new Date());
                this.save(account);
            }
        } else {
            account.setBalance(item.creditBalance);
            account.setDateModified(new Date());
            this.save(account);
        }
    }

    @Transactional
    public Account save(Account t) {

        return accountDAO.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Account> findAll() {

        return accountDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Account get(Long id) {

        return accountDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Account> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Double getNumYears(Integer lastPaid, Registration reg) {
        /**
         * @param <b>last<b/> paid should be in Integer<br/> between 2005 and
         * 2014 <br/>safe to hard code for now
         */
        Double _lastPaid = new Double(lastPaid.toString());
        List<Integer> range = new ArrayList<Integer>();
        Integer start = 1980;
        while (start < 2014) {
            range.add(start);
            start++;
        }
        /**
         * check if date is within range EHT == 6 months EHO & others 9months
         */
        Boolean found = false;
        for (int i = 0; i < range.size(); i++) {
            if (lastPaid.equals(range.get(i))) {
                found = true;
                break;
            }
        }
        if (!found) {
            throw new CouncilException(lastPaid + " Is not a valid date or is not within the expected range of 2005-2013");
        }
        if (lastPaid <= 2011) {
            _lastPaid += 1D;
            if (reg.getCourse().getName().equalsIgnoreCase("ENVIRONMENTAL HEALTH TECHNICIAN")) {
                _lastPaid -= 0.5;
            } else {
                _lastPaid -= 0.75;
            }
        }
        return 2014 - _lastPaid;

    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Account> getPersonalAccountsErrors() {

        return accountDAO.getPersonalAccountsErrors();
    }

    @Override
    public int update(Account account) {
        return entityManager.createQuery("update Account a set a.balance=:balance where a=:account").setParameter("account", account).setParameter("balance", account.getBalance()).executeUpdate();
    }

    
}
