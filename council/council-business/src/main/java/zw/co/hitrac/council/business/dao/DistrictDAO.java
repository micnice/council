package zw.co.hitrac.council.business.dao;

import java.util.List;
import zw.co.hitrac.council.business.domain.District;
import zw.co.hitrac.council.business.domain.Province;

/**
 *
 * @author Constance Mabaso
 */
public interface DistrictDAO extends IGenericDAO<District> {
    public List<District> getDistrictsInProvince(Province province);
}
