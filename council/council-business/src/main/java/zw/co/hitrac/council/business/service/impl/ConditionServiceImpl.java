package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.ConditionDAO;
import zw.co.hitrac.council.business.domain.Conditions;
import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.business.service.ConditionService;

import java.util.List;

/**
 * @author Michael Matiashe
 */
@Service
@Transactional
public class ConditionServiceImpl implements ConditionService {

    @Autowired
    private ConditionDAO conditionDAO;

    @Transactional
    public Conditions save(Conditions condition) {
        return conditionDAO.save(condition);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Conditions> findAll() {
        return conditionDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Conditions get(Long id) {
        return conditionDAO.get(id);
    }

    public void setConditionDAO(ConditionDAO conditionDAO) {
        this.conditionDAO = conditionDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Conditions> findAll(Boolean examCondition) {
        return conditionDAO.findAll(examCondition);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Conditions> getRegisterConditions() {
        return conditionDAO.getRegisterConditions();
    }


}
