package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.RegistrantCPDItemDAO;
import zw.co.hitrac.council.business.dao.repo.RegistrantCPDItemRepository;
import zw.co.hitrac.council.business.domain.RegistrantCPDCategory;
import zw.co.hitrac.council.business.domain.RegistrantCPDItem;

/**
 *
 * @author Constance Mabaso
 */
@Repository
public class RegistrantCPDItemDAOImpl implements RegistrantCPDItemDAO {

    @Autowired
    private RegistrantCPDItemRepository registrantCPDItemRepository;
    
    @PersistenceContext
    private EntityManager entityManager;

    public RegistrantCPDItem save(RegistrantCPDItem registrantCPDItem) {
        return registrantCPDItemRepository.save(registrantCPDItem);
    }

    public List<RegistrantCPDItem> findAll() {
        return registrantCPDItemRepository.findAll();
    }

    public RegistrantCPDItem get(Long id) {
        return registrantCPDItemRepository.findOne(id);
    }

  
public List<RegistrantCPDItem> getRegistrantCPDItems (RegistrantCPDCategory registrantCPDCategory){
    return entityManager.createQuery("select r from RegistrantCPDItem r where r.registrantCPDCategory=:registrantCPDCategory").setParameter("registrantCPDCategory", registrantCPDCategory).getResultList();
}

    void setRegistrantCPDItemRepository(RegistrantCPDItemRepository registrantCPDItemRepository) {
         this.registrantCPDItemRepository = registrantCPDItemRepository;
    }

    public List<RegistrantCPDItem> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
