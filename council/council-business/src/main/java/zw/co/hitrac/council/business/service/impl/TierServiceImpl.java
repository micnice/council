package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.TierDAO;
import zw.co.hitrac.council.business.domain.Tier;
import zw.co.hitrac.council.business.service.TierService;

import java.util.List;

/**
 *
 * @author Edward Zengeni
 */
@Service
@Transactional
public class TierServiceImpl implements TierService{
    
    @Autowired
    private TierDAO tierDAO;
    

    @Transactional
    public Tier save(Tier tier) {
       return tierDAO.save(tier);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Tier> findAll() {
        return tierDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Tier get(Long id) {
       return tierDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Tier> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setTierDAO(TierDAO tierDAO) {

        this.tierDAO = tierDAO;
    }
    
}
