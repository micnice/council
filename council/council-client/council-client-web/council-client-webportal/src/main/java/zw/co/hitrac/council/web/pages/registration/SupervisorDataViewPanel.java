
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.registration;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PageableListView;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigator;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.web.pages.institution.InstitutionContactAddressPage;

/**
 *
 * @author tdhlakama
 */
public class SupervisorDataViewPanel extends Panel {
    
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private RegistrantService registrantService;
    

    public SupervisorDataViewPanel(String id, IModel<List<RegistrantData>> model,final Institution institution) {
        super(id);

        PageableListView<RegistrantData> eachItem = new PageableListView<RegistrantData>("eachItem", model, 20) {
            @Override
            protected void populateItem(final ListItem<RegistrantData> item) {
                Link<RegistrantData> viewLink = new Link("institutionContactPage", item.getModel()) {
                    @Override
                    public void onClick() {
                      Institution updatedInstitution =institutionService.get(institution.getId());
                              updatedInstitution.setSupervisor(registrantService.get(item.getModelObject().getId()));
                              institutionService.save(updatedInstitution);
                              setResponsePage(new InstitutionContactAddressPage(updatedInstitution.getId()));
                    }
                };

                PageParameters params = new PageParameters();
                params.add("registrantId", item.getModelObject().getId());
                item.add(viewLink);

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                
                viewLink.add(new Label("lastname", item.getModelObject().getLastname()));
                item.add(new Label("firstname", item.getModelObject().getFirstname()));
                item.add(new Label("middlename", item.getModelObject().getMiddlename()));
                item.add(new Label("idNumber", item.getModelObject().getIdNumber()));
                item.add(new Label("registrationNumber", item.getModelObject().getRegistrationNumber()));
                item.add(new Label("gender", item.getModelObject().getGender()));

            }
        };

        add(new PagingNavigator("navigator", eachItem));
        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
