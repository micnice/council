/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.models;

import java.util.List;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.GeneralParameters;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.utils.HrisComparator;

/**
 *
 * @author tdhlakama
 */
public class ExternalInstitutionModel extends LoadableDetachableModel<List<Institution>> {

    private final InstitutionService institutionService;
    private final GeneralParametersService generalParametersService;
    private HrisComparator hrisComparator = new HrisComparator();

    public ExternalInstitutionModel(InstitutionService institutionService, GeneralParametersService generalParametersService) {
        this.institutionService = institutionService;
        this.generalParametersService = generalParametersService;

    }

    @Override
    protected List<Institution> load() {
        if (generalParametersService.get().getInstitutionType() == null) {
            return (List<Institution>) hrisComparator.sort(institutionService.findAll());
        } else {
            return (List<Institution>) hrisComparator.sort(institutionService.getInstitutions(generalParametersService.get().getInstitutionType()));
        }
    }
}
