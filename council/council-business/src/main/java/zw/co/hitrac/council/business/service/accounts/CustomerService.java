/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.accounts;

import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.service.IGenericService;

/**
 *
 * @author tdhlakama
 */
public interface CustomerService extends IGenericService<Customer> {

    public Customer get(Account account);

    public Registrant getRegistrant(Account account);

    public Institution getInstitution(Account account);
}
