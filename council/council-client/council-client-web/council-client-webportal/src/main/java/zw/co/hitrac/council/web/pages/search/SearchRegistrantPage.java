package zw.co.hitrac.council.web.pages.search;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;

//~--- JDK imports ------------------------------------------------------------
import java.util.ArrayList;
import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import zw.co.hitrac.council.web.pages.registration.RegistrantDataViewPanel;
import zw.co.hitrac.council.web.pages.registration.RegistrantEditPage;

/**
 * @author Takunda Dhlakama
 */
public class SearchRegistrantPage extends TemplatePage {

    private String searchtxt;
    @SpringBean
    private RegistrantService registrantService;

    public SearchRegistrantPage() {
        menuPanelManager.getSearchPanel().setTopMenuCurrent(true);
        menuPanelManager.getSearchPanel().setSearchSimple(true);

        PropertyModel<String> messageModel = new PropertyModel<String>(this, "searchtxt");
        Form<?> form = new Form("form");

        form.add(new TextField<String>("searchtxt", messageModel).setRequired(true).add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("registrantEditPage", RegistrantEditPage.class));
        add(form);

        IModel<List<RegistrantData>> model = new LoadableDetachableModel<List<RegistrantData>>() {
            @Override
            protected List<RegistrantData> load() {
                if ((searchtxt == null) || searchtxt.equals("")) {
                    return new ArrayList<RegistrantData>();
                }
                if (searchtxt.length() > 2) {
                    return registrantService.getRegistrantList(searchtxt, null, null, null);
                }
                return new ArrayList<RegistrantData>();
            }
        };


        add(new Label("msgEmptyList", new LoadableDetachableModel<String>() {
            @Override
            protected String load() {
                if ((searchtxt == null) || searchtxt.equals("")) {
                    return "";
                } else {
                    final List<RegistrantData> list = model.getObject();
                    return "Search Results for : " + searchtxt + " - [" + list.size() + "]";
                }
            }
        }));

        // Registrant List Data View Panel
        add(new RegistrantDataViewPanel("registrantDataListPanel", model));
        add(new FeedbackPanel("feedback"));
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
