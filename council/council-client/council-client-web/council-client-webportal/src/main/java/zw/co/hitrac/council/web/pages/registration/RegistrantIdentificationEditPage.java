/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.registration;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.IdentificationType;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantIdentification;
import zw.co.hitrac.council.business.service.IdentificationTypeService;
import zw.co.hitrac.council.business.service.RegistrantIdentificationService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.CustomDateTextField;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

/**
 * @author kelvin
 */
public class RegistrantIdentificationEditPage extends TemplatePage {

    @SpringBean
    private RegistrantIdentificationService registrantIdentificationService;
    @SpringBean
    private IdentificationTypeService identificationTypeService;

    public RegistrantIdentificationEditPage(Long id, final IModel<Registrant> registrantModel) {

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        CompoundPropertyModel<RegistrantIdentification> model = new CompoundPropertyModel<RegistrantIdentification>(
                new RegistrantIdentificationEditPage.LoadableDetachableRegistrantIdentificationServiceModel(
                        id, registrantModel));
        final Long registrantId = model.getObject().getRegistrant().getId();

        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantId);

        setDefaultModel(model);

        Form<RegistrantIdentification> form = new Form<RegistrantIdentification>("form", (IModel<RegistrantIdentification>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    registrantIdentificationService.save(getModelObject());
                    setResponsePage(new RegistrantViewPage(getModelObject().getRegistrant().getId()));
                } catch (CouncilException ex) {
                    error(ex.getMessage());

                }

            }
        };
        form.add(new DropDownChoice("identificationType", identificationTypeService.findAll()).setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("idNumber").setRequired(true).add(new ErrorBehavior()));
        form.add(new CustomDateTextField("expiry").add(DatePickerUtil.getDatePicker()));


        add(form);
        add(new Label("registrant.fullname"));

        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(registrantId));
            }
        });

    }

    private final class LoadableDetachableRegistrantIdentificationServiceModel extends LoadableDetachableModel<RegistrantIdentification> {

        private Long id;
        private IModel<Registrant> registrantModel;

        public LoadableDetachableRegistrantIdentificationServiceModel(Long id, IModel<Registrant> registrantModel) {
            this.registrantModel = registrantModel;
            this.id = id;
        }

        @Override
        protected RegistrantIdentification load() {
            RegistrantIdentification registrantIdentification = null;

            if (id == null) {
                registrantIdentification = new RegistrantIdentification();
                Registrant registrant = registrantModel.getObject();
                registrantIdentification.setRegistrant(registrant);
            } else {
                registrantIdentification = registrantIdentificationService.get(id);
            }

            return registrantIdentification;
        }
    }
}
