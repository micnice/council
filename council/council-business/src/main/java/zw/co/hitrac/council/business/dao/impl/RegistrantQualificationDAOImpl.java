package zw.co.hitrac.council.business.dao.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.RegistrantQualificationDAO;
import zw.co.hitrac.council.business.dao.repo.RegistrantQualificationRepository;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.ProductIssuanceType;
import zw.co.hitrac.council.business.domain.Qualification;
import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.business.domain.RegisterType;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantQualification;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;

/**
 *
 * @author Michael Matiashe
 */
@Repository
public class RegistrantQualificationDAOImpl implements RegistrantQualificationDAO {

    @Autowired
    private RegistrantQualificationRepository registrantQualificationRepository;
    @PersistenceContext
    private EntityManager entityManager;

    public RegistrantQualification save(RegistrantQualification registrantQualification) {
        return registrantQualificationRepository.save(registrantQualification);
    }

    public List<RegistrantQualification> findAll() {
        return registrantQualificationRepository.findAll();
    }

    public RegistrantQualification get(Long id) {
        return registrantQualificationRepository.findOne(id);
    }

    /**
     * A setter method that will make mocking repo object easier
     *
     * @param registrantQualificationRepository
     */
    public void setRegistrantQualificationRepository(RegistrantQualificationRepository registrantQualificationRepository) {
        this.registrantQualificationRepository = registrantQualificationRepository;
    }

    public RegistrantQualification get(Registrant registrant, String name) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantQualification.class);
        criteria.add(Restrictions.eq("registrant", registrant));
        criteria.createAlias("qualification", "q");
        criteria.add(Restrictions.eq("q.name", name));
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        List<RegistrantQualification> list = criteria.list();
        if (list.isEmpty()) {
            return null;
        } else {
            return (RegistrantQualification) list.get(0);
        }
    }

    public RegistrantQualification get(Registrant registrant, Qualification qualification, Institution institution) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantQualification.class);
        criteria.add(Restrictions.eq("registrant", registrant));
        criteria.add(Restrictions.eq("qualification", qualification));
        criteria.add(Restrictions.eq("awardingInstitution", institution));
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        return (RegistrantQualification) criteria.uniqueResult();
    }

    public Date getDateAwarded(Registrant registrant, Course course) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantQualification.class);
        criteria.add(Restrictions.eq("registrant", registrant));
        criteria.add(Restrictions.eq("course", course));
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        List<RegistrantQualification> list = criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        if (list.isEmpty()) {
            return null;
        } else {
            //note it will return the first course
            for (RegistrantQualification rq : list) {
                if (rq.getDateAwarded() != null) {
                    return rq.getDateAwarded();
                }
            }
            return null;
        }
    }

    public Date getLastPBQDateAwarded(Registrant registrant) {
        List<RegistrantQualification> list = entityManager.createQuery("select rq from RegistrantQualification rq where rq.registrant=:registrant AND rq.retired=FALSE AND rq.course.basic=FALSE ORDER BY rq.dateAwarded DESC").setParameter("registrant", registrant).getResultList();
        if (list.isEmpty()) {
            return null;
        } else {
            //note it will return the first date list
            return list.get(0).getDateAwarded();
        }

    }

    public RegistrantQualification get(Registrant registrant, Qualification qualification) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantQualification.class);
        criteria.add(Restrictions.eq("registrant", registrant));
        criteria.add(Restrictions.eq("qualification", qualification));
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        return (RegistrantQualification) criteria.uniqueResult();
    }

    public List<RegistrantQualification> getRegistrantQualifications(Registrant registrant) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantQualification.class);
        criteria.add(Restrictions.eq("registrant", registrant));
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        criteria.createAlias("qualification", "q");
        criteria.addOrder(Order.asc("q.name"));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public RegistrantQualification getRegistrantCurrentQualifications(Registrant registrant) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantQualification.class);
        criteria.add(Restrictions.eq("registrant", registrant));
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        criteria.createAlias("qualification", "q");
        criteria.addOrder(Order.asc("q.name"));
        List<RegistrantQualification> list = criteria.list();
        if (list.isEmpty()) {
            return null;
        } else {
            int listSize = list.size();

            Collections.sort(list, new Comparator<RegistrantQualification>() {
                public int compare(RegistrantQualification o1, RegistrantQualification o2) {
                    if (o1.getDateAwarded() == null || o2.getDateAwarded() == null) {
                        return 0;
                    }
                    return o1.getDateAwarded().compareTo(o2.getDateAwarded());
                }
            });

            return (RegistrantQualification) list.get(list.size() - 1);
        }
    }

    public List<RegistrantData> getRegistrantQualificationIds(Qualification qualification) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantQualification.class);
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        criteria.add(Restrictions.eq("qualification", qualification));
        criteria.createAlias("registrant", "r");
        criteria.createAlias("qualification", "q");
        ProjectionList proList = Projections.projectionList();
        proList.add(Projections.property("r.id"), "id");
        proList.add(Projections.property("r.firstname"), "firstname");
        proList.add(Projections.property("r.lastname"), "lastname");
        proList.add(Projections.property("r.middlename"), "middlename");
        proList.add(Projections.property("r.registrationNumber"), "registrationNumber");
        proList.add(Projections.property("r.idNumber"), "idNumber");
        proList.add(Projections.property("q.name"), "qualificationName");
        criteria.setProjection(proList);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        criteria.setResultTransformer(Transformers.aliasToBean(RegistrantData.class));
        return criteria.list();
    }

    public List<RegistrantData> getRegistrantQualifications() {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantQualification.class);
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        criteria.createAlias("registrant", "r");
        criteria.createAlias("qualification", "q");
        ProjectionList proList = Projections.projectionList();
        proList.add(Projections.property("r.id"), "id");
        proList.add(Projections.property("r.firstname"), "firstname");
        proList.add(Projections.property("r.lastname"), "lastname");
        proList.add(Projections.property("r.middlename"), "middlename");
        proList.add(Projections.property("r.registrationNumber"), "registrationNumber");
        proList.add(Projections.property("r.idNumber"), "idNumber");
        proList.add(Projections.property("q.name"), "qualificationName");
        criteria.setProjection(proList);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        criteria.setResultTransformer(Transformers.aliasToBean(RegistrantData.class));
        return criteria.list();
    }

    public List<RegistrantQualification> getRegistrantQualifications(RegisterType registerType) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantQualification.class);
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        Criteria r = criteria.createCriteria("qualification");
        r.add(Restrictions.eq("registerType", registerType));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public Integer getRegistrantQualifications(Qualification qualification) {
        Long l = (Long) entityManager.createQuery("SELECT COUNT(r) from RegistrantQualification r where r.qualification=:qualification and r.retired=:retired and r.registrant.dead=:dead").setParameter("qualification", qualification).setParameter("retired", Boolean.FALSE).setParameter("dead", Boolean.FALSE).getSingleResult();
        return l.intValue();
    }

    public List<RegistrantQualification> getRegistrantQualifications(Qualification qualification, Institution institution, Date startDate, Date endDate) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantQualification.class);
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        if (qualification != null) {
            criteria.add(Restrictions.eq("qualification", qualification));
        }
        if (institution != null) {
            criteria.add(Restrictions.eq("awardingInstitution", institution));
        }
        if (startDate != null && endDate != null) {
            criteria.add(Restrictions.between("dateAwarded", startDate, endDate));
        } else if (startDate != null && endDate == null) {
            criteria.add(Restrictions.between("dateAwarded", startDate, new Date()));
        }
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<RegistrantData> getRegistrantQualificationList(Qualification qualification, Institution institution, Date startDate, Date endDate) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantQualification.class);
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        if (qualification != null) {
            criteria.add(Restrictions.eq("qualification", qualification));
        }
        if (institution != null) {
            criteria.add(Restrictions.eq("awardingInstitution", institution));
        }
        if (startDate != null && endDate != null) {
            criteria.add(Restrictions.between("dateAwarded", startDate, endDate));
        } else if (startDate != null && endDate == null) {
            criteria.add(Restrictions.between("dateAwarded", startDate, new Date()));
        }
        criteria.createAlias("registrant", "r");
        criteria.add(Restrictions.eq("r.dead", Boolean.FALSE));
        criteria.createAlias("qualification", "q");
        ProjectionList proList = Projections.projectionList();
        proList.add(Projections.property("r.id"), "id");
        proList.add(Projections.property("r.firstname"), "firstname");
        proList.add(Projections.property("r.lastname"), "lastname");
        proList.add(Projections.property("r.middlename"), "middlename");
        proList.add(Projections.property("r.registrationNumber"), "registrationNumber");
        proList.add(Projections.property("r.idNumber"), "idNumber");
        proList.add(Projections.property("q.name"), "qualificationName");
        criteria.setProjection(proList);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        criteria.setResultTransformer(Transformers.aliasToBean(RegistrantData.class));
        return criteria.list();
    }

    public List<Registrant> getRegistrants(RegisterType registerType) {
        Set<Registrant> registrants = new HashSet<Registrant>();
        for (RegistrantQualification r : getRegistrantQualifications(registerType)) {
            //Return List of the undead           
            registrants.add(r.getRegistrant());

        }
        return new ArrayList<Registrant>(registrants);
    }

    public List<Registrant> getRegistrants(Qualification qualification, Institution institution, Date startDate, Date endDate) {
        Set<Registrant> registrants = new HashSet<Registrant>();
        for (RegistrantQualification r : getRegistrantQualifications(qualification, institution, startDate, endDate)) {
            // to do Return List of the undead           
            registrants.add(r.getRegistrant());
        }
        return new ArrayList<Registrant>(registrants);
    }

    public List<RegistrantQualification> getRegistrantQualificationCertificateType(Registrant registrant, ProductIssuanceType productIssuanceType) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantQualification.class);
        criteria.add(Restrictions.eq("retired", Boolean.FALSE));
        criteria.add(Restrictions.eq("registrant", registrant));
        Criteria r = criteria.createCriteria("qualification");
        r.add(Restrictions.eq("productIssuanceType", productIssuanceType));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<RegistrantQualification> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Qualification> getDistinctQualificationsInRegistrantQualification() {
        return entityManager.createQuery("select DISTINCT rq.qualification from RegistrantQualification rq where rq.retired=false and rq.qualification.retired=false ORDER BY rq.qualification ASC").getResultList();
    }

    public List<Institution> getDistinctInstitutionsInRegistrantQualification() {
        return entityManager.createQuery("select DISTINCT rq.institution from RegistrantQualification rq where rq.retired=false ORDER BY rq.institution.name ASC").getResultList();
    }

    //checking in registrant_qualification = course and register in registration
    public Integer getTotalPerQualificationMappedCourseAndRegister(Course course, Register register) {
        Long l = (Long) entityManager.createQuery("select DISTINCT count(reg.registrant.id) from Registration reg where reg.register.id=:register and reg.deRegistrationDate is null and reg.registrant.id in(select rq.registrant.id from RegistrantQualification rq where rq.course.id=:course and rq.retired=:retired)").setParameter("register", register.getId()).setParameter("retired", Boolean.FALSE).setParameter("course", course.getId()).getSingleResult();
        return l.intValue();
    }

    public RegistrantQualification getRegistrantQualificationByRegistrantAndCourse(Registrant registrant, Course course) {
        List<RegistrantQualification> registrantQualifications = entityManager.createQuery("SELECT rq FROM RegistrantQualification rq WHERE rq.registrant=:registrant and rq.course=:course").setParameter("registrant", registrant).setParameter("course", course).getResultList();
        if (!registrantQualifications.isEmpty()) {
            return registrantQualifications.get(0);
        } else {
            return null;
        }
    }

    /*
     Registrant Qualifications that appear on the practice certificate
     @param registrant
     */
    public List<RegistrantQualification> getRegistrantPracticeQualificationList(Registrant registrant) {
        return entityManager.createQuery("select rq from RegistrantQualification rq where rq.registrant=:registrant AND rq.retired=FALSE AND rq.qualification.productIssuanceType=:productIssuanceType ORDER BY rq.dateAwarded ASC").setParameter("registrant", registrant).setParameter("productIssuanceType", ProductIssuanceType.PRACTICING_CERTIFICATE).getResultList();
    }

    /*
     Registrant Additional Qualifications that appear on the practice certificate
     @param registrant
     */
    public List<RegistrantQualification> getRegistrantAdditionalPracticeQualificationList(Registrant registrant) {
        return entityManager.createQuery("select rq from RegistrantQualification rq where rq.registrant=:registrant AND rq.retired=FALSE AND rq.qualification.productIssuanceType=:productIssuanceType ORDER BY rq.dateAwarded ASC").setParameter("registrant", registrant).setParameter("productIssuanceType", ProductIssuanceType.ADDITIONAL_QUALIFICATION_CERTIFICATE).getResultList();
    }

    @Override
    public List<RegistrantData> getRegistrantDataListWithBoth(Course course, Course course2, CouncilDuration councilDuration, String status) {
        String queryString = "";
        String ids = "";
        List<Long> durationIDs = new ArrayList<Long>();
        if (councilDuration != null) {
            for (Duration d : councilDuration.getDurations()) {
                durationIDs.add(d.getId());
            }
            for (Long durationID : durationIDs) {
                if (ids == "") {
                    ids = ids + durationID;
                } else {
                    ids = ids + "," + durationID;
                }
            }
        }
        if (councilDuration == null) {
            queryString = "SELECT * FROM registrant WHERE EXISTS ( SELECT 1 FROM registrantqualification WHERE registrantqualification.course_id =" + course.getId() + " AND registrant.id=registrantqualification.registrant_id) AND EXISTS ( SELECT 1 FROM registrantqualification WHERE registrantqualification.course_id =" + course2.getId() + " AND registrant.id=registrantqualification.registrant_id)";
        } else if (status.equals("Active")) {
            queryString = "SELECT * from registrant WHERE EXISTS (SELECT 1 FROM registrantactivity WHERE registrantactivity.registrantActivityType='RENEWAL' AND registrant.id=registrantactivity.registrant_id AND registrantactivity.duration_id IN (" + ids + ")) AND (EXISTS (SELECT 1 FROM registrantqualification WHERE registrantqualification.course_id =" + course.getId() + " AND registrant.id=registrantqualification.registrant_id) AND EXISTS ( SELECT 1 FROM registrantqualification WHERE registrantqualification.course_id =" + course2.getId() + " AND registrant.id=registrantqualification.registrant_id))";
        } else {
            queryString = "SELECT * from registrant JOIN registration ON registrant.id=registration.registrant_id WHERE registration.deregistrationDate IS NULL AND EXISTS (SELECT 1 FROM registrantactivity WHERE registrantactivity.registrantActivityType='RENEWAL' AND registrantactivity.duration_id IN (" + ids + ") AND registrant.id!=registrantactivity.registrant_id) AND EXISTS (SELECT 1 FROM registrantqualification WHERE registrantqualification.course_id=" + course.getId() + " AND registrant.id=registrantqualification.registrant_id) AND EXISTS (SELECT 1 FROM registrantqualification WHERE registrantqualification.course_id =" + course2.getId() + " AND registrant.id=registrantqualification.registrant_id)";
        }

        return entityManager.createNativeQuery(queryString, RegistrantData.class).getResultList();

    }

    public List<RegistrantData> getRegistrantDataListWithBoth(Qualification qualification, Qualification qualification2) {
        String queryString = "SELECT * FROM registrant r WHERE r.id IN ( SELECT q.registrant_id FROM registrantqualification q WHERE q.qualification_id =" + qualification.getId() + ") AND r.id IN ( SELECT p.registrant_id FROM registrantqualification p WHERE p.qualification_id =" + qualification2.getId() + ")";
        System.out.println("ACTIVE - " + queryString.toString());
        return entityManager.createNativeQuery(queryString, RegistrantData.class).getResultList();
    }

    public List<Course> getRegisteredDisciplines(Registrant registrant) {
        return entityManager.createQuery("SELECT DISTINCT (r.course) from RegistrantQualification r where r.retired= FALSE and r.registrant=:registrant").setParameter("registrant", registrant).getResultList();
    }
}
