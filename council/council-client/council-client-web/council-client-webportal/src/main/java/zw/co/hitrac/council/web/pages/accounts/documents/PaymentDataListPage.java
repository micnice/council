package zw.co.hitrac.council.web.pages.accounts.documents;

import zw.co.hitrac.council.business.domain.accounts.PaymentData;
import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.Prepayment;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.domain.accounts.ReceiptItem;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.accounts.AccountService;
import zw.co.hitrac.council.business.service.accounts.PaymentDataService;
import zw.co.hitrac.council.business.service.accounts.PrepaymentService;
import zw.co.hitrac.council.business.service.accounts.ProductService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.CustomDateLabel;

/**
 *
 * @author Michael Matiashe
 */
public class PaymentDataListPage extends TemplatePage {

    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private AccountService accountService;
    @SpringBean
    private ProductService productService;
    @SpringBean
    private PaymentDataService paymentDataService;

    public PaymentDataListPage(final List<PaymentData> datas) {

        IModel<List<PaymentData>> model = new LoadableDetachableModel<List<PaymentData>>() {

            @Override
            protected List<PaymentData> load() {
                return datas;
            }
        };

        PropertyListView<PaymentData> eachItem = new PropertyListView<PaymentData>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<PaymentData> item) {
                Link<PaymentData> viewLink = new Link<PaymentData>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                    }
                };
                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                viewLink.add(new Label("receiptNumber"));
                item.add(new CustomDateLabel("dateOfPayment"));
                item.add(new Label("regNumber"));
                item.add(new Label("name"));
                item.add(new Label("description"));
                item.add(new Label("code"));
                item.add(new Label("amount"));
                item.add(new Label("totalAmount"));
            }
        };

        add(eachItem);
        add(new Link("post") {

            @Override
            public void onClick() {
                for (PaymentData paymentData : datas) {
                    Registrant registrant = registrantService.getRegistrantByRegistrationNumber(paymentData.getReceiptNumber());
                    if (registrant != null) {
                        Account customerAccount = registrant.getCustomerAccount().getAccount();
                        customerAccount.credit(paymentData.getAmount());
                        accountService.save(customerAccount);
                        paymentData.setPostedToSalesAccount(Boolean.TRUE);
                        paymentDataService.save(paymentData);

                        Product paidProduct = productService.getProductByCode(paymentData.getCode());
                        if (paidProduct != null) {
                            Account salesAccount = paidProduct.getSalesAccount();
                            salesAccount.debit(paymentData.getAmount());
                            accountService.save(salesAccount);
                            paymentData.setPostedToPersonalAccountt(Boolean.TRUE);
                            paymentDataService.save(paymentData);
                        }

                    }
                }
            }
        });

    }

    private void generateReceiptItems(List<PaymentData> datas) {
        for (PaymentData paymentData : datas) {
            ReceiptItem receiptItem = new ReceiptItem();
            receiptItem.setAmount(paymentData.getAmount());
            receiptItem.setDateCreated(null);

        }
    }
}
