package zw.co.hitrac.council.web.pages.configure;

import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CourseGroup;
import zw.co.hitrac.council.business.service.CourseGroupService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

/**
 *
 * @author Michael Matiashe
 */
public class CourseGroupListPage extends IAdministerDatabaseBasePage{
    
    @SpringBean
    private CourseGroupService courseGroupService;

    public CourseGroupListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink",AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink",AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {

            @Override
            public void onClick() {
               setResponsePage(new CourseGroupEditPage(null));
            }
        });
        
        IModel<List<CourseGroup>> model=new LoadableDetachableModel<List<CourseGroup>>() {

            @Override
            protected List<CourseGroup> load() {
               return courseGroupService.findAll();
            }
        };
        
        PropertyListView<CourseGroup> eachItem=new PropertyListView<CourseGroup>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<CourseGroup> item) {
                Link<CourseGroup> viewLink=new Link<CourseGroup>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                       setResponsePage(new CourseGroupViewPage(getModelObject().getId()));
                    }
                };
                if(item.getIndex()%2==0){
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };
        
        add(eachItem);
    }
    
    
    
}
