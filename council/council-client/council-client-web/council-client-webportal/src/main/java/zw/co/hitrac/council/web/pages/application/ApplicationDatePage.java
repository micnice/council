package zw.co.hitrac.council.web.pages.application;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.process.ApplicationProcess;
import zw.co.hitrac.council.business.service.ApplicationService;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.CustomDateTextField;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

//~--- JDK imports ------------------------------------------------------------

import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.UserService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.web.utility.CustomDateLabel;

/**
 *
 * @author Michael Matiashe
 * @author Kelvin Goredema
 */
public class ApplicationDatePage extends TemplatePage {

    @SpringBean
    private CourseService courseService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private ApplicationProcess applicationProcess;
    @SpringBean
    private ApplicationService applicationService;
    @SpringBean
    private RegistrationService registrationService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private UserService userService;
    @SpringBean
    private RegistrationProcess registrationProcess;
    @SpringBean
    private DebtComponentService debtComponentService;
    Boolean show;

    public ApplicationDatePage(long id) {
        menuPanelManager.getApplicationPanel().setTopMenuCurrent(true);

        if (!generalParametersService.get().getRegistrationByApplication()) {
            show = Boolean.FALSE;
        } else {
            show = Boolean.TRUE;
        }
        final CompoundPropertyModel<Application> model =
                new CompoundPropertyModel<Application>(applicationService.get(id));

        setDefaultModel(model);
        Form<Application> form = new Form<Application>("form", (IModel<Application>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    Application application = applicationService.save(getModelObject());
                    setResponsePage(new CgsApplicationViewPage(getModelObject().getId()));
                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };
        
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new CgsApplicationViewPage(model.getObject().getId()));
            }
        });
        
        form.add(new Label("currentStatus", model.getObject().getApplicationTextStatus()));
        form.add(new CustomDateLabel("datePosted").add(DatePickerUtil.getDatePicker()));
        form.add(new Label("datePostedLabel", "Date Posted"));
        add(form);
        add(new Label("registrant.fullname"));
        add(new FeedbackPanel("feedback"));
    }
}

//~ Formatted by Jindent --- http://www.jindent.com
