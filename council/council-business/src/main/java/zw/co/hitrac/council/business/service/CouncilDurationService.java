package zw.co.hitrac.council.business.service;

import java.util.List;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Duration;

/**
 *
 * @author Michael Matiashe
 */
public interface CouncilDurationService extends IGenericService<CouncilDuration> {
            
    public CouncilDuration getCouncilDurationFromDuration(Duration duration);
    
    public List<CouncilDuration> getActiveCouncilDurations();

}
