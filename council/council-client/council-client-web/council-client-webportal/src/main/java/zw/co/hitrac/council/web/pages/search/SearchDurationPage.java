/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.search;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.domain.RegistrantActivityType;
import zw.co.hitrac.council.business.service.DurationService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.reports.QualificationRegisterReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 * @author tdhlakama
 */
public class SearchDurationPage extends TemplatePage {

    @SpringBean
    private DurationService durationService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    private RegistrantActivityType registrantActivityType;
    private Duration duration;
    @SpringBean
    private DataSource dataSource;

    public SearchDurationPage() {
        menuPanelManager.getSearchPanel().setTopMenuCurrent(true);
        menuPanelManager.getSearchPanel().setSearchAdvanced(true);

        PropertyModel<RegistrantActivityType> registrantActivityTypeModel = new PropertyModel<RegistrantActivityType>(this, "registrantActivityType");
        PropertyModel<Duration>durationModel = new PropertyModel<Duration>(this, "duration");
        Form<?> form = new Form("form");

        List<RegistrantActivityType> registrantActivityTypes = new ArrayList<RegistrantActivityType>(Arrays.asList(RegistrantActivityType.values()));
        if (!generalParametersService.get().getRegistrationByApplication()) {
            registrantActivityTypes.remove(RegistrantActivityType.CHANGE_OF_NAME);
            registrantActivityTypes.remove(RegistrantActivityType.DEREGISTRATION);
            registrantActivityTypes.remove(RegistrantActivityType.EXAM_REGISTRATION);
            registrantActivityTypes.remove(RegistrantActivityType.PENALTY);
        }
        form.add(new DropDownChoice("registrantActivityType", registrantActivityTypeModel, registrantActivityTypes).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("duration", durationModel, durationService.findAll()).setRequired(true).add(new ErrorBehavior()));
        add(form);

        form.add(new Button("qualificationRegister") {
            @Override
            public void onSubmit() {
                Map parameters = new HashMap();
                QualificationRegisterReport qualificationRegisterReport = new QualificationRegisterReport();
                try {

                    parameters.put("comment", registrantActivityType + " Statistics for Period " + duration.getName());
                    parameters.put("activityType", registrantActivityType);
                    ///problem - search different durations
                    parameters.put("duration", duration.getName());
                    List<Integer> registers = new ArrayList<Integer>();
                    registers.add(generalParametersService.get().getMainRegister().getId().intValue());
                    registers.add(generalParametersService.get().getProvisionalRegister().getId().intValue());
                    parameters.put("registerID", registers);
                    ContentType contentType = ContentType.PDF;
                    final Connection connection = dataSource.getConnection(); //DBConnect.getConnection();
                    ByteArrayResource resource = null;
                    try {
                        resource = ReportResourceUtils.getReportResource(qualificationRegisterReport, contentType, connection, parameters);
                    } finally {
                        connection.close();
                    }
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                } catch (SQLException ex) {
                    ex.printStackTrace(System.out);
                }

            }
        });
        add(new FeedbackPanel("feedback"));
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
