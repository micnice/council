package zw.co.hitrac.council.business.dao;

import zw.co.hitrac.council.business.domain.IdentificationType;

/**
 *
 * @author Charles Chigoriwa
 */
public interface IdentificationTypeDAO extends IGenericDAO<IdentificationType> {
    
}
