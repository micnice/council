package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------
import java.util.HashMap;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Qualification;
import zw.co.hitrac.council.business.service.QualificationService;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import zw.co.hitrac.council.reports.QualificationReportList;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 * @author Michael Matiashe
 */
public class QualificationListPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private QualificationService qualificationService;

    public QualificationListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new QualificationEditPage(null));
            }
        });

        IModel<List<Qualification>> model = new LoadableDetachableModel<List<Qualification>>() {
            @Override
            protected List<Qualification> load() {
                return qualificationService.findAll();
            }
        };
        PropertyListView<Qualification> eachItem = new PropertyListView<Qualification>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<Qualification> item) {
                Link<Qualification> viewLink = new Link<Qualification>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new QualificationViewPage(getModelObject().getId()));
                    }
                };
                item.add(viewLink);
                viewLink.add(new Label("name"));
                item.add(new Label("course"));
                item.add(new Label("registerType"));
                item.add(new Label("retired"));
            }
        };

        add(eachItem);


        add(new Link("report") {
            @Override
            public void onClick() {
                Map parameters = new HashMap();
                parameters.put("comment", "Active Qualifications");
                QualificationReportList qualificationReportList = new QualificationReportList();
                try {
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(qualificationReportList, contentType, qualificationService.findAll(Boolean.FALSE), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);
                    resource.respond(a);
                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

        add(new Link("retired") {
            @Override
            public void onClick() {
                Map parameters = new HashMap();
                parameters.put("comment", "Retired Qualifications");
                QualificationReportList qualificationReportList = new QualificationReportList();
                try {
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(qualificationReportList, contentType, qualificationService.findAll(Boolean.TRUE), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);
                    resource.respond(a);
                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
