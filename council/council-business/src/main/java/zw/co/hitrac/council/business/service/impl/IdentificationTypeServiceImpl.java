package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.IdentificationTypeDAO;
import zw.co.hitrac.council.business.domain.IdentificationType;
import zw.co.hitrac.council.business.service.IdentificationTypeService;

import java.util.List;

/**
 *
 * @author Charles Chigoriwa
 */
@Service
@Transactional
public class IdentificationTypeServiceImpl implements IdentificationTypeService {

    @Autowired
    private IdentificationTypeDAO identificationTypeDAO;

    @Transactional
    public IdentificationType save(IdentificationType identificationType) {
        return identificationTypeDAO.save(identificationType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<IdentificationType> findAll() {
        return identificationTypeDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public IdentificationType get(Long id) {
        return identificationTypeDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<IdentificationType> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * A DAO setter method to facilitate mocking
     * @param identificationTypeDAO
     */
    public void setIdentificationTypeDAO(IdentificationTypeDAO identificationTypeDAO) {
        this.identificationTypeDAO = identificationTypeDAO;
    }

   
    
    
}
