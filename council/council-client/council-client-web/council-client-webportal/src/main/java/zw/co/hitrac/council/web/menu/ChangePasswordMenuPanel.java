package zw.co.hitrac.council.web.menu;

import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.pages.accounts.payments.ItemUserPaymentPage;
import zw.co.hitrac.council.web.pages.security.ChangePasswordPage;
import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;

/**
 *
 * @author Charles Chigoriwa
 */
public class ChangePasswordMenuPanel extends MenuPanel {

    public ChangePasswordMenuPanel(String id) {
        super(id);
        add(new Link<Void>("changePasswordLink") {
            @Override
            public void onClick() {
                setResponsePage(new ChangePasswordPage(CouncilSession.get().getUser().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("itemUserPaymentPage", ItemUserPaymentPage.class) {
            @Override
            protected void onConfigure() {
                setVisibilityAllowed(CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.REGISTRY_CLERK))));
            }
        });
    }
}
