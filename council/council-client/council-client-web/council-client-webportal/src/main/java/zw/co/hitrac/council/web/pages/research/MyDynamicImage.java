package zw.co.hitrac.council.web.pages.research;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.model.IModel;
import org.apache.wicket.request.resource.IResource;

/**
 *
 * @author charlesc
 */
public class MyDynamicImage extends Image {
    public MyDynamicImage(String id, IResource imageResource) {
        super(id, imageResource);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
