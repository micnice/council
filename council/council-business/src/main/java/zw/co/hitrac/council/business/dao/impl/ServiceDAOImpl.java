package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.ServiceDAO;
import zw.co.hitrac.council.business.dao.repo.ServiceRepository;
import zw.co.hitrac.council.business.domain.Service;

/**
 *
 * @author tidza
 */
@Repository
public class ServiceDAOImpl implements ServiceDAO {

    @Autowired
    private ServiceRepository serviceRepository;

    public Service save(Service t) {
        return serviceRepository.save(t);
    }

    public List<Service> findAll() {
        return serviceRepository.findAll();
    }

    public Service get(Long id) {
        return serviceRepository.findOne(id);
    }

    public void setServiceRepository(ServiceRepository serviceRepository) {
        this.serviceRepository = serviceRepository;
    }

    public List<Service> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
   
}
