package zw.co.hitrac.council.reports;

//~--- non-JDK imports --------------------------------------------------------

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.reports.utils.IReportManager;
import zw.co.hitrac.council.reports.utils.ReportManager;

import java.io.Serializable;

//~--- JDK imports ------------------------------------------------------------

/**
 * @author Charles Chigoriwa
 */
public abstract class Report implements Serializable {

    public final static String JRXML = "jrxml";
    public final static String JASPER = "jasper";
    protected final static String PDF_EXTENSION = "pdf";
    protected final static String EXCEL_EXTENSION = "xlsx";
    protected final static String DOC_EXTENSION = "doc";

    public static void main(String[] args) {

        System.out.println(DefaultImplReport.class.getPackage().getName());
    }

    public final String getOutputReportName(ContentType contentType) {

        if (contentType.equals(ContentType.PDF)) {
            return concat(getOutputReportName(), PDF_EXTENSION);
        } else if (contentType.equals(ContentType.DOC)) {
            return concat(getOutputReportName(), DOC_EXTENSION);
        } else if (contentType.equals(ContentType.EXCEL)) {
            return concat(getOutputReportName(), EXCEL_EXTENSION);
        } else {
            return getOutputReportName();
        }
    }

    protected abstract String getOutputReportName();

    private static String concat(String prefix, String suffix) {

        return prefix + "." + suffix;
    }

    public final String getJRXMLReportPath() {

        return concat(getReportPath(), JRXML);
    }

    protected abstract String getReportPath();

    public final JasperReport getSubReport() {

        return getSubReport("header.jrxml");
    }

    protected JasperReport getSubReport(String subReportName) {

        if (!subReportName.endsWith(Report.JRXML)) {
            throw new IllegalArgumentException("Uknown extension of report path. Must specify JRXML extension");
        }

        IReportManager reportManager = new ReportManager();

        String reportPath = "/" + this.getClass().getPackage().getName().replace(".", "/") + "/" + subReportName;
        JasperReport report = null;

        try {
            report = reportManager.getJasperReport(reportPath);
        } catch (JRException e) {
            e.printStackTrace();
        }

        return report;
    }

    public final String getJasperReportPath() {

        return concat(getReportPath(), JASPER);
    }
}

//~ Formatted by Jindent --- http://www.jindent.com
