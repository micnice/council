package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.PageReference;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Citizenship;
import zw.co.hitrac.council.business.domain.IdentificationType;
import zw.co.hitrac.council.business.domain.IdentificationValidationRule;
import zw.co.hitrac.council.business.service.CitizenshipService;
import zw.co.hitrac.council.business.service.IdentificationTypeService;
import zw.co.hitrac.council.business.service.IdentificationValidationRuleService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

import java.util.List;

/**
 * Created by clive on 6/16/15.
 */
public class IdentificationValidationRuleEditPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private IdentificationValidationRuleService identificationValidationRuleService;
    @SpringBean
    private CitizenshipService citizenshipService;
    @SpringBean
    private IdentificationTypeService identificationTypeService;

    public IdentificationValidationRuleEditPage(Long id, final PageReference pageReference) {

        setDefaultModel(
                new CompoundPropertyModel<IdentificationValidationRule>(new LoadableDetachableValidationRuleModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));

        Form<IdentificationValidationRule> form = new Form<IdentificationValidationRule>("form",
                (IModel<IdentificationValidationRule>) getDefaultModel()) {

            @Override
            public void onSubmit() {

                identificationValidationRuleService.save(getModelObject());
                setResponsePage(pageReference.getPage());
            }
        };

        form.add(new Link("returnLink") {

            public void onClick() {

                setResponsePage(pageReference.getPage());
            }
        });

        form.add(new DropDownChoice("citizenship", new CitizenshipModel()).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("identificationType", new IdentificationTypeModel()).setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("example").setRequired(true).add(new ErrorBehavior()));
        form.add(new CheckBox("active"));
        add(form);
    }

    private final class LoadableDetachableValidationRuleModel extends LoadableDetachableModel<IdentificationValidationRule> {

        private Long id;

        public LoadableDetachableValidationRuleModel(Long id) {

            this.id = id;
        }

        @Override
        protected IdentificationValidationRule load() {

            if(id == null){
                return new IdentificationValidationRule();
            }

            return identificationValidationRuleService.get(id);
        }
    }

    private final class CitizenshipModel extends LoadableDetachableModel<List<Citizenship>> {

        @Override
        protected List<Citizenship> load() {

            return citizenshipService.findAll();
        }
    }

    private final class IdentificationTypeModel extends LoadableDetachableModel<List<IdentificationType>> {

        @Override
        protected List<IdentificationType> load() {

            return identificationTypeService.findAll();
        }
    }
}
