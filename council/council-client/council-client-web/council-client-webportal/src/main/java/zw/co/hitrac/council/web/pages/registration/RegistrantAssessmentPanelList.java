/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.registration;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantAssessment;
import zw.co.hitrac.council.business.service.RegistrantAssessmentService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.utility.CustomDateLabel;
import zw.co.hitrac.council.web.utility.CustomDateTimeLabel;

/**
 *
 * @author kelvin
 */
public class RegistrantAssessmentPanelList extends Panel {

    @SpringBean
    private RegistrantAssessmentService registrantAssessmentService;

    public RegistrantAssessmentPanelList(String id, final IModel<Registrant> registrantModel) {
        super(id);

        add(new Link<Registrant>("registrantAssessmentLink") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantAssessmentEditPage(registrantModel));
            }
        });

        add(new PropertyListView<RegistrantAssessment>("registrantAssessmentList",registrantAssessmentService.getAssessments(registrantModel.getObject())) {
            @Override
            protected void populateItem(ListItem<RegistrantAssessment> item) {
                Link<RegistrantAssessment> viewLink = new Link<RegistrantAssessment>("registrantAssessmentLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new RegistrantAssessmentEditPage(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(viewLink);
                item.add(new Label("assessmentType"));                
                item.add(new Label("valueObtained"));
                item.add(new Label("assessor"));
                item.add(new CustomDateLabel("date"));
                item.add(new Label("createdBy"));
                item.add(new Label("modifiedBy"));
                item.add(CustomDateTimeLabel.forDate("dateCreated",
                        new PropertyModel<>(item.getModel(), "dateCreated")));
                item.add(CustomDateTimeLabel.forDate("dateModified",
                        new PropertyModel<>(item.getModel(), "dateModified")));



            }
        });

    }
}
