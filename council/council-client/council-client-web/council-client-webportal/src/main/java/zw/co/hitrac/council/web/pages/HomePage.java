package zw.co.hitrac.council.web.pages;

import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.GeneralParameters;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.models.VersionReadOnlyModel;
import zw.co.hitrac.council.web.pages.accounts.payments.FinancePage;
import zw.co.hitrac.council.web.pages.configure.ConfigureSystemPage;
import zw.co.hitrac.council.web.pages.examinations.ExaminationsPage;
import zw.co.hitrac.council.web.pages.application.ApplicationListPage;
import zw.co.hitrac.council.web.pages.institution.InstitutionMenuPage;
import zw.co.hitrac.council.web.pages.registration.RegistrationViewPage;
import zw.co.hitrac.council.web.pages.reports.ViewReportsPage;
import zw.co.hitrac.council.web.pages.search.SearchPage;
import zw.co.hitrac.council.web.pages.research.CouncilSiteImageResource;
import zw.co.hitrac.council.web.pages.research.MyDynamicImageResource;
import zw.co.hitrac.council.web.pages.security.ChangePasswordPage;
import zw.co.hitrac.council.web.pages.security.SignOutPage;
import static zw.co.hitrac.council.web.utility.GeneralUtils.*;

/**
 *
 * @author charlesc
 */
public class HomePage extends WebPage {

    @SpringBean
    private GeneralParametersService generalParametersService;

    public HomePage() {
        GeneralParameters generalParameters = generalParametersService.get();
        String name = generalParameters.getRegistrantName();
        String councilName = generalParameters.getCouncilName();

        if ((name == null) || name.trim().equals("")) {
            name = "Registrant";
        }

        councilName = ((councilName == null) || councilName.trim().equals(""))
                ? "Council"
                : councilName;
        add(new Label("displayCouncilName", councilName));
        add(new Label("councilName", councilName));

        // links on Home Page
        Link<Void> registrationViewLink = new BookmarkablePageLink<Void>("registrationViewLink",
                RegistrationViewPage.class);

        add(registrationViewLink);
        registrationViewLink.add(new Label("registrantName", name));
        add(new BookmarkablePageLink<Void>("viewReportsLink", ViewReportsPage.class) {
            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.ACCOUNTS_OFFICER, Role.EDUCATION_OFFICER, Role.IT_OFFICER)));
                return enabled;
            }
        });
        add(new BookmarkablePageLink<Void>("financePage", FinancePage.class) {
            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.ACCOUNTS_OFFICER, Role.IT_OFFICER)));
                return enabled;
            }
        });
        
         add(new Link<Void>("changePasswordLink") {
            @Override
            public void onClick() {
                setResponsePage(new ChangePasswordPage(CouncilSession.get().getUser().getId()));
            }
        });
         
        add(new BookmarkablePageLink<Void>("configureSystemLink", ConfigureSystemPage.class) {
            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.IT_OFFICER)));
                return enabled;
            }
        });
        add(new BookmarkablePageLink<Void>("searchLink", SearchPage.class));
        add(new BookmarkablePageLink<Void>("examinationsLink", ExaminationsPage.class) {
            @Override
            public boolean isEnabled() {
                Boolean enabled = CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.EDUCATION_OFFICER, Role.IT_OFFICER)));
                return enabled;
            }
        });
        add(new BookmarkablePageLink<Void>("applicationLink", ApplicationListPage.class));
        add(new BookmarkablePageLink<Void>("institutionLink", InstitutionMenuPage.class));
        add(new BookmarkablePageLink<Void>("syslogout", SignOutPage.class));
        add(new Image("siteLogoImage", new MyDynamicImageResource(generalParametersService)));
        add(new Image("councilPicture", new CouncilSiteImageResource(generalParametersService)));
        
        add(new Label("version",new VersionReadOnlyModel()));
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
