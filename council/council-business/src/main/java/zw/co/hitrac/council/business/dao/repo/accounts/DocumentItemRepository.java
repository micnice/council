
package zw.co.hitrac.council.business.dao.repo.accounts;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.accounts.DocumentItem;

/**
 *
 * @author Michael Matiashe
 */
public interface DocumentItemRepository extends CrudRepository<DocumentItem, Long> {

    public List<DocumentItem> findAll();
}
