package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.RegistrantCPDCategory;
import zw.co.hitrac.council.business.service.RegistrantCPDCategoryService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;

/**
 *
 * @author Constance Mabaso
 */
public class RegistrantCPDCategoryListPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private RegistrantCPDCategoryService registrantCPDCategoryService;

    public RegistrantCPDCategoryListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantCPDCategoryEditPage(null));
            }
        });

        IModel<List<RegistrantCPDCategory>> model = new LoadableDetachableModel<List<RegistrantCPDCategory>>() {
            @Override
            protected List<RegistrantCPDCategory> load() {
                return registrantCPDCategoryService.findAll();
            }
        };
        PropertyListView<RegistrantCPDCategory> eachItem = new PropertyListView<RegistrantCPDCategory>("eachItem",
                                                               model) {
            @Override
            protected void populateItem(ListItem<RegistrantCPDCategory> item) {
                Link<RegistrantCPDCategory> viewLink = new Link<RegistrantCPDCategory>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new RegistrantCPDCategoryViewPage(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };

        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
