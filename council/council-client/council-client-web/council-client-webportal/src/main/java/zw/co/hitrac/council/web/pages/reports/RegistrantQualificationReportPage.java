
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.reports;

//~--- non-JDK imports --------------------------------------------------------
import java.util.Arrays;
import java.util.Date;
import net.sf.jasperreports.engine.JRException;

import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

//~--- JDK imports ------------------------------------------------------------

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.wicket.markup.html.form.TextField;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Qualification;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.QualificationService;
import zw.co.hitrac.council.business.service.RegistrantQualificationService;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.reports.RegistrantDataReport;
import zw.co.hitrac.council.reports.RegistrantsReport;
import zw.co.hitrac.council.web.models.AcademicInstitutionstModel;
import zw.co.hitrac.council.web.models.QualificationListModel;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

/**
 *
 * @author tdhlakama
 */
public class RegistrantQualificationReportPage extends IReportPage {

    @SpringBean
    private QualificationService QualificationService;
    @SpringBean
    private RegistrantQualificationService registrantQualificationService;
    @SpringBean
    private InstitutionService institutionService;
    private Qualification qualification;
    private Date startDate, endDate;
    private Institution institution;
    private ContentType contentType;

    public RegistrantQualificationReportPage() {
        PropertyModel<Qualification> qualificationModel = new PropertyModel<Qualification>(this, "qualification");
        PropertyModel<Date> startDateModel = new PropertyModel<Date>(this, "startDate");
        PropertyModel<Date> endDateModel = new PropertyModel<Date>(this, "endDate");
        PropertyModel<Institution> institutionModel = new PropertyModel<Institution>(this, "institution");
        PropertyModel<ContentType> contentTypeModel = new PropertyModel<ContentType>(this, "contentType");
        Form<?> form = new Form("form") {
            @Override
            protected void onSubmit() {
                try {
                    RegistrantDataReport registrantDataReport = new RegistrantDataReport();
                    ContentType contentType = null;
                    Map parameters = new HashMap();

                    String query = "List of Individuals who have obtained the " + qualification.getName() + " qualification ";

                    if (institution != null) {
                        query.concat(" at institution" + institution.getName());
                    }
                    if (startDate != null && endDate != null) {
                        query.concat(" between " + DateUtil.getDate(startDate) + " to " + DateUtil.getDate(endDate));
                    } else if (startDate != null && endDate == null) {
                        query.concat(" between " + DateUtil.getDate(startDate) + " to " + DateUtil.getDate(new Date()));
                    }

                    ByteArrayResource resource;
                    parameters.put("comment", query);
                    try {
                        resource = ReportResourceUtils.getReportResource(registrantDataReport, contentType, registrantQualificationService.getRegistrantQualificationList(qualification, institution, startDate, endDate),
                                parameters);

                        IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                RequestCycle.get().getResponse(), null);

                        resource.respond(a);
                    } catch (JRException ex) {
                        Logger.getLogger(RegistrantsReport.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };
        form.add(new TextField<Date>("startDate", startDateModel).add(DatePickerUtil.getDatePicker()));
        form.add(new TextField<Date>("endDate", endDateModel).add(DatePickerUtil.getDatePicker()));
        form.add(new DropDownChoice("qualification", qualificationModel, new QualificationListModel(QualificationService)).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("institution", institutionModel, new AcademicInstitutionstModel(institutionService)));
        form.add(new DropDownChoice("contentType", contentTypeModel, Arrays.asList(ContentType.values())));
        add(form);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
