/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao;

import java.util.List;

/**
 *
 * @author kelvin
 */
public interface GenericDAO {

    public List<Long> getAllLean(String ClassName);

    public Object getLean(String ClassName, Long id);
}
