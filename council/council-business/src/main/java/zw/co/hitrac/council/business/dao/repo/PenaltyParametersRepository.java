
package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.PenaltyParameters;

/**
 *
 * @author Michael Matiashe
 */
public interface PenaltyParametersRepository   extends CrudRepository<PenaltyParameters, Long>{
    
    public List<PenaltyParameters> findAll();
}
