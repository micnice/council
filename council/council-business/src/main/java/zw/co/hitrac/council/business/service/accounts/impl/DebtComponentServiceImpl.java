package zw.co.hitrac.council.business.service.accounts.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.accounts.DebtComponentDAO;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.service.accounts.CustomerService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.business.service.accounts.ProductService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.TypeOfService;

/**
 *
 * @author Michael Matiashe
 */
@Service
@Transactional
public class DebtComponentServiceImpl implements DebtComponentService {

    @Autowired
    private DebtComponentDAO debtComponentDAO;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private ProductService productService;

    @Transactional
    public DebtComponent save(DebtComponent t) {
        return debtComponentDAO.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<DebtComponent> findAll() {
        return debtComponentDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public DebtComponent get(Long id) {
        return debtComponentDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<DebtComponent> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setDebtComponentDao(DebtComponentDAO debtComponentDAO) {
        this.debtComponentDAO = debtComponentDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<DebtComponent> getDebtComponents(Account account) {
        return this.debtComponentDAO.getDebtComponents(account);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<DebtComponent> getDebtComponents(Customer customerAccount) {
        return this.debtComponentDAO.getDebtComponents(customerAccount);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Boolean hasAccountDebtComponents(Customer customerAccount, TypeOfService typeOfService){
        return debtComponentDAO.hasAccountDebtComponents(customerAccount, typeOfService);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<DebtComponent> getDebtComponents(Product product, Date startDate, Date endDate) {

        return debtComponentDAO.getDebtComponents(product, startDate, endDate);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<DebtComponent> getViewRemovedDeptComponets(Customer customerAccount) {
        return debtComponentDAO.getViewRemovedDeptComponets(customerAccount);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<DebtComponent> getViewDebtComponentsRemoved(Account account) {
        return debtComponentDAO.getViewDebtComponentsRemoved(account);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<DebtComponent> getPenaltyDeptComponets() {
        return debtComponentDAO.getPenaltyDeptComponets();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public BigDecimal getCustomerBalanceDue(Customer customerAccount) {
        return debtComponentDAO.getCustomerBalanceDue(customerAccount);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<DebtComponent> getUnPaidDeptComponets() {
        return debtComponentDAO.getUnPaidDeptComponets();
    }

    @Override
    public List<DebtComponent> getRemovedDeptComponents() {
        return debtComponentDAO.getRemovedDeptComponents();
    }

}
