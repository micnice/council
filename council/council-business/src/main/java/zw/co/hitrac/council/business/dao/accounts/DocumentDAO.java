package zw.co.hitrac.council.business.dao.accounts;

import java.util.List;
import zw.co.hitrac.council.business.dao.IGenericDAO;
import zw.co.hitrac.council.business.domain.accounts.Document;
import zw.co.hitrac.council.business.domain.accounts.DocumentType;

/**
 *
 * @author Michael Matiashe
 */
public interface DocumentDAO extends IGenericDAO<Document> {
    public List<Document> getDocuments(DocumentType documentType);
}
