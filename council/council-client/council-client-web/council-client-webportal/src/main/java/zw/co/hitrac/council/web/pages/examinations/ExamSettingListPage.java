package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.service.examinations.ExamSettingService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.configure.AdministerDatabasePage;
import zw.co.hitrac.council.web.pages.configure.IAdministerDatabaseBasePage;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;
import zw.co.hitrac.council.web.utility.CustomDateLabel;

/**
 *
 * @author Constance Mabaso
 */
public class ExamSettingListPage extends IExaminationsPage {

    @SpringBean
    private ExamSettingService examSettingService;

    public ExamSettingListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new ExamSettingEditPage(null));
            }
        });

        IModel<List<ExamSetting>> model = new LoadableDetachableModel<List<ExamSetting>>() {
            @Override
            protected List<ExamSetting> load() {
                return examSettingService.findAll();
            }
        };
        PropertyListView<ExamSetting> eachItem = new PropertyListView<ExamSetting>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<ExamSetting> item) {
                Link<ExamSetting> viewLink = new Link<ExamSetting>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new ExamSettingViewPage(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(viewLink);
                item.add(new CustomDateLabel("startDate"));
                viewLink.add(new Label("examPeriod"));
                item.add(new CustomDateLabel("endDate"));
                item.add(new Label("examYear"));
                item.add(new Label("activePeriod"));

            }
        };

        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
