package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.MaritalStatus;
import zw.co.hitrac.council.business.service.MaritalStatusService;

/**
 *
 * @author Edward Zengeni
 */
public class MaritalStatusViewPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private MaritalStatusService maritalStatusService;

    public MaritalStatusViewPage(Long id) {
        CompoundPropertyModel<MaritalStatus> model = new CompoundPropertyModel<MaritalStatus>(
                                                         new MaritalStatusViewPage.LoadableDetachableMaritalStatusModel(
                                                             id));

        setDefaultModel(model);
        add(new Link<MaritalStatus>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new MaritalStatusEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", MaritalStatusListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("maritalStatus", model.bind("name")));
    }

    private final class LoadableDetachableMaritalStatusModel extends LoadableDetachableModel<MaritalStatus> {
        private Long id;

        public LoadableDetachableMaritalStatusModel(Long id) {
            this.id = id;
        }

        @Override
        protected MaritalStatus load() {
            return maritalStatusService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
