package zw.co.hitrac.council.webservices.api;

import zw.co.hitrac.council.business.domain.AddressType;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.Serializable;
import java.util.List;
import zw.co.hitrac.council.webservices.api.domain.LeanAddressType;

/**
 *
 * @author Daniel Nkhoma
 */
@Path("/addressTypes")
@Produces(MediaType.APPLICATION_XML)
@Consumes(MediaType.APPLICATION_XML)
public interface AddressTypeWebService extends Serializable{
    
    @GET
    @Path("/list")
    public List<AddressType> getAddressTypes();
    
    @GET
    @Path("/lean")
    public List<LeanAddressType> getAllLean();
    
}
