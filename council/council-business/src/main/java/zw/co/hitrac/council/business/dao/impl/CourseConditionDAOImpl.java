/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.CourseConditionDAO;
import zw.co.hitrac.council.business.dao.repo.CourseConditionRepository;
import zw.co.hitrac.council.business.domain.Conditions;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.CourseCondition;
import zw.co.hitrac.council.business.domain.Register;

/**
 *
 * @author tdhlakama
 */
@Repository
public class CourseConditionDAOImpl implements CourseConditionDAO {

    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    private CourseConditionRepository courseConditionRepository;

    public CourseCondition save(CourseCondition courseCondition) {
        return courseConditionRepository.save(courseCondition);
    }

    public List<CourseCondition> findAll() {
        return courseConditionRepository.findAll();
    }

    public CourseCondition get(Long id) {
        return courseConditionRepository.findOne(id);
    }

    /**
     * A setter method that will make mocking repo object easier
     *
     * @param courseConditionRepository
     */
    public void setCourseConditionRepository(CourseConditionRepository courseConditionRepository) {
        this.courseConditionRepository = courseConditionRepository;
    }

    public List<Conditions> getConditions(Course course, Register register) {
        return entityManager.createQuery("Select c.condition from CourseCondition c where c.course=:course and c.register=:register and c.retired=false")
                .setParameter("course", course).setParameter("register", register).getResultList();
    }

    @Override
    public List<CourseCondition> findAll(Boolean retired) {
        return entityManager.createQuery("Select c from CourseCondition c where c.retired=:retired")
                .setParameter("retired", retired).getResultList();
    }

}
