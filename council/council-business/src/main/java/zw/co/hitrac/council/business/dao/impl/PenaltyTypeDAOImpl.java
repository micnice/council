/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.PenaltyTypeDAO;
import zw.co.hitrac.council.business.dao.repo.PenaltyTypeRepository;

import zw.co.hitrac.council.business.domain.PenaltyType;

/**
 *
 * @author kelvin
 */
@Repository
public class PenaltyTypeDAOImpl implements PenaltyTypeDAO{
     @Autowired  
     private PenaltyTypeRepository penaltyTypeRepository;
    
   public PenaltyType save(PenaltyType penaltyType){
       return penaltyTypeRepository.save(penaltyType);
   }
   
   public List<PenaltyType> findAll(){
       return penaltyTypeRepository.findAll();
   }
   
   public PenaltyType get(Long id){
       return penaltyTypeRepository.findOne(id);
   }
 /**
     * A setter method that will make mocking repo object easier
     * @param penaltyTypeRepository 
     */
   
   public void setPenaltyTypeRepository(PenaltyTypeRepository penaltyTypeRepository) {
         this.penaltyTypeRepository = penaltyTypeRepository;
    }

    public List<PenaltyType> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
