package zw.co.hitrac.council.business.dao.accounts;

import zw.co.hitrac.council.business.dao.IGenericDAO;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.User;
import zw.co.hitrac.council.business.domain.accounts.OldReceipt;
import zw.co.hitrac.council.business.domain.accounts.PaymentMethod;
import zw.co.hitrac.council.business.domain.accounts.PaymentType;
import zw.co.hitrac.council.business.domain.accounts.Product;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Michael Matiashe
 */
public interface OldReceiptDAO extends IGenericDAO<OldReceipt> {

    public List<OldReceipt> getOldRenewalReceipts();

    public List<OldReceipt> getReceipts(Long receiptNumber, Product product, PaymentMethod paymentMethod, PaymentType paymentType, Date startDate, Date endDate, User user, Boolean cancel, Boolean bank, String sourceReference);
    
    public List<OldReceipt> getReceiptErrors(Long receiptNumber, Product product, PaymentMethod paymentMethod, PaymentType paymentType, Date startDate, Date endDate, User user, Boolean cancel, Boolean bank, String sourceReference);
    
    public List<OldReceipt> getReceipts(Product product, Date startDate, Date endDate, Boolean cancel,String sourceReference);

    public List<OldReceipt> getReceipts(Product product, Registrant registrant);

    public Double getReceiptTotal(Long receiptNumber, Product product, PaymentMethod paymentMethod, PaymentType paymentType, Date startDate, Date endDate, User user, Boolean cancel, Boolean bank);

    public Double getReceiptTotal(Product product, Date startDate, Date endDate, Boolean cancel);
    
    public List<String> getAccountSourceReferences();
}
