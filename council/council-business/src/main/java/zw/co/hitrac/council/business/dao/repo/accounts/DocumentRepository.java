
package zw.co.hitrac.council.business.dao.repo.accounts;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.accounts.Document;

/**
 *
 * @author Michael Matiashe
 */
public interface DocumentRepository extends CrudRepository<Document, Long> {

    public List<Document> findAll();
}
