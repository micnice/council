package zw.co.hitrac.council.web.pages.accounts.documents;

import java.util.List;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.PrepaymentDetails;
import zw.co.hitrac.council.business.service.accounts.PrepaymentDetailsService;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.CustomDateLabel;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.pages.registration.RegistrantPanel;
import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;

/**
 *
 * @author Michael Matiashe
 */
public class PrepaymentDetailListPage extends TemplatePage {

    @SpringBean
    private PrepaymentDetailsService prepaymentDetailsService;

    public PrepaymentDetailListPage(final Registrant registrant) {
        
        add(new RegistrantPanel("registrantPanel", registrant.getId()));
        IModel<List<PrepaymentDetails>> model = new LoadableDetachableModel<List<PrepaymentDetails>>() {

            @Override
            protected List<PrepaymentDetails> load() {
                return prepaymentDetailsService.findByCustomerAndCancelled(registrant.getCustomerAccount(), Boolean.FALSE);
            }
        };

        PropertyListView<PrepaymentDetails> eachItem = new PropertyListView<PrepaymentDetails>("eachItem", model) {
            @Override
            protected void populateItem(final ListItem<PrepaymentDetails> item) {
                Link<PrepaymentDetails> viewLink = new Link<PrepaymentDetails>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        if (CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.FINANCIAL_ADMIN_EXECUTIVE, Role.ACCOUNTS_OFFICER)))) {
                            setResponsePage(new PrepaymentDetailsEditPage(getModelObject().getId()));
                        } else {
                            setResponsePage(getPage());
                        }

                    }
                };
                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                viewLink.add(new CustomDateLabel("paymentDetails.dateOfPayment"));
                item.add(new Label("prepayment.institution"));
                item.add(new CustomDateLabel("prepayment"));
                item.add(new Label("paymentDetails.receiptHeader.totalAmountPaid"));
                item.add(new Link("correct") {

                    @Override
                    public void onClick() {

                        if (CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.FINANCIAL_ADMIN_EXECUTIVE, Role.ACCOUNTS_OFFICER)))) {
                            setResponsePage(new PrepaymentDetailsEditPage(item.getModelObject().getId()));
                        } else {
                            setResponsePage(getPage());
                        }

                    }
                });
            }
        };

        add(eachItem);

    }
}
