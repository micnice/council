package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.InstitutionInspectionDAO;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.InstitutionInspection;
import zw.co.hitrac.council.business.service.InstitutionInspectionService;

import java.util.List;

/**
 *
 * @author Constance Mabaso
 */
@Service
@Transactional
public class InstitutionInspectionServiceImpl implements InstitutionInspectionService {

    @Autowired
    private InstitutionInspectionDAO institutionInspectionDAO;

    @Transactional
    public InstitutionInspection save(InstitutionInspection institutionInspection) {
        return institutionInspectionDAO.save(institutionInspection);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<InstitutionInspection> findAll() {
        return institutionInspectionDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public InstitutionInspection get(Long id) {
        return institutionInspectionDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<InstitutionInspection> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setInstitutionInspectionDAO(InstitutionInspectionDAO institutionInspectionDAO) {
        this.institutionInspectionDAO = institutionInspectionDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<InstitutionInspection> get(Application application) {
        return institutionInspectionDAO.get(application);
    }
}
