package zw.co.hitrac.council.web.pages.reports;

//~--- non-JDK imports --------------------------------------------------------
import net.sf.jasperreports.engine.JRException;

import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;

import zw.co.hitrac.council.reports.Report;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

//~--- JDK imports ------------------------------------------------------------

import java.util.Collection;
import java.util.Map;

/**
 *
 * @author Charles Chigoriwa
 */
public abstract class IReportPage extends TemplatePage {

    public IReportPage() {
        menuPanelManager.getViewReportsPanel().setTopMenuCurrent(true);
    }

    protected void processReport(Report report, ContentType contentType, Collection<?> collection,
            Map<String, Object> parameters)
            throws JRException {
        ByteArrayResource resource = ReportResourceUtils.getReportResource(report, contentType, collection,
                parameters);
        IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                RequestCycle.get().getResponse(), null);

        resource.respond(a);

        // To make Wicket stop processing form after sending response
        RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
