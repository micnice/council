package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.CertificateReprint;

/**
 *
 * @author Matiashe Michael
 */
public interface CertificateReprintService extends IGenericService<CertificateReprint> {
    
}
