package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.CountryDAO;
import zw.co.hitrac.council.business.dao.repo.CountryRepository;
import zw.co.hitrac.council.business.domain.City;
import zw.co.hitrac.council.business.domain.Country;

/**
 *
 * @author Charles Chigoriwa
 */
@Repository
public class CountryDAOImpl implements CountryDAO {

    @Autowired
    private CountryRepository countryRepository;

    public Country save(Country country) {
        return countryRepository.save(country);
    }

    public List<Country> findAll() {
        return countryRepository.findAll();
    }

    public Country get(Long id) {
        return countryRepository.findOne(id);
    }
public void setCountryRepository(CountryRepository countryRepository) {
         this.countryRepository = countryRepository;
   
}

    public List<Country> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}