/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.EmploymentAreaDAO;
import zw.co.hitrac.council.business.domain.EmploymentArea;
import zw.co.hitrac.council.business.service.EmploymentAreaService;

import java.util.List;

/**
 * @author kelvin
 */
@Service
@Transactional
public class EmploymentAreaServiceImpl implements EmploymentAreaService {

    @Autowired
    private EmploymentAreaDAO employmentDao;

    @Transactional
    public EmploymentArea save(EmploymentArea t) {

        return employmentDao.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<EmploymentArea> findAll() {

        return employmentDao.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public EmploymentArea get(Long id) {

        return employmentDao.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<EmploymentArea> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setEmploymentAreaDAO(EmploymentAreaDAO employmentDAO) {

        this.employmentDao = employmentDAO;
    }
}