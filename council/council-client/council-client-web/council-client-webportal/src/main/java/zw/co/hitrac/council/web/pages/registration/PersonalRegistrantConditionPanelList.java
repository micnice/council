package zw.co.hitrac.council.web.pages.registration;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.service.RegistrantConditionService;

/**
 *
 * @author Daniel Nkhoma
 */
public class PersonalRegistrantConditionPanelList extends Panel {

    @SpringBean
    private RegistrantConditionService registrantConditionService;

    public PersonalRegistrantConditionPanelList(String id) {
        super(id);
       
    }
}
