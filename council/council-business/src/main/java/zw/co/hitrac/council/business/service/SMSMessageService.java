package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.AddressType;
import zw.co.hitrac.council.business.domain.SMSMessage;

/**
 *
 * @author Michael Matiashe
 */
public interface SMSMessageService extends IGenericService<SMSMessage> {
}
