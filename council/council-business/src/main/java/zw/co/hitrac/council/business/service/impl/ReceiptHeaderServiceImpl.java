package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.accounts.ReceiptHeaderDAO;
import zw.co.hitrac.council.business.domain.TypeOfService;
import zw.co.hitrac.council.business.domain.User;
import zw.co.hitrac.council.business.domain.accounts.*;
import zw.co.hitrac.council.business.service.accounts.ReceiptHeaderService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;

import zw.co.hitrac.council.business.domain.CouncilDuration;

/**
 * @author Tatenda Chiwandire
 */
@Service
@Transactional
public class ReceiptHeaderServiceImpl implements ReceiptHeaderService {

    @Autowired
    private ReceiptHeaderDAO receiptHeaderDAO;

    @Transactional
    public ReceiptHeader save(ReceiptHeader t) {
        return receiptHeaderDAO.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ReceiptHeader> findAll() {
        return receiptHeaderDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public ReceiptHeader get(Long id) {
        return receiptHeaderDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ReceiptHeader> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setReceiptHeaderDao(ReceiptHeaderDAO receiptHeaderDAO) {
        this.receiptHeaderDAO = receiptHeaderDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ReceiptHeader> getReceiptHeaders(Long receiptNumber, PaymentMethod paymentMethod, PaymentType paymentType, Date startDate, Date endDate, User user, TransactionType transactionType, Boolean reversed) {
        return receiptHeaderDAO.getReceiptHeaders(receiptNumber, paymentMethod, paymentType, startDate, endDate, user, transactionType, reversed);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Integer getTotalNumberOfReceipts(PaymentMethod paymentMethod, Date startDate, Date endDate, User user, TransactionType transactionType, Boolean reversed) {
        return receiptHeaderDAO.getTotalNumberOfReceipts(paymentMethod, startDate, endDate, user, transactionType, reversed);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ReceiptHeader> getReceiptHeadersDeposit(Long receiptNumber, PaymentMethod paymentMethod, PaymentType paymentType, Date startDate, Date endDate, User user, TransactionType transactionType, Boolean reversed) {
        return receiptHeaderDAO.getReceiptHeadersDeposit(receiptNumber, paymentMethod, paymentType, startDate, endDate, user, transactionType, reversed);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ReceiptHeader> getCarryForwardsList(Date startDate, Date endDate, User user, TransactionType transactionType, Boolean utilised) {
        return receiptHeaderDAO.getCarryForwardsList(startDate, endDate, user, transactionType, utilised);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public BigDecimal getCarryForwardBalance(Date endDate, Boolean utilised, Customer customer, TransactionType transactionType) {
        return receiptHeaderDAO.getCarryForwardBalance(endDate, utilised, customer, transactionType);
    }

    @Override
    public List<ReceiptHeader> getCarryForwardBalanceList(Date endDate, Boolean utilised, TransactionType transactionType) {
        return receiptHeaderDAO.getCarryForwardBalanceList(endDate, utilised, transactionType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ReceiptHeader> getChangedBanksList(TransactionType transactionTypeChange, Date startDate, Date endDate) {
        return receiptHeaderDAO.getChangedBanksList(transactionTypeChange, startDate, endDate);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ReceiptHeader> getReceiptListComments() {
        return receiptHeaderDAO.getReceiptListComments();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ReceiptHeader> getReceiptHeadersAllDates(PaymentMethod paymentMethod, PaymentType paymentType, Date startDate, Date endDate, Date depositStartDate, Date depositEndDate, User user, TransactionType transactionType, Boolean reversed) {
        return receiptHeaderDAO.getReceiptHeadersAllDates(paymentMethod, paymentType, startDate, endDate, depositStartDate, depositEndDate, user, transactionType, reversed);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ReceiptHeader> getRegistrantCarryForwardCollected(Date endDate, TransactionType transactionType) {
        return receiptHeaderDAO.getRegistrantCarryForwardCollected(endDate, transactionType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Long getNumberOfReceipts(Date endDate, User user) {
        return receiptHeaderDAO.getNumberOfReceipts(endDate, user);
    }

    @Override
    public List<ReceiptHeader> getReceiptHeaderByCouncilDurationAndAccountAndTypeOfService(CouncilDuration councilDuration, Account account, TypeOfService typeOfService) {
        return receiptHeaderDAO.getReceiptHeaderByCouncilDurationAndAccountAndTypeOfService(councilDuration, account, typeOfService);
    }

    public List<CouncilDuration> getCouncilDurationsfromRecieptHeaders(Account account, TypeOfService typeOfService) {
        return receiptHeaderDAO.getCouncilDurationsfromRecieptHeaders(account, typeOfService);

    }

    @Override
    public List<ReceiptHeader> getRegistrantCarryForwardCollectedForReport(Date startDate, Date endDate, TransactionType transactionType) {
        return receiptHeaderDAO.getRegistrantCarryForwardCollectedForReport(startDate, endDate, transactionType);
    }

    @Override
    public List<ReceiptHeader> getCarryForwardBalanceListForReport(Date startDate, Date endDate, Boolean utilised, TransactionType transactionType) {
        return receiptHeaderDAO.getRegistrantCarryForwardCollectedForReport(startDate, endDate, transactionType);
    }

    @Override
    public BigDecimal getCarryForwardBalanceForReport(Date startDate, Date endDate, Boolean utilised, Customer customer, TransactionType transactionType) {
        return receiptHeaderDAO.getCarryForwardBalanceForReport(startDate, endDate, utilised, customer, transactionType);
    }

    @Override
    public void correctMissingChangeOfBankReport() {
        receiptHeaderDAO.correctMissingChangeOfBankReport();
    }

}
