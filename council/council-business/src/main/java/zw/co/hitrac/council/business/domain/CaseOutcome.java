/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author kelvin
 */
@Entity
@Table(name = "caseoutcome")
@Audited
public class CaseOutcome extends BaseEntity {

    private Boolean dates = Boolean.FALSE;
    private Boolean guilty = Boolean.TRUE;
    private RegistrantStatus registrantStatus;

    public Boolean isDates() {
        if (dates == null) {
            return Boolean.FALSE;
        }
        return dates;
    }

    public void setDates(Boolean dates) {
        this.dates = dates;
    }

    public Boolean getGuilty() {
        if (guilty == null) {
            return Boolean.TRUE;
        }
        return guilty;
    }

    public void setGuilty(Boolean guilty) {
        this.guilty = guilty;
    }
    
    @ManyToOne
    public RegistrantStatus getRegistrantStatus() {
        return registrantStatus;
    }

    public void setRegistrantStatus(RegistrantStatus registrantStatus) {
        this.registrantStatus = registrantStatus;
    }
    
    
}
