
package zw.co.hitrac.council.business.dao.repo.accounts;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.accounts.ReceiptItem;


/**
 *
 * @author Michael Matiashe
 */
public interface ReceiptItemRepository extends CrudRepository<ReceiptItem, Long> {

    public List<ReceiptItem> findAll();
}
