package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.RegistrantCPDItemDAO;
import zw.co.hitrac.council.business.domain.RegistrantCPDCategory;
import zw.co.hitrac.council.business.domain.RegistrantCPDItem;
import zw.co.hitrac.council.business.service.RegistrantCPDItemService;

import java.util.List;

/**
 *
 * @author Constance Mabaso
 */
@Service
@Transactional
public class RegistrantCPDItemServiceImpl implements RegistrantCPDItemService {

    @Autowired
    private RegistrantCPDItemDAO registrantCPDItemDAO;

    @Transactional
    public RegistrantCPDItem save(RegistrantCPDItem registrantCPDItem) {
        return registrantCPDItemDAO.save(registrantCPDItem);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantCPDItem> findAll() {
        return registrantCPDItemDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantCPDItem get(Long id) {
        return registrantCPDItemDAO.get(id);
        }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantCPDItem> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public RegistrantCPDItemDAO getRegistrantCPDItemDAO() {
        return registrantCPDItemDAO;
    }

    public void setRegistrantCPDItemDAO(RegistrantCPDItemDAO registrantCPDItemDAO) {
        this.registrantCPDItemDAO = registrantCPDItemDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantCPDItem> getRegistrantCPDItems(RegistrantCPDCategory registrantCPDCategory) {
        return registrantCPDItemDAO.getRegistrantCPDItems(registrantCPDCategory);
    }
    
}
