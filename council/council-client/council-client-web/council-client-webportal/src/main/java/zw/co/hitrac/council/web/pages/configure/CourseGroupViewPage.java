package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CourseGroup;
import zw.co.hitrac.council.business.service.CourseGroupService;

/**
 *
 * @author Michael Matiashe
 */
public class CourseGroupViewPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private CourseGroupService courseGroupService;

    public CourseGroupViewPage(Long id) {
        CompoundPropertyModel<CourseGroup> model=new CompoundPropertyModel<CourseGroup>(new LoadableDetachableCourseGroupModel(id));
        setDefaultModel(model);
        add(new Link<CourseGroup>("editLink",model){

            @Override
            public void onClick() {
                setResponsePage(new CourseGroupEditPage(getModelObject().getId()));
            }
            
        });
        add(new BookmarkablePageLink<Void>("returnLink", CourseGroupListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("courseGroup",model.bind("name")));
    }


    private final class LoadableDetachableCourseGroupModel extends LoadableDetachableModel<CourseGroup> {

        private Long id;

        public LoadableDetachableCourseGroupModel(Long id) {
            this.id = id;
        }

        @Override
        protected CourseGroup load() {

            return courseGroupService.get(id);
        }
    }
}
