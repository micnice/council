/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.utils;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author Judge Muzinda class to map council takeon balances
 */
public class TakeOnBalanceDMO implements Serializable {

    public Long id;
    public Integer year;
    public BigDecimal creditBalance = BigDecimal.ZERO;
    public BigDecimal debitBalance = BigDecimal.ZERO;

    public TakeOnBalanceDMO() {
    }

    public TakeOnBalanceDMO(Long id, Integer year) {
        this.id = id;
        this.year = year;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getYear() {
        if (year == null) {
            return 0;
        }
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public BigDecimal getCreditBalance() {
        return creditBalance;
    }

    public void setCreditBalance(BigDecimal creditBalance) {
        this.creditBalance = creditBalance;
    }

    public BigDecimal getDebitBalance() {
        return debitBalance;
    }

    public void setDebitBalance(BigDecimal debitBalance) {
        this.debitBalance = debitBalance;
    }
}