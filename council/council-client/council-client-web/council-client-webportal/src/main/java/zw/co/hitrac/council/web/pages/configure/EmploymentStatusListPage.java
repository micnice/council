/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.configure;

import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.EmploymentStatus;
import zw.co.hitrac.council.business.service.EmploymentStatusService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

/**
 *
 * @author kelvin
 */
public class EmploymentStatusListPage extends IAdministerDatabaseBasePage{
    
    @SpringBean
    private EmploymentStatusService employmentStatusService;

    public EmploymentStatusListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink",AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink",AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {

            @Override
            public void onClick() {
               setResponsePage(new EmploymentStatusEditPage(null));
            }
        });
        
        IModel<List<EmploymentStatus>> model=new LoadableDetachableModel<List<EmploymentStatus>>() {

            @Override
            protected List<EmploymentStatus> load() {
               return employmentStatusService.findAll();
            }
        };
        
        PropertyListView<EmploymentStatus> eachItem=new PropertyListView<EmploymentStatus>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<EmploymentStatus> item) {
                Link<EmploymentStatus> viewLink=new Link<EmploymentStatus>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                       setResponsePage(new EmploymentStatusViewPage(getModelObject().getId()));
                    }
                };
                if(item.getIndex()%2==0){
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };
        
        add(eachItem);
    }
    
    
    
}
