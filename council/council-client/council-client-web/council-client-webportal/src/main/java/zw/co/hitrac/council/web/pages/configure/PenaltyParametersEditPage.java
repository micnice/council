package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.PenaltyParameters;
import zw.co.hitrac.council.business.service.PenaltyParametersService;

/**
 *
 * @author Matiashe Michael
 */
public class PenaltyParametersEditPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private PenaltyParametersService penaltyParametersService;

    public PenaltyParametersEditPage() {
        setDefaultModel(new CompoundPropertyModel<PenaltyParameters>(new LoadableDetachablePenaltyParametersModel()));

        Form<PenaltyParameters> form = new Form<PenaltyParameters>("form", (IModel<PenaltyParameters>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                penaltyParametersService.save(getModelObject());
                setResponsePage(new PenaltyParametersViewPage());

            }
        };

        form.add(new TextField<String>("penaltyPeriod"));
        form.add(new TextField<String>("percentagePenalty"));
        form.add(new TextField<String>("charge"));
        form.add(new CheckBox("hasReregistrationProduct"));
        form.add(new TextField<String>("reRegistrationPeriod"));
        form.add(new CheckBox("penaltyIsIncremental"));
        
        form.add(new BookmarkablePageLink<Void>("returnLink", PenaltyParametersViewPage.class));
        add(form);
    }

    private final class LoadableDetachablePenaltyParametersModel extends LoadableDetachableModel<PenaltyParameters> {

        @Override
        protected PenaltyParameters load() {
            return penaltyParametersService.get();
        }
    }
}
