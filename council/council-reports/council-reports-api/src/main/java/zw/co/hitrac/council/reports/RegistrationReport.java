package zw.co.hitrac.council.reports;

/**
 *
 * @author Charles Chigoriwa
 */
public class RegistrationReport extends Report {
    @Override
    protected String getOutputReportName() {
        return "RegistrationReport";
    }

    @Override
    protected String getReportPath() {
        return "/zw/co/hitrac/council/reports/RegistrationReport";
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
