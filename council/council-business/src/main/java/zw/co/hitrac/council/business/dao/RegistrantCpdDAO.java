package zw.co.hitrac.council.business.dao;

import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantCpd;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;

import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author Constance Mabaso
 */
public interface RegistrantCpdDAO extends IGenericDAO<RegistrantCpd> {

    BigDecimal getCurrentCPDs(Registrant registrant, Duration duration);

    BigDecimal getCurrentCPDs(Registrant registrant, CouncilDuration duration);

    List<RegistrantCpd> getCurrentCPDsList(Registrant registrant, Duration duration);

    List<RegistrantCpd> getCurrentCPDsList(Registrant registrant);

    List<RegistrantCpd> getCPDsList(Registrant registrant);

    List<RegistrantData> getPaidAnnualFeeAndInadequateCPDS(CouncilDuration councilDuration);
}
