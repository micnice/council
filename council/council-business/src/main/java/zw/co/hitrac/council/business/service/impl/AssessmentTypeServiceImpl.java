/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.AssessmentTypeDAO;
import zw.co.hitrac.council.business.domain.AssessmentType;
import zw.co.hitrac.council.business.service.AssessmentTypeService;

import java.util.List;

/**
 *
 * @author kelvin
 */
@Service
@Transactional
public class AssessmentTypeServiceImpl implements AssessmentTypeService{
@Autowired
    private AssessmentTypeDAO addressTypeDAO;
    

    @Transactional
    public AssessmentType save(AssessmentType addressType) {
       return addressTypeDAO.save(addressType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<AssessmentType> findAll() {
        return addressTypeDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public AssessmentType get(Long id) {
       return addressTypeDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<AssessmentType> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setAssessmentDAO(AssessmentTypeDAO addressTypeDAO) {

        this.addressTypeDAO = addressTypeDAO;
    }
    
}
