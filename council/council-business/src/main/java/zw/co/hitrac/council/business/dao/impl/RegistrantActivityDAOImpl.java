package zw.co.hitrac.council.business.dao.impl;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DateType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.RegistrantActivityDAO;
import zw.co.hitrac.council.business.dao.repo.RegistrantActivityRepository;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.GeneralParametersService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;

/**
 * @author Kelvin Goredema
 * @author Morris Baradza
 * @author Michael Matiashe
 */
@Repository
public class RegistrantActivityDAOImpl implements RegistrantActivityDAO {

    @Autowired
    private RegistrantActivityRepository registrantActivityRepository;
    @Autowired
    private GeneralParametersService generalParametersService;
    @PersistenceContext
    private EntityManager entityManager;

    public RegistrantActivity save(RegistrantActivity registrantActivity) {

        return registrantActivityRepository.save(registrantActivity);
    }

    public List<RegistrantActivity> findAll() {

        return registrantActivityRepository.findAll();

    }

    public RegistrantActivity get(Long id) {

        return registrantActivityRepository.findOne(id);

    }

    public List<RegistrantActivity> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setRegistrantActivityRepository(RegistrantActivityRepository registrantActivityRepository) {

        this.registrantActivityRepository = registrantActivityRepository;
    }

    public void remove(RegistrantActivity registrantActivity) {

        registrantActivityRepository.delete(registrantActivity.getId());
    }

    public List<RegistrantActivity> getRegistrantActivities(Registrant registrant) {

        return this.entityManager.createQuery("Select a from RegistrantActivity a where a.registrant=:registrant")
                .setParameter("registrant", registrant)
                .getResultList();
    }

    @Override
    public List<CouncilDuration> getRegisteredRenewalPeriods(String registrationNumber) {
        return this.entityManager.createQuery("Select DISTINCT a.duration.councilDuration from RegistrantActivity a where a.registrant.registrationNumber=:registrationNumber and a.registrantActivityType=:registrantActivityType ORDER BY a.duration.councilDuration.name ASC")
                .setParameter("registrationNumber", registrationNumber)
                .setParameter("registrantActivityType", RegistrantActivityType.RENEWAL)
                .getResultList();
    }

    public CouncilDuration getLastCouncilDurationRenewal(Registrant registrant) {
        List<RegistrantActivity> list = this.entityManager.createQuery("Select a from RegistrantActivity a where a.registrant=:registrant and a.registrantActivityType=:registrantActivityType ORDER BY a.duration.endDate DESC")
                .setParameter("registrant", registrant)
                .setParameter("registrantActivityType", RegistrantActivityType.RENEWAL)
                .getResultList();
        if (list.isEmpty()) {
            return null;
        } else {
            return list.get(0).getDuration().getCouncilDuration();
        }
    }

    public String getLastRenewal(Registrant registrant) {
        List<String> list = this.entityManager.createQuery("Select MAX(a.duration.councilDuration.name) from RegistrantActivity a where a.registrant=:registrant and a.registrantActivityType=:registrantActivityType ORDER BY a.duration.endDate DESC")
                .setParameter("registrant", registrant)
                .setParameter("registrantActivityType", RegistrantActivityType.RENEWAL)
                .getResultList();
        if (list.isEmpty()) {
            return null;
        } else {
            return list.get(0);
        }
    }

    public RegistrantActivity getLastRenewalActivity(Registrant registrant) {
        List<RegistrantActivity> list = this.entityManager.createQuery("Select a from RegistrantActivity a where a.registrant=:registrant and a.registrantActivityType=:registrantActivityType ORDER BY a.duration.endDate DESC")
                .setParameter("registrant", registrant)
                .setParameter("registrantActivityType", RegistrantActivityType.RENEWAL)
                .getResultList();
        if (list.isEmpty()) {
            return null;
        } else {
            return list.get(0);
        }
    }

    public RegistrantActivity getRegistrantActivity(RegistrantActivityType registrantActivityType, Registrant registrant) {
        List<RegistrantActivity> list = this.entityManager.createQuery("Select a from RegistrantActivity a where a.registrant=:registrant and a.registrantActivityType=:registrantActivityType")
                .setParameter("registrant", registrant)
                .setParameter("registrantActivityType", registrantActivityType)
                .getResultList();
        if (list.isEmpty()) {
            return null;
        } else {
            return list.get(0);
        }
    }

    public Integer getTotalPerCourseAndDurationAndRegisterAndRegistrantActivity(Course course, Register register, List<Duration> durations, RegistrantActivityType registrantActivityType, Boolean active) {

        return getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(course, register, durations, registrantActivityType, active, null);
    }

    public Integer getTotalPerQualificationAndDurationAndRegisterAndRegistrantActivity(Qualification qualification, Register register, Duration duration, RegistrantActivityType registrantActivityType) {

        Long l = (Long) entityManager.createQuery("select DISTINCT count(reg.registrant.id) from Registration reg where reg.registrant.id in(select ra.registrant.id from RegistrantActivity ra where ra.registrantActivityType=:registrantActivityType and ra.duration.id=:duration) and reg.register.id=:register and reg.deRegistrationDate is null and reg.registrant.id in(select rq.registrant.id from RegistrantQualification rq where rq.qualification.id=:qualification and rq.retired=:retired)").setParameter("register", register.getId()).setParameter("duration", duration.getId()).setParameter("registrantActivityType", registrantActivityType).setParameter("retired", Boolean.FALSE).setParameter("qualification", qualification.getId()).getSingleResult();
        return l.intValue();
    }

    //course in registrant qualification
    public Integer getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(Course course, Register register, List<Duration> durations, RegistrantActivityType registrantActivityType, Boolean active, Conditions condition) {

        EmploymentType employmentType = null;

        return getListPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(course,
                register, durations, registrantActivityType, active, employmentType, condition).size();

    }
    //select d.startDate, d.endDate, r.registrant_id from registrantactivity r join duration d on r.duration_id=d.id where r.registrant_id=38 order by d.endDate desc limit 1

    //course in registrant qualification
    @Override
    public List<RegistrantData> getListPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(Course course, Register register, List<Duration> durations, RegistrantActivityType registrantActivityType, Boolean active, Enum<EmploymentType> e, Conditions condition) {

        StringBuilder sb = new StringBuilder();
        StringBuilder list = new StringBuilder();
        String idList = null;
        String businessAddress = null;
        String residentialAddress = null;
        String postalAddress = null;
        String mobileNumber = null;
        String business = null;
        String emailAddress = null;
        List<RegistrantData> registrantDataList;

        if (generalParametersService.get().getBusinessAddressType() != null) {
            businessAddress = "(select CONCAT_WS(' ',ra.address1, ra.address2, c.name) from registrantaddress ra inner join city c on c.id=ra.city_id where ra.registrant_id =registrant.id AND ra.retired = 0 AND ra.addressType_id =" + generalParametersService.get().getBusinessAddressType().getId() + " LIMIT 1) businessAddress";
        }

        if (generalParametersService.get().getPhysicalAddressType() != null) {
            residentialAddress = "(select CONCAT_WS(' ',ra.address1, ra.address2, c.name) from registrantaddress ra inner join city c on c.id=ra.city_id where ra.registrant_id =registrant.id AND ra.retired = 0 AND ra.addressType_id =" + generalParametersService.get().getPhysicalAddressType().getId() + " LIMIT 1) residentialAddress";
        }

        if (generalParametersService.get().getContactAddressType() != null) {
            postalAddress = "(select CONCAT_WS(' ',ra.address1, ra.address2, c.name) from registrantaddress ra inner join city c on c.id=ra.city_id where ra.registrant_id =registrant.id AND ra.retired = 0 AND ra.addressType_id =" + generalParametersService.get().getContactAddressType().getId() + " LIMIT 1) postalAddress";
        }

        if (generalParametersService.get().getMobileNumberContactType() != null) {
            mobileNumber = "(select contact_detail from registrantcontact rc where rc.registrant_id =registrant.id AND rc.retired = 0 AND rc.contactType_id =" + generalParametersService.get().getMobileNumberContactType().getId() + " LIMIT 1) mobileNumber";
        }

        if (generalParametersService.get().getBusinessNumberContactType() != null) {
            business = "(select contact_detail from registrantcontact rc where rc.registrant_id =registrant.id AND rc.retired = 0 AND rc.contactType_id =" + generalParametersService.get().getBusinessNumberContactType().getId() + " LIMIT 1) business";
        }

        if (generalParametersService.get().getEmailContactType() != null) {
            emailAddress = "(select contact_detail from registrantcontact rc where rc.registrant_id =registrant.id AND rc.retired = 0 AND rc.contactType_id =" + generalParametersService.get().getEmailContactType().getId() + " LIMIT 1) emailAddress";
        }

        for (Duration d : durations) {
            list.append(d.getId().toString()).append(",");
        }

        if (!durations.isEmpty()) {
            idList = list.substring(0, list.lastIndexOf(","));
        }

        if (active) {
            sb.append("select distinct registrant.id, registrant.firstname, registrant.middlename, registrant.lastname, registrant.birthDate, "
                    + "registrant.registrationNumber,conditions.name as conditionName, qualification.name as qualificationName, registrant.idNumber,registrant.gender,reg.registrationDate, "
                    + "e.employmentType as employmentTypeName, case WHEN i.name IS NULL THEN e.employer ELSE i.name END AS employerName, course.name as courseName, t.name as titleName, "
                    + "cc.name as cityName, dd.name as districtName , pp.name as provinceName");

            sb.append(",");
            sb.append(businessAddress == null ? " '' as businessAddress " : businessAddress);
            sb.append(",");
            sb.append(residentialAddress == null ? " '' as residentialAddress " : residentialAddress);
            sb.append(",");
            sb.append(postalAddress == null ? " '' as postalAddress " : postalAddress);
            sb.append(",");
            sb.append(mobileNumber == null ? " '' as mobileNumber " : mobileNumber);
            sb.append(",");
            sb.append(business == null ? " '' as business " : business);
            sb.append(",");
            sb.append(emailAddress == null ? " '' as emailAddress " : emailAddress);

            sb.append(" FROM registrant INNER JOIN registration reg ON reg.registrant_id=registrant.id ");

            if (condition != null) {
                sb.append(" INNER JOIN registrantcondition rc on rc.registrant_id = registrant.id and rc.status=1 and rc.condition_id=").append(condition.getId());
            }
            sb.append(" LEFT JOIN title t on t.id = registrant.title_id ");
            sb.append(" LEFT JOIN employment e on e.registrant_id = registrant.id ");
            sb.append(" LEFT JOIN city cc on e.city_id = cc.id ");
            sb.append(" LEFT JOIN district dd on cc.district_id = dd.id ");
            sb.append(" LEFT JOIN province pp on dd.province_id = pp.id ");
            sb.append(" LEFT JOIN institution i on e.institution_id = i.id ");
            sb.append(" LEFT JOIN registrantcondition on registrantcondition.registrant_id=registrant.id");
            sb.append(" LEFT JOIN conditions on registrantcondition.condition_id=conditions.id");
            sb.append(" LEFT JOIN registrantqualification on registrantqualification.registrant_id=registrant.id");
            sb.append(" LEFT JOIN qualification on registrantqualification.qualification_id=qualification.id");
            sb.append(" LEFT JOIN course on course.id=registrantqualification.course_id");
            sb.append(" WHERE registrant.id IN (SELECT DISTINCT registrant_id ");
            sb.append(" FROM registrantactivity WHERE duration_id IN ").append("(" + StringUtils.defaultString(idList, "") + ")");
            sb.append(" AND registrantActivityType='" + registrantActivityType + "'");
            sb.append(" AND registrant_id IS NOT NULL)");
            if (e != null) {
                sb.append(" AND e.employmentType='" + e + "'");
            }
            if (register != null) {
                sb.append(" AND reg.register_id ='" + register.getId() + "'");
            }
            sb.append(" AND reg.deRegistrationDate IS NULL ");
            sb.append(" AND reg.registrant_id IN (SELECT rq.registrant_id FROM registrantqualification rq ");
            sb.append(" WHERE rq.course_id='" + course.getId() + "'");
            sb.append(" AND rq.retired = FALSE");
            sb.append(" AND rq.registrant_id IS NOT NULL)");
            sb.append(" order by registrant.lastname, registrant.firstname, registrant.middlename DESC");

            Session session = entityManager.unwrap(Session.class);

            SQLQuery query = session.createSQLQuery(sb.toString());
            query.addScalar("id", LongType.INSTANCE);
            query.addScalar("titleName", StringType.INSTANCE);
            query.addScalar("firstname", StringType.INSTANCE);
            query.addScalar("middlename", StringType.INSTANCE);
            query.addScalar("lastname", StringType.INSTANCE);
            query.addScalar("gender", StringType.INSTANCE);
            query.addScalar("registrationNumber", StringType.INSTANCE);
            query.addScalar("idNumber", StringType.INSTANCE);
            query.addScalar("courseName", StringType.INSTANCE);
            query.addScalar("qualificationName", StringType.INSTANCE);
            query.addScalar("registrationDate", DateType.INSTANCE);
            query.addScalar("businessAddress", StringType.INSTANCE);
            query.addScalar("residentialAddress", StringType.INSTANCE);
            query.addScalar("postalAddress", StringType.INSTANCE);
            query.addScalar("mobileNumber", StringType.INSTANCE);
            query.addScalar("business", StringType.INSTANCE);
            query.addScalar("emailAddress", StringType.INSTANCE);
            query.addScalar("employmentTypeName", StringType.INSTANCE);
            query.addScalar("employerName", StringType.INSTANCE);
            query.addScalar("conditionName", StringType.INSTANCE);
            query.addScalar("birthDate", DateType.INSTANCE);
            query.addScalar("cityName", StringType.INSTANCE);
            query.addScalar("districtName", StringType.INSTANCE);
            query.addScalar("provinceName", StringType.INSTANCE);
            query.setResultTransformer(Transformers.aliasToBean(RegistrantData.class));
            registrantDataList = query.list();
        } else {
            sb.append("select distinct registrant.id, registrant.firstname, registrant.middlename, registrant.lastname, registrant.birthDate,"
                    + "registrant.registrationNumber, qualification.name as qualificationName, registrant.idNumber,registrant.gender,reg.registrationDate, "
                    + "e.employmentType as employmentTypeName, case WHEN i.name IS NULL THEN e.employer ELSE i.name END AS employerName , course.name as courseName, t.name as titleName, "
                    + "cc.name as cityName, dd.name as districtName , pp.name as provinceName");
            sb.append(",");
            sb.append(businessAddress == null ? " '' as businessAddress " : businessAddress);
            sb.append(",");
            sb.append(residentialAddress == null ? " '' as residentialAddress " : residentialAddress);
            sb.append(",");
            sb.append(postalAddress == null ? " '' as postalAddress " : postalAddress);
            sb.append(",");
            sb.append(mobileNumber == null ? " '' as mobileNumber " : mobileNumber);
            sb.append(",");
            sb.append(business == null ? " '' as business " : business);
            sb.append(",");
            sb.append(emailAddress == null ? " '' as emailAddress " : emailAddress);

            sb.append(" FROM registrant INNER JOIN registration reg ON reg.registrant_id=registrant.id ");

            if (condition != null) {
                sb.append(" INNER JOIN registrantcondition rc on rc.registrant_id = registrant.id and rc.status=1 and rc.condition_id=").append(condition.getId());
            }

            sb.append(" LEFT JOIN title t on t.id = registrant.title_id ");
            sb.append(" LEFT JOIN employment e on e.registrant_id = registrant.id ");
            sb.append(" LEFT JOIN city cc on e.city_id = cc.id ");
            sb.append(" LEFT JOIN district dd on cc.district_id = dd.id ");
            sb.append(" LEFT JOIN province pp on dd.province_id = pp.id ");
            sb.append(" LEFT JOIN institution i on e.institution_id = i.id ");
            sb.append(" LEFT JOIN registrantqualification on registrantqualification.registrant_id=registrant.id");
            sb.append(" LEFT JOIN qualification on registrantqualification.qualification_id=qualification.id");
            sb.append(" LEFT JOIN course on course.id=registrantqualification.course_id");
            sb.append(" WHERE registrant.id NOT IN (SELECT DISTINCT registrant_id ");
            sb.append(" FROM registrantactivity WHERE duration_id IN ").append("(" + StringUtils.defaultString(idList, "") + ")");
            sb.append(" AND registrantActivityType='" + registrantActivityType + "'");
            sb.append(" AND registrant_id IS NOT NULL)");
            if (e != null) {
                sb.append(" AND e.employmentType='" + e + "'");
            }
            if (register != null) {
                sb.append(" AND reg.register_id ='" + register.getId() + "'");
            }
            sb.append(" AND reg.deRegistrationDate IS NULL ");
            sb.append(" AND reg.registrant_id IN (SELECT rq.registrant_id FROM registrantqualification rq ");
            sb.append(" WHERE rq.course_id='" + course.getId() + "'");
            sb.append(" AND rq.retired = FALSE");
            sb.append(" AND rq.registrant_id IS NOT NULL)");
            sb.append(" order by registrant.lastname, registrant.firstname, registrant.middlename DESC");


            Session session = entityManager.unwrap(Session.class);

            SQLQuery query = session.createSQLQuery(sb.toString());
            query.addScalar("id", LongType.INSTANCE);
            query.addScalar("titleName", StringType.INSTANCE);
            query.addScalar("firstname", StringType.INSTANCE);
            query.addScalar("middlename", StringType.INSTANCE);
            query.addScalar("lastname", StringType.INSTANCE);
            query.addScalar("gender", StringType.INSTANCE);
            query.addScalar("registrationNumber", StringType.INSTANCE);
            query.addScalar("idNumber", StringType.INSTANCE);
            query.addScalar("courseName", StringType.INSTANCE);
            query.addScalar("qualificationName", StringType.INSTANCE);
            query.addScalar("registrationDate", DateType.INSTANCE);
            query.addScalar("businessAddress", StringType.INSTANCE);
            query.addScalar("residentialAddress", StringType.INSTANCE);
            query.addScalar("postalAddress", StringType.INSTANCE);
            query.addScalar("mobileNumber", StringType.INSTANCE);
            query.addScalar("business", StringType.INSTANCE);
            query.addScalar("emailAddress", StringType.INSTANCE);
            query.addScalar("employmentTypeName", StringType.INSTANCE);
            query.addScalar("employerName", StringType.INSTANCE);
            query.addScalar("birthDate", DateType.INSTANCE);
            query.addScalar("cityName", StringType.INSTANCE);
            query.addScalar("districtName", StringType.INSTANCE);
            query.addScalar("provinceName", StringType.INSTANCE);
            query.setResultTransformer(Transformers.aliasToBean(RegistrantData.class));

            System.out.println(sb.toString());
            registrantDataList = query.list();
        }
        return new ArrayList<>(new HashSet<RegistrantData>(registrantDataList));

    }

    public Integer getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(Course course, Register register, List<Duration> durations, RegistrantActivityType registrantActivityType, Boolean active, Enum<EmploymentType> e, Conditions condition) {

        return getListPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(course, register, durations, registrantActivityType, active, e, condition).size();
    }
    //code should start before end duration date for registration start date and qualification date awarded

    public List<RegistrantActivity> getRegistrantActivities(Registrant registrant, RegistrantActivityType registrantActivityType) {

        return this.entityManager.createQuery("Select a from RegistrantActivity a where a.registrant=:registrant"
                + " and a.registrantActivityType=:registrantActivityType")
                .setParameter("registrant", registrant)
                .setParameter("registrantActivityType", registrantActivityType)
                .getResultList();
    }

    public RegistrantActivity getRegistrantActivityByDuration(Registrant registrant, Duration duration) {

        List<RegistrantActivity> registrantActivities = this.entityManager.createQuery("Select a from RegistrantActivity a where a.registrant=:registrant"
                + " and a.duration=:duration")
                .setParameter("registrant", registrant)
                .setParameter("duration", duration)
                .getResultList();
        if (!registrantActivities.isEmpty() || registrantActivities.size() > 0) {
            return registrantActivities.get(0);
        } else {
            return null;
        }
    }

    public boolean checkRegistrantActivity(Registrant registrant, RegistrantActivityType registrantActivityType, Duration duration) {
        Long count = (Long) this.entityManager.createQuery("Select count(a) from RegistrantActivity a where a.registrant=:registrant"
                + " and a.registrantActivityType=:registrantActivityType and a.duration=:duration ")
                .setParameter("registrant", registrant)
                .setParameter("registrantActivityType", registrantActivityType)
                .setParameter("duration", duration)
                .getSingleResult();
        return count > 0L ? true : false;
    }

    public boolean checkRegistrantActivity(Registrant registrant, RegistrantActivityType registrantActivityType, CouncilDuration councilDuration) {
        Long count = (Long) this.entityManager.createQuery("Select count(a) from RegistrantActivity a where a.registrant=:registrant"
                + " and a.registrantActivityType=:registrantActivityType and a.duration.councilDuration=:councilDuration ")
                .setParameter("registrant", registrant)
                .setParameter("registrantActivityType", registrantActivityType)
                .setParameter("councilDuration", councilDuration)
                .getSingleResult();
        return count > 0L ? true : false;
    }

    public List<RegistrantData> getRegistrant(RegistrantActivityType registrantActivityType, Date startDate, Date endDate) {


        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(RegistrantActivity.class);
        criteria.add(Restrictions.eq("registrantActivityType", registrantActivityType));
        criteria.add(Restrictions.between("startDate", startDate, endDate));
        criteria.createAlias("registrant", "r");
        ProjectionList proList = Projections.projectionList();
        proList.add(Projections.property("r.id"), "id");
        proList.add(Projections.property("r.firstname"), "firstname");
        proList.add(Projections.property("r.lastname"), "lastname");
        proList.add(Projections.property("r.middlename"), "middlename");
        proList.add(Projections.property("r.registrationNumber"), "registrationNumber");
        proList.add(Projections.property("r.idNumber"), "idNumber");
        criteria.setProjection(proList);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
        criteria.setResultTransformer(Transformers.aliasToBean(RegistrantData.class));
        return criteria.list();
    }

    public List<RegistrantData> getListPerCourseAndDurationAndRegisterAndRegistrantActivity(Course course, Register register, List<Duration> durations, RegistrantActivityType registrantActivityType, Boolean active) {

        String list = "";
        for (Duration d : durations) {
            list += d.getId().toString() + ",";
        }
        if (!list.isEmpty()) {
            list = list.substring(0, list.length() - 1);
        }

        if (active) {
            String queryString = "select registrant.id, registrant.firstname, registrant.middlename, registrant.lastname, registrant.registrationNumber, registrant.idNumber, c.name as courseName FROM registrant INNER JOIN registration reg ON reg.registrant_id = registrant.id INNER JOIN course c on c.id = reg.course_id WHERE registrant.id IN ( SELECT DISTINCT registrant_id FROM registrantactivity WHERE duration_id IN (" + list + ") AND registrantActivityType='" + registrantActivityType + "' AND registrant_id IS NOT NULL) AND reg.register_id =" + register.getId() + " AND reg.deRegistrationDate IS NULL AND reg.course_id = " + course.getId() + " order by registrant.lastname, registrant.firstname, registrant.middlename DESC";
            return entityManager.createNativeQuery(queryString, RegistrantData.class).getResultList();
        } else {
            String queryString = "select registrant.id, registrant.firstname, registrant.middlename, registrant.lastname, registrant.registrationNumber, registrant.idNumber, c.name as courseName FROM registrant INNER JOIN registration reg ON reg.registrant_id = registrant.id INNER JOIN course c on c.id = reg.course_id WHERE registrant.id NOT IN ( SELECT DISTINCT registrant_id FROM registrantactivity WHERE duration_id IN (" + list + ") AND registrantActivityType='" + registrantActivityType + "' AND registrant_id IS NOT NULL) AND reg.register_id =" + register.getId() + " AND reg.deRegistrationDate IS NULL  AND reg.course_id = " + course.getId() + " order by registrant.lastname, registrant.firstname, registrant.middlename DESC";
            return entityManager.createNativeQuery(queryString, RegistrantData.class).getResultList();
        }
    }

    public List<RegistrantContact> getEmailAddressesPerQualificationMappedCourse(Course course, Register register, ContactType contactType) {

        if (course != null && register != null) {
            return entityManager.createQuery("SELECT a FROM RegistrantContact a, Registration r, RegistrantQualification rq WHERE a.registrant=r.registrant and r.registrant=rq.registrant and a.contactType=:contactType and r.register=:register and rq.course=:course").setParameter("contactType", contactType).setParameter("course", course).setParameter("register", register).getResultList();
        } else if (course != null && register == null) {
            return entityManager.createQuery("SELECT a FROM RegistrantContact a, Registration r, RegistrantQualification rq WHERE a.registrant=r.registrant and r.registrant=rq.registrant and a.contactType=:contactType and rq.course=:course").setParameter("contactType", contactType).setParameter("course", course).getResultList();
        } else if (course == null && register != null) {
            return entityManager.createQuery("SELECT a FROM RegistrantContact a, Registration r WHERE a.registrant =r.registrant and a.contactType=:contactType and r.register=:register").setParameter("contactType", contactType).setParameter("register", register).getResultList();
        } else {
            return entityManager.createQuery("SELECT a FROM RegistrantContact a, Registration r WHERE a.registrant=r.registrant and a.contactType=:contactType").setParameter("contactType", contactType).getResultList();
        }
    }

    public List<String> getEmailAddresses(Course course, Register register, ContactType contactType) {

        if (course != null && register != null) {
            return entityManager.createQuery("SELECT a.contactDetail FROM RegistrantContact a, Registration r, RegistrantQualification rq WHERE a.registrant=r.registrant and r.registrant=rq.registrant and a.contactType=:contactType and r.register=:register and rq.course=:course").setParameter("contactType", contactType).setParameter("course", course).setParameter("register", register).getResultList();
        } else if (course != null && register == null) {
            return entityManager.createQuery("SELECT a.contactDetail FROM RegistrantContact a, Registration r, RegistrantQualification rq WHERE a.registrant=r.registrant and r.registrant=rq.registrant and a.contactType=:contactType and rq.course=:course").setParameter("contactType", contactType).setParameter("course", course).getResultList();
        } else if (course == null && register != null) {
            return entityManager.createQuery("SELECT a.contactDetail FROM RegistrantContact a, Registration r WHERE a.registrant=r.registrant and a.contactType=:contactType and r.register=:register").setParameter("contactType", contactType).setParameter("register", register).getResultList();
        } else {
            return entityManager.createQuery("SELECT a.contactDetail FROM RegistrantContact a, Registration r WHERE a.registrant=r.registrant and a.contactType=:contactType").setParameter("contactType", contactType).getResultList();
        }
    }

    public List<RegistrantData> getListPerQualificationAndDurationAndRegisterAndRegistrantActivity(Qualification qualification, Register register, Duration duration, RegistrantActivityType registrantActivityType) {
        return entityManager.createQuery("select DISTINCT new zw.co.hitrac.council.business.domain.reports.RegistrantData(reg.registrant.id, reg.registrant.firstname,reg.registrant.lastname,reg.registrant.middlename,reg.registrant.birthDate,reg.registrant.gender,reg.registrant.idNumber,reg.registrant.registrationNumber) from Registration reg where reg.registrant.id in(select ra.registrant.id from RegistrantActivity ra where ra.registrantActivityType=:registrantActivityType and ra.duration.id=:duration) and reg.register.id=:register and reg.deRegistrationDate is null and reg.registrant.id in (select rq.registrant.id from RegistrantQualification rq where rq.qualification=:qualification and rq.retired=:retired) ORDER BY reg.registrant.lastname, reg.registrant.lastname,reg.registrant.firstname DESC").setParameter("retired", Boolean.FALSE).setParameter("register", register.getId()).setParameter("duration", duration.getId()).setParameter("registrantActivityType", registrantActivityType).setParameter("qualification", qualification).getResultList();
    }

    public List<RegistrantActivity> getRegistrantsByRegistrantActivityType(RegistrantActivityType registrantActivityType) {

        return entityManager.createQuery("SELECT ra FROM RegistrantActivity ra WHERE ra.registrantActivityType=:registrantActivityType AND ra.retired=FALSE").setParameter("registrantActivityType", registrantActivityType).getResultList();
    }

    @Override
    public List<Registrant> getRegistrantsRenewedInCouncilDurationNotInListOfCouncilDurations(CouncilDuration councilDuration, List<CouncilDuration> councilDurations) {
        return this.entityManager.createQuery("Select DISTINCT a.registrant from RegistrantActivity a where a.duration.councilDuration=:councilDuration and a.registrantActivityType=:registrantActivityType and a.registrant not in(select b.registrant from RegistrantActivity b where b.duration.councilDuration in :councilDurations and b.registrantActivityType=:registrantActivityType)")
                .setParameter("councilDuration", councilDuration)
                .setParameter("councilDurations", councilDurations)
                .setParameter("registrantActivityType", RegistrantActivityType.RENEWAL)
                .getResultList();
    }

    private List<Registrant> getRegistrantsRenewedInCouncilDuration(CouncilDuration councilDuration) {
        return this.entityManager.createQuery("Select DISTINCT a.registrant from RegistrantActivity a where a.duration.councilDuration=:councilDuration and a.registrantActivityType=:registrantActivityType")
                .setParameter("councilDuration", councilDuration)
                .setParameter("registrantActivityType", RegistrantActivityType.RENEWAL)
                .getResultList();
    }

    @Override
    public List<Registrant> getRegistrantsRenewedInCouncilDuration(CouncilDuration councilDuration, Course course) {
        if (course == null) {
            return getRegistrantsRenewedInCouncilDuration(councilDuration);
        } else {
            return entityManager.createQuery("select r.registrant from RegistrantActivity r where r.registrantActivityType=:registrantActivityType and r.duration.councilDuration=:councilDuration and r.registrant in (select reg.registrant from Registration reg where reg.course=:course and reg.deRegistrationDate is null)")
                    .setParameter("councilDuration", councilDuration)
                    .setParameter("registrantActivityType", RegistrantActivityType.RENEWAL)
                    .setParameter("course", course)
                    .getResultList();
        }
    }

    @Override
    public List<Registrant> getRegistrantsWhoNeverRenewed() {
        return this.entityManager.createQuery("Select DISTINCT r from Registrant r where r not in(select b.registrant from RegistrantActivity b where b.registrantActivityType=:registrantActivityType)")
                .setParameter("registrantActivityType", RegistrantActivityType.RENEWAL)
                .getResultList();
    }
}
