package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Country;
import zw.co.hitrac.council.business.service.CountryService;

import java.util.List;

/**
 *
 * @author Charles Chigoriwa
 */
public class CountryListPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private CountryService countryService;

    public CountryListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new CountryEditPage(null, getPageReference()));
            }
        });

        IModel<List<Country>> model = new LoadableDetachableModel<List<Country>>() {
            @Override
            protected List<Country> load() {
                return countryService.findAll();
            }
        };

        PropertyListView<Country> eachItem = new PropertyListView<Country>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<Country> item) {
                Link<Country> viewLink = new Link<Country>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new CountryViewPage(getModelObject().getId()));
                    }
                };
                item.add(viewLink);
                viewLink.add(new Label("name"));
                item.add(new Label("isoCode1"));
                item.add(new Label("isoCode2"));
                item.add(new Label("iana"));
                item.add(new Label("ioc"));
                item.add(new Label("itu"));
                item.add(new Label("un"));
                item.add(new Label("iso"));
            }
        };

        add(eachItem);
    }
}
