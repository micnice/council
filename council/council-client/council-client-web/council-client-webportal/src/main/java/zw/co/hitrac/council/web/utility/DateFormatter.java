
package zw.co.hitrac.council.web.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author morris baradza
 */
public class DateFormatter {
   private static SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    public static Date formatDate(String date1) throws ParseException{
        return dateFormat.parse(date1);
    }
}
