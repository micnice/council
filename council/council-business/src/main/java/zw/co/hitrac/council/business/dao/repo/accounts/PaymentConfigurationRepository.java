
package zw.co.hitrac.council.business.dao.repo.accounts;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.PaymentConfiguration;

/**
 *
 * @author Kelvin Goredema
 */
public interface PaymentConfigurationRepository extends CrudRepository<PaymentConfiguration, Long>{
    public List<PaymentConfiguration> findAll();
}
