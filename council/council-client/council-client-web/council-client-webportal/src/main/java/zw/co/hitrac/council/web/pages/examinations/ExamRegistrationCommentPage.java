package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.business.service.ModulePaperService;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Michael Matiashe
 */
public class ExamRegistrationCommentPage extends IExaminationsPage {

    @SpringBean
    private ExamRegistrationService examRegistrationService;
    @SpringBean
    ModulePaperService modulePaperService;

    public ExamRegistrationCommentPage(final Long id) {
        CompoundPropertyModel<ExamRegistration> model =
                new CompoundPropertyModel<ExamRegistration>(
                new ExamRegistrationCommentPage.LoadableDetachableExamRegistrationModel(id));

        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);

        final Long registrantId = model.getObject().getRegistration().getRegistrant().getId();

        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantId);
        setDefaultModel(model);

        Form<ExamRegistration> form = new Form<ExamRegistration>("form", (IModel<ExamRegistration>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                examRegistrationService.save(getModelObject());
                setResponsePage(new RegistrantExamResultPage(id));
            }
        };

        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantExamResultPage(id));
            }
        });
        form.add(new TextField<String>("passComment").setRequired(true).add(new ErrorBehavior()));
        add(form);
        add(new Label("candidateNumber"));
        add(new FeedbackPanel("feedback"));
    }

    private final class LoadableDetachableExamRegistrationModel extends LoadableDetachableModel<ExamRegistration> {

        private Long id;

        public LoadableDetachableExamRegistrationModel(Long id) {
            this.id = id;
        }

        @Override
        protected ExamRegistration load() {
            return examRegistrationService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
