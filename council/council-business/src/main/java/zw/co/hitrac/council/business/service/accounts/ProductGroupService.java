/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.accounts;

import zw.co.hitrac.council.business.domain.accounts.ProductGroup;
import zw.co.hitrac.council.business.service.IGenericService;

/**
 *
 * @author tidza
 */
public interface ProductGroupService extends IGenericService<ProductGroup> {
    
}
