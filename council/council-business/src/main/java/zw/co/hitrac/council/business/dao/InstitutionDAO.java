/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao;

import java.util.List;
import zw.co.hitrac.council.business.domain.City;
import zw.co.hitrac.council.business.domain.District;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.InstitutionCategory;
import zw.co.hitrac.council.business.domain.InstitutionType;
import zw.co.hitrac.council.business.domain.Province;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.Customer;

/**
 *
 * @author tidza
 */
public interface InstitutionDAO extends IGenericDAO<Institution> {
    public Institution getInstitutionByAccount(Customer customer);
    public List<Institution> getAll(Boolean academic, Boolean school,Boolean council);
    public List<Institution> getInstitutionInDistrict(District district);
    public Institution getInstitution(Customer customer);
    public List<Institution> getInstitutions(InstitutionType institutionType);

    public List<Institution> getInstitutions(String code, InstitutionType institutionType, InstitutionCategory institutionCategory, Boolean retired);
    public void removeDuplicates(Institution institution, List<Institution> institutions);
    public void retire(List<Institution> institutions);

    public Boolean checkInstitutionSupervisor(Registrant registrant);    
    public Institution getInstitutionBySupervisor(Registrant registrant);
    public List<Institution> getInstitutionByPractitionerInCharge(Registrant registrant);
    public Long getTotalCount(Province province, District district, City city, InstitutionType institutionType, InstitutionCategory institutionCategory);
    public void createInstitutionAccounts();
}

