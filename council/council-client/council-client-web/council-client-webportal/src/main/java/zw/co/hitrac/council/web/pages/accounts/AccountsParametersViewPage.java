package zw.co.hitrac.council.web.pages.accounts;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.AccountsParameters;
import zw.co.hitrac.council.business.service.accounts.AccountsParametersService;
import zw.co.hitrac.council.web.pages.configure.AdministerDatabasePage;

/**
 *
 * @author Matiashe Michael
 */
public class AccountsParametersViewPage extends IGeneralLedgerPage {

    @SpringBean
    private AccountsParametersService accountsParametersService;

    public AccountsParametersViewPage() {
        CompoundPropertyModel<AccountsParameters> model = new CompoundPropertyModel<AccountsParameters>(new LoadableDetachableAccountsParametersModel());
        setDefaultModel(model);
        add(new Link<AccountsParameters>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new AccountsParametersEditPage());
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Label("invoiceTransactionType"));
        add(new Label("manualTransactionType"));        
        add(new Label("manualPaymentMethod"));        
        add(new Label("creditNoteTransactionType"));
        add(new Label("refundTransactionType"));
        add(new Label("defaultPaymentMethod"));
        add(new Label("carryForwardTransactionType"));
        add(new Label("studentRegistrationPayment"));
        add(new Label("qualificationFee"));
        add(new Label("registrationApplicationPayment"));
        add(new Label("practisingCertificatePayment"));
        add(new Label("payingPractitionersMaximumAge"));
        add(new Label("payingMaximumAge"));
        add(new Label("printPDFReceipt"));
        add(new Label("allowCashOverpayment"));
        add(new Label("allowAccruals"));
        

    }

    private final class LoadableDetachableAccountsParametersModel extends LoadableDetachableModel<AccountsParameters> {

        @Override
        protected AccountsParameters load() {

            return accountsParametersService.get();
        }
    }
}
