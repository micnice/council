package zw.co.hitrac.council.business.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.CourseCertificateNumberDAO;
import zw.co.hitrac.council.business.dao.repo.CourseCertificateNumberRepository;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.CourseCertificateNumber;
import zw.co.hitrac.council.business.domain.Register;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 *
 * @author Charles Chigoriwa
 */
@Repository
public class CourseCertificateNumberDAOImpl implements CourseCertificateNumberDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private CourseCertificateNumberRepository courseCertificateNumberRepository;

    public CourseCertificateNumber save(CourseCertificateNumber courseCertificateNumber) {
        return courseCertificateNumberRepository.save(courseCertificateNumber);
    }

    public CourseCertificateNumber get(Long id) {
        return courseCertificateNumberRepository.findOne(id);
    }

    /**
     * A setter method that will make mocking repo object easier
     *
     * @param courseCertificateNumberRepository
     */
    public void setCourseCertificateNumberRepository(CourseCertificateNumberRepository courseCertificateNumberRepository) {
        this.courseCertificateNumberRepository = courseCertificateNumberRepository;
    }

    @Override
    public List<CourseCertificateNumber> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public CourseCertificateNumber get(Course course) {

        return (CourseCertificateNumber) entityManager.createQuery("select c from CourseCertificateNumber c where c.course=:course and c.register IS NULL").setParameter("course", course).getSingleResult();
    }

    @Override
    public CourseCertificateNumber get(Course course, Register register) {
        if (register == null) {
            return get(course);
        }
        return (CourseCertificateNumber) entityManager.createQuery("select c from CourseCertificateNumber c where c.course=:course and c.register=:register").setParameter("course", course).setParameter("register", register).getSingleResult();
    }

    @Override
    public List<CourseCertificateNumber> findAll() {
        return courseCertificateNumberRepository.findAll();
    }

    @Override
    public void resetCertificateNumbers() {
        for (CourseCertificateNumber ccn : findAll()) {
            ccn.setCertificateNumber(0L);
            save(ccn);
        }
    }

}
