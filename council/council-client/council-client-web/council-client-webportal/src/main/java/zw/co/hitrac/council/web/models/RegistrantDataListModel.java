package zw.co.hitrac.council.web.models;

import java.util.List;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.RegistrantService;

/**
 *
 * @author Matiashe Michael
 */
public class RegistrantDataListModel extends LoadableDetachableModel<List<RegistrantData>> {
    
    private final RegistrantService registrantService;

    public RegistrantDataListModel(RegistrantService registrantService) {
        this.registrantService = registrantService;
    }

    @Override
    protected List<RegistrantData> load() {
      return this.registrantService.getRegistrants();
    }
    
}
