package zw.co.hitrac.council.web.pages.accounts.payments;

import zw.co.hitrac.council.business.service.CouncilDurationService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.accounts.*;
import zw.co.hitrac.council.web.models.CouncilDurationListModel;
import zw.co.hitrac.council.web.pages.accounts.documents.DebtComponentsPage;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.domain.accounts.PaymentDetails;
import zw.co.hitrac.council.business.domain.accounts.Prepayment;
import zw.co.hitrac.council.business.domain.accounts.PrepaymentDetails;
import zw.co.hitrac.council.business.domain.accounts.ReceiptItem;
import zw.co.hitrac.council.business.domain.accounts.TransactionType;
import zw.co.hitrac.council.business.process.InvoiceProcess;
import zw.co.hitrac.council.business.process.PaymentProcess;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.PrepaymentListModel;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.SubmitOnceForm;

/**
 * @author Michael Matiashe
 */
public class PrepaymentPage extends TemplatePage {

    @SpringBean
    private PrepaymentDetailsService prepaymentDetailsService;
    @SpringBean
    private PrepaymentService prepaymentService;
    @SpringBean
    private PaymentProcess paymentProcess;
    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private AccountsParametersService accountsParametersService;
    @SpringBean
    private InvoiceProcess invoiceProcess;
    @SpringBean
    private ReceiptItemService receiptItemService;
    @SpringBean
    private CouncilDurationService councilDurationService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private PaymentDetailsService paymentDetailsService;

    private Institution institution;
    private List<ReceiptItem> receiptItems = new ArrayList<ReceiptItem>();
    private Set<ReceiptItem> receiptItemList = new HashSet<ReceiptItem>();

    public PrepaymentPage(final Customer customer, final List<DebtComponent> debtComponents) {
        initReceiptItems(debtComponents);
        PrepaymentDetails prepaymentDetails = new PrepaymentDetails(customer);
        setDefaultModel(new CompoundPropertyModel<PrepaymentDetails>(prepaymentDetails));
        Form<PrepaymentDetails> form = new SubmitOnceForm<PrepaymentDetails>("form", (IModel<PrepaymentDetails>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    if (CouncilSession.get().getUser() == null) {
                        error("Please Logout and Login - Session Timed Out");
                    }

                    TransactionType transactionType = getModelObject().getPrepayment().getTransactionType();
                    paymentProcess.payThroughInstitution(getModelObject().getPrepayment(), transactionType, debtComponents);

                    receiptItems = receiptItemService.getReceiptItemsWithNoReceiptHeader(customer.getAccount());
                    for (ReceiptItem receiptItem : receiptItems) {
                        if (receiptItem.getPaymentPeriod() == null) {
                            receiptItem.setPaymentPeriod(getModelObject().getCouncilDuration());
                        }
                        receiptItemList.add(receiptItem);
                    }
                    PaymentDetails paymentDetails = invoiceProcess.payThroughInstitution(getModelObject().getPrepayment(), receiptItemList, CouncilSession.get().getUser(), customer, institution);
                    paymentDetails.setCouncilDuration(getModelObject().getCouncilDuration());
                    paymentDetailsService.save(paymentDetails);
                    getModelObject().setPaymentDetails(paymentDetails);
                    getModelObject().setCreatedBy(CouncilSession.get().getUser());

                    prepaymentDetailsService.save(getModelObject());
                    setResponsePage(new PaymentConfirmationPage(paymentDetails, customer.getAccount()));

                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }

            @Override
            protected void onRepeatSubmit() {
                PrepaymentDetails prepaymentDetails = this.getModelObject();
                setResponsePage(new PaymentConfirmationPage(prepaymentDetails.getPaymentDetails(), customer.getAccount()));
            }
        };

        if (generalParametersService.get().getShowActiveDurationsOnly()) {
            form.add(new DropDownChoice("councilDuration", councilDurationService.getActiveCouncilDurations()));
        } else {
            form.add(new DropDownChoice("councilDuration", councilDurationService.findAll()));
        }

        DropDownChoice<Institution> institutions = new DropDownChoice<Institution>("institution",
                new PropertyModel<Institution>(this, "institution"), new PrepaymentPage.InstitutionModel(),
                new ChoiceRenderer<Institution>("name", "id")) {
            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }

            @Override
            protected void onSelectionChanged(Institution newSelection) {
                PrepaymentDetails prepaymentDetails = (PrepaymentDetails) PrepaymentPage.this.getDefaultModelObject();
                prepaymentDetails.setPrepayment(null);
            }
        };

        institutions.setRequired(true).add(new ErrorBehavior());
        form.add(institutions);

        DropDownChoice<Prepayment> prepayments = new DropDownChoice<Prepayment>("prepayment", new PrepaymentPage.PrepaymentModel());
        form.add(prepayments);

        add(form);
        form.add(new ReceiptItemListView("receiptItems", new LoadableDetachableModel<List<? extends ReceiptItem>>() {
            @Override
            protected List<? extends ReceiptItem> load() {
                return receiptItems;
            }
        }));

        BigDecimal totalAmountDue = BigDecimal.ZERO;
        for (ReceiptItem item : receiptItems) {
            totalAmountDue = totalAmountDue.add(item.getDebtComponent().getRemainingBalance());
        }

        add(new FeedbackPanel("feedback"));
        add(new Label("customer.customerName"));
        add(new Label("totalAmountDue", totalAmountDue));
        form.add(new Link<Void>("debtComponentPage") {
            @Override
            public void onClick() {
                setResponsePage(new DebtComponentsPage(registrantService.getRegistrant(customer).getId()));
            }
        });
    }

    private final class ReceiptItemListView extends ListView<ReceiptItem> {

        public ReceiptItemListView(String id) {
            super(id);
            setReuseItems(true);
        }

        public ReceiptItemListView(String id, IModel<? extends List<? extends ReceiptItem>> model) {
            super(id, model);
            setReuseItems(true);
        }

        public ReceiptItemListView(String id, List<? extends ReceiptItem> list) {
            super(id, list);
            setReuseItems(true);
        }

        @Override
        protected void populateItem(final ListItem<ReceiptItem> item) {
            item.setDefaultModel(new CompoundPropertyModel<ReceiptItem>(item.getModelObject()));
            item.add(new TextField<BigDecimal>("amount").setRequired(true).add(new ErrorBehavior()));
            item.add(new DropDownChoice("paymentPeriod", new CouncilDurationListModel(councilDurationService)));
            item.add(new Label("debtComponent.remainingBalance"));
            item.add(new Label("debtComponent.transactionComponent.account.name"));
        }
    }

    private void initReceiptItems(List<DebtComponent> debtComponents) {
        for (DebtComponent debtComponent : debtComponents) {
            ReceiptItem receiptItem = new ReceiptItem();
            receiptItem.setDebtComponent(debtComponent);
            receiptItem.setAmount(debtComponent.getRemainingBalance());
            receiptItem.setCreatedBy(CouncilSession.get().getUser());
            receiptItem.setDateCreated(new Date());
            receiptItems.add(receiptItem);
        }
    }

    private class PrepaymentModel extends LoadableDetachableModel<List<? extends Prepayment>> {

        protected List<? extends Prepayment> load() {
            if (institution != null) {
                return new PrepaymentListModel(prepaymentService, institution).getObject();
            } else {
                return new ArrayList<Prepayment>();
            }
        }
    }

    private final class InstitutionModel extends LoadableDetachableModel<List<? extends Institution>> {

        protected List<? extends Institution> load() {
            return prepaymentService.withPrepayments();
        }
    }
}
