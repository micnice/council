package zw.co.hitrac.council.web.pages.accounts;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.ProductPrice;
import zw.co.hitrac.council.business.service.accounts.ProductPriceService;

import java.util.List;

/**
 *
 * @author Charles Chigoriwa
 */
public class ProductPriceListPage extends IInventoryModulePage {

    @SpringBean
    private ProductPriceService productPriceService;

    public ProductPriceListPage() {
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new ProductPriceEditPage(null));
            }
        });

        IModel<List<ProductPrice>> model = new LoadableDetachableModel<List<ProductPrice>>() {
            @Override
            protected List<ProductPrice> load() {
                return productPriceService.findAll();
            }
        };

        PropertyListView<ProductPrice> eachItem = new PropertyListView<ProductPrice>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<ProductPrice> item) {
                Link<ProductPrice> viewLink = new Link<ProductPrice>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new ProductPriceViewPage(getModelObject().getId()));
                    }
                };
                item.add(viewLink);
                viewLink.add(new Label("product.name"));
                item.add(new Label( "product.register"));
                item.add(new Label("price"));
            }
        };

        add(eachItem);
    }
}
