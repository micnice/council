/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.EmploymentStatus;
import zw.co.hitrac.council.business.service.EmploymentStatusService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author kelvin
 */
public class EmploymentStatusEditPage extends IAdministerDatabaseBasePage{
    
    @SpringBean
    private EmploymentStatusService employmentStatusService;

    public EmploymentStatusEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<EmploymentStatus>(new EmploymentStatusEditPage.LoadableDetachableEmploymentStatusModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink",AdministerDatabasePage.class));
        Form<EmploymentStatus> form=new Form<EmploymentStatus>("form",(IModel<EmploymentStatus>)getDefaultModel()) {
            @Override
            public void onSubmit(){
                employmentStatusService.save(getModelObject());
                setResponsePage(new EmploymentStatusViewPage(getModelObject().getId()));
            }
        };
        
        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior())); 
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink",EmploymentStatusListPage.class));
        add(form);
    }
    
    
    private final class LoadableDetachableEmploymentStatusModel extends LoadableDetachableModel<EmploymentStatus> {

        private Long id;

        public LoadableDetachableEmploymentStatusModel(Long id) {
            this.id = id;
        }

        @Override
        protected EmploymentStatus load() {
            if(id==null){
                return new EmploymentStatus();
            }
            return employmentStatusService.get(id);
        }
    }
    
    
}

    

