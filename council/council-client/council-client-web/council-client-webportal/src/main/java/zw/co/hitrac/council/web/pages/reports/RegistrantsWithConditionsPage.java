package zw.co.hitrac.council.web.pages.reports;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;
import org.apache.wicket.PageReference;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.CouncilDurationService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.reports.DefaultReport;
import zw.co.hitrac.council.reports.RegistrantsWithConditionsDetailedReport;
import zw.co.hitrac.council.reports.RegistrantsWithConditionsSummaryReport;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

import java.util.*;
import zw.co.hitrac.council.reports.utils.ContentType;

/**
 * Created by clive on 6/30/15.
 */
public class RegistrantsWithConditionsPage extends TemplatePage {

    @SpringBean
    private CouncilDurationService councilDurationService;
    @SpringBean
    private RegistrantService registrantService;
    private CouncilDuration councilDuration;
    private ContentType contentType;

    public RegistrantsWithConditionsPage(final PageReference pageReference) {

        menuPanelManager.getSearchPanel().setTopMenuCurrent(true);
        menuPanelManager.getSearchPanel().setSearchAdvanced(true);

        PropertyModel<CouncilDuration> councilDurationPropertyModel = new PropertyModel<CouncilDuration>(this, "councilDuration");
        PropertyModel<ContentType> contentTypeModel = new PropertyModel<ContentType>(this, "contentType");
        Form<?> form = new Form("form");
        form.add(new DropDownChoice("councilDuration", councilDurationPropertyModel, councilDurationService.findAll()).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("contentType", contentTypeModel, Arrays.asList(ContentType.values())));
        add(form);

        form.add(new Link("returnLink") {

            public void onClick() {

                setResponsePage(pageReference.getPage());
            }
        });

        form.add(new Button("showDetailedList") {

            @Override
            public void onSubmit() {
                boolean detailedReport = true;
                showRegistrantsWithConditionsReport(detailedReport);
            }
        });

        form.add(new Button("showSummaryTotals") {

            @Override
            public void onSubmit() {

                boolean detailedReport = false;
                showRegistrantsWithConditionsReport(detailedReport);
            }
        });

        add(new FeedbackPanel("feedback"));

    }

    private void showRegistrantsWithConditionsReport(boolean showDetailedReport) {

        Map parameters = new HashMap();
        List<RegistrantData> registrantDataList = registrantService.getRegistrantsWithConditions(councilDuration);

        Collections.sort(registrantDataList, new Comparator<RegistrantData>() {

            public int compare(RegistrantData o1, RegistrantData o2) {

                return ComparisonChain.start()
                        .compare(o1.getConditionName(),
                                o2.getConditionName(), Ordering.natural().nullsLast())
                        .compare(o1.getFullName(),
                                o2.getFullName(), Ordering.natural().nullsLast())
                        .result();

            }
        });

        DefaultReport report;

        if (showDetailedReport) {

            report = new RegistrantsWithConditionsDetailedReport();
            parameters.put("comment", "Detailed Register for practitioners with conditions for "
                    + councilDuration.getName());

        } else {

            report = new RegistrantsWithConditionsSummaryReport();
            parameters.put("comment", "Summary Register for practitioners with conditions for "
                    + councilDuration.getName());

        }

        try {

            ReportResourceUtils.processJavaBeanReport(report, registrantDataList, parameters);

        } catch (Exception e) {

            e.printStackTrace(System.err);
        }
    }
}
