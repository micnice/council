package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.AddressType;
import zw.co.hitrac.council.business.service.AddressTypeService;

/**
 *
 * @author charlesc
 */
public class AddressTypeViewPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private AddressTypeService addressTypeService;

    public AddressTypeViewPage(Long id) {
        CompoundPropertyModel<AddressType> model=new CompoundPropertyModel<AddressType>(new LoadableDetachableAddressTypeModel(id));
        setDefaultModel(model);
        add(new Link<AddressType>("editLink",model){

            @Override
            public void onClick() {
                setResponsePage(new AddressTypeEditPage(getModelObject().getId()));
            }
            
        });
        add(new BookmarkablePageLink<Void>("returnLink", AddressTypeListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("addressType",model.bind("name")));
    }


    private final class LoadableDetachableAddressTypeModel extends LoadableDetachableModel<AddressType> {

        private Long id;

        public LoadableDetachableAddressTypeModel(Long id) {
            this.id = id;
        }

        @Override
        protected AddressType load() {

            return addressTypeService.get(id);
        }
    }
}
