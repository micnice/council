package zw.co.hitrac.council.web.pages.registration;

//~--- non-JDK imports --------------------------------------------------------
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButton;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButtons;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogIcon;
import com.googlecode.wicket.jquery.ui.widget.dialog.MessageDialog;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantQualification;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrantQualificationService;
import zw.co.hitrac.council.reports.Report;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.utility.CustomDateLabel;
import zw.co.hitrac.council.web.utility.CustomDateTimeLabel;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

import java.util.Collection;
import java.util.Map;

/**
 *
 * @author Michael Matiashe
 */
public class RegistrantQualificationPanelList extends Panel {

    /**
     * Constructor
     *
     * @param id
     */
    @SpringBean
    RegistrantQualificationService registrantQualificationService;
    @SpringBean
    private GeneralParametersService generalParametersService;

    public RegistrantQualificationPanelList(String id) {
        super(id);

        // - Used to Display a to list Registrations
        add(new PropertyListView<RegistrantQualification>("registrantQualificationList") {
            @Override
            protected void populateItem(final ListItem<RegistrantQualification> item) {
                Link<RegistrantQualification> viewLink =
                        new Link<RegistrantQualification>("registrantQualificationPage", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new RegistrantQualificationViewPage(getModelObject().getId()));
                    }
                };

                item.add(viewLink);
                item.add(new CustomDateLabel("dateAwarded"));
                item.add(new Label("institution"));
                item.add(new Label("awardingInstitution"));
                item.add(new Label("qualification"));
                item.add(new Label("qualificationType"));
                item.add(new Label("course"));
                item.add(new Label("createdBy"));
                item.add(new Label("modifiedBy"));
                item.add(CustomDateTimeLabel.forDate("dateCreated",
                        new PropertyModel<>(item.getModel(), "dateCreated")));
                item.add(CustomDateTimeLabel.forDate("dateModified",
                        new PropertyModel<>(item.getModel(), "dateModified")));

                final Long documentId = item.getModelObject().getId();
                final MessageDialog warningDialog = new MessageDialog("warningDialog", "Warning", "Are you sure you want to remove qualification?", DialogButtons.YES_NO, DialogIcon.WARN) {
                    private static final long serialVersionUID = 1L;

                    @Override
                    public void onClose(AjaxRequestTarget target, DialogButton button) {
                        if (button != null && button.equals(LBL_YES)) {
                            try {
                                RegistrantQualification su = registrantQualificationService.get(documentId);
                                su.setRetired(Boolean.TRUE);
                                registrantQualificationService.save(su);
                                setResponsePage(getPage());
                            } catch (Exception ex) {
                                ex.printStackTrace(System.out);
                            }
                        }
                    }
                };

                item.add(warningDialog);

                item.add(new AjaxLink("remove") {
                    private static final long serialVersionUID = 1L;

                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        warningDialog.open(target);
                    }
                });

                item.setVisible(!item.getModelObject().getRetired());

            }
        });
    }

    public RegistrantQualificationPanelList(String id, final IModel<Registrant> registrantModel) {
        this(id);
        // method to add a new Registration - Id is null
        add(new Link<Registrant>("addLocalQualification") {
            @Override
            public void onClick() {
                if (!generalParametersService.get().getHasProcessedByBoard()) {
                    setResponsePage(new RegistrantQualificationCoursePage(registrantModel, Boolean.FALSE));
                } else {
                    setResponsePage(new RegistrantQualificationPage(registrantModel, Boolean.FALSE));
                }
            }
        });

        add(new Link<Registrant>("addForeignQualification") {
            @Override
            public void onClick() {
                if (!generalParametersService.get().getHasProcessedByBoard()) {
                    setResponsePage(new RegistrantQualificationCoursePage(registrantModel, Boolean.TRUE));
                } else {
                    setResponsePage(new RegistrantQualificationPage(registrantModel, Boolean.TRUE));
                }
            }
        });

    }

    protected void processReport(Report report, ContentType contentType, Collection<?> collection,
            Map<String, Object> parameters)
            throws JRException {
        ByteArrayResource resource = ReportResourceUtils.getReportResource(report, contentType, collection,
                parameters);
        IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                RequestCycle.get().getResponse(), null);

        resource.respond(a);

        // To make Wicket stop processing form after sending response
        RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
