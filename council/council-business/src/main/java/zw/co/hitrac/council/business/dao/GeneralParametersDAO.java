package zw.co.hitrac.council.business.dao;

import zw.co.hitrac.council.business.domain.GeneralParameters;
import zw.co.hitrac.council.business.domain.RequiredParameter;

/**
 *
 * @author Edawrd Zengeni
 */
public interface GeneralParametersDAO extends IGenericDAO<GeneralParameters> {

    
}
