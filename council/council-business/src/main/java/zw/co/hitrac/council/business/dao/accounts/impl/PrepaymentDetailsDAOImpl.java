package zw.co.hitrac.council.business.dao.accounts.impl;

import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.accounts.PrepaymentDetailsDAO;
import zw.co.hitrac.council.business.dao.repo.accounts.PrepaymentDetailsRepository;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.accounts.PaymentDetails;
import zw.co.hitrac.council.business.domain.accounts.Prepayment;
import zw.co.hitrac.council.business.domain.accounts.PrepaymentDetails;

/**
 *
 * @author Michael Matiashe
 */
@Repository
public class PrepaymentDetailsDAOImpl implements PrepaymentDetailsDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private PrepaymentDetailsRepository prepaymentDetailsRepository;

    public PrepaymentDetails save(PrepaymentDetails t) {
        return prepaymentDetailsRepository.save(t);
    }

    public List<PrepaymentDetails> findAll() {
        return prepaymentDetailsRepository.findAll();
    }

    public PrepaymentDetails get(Long id) {
        return prepaymentDetailsRepository.findOne(id);
    }

    public PrepaymentDetailsRepository getPrepaymentDetailsRepository() {
        return prepaymentDetailsRepository;
    }

    public void setPrepaymentDetailsRepository(PrepaymentDetailsRepository prepaymentDetailsRepository) {
        this.prepaymentDetailsRepository = prepaymentDetailsRepository;
    }

    public List<PrepaymentDetails> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<PrepaymentDetails> findByUid(String uid) {
        return prepaymentDetailsRepository.findByUid(uid);
    }

    public PrepaymentDetails findByPaymentDetails(PaymentDetails paymentDetails) {
        List<PrepaymentDetails> pds = entityManager.createQuery("SELECT p FROM PrepaymentDetails p  WHERE p.paymentDetails=:paymentDetails and p.cancelled=false").setParameter("paymentDetails", paymentDetails).getResultList();
        if (!pds.isEmpty()) {
            return pds.get(0);
        } else {
            return null;
        }
    }

    public List<PrepaymentDetails> findByPrepayment(Prepayment prepayment) {
        return entityManager.createQuery("SELECT  p from PrepaymentDetails p WHERE p.prepayment=:prepayment and p.cancelled=FALSE").setParameter("prepayment", prepayment).getResultList();
    }

    @Override
    public List<PrepaymentDetails> findByCustomerAndCancelled(Customer customer, Boolean cancelled) {
        return entityManager.createQuery("SELECT p from PrepaymentDetails p WHERE p.customer=:customer and p.cancelled=:cancelled").setParameter("customer", customer).setParameter("cancelled", cancelled).getResultList();
    }

    public List<PaymentDetails> findReceiptHeaders(Prepayment prepayment) {
        return entityManager.createQuery("SELECT  p.paymentDetails from PrepaymentDetails p WHERE p.prepayment=:prepayment").setParameter("prepayment", prepayment).getResultList();
    }

    @Override
    public List<PrepaymentDetails> findAllModifiedByDates(Date startDate, Date endDate) {
        return entityManager.createQuery("SELECT p FROM PrepaymentDetails p WHERE p.dateModified!=null and p.dateModified BETWEEN :startDate and :endDate and p.paymentDetails.receiptHeader.reversed =FALSE").setParameter("startDate", startDate).setParameter("endDate", endDate).getResultList();
    }

    @Override
    public List<PrepaymentDetails> findAllByDates(Date startDate, Date endDate) {
        return entityManager.createQuery("SELECT p FROM PrepaymentDetails p WHERE p.dateModified!=null and p.paymentDetails.receiptHeader.date BETWEEN :startDate and :endDate and p.paymentDetails.receiptHeader.reversed =FALSE").setParameter("startDate", startDate).setParameter("endDate", endDate).getResultList();
    }
}
