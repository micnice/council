package zw.co.hitrac.council.business.dao;

import zw.co.hitrac.council.business.domain.RequiredParameter;
import zw.co.hitrac.council.business.service.IGenericService;

/**
 * Created by tndangana on 2/13/17.
 */
public interface RequiredParameterDAO extends IGenericDAO<RequiredParameter> {

}
