package zw.co.hitrac.council.web.menu;

import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import zw.co.hitrac.council.web.pages.registration.RegistrationViewPage;

/**
 *
 * @author Takunda Dhlakama
 * @author Edward Zengeni
 */
public class RegistrantMenuPanel extends MenuPanel {

    private Link<Void> viewRegistrantLink;

    public RegistrantMenuPanel(String id) {
        super(id);
        viewRegistrantLink = new BookmarkablePageLink<Void>("viewRegistrantLink", RegistrationViewPage.class);
        add(viewRegistrantLink);
    }

    @Override
    protected void onConfigure() {
        super.onConfigure();
        this.setVisibilityAllowed(topMenuCurrent);
        addCurrentBehavior(viewRegistrantLink, topMenuCurrent);
    }
   
}
