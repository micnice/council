package zw.co.hitrac.council.web.pages;

//~--- non-JDK imports --------------------------------------------------------
import com.googlecode.wicket.jquery.core.JQueryBehavior;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.authroles.authentication.AuthenticatedWebApplication;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.GeneralParameters;
import zw.co.hitrac.council.business.service.ApplicationService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrantConditionService;
import zw.co.hitrac.council.business.utils.StringFormattingUtil;
import zw.co.hitrac.council.web.config.CouncilApplication;
import zw.co.hitrac.council.web.menu.MenuPanelManager;
import zw.co.hitrac.council.web.models.VersionReadOnlyModel;
import zw.co.hitrac.council.web.pages.application.FiltedApplicationPendingPage;
import zw.co.hitrac.council.web.pages.registration.ConditionsPage;
import zw.co.hitrac.council.web.pages.research.MyDynamicImageResource;
import zw.co.hitrac.council.web.pages.security.SignOutPage;

/**
 * Template Page for all the system pages.
 *
 * @author Charles Chigoriwa
 */
public abstract class TemplatePage extends WebPage {

    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private ApplicationService applicationService;
    @SpringBean
    private RegistrantConditionService registrantConditionService;
    protected final MenuPanelManager menuPanelManager;
    private String councilName;

    public String getCouncilName() {

        return councilName;
    }

    public TemplatePage() {
        this.add(new JQueryBehavior("#tabs", "tabs"));
        menuPanelManager = new MenuPanelManager();
        add(menuPanelManager.getRegistrantionPanel());
        add(menuPanelManager.getConfigureMenuPanel());
        add(menuPanelManager.getChangePasswordMenuPanel());
        add(menuPanelManager.getRegistrantMenuPanel());
        add(menuPanelManager.getRegistrantBackMenuPanel());
        add(menuPanelManager.getAccountingPanel());
        add(menuPanelManager.getSearchPanel());
        add(menuPanelManager.getViewReportsPanel());
        add(menuPanelManager.getExamPanel());
        add(menuPanelManager.getApplicationPanel());
        add(menuPanelManager.getInstitutionPanel());
        add(menuPanelManager.getFinanceMenuPanel());

        add(new Label("version", new VersionReadOnlyModel()));

        Integer totalPendingApplications;
        Integer totalExpiredConditions;

        GeneralParameters generalParameters = generalParametersService.get();
        this.councilName = generalParameters.getCouncilName();
        this.councilName = StringUtils.isEmpty(councilName) ? "Council" : councilName;

        if (generalParameters.getHasProcessedByBoard()) {
            totalPendingApplications = applicationService.getTotalApplicationsByStatus(Application.APPLICATIONPENDING);
        } else {
            totalPendingApplications = applicationService.getNumberOfPendingApplications();
        }

        totalExpiredConditions = registrantConditionService.getNumberOfConditionsExpired();

        String pendingAlert = StringFormattingUtil.join(totalPendingApplications, "Pending Applications");
        String conditionAlert = StringFormattingUtil.join(totalExpiredConditions, "Expired Conditions");


        add(new Label("displayCouncilName", this.councilName));
        add(new Image("siteLogoImage", new MyDynamicImageResource(generalParametersService)));
        add(new BookmarkablePageLink<Void>("syslogout", SignOutPage.class));
        Link<Void> link = new Link<Void>("pending") {
            @Override
            public void onClick() {
                setResponsePage(new FiltedApplicationPendingPage());
            }
        };
        link.add(new Label("pendingApplicationLabel", pendingAlert));
        Link<Void> expiredConditionsLink = new Link<Void>("expired") {
            @Override
            public void onClick() {
                setResponsePage(new ConditionsPage());
            }
        };
        add(link);
        expiredConditionsLink.add(new Label("exipiredConditionsLabel", conditionAlert));
        add(expiredConditionsLink);

    }

    public MenuPanelManager getMenuPanelManager() {
        return menuPanelManager;
    }

    @Override
    protected void onConfigure() {

        super.onConfigure();

        AuthenticatedWebSession session = AuthenticatedWebSession.get();

        if ((session == null) || !session.isSignedIn() || session.isTemporary() || session.isSessionInvalidated()) {

            AuthenticatedWebApplication authenticatedWebApplication
                = (AuthenticatedWebApplication) CouncilApplication.get();

            authenticatedWebApplication.restartResponseAtSignInPage();
        }
    }   
    
}


//~ Formatted by Jindent --- http://www.jindent.com
