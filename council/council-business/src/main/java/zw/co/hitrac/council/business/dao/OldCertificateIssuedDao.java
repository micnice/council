/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao;

import java.util.List;
import zw.co.hitrac.council.business.domain.OldCertificateIssued;
import zw.co.hitrac.council.business.domain.Registrant;

/**
 *
 * @author kelvin
 */
public interface OldCertificateIssuedDao extends IGenericDAO<OldCertificateIssued>{
    public List<OldCertificateIssued> getCertificates(Registrant registrant);
    
    public OldCertificateIssued getCertificate(Long certificateProcessRecordID);
}
