package zw.co.hitrac.council.web.pages.application;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.extensions.yui.calendar.DatePicker;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.ApplicationPurpose;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.process.ApplicationProcess;
import zw.co.hitrac.council.business.service.ApplicationService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.CustomDateTextField;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

//~--- JDK imports ------------------------------------------------------------

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.TransferRuleService;
import zw.co.hitrac.council.business.service.UserService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.models.TransferRuleListModel;
import zw.co.hitrac.council.web.pages.accounts.documents.DebtComponentsPage;
import zw.co.hitrac.council.web.pages.registration.RegistrantPersonalConditionPage;
import zw.co.hitrac.council.web.pages.registration.RegistrantViewPage;
import zw.co.hitrac.council.web.pages.registration.RequirementConfirmationPage;
import zw.co.hitrac.council.web.pages.registration.TransferPage;

/**
 *
 * @author Michael Matiashe
 * @author Kelvin Goredema
 */
public class TransferApplicationPage extends TemplatePage {

    @SpringBean
    private ApplicationProcess applicationProcess;
    @SpringBean
    private ApplicationService applicationService;
    @SpringBean
    private RegistrationService registrationService;
    @SpringBean
    private TransferRuleService transferRuleService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private UserService userService;
    @SpringBean
    private RegistrationProcess registrationProcess;
    @SpringBean
    private DebtComponentService debtComponentService;
    private Boolean show;

    public TransferApplicationPage(final IModel<Registrant> registrantModel) {
        menuPanelManager.getApplicationPanel().setTopMenuCurrent(true);

        show = generalParametersService.get().getRegistrationByApplication();
        CompoundPropertyModel<Application> model = new CompoundPropertyModel<Application>(new Application());

        setDefaultModel(model);
        model.getObject().setRegistrant(registrantModel.getObject());
        model.getObject().setApplicationDate(new Date());
        model.getObject().setApplicationPurpose(ApplicationPurpose.TRANSFER);

        Registration r = registrationProcess.activeRegisterNotStudentRegister(registrantModel.getObject());

        if (r != null) {
            model.getObject().setCourse(r.getCourse());
            model.getObject().setApprovedCourse(r.getCourse());
        }

        Form<Application> form = new Form<Application>("form", (IModel<Application>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    getModelObject().setCreatedBy(CouncilSession.get().getUser());
                    applicationProcess.apply(getModelObject());
                    if (generalParametersService.get().getRegistrationByApplication()) {
                        setResponsePage(new RegistrantPersonalConditionPage(registrantModel, Boolean.FALSE));
                    } else {
                        setResponsePage(new RequirementConfirmationPage(getModelObject()));
                    }
                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };
        form.add(new Label("approvedByLabel", "Decision By") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new DropDownChoice("approvedBy", userService.getAuthorisers()) {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new Label("processedByLabel", "Prossesed By") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new DropDownChoice("processedBy", userService.getAuthorisers()) {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });

        form.add(new Label("dateApprovedLabel", "Decision Date") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new CustomDateTextField("dateApproved") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new Label("dateProcessedLabel", "Final Processing Date") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new CustomDateTextField("dateProcessed") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });

        form.add(new DropDownChoice("applicationStatus", Application.getApplicationStates) {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new DropDownChoice("transferRule", new TransferRuleListModel(transferRuleService)).setRequired(true).add(new ErrorBehavior()));
        form.add(new Label("applicationPurposeLabel", model.getObject().getApplicationPurpose()));
        form.add(new CustomDateTextField("applicationDate").setRequired(true).add(new ErrorBehavior()).add(new DatePicker()));
        form.add(new Label("commentLabel", "Comment") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new TextArea("comment") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new Label("applicationStatusLabel", "Application Decision") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.FALSE);
            }
        });
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {

                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
            }
        });
        add(form);
        add(new Label("registrant.fullname"));
        add(new FeedbackPanel("feedback"));
    }

    public TransferApplicationPage(long id) {
        menuPanelManager.getApplicationPanel().setTopMenuCurrent(true);

        show = generalParametersService.get().getRegistrationByApplication();
        final CompoundPropertyModel<Application> model = new CompoundPropertyModel<Application>(applicationService.get(id));
        setDefaultModel(model);


        Form<Application> form = new Form<Application>("form", (IModel<Application>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    Application application = applicationProcess.approve(getModelObject());
                    List<DebtComponent> debtComponents = debtComponentService.getDebtComponents(application.getRegistrant().getCustomerAccount());
                    if (debtComponents.isEmpty()) {
                        if (generalParametersService.get().getApproveAndRegister()) {
                            setResponsePage(new TransferPage(application));
                        } else {
                            setResponsePage(new TransferApplicationViewPage(application.getId()));
                        }
                    } else {
                        //select items to pay for
                        setResponsePage(new DebtComponentsPage(application.getRegistrant().getId()));
                    }
                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };

        form.add(new Label("approvedByLabel", "Approved By"));
        form.add(new DropDownChoice("approvedBy", userService.getAuthorisers()));
        form.add(new DropDownChoice("processedBy", userService.getApplicationpProcessors()));
        form.add(new Label("dateApprovedLabel", "Decision Date"));
        form.add(new Label("dateProcessedLabel", "Final Processing Date"));
        form.add(new Label("processedByLabel", "Prossesed By"));
        form.add(new CustomDateTextField("dateApproved").add(DatePickerUtil.getDatePicker()));
        form.add(new CustomDateTextField("dateProcessed").add(DatePickerUtil.getDatePicker()));

        final List<String> applicationStates = Arrays.asList(new String[]{Application.APPLICATIONPENDING, Application.APPLICATIONAPPROVED,
            Application.APPLICATIONDECLINED, Application.APPLICATION_AWAITING_COUNCIL_APPROVAL, Application.APPLICATIONUNDERREVIEW});

        form.add(new DropDownChoice("applicationStatus", applicationStates).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("transferRule", new TransferRuleListModel(transferRuleService)).setRequired(true).add(new ErrorBehavior()));
        form.add(new Label("applicationPurposeLabel", model.getObject().getApplicationPurpose()));
        form.add(new Label("applicationStatusLabel", "Application Decision"));
        form.add(new CustomDateTextField("applicationDate").setRequired(true).add(new ErrorBehavior()).add(new DatePicker()));
        form.add(new Label("commentLabel", "Comment") {
            @Override
            protected void onConfigure() {
                setVisible(Boolean.TRUE);
            }
        });
        form.add(new TextArea("comment"));
        form.add(new Link<Void>("registrantViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantViewPage(model.getObject().getRegistrant().getId()));
            }
        });
        add(form);
        add(new Label("registrant.fullname"));
        add(new FeedbackPanel("feedback"));
    }
}

//~ Formatted by Jindent --- http://www.jindent.com
