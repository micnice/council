/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao;

import zw.co.hitrac.council.business.domain.*;

import java.util.List;

/**
 * @author tidza
 */
public interface InstitutionManagerDAO extends IGenericDAO<InstitutionManager> {

    List<InstitutionManager> findInstitutionManagers(Registrant registrant, Institution institution);

    boolean checkDuplicate(Institution institution, Registrant registrant, Boolean supervisor, Boolean practitionerInCharge);

}