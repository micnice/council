package zw.co.hitrac.council.web.models;

import java.util.List;
import org.apache.wicket.model.LoadableDetachableModel;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.service.CouncilDurationService;

/**
 *
 * @author Charles Chigoriwa
 */
public class CouncilDurationListModel extends LoadableDetachableModel<List<CouncilDuration>> {

    private final CouncilDurationService councilDurationService;

    public CouncilDurationListModel(CouncilDurationService councilDurationService) {
        this.councilDurationService = councilDurationService;
    }

    @Override
    protected List<CouncilDuration> load() {
        return this.councilDurationService.findAll();
    }

}
