package zw.co.hitrac.council.business.domain;

import org.apache.commons.lang.BooleanUtils;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Edward Zengeni
 * @author Michael Matiashe
 */
@Entity
@Table(name = "generalparameters")
@Audited
public class GeneralParameters extends BaseIdEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String registrantName;
    private String email;
    private String emailPassword;
    private ContactType emailContactType;
    private ContactType businessNumberContactType;
    private ContactType mobileNumberContactType;
    private ContactType faxNumberContactType;
    private Conditions mainRegisterCondition;
    private Register studentRegister;
    private Register mainRegister;
    private Register provisionalRegister;
    private Register institutionRegister;
    private Register specialistRegister;
    private Register internRegister;
    private Register maintenanceRegister;
    private CouncilDuration councilDuration;
    private String councilAbbreviation;
    private String councilName;
    private Institution institution;
    private long version;
    private String content_Url;
    private String content_UserName;
    private String content_Password;
    private Integer minimumRegistrationAge = 0;
    private Integer minimumPeriodToStartPBQ = 0;
    private Integer minimumPeriodForSup = 0;
    private Integer minimumYearsForInstitutionApplication = 0;
    private Integer minimumYearsForCGSApplication = 0;
    private Integer minimumYearsForPBQCGSApplication = 0;
    private Integer minimumYearsInProvisionalRegister = 0;
    private Integer minimumYearsInInternRegister = 0;
    private Boolean examinationModule = Boolean.FALSE;
    private Boolean allowMultipleRegistrations = Boolean.FALSE;
    private Boolean registrationByApplication = Boolean.FALSE;
    private InstitutionType institutionType;
    private Integer lastRegistrationNumber = 0;
    private Integer lastCandidateNumber = 0;
    private Integer lastCertificateNumber = 0; //Diploma
    private Integer lastPracticeCertificateNumber = 0;
    private Integer lastCGSCertificateNumber = 0;
    private Integer numberOfTimesToWriteSupExam = 0;
    private Boolean hasReminderOnApplications = Boolean.FALSE;
    private Integer minimumDaysPendingApplicationsLocal = 0;
    private Integer minimumDaysPendingApplicationsForeign = 0;
    private Integer minimumDaysPendingApplicationsCGS = 0;
    private Integer incrementCertificateNumber = 0;
    private Integer unrestrictedPCPeriod = 0;
    private Boolean hasProcessedByBoard = Boolean.FALSE;
    private Boolean registrationNumberHasPrefix = Boolean.FALSE;
    private Boolean registrationNumberHasZeroPrefix;
    private String councilPrefixForRegistrationNumber;
    private Boolean generateRegnumberOnRegistrant = Boolean.FALSE;
    private InstitutionType healthInstitutionType;
    private Boolean approveAndRegister = Boolean.FALSE;
    private Integer registrationNumberIncrement = 0;
    private Boolean allowPreRegistrantion = Boolean.FALSE;
    private RegistrantStatus suspendedStatus;
    private Boolean registrationnumberIsByCourse;
    private Boolean directApproval = Boolean.FALSE;
    private Boolean requireAffidavit = Boolean.FALSE;
    private Requirement affidavit;
    private Integer monthsToRequireAffidavit;
    private Boolean billingModule = Boolean.TRUE;
    private RegistrantStatus deRegisteredStatus;
    private RegistrantStatus voidedStatus;
    private Boolean reportsByCourse = Boolean.TRUE;
    private AddressType physicalAddressType;
    private AddressType contactAddressType;
    private AddressType businessAddressType;
    private String emailSignature;
    private String integrationUrl;
    private Boolean integrationAllowed = Boolean.FALSE;
    private Boolean directRegistration = Boolean.FALSE;
    private Integer registrationCertificateSerial = 0;
    private Boolean registrationCertificateSerialIsIncrimental = Boolean.FALSE;
    private Boolean courseCertificateNumber = Boolean.FALSE;
    private Boolean registrationNumberByYear = Boolean.FALSE;
    private Boolean examClearance = Boolean.FALSE;
    private Boolean allowSendingBirthDayRemindersToRegistrants = Boolean.FALSE;
    private Boolean billEachPostBasicQualification = Boolean.FALSE;
    private Boolean printJavaScriptReceipt = Boolean.FALSE;
    private Boolean captureCPDsDirectly = Boolean.FALSE;
    private Boolean dynamicDisciplineReport = Boolean.FALSE;
    private Boolean allowRenewalBeforeCPDs = Boolean.FALSE;
    private Boolean allowCapitalizationOfFirstLetters = Boolean.FALSE;
    private Boolean allowChangeOfRegistrationNumberAfterTransfer = Boolean.FALSE;
    private Boolean captureCPDsDirectlyAndInDirectly = Boolean.FALSE;
    private Boolean showActiveDurationsOnly = Boolean.FALSE;
    private Boolean studentRegistrationWithRenewal = Boolean.FALSE;
    private Boolean registerQualificationBeforeTransfer = Boolean.FALSE;
    private Boolean issueDiplomaWithoutExams = Boolean.FALSE;
    private Boolean hasQuaterRenewal;
    private Boolean lastRenewalPeriodShowsEndDate = Boolean.FALSE;


    public Boolean getlastRenewalPeriodShowsEndDate() {
        return BooleanUtils.toBoolean(lastRenewalPeriodShowsEndDate);
    }

    public void setlastRenewalPeriodShowsEndDate(Boolean lastRenewalPeriodShowsEndDate) {
        this.lastRenewalPeriodShowsEndDate = lastRenewalPeriodShowsEndDate;
    }

    public Boolean getHasQuaterRenewal() {
        return BooleanUtils.toBoolean(hasQuaterRenewal);
    }

    public void setHasQuaterRenewal(Boolean hasQuaterRenewal) {
        this.hasQuaterRenewal = hasQuaterRenewal;
    }

    public Boolean getIssueDiplomaWithoutExams() {
        return BooleanUtils.toBoolean(issueDiplomaWithoutExams);
    }

    public void setIssueDiplomaWithoutExams(Boolean issueDiplomaWithoutExams) {
        issueDiplomaWithoutExams = issueDiplomaWithoutExams;
    }

    public Boolean getRegisterQualificationBeforeTransfer() {
        return BooleanUtils.toBoolean(registerQualificationBeforeTransfer);
    }

    public void setRegisterQualificationBeforeTransfer(Boolean registerQualificationBeforeTransfer) {
        this.registerQualificationBeforeTransfer = registerQualificationBeforeTransfer;
    }

    public Boolean getStudentRegistrationWithRenewal() {
        return BooleanUtils.toBoolean(studentRegistrationWithRenewal);
    }

    public void setStudentRegistrationWithRenewal(Boolean studentRegistrationWithRenewal) {
        this.studentRegistrationWithRenewal = studentRegistrationWithRenewal;
    }

    public Boolean getShowActiveDurationsOnly() {
        return BooleanUtils.toBoolean(showActiveDurationsOnly);
    }

    public void setShowActiveDurationsOnly(Boolean showActiveDurationsOnly) {
        this.showActiveDurationsOnly = showActiveDurationsOnly;
    }

    public Boolean getRegistrationNumberHasZeroPrefix() {
        return BooleanUtils.toBoolean(registrationNumberHasZeroPrefix);
    }

    public void setRegistrationNumberHasZeroPrefix(Boolean registrationNumberHasZeroPrefix) {
        this.registrationNumberHasZeroPrefix = registrationNumberHasZeroPrefix;
    }

    public Boolean getCaptureCPDsDirectlyAndInDirectly() {
        return BooleanUtils.toBoolean(captureCPDsDirectlyAndInDirectly);
    }

    public void setCaptureCPDsDirectlyAndInDirectly(Boolean captureCPDsDirectlyAndInDirectly) {
        this.captureCPDsDirectlyAndInDirectly = captureCPDsDirectlyAndInDirectly;
    }

    @OneToOne
    public Conditions getMainRegisterCondition() {
        return mainRegisterCondition;
    }

    public void setMainRegisterCondition(Conditions mainRegisterCondition) {
        this.mainRegisterCondition = mainRegisterCondition;
    }

    public Boolean getAllowCapitalizationOfFirstLetters() {
        return BooleanUtils.toBoolean(allowCapitalizationOfFirstLetters);
    }

    public void setAllowCapitalizationOfFirstLetters(Boolean allowCapitalizationOfFirstLetters) {
        this.allowCapitalizationOfFirstLetters = allowCapitalizationOfFirstLetters;
    }

    public Boolean getAllowChangeOfRegistrationNumberAfterTransfer() {
        return BooleanUtils.toBoolean(allowChangeOfRegistrationNumberAfterTransfer);
    }

    public void setAllowChangeOfRegistrationNumberAfterTransfer(Boolean allowChangeOfRegistrationNumberAfterTransfer) {
        this.allowChangeOfRegistrationNumberAfterTransfer = allowChangeOfRegistrationNumberAfterTransfer;
    }

    public Boolean getAllowRenewalBeforeCPDs() {
        return BooleanUtils.toBoolean(allowRenewalBeforeCPDs);
    }

    public void setAllowRenewalBeforeCPDs(Boolean allowRenewalBeforeCPDs) {
        this.allowRenewalBeforeCPDs = allowRenewalBeforeCPDs;
    }

    /*
    * IF true allows CPDs to be captured as values : In Registrant CPD Points
    * else the CPD Points Item Will be used
    */
    public Boolean getCaptureCPDsDirectly() {
        return BooleanUtils.toBoolean(captureCPDsDirectly);
    }

    public void setCaptureCPDsDirectly(Boolean captureCPDsDirectly) {
        this.captureCPDsDirectly = captureCPDsDirectly;
    }

    //Please note that for this to work the email contact type in GeneralParameters must be set

    public Boolean getAllowSendingBirthDayRemindersToRegistrants() {
        return BooleanUtils.toBoolean(allowSendingBirthDayRemindersToRegistrants);
    }

    public void setAllowSendingBirthDayRemindersToRegistrants(Boolean allowSendingBirthDayRemindersToRegistrants) {
        this.allowSendingBirthDayRemindersToRegistrants = BooleanUtils.toBoolean(allowSendingBirthDayRemindersToRegistrants);
    }

    public Boolean getDynamicDisciplineReport() {
        return BooleanUtils.toBoolean(dynamicDisciplineReport);
    }

    public void setDynamicDisciplineReport(Boolean dynamicDisciplineReport) {
        this.dynamicDisciplineReport = dynamicDisciplineReport;
    }

    /*
   * IF true generated registration number will include year
   */
    public Boolean getRegistrationNumberByYear() {
        return BooleanUtils.toBoolean(registrationNumberByYear);
    }

    public void setRegistrationNumberByYear(Boolean registrationNumberByYear) {
        this.registrationNumberByYear = registrationNumberByYear;
    }

    public Integer getRegistrationCertificateSerial() {
        return registrationCertificateSerial;
    }

    public void setRegistrationCertificateSerial(Integer registrationCertificateSerial) {
        this.registrationCertificateSerial = registrationCertificateSerial;
    }

    public Boolean getRegistrationCertificateSerialIsIncrimental() {
        return BooleanUtils.toBoolean(registrationCertificateSerialIsIncrimental);
    }

    public void setRegistrationCertificateSerialIsIncrimental(Boolean registrationCertificateSerialIsIncrimental) {
        this.registrationCertificateSerialIsIncrimental = registrationCertificateSerialIsIncrimental;
    }

    /*
    * Transfer Process Will Create Registration
     */
    public Boolean getDirectRegistration() {
        return BooleanUtils.toBoolean(directRegistration);
    }

    public void setDirectRegistration(Boolean directRegistration) {
        this.directRegistration = directRegistration;
    }

    /*
   * Integration Url
    */
    public String getIntegrationUrl() {
        return integrationUrl;
    }

    public void setIntegrationUrl(String integrationUrl) {
        this.integrationUrl = integrationUrl;
    }

    /*
   * Determines if Integration Platform is allowed in the system
    */
    public Boolean isIntegrationAllowed() {
        return BooleanUtils.toBoolean(integrationAllowed);
    }

    public void setIntegrationAllowed(Boolean integrationAllowed) {
        this.integrationAllowed = integrationAllowed;
    }

    /*
    * If True Reports will be generated from Registrant Qualification Course
    * else the reports will be generated from Registration Course
  */
    public Boolean isReportsByCourse() {
        return BooleanUtils.toBoolean(reportsByCourse);
    }

    @OneToOne
    public ContactType getBusinessNumberContactType() {
        return businessNumberContactType;
    }

    public void setBusinessNumberContactType(ContactType businessNumberContactType) {
        this.businessNumberContactType = businessNumberContactType;
    }

    @OneToOne
    public ContactType getMobileNumberContactType() {
        return mobileNumberContactType;
    }

    public void setMobileNumberContactType(ContactType mobileNumberContactType) {
        this.mobileNumberContactType = mobileNumberContactType;
    }

    @OneToOne
    public ContactType getFaxNumberContactType() {
        return faxNumberContactType;
    }

    public void setFaxNumberContactType(ContactType faxNumberContactType) {
        this.faxNumberContactType = faxNumberContactType;
    }

    @OneToOne
    public AddressType getPhysicalAddressType() {
        return physicalAddressType;
    }

    public void setPhysicalAddressType(AddressType physicalAddressType) {
        this.physicalAddressType = physicalAddressType;
    }

    @OneToOne
    public AddressType getContactAddressType() {
        return contactAddressType;
    }

    public void setContactAddressType(AddressType contactAddressType) {
        this.contactAddressType = contactAddressType;
    }

    @OneToOne
    public AddressType getBusinessAddressType() {
        return businessAddressType;
    }

    public void setBusinessAddressType(AddressType businessAddressType) {
        this.businessAddressType = businessAddressType;
    }

    public void setReportsByCourse(Boolean reportsByCourse) {
        this.reportsByCourse = reportsByCourse;
    }

    public Boolean isRequireAffidavit() {
        return BooleanUtils.toBoolean(requireAffidavit);
    }

    public void setRequireAffidavit(Boolean requireAffidavit) {
        this.requireAffidavit = requireAffidavit;
    }

    public Integer getMonthsToRequireAffidavit() {
        if (monthsToRequireAffidavit == null) {
            return 0;
        } else {
            return monthsToRequireAffidavit;
        }
    }

    public void setMonthsToRequireAffidavit(Integer monthsToRequireAffidavit) {
        this.monthsToRequireAffidavit = monthsToRequireAffidavit;
    }

    @OneToOne
    public Requirement getAffidavit() {
        return affidavit;
    }

    public void setAffidavit(Requirement affidavit) {
        this.affidavit = affidavit;
    }

    public Boolean getDirectApproval() {
        return BooleanUtils.toBoolean(directApproval);
    }

    public void setDirectApproval(Boolean directApproval) {
        this.directApproval = directApproval;
    }

    @OneToOne
    public ContactType getEmailContactType() {
        return emailContactType;
    }

    public void setEmailContactType(ContactType emailContactType) {
        this.emailContactType = emailContactType;
    }

    public Boolean getRegistrationnumberIsByCourse() {
        return BooleanUtils.toBoolean(registrationnumberIsByCourse);
    }

    public void setRegistrationnumberIsByCourse(Boolean registrationnumberIsByCourse) {
        this.registrationnumberIsByCourse = registrationnumberIsByCourse;
    }

    public Integer getRegistrationNumberIncrement() {
        if (registrationNumberIncrement == null) {
            return 1;
        }
        return registrationNumberIncrement;
    }

    public void setRegistrationNumberIncrement(Integer registrationNumberIncrement) {
        this.registrationNumberIncrement = registrationNumberIncrement;
    }

    public Boolean getApproveAndRegister() {
        return BooleanUtils.toBoolean(approveAndRegister);
    }

    public void setApproveAndRegister(Boolean approveAndRegister) {
        this.approveAndRegister = approveAndRegister;
    }

    @OneToOne
    public InstitutionType getHealthInstitutionType() {
        return healthInstitutionType;
    }

    public void setHealthInstitutionType(InstitutionType healthInstitutionType) {
        this.healthInstitutionType = healthInstitutionType;
    }

    public String getCouncilPrefixForRegistrationNumber() {
        if (councilPrefixForRegistrationNumber == null) {
            return "";
        }
        return councilPrefixForRegistrationNumber;
    }

    public void setCouncilPrefixForRegistrationNumber(String councilPrefixForRegistrationNumber) {
        this.councilPrefixForRegistrationNumber = councilPrefixForRegistrationNumber;
    }

    public Boolean getRegistrationNumberHasPrefix() {
        return BooleanUtils.toBoolean(registrationNumberHasPrefix);
    }

    public void setRegistrationNumberHasPrefix(Boolean registrationNumberHasPrefix) {
        this.registrationNumberHasPrefix = registrationNumberHasPrefix;
    }

    /*
     * If True Application Needs Board Approval
     */
    public Boolean getHasProcessedByBoard() {
        return BooleanUtils.toBoolean(hasProcessedByBoard);
    }

    public void setHasProcessedByBoard(Boolean hasProcessedByBoard) {
        this.hasProcessedByBoard = hasProcessedByBoard;
    }

    public Integer getIncrementCertificateNumber() {
        if (incrementCertificateNumber == null) {
            return 1;
        }
        return incrementCertificateNumber;
    }

    public void setIncrementCertificateNumber(Integer incrementCertificateNumber) {
        this.incrementCertificateNumber = incrementCertificateNumber;
    }

    public Integer getUnrestrictedPCPeriod() {
        if (unrestrictedPCPeriod == null) {
            return 0;
        }
        return unrestrictedPCPeriod;
    }

    public void setUnrestrictedPCPeriod(Integer unrestrictedPCPeriod) {
        this.unrestrictedPCPeriod = unrestrictedPCPeriod;
    }

    public Boolean getHasReminderOnApplications() {
        return BooleanUtils.toBoolean(hasReminderOnApplications);
    }

    public void setHasReminderOnApplications(Boolean hasReminderOnApplications) {
        this.hasReminderOnApplications = hasReminderOnApplications;
    }

    public Integer getMinimumDaysPendingApplicationsLocal() {
        if (minimumDaysPendingApplicationsLocal == null) {
            return 0;
        }
        return minimumDaysPendingApplicationsLocal;
    }

    public void setMinimumDaysPendingApplicationsLocal(Integer minimumDaysPendingApplicationsLocal) {
        this.minimumDaysPendingApplicationsLocal = minimumDaysPendingApplicationsLocal;
    }

    public Integer getMinimumDaysPendingApplicationsForeign() {
        if (minimumDaysPendingApplicationsForeign == null) {
            return 0;
        }
        return minimumDaysPendingApplicationsForeign;
    }

    public void setMinimumDaysPendingApplicationsForeign(Integer minimumDaysPendingApplicationsForeign) {
        this.minimumDaysPendingApplicationsForeign = minimumDaysPendingApplicationsForeign;
    }

    public Integer getMinimumYearsInProvisionalRegister() {
        if (minimumYearsInProvisionalRegister == null) {
            return 0;
        }
        return minimumYearsInProvisionalRegister;
    }

    public void setMinimumYearsInProvisionalRegister(Integer minimumYearsInProvisionalRegister) {
        this.minimumYearsInProvisionalRegister = minimumYearsInProvisionalRegister;
    }

    public Integer getMinimumYearsInInternRegister() {
        if (minimumYearsInInternRegister == null) {
            return 0;
        }
        return minimumYearsInInternRegister;
    }

    public void setMinimumYearsInInternRegister(Integer minimumYearsInInternRegister) {
        this.minimumYearsInInternRegister = minimumYearsInInternRegister;
    }

    public Boolean getRegistrationByApplication() {
        return BooleanUtils.toBoolean(registrationByApplication);
    }

    public void setRegistrationByApplication(Boolean registrationByApplication) {
        this.registrationByApplication = registrationByApplication;
    }

    public Integer getLastRegistrationNumber() {
        return lastRegistrationNumber;
    }

    public void setLastRegistrationNumber(Integer lastRegistrationNumber) {
        this.lastRegistrationNumber = lastRegistrationNumber;
    }

    public Integer getNumberOfTimesToWriteSupExam() {
        return numberOfTimesToWriteSupExam;
    }

    public void setNumberOfTimesToWriteSupExam(Integer numberOfTimesToWriteSupExam) {
        this.numberOfTimesToWriteSupExam = numberOfTimesToWriteSupExam;
    }

    public Integer getLastCandidateNumber() {
        return lastCandidateNumber;
    }

    public void setLastCandidateNumber(Integer lastCandidateNumber) {
        this.lastCandidateNumber = lastCandidateNumber;
    }

    public String getContent_Url() {
        return content_Url;
    }

    public void setContent_Url(String content_Url) {
        this.content_Url = content_Url;
    }

    public String getContent_UserName() {
        return content_UserName;
    }

    public void setContent_UserName(String content_UserName) {
        this.content_UserName = content_UserName;
    }

    public String getContent_Password() {
        return content_Password;
    }

    public void setContent_Password(String content_Password) {
        this.content_Password = content_Password;
    }

    public Integer getMinimumPeriodForSup() {
        return minimumPeriodForSup;
    }

    public void setMinimumPeriodForSup(Integer minimumPeriodForSup) {
        this.minimumPeriodForSup = minimumPeriodForSup;
    }

    public Integer getMinimumPeriodToStartPBQ() {
        return minimumPeriodToStartPBQ;
    }

    public void setMinimumPeriodToStartPBQ(Integer minimumPeriodToStartPBQ) {
        this.minimumPeriodToStartPBQ = minimumPeriodToStartPBQ;
    }

    /*
    * Application Institution - Should be a saved Institution
    */
    @OneToOne
    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    /*
   * Abbreviation of Application
   */
    public String getCouncilAbbreviation() {
        return councilAbbreviation;
    }

    public void setCouncilAbbreviation(String councilAbbreviation) {
        this.councilAbbreviation = councilAbbreviation;
    }

    @OneToOne
    public Register getInstitutionRegister() {
        return institutionRegister;
    }

    public void setInstitutionRegister(Register institutionRegister) {
        this.institutionRegister = institutionRegister;
    }

    @OneToOne
    public CouncilDuration getCouncilDuration() {
        return councilDuration;
    }

    public void setCouncilDuration(CouncilDuration councilDuration) {
        this.councilDuration = councilDuration;
    }

    @OneToOne
    public Register getStudentRegister() {
        return studentRegister;
    }

    public void setStudentRegister(Register studentRegister) {
        this.studentRegister = studentRegister;
    }

    @OneToOne
    public Register getMainRegister() {
        return mainRegister;
    }

    public void setMainRegister(Register mainRegister) {
        this.mainRegister = mainRegister;
    }

    @OneToOne
    public Register getProvisionalRegister() {
        return provisionalRegister;
    }

    public void setProvisionalRegister(Register provisionalRegister) {
        this.provisionalRegister = provisionalRegister;
    }

    @OneToOne
    public Register getMaintenanceRegister() {
        return maintenanceRegister;
    }

    public void setMaintenanceRegister(Register maintenanceRegister) {
        this.maintenanceRegister = maintenanceRegister;
    }

    @OneToOne
    public Register getSpecialistRegister() {
        return specialistRegister;
    }

    public void setSpecialistRegister(Register specialistRegister) {
        this.specialistRegister = specialistRegister;
    }

    @OneToOne
    public Register getInternRegister() {
        return internRegister;
    }

    public void setInternRegister(Register internRegister) {
        this.internRegister = internRegister;
    }

    public Boolean getAllowMultipleRegistrations() {
        return BooleanUtils.toBoolean(allowMultipleRegistrations);
    }

    public void setAllowMultipleRegistrations(Boolean allowMultipleRegistrations) {
        this.allowMultipleRegistrations = allowMultipleRegistrations;
    }

    /*
  * Name used to Identify Registrants in the system
  */
    public String getRegistrantName() {
        return registrantName;
    }

    public void setRegistrantName(String registrantName) {
        this.registrantName = registrantName;
    }

    @Version
    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    /*
    * Application Name that is shown
    */
    public String getCouncilName() {
        return councilName;
    }

    public void setCouncilName(String councilName) {
        this.councilName = councilName;
    }

    public Integer getMinimumRegistrationAge() {
        return minimumRegistrationAge;
    }

    public void setMinimumRegistrationAge(Integer minimumRegistrationAge) {
        this.minimumRegistrationAge = minimumRegistrationAge;
    }

    @Override
    public String toString() {
        return getRegistrantName();
    }

    /*
    * If True Examnination Module is enbaled
    */
    public Boolean getExaminationModule() {
        return BooleanUtils.toBoolean(examinationModule);
    }

    public void setExaminationModule(Boolean examinationModule) {
        this.examinationModule = examinationModule;
    }

    public Boolean getCourseCertificateNumber() {
        return BooleanUtils.toBoolean(courseCertificateNumber);
    }

    public void setCourseCertificateNumber(Boolean courseCertificateNumber) {
        this.courseCertificateNumber = courseCertificateNumber;
    }

    @OneToOne
    public RegistrantStatus getVoidedStatus() {
        return voidedStatus;
    }

    public void setVoidedStatus(RegistrantStatus voidedStatus) {
        this.voidedStatus = voidedStatus;
    }

    public Integer getMinimumYearsForInstitutionApplication() {
        return minimumYearsForInstitutionApplication;
    }

    public void setMinimumYearsForInstitutionApplication(Integer minimumYearsForInstitutionApplication) {
        this.minimumYearsForInstitutionApplication = minimumYearsForInstitutionApplication;
    }

    public Integer getLastCertificateNumber() {
        if (lastCertificateNumber == null) {
            return 0;
        }
        return lastCertificateNumber;
    }

    public void setLastCertificateNumber(Integer lastCertificateNumber) {
        this.lastCertificateNumber = lastCertificateNumber;
    }

    @ManyToOne
    public InstitutionType getInstitutionType() {
        return institutionType;
    }

    public void setInstitutionType(InstitutionType institutionType) {
        this.institutionType = institutionType;
    }

    public Integer getMinimumDaysPendingApplicationsCGS() {
        if (minimumDaysPendingApplicationsCGS == null) {
            return 0;
        }
        return minimumDaysPendingApplicationsCGS;
    }

    public void setMinimumDaysPendingApplicationsCGS(Integer minimumDaysPendingApplicationsCGS) {
        this.minimumDaysPendingApplicationsCGS = minimumDaysPendingApplicationsCGS;
    }

    public Integer getMinimumYearsForCGSApplication() {
        if (minimumYearsForCGSApplication == null) {
            return 0;
        }
        return minimumYearsForCGSApplication;
    }

    public void setMinimumYearsForCGSApplication(Integer minimumYearsForCGSApplication) {
        this.minimumYearsForCGSApplication = minimumYearsForCGSApplication;
    }

    public Integer getLastPracticeCertificateNumber() {
        if (lastPracticeCertificateNumber == null) {
            return 0;
        }
        return lastPracticeCertificateNumber;
    }

    public void setLastPracticeCertificateNumber(Integer lastPracticeCertificateNumber) {
        this.lastPracticeCertificateNumber = lastPracticeCertificateNumber;
    }

    public Integer getLastCGSCertificateNumber() {
        if (lastCGSCertificateNumber == null) {
            return 0;
        }
        return lastCGSCertificateNumber;
    }

    public void setLastCGSCertificateNumber(Integer lastCGSCertificateNumber) {
        this.lastCGSCertificateNumber = lastCGSCertificateNumber;
    }

    /*
    * If True Generate Registration Number for Individual on Creation of a Registrant
    */
    public Boolean getGenerateRegnumberOnRegistrant() {
        return BooleanUtils.toBoolean(generateRegnumberOnRegistrant);
    }

    public void setGenerateRegnumberOnRegistrant(Boolean generateRegnumberOnRegistrant) {
        this.generateRegnumberOnRegistrant = generateRegnumberOnRegistrant;
    }

    /*
   * Generate Registration Certificate and Practice Certificate when a Registrant From File
   */
    public Boolean getAllowPreRegistrantion() {
        return BooleanUtils.toBoolean(allowPreRegistrantion);
    }

    public void setAllowPreRegistrantion(Boolean allowPreRegistrantion) {
        this.allowPreRegistrantion = allowPreRegistrantion;
    }

    @OneToOne
    public RegistrantStatus getSuspendedStatus() {
        return suspendedStatus;
    }

    public void setSuspendedStatus(RegistrantStatus suspendedStatus) {
        this.suspendedStatus = suspendedStatus;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailPassword() {
        return emailPassword;
    }

    public void setEmailPassword(String emailPassword) {
        this.emailPassword = emailPassword;
    }

    @OneToOne
    public RegistrantStatus getDeRegisteredStatus() {
        return deRegisteredStatus;
    }

    public void setDeRegisteredStatus(RegistrantStatus deRegisteredStatus) {
        this.deRegisteredStatus = deRegisteredStatus;
    }

    /*
    * If True Reports Billing of Activities will be done, Payment Module Enabled
    */
    public Boolean isBillingModule() {
        return BooleanUtils.toBoolean(billingModule);
    }

    public void setBillingModule(Boolean billingModule) {
        this.billingModule = billingModule;
    }

    public String getEmailSignature() {
        return emailSignature == null ? "" : emailSignature;
    }

    public void setEmailSignature(String emailSignature) {
        this.emailSignature = emailSignature;
    }

    /*
    * If True Exam Registration will Require an Examination Clearance Letter
    */
    public Boolean getExamClearance() {
        return BooleanUtils.toBoolean(examClearance);
    }

    public void setExamClearance(Boolean examClearance) {
        this.examClearance = examClearance;
    }

    public Integer getMinimumYearsForPBQCGSApplication() {
        if (minimumYearsForPBQCGSApplication == null) {
            return 0;
        }
        return minimumYearsForPBQCGSApplication;
    }

    public void setMinimumYearsForPBQCGSApplication(Integer minimumYearsForPBQCGSApplication) {
        this.minimumYearsForPBQCGSApplication = minimumYearsForPBQCGSApplication;
    }

}
