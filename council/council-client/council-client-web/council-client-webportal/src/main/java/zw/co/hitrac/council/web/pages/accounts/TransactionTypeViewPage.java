package zw.co.hitrac.council.web.pages.accounts;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.TransactionType;
import zw.co.hitrac.council.business.service.accounts.TransactionTypeService;

/**
 *
 * @author Matiashe Michael
 */
public class TransactionTypeViewPage extends IAccountsReceivablePage {

    @SpringBean
    private TransactionTypeService transactionTypeService;

    public TransactionTypeViewPage(Long id) {
        CompoundPropertyModel<TransactionType> model = new CompoundPropertyModel<TransactionType>(new LoadableDetachableTransactionTypeModel(id));
        setDefaultModel(model);
        add(new Link<TransactionType>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new TransactionTypeEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", TransactionTypeListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("effect"));
        add(new Label("drLedger.name"));
        add(new Label("crLedger.name"));
        add(new Label("book.name"));
        add(new Label("transactionType", model.bind("name")));
    }

    private final class LoadableDetachableTransactionTypeModel extends LoadableDetachableModel<TransactionType> {

        private Long id;

        public LoadableDetachableTransactionTypeModel(Long id) {
            this.id = id;
        }

        @Override
        protected TransactionType load() {

            return transactionTypeService.get(id);
        }
    }
}
