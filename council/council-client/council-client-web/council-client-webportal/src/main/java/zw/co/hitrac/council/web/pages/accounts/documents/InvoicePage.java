package zw.co.hitrac.council.web.pages.accounts.documents;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.TypeOfService;
import zw.co.hitrac.council.business.domain.accounts.*;
import zw.co.hitrac.council.business.process.InvoiceProcess;
import zw.co.hitrac.council.business.process.PaymentProcess;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.accounts.CustomerService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.business.service.accounts.ProductService;
import zw.co.hitrac.council.business.service.accounts.ReceiptItemService;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.accounts.IAccountingPage;
import zw.co.hitrac.council.web.pages.accounts.payments.PaymentConfirmationPage;
import zw.co.hitrac.council.web.pages.institution.InstitutionViewPage;
import zw.co.hitrac.council.web.pages.registration.RegistrantViewPage;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

/**
 *
 * @author Charles Chigoriwa
 * @author Michael Matiashe
 */
public class InvoicePage extends IAccountingPage {

    private List<DocumentItem> documentItems;
    private List<DocumentItem> otherItems;
    private List<DocumentItem> registrationsItems;
    private List<DocumentItem> applicationItems;
    private List<DocumentItem> annualItems;
    private List<DocumentItem> penaltyItems;
    private List<DocumentItem> disciplinaryItems;
    @SpringBean
    private ProductService productService;
    @SpringBean
    private InvoiceProcess invoiceProcess;
    @SpringBean
    private DebtComponentService debtComponentService;
    @SpringBean
    private CustomerService customerService;
    @SpringBean
    private RegistrantService registrantService;
    @SpringBean
    private ReceiptItemService receiptItemService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private PaymentProcess paymentProcess;

    public InvoicePage(final Customer customer, final Boolean isInstitution) {
        initDocumentItems();
        final Document document = new Document();
        document.setCustomer(customer);
        document.setDocumentType(DocumentType.INVOICE);
        document.setCreatedBy(CouncilSession.get().getUser());
        document.setDate(new Date());
        document.setDateCreated(new Date());
        add(new Label("customer.customerName", customer.getCustomerName()));
        Form<Document> form = new Form<Document>("form", Model.of(document)) {
            @Override
            protected void onSubmit() {
                List<DocumentItem> tobeProcessedDocumentItems = new ArrayList<DocumentItem>();
                documentItems.addAll(annualItems);
                documentItems.addAll(registrationsItems);
                documentItems.addAll(applicationItems);
                documentItems.addAll(otherItems);
                documentItems.addAll(penaltyItems);
                documentItems.addAll(disciplinaryItems);

                for (DocumentItem documentItem : documentItems) {
                    if (documentItem.getQuantity() > 0) {
                        tobeProcessedDocumentItems.add(documentItem);
                    }
                }
                if (!tobeProcessedDocumentItems.isEmpty()) {
                    invoiceProcess.invoice(customer, document, tobeProcessedDocumentItems);
                }
                Customer updatedCustomer = customerService.get(customer.getId());
                List<DebtComponent> debtComponents = debtComponentService.getDebtComponents(updatedCustomer.getAccount());
                List<ReceiptItem> receiptItemList = receiptItemService.getReceiptItemsWithNoReceiptHeader(updatedCustomer.getAccount());

                if (debtComponents.isEmpty() && !receiptItemList.isEmpty()) {
                    Set<ReceiptItem> receiptItems = new HashSet<ReceiptItem>();

                    if (receiptItemList.isEmpty()) {
                        if (!isInstitution) {
                            setResponsePage(new RegistrantViewPage(registrantService.getRegistrant(customer).getId()));
                        } else {
                            setResponsePage(new InstitutionViewPage(institutionService.getInstitution(customer).getId()));
                        }
                    } else {
                        for (ReceiptItem receiptItem : receiptItemList) {
                            receiptItems.add(receiptItem);
                        }
                        PaymentDetails paymentDetails = paymentProcess.accountBallancePaysAll(receiptItems, CouncilSession.get().getUser(), customer, null);

                        setResponsePage(new PaymentConfirmationPage(paymentDetails, updatedCustomer.getAccount()));
                    }
                } else if (!debtComponents.isEmpty() && !receiptItemList.isEmpty()) {
                    Set<ReceiptItem> receiptItems = new HashSet<ReceiptItem>();
                    if (receiptItemList.isEmpty()) {
                        if (!isInstitution) {
                            setResponsePage(new RegistrantViewPage(registrantService.getRegistrant(customer).getId()));
                        } else {
                            setResponsePage(new InstitutionViewPage(institutionService.getInstitution(customer).getId()));
                        }
                    } else {
                        for (ReceiptItem receiptItem : receiptItemList) {
                            receiptItems.add(receiptItem);
                        }

                        paymentProcess.accountBalancePaysLess(receiptItems, CouncilSession.get().getUser(), customer, null);
                    }
                    if (!isInstitution) {
                        setResponsePage(new DebtComponentsPage(registrantService.getRegistrant(updatedCustomer).getId()));
                    } else {
                        setResponsePage(new InstitutionDebtComponentsPage(institutionService.getInstitution(updatedCustomer).getId()));
                    }

                } else {
                    //Select the items you want to pay for

                    if (isInstitution) {
                        setResponsePage(new InstitutionDebtComponentsPage(institutionService.getInstitution(updatedCustomer).getId()));
                    } else {
                        setResponsePage(new DebtComponentsPage(registrantService.getRegistrant(updatedCustomer).getId()));
                    }

                }
            }
        };
        form.add(new DocumentItemListView("otherItems", this.otherItems));
        form.add(new DocumentItemListView("registrationsItems", this.registrationsItems));
        form.add(new DocumentItemListView("applicationItems", this.applicationItems));
        form.add(new DocumentItemListView("annualItems", this.annualItems));
        form.add(new DocumentItemListView("penaltyItems", this.penaltyItems));
        form.add(new DocumentItemListView("disciplinaryItems", this.disciplinaryItems));
        add(new FeedbackPanel("feedback"));
        form.add(new Link<Registrant>("registrantPaymentPage") {
            @Override
            public void onClick() {
                Registrant registrant = registrantService.getRegistrant(customer);
                if (registrant != null) {
                    setResponsePage(new DebtComponentsPage(registrant.getId()));
                }
            }
        });
        add(form);
    }

    public List<DocumentItem> getDocumentItems() {
        return documentItems;
    }

    public void setDocumentItems(List<DocumentItem> documentItems) {
        this.documentItems = documentItems;
    }

    public List<DocumentItem> getOtherItems() {
        return otherItems;
    }

    public void setOtherItems(List<DocumentItem> otherItems) {
        this.otherItems = otherItems;
    }

    public List<DocumentItem> getRegistrationsItems() {
        return registrationsItems;
    }

    public void setRegistrationsItems(List<DocumentItem> registrationsItems) {
        this.registrationsItems = registrationsItems;
    }

    public List<DocumentItem> getApplicationItems() {
        return applicationItems;
    }

    public void setApplicationItems(List<DocumentItem> applicationItems) {
        this.applicationItems = applicationItems;
    }

    public List<DocumentItem> getAnnualItems() {
        return annualItems;
    }

    public void setAnnualItems(List<DocumentItem> annualItems) {
        this.annualItems = annualItems;
    }

    public List<DocumentItem> getPenaltyItems() {
        return penaltyItems;
    }

    public void setPenaltyItems(List<DocumentItem> penaltyItems) {
        this.penaltyItems = penaltyItems;
    }

    public List<DocumentItem> getDisciplinaryItems() {
        return disciplinaryItems;
    }

    public void setDisciplinaryItems(List<DocumentItem> disciplinaryItems) {
        this.disciplinaryItems = disciplinaryItems;
    }

    private void initDocumentItems() {
        this.documentItems = new ArrayList<DocumentItem>();
        this.annualItems = new ArrayList<DocumentItem>();
        this.registrationsItems = new ArrayList<DocumentItem>();
        this.applicationItems = new ArrayList<DocumentItem>();
        this.otherItems = new ArrayList<DocumentItem>();
        this.penaltyItems = new ArrayList<DocumentItem>();
        this.disciplinaryItems = new ArrayList<DocumentItem>();
        List<Product> allProducts = this.productService.findAll(Boolean.FALSE, Boolean.TRUE);
        for (Product product : allProducts) {
            DocumentItem documentItem = new DocumentItem();
            documentItem.setAccount(product.getSalesAccount());
            if (product.getProductPrice() == null) {
                documentItem.setPrice(new BigDecimal(BigInteger.ZERO));
            } else {
                documentItem.setPrice(product.getProductPrice().getPrice());
            }
            documentItem.setQuantity(0);
            documentItem.setProduct(product);
            documentItem.setCreatedBy(CouncilSession.get().getUser());
            if (product.getTypeOfService() != null) {
                if (product.getTypeOfService().equals(TypeOfService.ANNUAL_FEES)) {
                    this.annualItems.add(documentItem);
                } else if (product.getTypeOfService().equals(TypeOfService.REGISTRATION_FEES) || product.getTypeOfService().equals(TypeOfService.RE_REGISTRATION_FEES)) {
                    this.registrationsItems.add(documentItem);
                } else if (product.getTypeOfService().equals(TypeOfService.APPLICATION_FEES) || product.getTypeOfService().equals(TypeOfService.APPLICATION_FOR_CGS_FEES) || product.getTypeOfService().equals(TypeOfService.APPLICATION_FOR_SUPERVISION) || product.getTypeOfService().equals(TypeOfService.APPLICATION_FOR_UNRESTRICTED_PRACTICE) || product.getTypeOfService().equals(TypeOfService.APPLICATION_FOR_INSTITUTION_REGISTRATION) || product.getTypeOfService().equals(TypeOfService.TRANSFER_FEES) || product.getTypeOfService().equals(TypeOfService.TRANSCRIPT_OF_TRAINING_FEES) || product.getTypeOfService().equals(TypeOfService.CHANGE_OF_NAME_FEES)) {
                    this.applicationItems.add(documentItem);
                } else if (product.getTypeOfService().equals(TypeOfService.PENALTY_FEES)) {
                    this.penaltyItems.add(documentItem);
                } else if (product.getTypeOfService().equals(TypeOfService.DISCIPLINARY_FEES)) {
                    this.disciplinaryItems.add(documentItem);
                } else {
                    this.otherItems.add(documentItem);
                }
            }
        }
    }

    private final static class DocumentItemListView extends ListView<DocumentItem> {

        public DocumentItemListView(String id) {
            super(id);
            setReuseItems(true);
        }

        public DocumentItemListView(String id, IModel<? extends List<? extends DocumentItem>> model) {
            super(id, model);
            setReuseItems(true);
        }

        public DocumentItemListView(String id, List<? extends DocumentItem> list) {
            super(id, list);
            setReuseItems(true);
        }

        @Override
        protected void populateItem(ListItem<DocumentItem> item) {
            item.setDefaultModel(new CompoundPropertyModel<DocumentItem>(item.getModelObject()));
            item.add(new TextField<Integer>("quantity").setRequired(true).add(new ErrorBehavior()));
            item.add(new Label("product.name"));
            item.add(new Label("product.register"));
            item.add(new Label("price"));
            if (item.getIndex() % 2 == 0) {
                item.add(new EvenTableRowBehavior());
            }
        }
    }
}
