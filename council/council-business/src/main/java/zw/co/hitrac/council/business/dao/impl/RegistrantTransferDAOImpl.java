package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.RegistrantTransferDAO;
import zw.co.hitrac.council.business.dao.RegistrantTransferDAO;
import zw.co.hitrac.council.business.dao.repo.RegistrantTransferRepository;
import zw.co.hitrac.council.business.domain.RegistrantTransfer;

/**
 *
 * @author Michael Matiashe
 */
@Repository
public class RegistrantTransferDAOImpl implements RegistrantTransferDAO {
   @Autowired
   private RegistrantTransferRepository registrantTransferRepository;
    
   public RegistrantTransfer save(RegistrantTransfer registrantTransfer){
       return registrantTransferRepository.save(registrantTransfer);
   }
   
   public List<RegistrantTransfer> findAll(){
       return registrantTransferRepository.findAll();
   }
   
   public RegistrantTransfer get(Long id){
       return registrantTransferRepository.findOne(id);
   }
 /**
     * A setter method that will make mocking repo object easier
     * @param registrantTransferRepository 
     */
   
   public void setRegistrantTransferRepository(RegistrantTransferRepository registrantTransferRepository) {
         this.registrantTransferRepository = registrantTransferRepository;
    }

    public List<RegistrantTransfer> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
