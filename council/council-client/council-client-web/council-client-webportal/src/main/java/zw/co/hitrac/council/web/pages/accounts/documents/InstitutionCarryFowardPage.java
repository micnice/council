package zw.co.hitrac.council.web.pages.accounts.documents;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Check;
import org.apache.wicket.markup.html.form.CheckGroup;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.domain.accounts.PaymentDetails;
import zw.co.hitrac.council.business.domain.accounts.ReceiptItem;
import zw.co.hitrac.council.business.domain.accounts.TransactionType;
import zw.co.hitrac.council.business.process.InvoiceProcess;
import zw.co.hitrac.council.business.process.PaymentProcess;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.service.accounts.AccountService;
import zw.co.hitrac.council.business.service.accounts.AccountsParametersService;
import zw.co.hitrac.council.business.service.accounts.CustomerService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.business.service.accounts.ReceiptItemService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.accounts.payments.PaymentConfirmationPage;
import zw.co.hitrac.council.web.pages.institution.InstitutionViewPage;
import zw.co.hitrac.council.web.utility.DetachableInstitutionModel;

/**
 *
 * @author Takunda Dhlakama
 * @author Charles Chigoriwa
 */
public class InstitutionCarryFowardPage extends TemplatePage {
    
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    AccountService accountService;
    @SpringBean
    CustomerService customerAccountService;
    @SpringBean
    RegistrationService registrationService;
    @SpringBean
    DebtComponentService debtComponentService;
    @SpringBean
    GeneralParametersService generalParametersService;
    @SpringBean
    private InvoiceProcess invoiceProcess;
    @SpringBean
    private PaymentProcess paymentProcess;
    @SpringBean
    private AccountsParametersService accountsParametersService;
    @SpringBean
    private ReceiptItemService receiptItemService;
    private List<DebtComponent> selectedDebtComponents = new ArrayList<DebtComponent>();
    private Long institutionId;
    
    public InstitutionCarryFowardPage(PageParameters parameters) {
        this(parameters.get("institutionId").toLong());
    }
    
    public InstitutionCarryFowardPage(Long id) {
        institutionId = id;
        
        final CompoundPropertyModel<Institution> institutionModel = new CompoundPropertyModel<Institution>(new DetachableInstitutionModel(id, institutionService));
        setDefaultModel(institutionModel);
        add(new Label("fullname"));
        final Account account = accountService.get(institutionModel.getObject().getCustomerAccount().getAccount().getId());
        add(new Label("balance", account.getBalance().toString()));
        
        IModel<List<DebtComponent>> debtComponentsModel = new LoadableDetachableModel<List<DebtComponent>>() {
            @Override
            protected List<DebtComponent> load() {
                return debtComponentService.getDebtComponents(institutionModel.getObject().getCustomerAccount());
            }
        };
        add(new FeedbackPanel("feedback"));
        Form<Void> form = new Form<Void>("form") {
            @Override
            protected void onSubmit() {
                try {
                    if (!selectedDebtComponents.isEmpty()) {
                        Customer customer = customerAccountService.get(institutionModel.getObject().getCustomerAccount().getId());
                        TransactionType transactionType = accountsParametersService.get().getInvoiceTransactionType();
                        paymentProcess.payThroughCarryForward(account, customer, transactionType, selectedDebtComponents);
                        List<DebtComponent> debtComponents = debtComponentService.getDebtComponents(customer.getAccount());
                        List<ReceiptItem> receiptItemList = receiptItemService.getReceiptItemsWithNoReceiptHeader(customer.getAccount());
                        if (debtComponents.isEmpty() && !receiptItemList.isEmpty()) {
                            Set<ReceiptItem> receiptItems = new HashSet<ReceiptItem>();
                            
                            if (receiptItemList.isEmpty()) {
                                setResponsePage(new InstitutionViewPage(institutionService.getInstitution(customer).getId()));
                            } else {
                                for (ReceiptItem receiptItem : receiptItemList) {
                                    receiptItems.add(receiptItem);
                                }
                                PaymentDetails paymentDetails = paymentProcess.accountBallancePaysAll(receiptItems, CouncilSession.get().getUser(), customer, null);
                                
                                setResponsePage(new PaymentConfirmationPage(paymentDetails, customer.getAccount()));
                            }
                        } else if (!debtComponents.isEmpty() && !receiptItemList.isEmpty()) {
                            Set<ReceiptItem> receiptItems = new HashSet<ReceiptItem>();
                            if (receiptItemList.isEmpty()) {
                                setResponsePage(new InstitutionViewPage(institutionService.getInstitution(customer).getId()));
                            } else {
                                for (ReceiptItem receiptItem : receiptItemList) {
                                    receiptItems.add(receiptItem);
                                }
                                
                                paymentProcess.accountBalancePaysLess(receiptItems, CouncilSession.get().getUser(), customer, null);
                            }
                            setResponsePage(new DebtComponentsPage(institutionService.getInstitution(customer).getId()));
                            
                        } else {
                            //Select the items you want to pay for
                            setResponsePage(new DebtComponentsPage(institutionService.getInstitution(customer).getId()));
                        }
                    }
                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };
        
        CheckGroup<DebtComponent> selectedDebtComponentsGroup = new CheckGroup<DebtComponent>("selectedDebtComponents", this.selectedDebtComponents);
        form.add(selectedDebtComponentsGroup);
        
        add(form);
        PropertyListView<DebtComponent> eachItem = new PropertyListView<DebtComponent>("eachItem", debtComponentsModel) {
            @Override
            protected void populateItem(final ListItem<DebtComponent> item) {
                
                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                
                item.add(new Check("selected", item.getModel()));
                item.add(new Label("product.name"));
                item.add(new Label("remainingBalance"));
            }
        };
        
        selectedDebtComponentsGroup.add(eachItem);
        
        form.add(new Link<Void>("institutionViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionViewPage(institutionModel.getObject().getId()));
            }
        });
        
        form.add(new Link<Institution>("creditNoteLink") {
            @Override
            public void onClick() {
                setResponsePage(new CreditNotePage(institutionModel.getObject().getId()));
            }
        });
        
        form.add(new Link<Institution>("institutionPaymentPage") {
            @Override
            public void onClick() {
                setResponsePage(new DebtComponentsPage(institutionModel.getObject().getId()));
                
            }
        });
        
    }
    
    @Override
    protected void onConfigure() {
        //if billing module disabled - direct to dashboard
        if (!generalParametersService.get().isBillingModule()) {
            if (institutionId != null) {
                setResponsePage(new InstitutionViewPage(institutionId));
            }
        }
        
    }
}
