/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service.accounts.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.accounts.BankDAO;
import zw.co.hitrac.council.business.domain.accounts.Bank;
import zw.co.hitrac.council.business.service.accounts.BankService;

import java.util.List;

/**
 *
 * @author Michael Matiashe
 */

@Service
@Transactional
public class BankServiceImpl implements BankService {

    @Autowired
    private BankDAO bankAccountDAO;

    @Transactional
    public Bank save(Bank t) {
        return bankAccountDAO.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Bank> findAll() {
        return bankAccountDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Bank get(Long id) {
        return bankAccountDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Bank> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setBankAccountDao(BankDAO bankAccountDAO) {

        this.bankAccountDAO = bankAccountDAO;
    }

}
