package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.PostDAO;
import zw.co.hitrac.council.business.dao.repo.PostRepository;
import zw.co.hitrac.council.business.domain.Post;

/**
 *
 * @author tidza
 */
@Repository
public class PostDAOImpl implements PostDAO {

    @Autowired
    private PostRepository postRepository;

    public Post save(Post t) {
        return postRepository.save(t);
    }

    public List<Post> findAll() {
        return postRepository.findAll();
    }

    public Post get(Long id) {
        return postRepository.findOne(id);
    }

    public void setPostRepository(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    public List<Post> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
   
}
