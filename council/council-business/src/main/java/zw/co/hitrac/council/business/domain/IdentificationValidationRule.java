package zw.co.hitrac.council.business.domain;

import org.apache.commons.lang.StringUtils;
import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.utils.CouncilException;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;

/**
 * Created by clive on 6/16/15.
 */

@Entity
@Table(name = "identificationValidationRule")
@Audited
public class IdentificationValidationRule extends BaseIdEntity implements Serializable {

    private String example;
    private Citizenship citizenship;
    private IdentificationType identificationType;
    private Boolean active = Boolean.TRUE;

    /**
     *
     * Method to generate a regex from an example input
     * @return A regular expression
     */
    @Transient
    public String getValidationRegularExpressionFromExample(){

        if(StringUtils.isBlank(getExample())){
            throw new CouncilException("Cannot generate validation expression when example is null or empty");
        }

        String regex = getExample().replaceAll("[A-Za-z]", "[A-Za-z]").replaceAll("\\d", "\\\\d").replaceAll("\\s", "\\\\s");

        return regex;

    }


    public String getExample() {

        return example;
    }

    public void setExample(String identificationValidationRegex) {

        this.example = identificationValidationRegex;
    }

    @ManyToOne
    public Citizenship getCitizenship() {

        return citizenship;
    }

    public void setCitizenship(Citizenship citizenship) {

        this.citizenship = citizenship;
    }

    @ManyToOne
    public IdentificationType getIdentificationType() {

        return identificationType;
    }

    public void setIdentificationType(IdentificationType identificationType) {

        this.identificationType = identificationType;
    }

    public Boolean getActive() {

        return active == null ? Boolean.TRUE : active;
    }

    public void setActive(Boolean enabled) {

        this.active = enabled;
    }

}
