package zw.co.hitrac.council.web.pages.reports;

import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.ContactType;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.ContactTypeService;
import zw.co.hitrac.council.business.service.RegistrantContactService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.reports.RegistrantContactDetails;
import zw.co.hitrac.council.reports.RegistrantsReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AllRegistrantsContactPage extends IReportPage {

    @SpringBean
    private ContactTypeService contactTypeService;
    @SpringBean
    private RegistrantContactService registrantContactService;
    private ContactType contactType;
    private Course course;
    private ContentType contentType;

    public AllRegistrantsContactPage() {
        PropertyModel<ContactType> contactTypeModel = new PropertyModel<ContactType>(this, "contactType");
        PropertyModel<Course> courseModel = new PropertyModel<Course>(this, "course");
        PropertyModel<ContentType> contentTypeModel = new PropertyModel<ContentType>(this, "contentType");
        Form<?> form = new Form("form") {
            @Override
            protected void onSubmit() {
                try {
                    RegistrantContactDetails registrantDataReport = new RegistrantContactDetails();

                    Map parameters = new HashMap();

                    ByteArrayResource resource;

                    if (course != null) {
                        parameters.put("comment", course + " - " + contactType + " Details");
                    } else {
                        parameters.put("comment", "Individual - " + contactType + " Details");
                    }
                    try {
                        resource = ReportResourceUtils.getReportResource(registrantDataReport, contentType, registrantContactService.getAllRegistrantDataAndContacts(contactType),
                                parameters);

                        IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                RequestCycle.get().getResponse(), null);

                        resource.respond(a);
                    } catch (JRException ex) {
                        Logger.getLogger(RegistrantsReport.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }
        };
        form.add(new DropDownChoice("contactType", contactTypeModel, contactTypeService.findAll()).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("contentType", contentTypeModel, Arrays.asList(ContentType.values())));
        form.add(new Button("incorrectIdNumbers") {
            @Override
            public void onSubmit() {
                Set<RegistrantData> registrants = new HashSet<RegistrantData>();
                Map parameters = new HashMap();
                parameters.put("comment", "Those without ID Numbers or Incorrect ID Numbers");

                try {
                    RegistrantContactDetails registrantDataReport = new RegistrantContactDetails();

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(registrantDataReport, contentType, registrantContactService.getRegistrantDataAddressCorrections(contactType),
                            parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });
        add(form);
    }

}
