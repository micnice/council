/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.accounts;

import org.apache.wicket.markup.html.link.BookmarkablePageLink;

/**
 *
 * @author tdhlakama
 */
public class AccountingPage extends IAccountingPage {

    public AccountingPage() {
        add(new BookmarkablePageLink<Void>("accountsReceivableLink", AccountsReceivablePage.class));
        add(new BookmarkablePageLink<Void>("bankBookLink", BankBookPage.class));
        add(new BookmarkablePageLink<Void>("generalLedgerLink", GeneralLedgerPage.class));
        add(new BookmarkablePageLink<Void>("inventoryModuleLink", InventoryModulePage.class));
        add(new BookmarkablePageLink<Void>("invoiceTransactionType", AccountsParametersViewPage.class));
    }
}
