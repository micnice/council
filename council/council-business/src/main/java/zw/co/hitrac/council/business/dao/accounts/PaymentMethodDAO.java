
package zw.co.hitrac.council.business.dao.accounts;

import zw.co.hitrac.council.business.dao.IGenericDAO;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.PaymentMethod;

/**
 *
 * @author Tatenda Chiwandire
 * @author Michael Matiashe
 */
public interface PaymentMethodDAO extends IGenericDAO<PaymentMethod> {
   
}
