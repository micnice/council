package zw.co.hitrac.council.web.pages.registration;

import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.web.pages.TemplatePage;

/**
 *
 * @author tdhlakama
 */
public class RegistrantRegistrationListPage extends TemplatePage {

    @SpringBean
    RegistrationService registrationService;

    public RegistrantRegistrationListPage(final IModel<Registrant> registrantModel) {
        menuPanelManager.getRegistrantBackMenuPanel().setTopMenuCurrent(true);
        menuPanelManager.getRegistrantBackMenuPanel().setRegistrantId(registrantModel.getObject().getId());
        
        add(new RegistrantPanel("registrantPanel", registrantModel.getObject().getId()));
        add(new RegistrationsPanelList("registrations", registrationService.getRegistrations(registrantModel.getObject())));
    }
}
