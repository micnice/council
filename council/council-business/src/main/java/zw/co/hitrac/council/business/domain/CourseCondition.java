package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

/**
 *
 * @author tdhlakama
 */
@Entity
@Table(name="course_condition")
@Audited
public class CourseCondition extends BaseIdEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    private Register register;
    private Course course;
    private Conditions condition;

    @ManyToOne(optional = false)
    public Register getRegister() {
        return register;
    }

    public void setRegister(Register register) {
        this.register = register;
    }

    @ManyToOne(optional = true)
    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course =course;
    }

    @ManyToOne(optional = true)    
    public Conditions getCondition() {
        return condition;
    }

    public void setCondition(Conditions condition) {
        this.condition = condition;
    }
    
    @Override
    public String toString() {
        return getCondition().getName();
    }

}
