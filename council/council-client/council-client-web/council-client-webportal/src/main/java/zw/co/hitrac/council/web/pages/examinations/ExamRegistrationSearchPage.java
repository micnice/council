
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;
import zw.co.hitrac.council.business.service.CourseService;
import zw.co.hitrac.council.business.service.InstitutionService;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationService;
import zw.co.hitrac.council.business.service.examinations.ExamSettingService;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.List;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.CourseListModel;
import zw.co.hitrac.council.web.models.ExamSettingListModel;

/**
 *
 * @author tdhlakama
 */
public class ExamRegistrationSearchPage extends IExaminationsPage {

    @SpringBean
    private ExamRegistrationService examRegistrationService;
    @SpringBean
    private ExamSettingService examSettingService;
    @SpringBean
    private InstitutionService institutionService;
    @SpringBean
    private CourseService courseService;
    private Institution institution;
    private ExamSetting examSetting;
    private String candidateNumber;
    private String idNumber;
    private Course course;
    private HrisComparator hrisComparator = new HrisComparator();

    public ExamRegistrationSearchPage() {
        PropertyModel<ExamSetting> examSettingModel = new PropertyModel<ExamSetting>(this, "examSetting");
        PropertyModel<Institution> institutionModel = new PropertyModel<Institution>(this, "institution");
        PropertyModel<String> candidateNumberModel = new PropertyModel<String>(this, "candidateNumber");
        PropertyModel<String> idNumberModel = new PropertyModel<String>(this, "idNumber");
        PropertyModel<Course> courseModel = new PropertyModel<Course>(this, "course");
        Form<?> form = new Form("form");
        form.add(new DropDownChoice("examSetting", examSettingModel, new ExamSettingListModel(examSettingService)));
        form.add(new DropDownChoice("course", courseModel, new CourseListModel(courseService), new ChoiceRenderer<Course>()) {
            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }
        }.add(new ErrorBehavior()));
        form.add(new DropDownChoice("institution", institutionModel, new ExamRegistrationSearchPage.InstitutionsModel()));
        form.add(new TextField<String>("candidateNumber", candidateNumberModel));
        form.add(new TextField<String>("idNumber", idNumberModel));
        add(form);

        form.add(new Button("examRegistrationSearch") {
            @Override
            public void onSubmit() {
                setResponsePage(ExamRegistrationSearchPage.class);
            }
        });

        final IModel<List<ExamRegistration>> model = new LoadableDetachableModel<List<ExamRegistration>>() {
            @Override
            protected List<ExamRegistration> load() {
                if ((examSetting == null) && (institution == null) && (course == null) && (candidateNumber == null)
                        && (idNumber == null)) {
                    return new ArrayList<ExamRegistration>();
                } else {
                    return examRegistrationService.getAllExamCandidates(examSetting, course, institution,
                            candidateNumber, idNumber, null, null, null,null,null);
                }
            }
        };

        // Exam Registration List Data View Panel
        add(new ExamCandidatesDataViewPanel("examRegistrationDataListPanel", model));
    }

    private class InstitutionsModel extends LoadableDetachableModel<List<? extends Institution>> {

        protected List<? extends Institution> load() {
            if (course != null) {
                return (List<Institution>) hrisComparator.sort((new ArrayList<Institution>(courseService.get(course.getId()).getTrainingInstitutions())));
            } else {
                return new ArrayList<Institution>();
            }
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
