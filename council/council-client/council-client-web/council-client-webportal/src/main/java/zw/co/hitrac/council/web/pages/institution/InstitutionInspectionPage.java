package zw.co.hitrac.council.web.pages.institution;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.InstitutionInspection;
import zw.co.hitrac.council.business.service.InstitutionInspectionService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.CustomDateTextField;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

import java.util.Arrays;
import java.util.List;

//~--- JDK imports ------------------------------------------------------------

/**
 *
 * @author Constance Mabaso
 */
public class InstitutionInspectionPage extends TemplatePage {

    @SpringBean
    private InstitutionInspectionService institutionInspectionService;

    public InstitutionInspectionPage(final IModel<Application> applicationModel) {
        menuPanelManager.getApplicationPanel().setTopMenuCurrent(true);
        CompoundPropertyModel<InstitutionInspection> model = new CompoundPropertyModel<InstitutionInspection>(
                new InstitutionInspectionPage.LoadableDetachableInstitutionInspectionModel(applicationModel.getObject()));

        model.getObject().setApplication(applicationModel.getObject());
        setDefaultModel(model);

        Form<InstitutionInspection> form = new Form<InstitutionInspection>("form",
                (IModel<InstitutionInspection>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                institutionInspectionService.save(getModelObject());
                setResponsePage(new InstitutionApplicationViewPage(applicationModel.getObject().getId()));
            }
        };

        form.add(new TextArea<String>("findings").setVisible(Boolean.FALSE));
        form.add(new  CustomDateTextField("inspectionDate").setVisible(Boolean.FALSE));
        form.add(new  CustomDateTextField("expectedInspectionDate").setRequired(true).add(new ErrorBehavior()).add(DatePickerUtil.getDatePicker()));

        List decisionlist = Arrays.asList(new String[]{InstitutionInspection.DECISIONPENDING});

        form.add(new DropDownChoice("decisionStatus", decisionlist));
        form.add(new CheckBox("inspectionDone").setVisible(Boolean.FALSE));
        form.add(new Link<Void>("institutionViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionApplicationViewPage(applicationModel.getObject().getId()));
            }
        });
        add(form);
        add(new Label("application.institution.name"));
    }

    public InstitutionInspectionPage(Long id) {
        menuPanelManager.getApplicationPanel().setTopMenuCurrent(true);
        CompoundPropertyModel<InstitutionInspection> model =
                new CompoundPropertyModel<InstitutionInspection>(
                new InstitutionInspectionPage.LoadableDetachableInstitutionInspectionModel(id));
        final Long applicationId = model.getObject().getApplication().getId();
        setDefaultModel(model);

        Form<InstitutionInspection> form = new Form<InstitutionInspection>("form",
                (IModel<InstitutionInspection>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                institutionInspectionService.save(getModelObject());
                setResponsePage(new InstitutionApplicationViewPage(applicationId));
            }
        };

        form.add(new Link<Void>("institutionViewPage") {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionApplicationViewPage(applicationId));
            }
        });
        form.add(new TextArea<String>("findings").setRequired(true).add(new ErrorBehavior()));
        form.add(new CustomDateTextField("inspectionDate").setRequired(true).add(new ErrorBehavior()).add(DatePickerUtil.getDatePicker()));
        form.add(new CustomDateTextField("expectedInspectionDate").setRequired(true).add(new ErrorBehavior()).add(DatePickerUtil.getDatePicker()));
        form.add(new CheckBox("inspectionDone"));

        List decisionlist = Arrays.asList(new String[]{InstitutionInspection.DECISIONPENDING,
            InstitutionInspection.DECISIONAPPROVED, InstitutionInspection.DECISIONDECLINED});

        form.add(new DropDownChoice("decisionStatus", decisionlist).setRequired(true).add(new ErrorBehavior()));
        add(form);
        add(new Label("application.institution.name"));
    }

    private final class LoadableDetachableInstitutionInspectionModel
            extends LoadableDetachableModel<InstitutionInspection> {

        private Long id;
        private Application application;

        public LoadableDetachableInstitutionInspectionModel(Application application) {
            this.application = application;
        }

        public LoadableDetachableInstitutionInspectionModel(Long id) {
            this.id = id;
        }

        @Override
        protected InstitutionInspection load() {
            InstitutionInspection institutionInspection = null;

            if (id == null) {
                institutionInspection = new InstitutionInspection();
                institutionInspection.setApplication(application);
            } else {
                institutionInspection = institutionInspectionService.get(id);
            }

            return institutionInspection;
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
