package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.IdentificationType;
import zw.co.hitrac.council.business.service.IdentificationTypeService;

/**
 *
 * @author charlesc
 */
public class IdentificationTypeViewPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private IdentificationTypeService identificationTypeService;

    public IdentificationTypeViewPage(Long id) {
        CompoundPropertyModel<IdentificationType> model =
            new CompoundPropertyModel<IdentificationType>(new LoadableDetachableIdentificationTypeModel(id));

        setDefaultModel(model);
        add(new Link<IdentificationType>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new IdentificationTypeEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", IdentificationTypeListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("identityType", model.bind("name")));
    }

    private final class LoadableDetachableIdentificationTypeModel extends LoadableDetachableModel<IdentificationType> {
        private Long id;

        public LoadableDetachableIdentificationTypeModel(Long id) {
            this.id = id;
        }

        @Override
        protected IdentificationType load() {
            return identificationTypeService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
