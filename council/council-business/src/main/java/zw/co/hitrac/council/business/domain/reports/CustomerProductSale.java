/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain.reports;

import java.io.Serializable;
import java.math.BigDecimal;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.accounts.ReceiptHeader;

/**
 *
 * @author tdhlakama
 */
public class CustomerProductSale implements Serializable {

    private ReceiptHeader receiptHeader;
    private Registrant registrant;
    private Institution institution;
    private BigDecimal amountPaid = BigDecimal.ZERO;
    private BigDecimal carryForward = BigDecimal.ZERO;

    public ReceiptHeader getReceiptHeader() {
        return receiptHeader;
    }

    public void setReceiptHeader(ReceiptHeader receiptHeader) {
        this.receiptHeader = receiptHeader;
    }

    public Registrant getRegistrant() {
        return registrant;
    }

    public void setRegistrant(Registrant registrant) {
        this.registrant = registrant;
    }

    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    public String getCustomer() {
        if (registrant != null) {
            return registrant.getFullname();
        }
        if (institution != null) {
            return institution.getName();
        }
        return "";
    }

    public BigDecimal getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }

    public BigDecimal getCarryForward() {
        return carryForward;
    }

    public void setCarryForward(BigDecimal carryForward) {
        this.carryForward = carryForward;
    }
}