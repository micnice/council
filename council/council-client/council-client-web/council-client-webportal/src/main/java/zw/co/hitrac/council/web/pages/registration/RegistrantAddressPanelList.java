/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.registration;

import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButton;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButtons;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogIcon;
import com.googlecode.wicket.jquery.ui.widget.dialog.MessageDialog;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantAddress;
import zw.co.hitrac.council.business.service.RegistrantAddressService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.utility.CustomDateTimeLabel;

/**
 *
 * @author kelvin
 */
public class RegistrantAddressPanelList extends Panel {

    @SpringBean
    private RegistrantAddressService registrantAddressService;

    public RegistrantAddressPanelList(String id, final IModel<Registrant> registrantModel) {
        super(id);

        add(new Link<Registrant>("registrantAddressLink") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantAddressEditPage(registrantModel));
            }
        });

        add(new PropertyListView<RegistrantAddress>("registrantAddressList", registrantAddressService.getAddresses(registrantModel.getObject(), null)) {
            @Override
            protected void populateItem(ListItem<RegistrantAddress> item) {
                Link<RegistrantAddress> viewLink = new Link<RegistrantAddress>("registrantAddressLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new RegistrantAddressEditPage(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                item.add(new Label("addressType"));
                item.add(new Label("fullAddress"));
                item.add(new Label("preferred"));

                item.add(new Label("createdBy"));
                item.add(new Label("modifiedBy"));
                item.add(CustomDateTimeLabel.forDate("dateCreated",
                        new PropertyModel<>(item.getModel(), "dateCreated")));
                item.add(CustomDateTimeLabel.forDate("dateModified",
                        new PropertyModel<>(item.getModel(), "dateModified")));

                final Long documentId = item.getModelObject().getId();
                final MessageDialog warningDialog = new MessageDialog("warningDialog", "Warning", "Are you sure you want to remove?", DialogButtons.YES_NO, DialogIcon.WARN) {
                    private static final long serialVersionUID = 1L;

                    @Override
                    public void onClose(AjaxRequestTarget target, DialogButton button) {
                        if (button != null && button.equals(LBL_YES)) {
                            try {
                                RegistrantAddress su = registrantAddressService.get(documentId);
                                su.setRetired(Boolean.TRUE);
                                registrantAddressService.save(su);
                                setResponsePage(new RegistrantViewPage(registrantModel.getObject().getId()));
                            } catch (Exception ex) {
                                ex.printStackTrace(System.out);
                            }
                        }
                    }
                };

                item.add(warningDialog);

                item.add(new AjaxLink("remove") {
                    private static final long serialVersionUID = 1L;

                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        warningDialog.open(target);
                    }
                });

                item.setVisible(!item.getModelObject().getRetired());
            }
        });

    }

    public RegistrantAddressPanelList(String id, final Institution institution) {
        super(id);

        add(new Link<Registrant>("registrantAddressLink") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantAddressEditPage(institution));
            }
        });

        add(new PropertyListView<RegistrantAddress>("registrantAddressList", registrantAddressService.getAddresses(null, institution)) {
            @Override
            protected void populateItem(ListItem<RegistrantAddress> item) {
                Link<RegistrantAddress> viewLink = new Link<RegistrantAddress>("registrantAddressLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new RegistrantAddressEditPage(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                item.add(new Label("addressType"));
                item.add(new Label("fullAddress"));
                item.add(new Label("preferred"));

                item.add(new Label("createdBy"));
                item.add(new Label("modifiedBy"));
                item.add(CustomDateTimeLabel.forDate("dateCreated",
                        new PropertyModel<>(item.getModel(), "dateCreated")));
                item.add(CustomDateTimeLabel.forDate("dateModified",
                        new PropertyModel<>(item.getModel(), "dateModified")));

                final Long documentId = item.getModelObject().getId();
                final MessageDialog warningDialog = new MessageDialog("warningDialog", "Warning", "Are you sure you want to remove?", DialogButtons.YES_NO, DialogIcon.WARN) {
                    private static final long serialVersionUID = 1L;

                    @Override
                    public void onClose(AjaxRequestTarget target, DialogButton button) {
                        if (button != null && button.equals(LBL_YES)) {
                            try {
                                RegistrantAddress su = registrantAddressService.get(documentId);
                                su.setRetired(Boolean.TRUE);
                                registrantAddressService.save(su);
                                setResponsePage(getPage());
                            } catch (Exception ex) {
                                ex.printStackTrace(System.out);
                            }
                        }
                    }
                };

                item.add(warningDialog);

                item.add(new AjaxLink("remove") {
                    private static final long serialVersionUID = 1L;

                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        warningDialog.open(target);
                    }
                });

                item.setVisible(!item.getModelObject().getRetired());
            }
        });

    }

    public RegistrantAddressPanelList(String id, final Institution institution, final Boolean cgs) {
        super(id);

        add(new Link<Registrant>("registrantAddressLink") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantAddressEditPage(institution));
            }

            @Override
            protected void onConfigure() {
                setVisible(cgs);
            }
        });

        add(new PropertyListView<RegistrantAddress>("registrantAddressList", registrantAddressService.getAddresses(null, institution)) {
            @Override
            protected void populateItem(ListItem<RegistrantAddress> item) {
                Link<RegistrantAddress> viewLink = new Link<RegistrantAddress>("registrantAddressLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new RegistrantAddressEditPage(getModelObject().getId()));
                    }

                    @Override
                    protected void onConfigure() {
                        setVisible(cgs);
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                item.add(new Label("addressType"));
                item.add(new Label("fullAddress"));
                item.add(new Label("preferred"));

                item.add(new Label("createdBy"));
                item.add(new Label("modifiedBy"));
                item.add(CustomDateTimeLabel.forDate("dateCreated",
                        new PropertyModel<>(item.getModel(), "dateCreated")));
                item.add(CustomDateTimeLabel.forDate("dateModified",
                        new PropertyModel<>(item.getModel(), "dateModified")));

                final Long documentId = item.getModelObject().getId();
                final MessageDialog warningDialog = new MessageDialog("warningDialog", "Warning", "Are you sure you want to remove?", DialogButtons.YES_NO, DialogIcon.WARN) {
                    private static final long serialVersionUID = 1L;

                    @Override
                    public void onClose(AjaxRequestTarget target, DialogButton button) {
                        if (button != null && button.equals(LBL_YES)) {
                            try {
                                RegistrantAddress su = registrantAddressService.get(documentId);
                                su.setRetired(Boolean.TRUE);
                                registrantAddressService.save(su);
                                setResponsePage(new RegistrantViewPage(institution.getId()));
                            } catch (Exception ex) {
                                ex.printStackTrace(System.out);
                            }
                        }
                    }
                };

                item.add(warningDialog);

                item.add(new AjaxLink("remove") {
                    private static final long serialVersionUID = 1L;

                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        warningDialog.open(target);
                    }
                });
                item.setVisible(!item.getModelObject().getRetired());
            }
        });

    }
}
