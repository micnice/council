package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.RevisionListener;
import zw.co.hitrac.council.business.utils.Util;

import java.util.Optional;

/**
 * Created by scott on 09/12/2016.
 */
public class CustomRevisionListener implements RevisionListener {

    @Override
    public void newRevision(Object revisionEntity) {

        BaseRevisionEntity baseRevisionEntity = (BaseRevisionEntity) revisionEntity;
        final Optional<User> springSecurityUser = Util.getCurrentUser();

        springSecurityUser.ifPresent(u -> baseRevisionEntity.setUsername(u.getUsername()));

    }
}
