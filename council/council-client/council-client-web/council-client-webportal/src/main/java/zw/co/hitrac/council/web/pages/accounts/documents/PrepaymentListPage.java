package zw.co.hitrac.council.web.pages.accounts.documents;

import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.accounts.Prepayment;
import zw.co.hitrac.council.business.service.accounts.PrepaymentService;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.CustomDateLabel;

/**
 *
 * @author Michael Matiashe
 */
public class PrepaymentListPage extends TemplatePage {

    @SpringBean
    private PrepaymentService prepaymentService;
    private HrisComparator hrisComparator = new HrisComparator();

    public PrepaymentListPage() {

        IModel<List<Prepayment>> model = new LoadableDetachableModel<List<Prepayment>>() {

            @Override
            protected List<Prepayment> load() {
                return ((List<Prepayment>) hrisComparator.sort(prepaymentService.findByCanceled(Boolean.FALSE)));
            }
        };

        PropertyListView<Prepayment> eachItem = new PropertyListView<Prepayment>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<Prepayment> item) {
                Link<Prepayment> viewLink = new Link<Prepayment>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new PrepaymentViewPage(getModelObject().getId()));
                    }
                };
                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                item.add(new CustomDateLabel("dateOfPayment"));
                item.add(new CustomDateLabel("dateOfDeposit"));
                item.add(new Label("institution"));
                item.add(new Label("balance"));
                item.add(new Label("amount"));
            }
        };

        add(eachItem);

    }
}
