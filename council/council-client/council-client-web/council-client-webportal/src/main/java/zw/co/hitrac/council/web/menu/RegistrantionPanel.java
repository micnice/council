package zw.co.hitrac.council.web.menu;

import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.GeneralParameters;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.web.pages.registration.RegistrantEditPage;
import zw.co.hitrac.council.web.pages.registration.RegistrantListPage;
import zw.co.hitrac.council.web.pages.registration.RegistrantTransferListPage;
import zw.co.hitrac.council.web.pages.registration.RegistrationViewPage;

/**
 *
 * @author Takunda Dhlakama
 * @author Edward Zengeni
 */
public class RegistrantionPanel extends MenuPanel {

    private boolean registrantionPageCurrent;
    private boolean registrantEditPageCurrent;
    private boolean registrantListPageCurrent;
    private boolean registrantTransferListCurrent;

    private Link<Void> registrationLink;
    private Link<Void> registrantEditPage;
    private Link<Void> registrantListPage;
    private Link<Void> registrantTransferListPage;

    private WebMarkupContainer childMenuLinks;
    @SpringBean
    private GeneralParametersService generalParametersService;

    public RegistrantionPanel(String id) {
        super(id);
        GeneralParameters generalParameters = generalParametersService.get();
        String name = generalParameters.getRegistrantName();
        if ((name == null) || name.trim().equals("")) {
            name = "Registrant";
        }
        registrationLink = new BookmarkablePageLink<Void>("registrationViewLink", RegistrationViewPage.class);
        add(registrationLink.add(new Label("registrantName", name)));
        add(childMenuLinks = new WebMarkupContainer("childMenuLinks"));
        childMenuLinks.add(registrantEditPage = new BookmarkablePageLink<Void>("registrantEditPage", RegistrantEditPage.class));
        childMenuLinks.add(registrantListPage = new BookmarkablePageLink<Void>("registrantListPage", RegistrantListPage.class));
        childMenuLinks.add(registrantTransferListPage = new BookmarkablePageLink<Void>("registrantDueToTransferListPage", RegistrantTransferListPage.class));
    }

    @Override
    protected void onConfigure() {
        childMenuLinks.setVisibilityAllowed(topMenuCurrent);
        addCurrentBehavior(registrationLink, topMenuCurrent);
        addCurrentBehavior(registrantEditPage, registrantEditPageCurrent);
        addCurrentBehavior(registrantListPage, registrantListPageCurrent);
        addCurrentBehavior(registrantTransferListPage, registrantTransferListCurrent);
    }

    public boolean isRegistrantEditPageCurrent() {
        return registrantEditPageCurrent;
    }

    public void setRegistrantEditPageCurrent(boolean registrantEditPageCurrent) {
        this.registrantEditPageCurrent = registrantEditPageCurrent;
    }

    public boolean isRegistrantListPageCurrent() {
        return registrantListPageCurrent;
    }

    public void setRegistrantListPageCurrent(boolean registrantListPageCurrent) {
        this.registrantListPageCurrent = registrantListPageCurrent;
    }

    public boolean isRegistrantTransferListCurrent() {
        return registrantTransferListCurrent;
    }

    public void setRegistrantTransferListCurrent(boolean registrantTransferListCurrent) {
        this.registrantTransferListCurrent = registrantTransferListCurrent;
    }
}
