package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.ContactType;
import zw.co.hitrac.council.business.service.ContactTypeService;

import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author Matiashe Michael
 */
public class ContactTypeEditPage extends IAdministerDatabaseBasePage{
    
    @SpringBean
    private ContactTypeService contactTypeService;

    public ContactTypeEditPage(Long id) {
        setDefaultModel(new CompoundPropertyModel<ContactType>(new LoadableDetachableContactTypeModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink",AdministerDatabasePage.class));
        Form<ContactType> form=new Form<ContactType>("form",(IModel<ContactType>)getDefaultModel()) {
            @Override
            public void onSubmit(){
                contactTypeService.save(getModelObject());
                setResponsePage(new ContactTypeViewPage(getModelObject().getId()));
            }
        };
        
        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior())); 
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink",ContactTypeListPage.class));
        add(form);
    }
    
    
    private final class LoadableDetachableContactTypeModel extends LoadableDetachableModel<ContactType> {

        private Long id;

        public LoadableDetachableContactTypeModel(Long id) {
            this.id = id;
        }

        @Override
        protected ContactType load() {
            if(id==null){
                return new ContactType();
            }
            return contactTypeService.get(id);
        }
    }
    
    
}
