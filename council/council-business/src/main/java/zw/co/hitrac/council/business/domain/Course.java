package zw.co.hitrac.council.business.domain;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;
import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.utils.CaseUtil;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Takunda Dhlakama
 * @author Judge Muzinda
 * @author Charles Chigoriwa
 * @author Matiashe Michael
 */
@Entity
@Table(name = "course")
@Audited
@XmlRootElement
public class Course extends BaseEntity implements Serializable, Comparable<Course> {

    private static final long serialVersionUID = 1L;
    private Qualification qualification;
    private Set<ModulePaper> modulePapers = new HashSet<ModulePaper>();
    private Set<Qualification> qualifications = new HashSet<Qualification>();
    private CourseType courseType;
    private Integer weight;
    private Groups courseGroup;
    private Set<Requirement> requirements = new HashSet<Requirement>();
    private Boolean basic = Boolean.TRUE;
    private Integer courseDuration = 0; //Count is in months
    private String prefixName;
    private Tier tier;
    private RegisterType registerType;
    private String isoCode;
    private Set<Institution> trainingInstitutions = new HashSet<Institution>();
    private ProductIssuanceType productIssuanceType;
    private Boolean registered = Boolean.FALSE;
    private Integer lastCourseRegistrationNumber;
    private Boolean special = Boolean.FALSE;
    private Boolean hasCGSApplication = Boolean.TRUE;
    private String certificatePrefix;

    public String getCertificatePrefix() {
        return CaseUtil.upperCase(certificatePrefix);
    }

    public void setCertificatePrefix(String certificatePrefix) {
        this.certificatePrefix = certificatePrefix;
    }

    public Integer getLastCourseRegistrationNumber() {
        if (lastCourseRegistrationNumber == null) {
            return 1;
        }
        return lastCourseRegistrationNumber;
    }

    public void setLastCourseRegistrationNumber(Integer lastCourseRegistrationNumber) {
        this.lastCourseRegistrationNumber = lastCourseRegistrationNumber;








    }

    @ManyToMany
    public Set<Qualification> getQualifications() {
        return qualifications;
    }

    public void setQualifications(Set<Qualification> qualifications) {
        this.qualifications = qualifications;
    }

    @ManyToMany
    public Set<Requirement> getRequirements() {
        return requirements;
    }

    public void setRequirements(Set<Requirement> requirements) {
        this.requirements = requirements;
    }

    @Transient
    public List<Requirement> getExamRequirements() {
        Set<Requirement> examRequirements = new HashSet<Requirement>();
        if (requirements != null && !requirements.isEmpty()) {
            for (Requirement requirement : getRequirements()) {
                if (requirement.getExamRequirement()) {
                    examRequirements.add(requirement);
                }
            }
        }
        return new ArrayList<Requirement>(examRequirements);
    }

    @Enumerated(EnumType.STRING)
    public Groups getCourseGroup() {
        return courseGroup;
    }

    public void setCourseGroup(Groups courseGroup) {
        this.courseGroup = courseGroup;
    }

    @Enumerated(EnumType.STRING)
    @ManyToOne
    public CourseType getCourseType() {
        return courseType;
    }

    public void setCourseType(CourseType courseType) {
        this.courseType = courseType;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    @OneToMany(mappedBy = "course", cascade = CascadeType.ALL)
    public Set<ModulePaper> getModulePapers() {
        return modulePapers;
    }

    public void setModulePapers(Set<ModulePaper> modulePapers) {
        this.modulePapers = modulePapers;
    }

    @Transient
    public List<ModulePaper> getCoursePapers() {
        return new ArrayList<ModulePaper>(getModulePapers());
    }

    @ManyToOne
    public Qualification getQualification() {
        return qualification;
    }

    public void setQualification(Qualification qualification) {
        this.qualification = qualification;
    }

    public Boolean getBasic() {
        if (basic == null) {
            return Boolean.TRUE;
        }
        return basic;
    }

    public void setBasic(Boolean basic) {
        this.basic = basic;
    }

    public Integer getCourseDuration() {
        if (courseDuration == null) {
            return 0;
        }
        return courseDuration;
    }

    public void setCourseDuration(Integer courseDuration) {
        this.courseDuration = courseDuration;
    }

    @Transient
    public String getBasicCourse() {
        return basic ? "YES" : "NO";
    }

    public String getPrefixName() {
        return prefixName;
    }

    public void setPrefixName(String prefixName) {
        this.prefixName = prefixName;
    }

    @ManyToOne
    public Tier getTier() {
        return tier;
    }

    public void setTier(Tier tier) {
        this.tier = tier;
    }

    @ManyToOne
    public RegisterType getRegisterType() {
        return registerType;
    }

    public void setRegisterType(RegisterType registerType) {
        this.registerType = registerType;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

    @ManyToMany
    public Set<Institution> getTrainingInstitutions() {
        return trainingInstitutions;
    }

    public void setTrainingInstitutions(Set<Institution> trainingInstitutions) {
        this.trainingInstitutions = trainingInstitutions;
    }

    public Boolean getRegistered() {
        if (registered == null) {
            return Boolean.FALSE;
        }
        return registered;
    }

    public void setRegistered(Boolean registered) {
        this.registered = registered;
    }

    @Enumerated(EnumType.STRING)
    public ProductIssuanceType getProductIssuanceType() {
        return productIssuanceType;
    }

    public void setProductIssuanceType(ProductIssuanceType productIssuanceType) {
        this.productIssuanceType = productIssuanceType;
    }

    public Boolean getSpecial() {
        if (special == null) {
            return Boolean.FALSE;
        }
        return special;
    }

    public void setSpecial(Boolean special) {
        this.special = special;
    }

    public Boolean getHasCGSApplication() {
        if (hasCGSApplication == null) {
            return Boolean.TRUE;
        }
        return hasCGSApplication;
    }

    public void setHasCGSApplication(Boolean hasCGSApplication) {
        this.hasCGSApplication = hasCGSApplication;
    }

    @Override
    public String toString() {

        return super.toString();
    }

    @Override
    //For use in jasper reports and other uses
    public int compareTo(Course o) {

        return ComparisonChain.start()
                .compare(this.getName(), o.getName(), Ordering.natural().nullsLast())
                .result();
    }
}
