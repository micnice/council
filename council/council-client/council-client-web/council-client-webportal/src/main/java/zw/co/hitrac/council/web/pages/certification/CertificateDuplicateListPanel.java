package zw.co.hitrac.council.web.pages.certification;

import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PageableListView;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigator;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.accounts.ReceiptHeader;
import zw.co.hitrac.council.business.domain.reports.ItemCase;
import zw.co.hitrac.council.business.process.BillingProcess;
import zw.co.hitrac.council.business.process.ProductFinder;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.business.service.accounts.AccountsParametersService;
import zw.co.hitrac.council.business.service.accounts.ReceiptHeaderService;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.business.utils.HrisComparator;
import zw.co.hitrac.council.business.utils.StringFormattingUtil;
import zw.co.hitrac.council.reports.*;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.accounts.documents.DebtComponentsPage;
import zw.co.hitrac.council.web.utility.CustomDateLabel;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

import java.util.*;

/**
 * Created by edwin magodi
 */
public class CertificateDuplicateListPanel extends Panel {


    private static final String TOP_LOGO = "zw/co/hitrac/council/reports/images/logo1.jpg";
    private static final String WATERMARK_LOGO = "zw/co/hitrac/council/reports/images/logo.png";
    @SpringBean
    private ProductIssuanceService productIssuanceService;
    @SpringBean
    private ProductFinder productFinder;
    @SpringBean
    private RegistrantQualificationService registrantQualificationService;
    @SpringBean
    private RegistrantAddressService registrantAddressService;
    @SpringBean
    private RegistrantConditionService registrantConditionService;
    @SpringBean
    private RegistrationService registrationService;
    @SpringBean
    private RegistrantDisciplinaryService registrantDisciplinaryService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private RegistrantClearanceService registrantClearanceService;
    private Boolean docExport = Boolean.FALSE;
    @SpringBean
    private CourseConditionService courseConditionService;
    @SpringBean
    private ReceiptHeaderService receiptHeaderService;
    @SpringBean
    private AccountsParametersService accountsParametersService;
    @SpringBean
    private BillingProcess billingProcess;

    public CertificateDuplicateListPanel(String id, IModel<List<ProductIssuance>> model) {

        super(id);
        if (!generalParametersService.get().getRegistrationByApplication()) {
            docExport = Boolean.TRUE;
        } else {
            docExport = Boolean.FALSE;
        }

        PageableListView<ProductIssuance> eachItem = new PageableListView<ProductIssuance>("eachItem", model, 20) {

            @Override
            protected void populateItem(ListItem<ProductIssuance> item) {

                item.setModel(new CompoundPropertyModel<ProductIssuance>(item.getModel()));
                item.add(new Link<ProductIssuance>("processDuplicate", item.getModel()) {

                    @Override
                    public void onClick() {

                        billingProcess.billDuplicateCertificate(item.getModelObject());

                        setResponsePage(new DebtComponentsPage(item.getModelObject().getRegistrant().getId()));


                    }
                });
                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                item.add(new Label("productNumber"));
                item.add(new Label("productIssuanceType"));
                item.add(new CustomDateLabel("issueDate"));
                item.add(new Label("expiryDate"));
                item.add(new Label("createdBy"));
                item.add(new Label("modifiedBy"));
                item.add(new CustomDateLabel("dateCreated"));
                item.add(new CustomDateLabel("dateModified"));

            }
        };

        add(new PagingNavigator("navigator", eachItem));
        add(eachItem);
    }

}
