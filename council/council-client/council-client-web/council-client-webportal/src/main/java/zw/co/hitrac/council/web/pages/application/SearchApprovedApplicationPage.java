package zw.co.hitrac.council.web.pages.application;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.service.ApplicationService;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import zw.co.hitrac.council.business.domain.ApplicationPurpose;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.utils.DateUtil;
import zw.co.hitrac.council.reports.ApplicationListReport;
import zw.co.hitrac.council.reports.ApplicationPurposeReport;
import zw.co.hitrac.council.reports.ApplicationTypeReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 * @author Michael Matiashe
 */
public class SearchApprovedApplicationPage extends TemplatePage {

    private String searchtxt;
    @SpringBean
    private ApplicationService applicationService;
    private Date startDate, endDate;
    private ApplicationPurpose applicationPurpose;
    @SpringBean
    private GeneralParametersService generalParametersService;

    // private ApplicationPurpose applicationPurpose;
    public SearchApprovedApplicationPage() {
        menuPanelManager.getApplicationPanel().setTopMenuCurrent(true);
        menuPanelManager.getApplicationPanel().setSearchApprovedApplication(true);

        PropertyModel<String> messageModel = new PropertyModel<String>(this, "searchtxt");
        PropertyModel<Date> startDateModel = new PropertyModel<Date>(this, "startDate");
        PropertyModel<Date> endDateModel = new PropertyModel<Date>(this, "endDate");
        PropertyModel<ApplicationPurpose> applicationPurposeModel = new PropertyModel<ApplicationPurpose>(this, "applicationPurpose");
        Form<?> form = new Form("form");

        form.add(new TextField<String>("searchtxt", messageModel));

        form.add(new TextField<Date>("startDate", startDateModel).add(DatePickerUtil.getDatePicker()));
        form.add(new TextField<Date>("endDate", endDateModel).add(DatePickerUtil.getDatePicker()));

        List<ApplicationPurpose> applicationPurposes = new ArrayList<ApplicationPurpose>(Arrays.asList(ApplicationPurpose.values()));
        if (!generalParametersService.get().getRegistrationByApplication()) {
            applicationPurposes.remove(ApplicationPurpose.INTERN_REGISTRATION);
            applicationPurposes.remove(ApplicationPurpose.MAIN_REGISTRATION);
            applicationPurposes.remove(ApplicationPurpose.SPECIALIST_REGISTRATION);
            applicationPurposes.remove(ApplicationPurpose.STUDENT_REGISTRATION);
            applicationPurposes.remove(ApplicationPurpose.UNRESTRICTED_PRACTICING_CERTIFICATE);
        }
        form.add(new DropDownChoice("applicationPurpose", applicationPurposeModel, applicationPurposes));
        add(form);

        IModel<List<Application>> model = new LoadableDetachableModel<List<Application>>() {
            @Override
            protected List<Application> load() {
                if ((searchtxt == null) && (startDate == null) && (endDate == null) && (applicationPurpose == null)) {
                    return new ArrayList<Application>();
                } else {
                    return applicationService.getApplications(searchtxt, Application.APPLICATIONAPPROVED, startDate, endDate, applicationPurpose, null, null);
                }
            }
        };

        // Registrant List Data View Panel
        add(new ApprovedApplicationsDataViewPanel("unApprovedApplicationsDataListPanel", model));
        add(new FeedbackPanel("feedback"));

        form.add(new Button("report") {
            @Override
            public void onSubmit() {
                Map parameters = new HashMap();
                try {
                    if (endDate == null) {
                        endDate = new Date();
                    }
                    if (applicationPurpose == null) {
                        ApplicationListReport applicationListReport = new ApplicationListReport();
                        ContentType contentType = ContentType.PDF;
                        parameters.put("comment", "All Applications Done");
                        ByteArrayResource resource = ReportResourceUtils.getReportResource(applicationListReport, contentType, applicationService.getApplications(searchtxt, Application.APPLICATIONAPPROVED, startDate, endDate, applicationPurpose, null, null), parameters);
                        IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                RequestCycle.get().getResponse(), null);
                        resource.respond(a);
                        // To make Wicket stop processing form after sending response
                        RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                    }

                    if (applicationPurpose != null && startDate == null) {
                        ApplicationTypeReport applicationTypeReport = new ApplicationTypeReport();
                        ContentType contentType = ContentType.PDF;
                        parameters.put("comment", "Application Purpose - ( " + applicationPurpose.getName() + " ).");
                        ByteArrayResource resource = ReportResourceUtils.getReportResource(applicationTypeReport, contentType, applicationService.getApplications(searchtxt, Application.APPLICATIONAPPROVED, startDate, endDate, applicationPurpose, null, null), parameters);
                        IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                RequestCycle.get().getResponse(), null);
                        resource.respond(a);
                        // To make Wicket stop processing form after sending response
                        RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                    }

                    if (applicationPurpose != null && startDate == null) {
                        ApplicationTypeReport applicationTypeReport = new ApplicationTypeReport();
                        ContentType contentType = ContentType.PDF;
                        parameters.put("comment", "Application Purpose - ( " + applicationPurpose.getName() + " ).");
                        ByteArrayResource resource = ReportResourceUtils.getReportResource(applicationTypeReport, contentType, applicationService.getApplications(searchtxt, Application.APPLICATIONAPPROVED, startDate, endDate, applicationPurpose, null, null), parameters);
                        IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                RequestCycle.get().getResponse(), null);
                        resource.respond(a);
                        // To make Wicket stop processing form after sending response
                        RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                    }

                    if (applicationPurpose != null && startDate != null) {
                        ApplicationTypeReport applicationTypeReport = new ApplicationTypeReport();
                        ContentType contentType = ContentType.PDF;
                        parameters.put("comment", "Application Purpose - ( " + applicationPurpose.getName() + " )." + DateUtil.getDate(startDate) + " to " + DateUtil.getDate(endDate));
                        ByteArrayResource resource = ReportResourceUtils.getReportResource(applicationTypeReport, contentType, applicationService.getApplications(searchtxt, Application.APPLICATIONAPPROVED, startDate, endDate, applicationPurpose, null, null), parameters);
                        IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                RequestCycle.get().getResponse(), null);
                        resource.respond(a);
                        // To make Wicket stop processing form after sending response
                        RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                    }

                    if (applicationPurpose != null && startDate != null) {
                        ApplicationPurposeReport applicationPurposeReport = new ApplicationPurposeReport();
                        ContentType contentType = ContentType.PDF;
                        parameters.put("comment", "Application Purpose - ( " + applicationPurpose.getName() + " )." + " from " + DateUtil.getDate(startDate) + " to " + DateUtil.getDate(endDate));
                        ByteArrayResource resource = ReportResourceUtils.getReportResource(applicationPurposeReport, contentType, applicationService.getApplications(searchtxt, Application.APPLICATIONAPPROVED, startDate, endDate, applicationPurpose, null, null), parameters);
                        IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                RequestCycle.get().getResponse(), null);
                        resource.respond(a);
                        // To make Wicket stop processing form after sending response
                        RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                    }

                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

    }
}


//~ Formatted by Jindent --- http://www.jindent.com
