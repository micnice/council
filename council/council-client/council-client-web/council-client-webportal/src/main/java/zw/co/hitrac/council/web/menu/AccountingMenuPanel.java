package zw.co.hitrac.council.web.menu;

import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import zw.co.hitrac.council.business.utils.Role;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.pages.accounts.AccountingPage;
import zw.co.hitrac.council.web.pages.accounts.AccountsReceivablePage;
import zw.co.hitrac.council.web.pages.accounts.BankBookPage;
import zw.co.hitrac.council.web.pages.accounts.GeneralLedgerPage;
import zw.co.hitrac.council.web.pages.accounts.InventoryModulePage;
import static zw.co.hitrac.council.web.utility.GeneralUtils.getRoles;

/**
 *
 * @author Takunda Dhlakama
 */
public class AccountingMenuPanel extends MenuPanel {

    private Link<Void> accountingLink;
    private boolean accountsReceivable;
    private boolean bankBook;
    private boolean generalLedger;
    private boolean inventoryModule;
    private Link<Void> accountsReceivableLink;
    private Link<Void> bankBookLink;
    private Link<Void> generalLedgerLink;
    private Link<Void> inventoryModuleLink;
    private WebMarkupContainer childMenuLinks;

    public AccountingMenuPanel(String id) {
        super(id);
        add(accountingLink = new BookmarkablePageLink<Void>("accountingLink", AccountingPage.class) {
            @Override
            protected void onConfigure() {
                setVisibilityAllowed(CouncilSession.get().hasAnyRole(new Roles(getRoles(Role.SYSTEM_ADMINISTRATOR, Role.ACCOUNTS_OFFICER))));
            }
        });
        add(childMenuLinks = new WebMarkupContainer("childMenuLinks"));
        childMenuLinks.add(accountsReceivableLink = new BookmarkablePageLink<Void>("accountsReceivableLink", AccountsReceivablePage.class));
        childMenuLinks.add(bankBookLink = new BookmarkablePageLink<Void>("bankBookLink", BankBookPage.class));
        childMenuLinks.add(generalLedgerLink = new BookmarkablePageLink<Void>("generalLedgerLink", GeneralLedgerPage.class));
        childMenuLinks.add(inventoryModuleLink = new BookmarkablePageLink<Void>("inventoryModuleLink", InventoryModulePage.class));
    }

    @Override
    protected void onConfigure() {
        addCurrentBehavior(accountingLink, topMenuCurrent);
        childMenuLinks.setVisibilityAllowed(topMenuCurrent);
        addCurrentBehavior(generalLedgerLink, generalLedger);
        addCurrentBehavior(bankBookLink, bankBook);
        addCurrentBehavior(accountsReceivableLink, accountsReceivable);
        addCurrentBehavior(inventoryModuleLink, inventoryModule);
    }

    public Link<Void> getAccountingLink() {
        return accountingLink;
    }

    public void setAccountingLink(Link<Void> accountingLink) {
        this.accountingLink = accountingLink;
    }

    public boolean isInventoryModule() {
        return inventoryModule;
    }

    public void setInventoryModule(boolean inventoryModule) {
        this.inventoryModule = inventoryModule;
    }

    public Link<Void> getAccountsReceivableLink() {
        return accountsReceivableLink;
    }

    public void setAccountsReceivableLink(Link<Void> accountsReceivableLink) {
        this.accountsReceivableLink = accountsReceivableLink;
    }

    public Link<Void> getBankBookLink() {
        return bankBookLink;
    }

    public void setBankBookLink(Link<Void> bankBookLink) {
        this.bankBookLink = bankBookLink;
    }

    public Link<Void> getGeneralLedgerLink() {
        return generalLedgerLink;
    }

    public void setGeneralLedgerLink(Link<Void> generalLedgerLink) {
        this.generalLedgerLink = generalLedgerLink;
    }

    public Link<Void> getInventoryModuleLink() {
        return inventoryModuleLink;
    }

    public void setInventoryModuleLink(Link<Void> inventoryModuleLink) {
        this.inventoryModuleLink = inventoryModuleLink;
    }

    public WebMarkupContainer getChildMenuLinks() {
        return childMenuLinks;
    }

    public void setChildMenuLinks(WebMarkupContainer childMenuLinks) {
        this.childMenuLinks = childMenuLinks;
    }

    public boolean isAccountsReceivable() {
        return accountsReceivable;
    }

    public void setAccountsReceivable(boolean accountsReceivable) {
        this.accountsReceivable = accountsReceivable;
    }

    public boolean isBankBook() {
        return bankBook;
    }

    public void setBankBook(boolean bankBook) {
        this.bankBook = bankBook;
    }

    public boolean isGeneralLedger() {
        return generalLedger;
    }

    public void setGeneralLedger(boolean generalLedger) {
        this.generalLedger = generalLedger;
    }
}
