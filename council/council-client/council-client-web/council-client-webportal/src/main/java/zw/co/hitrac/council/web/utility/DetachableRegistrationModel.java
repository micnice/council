package zw.co.hitrac.council.web.utility;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.model.LoadableDetachableModel;

import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.service.RegistrationService;

/**
 *
 * @author Takunda Dhlakama
 */
public class DetachableRegistrationModel extends LoadableDetachableModel<Registration> {
    private static final long serialVersionUID = 1L;

    /**
     * database identity of the Registration
     */
    private final long id;

    /**
     * service dao reference - must be a wicket-wrapped proxy, holding onto a reference
     * to the real service dao will cause its serialization into session or a
     * not-serializable exception when the servlet container serializes the
     * session.
     */
    private final RegistrationService service;

    /**
     * Constructor
     *
     * @param id
     * @param service
     */
    public DetachableRegistrationModel(Long id, RegistrationService service) {
        this.id      = id;
        this.service = service;
    }

    /**
     * Loads the Registration from the database
     *
     * @see LoadableDetachableModel#load()
     */
    @Override
    protected Registration load() {
        Registration r = service.get(id);

        if (r == null) {
            return new Registration();
        } else {
            return r;
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
