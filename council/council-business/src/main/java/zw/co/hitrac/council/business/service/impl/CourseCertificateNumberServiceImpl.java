package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.CourseCertificateNumberDAO;
import zw.co.hitrac.council.business.domain.CourseCertificateNumber;
import zw.co.hitrac.council.business.service.CourseCertificateNumberService;

import java.util.List;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Register;

/**
 *
 * @author Charles Chigoriwa
 */
@Service
@Transactional
public class CourseCertificateNumberServiceImpl implements CourseCertificateNumberService {

    @Autowired
    private CourseCertificateNumberDAO courseCertificateNumberDAO;

    @Transactional
    public CourseCertificateNumber save(CourseCertificateNumber courseCertificateNumber) {
        return courseCertificateNumberDAO.save(courseCertificateNumber);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<CourseCertificateNumber> findAll() {
        return courseCertificateNumberDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public CourseCertificateNumber get(Long id) {
        return courseCertificateNumberDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<CourseCertificateNumber> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setCourseCertificateNumberDAO(CourseCertificateNumberDAO courseCertificateNumberDAO) {

        this.courseCertificateNumberDAO = courseCertificateNumberDAO;
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public CourseCertificateNumber get(Course course) {
        return courseCertificateNumberDAO.get(course);
    }

    @Override
    public void resetCertificateNumbers() {
        courseCertificateNumberDAO.resetCertificateNumbers();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public CourseCertificateNumber get(Course course, Register register) {
        return courseCertificateNumberDAO.get(course, register);
    }

}
