package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.ProductIssuanceDAO;
import zw.co.hitrac.council.business.domain.ProductIssuance;
import zw.co.hitrac.council.business.domain.ProductIssuanceType;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.service.ProductIssuanceService;

import java.util.Date;
import java.util.List;
import zw.co.hitrac.council.business.process.ProductIssuer;

/**
 *
 * @author Edward Zengeni
 * @author Michael Matiashe
 */
@Service
@Transactional
public class ProductIssuanceServiceImpl implements ProductIssuanceService {

    @Autowired
    private ProductIssuanceDAO productIssuanceDAO;

    @Autowired
    private ProductIssuer productIssuer;

    @Transactional
    public ProductIssuance save(ProductIssuance productIssuance) {

        productIssuance = productIssuanceDAO.save(productIssuance);

        if (productIssuance.getPracticeCertificateNumber() == null && productIssuance.getProductIssuanceType().equals(ProductIssuanceType.PRACTICING_CERTIFICATE)) {
            productIssuer.savePracticeCertificate(productIssuance);
        }

        return productIssuance;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ProductIssuance> findAll() {
        return productIssuanceDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public ProductIssuance get(Long id) {
        return productIssuanceDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ProductIssuance> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * A DAO setter method to facilitate mocking
     *
     * @param productIssuanceDAO
     */
    public void setProductIssuanceDAO(ProductIssuanceDAO productIssuanceDAO) {
        this.productIssuanceDAO = productIssuanceDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ProductIssuance> getProductIssuances(Registrant registrant) {
        return this.productIssuanceDAO.getProductIssuances(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ProductIssuance> getProductIssuances(ProductIssuanceType productIssuanceType) {

        return productIssuanceDAO.getProductIssuances(productIssuanceType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ProductIssuance> getProductIssuances(ProductIssuanceType productIssuanceType, Registrant registrant) {

        return productIssuanceDAO.getProductIssuances(productIssuanceType, registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ProductIssuance> getProductAllIssuances(Registrant registrant) {
        return productIssuanceDAO.getProductAllIssuances(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ProductIssuance> getProductUnClosedIssuances(ProductIssuanceType productIssuanceType) {

        return productIssuanceDAO.getProductUnClosedIssuances(productIssuanceType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ProductIssuance> getProductIssuances(ProductIssuanceType certificateType, Date startDate, Date endDate) {

        return productIssuanceDAO.getProductIssuances(certificateType, startDate, endDate);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ProductIssuance> getProductIssuanceList(String query, String productNumber) {
        return productIssuanceDAO.getProductIssuanceList(query, productNumber);

    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<ProductIssuance> getUnIssuedPractisingCertificates(Registrant registrant) {
        return productIssuanceDAO.getUnIssuedPractisingCertificates(registrant);
    }

    public void checkDuplicate(ProductIssuance productIssuance) {

    }
}
