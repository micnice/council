
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.RegistrantCpdItemConfig;
import zw.co.hitrac.council.business.service.RegistrantCpdItemConfigService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;

/**
 *
 * @author hitrac
 */
public class RegistrantCpdItemConfigEditPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private RegistrantCpdItemConfigService registrantCpdItemConfigService;

    public RegistrantCpdItemConfigEditPage(Long id) {
        setDefaultModel(
            new CompoundPropertyModel<RegistrantCpdItemConfig>(
                new RegistrantCpdItemConfigEditPage.LoadableDetachableRegistrantCpdItemConfigModel(id)));
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));

        Form<RegistrantCpdItemConfig> form = new Form<RegistrantCpdItemConfig>("form",
                                                 (IModel<RegistrantCpdItemConfig>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                registrantCpdItemConfigService.save(getModelObject());
                setResponsePage(new RegistrantCpdItemConfigViewPage(getModelObject().getId()));
            }
        };

        form.add(new TextField<String>("name").setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<String>("description").add(new ErrorBehavior()));
        form.add(new TextField<String>("totalCpdPointsPermissable").setRequired(true).add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink", RegistrantCpdItemConfigListPage.class));
        add(form);
    }

    private final class LoadableDetachableRegistrantCpdItemConfigModel
            extends LoadableDetachableModel<RegistrantCpdItemConfig> {
        private Long id;

        public LoadableDetachableRegistrantCpdItemConfigModel(Long id) {
            this.id = id;
        }

        @Override
        protected RegistrantCpdItemConfig load() {
            if (id == null) {
                return new RegistrantCpdItemConfig();
            }

            return registrantCpdItemConfigService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
