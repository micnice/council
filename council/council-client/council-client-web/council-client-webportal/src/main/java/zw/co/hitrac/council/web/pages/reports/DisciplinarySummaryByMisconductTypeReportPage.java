/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.reports;

import java.util.Arrays;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.PageReference;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.MisconductType;
import zw.co.hitrac.council.business.domain.RegistrantDisciplinary;
import zw.co.hitrac.council.business.service.MisconductTypeService;
import zw.co.hitrac.council.business.service.RegistrantDisciplinaryService;
import zw.co.hitrac.council.business.utils.StringFormattingUtil;
import zw.co.hitrac.council.reports.DefaultReport;
import zw.co.hitrac.council.reports.DisciplinarySummaryByMisconductTypeReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.GeneralUtils;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author clive
 */
public class DisciplinarySummaryByMisconductTypeReportPage extends TemplatePage {

    @SpringBean
    private MisconductTypeService misconductTypeService;
    @SpringBean
    private RegistrantDisciplinaryService registrantDisciplinaryService;
    private Integer startYear;
    private Integer endYear;

    private MisconductType misconductType;
    private ContentType contentType;

    public DisciplinarySummaryByMisconductTypeReportPage(final PageReference pageReference) {

        PropertyModel<Integer> startYearModel = new PropertyModel<Integer>(this, "startYear");
        PropertyModel<Integer> endYearModel = new PropertyModel<Integer>(this, "endYear");
        PropertyModel<String> misconductTypeModel = new PropertyModel<String>(this, "misconductType");
        PropertyModel<ContentType> contentTypeModel = new PropertyModel<ContentType>(this, "contentType");
        Form<?> form = new Form("form");
        form.add(new DropDownChoice("startYear", startYearModel, GeneralUtils.generateYearList()).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("endYear", endYearModel, GeneralUtils.generateYearList()).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("contentType", contentTypeModel, Arrays.asList(ContentType.values())));
        form.add(new DropDownChoice("misconductType", misconductTypeModel, misconductTypeService.findAll()));
        add(form);

        form.add(new Link("returnLink") {

            public void onClick() {

                setResponsePage(pageReference.getPage());
            }
        });

        form.add(new Button("showSummaryList") {

            @Override
            public void onSubmit() {

                try {

                    Map parameters = new HashMap();

                    DefaultReport report = new DisciplinarySummaryByMisconductTypeReport();

                    if (misconductType == null) {

                        if (startYear.equals(endYear)) {

                            parameters.put("comment", StringFormattingUtil.join("Summary of All Disciplinary Cases for",
                                    startYear));
                        } else {

                            parameters.put("comment", StringFormattingUtil.join("Summary of All Disciplinary Cases from",
                                    startYear, "to", endYear));
                        }
                    } else {

                        if (startYear.equals(endYear)) {

                            parameters.put("comment", StringFormattingUtil.join("Summary of All Disciplinary Cases for",
                                    misconductType.getName(), "for", startYear));
                        } else {

                            parameters.put("comment", StringFormattingUtil.join("Summary of All Disciplinary Cases for",
                                    misconductType.getName(), "from", startYear, "to", endYear));
                        }
                    }

                    ByteArrayResource resource = ReportResourceUtils.getReportResource(report, null,
                            registrantDisciplinaryService.find(misconductType, startYear, endYear), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);
                    resource.respond(a);
                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());

                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });

    }
}
