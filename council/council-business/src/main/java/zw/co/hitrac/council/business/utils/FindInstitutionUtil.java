/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.utils;

import java.io.Serializable;

/**
 *
 * @author Judge Muzinda
 */
public final class FindInstitutionUtil implements Serializable{
    
    static final String pattern = "^[A-Z]{1,}[0-9]{1,}";
    
    public static Boolean getInstitution(String id){
        return id.toUpperCase().matches(pattern) ? Boolean.TRUE : Boolean.FALSE;
    }
}
