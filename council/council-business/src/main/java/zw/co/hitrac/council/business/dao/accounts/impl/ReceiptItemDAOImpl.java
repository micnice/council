package zw.co.hitrac.council.business.dao.accounts.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.accounts.ReceiptItemDAO;
import zw.co.hitrac.council.business.dao.repo.accounts.ReceiptItemRepository;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.domain.TypeOfService;
import zw.co.hitrac.council.business.domain.User;
import zw.co.hitrac.council.business.domain.accounts.*;

/**
 * @author Michael Matiashe
 */
@Repository
public class ReceiptItemDAOImpl implements ReceiptItemDAO {

    @Autowired
    private ReceiptItemRepository receiptItemRepository;
    @PersistenceContext
    private EntityManager entityManager;

    public ReceiptItem save(ReceiptItem t) {
        return receiptItemRepository.save(t);
    }

    public List<ReceiptItem> findAll() {
        return receiptItemRepository.findAll();
    }

    public ReceiptItem get(Long id) {
        return receiptItemRepository.findOne(id);
    }

    public ReceiptItemRepository getReceiptItemRepository() {
        return receiptItemRepository;
    }

    public void setReceiptItemRepository(ReceiptItemRepository receiptItemRepository) {
        this.receiptItemRepository = receiptItemRepository;
    }

    public List<ReceiptItem> getReceiptItems(Product product, Date startDate, Date endDate, TransactionType transactionType, User user) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ReceiptItem.class);
        if (product != null) {
            criteria.createAlias("debtComponent", "d");
            criteria.add(Restrictions.eq("d.product", product));
        }
        criteria.createAlias("receiptHeader", "r");
        if (startDate != null && endDate != null) {
            criteria.add(Restrictions.between("r.date", startDate, endDate));
        } else if (startDate != null && endDate == null) {
            criteria.add(Restrictions.between("r.date", startDate, new Date()));
        }
        if (transactionType != null) {
            criteria.add(Restrictions.eq("r.transactionType", transactionType));
        }
        if (user != null) {
            criteria.add(Restrictions.eq("r.createdBy", user));
        }
        criteria.add(Restrictions.eq("r.reversed", Boolean.FALSE));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    public List<ReceiptItem> getReceiptItemsDeposit(Product product, Date startDate, Date endDate, TransactionType transactionType, User user) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ReceiptItem.class);
        if (product != null) {
            criteria.createAlias("debtComponent", "d");
            criteria.add(Restrictions.eq("d.product", product));
        }
        criteria.createAlias("receiptHeader", "r");
        if (transactionType != null) {
            criteria.add(Restrictions.eq("r.transactionType", transactionType));
        }
        if (user != null) {
            criteria.add(Restrictions.eq("r.createdBy", user));
        }
        if (startDate != null && endDate != null) {
            criteria.createAlias("r.paymentDetails", "p");
            criteria.add(Restrictions.between("p.dateOfDeposit", startDate, endDate));
        } else if (startDate != null && endDate == null) {
            criteria.createAlias("r.paymentDetails", "p");
            criteria.add(Restrictions.between("p.dateOfDeposit", startDate, new Date()));
        }
        criteria.add(Restrictions.eq("r.reversed", Boolean.FALSE));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    public List<ReceiptItem> getReceiptItems(Product product, PaymentMethod paymentMethod, PaymentType paymentType, Date startDate, Date endDate, TransactionType transactionType, User user) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ReceiptItem.class);
        if (product != null) {
            criteria.createAlias("debtComponent", "d");
            criteria.add(Restrictions.eq("d.product", product));
        }
        criteria.createAlias("receiptHeader", "r");
        if (startDate != null && endDate != null) {
            criteria.add(Restrictions.between("r.date", startDate, endDate));
        } else if (startDate != null && endDate == null) {
            criteria.add(Restrictions.between("r.date", startDate, new Date()));
        }
        if (transactionType != null) {
            criteria.add(Restrictions.eq("r.transactionType", transactionType));
        }
        if (user != null) {
            criteria.add(Restrictions.eq("r.createdBy", user));
        }
        if (paymentMethod != null) {
            criteria.createAlias("r.paymentDetails", "p");
            criteria.add(Restrictions.eq("p.paymentMethod", paymentMethod));
        }
        criteria.add(Restrictions.eq("r.reversed", Boolean.FALSE));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    public BigDecimal getReceiptItemsDepositTotal(Product product, Date startDate, Date endDate, TransactionType transactionType, User user) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ReceiptItem.class);
        if (product != null) {
            criteria.createAlias("debtComponent", "d");
            criteria.add(Restrictions.eq("d.product", product));
        }
        criteria.createAlias("receiptHeader", "r");
        if (transactionType != null) {
            criteria.add(Restrictions.eq("r.transactionType", transactionType));
        }
        if (user != null) {
            criteria.add(Restrictions.eq("r.createdBy", user));
        }

        if (startDate != null && endDate != null) {
            criteria.createAlias("r.paymentDetails", "p");
            criteria.add(Restrictions.between("p.dateOfDeposit", startDate, endDate));
        } else if (startDate != null && endDate == null) {
            criteria.createAlias("r.paymentDetails", "p");
            criteria.add(Restrictions.between("p.dateOfDeposit", startDate, new Date()));
        }
        criteria.add(Restrictions.eq("r.reversed", Boolean.FALSE));
        criteria.setProjection(Projections.sum("amount"));
        BigDecimal amount = (BigDecimal) criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).uniqueResult();
        if (amount == null) {
            return BigDecimal.ZERO;
        } else {
            return amount;
        }

    }

    public BigDecimal getReceiptItemsDepositTotal(Product product, Date startDate, Date endDate, Date depositStartDate, Date depositEndDate, TransactionType transactionType, User user) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ReceiptItem.class);
        if (product != null) {
            criteria.createAlias("debtComponent", "d");
            criteria.add(Restrictions.eq("d.product", product));
        }
        criteria.createAlias("receiptHeader", "r");
        if (transactionType != null) {
            criteria.add(Restrictions.eq("r.transactionType", transactionType));
        }
        if (user != null) {
            criteria.add(Restrictions.eq("r.createdBy", user));
        }
        if (startDate != null && endDate != null) {
            criteria.add(Restrictions.between("r.date", startDate, endDate));
        } else if (startDate != null && endDate == null) {
            criteria.add(Restrictions.between("r.date", startDate, new Date()));
        }
        if (depositStartDate != null && depositEndDate != null) {
            criteria.createAlias("r.paymentDetails", "p");
            criteria.add(Restrictions.between("p.dateOfDeposit", depositStartDate, depositEndDate));
        } else if (depositStartDate != null && depositEndDate == null) {
            criteria.createAlias("r.paymentDetails", "p");
            criteria.add(Restrictions.between("p.dateOfDeposit", depositStartDate, new Date()));
        }
        criteria.add(Restrictions.eq("r.reversed", Boolean.FALSE));
        criteria.setProjection(Projections.sum("amount"));
        BigDecimal amount = (BigDecimal) criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).uniqueResult();
        if (amount == null) {
            return BigDecimal.ZERO;
        } else {
            return amount;
        }

    }

    public BigDecimal getCalcualtedReceiptTotal(Product product, Date startDate, Date endDate, TransactionType transactionType, User user) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ReceiptItem.class);
        criteria.createAlias("debtComponent", "d");
        criteria.add(Restrictions.eq("d.product", product));
        criteria.createAlias("receiptHeader", "r");
        if (startDate != null && endDate != null) {
            criteria.add(Restrictions.between("r.date", startDate, endDate));
        } else if (startDate != null && endDate == null) {
            criteria.add(Restrictions.between("r.date", startDate, new Date()));
        }
        criteria.add(Restrictions.eq("r.transactionType", transactionType));
        if (user != null) {
            criteria.add(Restrictions.eq("r.createdBy", user));
        }
        criteria.add(Restrictions.eq("r.reversed", Boolean.FALSE));
        criteria.setProjection(Projections.sum("amount"));
        BigDecimal amount = (BigDecimal) criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).uniqueResult();
        if (amount == null) {
            return BigDecimal.ZERO;
        } else {
            return amount;
        }
    }

    public BigDecimal getCalcualtedReceiptTotal(Product product, PaymentMethod paymentMethod, PaymentType paymentType, Date startDate, Date endDate, TransactionType transactionType, User user) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ReceiptItem.class);
        criteria.createAlias("debtComponent", "d");
        criteria.add(Restrictions.eq("d.product", product));
        criteria.createAlias("receiptHeader", "r");
        if (startDate != null && endDate != null) {
            criteria.add(Restrictions.between("r.date", startDate, endDate));
        } else if (startDate != null && endDate == null) {
            criteria.add(Restrictions.between("r.date", startDate, new Date()));
        }
        if (transactionType != null) {
            criteria.add(Restrictions.eq("r.transactionType", transactionType));
        }
        if (user != null) {
            criteria.add(Restrictions.eq("r.createdBy", user));
        }
        if (paymentMethod != null) {
            criteria.createAlias("r.paymentDetails", "p");
            criteria.add(Restrictions.eq("p.paymentMethod", paymentMethod));
        }
        criteria.add(Restrictions.eq("r.reversed", Boolean.FALSE));
        criteria.setProjection(Projections.sum("amount"));
        BigDecimal amount = (BigDecimal) criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).uniqueResult();
        if (amount == null) {
            return BigDecimal.ZERO;
        } else {
            return amount;
        }

    }

    public BigDecimal getTotalPaidForDebtComponent(DebtComponent debtComponent) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ReceiptItem.class);
        criteria.createAlias("receiptHeader", "r");
        criteria.add(Restrictions.eq("debtComponent", debtComponent));
        criteria.add(Restrictions.eq("r.reversed", Boolean.FALSE));
        criteria.setProjection(Projections.sum("amount"));
        BigDecimal amount = (BigDecimal) criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).uniqueResult();
        if (amount == null) {
            return BigDecimal.ZERO;
        } else {
            return amount;
        }
    }

    public BigDecimal getTotalPaidForDebtComponent(DebtComponent debtComponent, TransactionType transactionType) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ReceiptItem.class);
        criteria.createAlias("receiptHeader", "r");
        criteria.add(Restrictions.eq("debtComponent", debtComponent));
        criteria.add(Restrictions.eq("r.reversed", Boolean.FALSE));
        criteria.add(Restrictions.eq("r.transactionType", transactionType));
        criteria.setProjection(Projections.sum("amount"));
        BigDecimal amount = (BigDecimal) criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).uniqueResult();
        if (amount == null) {
            return BigDecimal.ZERO;
        } else {
            return amount;
        }
    }

    public List<ReceiptItem> get(DebtComponent debtComponent) {

        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ReceiptItem.class);
        criteria.createAlias("receiptHeader", "r");
        criteria.add(Restrictions.eq("debtComponent", debtComponent));
        criteria.add(Restrictions.eq("r.reversed", Boolean.FALSE));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<ReceiptItem> getReceiptItemsWithNoReceiptHeader(Account account) {

        return entityManager.createQuery("select rI from ReceiptItem rI where rI.debtComponent.account=:account and rI.receiptHeader IS NULL").setParameter("account", account).getResultList();

    }

    public List<Account> getReceiptItemsWithNoReceiptHeaders() {
        return entityManager.createQuery("select DISTINCT rI.debtComponent.account from ReceiptItem rI where rI.receiptHeader IS NULL").getResultList();
    }

    //correcting dates - receiptheader date should be the same as receipt item date
    //correcting receipted  - receiptheader date should be the same as receipt item date    
    public void correctReceiptItems() {
        String queryString = "UPDATE receiptitem INNER JOIN receiptheader ON receiptitem.receiptHeader_id = receiptheader.id SET receiptitem.dateCreated = receiptheader.dateCreated where receiptitem.dateCreated is null";
        entityManager.createNativeQuery(queryString);
        queryString = "UPDATE receiptitem INNER JOIN receiptheader ON receiptitem.receiptHeader_id = receiptheader.id SET receiptitem.createdBy_id = receiptheader.createdBy_id WHERE receiptitem.createdBy_id IS NULL";
        entityManager.createNativeQuery(queryString);
    }

    public List<ReceiptItem> getIncomeGenerated(Date startDate, Date endDate) {
        return entityManager.createQuery("SELECT  r from ReceiptItem r  where  r.dateCreated >= :startDate AND r.dateCreated<=:endDate").setParameter("startDate", startDate).setParameter("endDate", endDate).getResultList();
    }

    public List<Product> getDistinctProductsInSales(Date startDate, Date endDate) {
        return entityManager.createQuery("select DISTINCT r.debtComponent.product from ReceiptItem r where r.dateCreated BETWEEN :dateCreated AND :dateCreated").setParameter("dateCreated", startDate).setParameter("dateCreated", endDate).getResultList();
    }

    public List<Product> getDistinctProductsInSales() {
        return entityManager.createQuery("select DISTINCT r.debtComponent.product from ReceiptItem r").getResultList();
    }

    public List<ReceiptItem> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<CouncilDuration> getCouncilDurationsPaidForByAccountAndTypeOfService(Account account, TypeOfService typeOfService) {
        return entityManager.createQuery("SELECT distinct r.paymentPeriod from ReceiptItem r where r.debtComponent.product.typeOfService=:typeOfService AND r.receiptHeader.reversed=FALSE AND r.receiptHeader.paymentDetails.customer.account=:account")
                .setParameter("account", account)
                .setParameter("typeOfService", typeOfService)
                .getResultList();
    }

    public boolean hasPaidAnnualFeeForCouncilDuration(Account account, CouncilDuration duration) {
        return entityManager.createQuery("SELECT distinct r from ReceiptItem r where r.debtComponent.product.typeOfService=:typeOfService AND r.receiptHeader.reversed=FALSE AND r.receiptHeader.paymentDetails.customer.account=:account AND r.paymentPeriod=:duration AND r.debtComponent.remainingBalance=0")
                .setParameter("account", account)
                .setParameter("duration", duration)
                .setParameter("typeOfService", TypeOfService.ANNUAL_FEES)
                .getResultList().isEmpty() ? false : true;
    }

    public boolean hasPaidAnnualFeeForDuration(Account account, Duration duration) {

        return entityManager.createQuery("SELECT distinct r from ReceiptItem r where r.debtComponent.product.typeOfService=:typeOfService AND r.receiptHeader.reversed=FALSE AND r.receiptHeader.paymentDetails.customer.account=:account AND r.paymentPeriod=:duration AND r.debtComponent.remainingBalance=" + BigDecimal.ZERO)
                .setParameter("account", account)
                .setParameter("duration", duration)
                .setParameter("typeOfService", TypeOfService.ANNUAL_FEES)
                .getResultList().isEmpty() ? false : true;
    }

    public List<Long> ListOfPaidAnnualFeeForCouncilDuration(Account account, CouncilDuration duration) {
        return entityManager.createQuery("SELECT distinct r.receiptHeader.id from ReceiptItem r where r.debtComponent.product.typeOfService=:typeOfService AND r.receiptHeader.reversed=FALSE AND r.receiptHeader.paymentDetails.customer.account=:account AND r.paymentPeriod=:duration AND r.debtComponent.remainingBalance=0")
                .setParameter("account", account)
                .setParameter("duration", duration)
                .setParameter("typeOfService", TypeOfService.ANNUAL_FEES)
                .getResultList();
    }

    public List<Customer> ListOfCustomersPaidAnnualFeeForCouncilDuration(CouncilDuration duration) {
        return entityManager.createQuery("SELECT distinct r.receiptHeader.paymentDetails.customer from ReceiptItem r where r.debtComponent.product.typeOfService=:typeOfService AND r.receiptHeader.reversed=FALSE AND r.paymentPeriod=:duration AND r.debtComponent.remainingBalance=0")
                .setParameter("duration", duration)
                .setParameter("typeOfService", TypeOfService.ANNUAL_FEES)
                .getResultList();
    }

    public BigDecimal getTotalAmountPaidForByAccountAndTypeOfServiceAndCouncilDuration(Account account, TypeOfService typeOfService, CouncilDuration duration) {

        BigDecimal amountPaid = (BigDecimal) entityManager.createQuery("SELECT SUM(r.amount) from ReceiptItem r where r.debtComponent.product.typeOfService=:typeOfService AND r.receiptHeader.reversed=FALSE AND r.receiptHeader.paymentDetails.customer.account=:account AND r.paymentPeriod=:duration AND r.receiptHeader.reversed=FALSE")
                .setParameter("account", account)
                .setParameter("typeOfService", typeOfService)
                .setParameter("duration", duration)
                .getSingleResult();

        if (amountPaid == null) {
            return BigDecimal.ZERO;
        } else {
            return amountPaid;
        }
    }


}