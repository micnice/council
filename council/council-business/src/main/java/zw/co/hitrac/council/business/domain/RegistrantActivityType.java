package zw.co.hitrac.council.business.domain;

/**
 *
 * @author Charles Chigoriwa
 */
public enum RegistrantActivityType {

    REGISTRATION("Registration"),
    EXAM_REGISTRATION("Exam Registration"),
    RENEWAL("Renewal"),
    DEREGISTRATION("Deregistered"),
    SUSPENDED("Suspended"),
    DEATH("Death"),
    CHANGE_OF_NAME("Change of Name"),
    PENALTY("Penalty"),
    RETIRED("Retired"),
    VIODED("Take Off"),
    TRANSFER("Transfer");

    private RegistrantActivityType(String name) {
        this.name = name;
    }
    private final String name;

    public String getName() {
        return name;
    }
}
