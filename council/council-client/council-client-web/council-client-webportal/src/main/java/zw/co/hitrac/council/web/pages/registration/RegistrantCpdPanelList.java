package zw.co.hitrac.council.web.pages.registration;

//~--- non-JDK imports --------------------------------------------------------
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButton;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogButtons;
import com.googlecode.wicket.jquery.ui.widget.dialog.DialogIcon;
import com.googlecode.wicket.jquery.ui.widget.dialog.MessageDialog;
import net.sf.jasperreports.engine.JRException;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantCpd;
import zw.co.hitrac.council.business.process.RegistrationProcess;
import zw.co.hitrac.council.business.service.DurationService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrantCpdService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.reports.RegistrantCPDCertificate;
import zw.co.hitrac.council.reports.RegistrantCPDHistory;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.utility.CustomDateTimeLabel;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.wicket.model.CompoundPropertyModel;
import zw.co.hitrac.council.business.domain.ProductIssuance;

//~--- JDK imports ------------------------------------------------------------
/**
 * @author Constance Mabaso
 * @author Matiashe Michael
 */
public class RegistrantCpdPanelList extends Panel {

    @SpringBean
    RegistrantCpdService registrantCpdService;
    @SpringBean
    GeneralParametersService generalParametersService;
    @SpringBean
    private DurationService durationService;
    @SpringBean
    private RegistrationService registrationService;
    @SpringBean
    private RegistrationProcess registrationProcess;
    private Duration duration;
    private LoadableDetachableModel<List<RegistrantCpd>> currentCPDListModel;

    public RegistrantCpdPanelList(String id, final IModel<Registrant> registrantModel) {
        super(id);
       
        // method to add a new RegistrantCpd - Id is null
        add(new Link<Registrant>("registrantCpdPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantCpdPage(registrantModel));
            }

            @Override
            protected void onConfigure() {
                setVisible(!generalParametersService.get().getCaptureCPDsDirectly());
            }
        });

        add(new Link<Registrant>("addCPDPage") {
            @Override
            public void onClick() {
                setResponsePage(new AddCPDPage(registrantModel));
            }

            @Override
            protected void onConfigure() {
                setVisible(generalParametersService.get().getCaptureCPDsDirectly());
            }
        });

        add(new Link<Registrant>("addCPDPageDirectly") {
            @Override
            public void onClick() {
                setResponsePage(new AddCPDPage(registrantModel));
            }

            @Override
            protected void onConfigure() {
                setVisible(generalParametersService.get().getCaptureCPDsDirectlyAndInDirectly());
            }
        });

        add(new Link<Registrant>("registrantCpdReportPage") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantCpdReportPage(registrantModel));
            }
        });

        add(new Link<Registrant>("registrantCpdHistory") {
            @Override
            public void onClick() {
                try {
                    RegistrantCPDHistory registrantCPDHistory = new RegistrantCPDHistory();
                    ContentType contentType = ContentType.PDF;
                    Map parameters = new HashMap();
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(registrantCPDHistory, contentType,
                            registrantCpdService.getCurrentCPDsList(registrantModel.getObject()), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            protected void onConfigure() {
                if (currentRegistration(registrantModel)) {
                    setVisible(studentNoneRegistration(registrantModel));
                } else {
                    setVisible(Boolean.FALSE);
                }
            }
        });

        if (currentRegistration(registrantModel)) {
            if (studentNoneRegistration(registrantModel)) {
                duration = durationService.getCurrentCourseDuration(registrationProcess.activeRegisterNotStudentRegister(registrantModel.getObject()).getCourse());
            }
        }

        add(new PropertyListView<RegistrantCpd>("registrantCpdList1",
                registrantCpdService.getCPDsList(registrantModel.getObject())) {

            @Override
                    protected void populateItem(final ListItem<RegistrantCpd> item) {
                        item.setModel(new CompoundPropertyModel<RegistrantCpd>(item.getModel()));


                        item.add(new Label("duration"));
                        item.add(new Label("point"));
                        item.add(new Label("issuedDate"));
                        item.add(new Label("expiryDate"));
                        item.add(new Label("activityNo"));
                        item.add(new Label("organisation"));
                        item.add(new Label("activityAttended"));
                        item.add(new Link<RegistrantCpd>("registrantCPDCertificate", item.getModel()) {
                            @Override
                            public void onClick() {

                                try {
                                    RegistrantCPDCertificate registrantCPDCertificate = new RegistrantCPDCertificate();
                                    ContentType contentType = ContentType.PDF;
                                    List<RegistrantCpd> list = new ArrayList<>();
                                    list.add(getModelObject());
                                    Map parameters = new HashMap();
                                    ByteArrayResource resource = ReportResourceUtils.getReportResource(registrantCPDCertificate, contentType,
                                            list, parameters);

                                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                            RequestCycle.get().getResponse(), null);

                                    resource.respond(a);

                                    // To make Wicket stop processing form after sending response
                                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                                } catch (JRException ex) {
                                    ex.printStackTrace();
                                }
                            }

                            @Override
                            protected void onConfigure() {
                                if (currentRegistration(registrantModel)) {
                                    setVisible(studentNoneRegistration(registrantModel));
                                } else {
                                    setVisible(Boolean.FALSE);
                                }

                                setVisible(generalParametersService.get().getCaptureCPDsDirectly() || generalParametersService.get().getCaptureCPDsDirectlyAndInDirectly());

                            }
                        });
                item.setVisible(item.getModelObject().getActivityNo()!=null);

                    }

                    @Override
                    protected void onConfigure() {
                        setVisible(generalParametersService.get().getCaptureCPDsDirectly() || generalParametersService.get().getCaptureCPDsDirectlyAndInDirectly());
                    }

                });

        WebMarkupContainer currentCPDListContainer = new WebMarkupContainer("currentCPDListContainer");
        currentCPDListContainer.setOutputMarkupId(true);
        add(currentCPDListContainer);

        currentCPDListModel = new RegistrantCurrentCPDListModel(registrantModel);

        currentCPDListContainer.add(new PropertyListView<RegistrantCpd>("registrantCpdList", currentCPDListModel) {
            @Override
            protected void populateItem(final ListItem<RegistrantCpd> item) {
                final MessageDialog warningDialog = new MessageDialog("warningDialog", "Warning", "Are you sure you want to remove CPDs?", DialogButtons.YES_NO, DialogIcon.WARN) {
                    final Long documentId = item.getModelObject().getId();

                    @Override
                    public void onClose(AjaxRequestTarget target, DialogButton button) {
                        if (button != null && button.equals(LBL_YES)) {
                            try {
                                RegistrantCpd su = registrantCpdService.get(documentId);
                                su.setRetired(Boolean.TRUE);
                                registrantCpdService.save(su);
                                setResponsePage(getPage());
                            } catch (Exception ex) {
                                ex.printStackTrace(System.out);
                            }
                        }
                    }
                };

                item.add(warningDialog);

                item.add(new AjaxLink("remove") {
                    private static final long serialVersionUID = 1L;

                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        warningDialog.open(target);
                    }
                });

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }
                item.add(new Label("duration"));
                item.add(new Label("points"));
                item.add(new Label("registrantCPDItem"));
                item.add(new Label("createdBy"));
                item.add(new Label("modifiedBy"));
                item.add(CustomDateTimeLabel.forDate("dateCreated",
                        new PropertyModel<>(item.getModel(), "dateCreated")));
                item.add(CustomDateTimeLabel.forDate("dateModified",
                        new PropertyModel<>(item.getModel(), "dateModified")));

                item.setVisible(!item.getModelObject().getRetired());
            }

            @Override
            protected void onConfigure() {
                setVisible(!generalParametersService.get().getCaptureCPDsDirectly() || generalParametersService.get().getCaptureCPDsDirectlyAndInDirectly());
            }

        });

        add(new DropDownChoice<Duration>("cpdDuration",
                new PropertyModel<>(this, "duration"), durationService.findAll()) {

                    @Override
                    protected void onSelectionChanged(Duration newSelection) {

                        if (newSelection != null) {

                            duration = newSelection;
                            currentCPDListContainer.detach();
                            currentCPDListModel.setObject(new RegistrantCurrentCPDListModel(registrantModel).getObject());
                        }
                    }

                    @Override
                    protected boolean wantOnSelectionChangedNotifications() {
                        return true;
                    }
                });

        currentCPDListContainer.add(new Label("totalPoints", new LoadableDetachableModel<String>() {

            @Override
            protected String load() {

                final BigDecimal totalPoints;

                if (currentRegistration(registrantModel)) {
                    totalPoints = registrantCpdService.getCurrentCPDs(registrantModel.getObject(), duration);
                } else {
                    totalPoints = BigDecimal.ZERO;
                }

                return ObjectUtils.toString(totalPoints, "0");
            }
        }));
    }

    public final Boolean currentRegistration(IModel<Registrant> registrantModel) {
        return registrationService.hasCurrentRegistration(registrantModel.getObject());
    }

    public final Boolean studentNoneRegistration(IModel<Registrant> registrantModel) {
        return registrationProcess.studentNoneRegistration(registrantModel.getObject());

    }

    private class RegistrantCurrentCPDListModel extends LoadableDetachableModel<List<RegistrantCpd>> {

        private IModel<Registrant> registrantIModel;

        public RegistrantCurrentCPDListModel(IModel<Registrant> registrantIModel) {
            this.registrantIModel = registrantIModel;
        }

        @Override
        protected List<RegistrantCpd> load() {

            final Registrant registrant = registrantIModel.getObject();
            return registrantCpdService.getCurrentCPDsList(registrant, duration);
        }
    }

}
