package zw.co.hitrac.council.web.pages.institution;

//~--- non-JDK imports --------------------------------------------------------
import zw.co.hitrac.council.web.pages.institution.InstitutionTypeListPage;
import zw.co.hitrac.council.web.pages.institution.InstitutionTypeEditPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.InstitutionType;
import zw.co.hitrac.council.business.service.InstitutionTypeService;
import zw.co.hitrac.council.web.pages.configure.IAdministerDatabaseBasePage;

/**
 *
 * @author Kelvin Goredema
 */
public class InstitutionTypeViewPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private InstitutionTypeService institutionTypeService;

    public InstitutionTypeViewPage(Long id) {
        CompoundPropertyModel<InstitutionType> model =
                new CompoundPropertyModel<InstitutionType>(new LoadableDetachableInstitutionTypeModel(id));

        setDefaultModel(model);
        add(new Link<InstitutionType>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new InstitutionTypeEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", InstitutionTypeListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("institutionType", model.bind("name")));
    }

    private final class LoadableDetachableInstitutionTypeModel extends LoadableDetachableModel<InstitutionType> {

        private Long id;

        public LoadableDetachableInstitutionTypeModel(Long id) {
            this.id = id;
        }

        @Override
        protected InstitutionType load() {
            return institutionTypeService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
