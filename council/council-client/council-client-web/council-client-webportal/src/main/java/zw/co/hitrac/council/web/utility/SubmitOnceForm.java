/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.utility;

import java.util.ArrayList;
import org.apache.wicket.MetaDataKey;
import static org.apache.wicket.ThreadContext.getSession;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.IFormSubmitter;
import org.apache.wicket.model.IModel;

/**
 *
 * @author micnice
 * @param <T>
 */
public class SubmitOnceForm<T> extends Form<T> {

    private static MetaDataKey<ArrayList<FormToken>> PROCESSED
            = new MetaDataKey<ArrayList<FormToken>>() {
            };

    public SubmitOnceForm(String id) {
        super(id);
    }

    public SubmitOnceForm(String id, IModel<T> model) {
        super(id, model);
    }
    

    protected void onRepeatSubmit() {
        error(getString("alreadySubmitted"));
    }

    @Override
    public void process(IFormSubmitter submittingComponent) {
        if (isAlreadyProcessed()) {
            onRepeatSubmit();
            return;
        }
        super.process(submittingComponent);
        updateProcessedForms();
    }

    private FormToken getToken() {
        return new FormToken(getPage().getPageReference(),
                getPageRelativePath());
    }

    private synchronized boolean isAlreadyProcessed() {
        ArrayList<FormToken> tokens = getSession().
                getMetaData(PROCESSED);
        if (tokens != null) {
            FormToken token = getToken();
            if (tokens.contains(token)) {
                return true;
            }
        }
        return false;
    }

    private synchronized void updateProcessedForms() {
        if (hasError()) {
            return;
        }
        ArrayList<FormToken> tokens = getSession().
                getMetaData(PROCESSED);
        if (tokens == null) {
            tokens = new ArrayList<FormToken>();
        }
        FormToken token = getToken();
        if (!tokens.contains(token)) {
            tokens.add(token);
            while (tokens.size() > 20) {
                tokens.remove(0);
            }
            getSession().setMetaData(PROCESSED, tokens);
        }
    }
}
