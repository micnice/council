package zw.co.hitrac.council.web.pages.accounts.payments;

import org.apache.wicket.extensions.yui.calendar.DatePicker;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Book;
import zw.co.hitrac.council.business.domain.accounts.*;
import zw.co.hitrac.council.business.process.PaymentProcess;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.business.service.accounts.PaymentMethodService;
import zw.co.hitrac.council.business.service.accounts.TransactionTypeService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.config.CouncilSession;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.TransactionTypeListModel;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.pages.accounts.documents.DebtComponentsPage;
import zw.co.hitrac.council.web.utility.CustomDateTextField;
import zw.co.hitrac.council.web.utility.DatePickerUtil;
import zw.co.hitrac.council.web.utility.SubmitOnceForm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.service.CouncilDurationService;
import zw.co.hitrac.council.web.models.CouncilDurationListModel;

/**
 * @author Takunda Dhlakama
 * @author Michael Matiashe
 */
public class PaymentPage extends TemplatePage {

    @SpringBean
    private PaymentMethodService paymentMethodService;
    @SpringBean
    private TransactionTypeService transactionTypeService;
    @SpringBean
    private PaymentProcess paymentProcess;
    @SpringBean
    private RegistrantService registrantService;
    private PaymentMethod paymentMethod;
    private List<ReceiptItem> receiptItems = new ArrayList<ReceiptItem>();
    private CustomDateTextField dateOfDeposit;
    private TextField<Long> generatedReceiptNumber;
    @SpringBean
    GeneralParametersService generalParametersService;
    @SpringBean
    private CouncilDurationService councilDurationService;

    public PaymentPage(final Customer customer, final List<DebtComponent> debtComponents) {

        initReceiptItems(debtComponents);

        PaymentDetails paymentDetails = new PaymentDetails();
        paymentDetails.setTransactionHeader(new TransactionHeader());
        paymentDetails.setReceiptHeader(new ReceiptHeader());
        paymentDetails.getReceiptHeader().setCreatedBy(CouncilSession.get().getUser());
        paymentDetails.setCustomer(customer);
        paymentDetails.setCreatedBy(CouncilSession.get().getUser());
        paymentDetails.setDateCreated(new Date());
        paymentDetails.setDateOfPayment(new Date());
        setDefaultModel(new CompoundPropertyModel<PaymentDetails>(paymentDetails));
        Form<PaymentDetails> form = new SubmitOnceForm<PaymentDetails>("form", (IModel<PaymentDetails>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                try {
                    getModelObject().setPaymentMethod(paymentMethod);

                    PaymentDetails $paymentDetails = this.getModelObject();

                    PaymentMethod paymentMethod = $paymentDetails.getPaymentMethod();
                    if (paymentMethod.getDepositDateRequired() && $paymentDetails.getDateOfDeposit() == null) {
                        dateOfDeposit.error("Date of Deposit required");
                        return;
                    }

                    if (paymentMethod.getReceiptNumberRequired() && $paymentDetails.getReceiptHeader().getGeneratedReceiptNumber() == null) {
                        generatedReceiptNumber.error("Manually Entered Receipt Reference Number is required");
                        return;
                    }

                    if (CouncilSession.get().getUser() == null) {
                        error("Please Logout and Login - Session Timed Out");
                    }
                    PaymentDetails paymentDetails = paymentProcess.processPayment(getModelObject(), receiptItems);
                    setResponsePage(new PaymentConfirmationPage(paymentDetails, customer.getAccount()));
                } catch (CouncilException ex) {
                    error(ex.getMessage());
                }
            }

            @Override
            protected void onRepeatSubmit() {
                PaymentDetails paymentDetails = this.getModelObject();
                setResponsePage(new PaymentConfirmationPage(paymentDetails, customer.getAccount()));
            }
        };

        if (generalParametersService.get().getShowActiveDurationsOnly()) {
            form.add(new DropDownChoice("councilDuration", councilDurationService.getActiveCouncilDurations()));
        } else {
            form.add(new DropDownChoice("councilDuration", councilDurationService.findAll()));
        }

        form.add(new CustomDateTextField("dateOfPayment").setRequired(true).add(new ErrorBehavior()).add(new DatePicker()));
        dateOfDeposit = new CustomDateTextField("dateOfDeposit");
        dateOfDeposit.add(new ErrorBehavior()).add(DatePickerUtil.getDatePicker());
        form.add(dateOfDeposit);
        form.add(new TextField<String>("receiptHeader.totalAmount").setRequired(true).add(new ErrorBehavior()));
        generatedReceiptNumber = new TextField<Long>("receiptHeader.generatedReceiptNumber");
        generatedReceiptNumber.add(new ErrorBehavior());
        form.add(generatedReceiptNumber);
        DropDownChoice<PaymentMethod> paymentMethods = new DropDownChoice<PaymentMethod>("paymentMethod",
                new PropertyModel<PaymentMethod>(this, "paymentMethod"), new PaymentPage.PaymentMethodModel(),
                new ChoiceRenderer<PaymentMethod>("name", "id")) {
            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }

            @Override
            protected void onSelectionChanged(PaymentMethod newSelection) {
                PaymentDetails paymentDetails = (PaymentDetails) PaymentPage.this.getDefaultModelObject();

                paymentDetails.getTransactionHeader().setTransactionType(null);
            }
        };

        paymentMethods.setRequired(true).add(new ErrorBehavior());
        form.add(paymentMethods);

        DropDownChoice<TransactionType> transactionTypes = new DropDownChoice<TransactionType>("transactionHeader.transactionType", new PaymentPage.TransactionTypeModel(),
                new ChoiceRenderer<TransactionType>("name", "id"));
        form.add(transactionTypes);

        add(form);
        form.add(new ReceiptItemListView("receiptItems", new LoadableDetachableModel<List<? extends ReceiptItem>>() {
            @Override
            protected List<? extends ReceiptItem> load() {
                return receiptItems;
            }
            //this.receiptItems
        }));

        BigDecimal totalAmountDue = BigDecimal.ZERO;
        for (ReceiptItem item : receiptItems) {
            totalAmountDue = totalAmountDue.add(item.getDebtComponent().getRemainingBalance());
        }

        add(new FeedbackPanel("feedback"));
        add(new Label("customer.customerName"));
        add(new Label("totalAmountDue", totalAmountDue));
        form.add(new Link<Void>("debtComponentPage") {
            @Override
            public void onClick() {
                setResponsePage(new DebtComponentsPage(registrantService.getRegistrant(customer).getId()));
            }
        });
    }

    private final class ReceiptItemListView extends ListView<ReceiptItem> {

        public ReceiptItemListView(String id) {
            super(id);
            setReuseItems(true);
        }

        public ReceiptItemListView(String id, IModel<? extends List<? extends ReceiptItem>> model) {
            super(id, model);
            setReuseItems(true);
        }

        public ReceiptItemListView(String id, List<? extends ReceiptItem> list) {
            super(id, list);
            setReuseItems(true);
        }

        @Override
        protected void populateItem(final ListItem<ReceiptItem> item) {
            item.setDefaultModel(new CompoundPropertyModel<ReceiptItem>(item.getModelObject()));
            item.add(new TextField<BigDecimal>("amount").setRequired(true).add(new ErrorBehavior()));
            item.add(new DropDownChoice("paymentPeriod", new CouncilDurationListModel(councilDurationService)));
            item.add(new Label("debtComponent.remainingBalance"));
            item.add(new Label("debtComponent.transactionComponent.account.name"));
        }
    }

    private void initReceiptItems(List<DebtComponent> debtComponents) {

        for (DebtComponent debtComponent : debtComponents) {
            ReceiptItem receiptItem = new ReceiptItem();
            receiptItem.setDebtComponent(debtComponent);
            receiptItem.setAmount(debtComponent.getRemainingBalance());
            receiptItem.setCreatedBy(CouncilSession.get().getUser());
            receiptItem.setDateCreated(new Date());
            receiptItem.setPaymentPeriod(debtComponent.getPaymentPeriod());
            receiptItems.add(receiptItem);
        }

    }

    private class TransactionTypeModel extends LoadableDetachableModel<List<? extends TransactionType>> {

        protected List<? extends TransactionType> load() {
            if (paymentMethod != null) {
                return new TransactionTypeListModel(transactionTypeService, paymentMethod, Book.CB).getObject();
            } else {
                return new ArrayList<TransactionType>();
            }
        }
    }

    private final class PaymentMethodModel extends LoadableDetachableModel<List<? extends PaymentMethod>> {

        protected List<? extends PaymentMethod> load() {
            return paymentMethodService.findAll();
        }
    }
}
