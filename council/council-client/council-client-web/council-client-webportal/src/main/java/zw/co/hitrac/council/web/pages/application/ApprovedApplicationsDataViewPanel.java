
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.application;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PageableListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import zw.co.hitrac.council.business.domain.Application;
import zw.co.hitrac.council.business.domain.ApplicationPurpose;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;

import org.apache.wicket.markup.html.navigation.paging.PagingNavigator;
import org.apache.wicket.model.Model;
import zw.co.hitrac.council.web.pages.institution.InstitutionRegistrationPage;
import zw.co.hitrac.council.web.pages.registration.RegistrantQualificationPage;
import zw.co.hitrac.council.web.pages.registration.RegistrationPage;
import zw.co.hitrac.council.web.pages.registration.TransferPage;
import zw.co.hitrac.council.web.utility.CustomDateLabel;

/**
 * @author Michael Matiashe
 */
public class ApprovedApplicationsDataViewPanel extends Panel {

    public ApprovedApplicationsDataViewPanel(String id, IModel<List<Application>> model) {
        super(id);

        PageableListView<Application> eachItem = new PageableListView<Application>("eachItem", model, 20) {
            @Override
            protected void populateItem(ListItem<Application> item) {
                Link<Application> viewLink = new Link<Application>("applicationViewPage", item.getModel()) {
                    @Override
                    public void onClick() {
                        if (getModel().getObject().getApplicationPurpose().equals(ApplicationPurpose.INSTITUTION_REGISTRATION)) {
                            setResponsePage(new InstitutionRegistrationPage(getModelObject().getInstitution().getId(), Boolean.TRUE));
                        } else if (getModel().getObject().getApplicationPurpose().equals(ApplicationPurpose.CERTIFICATE_OF_GOOD_STANDING)) {
                            setResponsePage(new CgsApplicationViewPage(getModel().getObject().getId()));
                        } else if (getModel().getObject().getApplicationPurpose().equals(ApplicationPurpose.CHANGE_OF_NAME)) {
                            setResponsePage(new ChangeOfNameApplicationViewPage(getModel().getObject().getId()));
                        } else if (getModel().getObject().getApplicationPurpose().equals(ApplicationPurpose.PROVISIONAL_QUALIIFICATION)) {
                            setResponsePage(new RegistrantQualificationPage(Model.of(getModel().getObject().getRegistrant()), getModelObject()));
                        } else if (getModel().getObject().getApplicationPurpose().equals(ApplicationPurpose.PROVISIONAL_REGISTRATION) || getModel().getObject().getApplicationPurpose().equals(ApplicationPurpose.MAIN_REGISTRATION) || getModel().getObject().getApplicationPurpose().equals(ApplicationPurpose.INTERN_REGISTRATION) || getModel().getObject().getApplicationPurpose().equals(ApplicationPurpose.STUDENT_REGISTRATION) || getModel().getObject().getApplicationPurpose().equals(ApplicationPurpose.INTERN_REGISTRATION)) {
                            CompoundPropertyModel<Application> applicationModel = new CompoundPropertyModel<Application>(getModel().getObject());
                            setResponsePage(new RegistrationPage(null,null,applicationModel,null,null));
                        } else if (getModel().getObject().getApplicationPurpose().equals(ApplicationPurpose.TRANSFER)) {
                            setResponsePage(new TransferPage(getModelObject()));
                        } else {
                            setResponsePage(new ApplicationViewPage(getModelObject().getId()));
                        }
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.setModel(new CompoundPropertyModel<Application>(item.getModel()));
                item.add(viewLink);
                item.add(new Label("registrant.fullname"));
                viewLink.add(new Label("applicationPurpose"));
                item.add(new CustomDateLabel("applicationDate"));
                item.add(new Label("institution"));
                item.add(new Label("course"));
                item.add(new Label("applicationTextStatus"));
            }
        };
        add(new PagingNavigator("navigator", eachItem));
        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
