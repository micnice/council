package zw.co.hitrac.council.webservices.api;

import java.io.Serializable;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import zw.co.hitrac.council.business.domain.RegistrantContact;
import zw.co.hitrac.council.webservices.api.domain.LeanRegistrantContact;

/**
 *
 * @author Daniel Nkhoma
 */
@Path("/registrantContacts")
@Produces(MediaType.APPLICATION_XML)
@Consumes(MediaType.APPLICATION_XML)
public interface RegistrantContactWebService extends Serializable {

    @GET
    @Path("list")
    public List<RegistrantContact> registrantContacts(Long registrantId);

    @GET
    @Path("contactlist")
    public List<RegistrantContact> registrantContacts(@QueryParam(value = "registrationNumber") String registrationNumber);

    @GET
    @Path("listAll")
    public List<RegistrantContact> registrantContacts();

    @GET
    @Path("lean")
    public List<LeanRegistrantContact> leanRegistrantContacts();

    @GET
    @Path("registrantContact/{id}")
    public RegistrantContact registrantContact(@PathParam(value = "id") Long id);

}
