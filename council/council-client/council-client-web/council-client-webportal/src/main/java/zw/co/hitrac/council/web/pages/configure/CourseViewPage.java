package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.service.CourseService;

/**
 *
 * @author Takunda Dhlakama
 * @author Michael Matiashe
 */
public class CourseViewPage extends IAdministerDatabaseBasePage {

    @SpringBean
    private CourseService courseService;

    public CourseViewPage(Long id) {
        CompoundPropertyModel<Course> model = new CompoundPropertyModel<Course>(new LoadableDetachableCourseTypeModel(id));
        setDefaultModel(model);
        add(new Link<Course>("editLink", model) {
            @Override
            public void onClick() {
                setResponsePage(new CourseEditPage(getModelObject().getId()));
            }
        });
        add(new BookmarkablePageLink<Void>("returnLink", CourseListPage.class));
        add(new Label("name"));
        add(new Label("description"));
        add(new Label("courseType"));
        add(new Label("weight"));
        add(new Label("courseGroup"));
        add(new Label("prefixName"));
        add(new Label("requirements"));
        add(new Label("qualification"));
        add(new Label("basicCourse"));
        add(new Label("courseDuration"));
        add(new Label("registerType"));
        add(new Label("isoCode"));
        add(new Label("trainingInstitutions"));        
    }

    private final class LoadableDetachableCourseTypeModel extends LoadableDetachableModel<Course> {

        private Long id;

        public LoadableDetachableCourseTypeModel(Long id) {
            this.id = id;
        }

        @Override
        protected Course load() {

            return courseService.get(id);
        }
    }
}
