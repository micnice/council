/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.dao.repo.accounts;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.accounts.ProductGroup;

/**
 *
 * @author tidza
 */
public interface ProductGroupRepository extends CrudRepository<ProductGroup, Long> {

    public List<ProductGroup> findAll();
}
