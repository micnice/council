package zw.co.hitrac.council.business.service.accounts.impl;

import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.accounts.PrepaymentDetailsDAO;
import zw.co.hitrac.council.business.domain.accounts.PaymentDetails;
import zw.co.hitrac.council.business.domain.accounts.Prepayment;
import zw.co.hitrac.council.business.domain.accounts.PrepaymentDetails;
import zw.co.hitrac.council.business.service.accounts.PrepaymentDetailsService;

import java.util.List;
import zw.co.hitrac.council.business.domain.accounts.Customer;

/**
 *
 * @author Michael Matiashe
 */
@Service
@Transactional
public class PrepaymentDetailsServiceImpl implements PrepaymentDetailsService {

    @Autowired
    private PrepaymentDetailsDAO prepaymentDetailsDAO;

    @Transactional
    public PrepaymentDetails save(PrepaymentDetails t) {
        return prepaymentDetailsDAO.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<PrepaymentDetails> findAll() {
        return prepaymentDetailsDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public PrepaymentDetails get(Long id) {
        return prepaymentDetailsDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<PrepaymentDetails> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setPrepaymentDetailsDao(PrepaymentDetailsDAO prepaymentDetailsDAO) {

        this.prepaymentDetailsDAO = prepaymentDetailsDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<PrepaymentDetails> findByUid(String uuid) {
        return prepaymentDetailsDAO.findByUid(uuid);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<PrepaymentDetails> findByPrepayment(Prepayment prepayment) {
        return prepaymentDetailsDAO.findByPrepayment(prepayment);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public PrepaymentDetails findByPaymentDetails(PaymentDetails paymentDetails) {

        return prepaymentDetailsDAO.findByPaymentDetails(paymentDetails);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<PaymentDetails> findReceiptHeaders(Prepayment prepayment) {
        return prepaymentDetailsDAO.findReceiptHeaders(prepayment);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<PrepaymentDetails> findByCustomerAndCancelled(Customer customer, Boolean cancelled) {
        return prepaymentDetailsDAO.findByCustomerAndCancelled(customer, cancelled);
    }

    @Override
    public List<PrepaymentDetails> findAllModifiedByDates(Date startDate, Date endDate) {
        return prepaymentDetailsDAO.findAllModifiedByDates(startDate, endDate);
    }

    @Override
    public List<PrepaymentDetails> findAllByDates(Date startDate, Date endDate) {
        return prepaymentDetailsDAO.findAllByDates(startDate, endDate);
    }

}
