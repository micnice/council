package zw.co.hitrac.council.webservices.api.domain;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Daniel Nkhoma
 */
@XmlRootElement
public class LeanAddressType {

    private Long addressTypeId;

    public LeanAddressType() {
    }

    public LeanAddressType(Long addressTypeId) {
        this.addressTypeId = addressTypeId;
    }

    public Long getAddressTypeId() {
        return addressTypeId;
    }

    public void setAddressTypeId(Long addressTypeId) {
        this.addressTypeId = addressTypeId;
    }


}
