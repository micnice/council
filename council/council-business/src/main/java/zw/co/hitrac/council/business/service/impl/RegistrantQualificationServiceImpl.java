package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.RegistrantQualificationDAO;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.RegistrantQualificationService;

import java.util.Date;
import java.util.List;

/**
 * @author Michael Matiashe
 */
@Service
@Transactional
public class RegistrantQualificationServiceImpl implements RegistrantQualificationService {

    @Autowired
    private RegistrantQualificationDAO registrantQualificationDAO;

    @Transactional
    public RegistrantQualification save(RegistrantQualification registrantQualification) {
        return registrantQualificationDAO.save(registrantQualification);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantQualification> findAll() {
        return registrantQualificationDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantQualification get(Long id) {
        return registrantQualificationDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantQualification> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setRegistrantQualificationDAO(RegistrantQualificationDAO registrantQualificationDAO) {
        this.registrantQualificationDAO = registrantQualificationDAO;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Date getDateAwarded(Registrant registrant, Course course) {
        return registrantQualificationDAO.getDateAwarded(registrant, course);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Date getLastPBQDateAwarded(Registrant registrant) {
        return registrantQualificationDAO.getLastPBQDateAwarded(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantQualification get(Registrant registrant, Qualification qualification, Institution institution) {
        return registrantQualificationDAO.get(registrant, qualification, institution);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantQualification get(Registrant registrant, Qualification qualification) {
        return registrantQualificationDAO.get(registrant, qualification);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantQualification get(Registrant registrant, String name) {
        return registrantQualificationDAO.get(registrant, name);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantQualification> getRegistrantQualifications(Registrant registrant) {
        return registrantQualificationDAO.getRegistrantQualifications(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getRegistrantQualifications() {
        return registrantQualificationDAO.getRegistrantQualifications();
    }

    @Override
    public RegistrantQualification getRegistrantCurrentQualifications(Registrant registrant) {
        return registrantQualificationDAO.getRegistrantCurrentQualifications(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getRegistrantQualificationIds(Qualification qualification) {
        return registrantQualificationDAO.getRegistrantQualificationIds(qualification);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Integer getRegistrantQualifications(Qualification qualification) {
        return registrantQualificationDAO.getRegistrantQualifications(qualification);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantQualification> getRegistrantQualifications(RegisterType registerType) {
        return registrantQualificationDAO.getRegistrantQualifications(registerType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantQualification> getRegistrantQualifications(Qualification qualification, Institution institution, Date startDate, Date endDate) {
        return registrantQualificationDAO.getRegistrantQualifications(qualification, institution, startDate, endDate);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Registrant> getRegistrants(RegisterType registerType) {
        return registrantQualificationDAO.getRegistrants(registerType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RegistrantQualification getRegistrantQualificationByRegistrantAndCourse(Registrant registrant, Course course) {
        return registrantQualificationDAO.getRegistrantQualificationByRegistrantAndCourse(registrant, course);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Registrant> getRegistrants(Qualification qualification, Institution institution, Date startDate, Date endDate) {
        return registrantQualificationDAO.getRegistrants(qualification, institution, startDate, endDate);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getRegistrantQualificationList(Qualification qualification, Institution institution, Date startDate, Date endDate) {
        return registrantQualificationDAO.getRegistrantQualificationList(qualification, institution, startDate, endDate);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantQualification> getRegistrantQualificationCertificateType(Registrant registrant, ProductIssuanceType productIssuanceType) {
        return registrantQualificationDAO.getRegistrantQualificationCertificateType(registrant, productIssuanceType);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Qualification> getDistinctQualificationsInRegistrantQualification() {
        return registrantQualificationDAO.getDistinctQualificationsInRegistrantQualification();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Integer getTotalPerQualificationMappedCourseAndRegister(Course course, Register register) {
        return registrantQualificationDAO.getTotalPerQualificationMappedCourseAndRegister(course, register);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Institution> getDistinctInstitutionsInRegistrantQualification() {
        return registrantQualificationDAO.getDistinctInstitutionsInRegistrantQualification();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantQualification> getRegistrantPracticeQualificationList(Registrant registrant) {
        return registrantQualificationDAO.getRegistrantPracticeQualificationList(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantQualification> getRegistrantAdditionalPracticeQualificationList(Registrant registrant) {
        return registrantQualificationDAO.getRegistrantAdditionalPracticeQualificationList(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getRegistrantDataListWithBoth(Course course, Course course2, CouncilDuration councilDuration, String status) {
        return registrantQualificationDAO.getRegistrantDataListWithBoth(course, course2, councilDuration, status);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getRegistrantDataListWithBoth(Qualification qualification, Qualification qualification2) {
        return registrantQualificationDAO.getRegistrantDataListWithBoth(qualification, qualification2);
    }

    public List<Course> getRegisteredDisciplines(Registrant registrant) {
        return registrantQualificationDAO.getRegisteredDisciplines(registrant);
    }

    @Override
    public boolean checkQualification(Registrant registrant, Course course) {
        List<RegistrantQualification> list = getRegistrantQualifications(registrant);
        for (RegistrantQualification registrantQualification : list){


            if (registrantQualification.getCourse().equals(course))
                return true;

        }
        return false;
    }
}
