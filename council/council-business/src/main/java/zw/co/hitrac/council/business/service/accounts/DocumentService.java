
package zw.co.hitrac.council.business.service.accounts;

import zw.co.hitrac.council.business.domain.accounts.Document;
import zw.co.hitrac.council.business.service.IGenericService;

/**
 *
 * @author Michael 

 */
public interface DocumentService extends IGenericService<Document> {
    
}
