package zw.co.hitrac.council.business.dao.examinations.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.examinations.ExamRegistrationDebtComponentDao;
import zw.co.hitrac.council.business.dao.repo.examinations.ExamRegistrationDebtComponentRepository;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistration;
import zw.co.hitrac.council.business.domain.examinations.ExamRegistrationDebtComponent;
import zw.co.hitrac.council.business.domain.examinations.ExamSetting;

/**
 *
 * @author tidza
 */
@Repository
public class ExamRegistrationDebtComponentDaoImpl implements ExamRegistrationDebtComponentDao {

    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    private ExamRegistrationDebtComponentRepository examRegistrationRepository;

    public ExamRegistrationDebtComponent save(ExamRegistrationDebtComponent t) {
        return examRegistrationRepository.save(t);
    }

    public List<ExamRegistrationDebtComponent> findAll() {
        return examRegistrationRepository.findAll();
    }

    public ExamRegistrationDebtComponent get(Long id) {
        return examRegistrationRepository.findOne(id);
    }

    public void setExamRegistrationDebtComponentRepository(ExamRegistrationDebtComponentRepository examRegistrationRepository) {
        this.examRegistrationRepository = examRegistrationRepository;
    }

    public List<ExamRegistrationDebtComponent> getDebtComponents(ExamSetting examSetting, Course course, Institution institution) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ExamRegistrationDebtComponent.class);
        Criteria p = criteria.createCriteria("examRegistration");
        p.add(Restrictions.eq("examSetting", examSetting));
        p.add(Restrictions.eq("institution", institution));
        p.createAlias("registration", "r");
        p.add(Restrictions.eq("r.course", course));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<ExamRegistrationDebtComponent> getDebtComponents(ExamSetting examSetting, Product product, Institution institution) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ExamRegistrationDebtComponent.class);
        criteria.createAlias("examRegistration", "e");
        criteria.add(Restrictions.eq("e.examSetting", examSetting));
        if (institution != null) {
            criteria.add(Restrictions.eq("e.institution", institution));
        }
        criteria.createAlias("debtComponent", "d");
        criteria.add(Restrictions.eq("d.product", product));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<ExamRegistrationDebtComponent> getDebtComponents(ExamRegistration examRegistration) {
        
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(ExamRegistrationDebtComponent.class);
        criteria.add(Restrictions.eq("examRegistration", examRegistration));
        return criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
    }

    public List<ExamRegistrationDebtComponent> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
