package zw.co.hitrac.council.business.service;

import java.util.List;
import zw.co.hitrac.council.business.domain.WebUser;

/**
 *
 * @author tdhlakama
 */
public interface WebUserService extends IGenericService<WebUser> {

    WebUser get(String username, String password);

    WebUser getByUsername(String username);

    WebUser get(String registrationNumber);

    String createWebUser(String username, String password, String registrationNumber);
    
    String loginWebUser(String username, String password);
    
        void changeWebUserPassword(WebUser webUser, String password);
    

}
