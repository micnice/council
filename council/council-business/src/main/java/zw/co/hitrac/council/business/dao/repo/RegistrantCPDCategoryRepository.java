package zw.co.hitrac.council.business.dao.repo;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.RegistrantCPDCategory;

/**
 *
 * @author Constance Mabaso
 */
public interface RegistrantCPDCategoryRepository extends CrudRepository<RegistrantCPDCategory, Long> {
    public List<RegistrantCPDCategory> findAll();
    
}
