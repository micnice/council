/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.registration;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantIdentification;
import zw.co.hitrac.council.business.service.RegistrantIdentificationService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.utility.CustomDateTimeLabel;

/**
 *
 * @author kelvin
 */
public class RegistrantIdentificationPanelList extends Panel {

    @SpringBean
    private RegistrantIdentificationService registrantIdentificationService;

    public RegistrantIdentificationPanelList(String id, final IModel<Registrant> registrantModel) {
        super(id);

        add(new Link<Registrant>("registrantIdentificationLink") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantIdentificationEditPage(null,registrantModel));
            }
        });

        add(new PropertyListView<RegistrantIdentification>("registrantIdenticationList", registrantIdentificationService.getIdentities(registrantModel.getObject())) {
            @Override
            protected void populateItem(ListItem<RegistrantIdentification> item) {
                Link<RegistrantIdentification> viewLink = new Link<RegistrantIdentification>("registrantIdentificationLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new RegistrantIdentificationEditPage(getModelObject().getId(), null));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(viewLink);
                item.add(new Label("identificationType"));                
                item.add(new Label("idNumber"));
                item.add(new Label("expiry"));
                item.add(new Label("createdBy"));
                item.add(new Label("modifiedBy"));
                item.add(CustomDateTimeLabel.forDate("dateCreated",
                        new PropertyModel<>(item.getModel(), "dateCreated")));
                item.add(CustomDateTimeLabel.forDate("dateModified",
                        new PropertyModel<>(item.getModel(), "dateModified")));
            }
        });

    }
}
