package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.EmailMessageDAO;
import zw.co.hitrac.council.business.domain.EmailMessage;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.EmailMessageService;
import zw.co.hitrac.council.business.utils.CouncilException;

import java.util.Collection;
import java.util.List;

/**
 * @author Charles Chigoriwa
 */
@Service
@Transactional
public class EmailMessageServiceImpl implements EmailMessageService {

    @Autowired
    private EmailMessageDAO emailMessageDAO;

    @Transactional
    public EmailMessage save(EmailMessage emailMessage) {

        return emailMessageDAO.save(emailMessage);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<EmailMessage> findAll() {

        return emailMessageDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public EmailMessage get(Long id) {

        return emailMessageDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<EmailMessage> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setEmailMessageDAO(EmailMessageDAO emailMessageDAO) {

        this.emailMessageDAO = emailMessageDAO;
    }

    @Override
    public void send(RegistrantData registrantData, String subject, String message) {
        try {
            emailMessageDAO.send(registrantData, subject, message);
        } catch (Exception e) {
            System.out.println("------------- EMAIL 5 --------------------");
            e.printStackTrace(System.out);
            throw new CouncilException("Failed to send email to, please try again later" + e.getMessage());

        }
    }

    @Override
    public void send(Collection<RegistrantData> registrantDataList, String subject, String message) {
        try {
            emailMessageDAO.send(registrantDataList, subject, message);
        } catch (Exception e) {
            throw new CouncilException("Failed to send email to, please try again later" + e.getMessage());

        }
    }

    public void send(String email, final String subject, final String message) {
        try {
            emailMessageDAO.send(email, subject, message);
        } catch (Exception e) {
            throw new CouncilException("Failed to send email to, please try again later " + e.getMessage());
        }
    }
}
