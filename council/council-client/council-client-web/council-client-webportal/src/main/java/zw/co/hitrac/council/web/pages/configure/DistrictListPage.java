package zw.co.hitrac.council.web.pages.configure;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.District;
import zw.co.hitrac.council.business.service.DistrictService;

import java.util.List;

/**
 *
 * @author Constance Mabaso
 */
public class DistrictListPage extends IAdministerDatabaseBasePage{
    
    @SpringBean
    private DistrictService districtService;

    public DistrictListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink",AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink",AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {

            @Override
            public void onClick() {
               setResponsePage(new DistrictEditPage(null, getPageReference()));
            }
        });
        
        IModel<List<District>> model=new LoadableDetachableModel<List<District>>() {

            @Override
            protected List<District> load() {
               return districtService.findAll();
            }
        };
        
        PropertyListView<District> eachItem=new PropertyListView<District>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<District> item) {
                Link<District> viewLink=new Link<District>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                       setResponsePage(new DistrictViewPage(getModelObject().getId()));
                    }
                };
                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };
        
        add(eachItem);
    }
    
    
    
}
