
package zw.co.hitrac.council.web.menu;

import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import zw.co.hitrac.council.web.pages.registration.RegistrantViewPage;

/**
 *
 * @author Takunda Dhlakama
 * @author Edward Zengeni
 */
public class RegistrantBackMenuPanel extends MenuPanel {

    private BookmarkablePageLink<Void> viewRegistrantBackLink;
    private Long registrantId;

    public RegistrantBackMenuPanel(String id) {
        super(id);
        viewRegistrantBackLink = new BookmarkablePageLink<Void>("viewRegistrantBackLink", RegistrantViewPage.class);

        add(viewRegistrantBackLink);
    }

    public Long getRegistrantId() {
        return registrantId;
    }

    public void setRegistrantId(Long registrantId) {
        this.registrantId = registrantId;
    }

    @Override
    protected void onConfigure() {
        super.onConfigure();
        this.setVisibilityAllowed(topMenuCurrent);
        addCurrentBehavior(viewRegistrantBackLink, topMenuCurrent);
        if (registrantId != null) {
            viewRegistrantBackLink.getPageParameters().add("registrantId", registrantId);
        }
    }
}