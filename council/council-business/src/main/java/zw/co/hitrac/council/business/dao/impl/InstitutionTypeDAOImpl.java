package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.InstitutionTypeDAO;
import zw.co.hitrac.council.business.dao.repo.InstitutionTypeRepository;
import zw.co.hitrac.council.business.domain.InstitutionType;

/**
 *
 * @author kelvin
 */
@Repository
public class InstitutionTypeDAOImpl implements InstitutionTypeDAO {

    @Autowired
    private InstitutionTypeRepository institutionTypeRepository;

    public InstitutionType save(InstitutionType institutionType) {
        return institutionTypeRepository.save(institutionType);
    }

    public List<InstitutionType> findAll() {
        return institutionTypeRepository.findAll();


    }

    public InstitutionType get(Long id) {
        return institutionTypeRepository.findOne(id);

    }

    public void setInstitutionTypeRepository(InstitutionTypeRepository institutionTypeRepository) {
        this.institutionTypeRepository = institutionTypeRepository;
    }

    public List<InstitutionType> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}