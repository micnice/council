
package zw.co.hitrac.council.business.dao.accounts.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.accounts.DocumentItemDAO;
import zw.co.hitrac.council.business.dao.repo.accounts.DocumentItemRepository;
import zw.co.hitrac.council.business.domain.accounts.DocumentItem;

/**
 *
 * @author Tatenda Chiwandire
 * @author Michael Matiashe
 */
@Repository
public class DocumentItemDAOImpl implements DocumentItemDAO {

    @Autowired
    private DocumentItemRepository documentItemRepository;

    public DocumentItem save(DocumentItem t) {
        return documentItemRepository.save(t);
    }

    public List<DocumentItem> findAll() {
        return documentItemRepository.findAll();
    }

    public DocumentItem get(Long id) {
        return documentItemRepository.findOne(id);
    }

    public DocumentItemRepository getDocumentItemRepository() {
        return documentItemRepository;
    }

    public void setDocumentItemRepository(DocumentItemRepository documentItemRepository) {
        this.documentItemRepository = documentItemRepository;
    }

    public List<DocumentItem> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
