package zw.co.hitrac.council.web.pages.configure;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Post;
import zw.co.hitrac.council.business.service.PostService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;

/**
 *
 * @author Charles Chigoriwa
 */
public class PostListPage extends IAdministerDatabaseBasePage {
    @SpringBean
    private PostService postService;

    public PostListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new PostEditPage(null));
            }
        });

        IModel<List<Post>> model = new LoadableDetachableModel<List<Post>>() {
            @Override
            protected List<Post> load() {
                return postService.findAll();
            }
        };
        PropertyListView<Post> eachItem = new PropertyListView<Post>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<Post> item) {
                Link<Post> viewLink = new Link<Post>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new PostViewPage(getModelObject().getId()));
                    }
                };

                if (item.getIndex() % 2 == 0) {
                    item.add(new EvenTableRowBehavior());
                }

                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };

        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
