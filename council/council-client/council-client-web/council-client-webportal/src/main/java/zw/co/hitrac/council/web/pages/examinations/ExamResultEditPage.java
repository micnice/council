package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.examinations.ExamResult;
import zw.co.hitrac.council.business.service.examinations.ExamResultService;
import zw.co.hitrac.council.web.config.CouncilSession;

/**
 *
 * @author Constance Mabaso
 */
public class ExamResultEditPage extends IExaminationsPage {

    @SpringBean
    private ExamResultService examResultService;
    private TextField<String> mark;

    public ExamResultEditPage(Long id) {
        CompoundPropertyModel<ExamResult> model = new CompoundPropertyModel<ExamResult>(new LoadableDetachableExamResultModel(id));
        final Long examRegistrationId = model.getObject().getExamRegistration().getId();
        setDefaultModel(model);
        add(new FeedbackPanel("feedback"));
        Form<ExamResult> form = new Form<ExamResult>("form", (IModel<ExamResult>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                if (getModel().getObject().getMark() == null || getModel().getObject().getMark() > 100 || getModel().getObject().getMark() < 0) {
                    mark.error("Exam Result cannot be empty, greater than 100 or less than 0");
                } else {
                    if (!getModelObject().getExamRegistration().getClosed()) {
                        if (!getModelObject().getSupplementaryExam()) {
                            if (CouncilSession.get().getUser() != null) {
                                getModelObject().setCreatedBy(CouncilSession.get().getUser());
                                examResultService.save(getModelObject());
                            }
                        }
                    }
                    setResponsePage(new RegistrantExamResultPage(getModelObject().getExamRegistration().getId()));
                }
            }
        };

        mark = new TextField<String>("mark");
        form.add(mark);

        form.add(new Label("exam"));
        form.add(new Label("examRegistration.examSetting"));
        form.add(new CheckBox("examAttendance"));
        form.add(new CheckBox("disqualified"));
        form.add(new Link<Void>("returnLink") {
            @Override
            public void onClick() {
                setResponsePage(new RegistrantExamResultPage(examRegistrationId));
            }
        });
        add(new Label("examRegistration.registration.registrant.fullname"));
        add(new Label("examRegistration.candidateNumber"));
        add(form);
    }

    private final class LoadableDetachableExamResultModel extends LoadableDetachableModel<ExamResult> {

        private Long id;

        public LoadableDetachableExamResultModel(Long id) {
            this.id = id;
        }

        @Override
        protected ExamResult load() {
            return examResultService.get(id);
        }
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
