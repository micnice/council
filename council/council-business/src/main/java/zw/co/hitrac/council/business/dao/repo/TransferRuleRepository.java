package zw.co.hitrac.council.business.dao.repo;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import zw.co.hitrac.council.business.domain.TransferRule;

/**
 *
 * @author Michael Matiashe
 */
public interface TransferRuleRepository extends CrudRepository<TransferRule, Long> {

    public List<TransferRule> findAll();
}
