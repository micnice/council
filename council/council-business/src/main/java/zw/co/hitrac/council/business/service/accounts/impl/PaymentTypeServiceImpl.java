
package zw.co.hitrac.council.business.service.accounts.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.accounts.PaymentTypeDAO;
import zw.co.hitrac.council.business.domain.accounts.PaymentType;
import zw.co.hitrac.council.business.service.accounts.PaymentTypeService;

import java.util.List;

/**
 *
 * @author Tatenda Chiwandire
 */
@Service
@Transactional
public class PaymentTypeServiceImpl implements PaymentTypeService {

    @Autowired
    private PaymentTypeDAO paymentTypeDAO;

    @Transactional
    public PaymentType save(PaymentType t) {
        return paymentTypeDAO.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<PaymentType> findAll() {
        return paymentTypeDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public PaymentType get(Long id) {
        return paymentTypeDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<PaymentType> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setPaymentTypeDao(PaymentTypeDAO paymentTypeDAO) {

        this.paymentTypeDAO = paymentTypeDAO;
    }

}
