package zw.co.hitrac.council.web.pages.certification;

import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.models.CourseListModel;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.CustomDateTextField;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

import java.util.Arrays;
import java.util.Random;

import zw.co.hitrac.council.business.process.ProductIssuer;

/**
 * @author tdhlakama
 */
public class CertificationEditPage extends TemplatePage {

    @SpringBean
    private ProductIssuanceService productIssuanceService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    @SpringBean
    private CourseService courseService;
    @SpringBean
    private RegisterService registerService;
    @SpringBean
    private ProductIssuer productIssuer;
    @SpringBean
    private DurationService durationService;

    public CertificationEditPage(Long id, final IModel<Registrant> registrantModel) {
        setDefaultModel(new CompoundPropertyModel<ProductIssuance>(new LoadableDetachableProductIssuanceModel(id, registrantModel)));
        Form<ProductIssuance> form = new Form<ProductIssuance>("form", (IModel<ProductIssuance>) getDefaultModel()) {
            @Override
            public void onSubmit() {

                if (!generalParametersService.get().getRegistrationByApplication()) {
                    if (getModelObject().getProductIssuanceType().equals(ProductIssuanceType.PRACTICING_CERTIFICATE)) {
                        getModelObject().setProductNumber(getModelObject().getPracticeCertificateNumber());
                    }
                    if (getModelObject().getProductIssuanceType().equals(ProductIssuanceType.REGISTRATION_CERTIFICATE)) {
                        getModelObject().setProductNumber(getModelObject().getRegistrationCertificateNumber());
                    }
                    if (getModelObject().getProductIssuanceType().equals(ProductIssuanceType.CERTIFICATE_OF_GOOD_STANDING)) {
                        getModelObject().setProductNumber(getModelObject().getCgsCertificateNumber());
                    }
                }
                getModelObject().setPrefixName(getModel().getObject().getCourse().getPrefixName());
                getModelObject().setDateCreated(getModelObject().getIssueDate());
                try {
                    productIssuanceService.save(getModelObject());
                    setResponsePage(new SearchCertificatePage());
                } catch (CouncilException e) {
                    error(e.getMessage());
                }
            }
        };
        form.add(new CheckBox("issued"));
        form.add(new CustomDateTextField("createDate").add(DatePickerUtil.getDatePicker()).add(new ErrorBehavior()));
        form.add(new CustomDateTextField("expiryDate").add(DatePickerUtil.getDatePicker()).add(new ErrorBehavior()));
        form.add(new CustomDateTextField("issueDate").add(DatePickerUtil.getDatePicker()));
        form.add(new TextField<String>("examDate"));//String not an actual Date
        form.add(new DropDownChoice("register", registerService.findAll()));
        form.add(new DropDownChoice("productIssuanceType", Arrays.asList(ProductIssuanceType.values())));
        form.add(new DropDownChoice("course", new CourseListModel(courseService)));
        form.add(new TextField<String>("practiceCertificateNumber"));
        form.add(new TextField<String>("diplomaCertificateNumber"));
        form.add(new TextField<String>("cgsCertificateNumber"));
        form.add(new TextField<String>("registrationCertificateNumber"));
        form.add(new DropDownChoice("duration", durationService.findAll()));
        form.add(new TextField<String>("comment").setRequired(false).add(new ErrorBehavior()));
        form.add(new BookmarkablePageLink<Void>("returnLink", SearchCertificatePage.class));
        add(form);
        add(new FeedbackPanel("feedback"));
    }

    private final class LoadableDetachableProductIssuanceModel extends LoadableDetachableModel<ProductIssuance> {

        private IModel<Registrant> registrantIModel;
        private Long id;

        public LoadableDetachableProductIssuanceModel(Long id, IModel<Registrant> registrantIModel) {
            this.registrantIModel = registrantIModel;
            this.id = id;
        }

        @Override




        protected ProductIssuance load() {
            int random = 0;
            int random1 = 0;
            if (id == null) {
                ProductIssuance productIssuance = new ProductIssuance();
                random = new Random().nextInt();
                random = random < 0 ? random * -1 : random;
                random1 = new Random().nextInt();
                random1 = random1 < 0 ? random1 * -1 : random1;
                productIssuance.setProductNumber(String.valueOf(random1));
                productIssuance.setBarCode(random);
                productIssuance.setSerialNumber(random);
                final Registrant registrant = registrantIModel.getObject();
                productIssuance.setRegistrant(registrant);
                return productIssuance;
            } else {
                return productIssuanceService.get(id);
            }
        }

    }


}
