package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.RequiredParameterDAO;
import zw.co.hitrac.council.business.dao.repo.RequiredParameterRepository;
import zw.co.hitrac.council.business.domain.RequiredParameter;
import zw.co.hitrac.council.business.domain.RequiredParameter;
import zw.co.hitrac.council.business.service.RequiredParameterService;

import java.util.List;

/**
 * Created by tndangana on 2/13/17.
 */
@Service
public class RequiredParameterServiceImpl implements RequiredParameterService{

    @Autowired
    private RequiredParameterDAO generalParameterDAO;

    @Transactional
    public RequiredParameter save(RequiredParameter RequiredParameter) {
        List<RequiredParameter> list = findAll();
        if (RequiredParameter.getId() == null && !list.isEmpty()) {
            throw new IllegalStateException("Only one row is allowed for parameters!!!!!");
        } else if (RequiredParameter.getId() != null && get(RequiredParameter.getId()) == null) {
            throw new IllegalStateException("Row does not exist");
        } else if (list.size() >= 2) {
            throw new IllegalStateException("Only one row is allowed for parameters!!!!!");
        }
        return generalParameterDAO.save(RequiredParameter);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RequiredParameter> findAll() {
        return generalParameterDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RequiredParameter get(Long id) {
        return generalParameterDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RequiredParameter> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public RequiredParameter get() {
        List<RequiredParameter> parameters = generalParameterDAO.findAll();
        if (parameters.isEmpty()) {
            return new RequiredParameter();
        } else if (parameters.size()== 1) {
            return parameters.get(0);
        } else {
            throw new IllegalStateException("Only one row is allowed for parameters!!!!!");
        }
    }


    /**
     * A DAO setter method to facilitate mocking
     *
     * @param generalParameterDAO
     */
    public void setGeneralParameterDAO(RequiredParameterDAO generalParameterDAO) {
        this.generalParameterDAO = generalParameterDAO;
    }
}
