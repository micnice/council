package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.IdentificationType;

/**
 *
 * @author Charles Chigoriwa
 */
public interface IdentificationTypeService extends IGenericService<IdentificationType> {
    
}
