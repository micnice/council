package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 *
 * @author Edawrd Zengeni
 */
@Entity
@Table(name="maritalstatus")
@Audited
@XmlRootElement
public class MaritalStatus extends BaseEntity implements Serializable {
        
}
