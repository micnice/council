
package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.MaritalStatus;

/**
 *
 * @author Edward Zengeni
 */
public interface MaritalStatusService extends IGenericService<MaritalStatus> {
	public MaritalStatus findByName(String name);
}
