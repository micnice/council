/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.reports;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.sql.DataSource;
import net.sf.jasperreports.engine.JRException;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.domain.Qualification;
import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.business.domain.RegistrantActivityType;
import zw.co.hitrac.council.business.domain.reports.RegisterStatistic;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.CouncilDurationService;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegisterService;
import zw.co.hitrac.council.business.service.RegistrantActivityService;
import zw.co.hitrac.council.business.service.RegistrantQualificationService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.reports.QualificationStatisticReport;
import zw.co.hitrac.council.reports.RegistrantDataReport;
import zw.co.hitrac.council.reports.RegistrantsNotActive;
import zw.co.hitrac.council.reports.SummaryRegisterReport;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

/**
 *
 * @author tdhlakama
 */
public class DurationReportPage extends TemplatePage {

    @SpringBean
    private CouncilDurationService councilDurationService;
    @SpringBean
    private RegistrantActivityService registrantActivityService;
    @SpringBean
    private GeneralParametersService generalParametersService;
    private RegistrantActivityType registrantActivityType;
    private CouncilDuration councilDuration;
    @SpringBean
    private DataSource dataSource;
    @SpringBean
    private RegistrantQualificationService registrantQualificationService;
    @SpringBean
    private RegisterService registerService;
    @SpringBean
    private RegistrationService registrationService;
    private ContentType contentType;

    public DurationReportPage() {
        menuPanelManager.getSearchPanel().setTopMenuCurrent(true);
        menuPanelManager.getSearchPanel().setSearchAdvanced(true);

        PropertyModel<RegistrantActivityType> registrantActivityTypeModel = new PropertyModel<RegistrantActivityType>(this, "registrantActivityType");
        PropertyModel<CouncilDuration> councilDurationModel = new PropertyModel<CouncilDuration>(this, "councilDuration");
        PropertyModel<ContentType> contentTypeModel = new PropertyModel<ContentType>(this, "contentType");
        Form<?> form = new Form("form");

        List<RegistrantActivityType> registrantActivityTypes = new ArrayList<RegistrantActivityType>(Arrays.asList(RegistrantActivityType.values()));
        if (!generalParametersService.get().getRegistrationByApplication()) {
            registrantActivityTypes.remove(RegistrantActivityType.CHANGE_OF_NAME);
            registrantActivityTypes.remove(RegistrantActivityType.DEREGISTRATION);
            registrantActivityTypes.remove(RegistrantActivityType.EXAM_REGISTRATION);
            registrantActivityTypes.remove(RegistrantActivityType.PENALTY);
        }
        form.add(new DropDownChoice("registrantActivityType", registrantActivityTypeModel, registrantActivityTypes).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("councilDuration", councilDurationModel, councilDurationService.findAll()).setRequired(true).add(new ErrorBehavior()));
        form.add(new DropDownChoice("contentType", contentTypeModel, Arrays.asList(ContentType.values())));
        add(form);

        form.add(new Button("active") {
            @Override
            public void onSubmit() {
                Set<RegistrantData> registrants = new LinkedHashSet<RegistrantData>();
                Map parameters = new HashMap();
                parameters.put("comment", "Active - " + registrantActivityType + "s for Period " + councilDuration.getName());
                parameters.put("activityType", registrantActivityType);
                Set<Register> registers = new HashSet<Register>();
                registers.addAll(registerService.findAll());
                registers.remove(generalParametersService.get().getStudentRegister());
                List<Duration> durations = new ArrayList<Duration>();
                for (Duration d : councilDurationService.get(councilDuration.getId()).getDurations()) {
                    durations.add(d);
                }

                for (Course course : registrationService.getDistinctCourseFromRegistration()) {
                    for (Register r : registers) {
                        registrants.addAll(registrantActivityService.getListPerCourseAndDurationAndRegisterAndRegistrantActivity(course, r, durations, registrantActivityType, Boolean.TRUE));
                    }
                }

                try {
                    RegistrantDataReport registrantDataReport = new RegistrantDataReport();

                    ByteArrayResource resource
                            = ReportResourceUtils.getReportResource(registrantDataReport, contentType, registrants,
                                    parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        }
        );

        form.add(
                new Button("notActive") {
                    @Override
                    public void onSubmit() {
                        try {
                            RegistrantsNotActive registrantsNotActive = new RegistrantsNotActive();
                            Map parameters = new HashMap();
                            parameters.put("activityType", registrantActivityType);
                            List<Integer> registers = new ArrayList<Integer>();
                            registers.add(generalParametersService.get().getMainRegister().getId().intValue());
                            registers.add(generalParametersService.get().getProvisionalRegister().getId().intValue());
                            parameters.put("registerID", registers);
                            List<Integer> durations = new ArrayList<Integer>();
                            if (councilDuration != null) {
                                for (Duration d : councilDurationService.get(councilDuration.getId()).getDurations()) {
                                    durations.add(d.getId().intValue());
                                }
                            } else {
                                durations.add(0);
                            }
                            parameters.put("durationID", durations);
                            parameters.put("comment", "No " + registrantActivityType + "Activity have been found for Period " + councilDuration.getName());

                            final Connection connection = dataSource.getConnection(); //DBConnect.getConnection();
                            ByteArrayResource resource = null;
                            try {
                                resource = ReportResourceUtils.getReportResource(registrantsNotActive, contentType, connection, parameters);
                            } finally {
                                connection.close();
                            }
                            IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                    RequestCycle.get().getResponse(), null);

                            resource.respond(a);

                            // To make Wicket stop processing form after sending response
                            RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                        } catch (JRException ex) {
                            ex.printStackTrace(System.out);
                        } catch (SQLException ex) {
                            ex.printStackTrace(System.out);
                        }
                    }
                }
        );

        form.add(
                new Button("disciplineStatisticReport") {
                    @Override
                    public void onSubmit() {
                        List<RegisterStatistic> registerStatistics = new ArrayList<RegisterStatistic>();
                        Register main = generalParametersService.get().getMainRegister();
                        Register provisional = generalParametersService.get().getProvisionalRegister();
                        Register student = generalParametersService.get().getStudentRegister();
                        Register intern = generalParametersService.get().getInternRegister();
                        Register maintaince = generalParametersService.get().getMaintenanceRegister();
                        Register specialit = generalParametersService.get().getSpecialistRegister();

                        for (Course c : registrationService.getDistinctCourseFromRegistration()) {
                            RegisterStatistic statistic = new RegisterStatistic();
                            statistic.setCourse(c);
                            statistic.setMainRegister(main);
                            statistic.setStudentRegister(student);
                            statistic.setProvisionalRegister(provisional);
                            if (intern != null) {
                                statistic.setInternRegister(intern);
                            }
                            if (maintaince != null) {
                                statistic.setMaintainanceRegister(maintaince);
                            }
                            if (specialit != null) {
                                statistic.setSpecialistRegister(specialit);
                            }
                            statistic.setNumberOfRegistrantsInStudentRegister(statistic.getNumberOfRegistrantsInStudentRegister() + registrationService.getActiveRegistrations(statistic.getCourse(), student));
                            List<Duration> durations = new ArrayList<Duration>();
                            for (Duration d : councilDurationService.get(councilDuration.getId()).getDurations()) {
                                durations.add(d);
                            }

                            statistic.setNumberOfRegistrantsInMainRegister(registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getMainRegister(), durations, registrantActivityType, Boolean.TRUE, null));
                            statistic.setNumberOfRegistrantsInProvisionalRegister(registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getProvisionalRegister(), durations, registrantActivityType, Boolean.TRUE, null));

                            statistic.setNumberOfInActiveRegistrantsInMainRegister(registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getMainRegister(), durations, registrantActivityType, Boolean.FALSE, null));
                            statistic.setNumberOfInActiveRegistrantsInProvisionalRegister(registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getProvisionalRegister(), durations, registrantActivityType, Boolean.FALSE, null));

                            if (intern != null) {
                                statistic.setNumberOfRegistrantsInInternRegister(registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getInternRegister(), durations, registrantActivityType, Boolean.TRUE, null));
                                statistic.setNumberOfInActiveRegistrantsInInternRegister(registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getInternRegister(), durations, registrantActivityType, Boolean.FALSE, null));
                            }
                            if (maintaince != null) {
                                statistic.setNumberOfRegistrantsInMaintainceRegister(registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getMaintainanceRegister(), durations, registrantActivityType, Boolean.TRUE, null));
                                statistic.setNumberOfInActiveRegistrantsInMaintainceRegister(registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getMaintainanceRegister(), durations, registrantActivityType, Boolean.FALSE, null));
                            }
                            if (intern != null) {
                                statistic.setNumberOfRegistrantsInSpecialistRegister(registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getSpecialistRegister(), durations, registrantActivityType, Boolean.TRUE, null));
                                statistic.setNumberOfInActiveRegistrantsInSpecialistRegister(registrantActivityService.getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(c, statistic.getSpecialistRegister(), durations, registrantActivityType, Boolean.FALSE, null));
                            }

                            registerStatistics.add(statistic);
                        }

                        try {
                            Map parameters = new HashMap();
                            parameters.put("comment", " Discipline Statistics - " + registrantActivityType + " for Period " + councilDuration.getName());

                            SummaryRegisterReport disciplineStatisticReport = new SummaryRegisterReport();

                            ByteArrayResource resource
                            = ReportResourceUtils.getReportResource(disciplineStatisticReport, contentType, registerStatistics,
                                    parameters);
                            IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                    RequestCycle.get().getResponse(), null);

                            resource.respond(a);

                            // To make Wicket stop processing form after sending response
                            RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                        } catch (JRException ex) {
                            ex.printStackTrace(System.out);
                        }
                    }
                }
        );

        form.add(
                new Button("qualificationStatisticReport") {
                    @Override
                    public void onSubmit() {
                        List<RegisterStatistic> registerStatistics = new ArrayList<RegisterStatistic>();
                        Register main = generalParametersService.get().getMainRegister();
                        Register provisional = generalParametersService.get().getProvisionalRegister();
                        Register student = generalParametersService.get().getStudentRegister();
                        Register intern = generalParametersService.get().getInternRegister();
                        Register maintaince = generalParametersService.get().getMaintenanceRegister();
                        Register specialit = generalParametersService.get().getSpecialistRegister();

                        for (Qualification q : registrantQualificationService.getDistinctQualificationsInRegistrantQualification()) {
                            RegisterStatistic statistic = new RegisterStatistic();
                            statistic.setQualification(q);
                            statistic.setMainRegister(main);
                            statistic.setStudentRegister(student);
                            statistic.setProvisionalRegister(provisional);
                            if (intern != null) {
                                statistic.setInternRegister(intern);
                            }
                            if (maintaince != null) {
                                statistic.setMaintainanceRegister(maintaince);
                            }
                            if (specialit != null) {
                                statistic.setSpecialistRegister(specialit);
                            }

                            for (Duration d : councilDurationService.get(councilDuration.getId()).getDurations()) {
                                statistic.setNumberOfRegistrantsInMainRegister(statistic.getNumberOfRegistrantsInMainRegister() + registrantActivityService.getTotalPerQualificationAndDurationAndRegisterAndRegistrantActivity(q, statistic.getMainRegister(), d, registrantActivityType));
                                statistic.setNumberOfRegistrantsInProvisionalRegister(statistic.getNumberOfRegistrantsInProvisionalRegister() + registrantActivityService.getTotalPerQualificationAndDurationAndRegisterAndRegistrantActivity(q, statistic.getProvisionalRegister(), d, registrantActivityType));

                                if (intern != null) {
                                    statistic.setNumberOfRegistrantsInInternRegister(statistic.getNumberOfRegistrantsInInternRegister() + registrantActivityService.getTotalPerQualificationAndDurationAndRegisterAndRegistrantActivity(q, statistic.getInternRegister(), d, registrantActivityType));
                                }
                                if (maintaince != null) {
                                    statistic.setNumberOfRegistrantsInMaintainceRegister(statistic.getNumberOfRegistrantsInMaintainceRegister() + registrantActivityService.getTotalPerQualificationAndDurationAndRegisterAndRegistrantActivity(q, statistic.getMaintainanceRegister(), d, registrantActivityType));
                                }
                                if (intern != null) {
                                    statistic.setNumberOfRegistrantsInSpecialistRegister(statistic.getNumberOfRegistrantsInSpecialistRegister() + registrantActivityService.getTotalPerQualificationAndDurationAndRegisterAndRegistrantActivity(q, statistic.getSpecialistRegister(), d, registrantActivityType));
                                }
                            }
                            if (!q.getName().equals("NO QUALIFICATION")) {
                                registerStatistics.add(statistic);
                            }
                        }

                        try {
                            Map parameters = new HashMap();
                            parameters.put("comment", " Qualification Statistics - " + registrantActivityType + " for Period " + councilDuration.getName());

                            QualificationStatisticReport qualificationStatisticReport = new QualificationStatisticReport();

                            ByteArrayResource resource
                            = ReportResourceUtils.getReportResource(qualificationStatisticReport, contentType, registerStatistics,
                                    new HashMap<String, Object>());
                            IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                                    RequestCycle.get().getResponse(), null);

                            resource.respond(a);

                            // To make Wicket stop processing form after sending response
                            RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                        } catch (JRException ex) {
                            ex.printStackTrace(System.out);
                        }
                    }
                }
        );

        add(
                new FeedbackPanel("feedback"));

    }
}


//~ Formatted by Jindent --- http://www.jindent.com
