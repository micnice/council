/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.service;

import java.util.List;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.OldCertificateIssued;

/**
 *
 * @author kelvin
 */
public interface OldCertificateIssuedService extends IGenericService<OldCertificateIssued> {

    public List<OldCertificateIssued> getCertificates(Registrant registrant);
    
    public OldCertificateIssued getCertificate(Long certificateProcessRecordID);  
    
    
}
