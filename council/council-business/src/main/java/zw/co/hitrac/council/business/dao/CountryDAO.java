package zw.co.hitrac.council.business.dao;

import zw.co.hitrac.council.business.domain.Country;

/**
 *
 * @author Charles Chigoriwa
 */
public interface CountryDAO extends IGenericDAO<Country> {
    
}
