package zw.co.hitrac.council.business.service.impl;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.RegistrationDAO;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegisterService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.business.utils.DateUtil;

import static zw.co.hitrac.council.business.utils.DateUtil.convertToLocalDate;
import static zw.co.hitrac.council.business.utils.DateUtil.getPeriodLengthBetweenDates;
import static zw.co.hitrac.council.business.utils.StringFormattingUtil.getMissingFieldMessage;

/**
 * @author Kelvin Goredema
 * @author Michael Matiashe
 */
@Service
@Transactional
public class RegistrationServiceImpl implements RegistrationService {
    public static final long PROVISIONAL_TRANSFER_TO_MAIN_REGISTER_YEARS = 3L;

    public static final String REGISTRANT = "Registrant";

    @Autowired
    private RegistrationDAO registrationDAO;
    @Autowired
    private GeneralParametersService generalParametersService;

    @Transactional
    public Registration save(Registration registration) {

        return registrationDAO.save(registration);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Registration> findAll() {

        return registrationDAO.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Registration get(Long id) {

        return registrationDAO.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Registration> findAll(Boolean retired) {

        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Registration> getRegistrations(Registrant registrant) {

        return registrationDAO.getRegistrations(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Registration getRegistrationByApplication(Application application) {

        return registrationDAO.getRegistrationByApplication(application);
    }

    public Registration getCurrentRegistration(Institution institution) {
        return registrationDAO.getCurrentRegistration(institution);
    }

    @Override
    public Registration getCurrentRegistrationByRegistrant(Registrant registrant) {
        return registrationDAO.getCurrentRegistration(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Boolean getRegistrationIsDeRegistered(Registrant registrant, RegistrantStatus registrantStatus) {
        return registrationDAO.getRegistrationIsDeRegistered(registrant, registrantStatus);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Registration> getRegistrations(Course course, Register register, Institution institution) {

        return registrationDAO.getRegistrations(course, register, institution);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Registrant> getRegistrants(Course course, Register register, Institution institution, String searchText, String gender, Citizenship citizenship, Date startDate, Date endDate) {

        return registrationDAO.getRegistrants(course, register, institution, searchText, gender, citizenship, startDate, endDate);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<RegistrantData> getRegistrantList(Course course, Register register, Institution institution, RegistrantStatus registrantStatus, String searchText, String gender, Citizenship citizenship, Date startDate, Date endDate) {

        return registrationDAO.getRegistrantList(course, register, institution, registrantStatus, searchText, gender, citizenship, startDate, endDate);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Registration> getActiveRegistrations(Registrant registrant) {

        return registrationDAO.getActiveRegistrations(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Integer getActiveRegistrations(Course course, Register register) {

        return registrationDAO.getActiveRegistrations(course, register);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Integer getSuspendedRegistrations(Course course, Register register) {

        return registrationDAO.getSuspendedRegistrations(course, register);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Registration getCurrentRegistrationByRegister(Register register, Registrant registrant) {

        return registrationDAO.getCurrentRegistrationByRegister(register, registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Registration> getRegistrations(Course course, Register register, Institution institution, String searchText, String gender, Citizenship citizenship) {

        return registrationDAO.getRegistrations(course, register, institution, searchText, gender, citizenship);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Boolean hasCurrentRegistration(Registrant registrant) {

        return registrationDAO.hasCurrentRegistration(registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Registration getCurrentRegistrationByCourseAndRegister(Registrant registrant, Register register, Course course) {

        return registrationDAO.getCurrentRegistrationByCourseAndRegister(registrant, register, course);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Registration> getRegistrationsAll(Course course, Register register, Registrant registrant) {

        return registrationDAO.getRegistrationsAll(course, register, registrant);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Registration> getActiveRegistrationByRegister(Course course, Register register) {

        return registrationDAO.getActiveRegistrationByRegister(course, register);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Course> getDistinctCourseFromRegistration() {

        return registrationDAO.getDistinctCourseFromRegistration();
    }

    @Override
    public List<Registration> getActiveRegistrations(Course course) {
        return registrationDAO.getActiveRegistrations(course);
    }

    @Override
    public List<Registration> getRegisteredInstitutionsRegistrations(Boolean closed) {
        return registrationDAO.getRegisteredInstitutionsRegistrations(closed);
    }

    @Override
    public List<Registration> getActiveRegistrations(String idList) {
        return registrationDAO.getActiveRegistrations(idList);
    }

    @Override
    public Long getNumberOfYearsInRegister(Registration registration) {

        Objects.requireNonNull(registration, getMissingFieldMessage("Registration"));

        return getPeriodLengthBetweenDates(ChronoUnit.YEARS,
                convertToLocalDate(registration.getRegistrationDate()), LocalDate.now());

    }


    @Override
    public boolean isDueForTransferFromProvisionalToMainRegister(Registration registration) {

        Objects.requireNonNull(registration, getMissingFieldMessage(REGISTRANT));
        final boolean isProvisionalRegistration = (registration.getRegister().equals(generalParametersService.get().getProvisionalRegister()));
        if (isProvisionalRegistration) {

            return getNumberOfYearsInRegister(registration) >= generalParametersService.get().getMinimumYearsInProvisionalRegister();


        } else {
            return false;
        }

    }


    @Override
    public List<RegistrantData> getRegistrantsDueForTransfer() {
        List<RegistrantData> registrantDataList = new ArrayList<>();

        List<Registration> registrations = getRegistrations(null, generalParametersService.get().getProvisionalRegister()
                , null, null, null, null);

        registrations.forEach(r ->
        {
            if (isDueForTransferFromProvisionalToMainRegister(r)) {
                RegistrantData registrantData = new RegistrantData(r.getRegistrant().getId(),
                        r.getRegistrant().getFirstname(), r.getRegistrant().getLastname(),
                        r.getRegistrant().getMiddlename(), r.getRegistrant().getBirthDate(),
                        r.getRegistrant().getGender(), r.getRegistrant().getIdNumber(),
                        r.getRegistrant().getRegistrationNumber());
                registrantDataList.add(registrantData);
            }
        });


        return registrantDataList;
    }

    public Boolean hasBeenDeRegistered(Registrant registrant) {
        if (hasCurrentRegistration(registrant)) {
            return false;
        } else {
            return getRegistrationIsDeRegistered(registrant, generalParametersService.get().getDeRegisteredStatus());
        }
    }


    public boolean hasBeenRegisteredBeforeCouncilDuration(CouncilDuration councilDuration, Long registrantId){
        return registrationDAO.hasBeenRegisteredBeforeCouncilDuration(councilDuration,registrantId);
    }

    public List<Long> getRegistrantIdsRegisteredBeforeCouncilDuration(CouncilDuration councilDuration) {
        return registrationDAO.getRegistrantIdsRegisteredBeforeCouncilDuration(councilDuration);
    }
}
