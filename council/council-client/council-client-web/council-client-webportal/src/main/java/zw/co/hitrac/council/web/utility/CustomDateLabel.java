package zw.co.hitrac.council.web.utility;

import org.apache.wicket.datetime.DateConverter;
import org.apache.wicket.datetime.markup.html.basic.DateLabel;
import org.apache.wicket.model.IModel;

import java.util.Date;

/**
 * Created by clive on 7/8/15.
 *
 * Allows setting custom date format
 */
public class CustomDateLabel extends DateLabel {

    public CustomDateLabel(String id){

        super(id, DateTimeUtils.getConverter());
    }

    public CustomDateLabel(String id, DateConverter converter) {

        super(id, converter);
    }

    public CustomDateLabel(String id, IModel<Date> model, DateConverter converter) {

        super(id, model, converter);
    }
}
