package zw.co.hitrac.council.business.service;

import zw.co.hitrac.council.business.domain.CourseGroup;

/**
 *
 * @author Michael Matiashe
 */
public interface CourseGroupService extends IGenericService<CourseGroup> {
    
}
