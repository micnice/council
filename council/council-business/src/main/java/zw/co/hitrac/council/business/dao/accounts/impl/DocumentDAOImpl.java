package zw.co.hitrac.council.business.dao.accounts.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.accounts.DocumentDAO;
import zw.co.hitrac.council.business.dao.repo.accounts.DocumentRepository;
import zw.co.hitrac.council.business.domain.accounts.Document;
import zw.co.hitrac.council.business.domain.accounts.DocumentType;

/**
 *s
 * @author Michael Matiashe
 */
@Repository
public class DocumentDAOImpl implements DocumentDAO {

    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private DocumentRepository documentRepository;

    public Document save(Document t) {
        return documentRepository.save(t);
    }

    public List<Document> findAll() {
        return documentRepository.findAll();
    }

    public Document get(Long id) {
        return documentRepository.findOne(id);
    }

    public DocumentRepository getDocumentRepository() {
        return documentRepository;
    }

    public void setDocumentRepository(DocumentRepository documentRepository) {
        this.documentRepository = documentRepository;
    }

    public List<Document> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Document> getDocuments(DocumentType documentType) {
        return entityManager.createQuery("SELECT d FROM Document d WHERE d.documentType=:docType").setParameter("docType", documentType).getResultList();
    }

}
