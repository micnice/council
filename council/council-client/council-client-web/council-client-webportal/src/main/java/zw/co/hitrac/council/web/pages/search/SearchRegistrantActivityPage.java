package zw.co.hitrac.council.web.pages.search;

//~--- non-JDK imports --------------------------------------------------------
import net.sf.jasperreports.engine.JRException;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.handler.EmptyRequestHandler;
import org.apache.wicket.request.resource.ByteArrayResource;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.reports.RegistrantData;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.reports.utils.ContentType;
import zw.co.hitrac.council.web.pages.TemplatePage;
import zw.co.hitrac.council.web.utility.ReportResourceUtils;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import zw.co.hitrac.council.business.domain.RegistrantActivityType;
import zw.co.hitrac.council.business.service.RegistrantActivityService;
import zw.co.hitrac.council.reports.RegistrantDataReport;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.registration.RegistrantDataViewPanel;
import zw.co.hitrac.council.web.utility.DatePickerUtil;

/**
 *
 * @author takunda Dhlakama
 */
public class SearchRegistrantActivityPage extends TemplatePage {

    private Date startDate, endDate;
    @SpringBean
    GeneralParametersService generalParametersService;
    private static final String TOP_LOGO = "zw/co/hitrac/council/reports/images/logo1.jpg";
    private static final String WATERMARK_LOGO = "zw/co/hitrac/council/reports/images/logo.png";
    private RegistrantActivityType registrantActivityType;
    @SpringBean
    private RegistrantActivityService registrantActivityService;
  
    public SearchRegistrantActivityPage() {
        menuPanelManager.getSearchPanel().setTopMenuCurrent(true);
        menuPanelManager.getSearchPanel().setSearchAdvanced(true);

        PropertyModel<RegistrantActivityType> registrantActivityTypeModel = new PropertyModel<RegistrantActivityType>(this, "registrantActivityType");
        PropertyModel<Date> startDateModel = new PropertyModel<Date>(this, "startDate");
        PropertyModel<Date> endDateModel = new PropertyModel<Date>(this, "endDate");

        Form<?> form = new Form("form");

        List<RegistrantActivityType> registrantActivityTypes = new ArrayList<RegistrantActivityType>(Arrays.asList(RegistrantActivityType.values()));
        form.add(new DropDownChoice("registrantActivityType", registrantActivityTypeModel, registrantActivityTypes).setRequired(true).add(new ErrorBehavior()));
        form.add(new TextField<Date>("startDate", startDateModel).setRequired(true).add(new ErrorBehavior()).add(DatePickerUtil.getDatePicker()));
        form.add(new TextField<Date>("endDate", endDateModel).setRequired(true).add(new ErrorBehavior()).add(DatePickerUtil.getDatePicker()));
        add(form);

        final IModel<List<RegistrantData>> model = new LoadableDetachableModel<List<RegistrantData>>() {
            @Override
            protected List<RegistrantData> load() {
                if (registrantActivityType == null && startDate==null && endDate ==null) {
                    return new ArrayList<RegistrantData>();
                } else {
                    return registrantActivityService.getRegistrant(registrantActivityType, startDate, endDate);
                }
            }
        };

        form.add(new Button("registrantlist") {
            @Override
            public void onSubmit() {
                Map parameters = new HashMap();
                String start = "";
                String end = "";
                String activity = "";
                if (startDate != null) {
                    start = ("Start Date - " + startDate + ". ");
                }
                if (endDate != null) {
                    end = ("End Date - " + endDate + ". ");
                }
                if (registrantActivityType != null) {
                    activity = ("Activity - " + registrantActivityType + ". ");
                }                
                parameters.put("comment", "Registrant Activity * " + start.concat(end).concat(activity));
                try {
                    RegistrantDataReport registrantDataReport = new RegistrantDataReport();
                    ContentType contentType = ContentType.PDF;
                    ByteArrayResource resource = ReportResourceUtils.getReportResource(registrantDataReport, contentType,
                            registrantActivityService.getRegistrant(registrantActivityType, startDate, endDate), parameters);
                    IResource.Attributes a = new IResource.Attributes(RequestCycle.get().getRequest(),
                            RequestCycle.get().getResponse(), null);

                    resource.respond(a);

                    // To make Wicket stop processing form after sending response
                    RequestCycle.get().replaceAllRequestHandlers(new EmptyRequestHandler());
                } catch (JRException ex) {
                    ex.printStackTrace(System.out);
                }
            }
        });
    
        // Registrant List Data View Panel
        add(new RegistrantDataViewPanel("registrantDataListPanel", model));
        add(new FeedbackPanel("feedback"));
    }

}


//~ Formatted by Jindent --- http://www.jindent.com
