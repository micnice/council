package zw.co.hitrac.council.web.pages.registration;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.Employment;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.service.EmploymentService;
import zw.co.hitrac.council.business.service.RegistrantService;
import zw.co.hitrac.council.web.utility.CustomDateLabel;
import zw.co.hitrac.council.web.utility.CustomDateTimeLabel;

/**
 * @author Tatenda Chiwandire
 */
public class EmploymentPanelList extends Panel {

    @SpringBean
    private EmploymentService employmentService;
    @SpringBean
    private RegistrantService registrantService;

    /**
     * Constructor
     *
     * @param id
     */
    public EmploymentPanelList(String id) {
        super(id);

        add(new PropertyListView<Employment>("employmentList") {
            @Override
            protected void populateItem(ListItem<Employment> item) {
                Link<Employment> viewLink = new Link<Employment>("employmentEditPage", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new EmploymentEditPage(getModelObject().getId(), null));
                    }
                };

                item.add(viewLink);
                item.add(new CustomDateLabel("startDate"));
                item.add(new CustomDateLabel("endDate"));
                item.add(new Label("institution"));
                item.add(new Label("post"));
                item.add(new Label("employmentType"));
                item.add(new Label("employer"));
                item.add(new Label("employmentStatus"));
                item.add(new Label("employmentArea"));
                item.add(new Label("city"));
                item.add(new Label("createdBy"));
                item.add(new Label("modifiedBy"));
                item.add(CustomDateTimeLabel.forDate("dateCreated",
                        new PropertyModel<>(item.getModel(), "dateCreated")));
                item.add(CustomDateTimeLabel.forDate("dateModified",
                        new PropertyModel<>(item.getModel(), "dateModified")));
            }
        });
    }

    /**
     * Constructor
     *
     * @param id
     * @param registrantModel
     */
    public EmploymentPanelList(String id, final IModel<Registrant> registrantModel) {
        super(id);

        add(new Link<Registrant>("employmentEditPage") {
            @Override
            public void onClick() {
                setResponsePage(new EmploymentEditPage(null, registrantModel));
            }
        });

        add(new PropertyListView<Employment>("employmentList", employmentService.getEmployments(registrantModel.getObject())) {
            @Override
            protected void populateItem(ListItem<Employment> item) {
                Link<Employment> viewLink = new Link<Employment>("employmentEditPage", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new EmploymentEditPage(getModelObject().getId(), null));
                    }
                };

                item.add(viewLink);
                item.add(new CustomDateLabel("startDate"));
                item.add(new CustomDateLabel("endDate"));
                item.add(new Label("institution"));
                item.add(new Label("post"));
                item.add(new Label("employmentType"));
                item.add(new Label("employer"));
                item.add(new Label("employmentStatus"));
                item.add(new Label("employmentArea"));
                item.add(new Label("city"));
                item.add(new Label("createdBy"));
                item.add(new Label("modifiedBy"));
                item.add(CustomDateTimeLabel.forDate("dateCreated",
                        new PropertyModel<>(item.getModel(), "dateCreated")));
                item.add(CustomDateTimeLabel.forDate("dateModified",
                        new PropertyModel<>(item.getModel(), "dateModified")));

            }
        });


    }
}
