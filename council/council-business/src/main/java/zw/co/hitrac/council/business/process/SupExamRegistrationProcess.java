package zw.co.hitrac.council.business.process;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.domain.ModulePaper;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.Customer;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.domain.examinations.*;
import zw.co.hitrac.council.business.service.GeneralParametersService;
import zw.co.hitrac.council.business.service.RegistrantActivityService;
import zw.co.hitrac.council.business.service.RegistrationService;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationDebtComponentService;
import zw.co.hitrac.council.business.service.examinations.ExamRegistrationService;
import zw.co.hitrac.council.business.service.examinations.ExamResultService;
import zw.co.hitrac.council.business.service.examinations.ExamService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.business.utils.DateUtil;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author Mike Matiashe
 */
@Service
public class SupExamRegistrationProcess {

    @Autowired
    private ExamRegistrationService examRegistrationService;
    @Autowired
    private ExamService examService;
    @Autowired
    private InvoiceProcess invoiceProcess;
    @Autowired
    private RegistrationService registrationService;
    @Autowired
    private GeneralParametersService generalParametersService;
    @Autowired
    private ExamRegistrationProcess examRegistrationProcess;
    @Autowired
    private ExamResultService examResultService;
    @Autowired
    private ExamRegistrationDebtComponentService examRegistrationDebtComponentService;
    @Autowired
    RegistrantActivityService registrantActivityService;
    @Autowired
    private RegistrantActivityProcess registrantActivityProcess;

    @Transactional

    public ExamRegistration register(ExamRegistration examRegistration) {

        if (generalParametersService.get().getLastCandidateNumber() == null) {
            throw new CouncilException("Administrator MUST Configure Candidate numbers");
        }

        if (examRegistrationService.getNumberofRewrites(examRegistration.getRegistration()) > generalParametersService.get().getNumberOfTimesToWriteSupExam()) {
            throw new CouncilException("Candidate cannot write without council approval");
        }

//        if (!validateSup(examRegistration.getRegistration().getRegistrant())) {
//            throw new CouncilException("Registrant should register for sup within six months");
//
//        }
        if (registrationDateValidation(examRegistration)) {
            throw new CouncilException("Registration date cannot be future date");
        }

        List<Product> productsFound = new ArrayList<Product>();
        ExamSetting examSetting = examRegistration.getExamSetting();
        //filter exams needed settings
        List<ModulePaper> failedModulePapers = getFailedModulePapers(examRegistration.getRegistration());
        Set<ExamResult> failedResults = new HashSet<ExamResult>();
        for (ModulePaper modulePaper : examService.findExamPapers(examSetting)) {
            if (failedModulePapers.contains(modulePaper)) {

                //check if paper is already passed
                failedResults.add(new ExamResult(examRegistration, examService.getExam(examSetting, modulePaper)));

                Product moduleProduct = modulePaper.getProduct();
                if (moduleProduct == null) {
                    throw new CouncilException("Product for paper [" + modulePaper.getName() + "] not set");
                }

                if (moduleProduct.getProductPrice() == null) {
                    throw new CouncilException("Product price for module paper product [" + modulePaper.getProduct().getName() + "] not set");
                }

                productsFound.add(moduleProduct);
            }
        }

        if (productsFound.isEmpty()) {
            throw new CouncilException("No papers set for the period");
        }

        if (failedResults.isEmpty()) {
            throw new CouncilException("No exams set for that period");
        }

        Registration r = registrationService.get(examRegistration.getRegistration().getId());
        if (r == null) {
            throw new CouncilException("Cannot Register for Supplementary Exam, Previous Registration not Found");
        }

        Set<ExamResult> examResults = new HashSet<ExamResult>();
        Set<ModulePaper> passedPaperlist = new HashSet<ModulePaper>(getPassedModulePapers(r));

        for (ExamRegistration e : examRegistrationService.getExamRegistrations(r)) {
            if (!e.getReversedResult()) {
                for (ExamResult er : e.getExamPassList()) {
                    if (passedPaperlist.contains(er.getExam().getModulePaper())) {
                        examResults.add(er);
                    }
                }
            }
        }
        examRegistration.setSupplementaryExam(Boolean.TRUE);
        examRegistration = examRegistrationProcess.saveExamRegistrationCandidateNumber(examRegistration);

        for (ExamResult e : failedResults) {
            ExamResult newExamResult = new ExamResult();
            newExamResult.setSupplementaryExam(Boolean.FALSE);
            newExamResult.setExamRegistration(examRegistration);
            newExamResult.setExam(e.getExam());
            examResultService.save(newExamResult);
        }

        for (ExamResult e : examResults) {
            ExamResult newExamResult = new ExamResult();
            newExamResult.setMark(e.getMark());
            newExamResult.setExamAttendance(Boolean.TRUE);
            newExamResult.setSupplementaryExam(Boolean.TRUE); //Confirmation this is a supplementary Exam
            newExamResult.setExamRegistration(examRegistration);
            newExamResult.setExam(e.getExam());
            newExamResult.setPreviousExamRegistration(e.getExamRegistration()); //Previous Exam Registration
            examResultService.save(newExamResult);
        }
        for (Product productFound : productsFound) {
            BigDecimal price = productFound.getProductPrice().getPrice();

            Customer customer = examRegistration.getRegistration().getRegistrant().getCustomerAccount();
            Account salesAccount = productFound.getSalesAccount();
            DebtComponent debtComponent = invoiceProcess.invoice(price, customer, salesAccount, productFound);
            ExamRegistrationDebtComponent examRegistrationDebtComponent = new ExamRegistrationDebtComponent();
            examRegistrationDebtComponent.setExamRegistration(examRegistration);
            examRegistrationDebtComponent.setDebtComponent(debtComponent);
            examRegistrationDebtComponentService.save(examRegistrationDebtComponent);
        }
        registrantActivityProcess.examRegistrationRegistrantActivity(examRegistration.getRegistration(), examSetting);
        return examRegistration;
    }

    public Exam getFailedExamination(Registration studentRegistration) {
        for (ExamRegistration examRegistration : examRegistrationService.getExamRegistrations(studentRegistration)) {
            List<Exam> exams = examRegistration.getExamList();
            for (Exam exam : exams) {
                if (!examRegistration.getReversedResult()) {
                    if (!examRegistration.getPassStatus()) {
                        return exam;
                    }
                }
            }
        }
        return null;
    }


    public List<ModulePaper> getFailedModulePapers(Registration registration) {
        Set<ModulePaper> modulePapers = new HashSet<ModulePaper>();
        Set<ModulePaper> passedPapers = new HashSet<ModulePaper>();
        //includes disqualified papers
        Set<ModulePaper> failedPapers = new HashSet<ModulePaper>();
        Set<Exam> exams = new HashSet<Exam>();
        Set<Exam> passExams = new HashSet<Exam>();

        for (ExamRegistration e : examRegistrationService.getExamRegistrations(registration)) {
            if (!e.getReversedResult()) {
                exams.addAll(e.getExamFailedList());
            }
            if (e.getDisqualified()) {
                exams.addAll(e.getDisqualifiedExamList());
            }
        }

        for (Exam e : exams) {
            failedPapers.add(e.getModulePaper());
        }

        for (ExamRegistration e : examRegistrationService.getExamRegistrations(registration)) {
            if (!e.getReversedResult()) {
                passExams.addAll(e.getPassedExams());
            }
        }

        for (Exam e : passExams) {
            passedPapers.add(e.getModulePaper());
        }

        failedPapers.removeAll(passedPapers);

        modulePapers.addAll(failedPapers);

        return new ArrayList<ModulePaper>(modulePapers);
    }

    public List<ModulePaper> getPassedModulePapers(Registration registration) {
        Set<ModulePaper> passedPapers = new HashSet<ModulePaper>();
        Set<Exam> exams = new HashSet<Exam>();

        for (ExamRegistration e : examRegistrationService.getExamRegistrations(registration)) {
            if (!e.getReversedResult()) {
                exams.addAll(e.getPassedExams());
            }
        }

        for (Exam e : exams) {
            passedPapers.add(e.getModulePaper());
        }

        return new ArrayList<ModulePaper>(passedPapers);
    }

    public List<ModulePaper> getDisqualifiedModulePapers(Registration registration) {
        Set<ModulePaper> disqualifiedPapers = new HashSet<ModulePaper>();
        Set<Exam> exams = new HashSet<Exam>();

        for (ExamRegistration e : examRegistrationService.getExamRegistrations(registration)) {
            if (!e.getReversedResult()) {
                exams.addAll(e.getDisqualifiedExamList());
            }
        }

        for (Exam e : exams) {
            disqualifiedPapers.add(e.getModulePaper());
        }

        return new ArrayList<ModulePaper>(disqualifiedPapers);
    }

    private boolean validateSup(Registrant registrant) {
        boolean status = false;
        for (ExamRegistration r : examRegistrationService.getExamRegistrations(registrant)) {
            if (DateUtil.DifferenceInMonths(r.getDateCreated(), new Date()) < generalParametersService.get().getMinimumPeriodForSup()) {
                status = true;
            }
        }
        return status;
    }

    public boolean registrationDateValidation(ExamRegistration examRegistration) {
        boolean status = false;
        if (examRegistration.getRegistration().getRegistrationDate().after(new Date())) {
            status = true;
        }
        return status;
    }


    public void checkDuplicateRegistration(ExamRegistration examRegistration) {
        if (examRegistrationService.getCheckExamRegistration(examRegistration.getExamSetting(), examRegistration.getRegistration())) {
            throw new CouncilException("Cannot register for the same exam period");
        }
    }


    public Boolean checkOpenExamRegistration(Registration registration) {
        for (ExamRegistration e : examRegistrationService.getExamRegistrations(registration)) {
            if (e.getClosed() == null) {
                return Boolean.TRUE;
            }
            if (!e.getClosed()) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }


    public Boolean checkSupplementaryStatus(Registration registration) {
        if (!getFailedModulePapers(registration).isEmpty() || !getDisqualifiedModulePapers(registration).isEmpty()) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
}
