/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package zw.co.hitrac.council.business.domain;

import org.hibernate.envers.Audited;
import zw.co.hitrac.council.business.utils.DateUtil;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author edwin
 */
@Entity
@Table(name = "registrantdisciplinary")
@Audited
public class RegistrantDisciplinary extends BaseEntity {

    private Registrant registrant;
    private MisconductType misconductType;
    private Date caseDate;
    private CaseOutcome caseOutcome;
    private Date dateResolved;
    private Date penaltyStartDate;
    private Date penaltyEndDate;
    private String comment;
    private String caseStatus; //Pending/Closed
    public static String PENDING = "PENDING";
    public static String CLOSED = "CLOSED";

    @ManyToOne
    public Registrant getRegistrant() {
        return registrant;
    }

    public void setRegistrant(Registrant registrant) {
        this.registrant = registrant;
    }

    @ManyToOne
    public MisconductType getMisconductType() {
        return misconductType;
    }

    public void setMisconductType(MisconductType misconductType) {
        this.misconductType = misconductType;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getCaseDate() {
        return caseDate;
    }

    public void setCaseDate(Date caseDate) {
        this.caseDate = caseDate;
    }

    @Transient
    public Integer getCaseYear() {

        if (getCaseDate() == null) {
            return null;
        } else {
            return DateUtil.getYearFromDate(getCaseDate());
        }
    }

    @OneToOne
    public CaseOutcome getCaseOutcome() {
        return caseOutcome;
    }

    public void setCaseOutcome(CaseOutcome caseOutcome) {
        this.caseOutcome = caseOutcome;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getDateResolved() {
        return dateResolved;
    }

    public void setDateResolved(Date dateResolved) {
        this.dateResolved = dateResolved;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getPenaltyStartDate() {
        return penaltyStartDate;
    }

    public void setPenaltyStartDate(Date penaltyStartDate) {
        this.penaltyStartDate = penaltyStartDate;
    }

    @Temporal(javax.persistence.TemporalType.DATE)
    public Date getPenaltyEndDate() {
        return penaltyEndDate;
    }

    public void setPenaltyEndDate(Date penaltyEndDate) {
        this.penaltyEndDate = penaltyEndDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCaseStatus() {
        return caseStatus;
    }

    public void setCaseStatus(String caseStatus) {
        this.caseStatus = caseStatus;
    }

    public static List<String> getCaseStatusList = Arrays.asList(new String[]{RegistrantDisciplinary.PENDING,
            RegistrantDisciplinary.CLOSED});

}
