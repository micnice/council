package zw.co.hitrac.council.business.dao;

import java.util.Date;
import java.util.List;

import zw.co.hitrac.council.business.domain.Conditions;
import zw.co.hitrac.council.business.domain.ContactType;
import zw.co.hitrac.council.business.domain.CouncilDuration;
import zw.co.hitrac.council.business.domain.Course;
import zw.co.hitrac.council.business.domain.Duration;
import zw.co.hitrac.council.business.domain.EmploymentType;
import zw.co.hitrac.council.business.domain.Institution;
import zw.co.hitrac.council.business.domain.Qualification;
import zw.co.hitrac.council.business.domain.Register;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantActivity;
import zw.co.hitrac.council.business.domain.RegistrantActivityType;
import zw.co.hitrac.council.business.domain.RegistrantContact;
import zw.co.hitrac.council.business.domain.reports.RegistrantData;

/**
 * @author hitrac
 */
public interface RegistrantActivityDAO extends IGenericDAO<RegistrantActivity> {

     void remove(RegistrantActivity registrantActivity);

     List<RegistrantActivity> getRegistrantActivities(Registrant registrant);

     Integer getTotalPerCourseAndDurationAndRegisterAndRegistrantActivity(Course course, Register register, List<Duration> durations, RegistrantActivityType registrantActivityType, Boolean active);

     Integer getTotalPerQualificationAndDurationAndRegisterAndRegistrantActivity(Qualification qualification, Register register, Duration duration, RegistrantActivityType registrantActivityType);

     Integer getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(Course course, Register register, List<Duration> durations, RegistrantActivityType registrantActivityType, Boolean active, Conditions condition);

     List<RegistrantData> getListPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(Course course, Register register, List<Duration> durations, RegistrantActivityType registrantActivityType, Boolean active, Enum<EmploymentType> e, Conditions condition);

     Integer getTotalPerQualificationMappedCourseAndDurationAndRegisterAndRegistrantActivity(Course course, Register register, List<Duration> durations, RegistrantActivityType registrantActivityType, Boolean active, Enum<EmploymentType> e, Conditions condition);

     List<RegistrantActivity> getRegistrantActivities(Registrant registrant, RegistrantActivityType registrantActivityType);

     RegistrantActivity getRegistrantActivityByDuration(Registrant registrant, Duration duration);

     boolean checkRegistrantActivity(Registrant registrant, RegistrantActivityType registrantActivityType, Duration duration);

     boolean checkRegistrantActivity(Registrant registrant, RegistrantActivityType registrantActivityType, CouncilDuration councilDuration);

     List<RegistrantData> getRegistrant(RegistrantActivityType registrantActivityType, Date startDate, Date endDate);

     List<RegistrantData> getListPerCourseAndDurationAndRegisterAndRegistrantActivity(Course course, Register register, List<Duration> durations, RegistrantActivityType registrantActivityType, Boolean active);

     List<RegistrantContact> getEmailAddressesPerQualificationMappedCourse(Course course, Register register, ContactType contactType);

     List<String> getEmailAddresses(Course course, Register register, ContactType contactType);

     List<RegistrantData> getListPerQualificationAndDurationAndRegisterAndRegistrantActivity(Qualification qualification, Register register, Duration duration, RegistrantActivityType registrantActivityType);

     List<RegistrantActivity> getRegistrantsByRegistrantActivityType(RegistrantActivityType registrantActivityType);

     CouncilDuration getLastCouncilDurationRenewal(Registrant registrant);

     String getLastRenewal(Registrant registrant);

     RegistrantActivity getLastRenewalActivity(Registrant registrant);

     RegistrantActivity getRegistrantActivity(RegistrantActivityType registrantActivityType, Registrant registrant);

     List<CouncilDuration> getRegisteredRenewalPeriods(String registrationNumber);

     List<Registrant> getRegistrantsRenewedInCouncilDurationNotInListOfCouncilDurations(CouncilDuration councilDuration, List<CouncilDuration> councilDurations);

     List<Registrant> getRegistrantsRenewedInCouncilDuration(CouncilDuration councilDuration, Course course);

     List<Registrant> getRegistrantsWhoNeverRenewed();
}
