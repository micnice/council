package zw.co.hitrac.council.business.process;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.domain.*;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.domain.accounts.DebtComponent;
import zw.co.hitrac.council.business.domain.accounts.Product;
import zw.co.hitrac.council.business.service.*;
import zw.co.hitrac.council.business.service.accounts.AccountsParametersService;
import zw.co.hitrac.council.business.service.accounts.DebtComponentService;
import zw.co.hitrac.council.business.service.accounts.ReceiptItemService;
import zw.co.hitrac.council.business.utils.CouncilException;
import zw.co.hitrac.council.business.utils.DateUtil;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author Charles Chigoriwa
 * @author Michael Matiashe
 */
@Service
public class RenewalProcess {

    @Autowired
    private RegistrationService registrationService;
    @Autowired
    private GeneralParametersService generalParametersService;
    @Autowired
    private RegistrantDisciplinaryService registrantDisciplinaryService;
    @Autowired
    private RegistrantCpdItemConfigService registrantCpdItemConfigService;
    @Autowired
    private InvoiceProcess invoiceProcess;
    @Autowired
    private RegistrantActivityService registrantActivityService;
    @Autowired
    private RegistrantCpdService registrantCpdService;
    @Autowired
    private DurationService durationService;
    @Autowired
    private CouncilDurationService counciDurationService;
    @Autowired
    private ProductFinder productFinder;
    @Autowired
    private ProductIssuer productIssuer;
    @Autowired
    private RegistrantActivityProcess registrantActivityProcess;
    @Autowired
    private BillingProcess billingProcess;
    @Autowired
    private PenaltyParametersService penaltyParametersService;
    @Autowired
    private RegistrationProcess registrationProcess;
    @Autowired
    private RegistrantQualificationService registrantQualificationService;
    @Autowired
    private AccountsParametersService accountsParametersService;
    @Autowired
    private DebtComponentService debtComponentService;
    @Autowired
    private ReceiptItemService receiptItemService;

    @Transactional
    public void renew(Registrant registrant, final CouncilDuration councilDuration, final Course course) {

        DebtComponent debtComponent = null;
        Boolean reRegistration = Boolean.FALSE;
        Duration duration = null;
        if (councilDuration == null) {
            throw new CouncilException("Select Period to renew");
        }
        if (course == null) {
            throw new CouncilException("Select Discipline to renew");
        }

        Registration registration = null;
        registration = validRenewalRegistration(registrant, course);
        if (registration == null) {
            throw new CouncilException("No current registration with selected attributes");
        }

        duration = durationService.getDurationByCourseAndCouncilDuration(course, councilDuration);
        if (duration == null) {
            throw new CouncilException("Renewal for Discipline not set");
        }

        if (hasCurrentRenewalActivity(registrant, duration)) {
            throw new CouncilException("Current renewal exists");
        }


        if (generalParametersService.get().getAllowRenewalBeforeCPDs()) {
            //TODO: skip checking renewal points...
        } else {
            if (!hasAdequateCPDs(registrant, duration)) {
                throw new CouncilException("No Adequate CPDS");
            }
        }

        if (!registrantDisciplinaryService.getCurrentUnResolved(registrant).isEmpty()) {
            throw new CouncilException("Displinary case not resolved");
        }
        if (!receiptItemService.hasPaidAnnualFeeForCouncilDuration(registrant.getCustomerAccount().getAccount(), councilDuration)) {
            //check annual fee payment before billing
            if (penaltyParametersService.get().getHasReregistrationProduct()) {
                if (!generalParametersService.get().getRegistrationByApplication() && !generalParametersService.get().getAllowPreRegistrantion()) {
                    if (monthsBetween(new Date(), duration.getStartDate()) > penaltyParametersService.get().getReRegistrationPeriod()) {
                        if (accountsParametersService.get().getBillEachPostBasicQualification()) {
                            for (Course c : registrantQualificationService.getRegisteredDisciplines(registrant)) {
                                try {
                                    billingProcess.billReRegistrationProduct(registrant, c);
                                } catch (Exception e) {
                                    //Ignore----- Discipline not found for registrant qualification, billing will be skipped
                                }
                            }
                        } else {
                            billingProcess.billReRegistrationProduct(registration);
                        }
                    }

                    if (monthsBetween(new Date(), duration.getStartDate()) > penaltyParametersService.get().getPenaltyPeriod()) {
                        billingProcess.billPenaltyProduct(registration);
                    }
                }
            }

            if (generalParametersService.get().getRegistrationByApplication() || generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
                billWithPenalty(registrant);
                if (accountsParametersService.get().getPractisingCertificatePayment()) {
                    debtComponent = billingProcess.billPracticingCertificateProduct(registration, debtComponent);
                }
                if (!reRegistration) {
                    if (accountsParametersService.get().getPayingMaximumAge()) {
                        if (registration.getRegistrant().getIntegerValueOfAge() < accountsParametersService.get().getPayingPractitionersMaximumAge()) {
                            debtComponent = billingProcess.billAnnualProduct(registration, debtComponent, registration.getApplication());
                        }
                    } else {
                        debtComponent = billingProcess.billAnnualProduct(registration, debtComponent, registration.getApplication());
                    }
                }
            } else {

                if (accountsParametersService.get().getPayingMaximumAge()) {
                    if (registration.getRegistrant().getIntegerValueOfAge() < accountsParametersService.get().getPayingPractitionersMaximumAge()) {
                        debtComponent = billingProcess.billAnnualProduct(registration, debtComponent, registration.getApplication());
                    }
                } else {
                    debtComponent = billingProcess.billAnnualProduct(registration, debtComponent, registration.getApplication());
                }
            }
        }

        if (generalParametersService.get().getAllowRenewalBeforeCPDs()) {
            createRenewalPaidForValid(registrant, duration);
        } else {
            if (registration.getCourse().getSpecial()) {
                productIssuer.issueProductSpecialPracticeCertificate(registration, debtComponent);
            } else {
                productIssuer.issueProductPracticeCertificate(registration, debtComponent, duration);
            }
            registrantActivityProcess.renewalRegistrantActivity(registration, debtComponent, duration);
        }

    }

    public void renew(Registrant registrant, List<Duration> durations, Course course) {

        if (course == null) {
            throw new CouncilException("Select Discipline to renew");
        }

        Registration registration = null;
        registration = validRenewalRegistration(registrant, course);
        if (registration == null) {
            throw new CouncilException("No current registration with selected attributes");
        }

        Collections.sort(durations, (d1, d2) -> d2.getEndDate().compareTo(d1.getEndDate()));

        for (int i = 0; i < durations.size(); i++) {

            Duration duration = durations.get(i);

            if (duration.getCouncilDuration() == null) {
                throw new CouncilException("Council Duration not found");
            }

            if (hasCurrentRenewalActivity(registrant, duration))
                throw new CouncilException("Current renewal exists " + duration.getName());

            if (!hasPaidAnnualFeeForCouncilDuration(registrant.getCustomerAccount().getAccount(), duration.getCouncilDuration()))
                throw new CouncilException("No Payment for " + duration.getCouncilDuration().getName());


            if (i == 0) {
                createRenewalPaidForValid(registrant, duration);
            } else {
                createRenewalForDuration(registrant, duration);
            }

        }
    }

    public String getValueToBePaid(Registrant registrant) {
        if (!generalParametersService.get().getAllowPreRegistrantion() || generalParametersService.get().getGenerateRegnumberOnRegistrant()) {
            return StringUtils.EMPTY;
        }

        if (!hasCurrentRenewalActivity(registrant)) {
            return StringUtils.EMPTY;
        } else {
            Duration duration = null;
            BigDecimal price = BigDecimal.ZERO;
            //price = price.add(registrant.getCustomerAccount().getAccount().getBalance());

            Registration registration = registrationProcess.activeRegisterNotStudentRegister(registrant);
            if (registration != null) {
                duration = checkDuration(registration.getCourse());
                if (duration != null) {
                    CouncilDuration councilDuration1 = checkRenewalPeriod(duration);
                    if (councilDuration1 != null) {
                        RegistrantActivity registrantActivity = null;
                        registrantActivity = registrantActivityService.getRegistrantActivityByDuration(registrant, duration);
                        if (duration.getActive()) {
                            if (registrantActivity == null) {
                                Product reRegistrationProduct = null;
                                if (penaltyParametersService.get().getHasReregistrationProduct()) {
                                    reRegistrationProduct = productFinder.searchReRegistrationProduct(registration);
                                }
                                Product product = productFinder.searchAnnualProduct(registration);
                                if (product != null) {
                                    int differenceInMonths = (int) (DateUtil.DifferenceInMonths(duration.getStartDate(), new Date())) + 1;

                                    //generic
                                    if (registration.getRegister().isQuarterlyRenewal()) {
                                        int quarters = differenceInMonths / 3;


                                        if (differenceInMonths < penaltyParametersService.get().getReRegistrationPeriod()) {
                                            price = price.add(product.getProductPrice().getPrice().multiply(new BigDecimal(quarters * penaltyParametersService.get().getPercentagePenalty() / 100)));


                                        } else {
                                            price = price.add(reRegistrationProduct.getProductPrice().getPrice());

                                        }

                                    } else {
                                        if (differenceInMonths < penaltyParametersService.get().getReRegistrationPeriod()) {
                                            BigDecimal penalty = product.getProductPrice().getPrice();
                                            double penaltyRate = (differenceInMonths * penaltyParametersService.get().getPercentagePenalty());
                                            penalty = penalty.multiply(new BigDecimal(penaltyRate));
                                            penalty = penalty.divide(new BigDecimal("100"));
                                            price = penalty;

                                        } else {
                                            if (penaltyParametersService.get().getHasReregistrationProduct()) {
                                                price = price.add(reRegistrationProduct.getProductPrice().getPrice());

                                            }
                                        }
                                    }
                                    price = price.add(product.getProductPrice().getPrice());

                                }

                            }
                        }
                    }
                }
            }
            return " - " + round(price, 2, true);
        }

    }

    public String getAmountDue(Registrant registrant) {

        Registration registration = null;
        Duration duration = null;
        BigDecimal price = BigDecimal.ZERO;
        price = price.add(registrant.getCustomerAccount().getAccount().getBalance());

        String outpustring = null;
        BigDecimal totals;
        try {
            registration = registrationProcess.activeRegisterNotStudentRegister(registrant);
            CouncilDuration councilDuration = registrantActivityService.getLastCouncilDurationRenewal(registrant);
            Product product = productFinder.searchPenaltyProduct(registration);

            if (!registration.getRegister().isQuarterlyRenewal()) {
                if (councilDuration != null) {
                    Set<Duration> durations = councilDuration.getDurations();
                    Duration lastDuration = durations.iterator().next();

                    for (Duration k : durations) {
                        if (lastDuration.getEndDate().compareTo(k.getEndDate()) < 0) {
                            lastDuration = k;
                        }

                    }

                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


                    duration = lastDuration;


                }
            } else if (registration.getRegister().isQuarterlyRenewal()) {
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                duration = durationService.getCurrentCourseDuration(registration.getCourse());

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            }

            int differenceInMonths = (int) (DateUtil.DifferenceInMonths(duration.getStartDate(), new Date())) + 1;


            totals = new BigDecimal(0);

            //When months less than 8 then chharge fines per
            if (differenceInMonths < 8) {

                totals.add(product.getProductPrice().getPrice().multiply(new BigDecimal(0.2)).multiply(new BigDecimal(differenceInMonths)));

                price.add(totals);

                outpustring = "Student can pay $";
                return outpustring + round(price, 2, true);
            } else {

                outpustring = "Student can pay $";

                //Months greater than 7  a re application is processed .
                price.add(product.getProductPrice().getPrice());
                return outpustring + round(price, 2, true);

            }
        } catch (Exception ex) {

            if (registrant.getCustomerAccount().getAccount().getBalance().doubleValue() > 0) {
                return outpustring = "The student does not have a current registration however they can pay $" + price;
            } else {
                return outpustring = "The student does not have a current registration.";
            }

        }


        // return outpustring+ "- " + round(price, 2, true);
    }

    public void billWithPenalty(Registrant registrant) {
        Registration registration = null;
        Duration duration = null;
        BigDecimal price = BigDecimal.ZERO;
        registration = registrationProcess.activeRegisterNotStudentRegister(registrant);
        if (registration != null) {
            duration = checkDuration(registration.getCourse());
            if (duration != null) {
                CouncilDuration councilDuration1 = checkRenewalPeriod(duration);
                if (councilDuration1 != null) {
                    RegistrantActivity registrantActivity = null;
                    registrantActivity = registrantActivityService.getRegistrantActivityByDuration(registrant, duration);
                    if (duration.getActive()) {
                        Product reRegistrationProduct = null;
                        if (registrantActivity == null) {
                            if (penaltyParametersService.get().getHasReregistrationProduct()) {
                                reRegistrationProduct = productFinder.searchReRegistrationProduct(registration);
                            }
                            Product product = productFinder.searchAnnualProduct(registration);
                            Product penaltyProduct = productFinder.searchPenaltyProduct(registration);

                            //generic penalty
                            if (product != null) {
                                int differenceInMonths = (int) (DateUtil.DifferenceInMonths(duration.getStartDate(), new Date()));
                                if (registration.getRegister().isQuarterlyRenewal()) {
                                    double quarters1 = differenceInMonths / 3.0;

                                    int quarters = 0;
                                    if (quarters1 > 0.0 && quarters1 < 1.0) {

                                        quarters = 1;
                                    } else {
                                        quarters = (int) Math.ceil(quarters1);
                                    }

                                    if (differenceInMonths < penaltyParametersService.get().getReRegistrationPeriod()) {
                                        double percentage = new BigDecimal((double) penaltyParametersService.get().getPercentagePenalty() / (double) 100).doubleValue();
                                        price = price.add(product.getProductPrice().getPrice().multiply(new BigDecimal(quarters * percentage)));
                                        invoiceProcess.invoice(price, registrant.getCustomerAccount(), penaltyProduct.getSalesAccount(), product);
                                    } else {

                                        price = price.add(reRegistrationProduct.getProductPrice().getPrice().multiply(new BigDecimal(quarters)));
                                        penaltyProduct.getProductPrice().setPrice(price);
                                        invoiceProcess.invoice(price, registrant.getCustomerAccount(), penaltyProduct.getSalesAccount(), product);
                                    }

                                } else {

                                    if (differenceInMonths < penaltyParametersService.get().getReRegistrationPeriod()) {
                                        Double multipli = new Double((differenceInMonths * penaltyParametersService.get().getPercentagePenalty()));
                                        price = price.add(product.getProductPrice().getPrice().multiply(new BigDecimal(multipli)));
                                        price = price.divide(new BigDecimal("100"));


                                        penaltyProduct.getProductPrice().setPrice(price);
                                        invoiceProcess.invoice(price, registrant.getCustomerAccount(), penaltyProduct.getSalesAccount(), product);
                                    } else {
                                        if (penaltyParametersService.get().getHasReregistrationProduct()) {

                                            price = price.add(reRegistrationProduct.getProductPrice().getPrice());

                                            DebtComponent debtComponent = invoiceProcess.invoice(price, registrant.getCustomerAccount(), reRegistrationProduct.getSalesAccount(), product);
                                            productIssuer.issueRegistrationOrAdditionalQualficationCertificate(registration, debtComponent);
                                            registrantActivityProcess.registrationAndRenewalActivity(registration, debtComponent, duration);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static BigDecimal round(BigDecimal d, int scale, boolean roundUp) {
        int mode = (roundUp) ? BigDecimal.ROUND_UP : BigDecimal.ROUND_DOWN;
        return d.setScale(scale, mode);
    }

    //Check for double renewal

    /**
     * @param minuend
     * @param subtrahend
     * @return
     */
    public int monthsBetween(Date minuend, Date subtrahend) {
        Calendar cal = Calendar.getInstance();
        // default will be Gregorian in US Locales
        cal.setTime(minuend);
        int minuendMonth = cal.get(Calendar.MONTH);
        int minuendYear = cal.get(Calendar.YEAR);
        cal.setTime(subtrahend);
        int subtrahendMonth = cal.get(Calendar.MONTH);
        int subtrahendYear = cal.get(Calendar.YEAR);

        // the following will work okay for Gregorian but will not
        // work correctly in a Calendar where the number of months
        // in a year is not constant
        return ((minuendYear - subtrahendYear) * cal.getMaximum(Calendar.MONTH))
                + (minuendMonth - subtrahendMonth);
    }

    private boolean hasCurrentRenewalActivity(Registrant registrant) {
        List<RegistrantActivity> registrantActivities = this.registrantActivityService.getRegistrantActivities(registrant, RegistrantActivityType.RENEWAL);
        for (RegistrantActivity registrantActivity : registrantActivities) {
            if (registrantActivity.getEndDate().after(new Date())) {
                return true;
            }
        }
        return false;
    }


    public boolean hasCurrentRenewalActivity(Registrant registrant, Duration duration) {
        List<RegistrantActivity> registrantActivities = this.registrantActivityService.getRegistrantActivities(registrant, RegistrantActivityType.RENEWAL);
        for (RegistrantActivity registrantActivity : registrantActivities) {
            if (registrantActivity.getDuration() != null) {
                if (registrantActivity.getDuration().equals(duration)) {
                    return Boolean.TRUE;
                }
            }
        }

        return false;
    }


    public boolean hasPenalty(Course course) {
        if (!durationService.getCurrentCourseDuration(course).getEndDate().after(new Date())) {
            return true;
        }
        return false;
    }

    public void addCPDs(Registrant registrant, CouncilDuration councilDuration, List<RegistrantCpd> items) {
        Registration registration = registrationProcess.activeRegisterNotStudentRegister(registrant);
        if (registration != null) {
            Duration duration = durationService.getDurationByCourseAndCouncilDuration(registration.getCourse(), councilDuration);
            if (duration == null) {
                throw new CouncilException("Renewal for Discipline not set");
            }
            for (RegistrantCpd cpd : items) {
                cpd.setDuration(duration);
                registrantCpdService.save(cpd);
            }
            createRenewalPaidForValid(registrant, duration);
        }
    }

    public boolean has3monthPenalty(Course course) {
        return (durationService.getCurrentCourseDuration(course).getEndDate().after(new Date()));
    }

    public Duration checkDuration(Course course) {
        return durationService.getCurrentCourseDuration(course);
    }

    public CouncilDuration checkRenewalPeriod(Duration duration) {
        return counciDurationService.getCouncilDurationFromDuration(duration);
    }

    //this method was made for Allied council.Purpose is to increment penalty quartely when a registrant fails to pay his annual fees  on time
    public void incrementPenalty(Registration registration, Registrant registrant, Course course, RegistrantActivity registrantActivity, Product quarterlyProduct) {

        productFinder.searchPenaltyProduct(registration);
        if (!hasCurrentRenewalActivity(registrant)) {
            if (hasPenalty(course)) {
            }
        }
    }
    //Method to calculate quarters  in a year

    public int createQuarters(Calendar calendar) {
        int month = calendar.get(Calendar.MONTH);
        return (month >= Calendar.JANUARY && month <= Calendar.MARCH) ? 1
                : (month >= Calendar.APRIL && month <= Calendar.JUNE) ? 2
                : (month >= Calendar.JULY && month <= Calendar.SEPTEMBER) ? 3 : 4;
    }

    public Boolean checkValidDisplinaryPeriod(Registrant registrant, Duration duration) {
        List<RegistrantDisciplinary> disciplinarys = registrantDisciplinaryService.getCurrentUnResolved(registrant);
        for (RegistrantDisciplinary r : disciplinarys) {
            if (duration.getStartDate().before(r.getPenaltyEndDate()) && r.getDateResolved() == null) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    @Transactional
    public Registration validRenewalRegistration(Registrant registrant, Course course) {

        Registration registration = null;

        if (generalParametersService.get().getStudentRegister() == null) {
            throw new CouncilException("Student Register not Set");
        }
        registration = registrationService.getCurrentRegistrationByCourseAndRegister(registrant, generalParametersService.get().getStudentRegister(), course);
        //Check if not for an active registration
        if (registration != null) {
            throw new CouncilException("Students cannot renew");
        }
        //Check for an active registration
        if (registration == null) {
            if (generalParametersService.get().getMainRegister() == null) {
                throw new CouncilException("Main Register not Set");
            }
            registration = registrationService.getCurrentRegistrationByCourseAndRegister(registrant, generalParametersService.get().getMainRegister(), course);
        }
        if (registration == null) {
            if (generalParametersService.get().getProvisionalRegister() == null) {
                throw new CouncilException("Provisional Register not Set");
            }
            registration = registrationService.getCurrentRegistrationByCourseAndRegister(registrant, generalParametersService.get().getProvisionalRegister(), course);
        }
        if (registration == null) {
            if (generalParametersService.get().getSpecialistRegister() == null) {
                throw new CouncilException("Specialist Register not Set");
            }
            registration = registrationService.getCurrentRegistrationByCourseAndRegister(registrant, generalParametersService.get().getSpecialistRegister(), course);
        }
        if (registration == null) {
            if (generalParametersService.get().getInternRegister() == null) {
                throw new CouncilException("Intern Register not Set");
            }
            registration = registrationService.getCurrentRegistrationByCourseAndRegister(registrant, generalParametersService.get().getInternRegister(), course);
        }
        if (registration == null) {
            if (generalParametersService.get().getMaintenanceRegister() == null) {
                throw new CouncilException("Intern Register not Set");
            }
            registration = registrationService.getCurrentRegistrationByCourseAndRegister(registrant, generalParametersService.get().getMaintenanceRegister(), course);
        }
        if (registration == null) {
            throw new CouncilException("There is no current registration to renew");
        }

        return registration;

    }

    public boolean hasAdequateCPDs(Registrant registrant, Duration duration) {

        if (!((registrantCpdService.getCurrentCPDs(registrant, duration).compareTo(registrantCpdItemConfigService.get().getTotalCpdPointsPermissable()) >= 0))) {
            return false;
        }
        return true;
    }


    private boolean hasAdequateCPDs(Registrant registrant, List<Duration> durations) {

        for (Duration duration : durations) {

            if (!((registrantCpdService.getCurrentCPDs(registrant, duration).compareTo(registrantCpdItemConfigService.get().getTotalCpdPointsPermissable()) >= 0))) {
                return false;
            }
        }
        return true;
    }

    public boolean hasCurrentAdequateCPDs(Registrant registrant) {

        Registration registration = registrationProcess.activeRegisterNotStudentRegister(registrant);

        if (registration != null) {

            if (checkFirstTimeRegistrationPeriod(registration)) {
                return true;
            }

            Duration duration = durationService.getCurrentCourseDuration(registration.getCourse());
            if (!((registrantCpdService.getCurrentCPDs(registrant, duration).compareTo(registrantCpdItemConfigService.get().getTotalCpdPointsPermissable()) >= 0))) {
                return false;
            }
        }

        return true;
    }

    private boolean checkFirstTimeRegistrationPeriod(Registration registration) {

        Duration duration = durationService.getCurrentCourseDuration(registration.getCourse());

        if (registrantActivityService.checkRegistrantActivity(registration.getRegistrant(), RegistrantActivityType.REGISTRATION, duration)) {
            return true;
        }
        return false;

    }


    public boolean hasPendingAccountDebtComponentsByTypeOfService(Registrant registrant, TypeOfService typeOfService) {
        return debtComponentService.hasAccountDebtComponents(registrant.getCustomerAccount(), typeOfService);
    }

    public boolean hasPaidAnnualFeeForCouncilDuration(Account account, CouncilDuration duration) {
        if (!this.generalParametersService.get().isBillingModule()) {
            return true;
        }
        return receiptItemService.hasPaidAnnualFeeForCouncilDuration(account, duration);
    }

    public void createRenewalPaidForValid(Registrant registrant, Duration duration) {

        if (generalParametersService.get().getAllowRenewalBeforeCPDs()) {
            Registration registration = registrationProcess.activeRegisterNotStudentRegister(registrant);
            if (registration != null) {
                if (!hasCurrentRenewalActivity(registrant, duration)) {
                    if (hasAdequateCPDs(registrant, duration) && hasPaidAnnualFeeForCouncilDuration(registrant.getCustomerAccount().getAccount(), duration.getCouncilDuration())) {
                        if (registration.getCourse().getSpecial()) {
                            productIssuer.issueProductSpecialPracticeCertificate(registration, null);
                        } else {
                            productIssuer.issueProductPracticeCertificate(registration, null, duration);
                        }
                        registrantActivityProcess.renewalRegistrantActivity(registration, null, duration);
                    }
                }
            }
        }
    }

    public void createRenewalForDuration(Registrant registrant, Duration duration) {

        Registration registration = registrationProcess.activeRegisterNotStudentRegister(registrant);
        if (registration != null) {
            if (!hasCurrentRenewalActivity(registrant, duration)) {
                if (hasPaidAnnualFeeForCouncilDuration(registrant.getCustomerAccount().getAccount(), duration.getCouncilDuration())) {
                    registrantActivityProcess.renewalRegistrantActivity(registration, null, duration);
                }
            }
        }
    }


    public void createRenewalPaidForCurrentPeriod(Registrant registrant) {

        Registration registration = registrationProcess.activeRegisterNotStudentRegister(registrant);
        if (registration != null) {
            Duration duration = durationService.getCurrentCourseDuration(registration.getCourse());
            createRenewalPaidForValid(registrant, duration);
        }

    }

    public boolean hasCurrentRenewal(Registrant registrant) {
        if (registrationProcess.studentNoneRegistration(registrant)) {
            Registration registration = registrationProcess.activeRegisterNotStudentRegister(registrant);
            if (registration != null) {
                Course course = registration.getCourse();
                Duration duration = durationService.getCurrentCourseDuration(course);
                return hasCurrentRenewalActivity(registrant, duration);
            }
        }
        return false;
    }
}