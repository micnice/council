package zw.co.hitrac.council.web.pages.configure;

import java.util.List;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.ContactType;
import zw.co.hitrac.council.business.service.ContactTypeService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;

/**
 *
 * @author Matiashe Michael
 */
public class ContactTypeListPage extends IAdministerDatabaseBasePage{
    
    @SpringBean
    private ContactTypeService contactTypeService;

    public ContactTypeListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink",AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink",AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {

            @Override
            public void onClick() {
               setResponsePage(new ContactTypeEditPage(null));
            }
        });
        
        IModel<List<ContactType>> model=new LoadableDetachableModel<List<ContactType>>() {

            @Override
            protected List<ContactType> load() {
               return contactTypeService.findAll();
            }
        };
        
        PropertyListView<ContactType> eachItem=new PropertyListView<ContactType>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<ContactType> item) {
                Link<ContactType> viewLink=new Link<ContactType>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                       setResponsePage(new ContactTypeViewPage(getModelObject().getId()));
                    }
                };
                if(item.getIndex()%2==0){
                    item.add(new EvenTableRowBehavior());
                }
                item.add(viewLink);
                viewLink.add(new Label("name"));
            }
        };
        
        add(eachItem);
    }
    
    
    
}
