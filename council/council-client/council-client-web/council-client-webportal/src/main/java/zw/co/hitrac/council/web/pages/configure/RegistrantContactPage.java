/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package zw.co.hitrac.council.web.pages.configure;

import java.util.List;
import javax.persistence.OneToMany;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import zw.co.hitrac.council.business.domain.Registrant;
import zw.co.hitrac.council.business.domain.RegistrantAddress;
import zw.co.hitrac.council.business.domain.RegistrantContact;
import zw.co.hitrac.council.business.service.RegistrantContactService;
import zw.co.hitrac.council.web.misc.ErrorBehavior;
import zw.co.hitrac.council.web.pages.registration.RegistrantViewPage;


/**
 *
 * @author Clive Gurure
 */

public class RegistrantContactPage extends IAdministerDatabaseBasePage{
    
    @OneToMany
    private Registrant registrant;
    
    @SpringBean
    RegistrantContactService registrantContactService;
 
    public Registrant getRegistrant() {
        return registrant;
    }

    public void setRegistrant(Registrant registrant) {
        this.registrant = registrant;
    }

    public RegistrantContactPage(final IModel<Registrant> registrantModel) {
        RegistrantAddress registrantAddress = new RegistrantAddress();
        registrantAddress.setRegistrant((registrantModel.getObject()));

        CompoundPropertyModel<RegistrantAddress> model = new CompoundPropertyModel<RegistrantAddress>(registrantAddress);

        setDefaultModel(model);
        
        Form<RegistrantContact> form = new Form<RegistrantContact>("form", (IModel<RegistrantContact>) getDefaultModel()) {
            @Override
            public void onSubmit() {
                registrantContactService.save(getModelObject());
                setResponsePage(new RegistrantViewPage(this.getModelObject().getId()));
            }
        };

        form.add(new Label("registrant.fullname"));
        form.add(new TextField<String>("contactLine1").setRequired(false).add(new ErrorBehavior()));
        form.add(new DropDownChoice("contactType", registrantContactService.findAll()));
        add(form);
        form.add(new BookmarkablePageLink<Void>("returnLink", RegistrantViewPage.class));
        add(new FeedbackPanel("feedback"));
    }

  
}
