
package zw.co.hitrac.council.business.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import zw.co.hitrac.council.business.dao.accounts.AllocationDAO;
import zw.co.hitrac.council.business.domain.accounts.Allocation;
import zw.co.hitrac.council.business.service.accounts.AllocationService;

import java.util.List;

/**
 *
 * @author Constance Mabaso
 */
@Service
@Transactional
public class AllocationServiceImpl implements AllocationService {

    @Autowired
    private AllocationDAO allocationDao;

    @Transactional
    public Allocation save(Allocation t) {
        return allocationDao.save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Allocation> findAll() {
        return allocationDao.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Allocation get(Long id) {
        return allocationDao.get(id);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Allocation> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setAllocationDAO(AllocationDAO allocationDAO) {

        this.allocationDao = allocationDAO;
    }
}
