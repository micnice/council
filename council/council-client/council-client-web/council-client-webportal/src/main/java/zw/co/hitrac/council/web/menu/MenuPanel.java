package zw.co.hitrac.council.web.menu;

import org.apache.wicket.Component;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import zw.co.hitrac.council.web.misc.CurrentMenuBehavior;

/**
 *
 * @author Charles Chigoriwa
 */
public abstract class MenuPanel extends Panel{
    
    protected  boolean topMenuCurrent;

    public MenuPanel(String id) {
        super(id);
    }

    public MenuPanel(String id, IModel<?> model) {
        super(id, model);
    }

    public boolean isTopMenuCurrent() {
        return topMenuCurrent;
    }

    public void setTopMenuCurrent(boolean topMenuCurrent) {
        this.topMenuCurrent = topMenuCurrent;
    }
    
    public void addCurrentBehavior(Component component,boolean currentMenu){
        component.remove(component.getBehaviors(CurrentMenuBehavior.class).toArray(new Behavior[0]));
        component.add(new CurrentMenuBehavior(currentMenu));
    }

        
}
