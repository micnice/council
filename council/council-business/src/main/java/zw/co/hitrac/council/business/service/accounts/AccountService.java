
package zw.co.hitrac.council.business.service.accounts;

import java.util.List;
import zw.co.hitrac.council.business.domain.Registration;
import zw.co.hitrac.council.business.domain.accounts.Account;
import zw.co.hitrac.council.business.service.IGenericService;
import zw.co.hitrac.council.business.utils.TakeOnBalanceDMO;

/**
 *
 * @author Tatenda Chiwandire
 * @author Kelvin Goredema
 * @author Charles Chigoriwa

 */
public interface AccountService extends IGenericService<Account> {
    public List<Account> getGeneralLedgerAccounts();
    
    public List<Account> getAccountsByPersonalAccountType(Boolean personalAccount);
    
    public void getRegistrantTakeOnBalance(TakeOnBalanceDMO item);
    
    public Double getNumYears(Integer lastPaid, Registration reg);
    
    public List<Account> getPersonalAccountsErrors();

    public int update(Account account);
}
