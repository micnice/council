package zw.co.hitrac.council.business.dao.impl;

import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import zw.co.hitrac.council.business.dao.CourseGroupDAO;
import zw.co.hitrac.council.business.dao.repo.CourseGroupRepository;
import zw.co.hitrac.council.business.domain.CourseGroup;

/**
 *
 * @author Charles Chigoriwa
 */
@Repository
public class CourseGroupDAOImpl implements CourseGroupDAO{
   @Autowired
   private CourseGroupRepository courseGroupRepository;
    
   public CourseGroup save(CourseGroup courseGroup){
       return courseGroupRepository.save(courseGroup);
   }
   
   public List<CourseGroup> findAll(){
       return courseGroupRepository.findAll();
   }
   
   public CourseGroup get(Long id){
       return courseGroupRepository.findOne(id);
   }
 /**
     * A setter method that will make mocking repo object easier
     * @param courseGroupRepository 
     */
   
   public void setCourseGroupRepository(CourseGroupRepository courseGroupRepository) {
         this.courseGroupRepository = courseGroupRepository;
    }

    public List<CourseGroup> findAll(Boolean retired) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
