
/*
* To change this template, choose Tools | Templates
* and open the template in the editor.
 */
package zw.co.hitrac.council.web.pages.examinations;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import zw.co.hitrac.council.business.domain.examinations.Exam;
import zw.co.hitrac.council.business.service.examinations.ExamService;
import zw.co.hitrac.council.business.service.examinations.ExamSettingService;
import zw.co.hitrac.council.web.misc.EvenTableRowBehavior;
import zw.co.hitrac.council.web.pages.configure.AdministerDatabasePage;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;

/**
 *
 * @author tdhlakama
 */
public class ExamListPage extends IExaminationsPage {
    @SpringBean
    private ExamService        examService;
    @SpringBean
    private ExamSettingService examSettingService;

    public ExamListPage() {
        add(new BookmarkablePageLink<Void>("administerDatabaseLink", AdministerDatabasePage.class));
        add(new BookmarkablePageLink<Void>("returnLink", AdministerDatabasePage.class));
        add(new Link<Void>("addNewLink") {
            @Override
            public void onClick() {
                setResponsePage(new ExamEditPage(null));
            }
        });

        IModel<List<Exam>> model = new LoadableDetachableModel<List<Exam>>() {
            @Override
            protected List<Exam> load() {
                return examService.findAll();
            }
        };
        PropertyListView<Exam> eachItem = new PropertyListView<Exam>("eachItem", model) {
            @Override
            protected void populateItem(ListItem<Exam> item) {
                Link<Exam> viewLink = new Link<Exam>("viewLink", item.getModel()) {
                    @Override
                    public void onClick() {
                        setResponsePage(new ExamViewPage(getModelObject().getId()));
                    }
                };

                item.add(viewLink);
                viewLink.add(new Label("examDate"));
                item.add(new Label("examTime"));
                item.add(new Label("modulePaper"));
                item.add(new Label("examSetting"));
            }
        };

        add(eachItem);
    }
}


//~ Formatted by Jindent --- http://www.jindent.com
